﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
namespace SMAS.Web.Areas.LookupInfoReportArea.Models
{
    public class LookupInfoReportViewModel
    {
        [ResourceDisplayName("LookupInfo_Label_chkFullName")]
        public string Fullname { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkEmployeeCode")]
        public string EmployeeCode { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkBirthDate")]
        public DateTime? BirthDate { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkSex")]
        public bool Genre { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkSex")]
        public string GenreName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkEthnic")]
        public string EthnicName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkReligion")]
        public string ReligionResolution { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkIdentifyNumber")]
        public string IdentifyNumber { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkIdentifyIssuedDate")]
        public DateTime? IdentifyIssuedDate { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkIdentifyIssuedPlace")]
        public string IdentifyIssuedPlace { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkMobile")]
        public string Mobile { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkEmail")]
        public string Email { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkHomeTown")]
        public string HomeTown { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPermanentResidentalAddress")]
        public string PermanentResidentalAddress { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkYouthLeageMember")]
        public bool? IsYouthLeageMember { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkYouthLeageMember")]
        public string IsYouthLeageMemberName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkYouthLeagueJoinedDate")]
        public DateTime? YouthLeagueJoinedDate { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkCommunistPartyMember")]
        public bool? IsCommunistPartyMember { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkCommunistPartyMember")]
        public string IsCommunistPartyMemberName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkCommunistPartyJoinedDate")]
        public DateTime? CommunistPartyJoinedDate { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkFatherFullName")]
        public string FatherFullName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkFatherBirthDate")]
        public DateTime? FatherBirthDate { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkMotherFullName")]
        public string MotherFullName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkMotherBirthDate")]
        public DateTime? MotherBirthDate { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkSpouseFullName")]
        public string SpouseFullName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkSpouseBirthDate")]
        public DateTime? SpouseBirthDate { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkGraduationLevel")]
        public string GraduationLevelResolution { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkQualificationLevel")]
        public string QualificationLevelResolution { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkForeignLanguageGrade")]
        public string ForeignLanguageGradeResolution { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkITQualificationLevel")]
        public string ITQualificationLevelResolution { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPoliticalGrade")]
        public string PoliticalGradeResolution { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkSpecialityCat")]
        public string SpecialityCatResolution { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkContractType")]
        public string ContractTypeResolution { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkWorkType")]
        public string WorkTypeResolution { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkSchoolFaculty")]
        public string SchoolFacultyFacultyName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkSchool")]
        public string SchoolName { get; set; }


        public string Name { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkFatherBirthDate")]
        public string FatherYearOld { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkMotherBirthDate")]
        public string MotherYearOld { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkSpouseBirthDate")]
        public string SpouseYearOld { get; set; }
    }
}