﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LookupInfoReportArea.Models
{
    public class EmployeeSalaryVM
    {
        public int EmployeeSalaryID { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public Nullable<DateTime> AppliedDate { get; set; }
        public Nullable<int> EmployeeScaleID { get; set; }
        public string EmployeeScaleCode { get; set; }
        public string EmployeeScaleName { get; set; }
        public Nullable<int> SalaryLevelID { get; set; }
        public string SalaryLevelName { get; set; }
        public decimal? Coefficient { get; set; }
        public string CoefficientName
        {
            get
            {
                string strval = string.Empty;
                if (Coefficient.HasValue)
                {
                    strval = Coefficient.Value.ToString("0.00").Replace(",", ".");
                }
                return strval;
            }
        }
        public Nullable<int> SchoolID { get; set; }
        public Nullable<int> SupervisingDeptID { get; set; }
        public string SalaryResolution { get; set; }
        public int? SalaryAmount { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}