﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LookupInfoReportArea.Models
{
    public class InsertTemplateEmployeeVM
    {
        public int ColDesId { get; set; }
        public string ColExcel { get; set; }
        public string ExcelName { get; set; }
    }
}