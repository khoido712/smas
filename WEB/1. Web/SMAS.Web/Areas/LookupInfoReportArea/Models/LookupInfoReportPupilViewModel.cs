using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
namespace SMAS.Web.Areas.LookupInfoReportArea.Models
{
    public class LookupInfoReportPupilViewModel
    {
        [ResourceDisplayName("LookupInfo_Label_chkPFullName")]
        public string Fullname { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPPupilCode")]
        public string PupilCode { get; set; }


        [ResourceDisplayName("LookupInfo_Label_chkPEducationLevel")]
        public string EducationLevel { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPClass")]
        public string Class { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPBirthDate")]
        public DateTime? BirthDate { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPSex")]
        public int Genre { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPSex")]
        public string GenreName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPStatus")]
        public int Status { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPStatus")]
        public string StatusName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPEthnic")]
        public string EthnicName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPReligion")]
        public string ReligionResolution { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPHomeTown")]
        public string HomeTown { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPPermanentResidentalAddress")]
        public string PermanentResidentalAddress { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPFamilyType")]
        public string FamilyType { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPPolicyTarget")]
        public string PolicyTarget { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPFatherFullName")]
        public string FatherFullName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPFatherBirthDate")]
        public DateTime? FatherBirthDate { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPFatherMobile")]
        public string FatherMobile { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPMotherFullName")]
        public string MotherFullName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPMotherBirthDate")]
        public DateTime? MotherBirthDate { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPMotherMobile")]
        public string MotherMobile { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPSchool")]
        public string School { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPProvince")]
        public string Province { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPDistrict")]
        public string District { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPCommune")]
        public string Commune { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPBirthPlace")]
        public string BirthPlace { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPFatherJob")]
        public string FatherJob { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPMotherJob")]
        public string MotherJob { get; set; }

        [ResourceDisplayName("LookupInfo_Label_chkPSponsorFullName")]
        public string SponsorFullName { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPSponsorBirthDate")]
        public DateTime? SponsorBirthDate { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPSponsorJob")]
        public string SponsorJob { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPSponsorMobile")]
        public string SponsorMobile { get; set; }

        [ResourceDisplayName("LookupInfo_Label_chkPSponsorBirthDate")]
        public string SponsorYearOld { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPFatherBirthDate")]
        public string FatherYearOld { get; set; }
        [ResourceDisplayName("LookupInfo_Label_chkPMotherBirthDate")]
        public string MotherYearOld { get; set; }
        public int PupilID { get; set; }
        public int EducationLevelID { get; set; }
        public int? OrderInClass { get; set; }
        public string Name { get; set; }
        public int? ClassOrderNumber { get; set; }
    }
}
