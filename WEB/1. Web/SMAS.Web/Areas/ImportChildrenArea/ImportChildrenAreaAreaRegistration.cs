﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ImportChildrenArea
{
    public class ImportChildrenAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ImportChildrenArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ImportChildrenArea_default",
                "ImportChildrenArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
