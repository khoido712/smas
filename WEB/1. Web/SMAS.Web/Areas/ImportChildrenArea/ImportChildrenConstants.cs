using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ImportChildrenArea
{
    public class ImportChildrenConstants
    {
        public const string LIST_EDUCATION_LEVEL = "list_education_level";

        public const string LIST_IMPORTED_PUPIL = "LIST_IMPORTED_PUPIL";
        public const string DISABLE_BTNSAVE = "DISABLE_BTNSAVE";
        public const string MESSAGE_ERROR_IMPORT = "MessageErrorImport";
        public const string MESSAGE_STATUS_SUCCESS = "success";
    }
}