﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Common;
using System.Globalization;
using SMAS.Web.Areas.ImportChildrenArea.Models;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.ComponentModel;
using SMAS.Business.Business;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.ImportChildrenArea.Controllers
{
    public class ImportChildrenController : BaseController
    {
        IPupilProfileBusiness PupilProfileBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        IEducationLevelBusiness EducationLevelBusiness;
        IClassProfileBusiness ClassProfileBusiness;
        IProvinceBusiness ProvinceBusiness;
        IPolicyTargetBusiness PolicyTargetBusiness;
        IEthnicBusiness EthnicBusiness;
        IReligionBusiness ReligionBusiness;
        IDistrictBusiness DistrictBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        IPupilOfClassBusiness PupilOfClassBusiness;
        ICommuneBusiness CommuneBusiness;
        IVillageBusiness VillageBusiness;
        IPupilAbsenceBusiness PupilAbsenceBusiness;
        IGoodChildrenTicketBusiness GoodChildrenTicketBusiness;
        IDevelopmentOfChildrenBusiness DevelopmentOfChildrenBusiness;
        IPolicyRegimeBusiness PolicyRegimeBusiness;
        IDisabledTypeBusiness DisabledTypeBusiness;
        private readonly ICodeConfigBusiness CodeConfigBusiness;

        public ImportChildrenController(IPupilProfileBusiness pupilProfileBusiness,
                                        IAcademicYearBusiness academicYearBusiness,
                                        IEducationLevelBusiness educationLevelBusiness,
                                        IClassProfileBusiness classProfileBusiness,
                                        IProvinceBusiness provinceBusiness,
                                        IPolicyTargetBusiness policyTargetBusiness,
                                        IEthnicBusiness ethnicBusiness,
                                        IReligionBusiness religionBusiness,
                                        IDistrictBusiness districtBusiness,
                                        IReportDefinitionBusiness reportDefinitionBusiness,
                                        IPupilOfClassBusiness pupilOfClassBusiness,
                                        ICodeConfigBusiness CodeConfigBusiness,
                                        ICommuneBusiness communeBusiness,
                                        IVillageBusiness villageBusiness,
            IDisabledTypeBusiness DisabledTypeBusiness,
                IPolicyRegimeBusiness PolicyRegimeBusiness,
            IGoodChildrenTicketBusiness GoodChildrenTicketBusiness,
            IPupilAbsenceBusiness PupilAbsenceBusiness,
            IDevelopmentOfChildrenBusiness DevelopmentOfChildrenBusiness
            )
        {
            this.CodeConfigBusiness = CodeConfigBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.ProvinceBusiness = provinceBusiness;
            this.PolicyTargetBusiness = policyTargetBusiness;
            this.EthnicBusiness = ethnicBusiness;
            this.ReligionBusiness = religionBusiness;
            this.DistrictBusiness = districtBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.CommuneBusiness = communeBusiness;
            this.VillageBusiness = villageBusiness;
            this.DevelopmentOfChildrenBusiness = DevelopmentOfChildrenBusiness;
            this.PupilAbsenceBusiness = PupilAbsenceBusiness;
            this.DevelopmentOfChildrenBusiness = DevelopmentOfChildrenBusiness;
            this.GoodChildrenTicketBusiness = GoodChildrenTicketBusiness;
            this.PolicyRegimeBusiness = PolicyRegimeBusiness;
            this.DisabledTypeBusiness = DisabledTypeBusiness;
        }

        #region Constants
        private const string SEMICOLON_SPACE = "; ";
        #endregion
        public ActionResult Index()
        {
            GlobalInfo glo = new GlobalInfo();
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.All.Where(x => x.Grade == glo.AppliedLevel).ToList();
            EducationLevel objAll = new EducationLevel();
            objAll.EducationLevelID = 0;
            objAll.Resolution = "[Tất cả]";
            lstEducationLevel.Add(objAll);
            ViewData["checkFirst"] = 1;
            ViewData[ImportChildrenConstants.LIST_EDUCATION_LEVEL] = new SelectList(lstEducationLevel.OrderBy(x => x.EducationLevelID), "EducationLevelID", "Resolution");
            //ViewData[ImportChildrenConstants.LIST_EDUCATION_LEVEL] = new SelectList(glo.EducationLevels, "EducationLevelID", "Resolution");
            if (!new GlobalInfo().IsCurrentYear)
            {
                ViewData[ImportChildrenConstants.DISABLE_BTNSAVE] = true;
            }
            else
            {
                ViewData[ImportChildrenConstants.DISABLE_BTNSAVE] = false;
            }

            return View();
        }

        #region Export
        /// <summary>
        /// xuat file excel mau
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>

        public FileResult Export(int EducationLevelID)
        {
            GlobalInfo glo = new GlobalInfo();

            EducationLevel edu = EducationLevelBusiness.Find(EducationLevelID);
            //if (edu == null) return null;

            int year = AcademicYearBusiness.Find(glo.AcademicYearID).Year;

            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.MAU_IMPORT_HO_SO_TRE_MOI);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            string reportName = ReportUtils.RemoveSpecialCharacters(reportDefinition.OutputNamePattern) + "." + reportDefinition.OutputFormat;

            IVTWorkbook book = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = book.GetSheet(1);
            IVTWorksheet refSheet = book.GetSheet(2); // Tỉnh thành
            IVTWorksheet refSheetDis = book.GetSheet(3); // Quận Huyện
            IVTWorksheet refSheetComm = book.GetSheet(4); // Xã phường

            IVTWorksheet refTemp = book.GetSheet(5);
            IVTWorksheet refHDC = book.GetSheet(6);

            IVTWorksheet sheetProvince = book.CopySheetToLast(refSheet, "C3");
            IVTWorksheet sheetDistrict = book.CopySheetToLast(refSheetDis, "D3");
            IVTWorksheet sheetCommune = book.CopySheetToLast(refSheetComm, "E3");

            IVTRange rangeProvince = refSheet.GetRange("A4", "C4");
            IVTRange rangeDistrict = refSheetDis.GetRange("A4", "D4");
            IVTRange rangeProOfCommune = refSheetComm.GetRange("A4", "E4");
            IVTRange rangeCommune = refSheetComm.GetRange("A5", "E5");

            IVTWorksheet sheetHDC = book.CopySheetToLast(refHDC, "A20");

            #region fill data for sheet
            Dictionary<string, object> dicSheet = new Dictionary<string, object>();
            dicSheet["SuperVisingDeptName"] = glo.SuperVisingDeptName;
            dicSheet["SchoolName"] = glo.SchoolName.ToUpper();
            dicSheet["DateTime"] = string.Format(Res.Get("Common_Label_DayMonYear"), DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            if (edu != null)
            {
                dicSheet["EducationLevel"] = edu.Resolution;
            }
            sheet.FillVariableValue(dicSheet);
            #endregion

            #region fill data for combobox values
            List<int> lstClassID = getClassFromEducationLevel(EducationLevelID).Select(x => x.ClassProfileID).ToList();
            Dictionary<string, object> dicRef = new Dictionary<string, object>();
            // Khối - Lớp
            List<object> listClass = new List<object>();
            Dictionary<string, object> dicClass = new Dictionary<string, object> { 
                {"AcademicYearID", glo.AcademicYearID.Value},
                {"IsActive", true},
                {"AppliedLevel", glo.AppliedLevel}
            };
            if (EducationLevelID != 0)
            {
                dicClass["EducationLevelID"] = EducationLevelID;
            }
            ClassProfileBusiness.SearchBySchool(glo.SchoolID.Value,
                                                dicClass).Where(x => lstClassID.Contains(x.ClassProfileID)).ToList().ForEach(u => listClass.Add(new { u.DisplayName }));
            dicRef["Classes"] = listClass;

            // Hình thức trúng tuyển
            List<object> listEnrolmentType = new List<object>();
            CommonList.EnrolmentTypeAvailable().ForEach(u => listEnrolmentType.Add(new { Value = u.value }));
            dicRef["EnrolmentTypes"] = listEnrolmentType;


            // Sheet Tỉnh/Thành
            List<Province> lstPro = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.ProvinceName).ToList();
            List<object> listProvince_Temp = new List<object>();
            int i = 1;
            int startRow = 4;
            foreach (var pr in lstPro)
            {
                sheetProvince.CopyPasteSameSize(rangeProvince, startRow, 1);
                listProvince_Temp.Add(new Dictionary<string, object> 
                    {
                        {"Order", i++}, 
                        {"ProvinceCode", pr.ProvinceCode},
                        {"ProvinceName", pr.ProvinceName},
                       
                    });
                startRow++;
            }
            // Fill Tỉnh/Thành
            Dictionary<string, object> dicSheetPro = new Dictionary<string, object>();
            dicSheetPro.Add("list", listProvince_Temp);
            sheetProvince.FillVariableValue(dicSheetPro);


            // Sheet Quận/Huyện           
            var lstDis = (from p in DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } })
                          join q in ProvinceBusiness.All on p.ProvinceID equals q.ProvinceID
                          select new
                          {
                              DistrictID = p.DistrictID,
                              DistrictName = p.DistrictName,
                              DistrictCode = p.DistrictCode,
                              ProvinceID = q.ProvinceID,
                              ProvinceName = q.ProvinceName
                          }).ToList();
            // Fill Quận/Huyện
            int startRow_Dis = 4;
            int order = 1;
            int beginRow_Dis = 4;
            foreach (var pr in lstPro)
            {
                int rowFirstByDis = 1;
                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count();
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        sheetDistrict.CopyPasteSameSize(rangeDistrict, startRow_Dis, 1);
                        sheetDistrict.SetCellValue(startRow_Dis, 1, order++);
                        if (rowFirstByDis == 1)
                        {
                            sheetDistrict.SetCellValue(startRow_Dis, 2, dis.ProvinceName);
                        }
                        sheetDistrict.SetCellValue(startRow_Dis, 3, dis.DistrictCode);
                        sheetDistrict.SetCellValue(startRow_Dis, 4, dis.DistrictName);
                        if (rowFirstByDis == numberOfDis)
                        {
                            IVTRange range = sheetDistrict.GetRange(beginRow_Dis, 2, beginRow_Dis + numberOfDis - 1, 2);
                            range.Merge();
                        }
                        startRow_Dis++;
                        rowFirstByDis++;
                    }
                    beginRow_Dis += numberOfDis;
                }
            }


            // Sheet Xã/Phường
            var lstCommune = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            // Fill Xã/Phường
            int numProvince = 1;
            int startRow_Pro = 4;

            int startRow_Com = 5;
            order = 1;

            int beginRow_Com = 5;
            foreach (var pr in lstPro)
            {
                // Thông tin tỉnh
                sheetCommune.CopyPasteSameSize(rangeProOfCommune, startRow_Pro, 1);
                sheetCommune.SetCellValue(startRow_Pro, 1, numProvince++);
                sheetCommune.SetCellValue(startRow_Pro, 2, pr.ProvinceName);

                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count;
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        int rowFirstOfDis = 1;
                        var lstCommByDis = lstCommune.Where(o => o.DistrictID == dis.DistrictID).OrderBy(o => o.CommuneName).ToList();
                        int numOfComm = lstCommByDis.Count;
                        if (numOfComm > 0)
                        {
                            foreach (var cm in lstCommByDis)
                            {
                                sheetCommune.CopyPasteSameSize(rangeCommune, startRow_Com, 1);
                                sheetCommune.SetCellValue(startRow_Com, 1, order++);
                                if (rowFirstOfDis == 1)
                                {
                                    sheetCommune.SetCellValue(startRow_Com, 2, dis.DistrictCode);
                                    sheetCommune.SetCellValue(startRow_Com, 3, dis.DistrictName);
                                }
                                sheetCommune.SetCellValue(startRow_Com, 4, cm.CommuneCode);
                                sheetCommune.SetCellValue(startRow_Com, 5, cm.CommuneName);
                                if (rowFirstOfDis == numOfComm)
                                {
                                    sheetCommune.GetRange(beginRow_Com, 2, beginRow_Com + numOfComm - 1, 2).Merge();
                                    sheetCommune.GetRange(beginRow_Com, 3, beginRow_Com + numOfComm - 1, 3).Merge();
                                }
                                startRow_Com++;
                                rowFirstOfDis++;
                            }
                        }
                        beginRow_Com += numOfComm; // dòng tiến hành merge
                    }
                }
                // set lại biến đếm cho tỉnh tiếp theo
                startRow_Pro += order;
                startRow_Com++;
                beginRow_Com++;
                order = 1;
            }


            // Đối tượng chính sách  
            List<object> listPolicyTarget = new List<object>();
            var lstPOT = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lstPOT.ForEach(u => listPolicyTarget.Add(new { u.Resolution }));
            dicRef["PolicyTargets"] = listPolicyTarget;

            // Dân tộc
            List<object> listEthnic = new List<object>();
            List<Ethnic> lst1 = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.EthnicName).ToList();
            Ethnic e1 = lst1.FirstOrDefault(o => o.EthnicName.Contains("Kinh"));
            lst1.Remove(e1);
            lst1.Insert(0, e1);
            lst1.ForEach(u => listEthnic.Add(new { u.EthnicName }));
            //EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList().ForEach(u => listEthnic.Add(new { u.EthnicName }));
            dicRef["Ethnics"] = listEthnic;

            // Chế độ chính sách
            List<object> listPolicyRegimes = new List<object>();
            List<PolicyRegime> lst11 = PolicyRegimeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lst11.ForEach(u => listPolicyRegimes.Add(new { u.Resolution }));
            //EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList().ForEach(u => listEthnic.Add(new { u.EthnicName }));
            dicRef["PolicyRegimes"] = listPolicyRegimes;

            // Tôn giáo
            List<object> listReligionObj = new List<object>();
            List<Religion> lstReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            Religion r1 = lstReligion.FirstOrDefault(o => o.Resolution.Contains("Không"));
            lstReligion.Remove(r1);
            lstReligion.Insert(0, r1);
            lstReligion.ForEach(u => listReligionObj.Add(new { u.Resolution }));
            //ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList().ForEach(u => listReligion.Add(new { u.Resolution }));
            dicRef["Religions"] = listReligionObj;

            //Loại khuyết tật
            List<object> listDisabledTyeObj = new List<object>();
            List<DisabledType> listDisabledTye = DisabledTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            listDisabledTye.ForEach(u => listDisabledTyeObj.Add(new { u.Resolution }));
            //ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList().ForEach(u => listReligion.Add(new { u.Resolution }));
            dicRef["DisabledTypes"] = listDisabledTyeObj;

            List<object> listBloodType = new List<object>();
            CommonList.BloodType().ForEach(u => listBloodType.Add(new { Value = u.value }));
            dicRef["BloodTypes"] = listBloodType;

            List<object> listProfileStatus = new List<object>();
            CommonList.PupilStatus().Where(u => u.key == SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString()).ToList().ForEach(u => listProfileStatus.Add(new { Value = u.value }));
            dicRef["ProfileStatuses"] = listProfileStatus;

            List<object> listGenres = new List<object>();
            CommonList.GenreAndSelect().ForEach(u => listGenres.Add(new { Value = u.value }));
            dicRef["Genres"] = listGenres;



            refTemp.FillVariableValue(dicRef);
            // Set lai ten cac Sheet
            sheetProvince.Name = "TinhThanh";
            refSheet.Delete();

            sheetDistrict.Name = "QuanHuyen";
            refSheetDis.Delete();

            sheetCommune.Name = "XaPhuong";
            refSheetComm.Delete();

            sheetHDC.Name = "HuongDanChung";
            refHDC.Delete();

            #endregion

            //sheet.SetColumnWidth(35, 0);
            FileStreamResult result = new FileStreamResult(book.ToStream(), "application/octet-stream");
            result.FileDownloadName = reportName;
            return result;
        }

        public FileResult DowloadFileErr(int EducationLevelID)
        {
            string reportName = string.Empty;
            Stream excel = this.SetDataToFileExcel(EducationLevelID, ref reportName, true);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = reportName;
            return result;
        }
        #endregion

        #region Import
        [ValidateAntiForgeryToken]

        public JsonResult Import(int EducationLevelID)
        {
            try
            {
                Session[ImportChildrenConstants.MESSAGE_ERROR_IMPORT] = null;
                Session[ImportChildrenConstants.MESSAGE_STATUS_SUCCESS] = null;
                HttpPostedFileBase file = Request.Files.Count > 0 ? Request.Files[0] : null;

                if (file == null)
                {
                    throw new BusinessException(Res.Get("InputExaminationMark_Label_NoFileSelected"));
                }

                if (!file.FileName.EndsWith(".xls"))
                {
                    throw new BusinessException(Res.Get("Common_Label_ExtensionError"));
                }

                if (file.ContentLength > 3072000)
                {
                    throw new BusinessException(Res.Get("InputExaminationMark_Label_MaxSizeExceeded"));
                }

                IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);
                IVTWorksheet sheet = book.GetSheet(1);

                List<ImportChildrenViewModel> lstPupilImported = ReadExcelFile(sheet, EducationLevelID);
                Session[ImportChildrenConstants.LIST_IMPORTED_PUPIL] = lstPupilImported;

                if (lstPupilImported.Count == 0)
                {
                    throw new BusinessException(Res.Get("ImportPupil_Label_NoPupilImported"));
                }

                bool isAllValid = lstPupilImported.All(u => u.IsValid);
                if (isAllValid)
                    SaveData();

                int countAllPupil = lstPupilImported.Count;
                int countPupilValid = lstPupilImported.Count(p => p.IsValid);
                string retMsg;
                if (isAllValid)
                {
                    retMsg = String.Format("{0} {1}/{2} trẻ", Res.Get("Common_Label_ImportSuccessMessage"), countPupilValid, countAllPupil);
                    Session[ImportChildrenConstants.MESSAGE_STATUS_SUCCESS] = ImportChildrenConstants.MESSAGE_STATUS_SUCCESS;
                    throw new BusinessException(retMsg);
                }
                JsonResult resx = Json(new JsonMessage(Res.Get("ImportPupil_Label_ImportFailure"), JsonMessage.ERROR), "text/html");
                return resx;
            }
            catch (Exception ex)
            {
                Session[ImportChildrenConstants.MESSAGE_ERROR_IMPORT] = Res.Get(ex.Message);
                JsonResult res = Json(new JsonMessage(Res.Get(ex.Message), JsonMessage.ERROR), "text/html");
                return res;
            }
        }

        public JsonResult LoadImportedGrid()
        {
            if (Session[ImportChildrenConstants.MESSAGE_ERROR_IMPORT] != null)
            {
                if (Session[ImportChildrenConstants.MESSAGE_STATUS_SUCCESS] != null)
                {
                    return Json(new JsonMessage(Session[ImportChildrenConstants.MESSAGE_ERROR_IMPORT].ToString(), JsonMessage.SUCCESS));
                }
                else
                {
                    return Json(new JsonMessage(Session[ImportChildrenConstants.MESSAGE_ERROR_IMPORT].ToString(), JsonMessage.ERROR));
                }
            }
            return Json(new JsonMessage(RenderPartialViewToString("_gridImported", null), "grid"));
            // return PartialView("_gridImported");
        }

        private List<ImportChildrenViewModel> ReadExcelFile(IVTWorksheet sheet, int educationLevelID)
        {
            Session["EducationLevelID"] = educationLevelID;
            GlobalInfo glo = new GlobalInfo();
            List<ImportChildrenViewModel> listPupil = new List<ImportChildrenViewModel>();
            List<ImportChildrenViewModel> listPupilTemp = new List<ImportChildrenViewModel>();
            // Congnv: Mẫu cũ
            //int rowIndex = 10;
            int rowIndex = 5;
            // Congnv: Thêm các tên cột trong bảng Excel (Hỗ trợ sửa đổi mẫu sau này)
            const string indexCol = "A";
            const string pupilCodeCol = "B";
            const string fullNameCol = "C";
            const string birthDateCol = "D";
            const string genreNameCol = "E";
            const string classNameCol = "F";
            const string enrolmentTypeNameCol = "G";
            //const string birthDateCol = "H";
            const string enrolmentDateCol = "H";
            const string provinceNameCol = "I";
            const string districtNameCol = "J";
            const string communeNameCol = "K";
            const string villageNameCol = "L";
            const string storageNameCol = "M";
            const string policyTargetNameCol = "N";
            const string policyRegimeNameCol = "O";
            const string disabledTypeNameCol = "P";
            const string ethnicNameCol = "Q";
            const string religionNameCol = "R";
            const string birthPlaceCol = "S";
            const string homeTownCol = "T";
            const string permanentResidentalAddressCol = "U";
            const string tempResidentalAddressCol = "V";
            const string mobileCol = "W";
            const string fatherFullNameCol = "X";
            const string fatherBirthDateCol = "Y";
            const string fatherJobCol = "Z";
            const string fatherMobileCol = "AA";
            const string fatherEmailCol = "AB";
            const string motherFullNameCol = "AC";
            const string motherBirthDateStrCol = "AD";
            const string motherJobCol = "AE";
            const string motherMobileCol = "AF";
            const string motherEmailCol = "AG";
            const string bloodTypeNameCol = "AH";
            const string profileStatusNameCol = "AI";
            const string minoriryFather = "AJ";
            const string minoriryMother = "AK";
            const string pupilCodeNewCol = "AL";


            // Danh sách Tỉnh/thành, Quận/Huyện, Xã/Phường
            List<Province> lstProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            List<District> lstDistrict = DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            List<Commune> listCom = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            IQueryable<Village> listVil = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });

            // Lấy các lớp có quyền
            List<int> lstClassID = getClassFromEducationLevel(educationLevelID).Select(x => x.ClassProfileID).ToList();

            Dictionary<string, object> dic1 = new Dictionary<string, object> { 
                { "AcademicYearID", glo.AcademicYearID.Value }, 
                //{ "EducationLevelID", educationLevelID }, 
                { "IsActive", true } 
            };
            if (educationLevelID != 0)
            {
                dic1["EducationLevelID"] = educationLevelID;
            }
            var listClass = ClassProfileBusiness.Search(dic1).Where(x => lstClassID.Contains(x.ClassProfileID)).Select(u => new { u.ClassProfileID, u.DisplayName }).ToList().Select(u => new ComboObject(u.ClassProfileID.ToString(), u.DisplayName.ToLower())).ToList();

            var listEthnic = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.EthnicID, u.EthnicName }).ToList().Select(u => new ComboObject(u.EthnicID.ToString(), u.EthnicName.ToLower())).ToList();
            var listPolicyRegime = PolicyRegimeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.PolicyRegimeID, u.Resolution }).ToList().Select(u => new ComboObject(u.PolicyRegimeID.ToString(), u.Resolution.ToLower())).ToList();
            var listDisabledType = DisabledTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.DisabledTypeID, u.Resolution }).ToList().Select(u => new ComboObject(u.DisabledTypeID.ToString(), u.Resolution.ToLower())).ToList();
            var listPolicyTarget = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.PolicyTargetID, u.Resolution }).ToList().Select(u => new ComboObject(u.PolicyTargetID.ToString(), u.Resolution.ToLower())).ToList();
            List<string> lstPolicyTargetNameTemp = listPolicyTarget.Select(o => o.value).ToList();
            var listReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.ReligionID, u.Resolution }).ToList().Select(u => new ComboObject(u.ReligionID.ToString(), u.Resolution.ToLower())).ToList();
            int tempCount = 0;
            bool isAutoGenCode = CodeConfigBusiness.IsAutoGenCode(glo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            bool isSchoolModify = CodeConfigBusiness.IsSchoolModify(glo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            string originalPupilCode = string.Empty;
            int maxOrderNumber = 0;
            int numberLength = 0;

            var lstEducationLevel = _globalInfo.EducationLevels.Select(x => x.EducationLevelID).ToList();
            string educationName = "";
            if (educationLevelID > 0)
            {
                educationName = (_globalInfo.EducationLevels.Count > 0 && educationLevelID != 0) ? _globalInfo.EducationLevels.Where(x => x.EducationLevelID == educationLevelID).FirstOrDefault().Resolution : "";
                lstEducationLevel = lstEducationLevel.Where(x => x == educationLevelID).ToList();
            }

            var lstEducationLevelWithClass = ClassProfileBusiness.All.Where(x => lstEducationLevel.Contains(x.EducationLevelID))
                                            .Select(x => new { EducationLevelID = x.EducationLevelID, ClassName = x.DisplayName }).ToList();

            List<AutoGenericCode> lstOutGenericCode = new List<AutoGenericCode>();
            if (isAutoGenCode)
            {
                //CodeConfigBusiness.GetListPupilCodeForImport(_globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, educationLevelID, _globalInfo.SchoolID.Value, out lstResult);               
                AutoGenericCode objAutoGen = null;
                foreach (var item in lstEducationLevel)
                {
                    CodeConfigBusiness.GetPupilCodeForImport(_globalInfo.AcademicYearID.Value, item, _globalInfo.SchoolID.Value, out originalPupilCode, out maxOrderNumber, out numberLength);

                    objAutoGen = new AutoGenericCode();
                    objAutoGen.Index = 0;
                    objAutoGen.OriginalPupilCode = originalPupilCode;
                    objAutoGen.MaxOrderNumber = maxOrderNumber;
                    objAutoGen.NumberLength = numberLength;
                    objAutoGen.EducationLevelID = item;
                    lstOutGenericCode.Add(objAutoGen);
                }
            }
            var listCheckIndex = new List<string>();
            IQueryable<Village> lstVillage = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });
            //Tìm kiếm học sinh trong năm học
            List<PupilProfileBO> lstPP = PupilProfileBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object>() { { "IsActive", true }, { "CurrentAcademicYearID", glo.AcademicYearID.Value } }).Where(x => x.IsActive).ToList();
            List<PupilOfClass> lstpoc = PupilOfClassBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object>() { { "AcademicYearID", glo.AcademicYearID.Value } }).ToList();

            // AnhVD9 - 20140829 - Biến lưu dữ liệu tận dụng để giảm thời gian import
            // Tỉnh thành trước đó
            string ProviceNameBefore = string.Empty;
            int ProvinceIdBefore = 0;
            // Quận/Huyện trước đó
            List<District> lstDistrictInProvince = new List<District>();
            string DistrictNameBefore = string.Empty;
            int DistrictIdBefore = 0;

            // AnhVD9 - 20140919 Hotfix -  Thông tin Mã HS không trùng với các năm học khác
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AppliedLevel", glo.AppliedLevel);
            dic.Add("SchoolID", glo.SchoolID);
            dic.Add("AcademicYearID", glo.AcademicYearID); // Năm hiện tại
            // Danh sách ClassId trong năm hiện tại
            List<int> LstClassIDInCurrentYear = ClassProfileBusiness.Search(dic).Select(o => o.ClassProfileID).ToList();
            // Danh sách lớp trong các năm khác - chỉ xét cùng cấp
            List<int> LstClassIDNotInCurrentYear = this.ClassProfileBusiness.All.Where(o => o.SchoolID == glo.SchoolID && o.EducationLevel.Grade == glo.AppliedLevel
                                                            && !LstClassIDInCurrentYear.Contains(o.ClassProfileID) && o.IsActive.Value).Select(o => o.ClassProfileID).ToList();
            // Lấy danh sách HS trong các năm khác trong cùng cấp
            List<string> lstPupilCodeOtherYear = this.PupilOfClassBusiness.All.Where(o => o.SchoolID == glo.SchoolID && LstClassIDNotInCurrentYear.Contains(o.ClassID)).Select(o => o.PupilProfile.PupilCode).ToList();
            List<string> lstPupilCodeCurrentYear = this.PupilOfClassBusiness.All.Where(o => o.SchoolID == glo.SchoolID && LstClassIDInCurrentYear.Contains(o.ClassID)).Select(o => o.PupilProfile.PupilCode).ToList();
            // End Hotfix
            StringBuilder ErrorMessage = new StringBuilder();
            while (!string.IsNullOrEmpty(GetCellString(sheet, indexCol + rowIndex)) || !string.IsNullOrEmpty(GetCellString(sheet, fullNameCol + rowIndex)) || !string.IsNullOrEmpty(GetCellString(sheet, classNameCol + rowIndex)))
            {
                var pupil = new ImportChildrenViewModel();
                pupil.checkInsertVilliage = false;
                pupil.IsValid = true;
                pupil.BirthDateStr = GetCellString(sheet, birthDateCol + rowIndex);
                pupil.EnrolmentDateStr = GetCellString(sheet, enrolmentDateCol + rowIndex);
                string fatherDate = GetCellString(sheet, fatherBirthDateCol + rowIndex);
                string motherDate = GetCellString(sheet, motherBirthDateStrCol + rowIndex);
                string fatherDateTemp = "";
                string motherDateTemp = "";
                int result = 0;
                if (fatherDate != null && fatherDate.Trim() != "" && int.TryParse(fatherDate, out result) && fatherDate.Trim().Length == 4)
                {
                    fatherDateTemp = "01/01/" + fatherDate;
                }

                if (motherDate != null && motherDate.Trim() != "" && int.TryParse(motherDate, out result) && motherDate.Trim().Length == 4)
                {
                    motherDateTemp = "01/01/" + motherDate;
                }

                pupil.FatherBirthDateStr = fatherDate;
                pupil.MotherBirthDateStr = motherDate;

                DateTime? birthDate = GetCellDate(sheet, birthDateCol + rowIndex);
                DateTime? enrolmentDate = GetCellDate(sheet, enrolmentDateCol + rowIndex);
                DateTime? fatherBirthDate = new DateTime?();
                DateTime? motherBirthDate = new DateTime?();
                if (fatherDateTemp != null && fatherDateTemp.Trim() != "")
                {
                    fatherBirthDate = DateTime.Parse(fatherDateTemp);
                }
                if (motherDateTemp != null && motherDateTemp.Trim() != "")
                {
                    motherBirthDate = DateTime.Parse(motherDateTemp);
                }
                string className = GetCellString(sheet, classNameCol + rowIndex);

                pupil.IndexStr = GetCellString(sheet, indexCol + rowIndex);
                int? index = GetCellIndex(pupil.IndexStr);
                var classID = SetFromList(listClass, className);
                if (classID == 0)
                {
                    ErrorMessage.Append("Thầy/cô không có quyền với lớp " + className);
                    pupil.IsValid = false;
                    pupil.ErrorMessage += ErrorMessage.ToString();
                    listPupil.Add(pupil);
                    rowIndex++;
                    tempCount++;
                    continue;
                }

                // Reset data error message
                ErrorMessage = new StringBuilder();
                if (index.HasValue) pupil.Index = index.Value;
                else
                {
                    pupil.Index = 0;
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_STTNotNumber")).Append(SEMICOLON_SPACE);
                }

                pupil.BirthDate = birthDate;
                pupil.BirthPlace = GetCellString(sheet, birthPlaceCol + rowIndex);
                pupil.BloodTypeName = GetCellString(sheet, bloodTypeNameCol + rowIndex);
                pupil.ClassName = className;
                pupil.EnrolmentDate = enrolmentDate;
                pupil.EnrolmentTypeName = GetCellString(sheet, enrolmentTypeNameCol + rowIndex);
                pupil.EthnicName = GetCellString(sheet, ethnicNameCol + rowIndex);
                pupil.FatherBirthDate = fatherBirthDate;
                pupil.FatherFullName = GetCellString(sheet, fatherFullNameCol + rowIndex);
                pupil.FatherJob = GetCellString(sheet, fatherJobCol + rowIndex);
                pupil.FatherMobile = GetCellString(sheet, fatherMobileCol + rowIndex);
                pupil.FullName = GetCellString(sheet, fullNameCol + rowIndex);
                pupil.Name = GetPupilName(pupil.FullName);
                pupil.GenreName = GetCellString(sheet, genreNameCol + rowIndex);
                pupil.HomeTown = GetCellString(sheet, homeTownCol + rowIndex);
                pupil.Mobile = GetCellString(sheet, mobileCol + rowIndex);
                pupil.MotherBirthDate = motherBirthDate;
                pupil.MotherFullName = GetCellString(sheet, motherFullNameCol + rowIndex);
                pupil.MotherJob = GetCellString(sheet, motherJobCol + rowIndex);
                pupil.MotherMobile = GetCellString(sheet, motherMobileCol + rowIndex);
                pupil.MotherEmail = GetCellString(sheet, motherEmailCol + rowIndex);
                pupil.FatherEmail = GetCellString(sheet, fatherEmailCol + rowIndex);
                pupil.StorageNumber = GetCellString(sheet, storageNameCol + rowIndex);
                pupil.PermanentResidentalAddress = GetCellString(sheet, permanentResidentalAddressCol + rowIndex);
                pupil.PolicyTargetName = GetCellString(sheet, policyTargetNameCol + rowIndex);
                pupil.PupilCode = GetCellString(sheet, pupilCodeCol + rowIndex);
                pupil.PupilCodeNew = GetCellString(sheet, pupilCodeNewCol + rowIndex);
                pupil.ProfileStatusName = GetCellString(sheet, profileStatusNameCol + rowIndex);
                pupil.PolicyRegimeName = GetCellString(sheet, policyRegimeNameCol + rowIndex);
                pupil.DisabledTypeName = GetCellString(sheet, disabledTypeNameCol + rowIndex);
                pupil.ReligionName = GetCellString(sheet, religionNameCol + rowIndex);
                pupil.CurrentSchoolID = glo.SchoolID.Value;
                pupil.TempResidentalAddress = GetCellString(sheet, tempResidentalAddressCol + rowIndex);
                pupil.BloodType = SetBloodType(pupil.BloodTypeName);
                pupil.CurrentClassID = classID;
                pupil.EnrolmentType = SetEnrolementType(pupil.EnrolmentTypeName);
                pupil.MinorityFatherName = GetCellString(sheet, minoriryFather + rowIndex);
                pupil.MinorityMotherName = GetCellString(sheet, minoriryMother + rowIndex);
                pupil.Genre = SetGenreNew(pupil.GenreName);
                pupil.PolicyTargetID = (int?)SetNulableFromList(listPolicyTarget, pupil.PolicyTargetName);
                pupil.ProfileStatus = SetProfileStatus(pupil.ProfileStatusName);

                pupil.EthnicID = (int?)SetNulableFromList(listEthnic, pupil.EthnicName);
                pupil.ReligionID = (int?)SetNulableFromList(listReligion, pupil.ReligionName);
                pupil.PolicyRegimeID = (pupil.PolicyTargetID != -1 && pupil.PolicyTargetID != null) ? (int?)SetNulableFromList(listPolicyRegime, pupil.PolicyRegimeName) : null;
                pupil.DisabledTypeID = (int?)SetNulableFromList(listDisabledType, pupil.DisabledTypeName);
                pupil.IsDisabled = (pupil.DisabledTypeID != -1 && pupil.DisabledTypeID != null) ? 1 : 0;
                #region AnhVD9 20140918 - Kiểm tra không cho Import trùng mã trong năm học trước
                if (!string.IsNullOrWhiteSpace(pupil.PupilCode))
                {
                    if (lstPupilCodeOtherYear.Contains(pupil.PupilCode))
                    {
                        if (!lstPupilCodeCurrentYear.Contains(pupil.PupilCode)) // năm học hiện tại chưa có mã này
                        {
                            ErrorMessage.Append(Res.Get("ImportPupill_PupilCode_InOtherYear")).Append(SEMICOLON_SPACE);
                        }
                    }
                }
                #endregion

                // Tỉnh/thành, Quận/Huyện, Xã/Phường, Thôm/Xóm
                #region Xử lý Tỉnh/thành, Quận/Huyện, Xã/Phường, Thôm/Xóm
                pupil.ProvinceName = GetCellString(sheet, provinceNameCol + rowIndex);
                pupil.DistrictName = GetCellString(sheet, districtNameCol + rowIndex);
                pupil.CommuneName = GetCellString(sheet, communeNameCol + rowIndex);
                pupil.VillageName = GetCellString(sheet, villageNameCol + rowIndex);

                //  Cho phép nhập Tên hoặc Mã Tỉnh thành
                if (!string.IsNullOrEmpty(pupil.ProvinceName))
                {
                    if (pupil.ProvinceName.Equals(ProviceNameBefore))
                    {
                        pupil.ProvinceID = ProvinceIdBefore;
                    }
                    else if (lstProvince.Exists(o => o.ProvinceName.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())))
                    {
                        Province provinceObj = lstProvince.Where(o => o.ProvinceName.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())).FirstOrDefault();
                        if (provinceObj != null)
                        {
                            pupil.ProvinceID = provinceObj.ProvinceID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            lstDistrictInProvince = lstDistrict.Where(o => o.ProvinceID == pupil.ProvinceID).ToList();
                            ProviceNameBefore = pupil.ProvinceName;
                            ProvinceIdBefore = provinceObj.ProvinceID;
                        }
                    }
                    else if (lstProvince.Exists(o => o.ProvinceCode.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())))
                    {
                        Province provinceObj = lstProvince.Where(o => o.ProvinceCode.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())).FirstOrDefault();
                        if (provinceObj != null)
                        {
                            pupil.ProvinceID = provinceObj.ProvinceID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            lstDistrictInProvince = lstDistrict.Where(o => o.ProvinceID == pupil.ProvinceID).ToList();
                            ProviceNameBefore = pupil.ProvinceName;
                            ProvinceIdBefore = provinceObj.ProvinceID;
                        }
                    }
                }

                // Quận/Huyện
                if (!string.IsNullOrEmpty(pupil.DistrictName))
                {
                    if (pupil.DistrictName.Equals(DistrictNameBefore))
                    {
                        pupil.DistrictID = DistrictIdBefore;
                    }
                    else if (lstDistrictInProvince.Exists(o => o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower()) || o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower())))
                    {
                        District obj = lstDistrictInProvince.Where(o => o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower()) || o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower())).FirstOrDefault();
                        if (obj != null)
                        {
                            pupil.DistrictID = obj.DistrictID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            DistrictNameBefore = pupil.DistrictName;
                            DistrictIdBefore = obj.DistrictID;
                        }

                    }
                    else if (lstDistrict.Exists(o => o.ProvinceID == pupil.ProvinceID && (o.DistrictName != null && o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower())) || (o.DistrictCode != null && o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower()))))
                    {
                        District obj = lstDistrict.Where(o => o.ProvinceID == pupil.ProvinceID && (o.DistrictName != null && o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower())) || (o.DistrictCode != null && o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower())))
                                        .FirstOrDefault();
                        if (obj != null)
                        {
                            pupil.DistrictID = obj.DistrictID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            lstDistrictInProvince = lstDistrict.Where(o => o.DistrictID == pupil.DistrictID).ToList();
                            DistrictNameBefore = pupil.DistrictName;
                            DistrictIdBefore = pupil.DistrictID.Value;
                        }
                    }
                }

                // Xã/Phường
                if (!string.IsNullOrEmpty(pupil.CommuneName))
                {
                    if (listCom.Exists(o => o.DistrictID == pupil.DistrictID && (o.CommuneName != null && o.CommuneName.ToLower().Equals(pupil.CommuneName.ToLower())) || (o.CommuneCode != null && o.CommuneCode.ToLower().Equals(pupil.CommuneName.ToLower()))))
                    {
                        Commune comObj = listCom.Where(o => o.DistrictID == pupil.DistrictID && (o.CommuneName != null && o.CommuneName.ToLower().Equals(pupil.CommuneName.ToLower())) || (o.CommuneCode != null && o.CommuneCode.ToLower().Equals(pupil.CommuneName.ToLower())))
                                        .FirstOrDefault();
                        if (comObj != null)
                        {
                            pupil.CommuneID = comObj.CommuneID;
                        }
                    }
                }

                // Thôn/Xóm
                if (!string.IsNullOrEmpty(pupil.VillageName) && pupil.CommuneID != null && pupil.CommuneID != 0)
                {
                    if (listVil.Any(o => o.CommuneID == pupil.CommuneID && ((o.VillageName != null && o.VillageName.ToLower().Equals(pupil.VillageName.ToLower())) || (o.VillageCode != null && o.VillageCode.ToLower().Equals(pupil.VillageName.ToLower())))))
                    {
                        Village villageObj = listVil.Where(o => o.CommuneID == pupil.CommuneID && ((o.VillageName != null && o.VillageName.ToLower().Equals(pupil.VillageName.ToLower())) || (o.VillageCode != null && o.VillageCode.ToLower().Equals(pupil.VillageName.ToLower())))).FirstOrDefault();
                        if (villageObj != null)
                        {
                            pupil.VillageID = villageObj.VillageID;
                        }
                    }
                }
                #endregion End AnhVD20140825
                string pupilCode = originalPupilCode + ToStringWithFixedLength(maxOrderNumber + 1 + tempCount, numberLength);
                #region // isAutoGenCode
                AutoGenericCode objAutoGenericCode = new AutoGenericCode();
                var tempt = lstEducationLevelWithClass.Where(x => x.ClassName.ToUpper().Equals(className.ToUpper())).FirstOrDefault();
                if (tempt == null && educationLevelID != 0)
                {
                    ErrorMessage.Append(className + " không thuộc khối đã chọn");
                    pupil.IsValid = false;
                    pupil.ErrorMessage += ErrorMessage.ToString();
                    listPupil.Add(pupil);
                    rowIndex++;
                    tempCount++;
                    continue;
                }
                else if (tempt != null)
                {
                    if (isAutoGenCode)
                    {
                        objAutoGenericCode = lstOutGenericCode.Where(x => x.EducationLevelID == tempt.EducationLevelID).FirstOrDefault();
                        originalPupilCode = objAutoGenericCode.OriginalPupilCode;
                        maxOrderNumber = objAutoGenericCode.MaxOrderNumber;
                        numberLength = objAutoGenericCode.NumberLength;
                        tempCount = objAutoGenericCode.Index;
                        pupilCode = originalPupilCode + ToStringWithFixedLength(maxOrderNumber + 1 + tempCount, numberLength);

                        if (isSchoolModify)
                        {
                            if (string.IsNullOrEmpty(pupil.PupilCode))
                            {
                                pupil.PupilCode = pupilCode;
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(pupil.PupilCode))
                            {
                                pupil.PupilCode = pupilCode;
                            }
                            else
                            {
                                int temp = 0;
                                int.TryParse(pupil.PupilCode.Substring((pupil.PupilCode.Length - numberLength), numberLength), out temp);
                                if (!pupil.PupilCode.StartsWith(originalPupilCode) || pupil.PupilCode.Length != pupilCode.Length || temp == 0)
                                {
                                    ErrorMessage.Append(Res.Get("ImportPupil_Label_OriginalPupilCode")).Append(SEMICOLON_SPACE);
                                }
                            }
                        }
                    }
                }


                #endregion


                #region true
                if (lstPP.Any(o => o.PupilCode == pupil.PupilCode))
                {
                    PupilProfileBO pupilProfile = lstPP.Where(o => o.PupilCode == pupil.PupilCode).OrderBy(o => o.AssignedDate).LastOrDefault();
                    List<PupilOfClass> lstpoc_temp = lstpoc.Where(o => o.PupilID == pupilProfile.PupilProfileID).ToList();
                    if (lstpoc_temp.Count() >= 2)
                    {
                        PupilOfClass pocAssignMax = lstpoc_temp.OrderBy(o => o.AssignedDate).LastOrDefault();
                        if (pupil.EnrolmentDate >= pocAssignMax.AssignedDate)
                        {
                            ErrorMessage.Append(Res.Get("PupilProfile_Validate_AssignDate")).Append(SEMICOLON_SPACE);
                        }
                    }
                    if (!string.IsNullOrEmpty(pupil.PupilCodeNew))
                    {
                        if (lstPP.Any(o => o.PupilCode == pupil.PupilCodeNew))
                        {
                            ErrorMessage.Append(string.Format("Mã trẻ {0} đã tồn tại.", pupil.PupilCodeNew)).Append(SEMICOLON_SPACE);
                        }
                    }
                    if (pupilProfile.CurrentClassID != classID)
                    {
                        dic = new Dictionary<string, object>();
                        dic["AcademicYearID"] = glo.AcademicYearID.Value;
                        dic["PupilID"] = pupilProfile.PupilProfileID;
                        dic["ClassID"] = pupilProfile.CurrentClassID;

                        bool PupilAbsenseConstraint = PupilAbsenceBusiness.SearchBySchool(glo.SchoolID.Value, dic).Any();
                        if (PupilAbsenseConstraint)
                        {
                            ErrorMessage.Append(Res.Get("ImportPupilChildren_Label_PupilAbsenceExist")).Append(SEMICOLON_SPACE);
                        }
                        bool DevelopmentOfChildrenConstraint = DevelopmentOfChildrenBusiness.SearchBySchool(glo.SchoolID.Value, dic).Any();
                        if (DevelopmentOfChildrenConstraint)
                        {
                            ErrorMessage.Append(Res.Get("ImportPupilChildren_Label_DevelopmentOfChildren_Exist")).Append(SEMICOLON_SPACE);
                        }
                        bool GoodTicketChildrenConstraint = GoodChildrenTicketBusiness.SearchBySchool(glo.SchoolID.Value, dic).Any();
                        if (GoodTicketChildrenConstraint)
                        {
                            ErrorMessage.Append(Res.Get("ImportPupilChildren_Label_GoodTicketChildren_Exist")).Append(SEMICOLON_SPACE);
                        }
                    }
                }
                else
                {
                    ErrorMessage.Append(Res.Get("Trẻ không tồn tại để cập nhật thông tin.")).Append(SEMICOLON_SPACE);
                    pupil.IsValid = false;
                    pupil.ErrorMessage += ErrorMessage.ToString();
                    listPupil.Add(pupil);
                    rowIndex++;
                    tempCount++;
                    continue;
                }
                #endregion

                ValidateImportedPupil(pupil);

                //check Email
                if (!string.IsNullOrEmpty(pupil.MotherEmail))
                {
                    string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Match match = Regex.Match(pupil.MotherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                    if (!match.Success)
                        ErrorMessage.Append(string.Format(Res.Get("Common_Validate_EmailErr"), "mẹ")).Append(SEMICOLON_SPACE);
                    else if (pupil.MotherEmail.Length > 50)
                    {
                        ErrorMessage.Append(string.Format(Res.Get("Common_Label_MaxlengthEmail"), "mẹ")).Append(SEMICOLON_SPACE);
                    }
                }
                //check Email Father
                if (!string.IsNullOrEmpty(pupil.FatherEmail))
                {
                    string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Match match = Regex.Match(pupil.FatherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                    if (!match.Success)
                        ErrorMessage.Append(string.Format(Res.Get("Common_Validate_EmailErr"), "cha")).Append(SEMICOLON_SPACE);
                    else if (pupil.FatherEmail.Length > 50)
                    {
                        ErrorMessage.Append(string.Format(Res.Get("Common_Label_MaxlengthEmail"), "cha")).Append(SEMICOLON_SPACE);
                    }
                }

                if (pupil.ReligionID == -1 && pupil.ReligionName != null && pupil.ReligionName.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_ReligionExisted")).Append(SEMICOLON_SPACE);
                }

                if (pupil.EthnicID == -1 && pupil.EthnicName != null && pupil.EthnicName.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_EthnicExisted")).Append(SEMICOLON_SPACE);
                }

                if (listPupil.Any(u => u.PupilCode.ToLower().Equals(pupil.PupilCode.ToLower())))
                {
                    ErrorMessage.Append(string.Format(Res.Get("ImportChildren_Label_PupilCodeExisted"), pupil.PupilCode)).Append(SEMICOLON_SPACE);
                }

                if (pupil.Index > 0 && pupil.CurrentClassID > 0 && listPupil.Any(u => u.Index == pupil.Index && u.CurrentClassID == pupil.CurrentClassID))
                {
                    ErrorMessage.Append(string.Format(Res.Get("ImportPupil_Label_IndexClassExisted"), pupil.ClassName, pupil.Index)).Append(SEMICOLON_SPACE);
                }

                // day Lớp 4-5 Ghép 01 không thuộc khối đã chọn;
                if (educationLevelID != 0 && !string.IsNullOrEmpty(pupil.ClassName) && !listClass.Any(u => u.key.Equals(pupil.CurrentClassID.ToString())))
                {
                    ErrorMessage.Append(string.Format(Res.Get("ImportPupil_Label_ClassNotExist"), pupil.ClassName)).Append(SEMICOLON_SPACE);
                }

                if (pupil.PolicyTargetID.HasValue && !listPolicyTarget.Any(o => pupil.PolicyTargetID != null && o.key == pupil.PolicyTargetID.Value.ToString()))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidPolicyTargetName")).Append(SEMICOLON_SPACE);
                }

                if (string.IsNullOrEmpty(pupil.ClassName))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidClassNameNull")).Append(SEMICOLON_SPACE);
                }
                if (string.IsNullOrEmpty(pupil.ProvinceName))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidProvinceNameNull")).Append(SEMICOLON_SPACE);
                }
                if (pupil.ProvinceName != null && pupil.ProvinceName.Trim() != "" && pupil.ProvinceID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidProvinceName")).Append(SEMICOLON_SPACE);
                }

                if (pupil.DistrictName != null && pupil.DistrictName.Trim() != "" && pupil.DistrictID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidDistrictName")).Append(SEMICOLON_SPACE);
                }
                bool checkDistrict = checkCompareDistrict(pupil.ProvinceID, pupil.DistrictID);
                bool checkCommune = checkCompareCommune(pupil.DistrictID, pupil.CommuneID);
                //Nhập tỉnh thành trước khi nhập quận huyện
                if (pupil.DistrictID != null && pupil.ProvinceID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidDistrictProvince")).Append(SEMICOLON_SPACE);
                }
                if (pupil.CommuneID != null && (pupil.DistrictID == null || pupil.ProvinceID == null))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidCommuneDistrict")).Append(SEMICOLON_SPACE);
                }
                if (!checkDistrict)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidDistrict")).Append(SEMICOLON_SPACE);
                }
                if (pupil.CommuneName != null && pupil.CommuneName.Trim() != "" && pupil.CommuneID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidCommuneName")).Append(SEMICOLON_SPACE);
                }
                if (!string.IsNullOrEmpty(pupil.StorageNumber) && pupil.StorageNumber.Length > 30)
                {
                    ErrorMessage.Append(Res.Get("Common_Label_Maxlength_StorageNumber")).Append(SEMICOLON_SPACE);
                }

                if (!checkCommune)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidCommune")).Append(SEMICOLON_SPACE);
                }

                if (!checkCompareVillage(pupil.VillageID, pupil.CommuneID))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidVilliageName")).Append(SEMICOLON_SPACE);
                }

                if (pupil.CommuneID == null || pupil.DistrictID == null || pupil.ProvinceID == null)
                {
                    if (pupil.VillageName != null && pupil.VillageName.Trim() != "")
                    {
                        ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidVilliage")).Append(SEMICOLON_SPACE);
                    }
                }
                if (pupil.VillageName != null && pupil.VillageName.Trim().Length > 50)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidVilliageLength")).Append(SEMICOLON_SPACE);
                }
                if ((int.TryParse(fatherDate, out result) == false || fatherDate.Trim().Length != 4) && fatherDate.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidFatherBirthDate")).Append(SEMICOLON_SPACE);
                }
                if ((int.TryParse(motherDate, out result) == false || motherDate.Trim().Length != 4) && motherDate.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidMotherBirthDate")).Append(SEMICOLON_SPACE);
                }
                //Kiểm tra thôn xóm có chưa?
                if (checkDistrict == true && checkCommune == true && pupil.IsValid == true)
                {
                    if (pupil.CommuneID != null && pupil.DistrictID != null && pupil.ProvinceID != null)
                    {
                        bool isCheck = listPupilTemp.Where(o => o.VillageName.Trim().ToUpper() == pupil.VillageName.Trim().ToUpper() && o.CommuneID == pupil.CommuneID).Count() > 0 ? true : false;

                        if (pupil.VillageID == null && pupil.VillageName != null && pupil.VillageName.Trim() != "" && !isCheck)
                        {
                            bool isCheck1 = lstVillage.Where(o => o.VillageName.Trim().ToUpper() == pupil.VillageName.Trim().ToUpper() && o.CommuneID == pupil.CommuneID).Count() > 0 ? true : false;
                            if (!isCheck1)
                            {
                                pupil.checkInsertVilliage = true;
                            }
                        }
                    }
                }

                // Gán lỗi
                if (ErrorMessage.Length > 0)
                {
                    pupil.IsValid = false;
                    pupil.ErrorMessage += ErrorMessage.ToString();
                }

                if (pupil.IsValid)
                    listPupilTemp.Add(pupil);
                else // Xóa bỏ ký tự ; cuối cùng
                {
                    if (!String.IsNullOrEmpty(pupil.ErrorMessage) && pupil.ErrorMessage.Length > 2)
                    {
                        pupil.ErrorMessage = pupil.ErrorMessage.Remove(pupil.ErrorMessage.Length - 2);
                    }
                }

                listPupil.Add(pupil);

                rowIndex++;
                if (tempt != null)
                {
                    objAutoGenericCode.Index++;
                    lstOutGenericCode.RemoveAll(x => x.EducationLevelID == objAutoGenericCode.EducationLevelID);
                    lstOutGenericCode.Add(objAutoGenericCode);
                }
                tempCount++;
            } // End while
            return listPupil;
        }

        private Stream SetDataToFileExcel(int EducationLevelID, ref string ReportName, bool isErrFile = false)
        {
            EducationLevel edu = EducationLevelBusiness.Find(EducationLevelID);
            //if (edu == null) return null;

            int year = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).Year;

            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.MAU_IMPORT_HO_SO_TRE_MOI);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            ReportName = ReportUtils.RemoveSpecialCharacters(reportDefinition.OutputNamePattern) + "." + reportDefinition.OutputFormat;

            IVTWorkbook book = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = book.GetSheet(1);
            IVTWorksheet refSheet = book.GetSheet(2); // Tỉnh thành
            IVTWorksheet refSheetDis = book.GetSheet(3); // Quận Huyện
            IVTWorksheet refSheetComm = book.GetSheet(4); // Xã phường

            IVTWorksheet refTemp = book.GetSheet(5);
            IVTWorksheet refHDC = book.GetSheet(6);

            IVTWorksheet sheetProvince = book.CopySheetToLast(refSheet, "C3");
            IVTWorksheet sheetDistrict = book.CopySheetToLast(refSheetDis, "D3");
            IVTWorksheet sheetCommune = book.CopySheetToLast(refSheetComm, "E3");

            IVTRange rangeProvince = refSheet.GetRange("A4", "C4");
            IVTRange rangeDistrict = refSheetDis.GetRange("A4", "D4");

            IVTRange rangeProOfCommune = refSheetComm.GetRange("A4", "E4");
            IVTRange rangeCommune = refSheetComm.GetRange("A5", "E5");

            IVTWorksheet sheetHDC = book.CopySheetToLast(refHDC, "A20");

            #region fill data for sheet
            Dictionary<string, object> dicSheet = new Dictionary<string, object>();
            dicSheet["SuperVisingDeptName"] = _globalInfo.SuperVisingDeptName;
            dicSheet["SchoolName"] = _globalInfo.SchoolName.ToUpper();
            dicSheet["DateTime"] = string.Format(Res.Get("Common_Label_DayMonYear"), DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            if (edu != null)
            {
                dicSheet["EducationLevel"] = edu.Resolution;
            }
            sheet.FillVariableValue(dicSheet);
            #endregion

            #region fill data for combobox values
            Dictionary<string, object> dicRef = new Dictionary<string, object>();
            // Khối - Lớp
            List<int> lstClassID = getClassFromEducationLevel(EducationLevelID).Select(x => x.ClassProfileID).ToList();
            List<object> listClass = new List<object>();
            Dictionary<string, object> dicClass = new Dictionary<string, object> { 
                {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                {"IsActive", true},
                {"AppliedLevel", _globalInfo.AppliedLevel}
            };
            if (EducationLevelID != 0)
            {
                dicClass["EducationLevelID"] = EducationLevelID;
            }
            ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value,
                                                dicClass).Where(x => lstClassID.Contains(x.ClassProfileID)).ToList()
                                .ForEach(u => listClass.Add(new { u.DisplayName }));
            dicRef["Classes"] = listClass;

            // Hình thức trúng tuyển
            List<object> listEnrolmentType = new List<object>();
            CommonList.EnrolmentTypeAvailable().ForEach(u => listEnrolmentType.Add(new { Value = u.value }));
            dicRef["EnrolmentTypes"] = listEnrolmentType;


            // Sheet Tỉnh/Thành
            List<Province> lstPro = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.ProvinceName).ToList();
            List<object> listProvince_Temp = new List<object>();
            int i = 1;
            int startRow = 4;
            foreach (var pr in lstPro)
            {
                sheetProvince.CopyPasteSameSize(rangeProvince, startRow, 1);
                listProvince_Temp.Add(new Dictionary<string, object> 
                    {
                        {"Order", i++}, 
                        {"ProvinceCode", pr.ProvinceCode},
                        {"ProvinceName", pr.ProvinceName},
                       
                    });
                startRow++;
            }
            // Fill Tỉnh/Thành
            Dictionary<string, object> dicSheetPro = new Dictionary<string, object>();
            dicSheetPro.Add("list", listProvince_Temp);
            sheetProvince.FillVariableValue(dicSheetPro);


            // Sheet Quận/Huyện           
            var lstDis = (from p in DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } })
                          join q in ProvinceBusiness.All on p.ProvinceID equals q.ProvinceID
                          select new
                          {
                              DistrictID = p.DistrictID,
                              DistrictName = p.DistrictName,
                              DistrictCode = p.DistrictCode,
                              ProvinceID = q.ProvinceID,
                              ProvinceName = q.ProvinceName
                          }).ToList();
            // Fill Quận/Huyện
            int startRow_Dis = 4;
            int order = 1;
            int beginRow_Dis = 4;
            foreach (var pr in lstPro)
            {
                int rowFirstByDis = 1;
                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count();
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        sheetDistrict.CopyPasteSameSize(rangeDistrict, startRow_Dis, 1);
                        sheetDistrict.SetCellValue(startRow_Dis, 1, order++);
                        if (rowFirstByDis == 1)
                        {
                            sheetDistrict.SetCellValue(startRow_Dis, 2, dis.ProvinceName);
                        }
                        sheetDistrict.SetCellValue(startRow_Dis, 3, dis.DistrictCode);
                        sheetDistrict.SetCellValue(startRow_Dis, 4, dis.DistrictName);
                        if (rowFirstByDis == numberOfDis)
                        {
                            IVTRange range = sheetDistrict.GetRange(beginRow_Dis, 2, beginRow_Dis + numberOfDis - 1, 2);
                            range.Merge();
                        }
                        startRow_Dis++;
                        rowFirstByDis++;
                    }
                    beginRow_Dis += numberOfDis;
                }
            }


            // Sheet Xã/Phường
            var lstCommune = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            // Fill Xã/Phường
            int numProvince = 1;
            int startRow_Pro = 4;

            int startRow_Com = 5;
            order = 1;

            int beginRow_Com = 5;
            foreach (var pr in lstPro)
            {
                // Thông tin tỉnh
                sheetCommune.CopyPasteSameSize(rangeProOfCommune, startRow_Pro, 1);
                sheetCommune.SetCellValue(startRow_Pro, 1, numProvince++);
                sheetCommune.SetCellValue(startRow_Pro, 2, pr.ProvinceName);

                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count;
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        int rowFirstOfDis = 1;
                        var lstCommByDis = lstCommune.Where(o => o.DistrictID == dis.DistrictID).OrderBy(o => o.CommuneName).ToList();
                        int numOfComm = lstCommByDis.Count;
                        if (numOfComm > 0)
                        {
                            foreach (var cm in lstCommByDis)
                            {
                                sheetCommune.CopyPasteSameSize(rangeCommune, startRow_Com, 1);
                                sheetCommune.SetCellValue(startRow_Com, 1, order++);
                                if (rowFirstOfDis == 1)
                                {
                                    sheetCommune.SetCellValue(startRow_Com, 2, dis.DistrictCode);
                                    sheetCommune.SetCellValue(startRow_Com, 3, dis.DistrictName);
                                }
                                sheetCommune.SetCellValue(startRow_Com, 4, cm.CommuneCode);
                                sheetCommune.SetCellValue(startRow_Com, 5, cm.CommuneName);
                                if (rowFirstOfDis == numOfComm)
                                {
                                    sheetCommune.GetRange(beginRow_Com, 2, beginRow_Com + numOfComm - 1, 2).Merge();
                                    sheetCommune.GetRange(beginRow_Com, 3, beginRow_Com + numOfComm - 1, 3).Merge();
                                }
                                startRow_Com++;
                                rowFirstOfDis++;
                            }
                        }
                        beginRow_Com += numOfComm; // dòng tiến hành merge
                    }
                }
                // set lại biến đếm cho tỉnh tiếp theo
                startRow_Pro += order;
                startRow_Com++;
                beginRow_Com++;
                order = 1;
            }


            // Đối tượng chính sách  
            List<object> listPolicyTarget = new List<object>();
            var lstPOT = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lstPOT.ForEach(u => listPolicyTarget.Add(new { u.Resolution }));
            dicRef["PolicyTargets"] = listPolicyTarget;

            // Dân tộc
            List<object> listEthnic = new List<object>();
            List<Ethnic> lst1 = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.EthnicName).ToList();
            Ethnic e1 = lst1.FirstOrDefault(o => o.EthnicName.Contains("Kinh"));
            lst1.Remove(e1);
            lst1.Insert(0, e1);
            lst1.ForEach(u => listEthnic.Add(new { u.EthnicName }));
            dicRef["Ethnics"] = listEthnic;

            // Chế độ chính sách
            List<object> listPolicyRegimes = new List<object>();
            List<PolicyRegime> lst11 = PolicyRegimeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lst11.ForEach(u => listPolicyRegimes.Add(new { u.Resolution }));
            //EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList().ForEach(u => listEthnic.Add(new { u.EthnicName }));
            dicRef["PolicyRegimes"] = listPolicyRegimes;

            //Loại khuyết tật
            List<object> listDisabledTyeObj = new List<object>();
            List<DisabledType> listDisabledTye = DisabledTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            listDisabledTye.ForEach(u => listDisabledTyeObj.Add(new { u.Resolution }));
            //ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList().ForEach(u => listReligion.Add(new { u.Resolution }));
            dicRef["DisabledTypes"] = listDisabledTyeObj;

            // Tôn giáo
            List<object> listReligionObj = new List<object>();
            List<Religion> lstReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            Religion r1 = lstReligion.FirstOrDefault(o => o.Resolution.Contains("Không"));
            lstReligion.Remove(r1);
            lstReligion.Insert(0, r1);
            lstReligion.ForEach(u => listReligionObj.Add(new { u.Resolution }));
            dicRef["Religions"] = listReligionObj;

            List<object> listBloodType = new List<object>();
            CommonList.BloodType().ForEach(u => listBloodType.Add(new { Value = u.value }));
            dicRef["BloodTypes"] = listBloodType;

            List<object> listForeignLanguageTraining = new List<object>();
            CommonList.ForeignLanguageTraining().ForEach(u => listForeignLanguageTraining.Add(new { Value = u.value }));
            dicRef["ForeignLanguageTrainings"] = listForeignLanguageTraining;

            List<object> listProfileStatus = new List<object>();
            CommonList.PupilStatus().Where(u => u.key == SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString()).ToList().ForEach(u => listProfileStatus.Add(new { Value = u.value }));
            dicRef["ProfileStatuses"] = listProfileStatus;

            List<object> listGenres = new List<object>();
            CommonList.GenreAndSelect().ForEach(u => listGenres.Add(new { Value = u.value }));
            dicRef["Genres"] = listGenres;

            refTemp.FillVariableValue(dicRef);
            // Set lai ten cac Sheet
            sheetProvince.Name = "TinhThanh";
            refSheet.Delete();

            sheetDistrict.Name = "QuanHuyen";
            refSheetDis.Delete();

            sheetCommune.Name = "XaPhuong";
            refSheetComm.Delete();

            sheetHDC.Name = "HuongDanChung";
            refHDC.Delete();

            #endregion

            //Neu la cap 1 thi an cot he hoc ngoai ngu
            if (_globalInfo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                sheet.SetColumnWidth(31, 0);

            if (isErrFile)
            {
                List<ImportChildrenViewModel> lstPupilImportedErr = new List<ImportChildrenViewModel>();
                lstPupilImportedErr = (List<ImportChildrenViewModel>)Session["LIST_IMPORTED_PUPIL"];
                lstPupilImportedErr = lstPupilImportedErr.Where(p => p.IsValid == false).ToList();
                ImportChildrenViewModel objPupilErr = null;
                int startRowErr = 5;
                for (int j = 0; j < lstPupilImportedErr.Count; j++)
                {
                    objPupilErr = lstPupilImportedErr[j];
                    //STT
                    sheet.SetCellValue(startRowErr, 1, j + 1);
                    //Ma HS
                    sheet.SetCellValue(startRowErr, 2, objPupilErr.PupilCode);
                    //Ho ten
                    sheet.SetCellValue(startRowErr, 3, objPupilErr.FullName);
                    //Ngay sinh
                    sheet.SetCellValue(startRowErr, 4, objPupilErr.BirthDate);
                    //Gioi tinh
                    sheet.SetCellValue(startRowErr, 5, objPupilErr.GenreName);
                    //Lop
                    sheet.SetCellValue(startRowErr, 6, objPupilErr.ClassName);
                    //Hinh thuc trung tuyen
                    sheet.SetCellValue(startRowErr, 7, objPupilErr.EnrolmentTypeName);
                    //Ngay vao truong
                    sheet.SetCellValue(startRowErr, 8, objPupilErr.EnrolmentDateStr);
                    //Tinh thanh
                    sheet.SetCellValue(startRowErr, 9, objPupilErr.ProvinceName);
                    //Quan huyen
                    sheet.SetCellValue(startRowErr, 10, objPupilErr.DistrictName);
                    //Xa,Phuong
                    sheet.SetCellValue(startRowErr, 11, objPupilErr.CommuneName);
                    //Thon xom
                    sheet.SetCellValue(startRowErr, 12, objPupilErr.VillageName);
                    //So dang bo
                    sheet.SetCellValue(startRowErr, 13, objPupilErr.StorageNumber);
                    //Doi tuong chinh sach
                    sheet.SetCellValue(startRowErr, 14, objPupilErr.PolicyTargetName);
                    sheet.SetCellValue(startRowErr, 15, objPupilErr.PolicyRegimeName);
                    sheet.SetCellValue(startRowErr, 16, objPupilErr.DisabledTypeName);
                    //Dan toc
                    sheet.SetCellValue(startRowErr, 17, objPupilErr.EthnicName);
                    //Ton giao
                    sheet.SetCellValue(startRowErr, 18, objPupilErr.ReligionName);
                    //Noi sinh
                    sheet.SetCellValue(startRowErr, 19, objPupilErr.BirthPlace);
                    //Que quan
                    sheet.SetCellValue(startRowErr, 20, objPupilErr.HomeTown);
                    //Dia chi thuong tru
                    sheet.SetCellValue(startRowErr, 21, objPupilErr.PermanentResidentalAddress);
                    //Dia chi tam tru
                    sheet.SetCellValue(startRowErr, 22, objPupilErr.PermanentResidentalAddress);
                    //So dien thoai di dong
                    sheet.SetCellValue(startRowErr, 23, objPupilErr.Mobile);
                    //Ho ten cha
                    sheet.SetCellValue(startRowErr, 24, objPupilErr.FatherFullName);
                    //Nam sinh cua cha
                    sheet.SetCellValue(startRowErr, 25, objPupilErr.FatherBirthDateStr);
                    //Nghe nghiep cua cha
                    sheet.SetCellValue(startRowErr, 26, objPupilErr.FatherJob);
                    //SDT cua cha
                    sheet.SetCellValue(startRowErr, 27, objPupilErr.FatherMobile);
                    //Email cha
                    sheet.SetCellValue(startRowErr, 28, objPupilErr.FatherEmail);
                    //Ho ten me
                    sheet.SetCellValue(startRowErr, 29, objPupilErr.MotherFullName);
                    //Nam sinh cua me
                    sheet.SetCellValue(startRowErr, 30, objPupilErr.MotherBirthDateStr);
                    //Nghe nghiep cua me
                    sheet.SetCellValue(startRowErr, 31, objPupilErr.MotherJob);
                    //SDT cua me
                    sheet.SetCellValue(startRowErr, 32, objPupilErr.MotherMobile);
                    //Email me
                    sheet.SetCellValue(startRowErr, 33, objPupilErr.MotherEmail);
                    //Nhom mau
                    sheet.SetCellValue(startRowErr, 34, objPupilErr.BloodTypeName);
                    sheet.SetCellValue(startRowErr, 35, objPupilErr.MinorityFatherName = true ? "x" : "");
                    sheet.SetCellValue(startRowErr, 36, objPupilErr.MinorityMotherName = true ? "x" : "");
                    //Trang thai
                    sheet.SetCellValue(startRowErr, 37, objPupilErr.ProfileStatusName);
                    //Ma HS moi
                    sheet.SetCellValue(startRowErr, 38, objPupilErr.PupilCodeNew);
                    //Mo ta
                    sheet.SetCellValue(startRowErr, 39, objPupilErr.ErrorMessage);

                    startRowErr++;
                }
                sheet.UnHideColumn(35);
                sheet.GetRange(5, 1, 5 + lstPupilImportedErr.Count - 1, 35).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            else
            {
                sheet.SetColumnWidth(35, 0);//an cot mo ta neu nhu download template mau
            }

            return book.ToStream();
        }
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult Save()
        {
            List<ImportChildrenViewModel> listPupil = (List<ImportChildrenViewModel>)Session[ImportChildrenConstants.LIST_IMPORTED_PUPIL];
            SaveData();
            int countAllPupil = listPupil.Count;
            int countPupilValid = listPupil.Count(p => p.IsValid);
            string retMsg = String.Format("{0} {1}/{2} trẻ", Res.Get("Common_Label_ImportSuccessMessage"), countPupilValid, countAllPupil);
            return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS), "text/html");

            //return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        private void SaveData()
        {
            GlobalInfo glo = new GlobalInfo();

            List<ImportChildrenViewModel> listPupil = (List<ImportChildrenViewModel>)Session[ImportChildrenConstants.LIST_IMPORTED_PUPIL];
            listPupil = listPupil.Where(x => x.IsValid).ToList();
            if (listPupil == null || !listPupil.Any())
            {
                return;
            }
            List<string> lstPupilCode = listPupil.Select(o => o.PupilCode).ToList();
            List<PupilProfileBO> listPupilUpdate = PupilProfileBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "IsActive", true } })
                                                                       .Where(o => o.IsActive && o.CurrentSchoolID == glo.SchoolID.Value && lstPupilCode.Contains(o.PupilCode))
                                                                       .ToList();
            List<string> listPupilCodeUpdate = listPupilUpdate.Select(o => o.PupilCode).ToList();

            List<ImportChildrenViewModel> lstPupilInsert = listPupil.Where(o => !listPupilCodeUpdate.Any(t => t.ToLower().Equals(o.PupilCode.ToLower()))).ToList();
            List<ImportChildrenViewModel> lstPupilUpdate = listPupil.Where(o => listPupilCodeUpdate.Any(t => t.ToLower().Equals(o.PupilCode.ToLower()))).ToList();
            List<int> lstUpdatePupilID = listPupilUpdate.Select(o => o.PupilProfileID).ToList();
            List<PupilProfile> lstPupilUpdatePP = PupilProfileBusiness.All
                                                   .Where(o => o.CurrentSchoolID == glo.SchoolID.Value && lstUpdatePupilID.Contains(o.PupilProfileID)).ToList();
            //Lay danh sach lop
            List<int> lstClassID = listPupil.Select(o => o.CurrentClassID).Distinct().ToList();
            List<PupilProfile> lstPupilInsertPP = new List<PupilProfile>();
            List<PupilProfile> lstPupilUpdateAll = new List<PupilProfile>();
            List<int> lstEnrollmentTypeInsert = new List<int>();
            List<int> lstEnrollmentTypeUpdate = new List<int>();
            List<int> lstOrderInsert = new List<int>();
            List<int> lstOrderUpdate = new List<int>();
            List<int> lstClassIDUpdate = new List<int>();
            List<ImportChildrenViewModel> lstPupilInsertClass = new List<ImportChildrenViewModel>();

            List<ImportChildrenViewModel> lstPupilUpdateClass = new List<ImportChildrenViewModel>();
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = glo.AcademicYearID;
            List<ImportChildrenViewModel> listPupilVilliage = listPupil.Where(o => o.checkInsertVilliage == true).ToList();

            foreach (int ClassID in lstClassID)
            {

                #region Insert
                lstPupilInsertClass = lstPupilInsert.Where(o => o.CurrentClassID == ClassID).ToList();
                foreach (ImportChildrenViewModel pupil in lstPupilInsertClass)
                {
                    if (pupil.IsValid)
                    {
                        var pupilProfile = new PupilProfile
                        {
                            CurrentAcademicYearID = glo.AcademicYearID.Value,
                            IsActive = true
                        };
                        bool isReal = listPupilVilliage.Where(o => o.PupilCode == pupil.PupilCode).Count() > 0 ? true : false;
                        if (isReal)
                        {
                            Village village = new Village();
                            village.VillageName = pupil.VillageName;
                            village.ShortName = pupil.VillageName;
                            village.CommuneID = pupil.CommuneID;
                            village.CreateDate = DateTime.Now;
                            village.IsActive = true;
                            VillageBusiness.Insert(village);
                            VillageBusiness.Save();
                            pupil.VillageID = village.VillageID;
                        }
                        else
                        {
                            if (pupil.VillageName != null && pupil.VillageName.Trim() != "")
                            {
                                Village vil = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true }, { "VillageName", pupil.VillageName }, { "CommuneID", pupil.CommuneID } }).FirstOrDefault();
                                if (vil != null)
                                {
                                    pupil.VillageID = vil.VillageID;
                                }
                            }
                            else
                            {
                                pupil.VillageID = null;

                            }
                        }
                        CopyValue(pupilProfile, pupil, true);
                        lstPupilInsertPP.Add(pupilProfile);
                        lstEnrollmentTypeInsert.Add(pupil.EnrolmentType);
                        lstOrderInsert.Add(pupil.Index);
                    }
                }
                #endregion

                #region update
                lstPupilUpdateClass = lstPupilUpdate.Where(o => o.CurrentClassID == ClassID).ToList();
                foreach (ImportChildrenViewModel pupil in lstPupilUpdateClass)
                {
                    PupilProfileBO ppBO = listPupilUpdate.FirstOrDefault(o => o.PupilCode == pupil.PupilCode);
                    if (pupil.IsValid)
                    {

                        PupilProfile pupilProfile = lstPupilUpdatePP.FirstOrDefault(o => o.PupilProfileID == ppBO.PupilProfileID);
                        if (pupilProfile != null)
                        {
                            int currentClassID = pupilProfile.CurrentClassID;
                            pupilProfile.CurrentAcademicYearID = glo.AcademicYearID.Value;
                            pupilProfile.IsActive = true;
                            bool isReal = listPupilVilliage.Where(o => o.PupilCode == pupil.PupilCode).Count() > 0 ? true : false;
                            if (isReal)
                            {
                                Village village = new Village();
                                village.VillageName = pupil.VillageName;
                                village.ShortName = pupil.VillageName;
                                village.CommuneID = pupil.CommuneID;
                                village.CreateDate = DateTime.Now;
                                village.IsActive = true;
                                VillageBusiness.Insert(village);
                                VillageBusiness.Save();
                                pupil.VillageID = village.VillageID;
                            }
                            else
                            {
                                if (pupil.VillageName != null && pupil.VillageName.Trim() != "")
                                {
                                    Village vil = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true }, { "VillageName", pupil.VillageName }, { "CommuneID", pupil.CommuneID } }).FirstOrDefault();
                                    if (vil != null)
                                    {
                                        pupil.VillageID = vil.VillageID;
                                    }
                                }
                                else
                                {
                                    pupil.VillageID = null;

                                }
                            }
                            CopyValue(pupilProfile, pupil, false);
                            lstPupilUpdateAll.Add(pupilProfile);
                            lstEnrollmentTypeUpdate.Add(pupil.EnrolmentType);
                            lstClassIDUpdate.Add(currentClassID);
                        }
                        lstOrderUpdate.Add(pupil.Index);
                    }
                }
                #endregion
            }
            if (lstPupilInsertPP.Any())
            {
                PupilProfileBusiness.ImportPupilProfile(glo.UserAccountID, lstPupilInsertPP, lstEnrollmentTypeInsert, lstOrderInsert, lstClassID);
            }
            if (lstPupilUpdateAll.Any())
            {
                PupilProfileBusiness.UpdateImportPupilProfile(glo.UserAccountID, lstPupilUpdateAll, lstEnrollmentTypeUpdate, lstOrderUpdate, lstClassIDUpdate, lstClassID);
            }

            VillageBusiness.Save();
            PupilProfileBusiness.Save();
        }
        #endregion

        #region Util Functions

        private IQueryable<ClassProfile> getClassFromEducationLevel(int EducationLevelID)
        {
            //: ClassProfileBussiness.SearchBySchool(UserInfo.SchoolID, Dictionnary)
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                    {"AppliedLevel",global.AppliedLevel.Value}, 
                    {"EducationLevelID",EducationLevelID},
                    {"AcademicYearID",global.AcademicYearID.Value}
            };

            //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
            if (!global.IsAdminSchoolRole && !global.IsViewAll && !UtilsBusiness.IsBGH(global.UserAccountID))
            {
                dic["UserAccountID"] = global.UserAccountID;
                //dic["Type"] = SMAS.Web.Constants.GlobalConstants.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_TEACHING;
            }

            IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic);
            //IQueryable<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(global.SchoolID.Value, dic);
            return lsCP;
        }

        private void CopyValue(PupilProfile pupilProfile, ImportChildrenViewModel pupil, bool isInsert)
        {
            pupilProfile.BirthDate = pupil.BirthDate.Value;
            pupilProfile.BirthPlace = pupil.BirthPlace;
            pupilProfile.BloodType = pupil.BloodType;
            pupilProfile.CreatedDate = DateTime.Now;
            pupilProfile.CurrentClassID = pupil.CurrentClassID;
            pupilProfile.CurrentSchoolID = pupil.CurrentSchoolID;
            pupilProfile.EnrolmentDate = pupil.EnrolmentDate.Value;
            pupilProfile.EthnicID = pupil.EthnicID;
            pupilProfile.FatherBirthDate = pupil.FatherBirthDate;
            pupilProfile.FatherEmail = string.Empty;
            pupilProfile.FatherFullName = pupil.FatherFullName;
            pupilProfile.FatherJob = pupil.FatherJob;
            pupilProfile.FatherMobile = pupil.FatherMobile;
            pupilProfile.ForeignLanguageTraining = pupil.ForeignLanguageTraining;
            pupilProfile.FullName = pupil.FullName;
            pupilProfile.Genre = pupil.Genre;
            pupilProfile.HomeTown = pupil.HomeTown;
            pupilProfile.MotherBirthDate = pupil.MotherBirthDate;
            pupilProfile.MotherEmail = string.Empty;
            pupilProfile.MotherFullName = pupil.MotherFullName;
            pupilProfile.MotherJob = pupil.MotherJob;
            pupilProfile.MotherMobile = pupil.MotherMobile;
            pupilProfile.MotherEmail = pupil.MotherEmail;
            pupilProfile.FatherEmail = pupil.FatherEmail;
            pupilProfile.StorageNumber = pupil.StorageNumber;
            pupilProfile.Mobile = pupil.Mobile;
            pupilProfile.Name = pupil.Name;
            pupilProfile.PermanentResidentalAddress = pupil.PermanentResidentalAddress;
            pupilProfile.PolicyTargetID = pupil.PolicyTargetID;
            pupilProfile.ProfileStatus = pupil.ProfileStatus;
            if (pupil.ProvinceID != null) pupilProfile.ProvinceID = (int)pupil.ProvinceID;
            pupilProfile.PupilCode = (!string.IsNullOrEmpty(pupil.PupilCodeNew) && isInsert == false) ? pupil.PupilCodeNew : pupil.PupilCode;
            pupilProfile.ReligionID = pupil.ReligionID;
            pupilProfile.TempResidentalAddress = pupil.TempResidentalAddress;
            //Do import moi cap nhat lai ProvinceID nen de DistrictID và CommuneID la null
            pupilProfile.CommuneID = pupil.CommuneID;
            pupilProfile.DistrictID = pupil.DistrictID;
            pupilProfile.VillageID = pupil.VillageID;
            pupilProfile.MinorityFather = (pupil.MinorityFatherName != null && pupil.MinorityFatherName.ToString() == "x") ? true : false;
            pupilProfile.MinorityMother = (pupil.MinorityMotherName != null && pupil.MinorityMotherName.ToString() == "x") ? true : false;
            pupilProfile.PolicyRegimeID = pupil.PolicyRegimeID;
            pupilProfile.DisabledTypeID = pupil.DisabledTypeID;
            pupilProfile.IsDisabled = pupil.IsDisabled == 1 ? true : false;
        }

        private string GetCellString(IVTWorksheet sheet, string cellName)
        {
            object o = sheet.GetRange(cellName, cellName).Value;
            return o == null ? string.Empty : o.ToString().Trim();
        }

        private DateTime? GetCellDate(IVTWorksheet sheet, string cellName)
        {
            object o = sheet.GetRange(cellName, cellName).Value;
            var culture = new CultureInfo("vi-VN");
            if (o != null)
            {
                try
                {
                    if (o is DateTime)
                        return (DateTime)o;
                    var oStr = o.ToString().Trim();
                    if (oStr.EndsWith(".00"))
                        oStr = oStr.Remove(oStr.Length - 4, 3);
                    if (oStr.EndsWith(".0"))
                        oStr = oStr.Remove(oStr.Length - 3, 2);

                    DateTime dt;

                    DateTime.TryParse(oStr, out dt);
                    return dt;
                }
                catch { }
            }
            return null;
        }

        private int SetGenre(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();
            ComboObject c = CommonList.GenreAndSelect().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? (int)255 : Convert.ToByte(c.key);
        }

        private int SetGenreNew(string value)
        {
            value = value == null ? string.Empty : value.Trim();
            return value == string.Empty ? SystemParamsInFile.GENRE_MALE : SystemParamsInFile.GENRE_FEMALE;
        }

        private int SetEnrolementType(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            ComboObject c = CommonList.EnrolmentType().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int? SetBloodType(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty) return null;

            ComboObject c = CommonList.BloodType().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetForeignLanguageTraining(string value)
        {
            GlobalInfo glo = new GlobalInfo();
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty)
                return glo.AppliedLevel != null && glo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_TERTIARY ? (int)SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_3_YEARS : (int)SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_UNIDENTIFIED;

            ComboObject c = CommonList.ForeignLanguageTraining().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetProfileStatus(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();
            ComboObject c = CommonList.PupilStatus().SingleOrDefault(u => u.key == SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString() && u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetFromList(List<ComboObject> list, string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();
            ComboObject c = list.FirstOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? 0 : Convert.ToInt32(c.key);
        }

        private int SetFromListCommune(List<Village> listVillage, int? communeID, string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();
            Village village = listVillage.Where(o => o.VillageName.Trim().ToLower() == value && o.CommuneID == communeID).FirstOrDefault();
            if (village != null)
                return village.VillageID;
            else
                return 0;
        }

        private int? SetNulableFromList(List<ComboObject> list, string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty) return null;

            ComboObject c = list.FirstOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToInt32(c.key);
        }

        private void ValidateImportedPupil(ImportChildrenViewModel pupil)
        {
            ValidationContext vc = new ValidationContext(pupil, null, null);
            List<string> messages = new List<string>();
            foreach (PropertyInfo pi in pupil.GetType().GetProperties())
            {
                string resKey = string.Empty;
                if (pi.Name == "FatherBirthDate" || pi.Name == "MotherBirthDate" || pi.Name == "MotherEmail" || pi.Name == "FatherEmail")
                {
                    continue;
                }
                foreach (var att in pi.GetCustomAttributes(true))
                {
                    if (att is DisplayAttribute)
                    {
                        resKey = ((DisplayAttribute)att).Name;
                        break;
                    }

                    if (att is DisplayNameAttribute)
                    {
                        resKey = ((DisplayAttribute)att).Name;
                        break;
                    }
                }

                foreach (var att in pi.GetCustomAttributes(true))
                {
                    if (att is ValidationAttribute)
                    {
                        ValidationAttribute attV = (ValidationAttribute)att;
                        vc.MemberName = pi.Name;
                        if (attV.GetValidationResult(pi.GetValue(pupil, null), vc) != ValidationResult.Success)
                        {
                            List<object> Params = new List<object>();
                            Params.Add(Res.Get(resKey));

                            if (att is StringLengthAttribute)
                            {
                                StringLengthAttribute attS = (StringLengthAttribute)att;
                                Params.Add(attS.MaximumLength);
                            }

                            messages.Add(string.Format(Res.Get(attV.ErrorMessageResourceName), Params.ToArray()));
                        }
                    }
                }
            }

            if (messages.Count > 0)
            {
                pupil.IsValid = false;
                pupil.ErrorMessage += string.Join(SEMICOLON_SPACE, messages);
                pupil.ErrorMessage += SEMICOLON_SPACE;
            }
        }

        private string GetPupilName(string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
                return string.Empty;

            fullName = fullName.Trim();

            int index = fullName.LastIndexOf(' ');

            return index > 0 ? fullName.Substring(index + 1) : fullName;
        }

        private int? GetCellIndex(string value)
        {
            if (string.IsNullOrEmpty(value) || value.Trim() == "" || !Regex.IsMatch(value, "^\\d+$"))
                return null;

            return Convert.ToInt32(value.Trim());
        }

        private string ToStringWithFixedLength(int number, int length)
        {
            if (number < 0) return string.Empty;

            if (number.ToString().Length > length) return string.Empty;

            return number.ToString().PadLeft(length, '0');
        }

        private bool checkCompareDistrict(int? provinceID, int? districtID)
        {
            if (districtID == null) return true;
            if (districtID != null && provinceID == null) return false;
            var check = DistrictBusiness.Search(new Dictionary<string, object> { { "ProvinceID", provinceID }, { "DistrictID", districtID }, { "IsActive", true } });
            if (check.Count() > 0)
                return true;
            return false;
        }
        private bool checkCompareCommune(int? districtID, int? communeID)
        {
            if (communeID == null || districtID == null)
                return true;
            if (communeID != null && districtID == null) return false;
            var check = CommuneBusiness.Search(new Dictionary<string, object> { { "DistrictID", districtID }, { "CommuneID", communeID }, { "IsActive", true } });
            if (check.Count() > 0)
                return true;
            return false;
        }
        private bool checkCompareVillage(int? villiageID, int? communeID)
        {
            if (villiageID == null || communeID == null) return true;
            if (villiageID != null && communeID == null) return false;
            var check = VillageBusiness.Search(new Dictionary<string, object> { { "CommuneID", communeID }, { "Villiage", villiageID }, { "IsActive", true } });
            if (check.Count() > 0)
                return true;
            return false;
        }
        #endregion

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}
