﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ProvinceSubjectArea.Models;
using SMAS.Business;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.ProvinceSubjectArea.Controllers
{
    public class ProvinceSubjectController : BaseController
    {        
        private readonly IProvinceSubjectBusiness ProvinceSubjectBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;

        public const string Math = "Toán";
        public const string Literature = "Văn";
        public const int CoefficientDefault = 2;

        public ProvinceSubjectController(IProvinceSubjectBusiness provincesubjectBusiness, IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness, IEducationLevelBusiness EducationLevelBusiness)
		{
			this.ProvinceSubjectBusiness = provincesubjectBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
		}
		
		//
        // GET: /ProvinceSubject/

        //GetViewData

        public void GetDataToList(IDictionary<string,object> dic)
        {

            ViewData[ProvinceSubjectConstants.Visible_ColCoefficient] = true;
            
            //EducationLevelID, AppliedLevel, AcademicYearOfProvinceID, IsRegularEducation

            IQueryable<ProvinceSubjectBO> listProvinceSubjectBO = ProvinceSubjectBusiness.GetListProvinceSubject(dic).OrderBy(o=>o.OrderInSubject);//.OrderBy(o=>o.OrderInSubject).ThenBy(o=>o.DisplayName);

            IQueryable<ProvinceSubjectViewModel> query = listProvinceSubjectBO.Select(sp => new ProvinceSubjectViewModel
                         {
                             ProvinceSubjectID = sp.ProvinceSubjectID,
                             DisplayName  = sp.DisplayName,
                             Abbreviation = sp.Abbreviation,
                             SubjectID = sp.SubjectID,
                             ProvinceID = sp.ProvinceID,
                             AcademicYearOfProvinceID = sp.AcademicYearOfProvinceID,
                             AppliedLevel = sp.AppliedLevel,
                             EducationLevelID = sp.EducationLevelID,
                             IsRegularEducation = sp.IsRegularEducation,
                             Coefficient = sp.Coefficient,
                             AppliedType = sp.AppliedType,
                             IsCommenting = sp.IsCommenting,
                             SectionPerWeekFirstSemester = sp.SectionPerWeekFirstSemester,
                             NumberOfLession = sp.NumberOfLession,
                             SectionPerWeekSecondSemester = sp.SectionPerWeekSecondSemester,
                             IsCoreSubject = sp.IsCoreSubject,
                             IsChoice = sp.IsChoice,
                             OrderInSubject = sp.OrderInSubject,
                             IsEditIsCommenting = sp.IsEditIsCommenting.Value
                             //IsChoice = sp.ProvinceSubjectID == null ? false: true
                         });

            if (int.Parse(dic["AppliedLevel"].ToString()) == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                ViewData[ProvinceSubjectConstants.Visible_ColCoefficient] = false;
            }
            IEnumerable<ProvinceSubjectViewModel> listProvinceSubjectViewModel = query.ToList();
            List<ProvinceSubjectViewModel> list = new List<ProvinceSubjectViewModel>();
            foreach (ProvinceSubjectViewModel ProvinceSubjectViewModel in listProvinceSubjectViewModel)
            {
                if (ProvinceSubjectViewModel.Coefficient == null)
                {
                    ProvinceSubjectViewModel.Coefficient = 1;
                    //if (ProvinceSubjectViewModel.DisplayName == Math || ProvinceSubjectViewModel.DisplayName == Literature)
                    if (ProvinceSubjectViewModel.IsCoreSubject == true && int.Parse(dic["AppliedLevel"].ToString()) == SystemParamsInFile.APPLIED_LEVEL_TERTIARY && (bool)dic["IsRegularEducation"] == true)
                    {
                        ProvinceSubjectViewModel.Coefficient = CoefficientDefault;
                    }
                }
                                
                if (ProvinceSubjectViewModel.SectionPerWeekFirstSemester == null)
                {
                    ProvinceSubjectViewModel.SectionPerWeekFirstSemester = ProvinceSubjectViewModel.NumberOfLession;
                }
                if (ProvinceSubjectViewModel.SectionPerWeekSecondSemester == null)
                {
                    ProvinceSubjectViewModel.SectionPerWeekSecondSemester = ProvinceSubjectViewModel.NumberOfLession;
                }
                list.Add(ProvinceSubjectViewModel);
            }
            ViewData[ProvinceSubjectConstants.LIST_IsCommenting] = CommonList.IsCommenting();
            ViewData[ProvinceSubjectConstants.LIST_AppliedType] = CommonList.AppliedTypeSubject();
            ViewData[ProvinceSubjectConstants.LIST_PROVINCESUBJECT] = list.OrderBy(o=>o.OrderInSubject).ThenBy(o=>o.DisplayName).ToList();
        }


        public void GetViewData()
        {
            //- cboAppliedLevel: CommonList.SchoolEducationGrade
            //- cboEducationLevel: EducationLevelBusiness.GetByGrade(cboAppliedLevel.Value). Mặc định là khối đầu cấp
            //- cboIsCommenting: CommonList.IsCommentingSubject
            //- cboAppliedType: CommonList.AppliedTypeSubject
            //Gọi hàm ProvinceSubjectBusiness.GetListProvinceSubject để lấy dữ liệu đưa lên Grid
            //- chkRegularEducation: chỉ hiển thị với cấp THPT

            List<AcademicYearOfProvince> listAcademicYear = AcademicYearOfProvinceBusiness.SearchByProvince(new GlobalInfo().ProvinceID.Value, new Dictionary<string, object>()).OrderByDescending(o => o.Year).ToList();
            ViewData[ProvinceSubjectConstants.LIST_AcademicYear] = listAcademicYear;
            AcademicYearOfProvince bt = listAcademicYear.Where(o => o.FirstSemesterStartDate <= DateTime.Now && o.SecondSemesterEndDate >= DateTime.Now).FirstOrDefault();
            string year = "";

            if (bt != null)
            {
                year = bt.DisplayTitle;
            }
            else
            {
                year = DateTime.Now.Year.ToString() + " - " + (DateTime.Now.Year + 1).ToString();
            }

            ViewData[ProvinceSubjectConstants.Current_Year] = year;
            ViewData[ProvinceSubjectConstants.LIST_AppliedLevel] = CommonList.SchoolEducationGrade();
            ViewData[ProvinceSubjectConstants.LIST_EducationLevel] = EducationLevelBusiness.GetByGrade(int.Parse(CommonList.SchoolEducationGrade().FirstOrDefault().key));
            ViewData[ProvinceSubjectConstants.LIST_IsCommenting] = CommonList.IsCommenting();
            ViewData[ProvinceSubjectConstants.LIST_AppliedType] = CommonList.AppliedTypeSubject();

            int EducationLevelID = EducationLevelBusiness.GetByGrade(int.Parse(CommonList.SchoolEducationGrade().FirstOrDefault().key)).FirstOrDefault().EducationLevelID;


            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = EducationLevelID;
            dic["AppliedLevel"] = CommonList.SchoolEducationGrade().FirstOrDefault().key;
            if (listAcademicYear != null && listAcademicYear.Count() > 0)
            {
                dic["AcademicYearOfProvinceID"] = listAcademicYear.FirstOrDefault().AcademicYearOfProvinceID;
            }
            dic["IsRegularEducation"] = false ;
            //EducationLevelID, AppliedLevel, AcademicYearOfProvinceID, IsRegularEducation
            GetDataToList(dic);
            ViewData[ProvinceSubjectConstants.Visible_chkRegularEducation] = "none";
            
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetcboEducationGrade(int? AppliedLevelID)
        {
            if (AppliedLevelID.HasValue)
            {
                //Load combobox lop hoc theo khoi hoc
                IEnumerable<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(AppliedLevelID.Value);
                if (listEducationLevel != null && listEducationLevel.Count() > 0)
                {
                    return Json(new SelectList(listEducationLevel, "EducationLevelID", "Resolution"));
                }
            }

            return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
        }


        public ActionResult Index()
        {
            
            //Get view data here
            GetViewData();
            SetViewDataPermission("ProvinceSubject", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            return View();
        }

		//
        // GET: /ProvinceSubject/Search



        public PartialViewResult ReloadList(int? AcademicYearOfProvinceID, int? AppliedLevelID, int? EducationLevelID, bool? IsRegularEducation)
        {
            SetViewDataPermission("ProvinceSubject", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EducationLevelID"] = EducationLevelID;
            SearchInfo["AppliedLevel"] = AppliedLevelID;
            SearchInfo["AcademicYearOfProvinceID"] = AcademicYearOfProvinceID;

            if (AppliedLevelID.HasValue && AppliedLevelID == 3)
            {
                SearchInfo["IsRegularEducation"] = IsRegularEducation;
            }
            else {
                SearchInfo["IsRegularEducation"] = false;
            }
            
			//add search info
			//
            GetDataToList(SearchInfo);


            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(FormCollection form, int? AcademicYearOfProvinceID, int? AppliedLevelID, int? EducationLevelID, bool? IsRegularEducation)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("ProvinceSubject", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EducationLevelID"] = EducationLevelID;
            SearchInfo["AppliedLevel"] = AppliedLevelID;
            //if (AppliedLevelID == 3)
            //{
            //    IsRegularEducation = true;
            //}
            //else {
            //    IsRegularEducation = false;
            //}
            SearchInfo["AcademicYearOfProvinceID"] = AcademicYearOfProvinceID;
            SearchInfo["IsRegularEducation"] = IsRegularEducation;

            if (!AcademicYearOfProvinceID.HasValue)
            {
                return Json(new JsonMessage(Res.Get("ProvinceSubject_Label_NoAcademicYear"), JsonMessage.ERROR));
            }
            IQueryable<ProvinceSubjectBO> listProvinceSubjectBO = ProvinceSubjectBusiness.GetListProvinceSubject(SearchInfo);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            foreach (string key in form.AllKeys)
            {
                dic[key] = form[key];
            }
            List<ProvinceSubject> listProvinceSubject = new List<ProvinceSubject>();
            foreach (ProvinceSubjectBO ProvinceSubjectBO in listProvinceSubjectBO)
            {
                
                string checkbox = form["form[chk_" + ProvinceSubjectBO.SubjectID+"]"];
                if (checkbox == "on")
                {
                    int? Coefficient = null;
                    ProvinceSubject ProvinceSubject = new ProvinceSubject();
                    if (form["form[Coefficient" + ProvinceSubjectBO.SubjectID.ToString() + "]"] != "")
                    {
                        Coefficient = SMAS.Business.Common.Utils.GetNullableByte(dic, "form[Coefficient" + ProvinceSubjectBO.SubjectID.ToString() + "]");
                    }
                    else
                    {
                        return Json(new JsonMessage(Res.Get("ProvinceSubject_Label_HasNoCoefficient"), JsonMessage.ERROR));
                    }
                    
                    int? AppliedType = SMAS.Business.Common.Utils.GetNullableByte(dic, "form[AppliedType" + ProvinceSubjectBO.SubjectID.ToString() + "]");
                    int? IsCommenting = SMAS.Business.Common.Utils.GetNullableByte(dic, "form[IsCommenting" + ProvinceSubjectBO.SubjectID.ToString() + "]");

                    decimal? SectionPerWeekFirstSemester = null;
                    decimal? SectionPerWeekSecondSemester = null;

                    if (form["form[SectionPerWeekFirstSemester" + ProvinceSubjectBO.SubjectID.ToString() + "]"] != "" && form["form[SectionPerWeekSecondSemester" + ProvinceSubjectBO.SubjectID.ToString() + "]"] != "")
                    {
                        SectionPerWeekFirstSemester = SMAS.Business.Common.Utils.GetNullableDecimal(dic, "form[SectionPerWeekFirstSemester" + ProvinceSubjectBO.SubjectID.ToString() + "]");
                        SectionPerWeekSecondSemester = SMAS.Business.Common.Utils.GetNullableDecimal(dic, "form[SectionPerWeekSecondSemester" + ProvinceSubjectBO.SubjectID.ToString() + "]");
                    }
                    else
                    {
                        return Json(new JsonMessage(Res.Get("ProvinceSubject_Label_HasNoSection"), JsonMessage.ERROR));
                    }

                    ProvinceSubject.SubjectID = ProvinceSubjectBO.SubjectID.Value;
                    ProvinceSubject.ProvinceID = new GlobalInfo().ProvinceID.Value;
                    ProvinceSubject.AcademicYearOfProvinceID = AcademicYearOfProvinceID.Value;
                    ProvinceSubject.AppliedLevel = AppliedLevelID.Value;
                    ProvinceSubject.EducationLevelID = (int)EducationLevelID.Value;
                    ProvinceSubject.IsRegularEducation = IsRegularEducation.Value;
                    ProvinceSubject.Coefficient = Coefficient;
                    ProvinceSubject.AppliedType = AppliedType.Value;
                    ProvinceSubject.IsCommenting = IsCommenting.Value;
                    ProvinceSubject.SectionPerWeekFirstSemester = SectionPerWeekFirstSemester;
                    ProvinceSubject.SectionPerWeekSecondSemester = SectionPerWeekSecondSemester;
                    listProvinceSubject.Add(ProvinceSubject);
                }
            }

            if (listProvinceSubject.Count > 0)
            {
                ProvinceSubjectBusiness.Insert(listProvinceSubject);
                ProvinceSubjectBusiness.Save();
            }
            else {
                return Json(new JsonMessage(Res.Get("ProvinceSubject_Label_FailedSave"),JsonMessage.ERROR));
            }

            
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ProvinceSubjectID)
        {
            ProvinceSubject provincesubject = this.ProvinceSubjectBusiness.Find(ProvinceSubjectID);
            TryUpdateModel(provincesubject);
            Utils.Utils.TrimObject(provincesubject);
            this.ProvinceSubjectBusiness.Update(provincesubject);
            this.ProvinceSubjectBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.ProvinceSubjectBusiness.Delete(id);
            this.ProvinceSubjectBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ProvinceSubjectViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("ProvinceSubject", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<ProvinceSubject> query = this.ProvinceSubjectBusiness.Search(SearchInfo);
            IQueryable<ProvinceSubjectViewModel> lst = query.Select(o => new ProvinceSubjectViewModel {               
						ProvinceSubjectID = o.ProvinceSubjectID,								
						SubjectID = o.SubjectID,								
						ProvinceID = o.ProvinceID,								
						AcademicYearOfProvinceID = o.AcademicYearOfProvinceID,								
						AppliedLevel = o.AppliedLevel,								
						EducationLevelID = o.EducationLevelID,								
						IsRegularEducation = o.IsRegularEducation,								
						Coefficient = o.Coefficient,								
						AppliedType = o.AppliedType,								
						IsCommenting = o.IsCommenting,								
						SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,								
						SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester								
					
				
            });

            return lst.ToList();
        }        
    }
}





