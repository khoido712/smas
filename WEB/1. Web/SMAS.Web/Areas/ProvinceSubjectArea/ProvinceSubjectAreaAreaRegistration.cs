﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ProvinceSubjectArea
{
    public class ProvinceSubjectAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ProvinceSubjectArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ProvinceSubjectArea_default",
                "ProvinceSubjectArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
