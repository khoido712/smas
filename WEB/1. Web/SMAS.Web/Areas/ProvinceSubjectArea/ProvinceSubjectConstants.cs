/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ProvinceSubjectArea
{
    public class ProvinceSubjectConstants
    {
        public const string LIST_PROVINCESUBJECT = "listProvinceSubject";

        public const string LIST_SchoolAppliedLevel = "LIST_SchoolAppliedLevel";
        public const string LIST_EducationLevel = "LIST_EducationLevel";
        public const string LIST_IsCommenting = "LIST_IsCommenting";
        public const string LIST_AppliedType = "LIST_AppliedType";
        public const string LIST_AcademicYear = "LIST_AcademicYear";
        public const string LIST_AppliedLevel = "LIST_AppliedLevel";

        public const string Visible_chkRegularEducation = "Visible_chkRegularEducation";
        public const string Visible_ColCoefficient = "Visible_ColCoefficient";
        public const string Current_Year = "Current_Year";
    }
}