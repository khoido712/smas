﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EducationProgramArea
{
    public class EducationProgramConstants
    {
        public const string LIST_GRADE = "listGrade";
        public const string LIST_EDUCATION_LEVEL = "listEducationLevel";
        public const string LIST_SUBJECT_CAT = "listSubjectCat";
        public const string LIST_SUBJECT = "listSubject";
        public const string labelEducationProgram = "labelEducationProgram";
    }
}