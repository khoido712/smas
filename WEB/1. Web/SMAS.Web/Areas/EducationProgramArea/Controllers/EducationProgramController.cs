﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.EducationProgramArea.Models;

namespace SMAS.Web.Areas.EducationProgramArea.Controllers
{
    public class EducationProgramController:BaseController
    {
         private readonly IEducationProgramBusiness EducationProgramBusiness;
         private readonly IEducationLevelBusiness EducationLevelBusiness;
         private readonly ISubjectCatBusiness SubjectCatBusiness;
         private readonly ITrainingTypeBusiness TrainingTypeBusiness;

         public EducationProgramController(IEducationProgramBusiness educationProgramBusiness, IEducationLevelBusiness educationLevelBusiness, ISubjectCatBusiness 
             subjectCatBusiness, ITrainingTypeBusiness trainingTypeBusiness)
		{
            this.EducationProgramBusiness = educationProgramBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
		}
		
        public ActionResult Index()
        {
            int grade = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["Grade"] = grade;
            ViewData[EducationProgramConstants.labelEducationProgram] = Res.Get("EducationProgram_Label_AppliedLevel1Education");
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.Search(dic).ToList();
            ViewData[EducationProgramConstants.LIST_EDUCATION_LEVEL] = lstEducationLevel;
            List<ComboObject> listGrade = new List<ComboObject>();
            
                listGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString(), Res.Get("EducationProgram_AppliedLevel_Level1")));
                listGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), Res.Get("EducationProgram_AppliedLevel_Level2")));
                listGrade.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_TERTIARY.ToString(), Res.Get("EducationProgram_AppliedLevel_Level3")));
                SelectList selectGrade = new SelectList(listGrade, "key", "value");
                ViewData[EducationProgramConstants.LIST_GRADE] = selectGrade;


                List<EducationProgram> lstEduationProgram = new List<EducationProgram>();
                List<EducationProgramViewModel> lstEducationProgramViewModel = new List<EducationProgramViewModel>();
                List<SubjectCat> lstSubjectCat = new List<SubjectCat>();
                IDictionary<string, object> dic1 = new Dictionary<string, object>();
                dic1["IsActive"] = true;
                dic1["AppliedLevel"] = grade;

                lstSubjectCat = SubjectCatBusiness.Search(dic1).OrderBy(o=>o.OrderInSubject).ThenBy(o=>o.DisplayName).ToList();
                ViewData[EducationProgramConstants.LIST_SUBJECT] = lstSubjectCat;
                lstEduationProgram = EducationProgramBusiness.Search(dic).ToList();
                for (int i = 0; i < lstSubjectCat.Count(); i++)
                {
                    EducationProgramViewModel education = new EducationProgramViewModel();
                    education.SubjectCatID = lstSubjectCat[i].SubjectCatID;
                    education.SubjectName = lstSubjectCat[i].DisplayName;
                    education.NumberOfLessonData = new decimal?[lstEducationLevel.Count];
                    decimal?[] lstNumberOfLesson = new decimal?[lstEducationLevel.Count];
                    for (int j = 0; j < lstEducationLevel.Count; j++)
                    {
                        education.EducationLevelID = lstEducationLevel[j].EducationLevelID;
                        EducationProgram educationProgram = lstEduationProgram.Where(o => o.SubjectCatID == lstSubjectCat[i].SubjectCatID && o.EducationLevelID == lstEducationLevel[j].EducationLevelID).FirstOrDefault();
                        if (educationProgram != null)
                        {
                            education.NumberOfLessonData[j] = educationProgram.NumberOfLesson;
                        }
                    }
                  
                    lstEducationProgramViewModel.Add(education);
                }
                ViewData[EducationProgramConstants.LIST_SUBJECT_CAT] = lstEducationProgramViewModel;
                return View();           
        }
        
        public PartialViewResult Search(SearchViewModel frm)
        {
            string nameEdu = "";
            if (frm.Grade == 1)
            {
                nameEdu = Res.Get("EducationProgram_Label_AppliedLevel1Education");
            }
            if (frm.Grade == 2)
            {
                nameEdu = Res.Get("EducationProgram_Label_AppliedLevel2Education");
            }
            if (frm.Grade == 3 && frm.IsGDTX == true)
            {
                nameEdu = Res.Get("EducationProgram_Label_AppliedLevel3EducationTX");
            }
            if(frm.Grade == 3 && frm.IsGDTX == false)
            {
                nameEdu = Res.Get("EducationProgram_Label_AppliedLevel3Education");
            }
            ViewData[EducationProgramConstants.labelEducationProgram] = nameEdu;
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["Grade"] = frm.Grade;
           
            List<EducationLevel> lstEducationLevel = new List<EducationLevel>();
            lstEducationLevel = EducationLevelBusiness.Search(SearchInfo).ToList();
            ViewData[EducationProgramConstants.LIST_EDUCATION_LEVEL] = lstEducationLevel;
            //IEnumerable<EducationalManagementGradeViewModel> lst = this._Search(SearchInfo);
            //ViewData[EducationalManagementGradeConstants.LIST_EDUCATIONALMANAGEMENTGRADE] = lst;

            //Get view data here
            List<EducationProgram> lstEduationProgram = new List<EducationProgram>();
            List<EducationProgramViewModel> lstEducationProgramViewModel = new List<EducationProgramViewModel>();
            List<SubjectCat> lstSubjectCat = new List<SubjectCat>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            dic["AppliedLevel"] = frm.Grade;

            lstSubjectCat = SubjectCatBusiness.Search(dic).OrderBy(o=>o.OrderInSubject).ThenBy(o=>o.DisplayName).ToList();
            ViewData[EducationProgramConstants.LIST_SUBJECT] = lstSubjectCat;

            IDictionary<string, object> dicEdu = new Dictionary<string, object>();
            dicEdu["IsActive"] = true;
            dicEdu["AppliedLevel"] = frm.Grade;

            if (frm.IsGDTX == true)
            {
                TrainingType trainingType = new TrainingType();
                IDictionary<string, object> dicTrainingType = new Dictionary<string, object>();
                dicTrainingType["Resolution"] = Res.Get("TrainingType_Resolution_GDTX");
                trainingType = TrainingTypeBusiness.Search(dicTrainingType).FirstOrDefault();
                dicEdu["TrainingTypeID"] = trainingType.TrainingTypeID;
                lstEduationProgram = EducationProgramBusiness.Search(dicEdu).ToList();
            }

            else
            {
                lstEduationProgram = EducationProgramBusiness.Search(dicEdu).ToList();
                lstEduationProgram = lstEduationProgram.Where(o => o.TrainingTypeID == null).ToList();
            }
           
            for (int i = 0; i < lstSubjectCat.Count; i++)
            {
                EducationProgramViewModel education = new EducationProgramViewModel();
                education.SubjectCatID = lstSubjectCat[i].SubjectCatID;
                education.SubjectName = lstSubjectCat[i].DisplayName;
                education.NumberOfLessonData = new decimal?[lstEducationLevel.Count];
                decimal?[] lstNumberOfLesson = new decimal?[lstEducationLevel.Count];
                for (int j = 0; j < lstEducationLevel.Count; j++)
                {
                    education.EducationLevelID = lstEducationLevel[j].EducationLevelID;
                    //education.NumberOfLessonData[j] = null;
                    EducationProgram educationProgram = lstEduationProgram.Where(o => o.SubjectCatID == lstSubjectCat[i].SubjectCatID && o.EducationLevelID == lstEducationLevel[j].EducationLevelID).FirstOrDefault();
                    if (educationProgram != null)
                    {
                        education.NumberOfLessonData[j] = educationProgram.NumberOfLesson;
                    }
                }
                //for (int p = 0; p < lstNumberOfLesson.Count(); p++)
                //{
                //    education.NumberOfLessonData[p] = lstNumberOfLesson[p];
                //}
                lstEducationProgramViewModel.Add(education);
            }
            //foreach(SubjectCat subjectCat in lstSubjectCat)
            //{
               
            //    //Lay gia tri
                
            //    foreach (EducationLevel educationLevel in lstEducationLevel)
            //    {
            //        EducationProgramViewModel education = new EducationProgramViewModel();
            //        education.SubjectCatID = subjectCat.SubjectCatID;
            //        education.SubjectName = subjectCat.DisplayName;
            //        EducationProgram educationProgram = new EducationProgram();
            //        educationProgram = lstEduationProgram.Where(o => o.SubjectCatID == subjectCat.SubjectCatID && o.EducationLevelID == educationLevel.EducationLevelID).FirstOrDefault();
            //        if (educationProgram != null)
            //        {
            //            education.NumberOfLessonData = educationProgram.NumberOfLesson;
            //            education.EducationLevelID = educationProgram.EducationLevelID;
                      
            //        }
            //        lstEducationProgramViewModel.Add(education);

            //    }
              
            //}
            ViewData[EducationProgramConstants.LIST_SUBJECT_CAT] =  lstEducationProgramViewModel;
            return PartialView("_List");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Save(SearchViewModel frm, FormCollection form)
        {
            
            List<EducationProgram> lstEducationProgram = new List<EducationProgram>();
            List<EducationLevel> lstEducationLevel = new List<EducationLevel>();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["Grade"] = frm.Grade;
           
            lstEducationLevel = EducationLevelBusiness.Search(SearchInfo).ToList();
            List<SubjectCat> lstSubjectCat = new List<SubjectCat>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            dic["AppliedLevel"] = frm.Grade;

            lstSubjectCat = SubjectCatBusiness.Search(dic).ToList();
            //lstEduationProgram = EducationProgramBusiness.Search(dic).ToList();
            int TrainingTypeID = 0;
            foreach (SubjectCat subjectCat in lstSubjectCat)
            {
               
                //Lay gia tri
                foreach (EducationLevel educationLevel in lstEducationLevel)
                {
                    EducationProgram education = new EducationProgram();
                    education.SubjectCatID = subjectCat.SubjectCatID;
                    education.AppliedLevel = frm.Grade;
                    education.IsActive = true;
                        education.EducationLevelID = educationLevel.EducationLevelID;
                        education.NumberOfLesson = GetMarkEducationProgram(educationLevel.EducationLevelID, subjectCat.SubjectCatID, form);
                        if (frm.IsGDTX == true)
                        {
                            TrainingType trainingType = new TrainingType();
                            IDictionary<string, object> dicTrainingType = new Dictionary<string, object>();
                            dicTrainingType["Resolution"] = Res.Get("TrainingType_Resolution_GDTX");
                            trainingType = TrainingTypeBusiness.Search(dicTrainingType).FirstOrDefault();
                            education.TrainingTypeID = trainingType.TrainingTypeID;
                            TrainingTypeID = trainingType.TrainingTypeID;
                        }
                    if (education.NumberOfLesson > 0)
                        {
                            lstEducationProgram.Add(education);
                        }
                }
              
            }
            if (lstEducationProgram.Count() > 0)
            {
                this.EducationProgramBusiness.Insert(frm.Grade, TrainingTypeID , lstEducationProgram);
                this.EducationProgramBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Error_Parameter")), JsonMessage.ERROR);
            }
        }

        public static decimal? GetMarkEducationProgram(int EducationLevelID, int SubjectID, FormCollection form)
        {
            string markEducationProgram = form.Get("NumberOfLesson_" + EducationLevelID + "_" + SubjectID);

            if (!string.IsNullOrEmpty(markEducationProgram))
                return Convert.ToDecimal(markEducationProgram);

            return 0;
        }

    }
}