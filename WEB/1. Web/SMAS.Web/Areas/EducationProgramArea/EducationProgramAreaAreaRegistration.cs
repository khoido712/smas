﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EducationProgramArea
{
    public class EducationProgramAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EducationProgramArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EducationProgramArea_default",
                "EducationProgramArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
