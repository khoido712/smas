﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.EducationProgramArea.Models
{
    public class EducationProgramViewModel
    {
        public int EducationLevelID { get; set; }
        public int SubjectCatID { get; set; }

        [ResourceDisplayName("EducationProgram_Label_SubjectName")]
        public string SubjectName { get; set; }

        public decimal?[] NumberOfLessonData { get; set; }

    }
}