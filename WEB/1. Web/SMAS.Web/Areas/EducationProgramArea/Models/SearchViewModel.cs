﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.EducationProgramArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("EducationProgram_Label_AppliedLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", EducationProgramConstants.LIST_GRADE)]
        [AdditionalMetadata("OnChange", "search()")]
        public int Grade { get; set; }

       
        [ResourceDisplayName("EducationProgram_Label_IsGDTX")]
        [AdditionalMetadata("OnChange", "search()")]
        public System.Boolean IsGDTX { get; set; }
    }
}