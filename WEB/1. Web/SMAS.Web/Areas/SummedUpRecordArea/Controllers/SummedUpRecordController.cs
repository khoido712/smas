﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.SummedUpRecordArea.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.Transactions;
using System.Configuration;

namespace SMAS.Web.Areas.SummedUpRecordArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class SummedUpRecordController : BaseController
    {
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly ISummedUpRecordClassBusiness SummedUpRecordClassBusiness;
        private readonly IClassMovementBusiness ClassMovementBusiness;
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IPupilLeavingOffBusiness PupilLeavingOffBusiness;
        private readonly IConductConfigByCapacityBusiness ConductConfigByCapacityBu;
        private readonly IConductConfigByViolationBusiness ConductConfigByViolationBu;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        private readonly IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness;
        private readonly IPupilRankingHistoryBusiness PupilRankingHistoryBusiness;
        private readonly IVPupilRankingBusiness VPupilRankingBusiness;

        public SummedUpRecordController(ISummedUpRecordBusiness summedUpRecordBusiness, IPeriodDeclarationBusiness PeriodDeclarationBusiness,
            IClassProfileBusiness ClassProfileBusiness, IClassSubjectBusiness ClassSubjectBusiness, IReportDefinitionBusiness ReportDefinitionBusiness,
            IPupilRankingBusiness PupilRankingBusiness, IExemptedSubjectBusiness ExemptedSubjectBusiness, ISchoolProfileBusiness SchoolProfileBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness, IAcademicYearBusiness AcademicYearBusiness, IMarkRecordBusiness MarkRecordBusiness,
            ISummedUpRecordClassBusiness SummedUpRecordClassBusiness, IClassMovementBusiness ClassMovementBusiness,
                                    ISchoolMovementBusiness SchoolMovementBusiness, IConductConfigByCapacityBusiness conductConfigByCapacityBu,
                                    IPupilLeavingOffBusiness PupilLeavingOffBusiness, IConductConfigByViolationBusiness conductConfigByViolationBu,
            IPupilProfileBusiness PupilProfileBusiness, ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness, IRegisterSubjectSpecializeBusiness registerSubjectSpecializeBusiness,
            IPupilRankingHistoryBusiness PupilRankingHistoryBusiness,
            IVPupilRankingBusiness VPupilRankingBusiness

            )
        {
            this.SummedUpRecordBusiness = summedUpRecordBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.PupilRankingBusiness = PupilRankingBusiness;
            this.ExemptedSubjectBusiness = ExemptedSubjectBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.SummedUpRecordClassBusiness = SummedUpRecordClassBusiness;
            this.ClassMovementBusiness = ClassMovementBusiness;
            this.SchoolMovementBusiness = SchoolMovementBusiness;
            this.PupilLeavingOffBusiness = PupilLeavingOffBusiness;
            this.ConductConfigByCapacityBu = conductConfigByCapacityBu;
            this.ConductConfigByViolationBu = conductConfigByViolationBu;
            this.PupilProfileBusiness = PupilProfileBusiness;
            this.SummedUpRecordHistoryBusiness = SummedUpRecordHistoryBusiness;
            this.RegisterSubjectSpecializeBusiness = registerSubjectSpecializeBusiness;
            this.PupilRankingHistoryBusiness = PupilRankingHistoryBusiness;
            this.VPupilRankingBusiness = VPupilRankingBusiness;
        }

        //
        // GET: /SummedUpRecord/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            bool semester1 = false;
            bool semester2 = false;
            if (new GlobalInfo().Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                semester1 = true;
            else semester2 = true;
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("Semester_Of_Year_First"), SystemParamsInFile.SEMESTER_OF_YEAR_FIRST, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("Semester_Of_Year_Second"), SystemParamsInFile.SEMESTER_OF_YEAR_SECOND, semester2, false));
            ViewData[SummedUpRecordConstants.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();


            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[SummedUpRecordConstants.LIST_EDUCATION_LEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            if (semester1)
            {
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
            }
            else if (semester2)
            {
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            }

            IEnumerable<PeriodDeclaration> listPeriod = PeriodDeclarationBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();
            foreach (PeriodDeclaration item in listPeriod)
            {
                item.Resolution = item.Resolution + " (" + Res.Get("Common_Label_From") + " "
                    + item.FromDate.Value.ToShortDateString() + " " + Res.Get("Common_Label_To") + " "
                    + item.EndDate.Value.ToShortDateString() + ")";
            }
            int period = 0;
            if (listPeriod != null && listPeriod.Count() != 0)
            {
                period = listPeriod.FirstOrDefault().PeriodDeclarationID;
            }
            foreach (PeriodDeclaration pd in listPeriod)
            {
                if (DateTime.Now >= pd.FromDate && DateTime.Now <= pd.EndDate)
                {
                    period = pd.PeriodDeclarationID;
                }
            }
            ViewData[SummedUpRecordConstants.LIST_PERIOD] = new SelectList(listPeriod, "PeriodDeclarationID", "Resolution", period);

            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
                dic["EducationLevelID"] = listEducationLevel.FirstOrDefault().Value;
                if (new GlobalInfo().IsAdminSchoolRole == false)
                {
                    dic["UserAccountID"] = new GlobalInfo().UserAccountID;
                    dic["Type"] = SMAS.Web.Constants.GlobalConstants.TEACHER_ROLE_HEADTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).ToList();
                ViewData[SummedUpRecordConstants.LIST_CLASS] = listClass;
            }
            // disable button xep hang
            ViewData[SummedUpRecordConstants.ENABLE_RANK] = false;
            return View();
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult GetcboPeriod(int? SemesterID)
        {
            if (SemesterID.HasValue)
            {
                DateTime startDate = new DateTime();
                DateTime endDate = new DateTime();
                DateTime nowDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                int PeriodDeclarationIDSelected = 0;
                //Load combobox lop hoc theo khoi hoc
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
                dic["Semester"] = SemesterID.Value;
                IEnumerable<PeriodDeclaration> listPeriod = PeriodDeclarationBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();
                if (listPeriod != null && listPeriod.Count() > 0)
                {
                    PeriodDeclarationIDSelected = listPeriod.FirstOrDefault().PeriodDeclarationID;
                    foreach (PeriodDeclaration item in listPeriod)
                    {
                        item.Resolution = item.Resolution + " (" + Res.Get("Common_Label_From") + " "
                            + item.FromDate.Value.ToShortDateString() + " " + Res.Get("Common_Label_To") + " "
                            + item.EndDate.Value.ToShortDateString() + ")";
                        startDate = new DateTime(item.FromDate.Value.Year, item.FromDate.Value.Month, item.FromDate.Value.Day, 0, 0, 0);
                        endDate = new DateTime(item.EndDate.Value.Year, item.EndDate.Value.Month, item.EndDate.Value.Day, 0, 0, 0);
                        if (startDate <= nowDate && nowDate <= endDate)
                        {
                            PeriodDeclarationIDSelected = item.PeriodDeclarationID;
                        }
                    }
                }
                return Json(new SelectList(listPeriod, "PeriodDeclarationID", "Resolution", PeriodDeclarationIDSelected));
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult GetClassPanel(int? id)
        {
            if (id.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
                dic["EducationLevelID"] = id.Value;
                if (new GlobalInfo().IsAdminSchoolRole == false)
                {
                    dic["UserAccountID"] = new GlobalInfo().UserAccountID;
                    dic["Type"] = SMAS.Web.Constants.GlobalConstants.TEACHER_ROLE_HEADTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).ToList();
                ViewData[SummedUpRecordConstants.LIST_CLASS] = listClass;


            }
            return PartialView("_ListClass");
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGridData(int? classId, int? semesterId, int? periodId)
        {
            int AcademicYearID = _globalInfo.AcademicYearID.Value;
            int SchoolID = _globalInfo.SchoolID.Value;
            if (classId.HasValue && classId > 0 && semesterId.HasValue && periodId.HasValue)
            {
                int ClassID = classId.Value;
                IDictionary<string, object> dicMovement = new Dictionary<string, object>();
                dicMovement["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicMovement["ClassID"] = classId.Value;

                PeriodDeclaration Period = PeriodDeclarationBusiness.Find(periodId.Value);
                DateTime? endSemester = Period.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? Period.AcademicYear.FirstSemesterEndDate : Period.AcademicYear.SecondSemesterEndDate;

                // DungVA 07/05
                string className = ClassProfileBusiness.Find(ClassID).DisplayName;
                ViewData[SummedUpRecordConstants.TITLE_PAGE] = Res.Get("PupilRankingSemester_List") + " học kỳ " + semesterId + " " + Period.Resolution + " (" + Period.FromDate.Value.ToShortDateString() + " - " + Period.EndDate.Value.ToShortDateString() + ")" + " lớp " + className;
                //End DungVA

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = classId.Value;
                // Lay danh sach trong lop
                List<PupilOfClassBO> ListPupilOfClass = PupilOfClassBusiness.GetPupilInClass(classId.Value)
                    .ToList()
                    .OrderBy(o => o.OrderInClass)
                    .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.Name))
                    .ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.PupilFullName)).ToList();

                foreach (PupilOfClassBO poc in ListPupilOfClass)
                {
                    poc.isShow = poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED;

                    // Neu khac 2 trang thai tren
                    if (poc.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                        || poc.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL
                        || poc.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF
                        )
                    {
                        poc.isShow = poc.EndDate.HasValue && endSemester.HasValue && poc.EndDate > endSemester;
                    }
                }

                ViewData[SummedUpRecordConstants.LIST_PUPIL_CLASS] = ListPupilOfClass;

                // Lay danh sach mon hoc
                dic["AcademicYearID"] = AcademicYearID;
                dic["IsApprenticeShipSubject"] = false;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                if (semesterId != SystemParamsInFile.SEMESTER_OF_YEAR_ALL) dic["Semester"] = semesterId;

                IQueryable<ClassSubject> iqCS = ClassSubjectBusiness.SearchBySchool(SchoolID, dic);
                //Loc ra mon tang cuong
                List<int?> lstInCreaseID = iqCS.Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease > 0).Select(p => p.SubjectIDIncrease).ToList();
                iqCS = iqCS.Where(p => !lstInCreaseID.Contains(p.SubjectID));

                List<ClassSubjectBO> ListClassSubject = (from o in iqCS
                                                         select new ClassSubjectBO
                                                         {
                                                             ClassID = o.ClassID,
                                                             SubjectID = o.SubjectID,
                                                             DisplayName = o.SubjectCat.DisplayName,
                                                             OrderInSubject = o.SubjectCat.OrderInSubject,
                                                             SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                                                             SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                                                             IsCommenting = o.IsCommenting,
                                                             AppliedType = o.AppliedType
                                                         }).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                ViewData[SummedUpRecordConstants.LIST_SUBJECT] = ListClassSubject;

                // Pupil ranking
                dic["SchoolID"] = SchoolID;
                dic["Semester"] = semesterId.Value;
                dic["PeriodID"] = periodId.Value;
                List<PupilOfClassRanking> LstPupilRanking = PupilRankingBusiness.GetPupilRankingOfClass(AcademicYearID, SchoolID, (int)semesterId.Value, ClassID, periodId).ToList();
                LstPupilRanking = LstPupilRanking.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.FullName).ToList();
                ViewData[SummedUpRecordConstants.LIST_PUPIL_RANKING] = LstPupilRanking;

                AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

                bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);

                List<SummedUpRecordBO> ListSummedUpRecord = new List<SummedUpRecordBO>();
                // Lay thong tin tong ket diem cua hoc sinh                
                //   List<SummedUpRecordBO> ListSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecord(AcademicYearID, SchoolID, (int)semesterId.Value, ClassID, periodId).ToList();
                if (!isMovedHistory)
                {
                    ListSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecordCurrentYear(AcademicYearID, SchoolID, (int)semesterId.Value, ClassID, periodId, false).ToList();
                }
                else
                {
                    ListSummedUpRecord = SummedUpRecordHistoryBusiness.GetSummedUpRecordHistory(AcademicYearID, SchoolID, (int)semesterId.Value, ClassID, periodId, false).ToList();
                }
                ListSummedUpRecord = ListSummedUpRecord.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.PupilFullName).ToList();
                ViewData[SummedUpRecordConstants.LIST_SUMMEDUPRECORD] = ListSummedUpRecord;

                List<ExemptedSubject> listExempteds = ExemptedSubjectBusiness.GetListExemptedSubject(classId.Value, semesterId.Value).ToList();
                // Lay them thong tin hien thi ve mon hoc mien giam
                Dictionary<string, bool> dicExemptedSubject = new Dictionary<string, bool>();
                foreach (PupilOfClassBO p in ListPupilOfClass)
                {
                    foreach (ClassSubjectBO cs in ListClassSubject)
                    {
                        dicExemptedSubject[p.PupilID + "_" + cs.SubjectID] = listExempteds.Any(u => u.PupilID == p.PupilID && u.SubjectID == cs.SubjectID);
                    }
                }

                ViewData[SummedUpRecordConstants.DIC_EXEMPTED_SUBJECT] = dicExemptedSubject;

                ClassProfile ClassProfile = ClassProfileBusiness.Find(ClassID);

                if (Period != null && ClassProfile != null)
                {
                    string periodInfo = Period.Resolution + "(" + Period.FromDate.Value.ToString("dd/MM/yyyy") + "-" + Period.EndDate.Value.ToString("dd/MM/yyyy") + ")";
                    ViewData[SummedUpRecordConstants.TITLE] = string.Format(Res.Get("SummedUpRecord_Label_Message"), semesterId, periodInfo, ClassProfile.DisplayName).ToUpper();
                }

                Session["ListPupilOfClass"] = ListPupilOfClass;
                Session["ListClassSubject"] = ListClassSubject;
                Session["LstPupilRanking"] = LstPupilRanking;
                Session["ListSummedUpRecord"] = ListSummedUpRecord;
                Session["ClassID"] = ClassID;
                Session["semesterId"] = semesterId;
                Session["periodId"] = periodId;

                // Kiem tra xem neu da tong ket roi thi enable button Xep hang
                bool? islock = Period.IsLock;
                // Kiem tra quyen cua user dang nhap he thong truoc
                // Sử dụng để check disable/enable button Xếp hạng
                int countRank = LstPupilRanking.Where(o => o.CapacityLevelID.HasValue).Count();
                if (_globalInfo.IsAdminSchoolRole)
                {
                    if (_globalInfo.IsCurrentYear)
                    {
                        ViewData[SummedUpRecordConstants.ENABLE_SUMMEDUP] = true;
                        ViewData[SummedUpRecordConstants.ENABLE_RANK] = true;
                    }
                    else
                    {
                        ViewData[SummedUpRecordConstants.ENABLE_SUMMEDUP] = false;
                        ViewData[SummedUpRecordConstants.ENABLE_RANK] = false;
                    }
                }
                else
                {
                    bool check = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, ClassID);
                    if (check == false)
                    {
                        ViewData[SummedUpRecordConstants.ENABLE_SUMMEDUP] = false;
                        ViewData[SummedUpRecordConstants.ENABLE_RANK] = false;
                    }
                    else
                    {
                        // Kiem tra con diem LHK da khoa het cac mon hay chua
                        int countLockedSubject = 0;
                        Dictionary<int, string> dicLockedMark = MarkRecordBusiness.GetLockMarkTitleBySubject(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID, (int)semesterId.Value);
                        foreach (var subject in ListClassSubject)
                        {
                            string strSubjectMarkTitle = dicLockedMark.ContainsKey(subject.SubjectID) ? dicLockedMark[subject.SubjectID] : "";
                            if (strSubjectMarkTitle.Contains(SystemParamsInFile.MARK_TYPE_LHK)) countLockedSubject++;
                        }
                        bool timePeriod = Period != null && (Period.FromDate <= DateTime.Now && DateTime.Now.Date <= Period.EndDate.Value.Date) && (countLockedSubject < ListClassSubject.Count);
                        if (timePeriod && (islock == false || (islock == true && _globalInfo.IsAdminSchoolRole)))
                        {
                            ViewData[SummedUpRecordConstants.ENABLE_SUMMEDUP] = true;
                            ViewData[SummedUpRecordConstants.ENABLE_RANK] = true;
                        }
                        else
                        {
                            ViewData[SummedUpRecordConstants.ENABLE_SUMMEDUP] = false;
                            ViewData[SummedUpRecordConstants.ENABLE_RANK] = false;
                        }
                    }
                }
                // Kiem tra xem neu chua co hanh kiem thi an button xep hang
                if (countRank == 0)
                {
                    ViewData[SummedUpRecordConstants.ENABLE_RANK] = true;
                }
                //TungNT48 start 30/3/2013
                // ENABLE/DISABLE button
                ViewData[SummedUpRecordConstants.ENABLE_EXCEL] = true;
                if (ListSummedUpRecord.Count() == 0 || ListClassSubject == null || ListClassSubject.Count() == 0)
                {
                    ViewData[SummedUpRecordConstants.ENABLE_EXCEL] = false;
                    ViewData[SummedUpRecordConstants.ENABLE_SUMMEDUP] = false;
                    ViewData[SummedUpRecordConstants.ENABLE_RANK] = false;
                }
                // end
            }
            else
            {
                ViewData["NotChooseClassYet"] = true;
                ViewData[SummedUpRecordConstants.TITLE_PAGE] = string.Empty;
            }

            AcademicYear academicYear = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID);

            // Lấy thông tin cấu hình xếp loại hạnh kiểm
            int rankingType = academicYear.ConductEstimationType; // GetRankingType();
            // gan gia tri mac dinh cho rankingType la xep loai theo hoc luc
            rankingType = rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_VIOLATION || rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY ? rankingType : SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY;

            /* Nếu cấu hình xếp loại hạnh kiểm theo vi phạm (rankingType = 2) hoặc cấu hình xếp loại hạnh kiểm theo học lực (rankingType = 1) và 
                không chọn tiêu chí xếp hạng theo hạnh kiểm thì button Tổng kết sẽ thực hiện cả nhiệm vụ tổng kết lẫn xếp hạng */
            if (rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_VIOLATION
                || (rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY
                    && (academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_MARK
                        || academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_CAPACITY
                        || academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY)))
            {
                ViewData[SummedUpRecordConstants.ENABLE_RANK] = false;
            }

            //lấy danh sách học sinh đăng ký môn chuyên môn tự chọn
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classId},
                {"SemesterID",semesterId},
                {"YearID",academicYear.Year}
            };
            ViewData[SummedUpRecordConstants.LIST_REGISTER_SUBJECT] = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegis);

            return PartialView("_List");
        }
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult SummarizeMark(int? ClassID, int? Semester, int? PeriodID, string[] ArrPupilRanking)
        {
            int AcademicYearID = _globalInfo.AcademicYearID.Value;
            AcademicYear AcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            int SchoolID = _globalInfo.SchoolID.Value;
            int ClassIDParam = ClassID.Value;
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassIDParam);

            SummedUpRecordClass objSummedUpRecordClass = new SummedUpRecordClass
            {
                AcademicYearID = AcademicYearID,
                ClassID = ClassID.Value,
                PeriodID = PeriodID,
                Semester = Semester.Value,
                CreatedDate = DateTime.Now,
                EducationLevelID = classProfile.EducationLevelID,
                SchoolID = AcademicYear.SchoolID,
                NumberOfPupil = ArrPupilRanking.Length,
                Type = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1,
                Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING,
                SynchronizeID = 0,
            };
            //12.02.2018 Kiem tra lop co dang thuc hien xep loai HK, tong ket, xep loai khong

            if (SummedUpRecordClassBusiness.CheckExistsSummedExcuting(objSummedUpRecordClass))
            {
                return Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_Summeding"), JsonMessage.ERROR));
            }
            //Danh dau lop dang thuc hien tinh tong ket, xep loai
            SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);
            // Lay danh sach mon hoc cua lop
            IDictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject["SchoolID"] = SchoolID;
            IQueryable<ClassSubject> iqCS = ClassSubjectBusiness.SearchByClass(ClassIDParam, dicClassSubject);
            //Loc ra mon tang cuong
            List<int?> lstInCreaseID = iqCS.Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease.Value > 0).Select(p => p.SubjectIDIncrease).ToList();
            iqCS = iqCS.Where(p => !lstInCreaseID.Contains(p.SubjectID));
            var listClassSubjectCat = (from o in iqCS
                                       select new
                                       {
                                           cs = o,
                                           subjectCat = o.SubjectCat
                                       }).ToList();
            List<ClassSubject> listClassSubject = listClassSubjectCat.Select(o => o.cs).ToList();
            // Mon hoc
            List<SubjectCat> listSubject = listClassSubjectCat.Select(o => o.subjectCat).ToList();
            // Lay thong tin tong ket diem cua hoc sinh                      

            bool isMovedHistory = UtilsBusiness.IsMoveHistory(AcademicYear);

            List<SummedUpRecordBO> ListSummedUpRecordBO = new List<SummedUpRecordBO>();
            if (!isMovedHistory)
            {
                ListSummedUpRecordBO = SummedUpRecordBusiness.GetSummedUpRecordCurrentYear(AcademicYearID, SchoolID, Semester.Value, ClassIDParam, PeriodID, false).ToList();
            }
            else
            {
                ListSummedUpRecordBO = SummedUpRecordHistoryBusiness.GetSummedUpRecordHistory(AcademicYearID, SchoolID, Semester.Value, ClassIDParam, PeriodID, false).ToList();
            }

            //lay thong tin hoc sinh dang ky mon chuyen mon tu chon
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                //{"SemesterID",Semester},
                {"YearID",AcademicYear.Year}
            };
            List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegis);

            //Chiendd: 09/02/2015. Sua loi khong lay duoc mon hoc co diem TBM=0;
            List<SummedUpRecord> ListSummedUpRecord = ListSummedUpRecordBO.Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                                                && ((u.SubjectTypeID != GlobalConstants.APPLIED_SUBJECT_ELECTIVE && u.SubjectTypeID != GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                                                                                && u.SubjectTypeID != GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE) || lstRegisterSubjectBO.Where(p => p.PupilID == u.PupilID && p.SubjectID == u.SubjectID && p.SubjectTypeID == u.SubjectTypeID && p.SemesterID == Semester).Count() > 0)
                                                                                && (u.SummedUpMark >= 0 || u.ReTestMark >= 0 || !string.IsNullOrEmpty(u.JudgementResult) || !string.IsNullOrEmpty(u.ReTestJudgement))
                                                                                && !lstInCreaseID.Contains(u.SubjectID))
                                                                           .Select(o => new SummedUpRecord
                                                                           {
                                                                               SummedUpRecordID = o.SummedUpRecordID == null ? 0 : o.SummedUpRecordID.Value,
                                                                               PupilID = o.PupilID,
                                                                               SchoolID = o.SchoolID.Value,
                                                                               ClassID = o.ClassID,
                                                                               AcademicYearID = o.AcademicYearID,
                                                                               Semester = o.Semester,
                                                                               PeriodID = o.PeriodID == null ? 0 : o.PeriodID.Value,
                                                                               SubjectID = o.SubjectID == null ? 0 : o.SubjectID.Value,
                                                                               SubjectCat = o.SubjectID == null ? null : listSubject.Find(s => s.SubjectCatID == o.SubjectID),
                                                                               SummedUpMark = o.SummedUpMark,
                                                                               JudgementResult = o.JudgementResult,
                                                                               ReTestJudgement = o.ReTestJudgement,
                                                                               ReTestMark = o.ReTestMark,
                                                                               IsCommenting = o.IsCommenting == null ? 0 : o.IsCommenting.Value,
                                                                               CreatedAcademicYear = o.Year == null ? 0 : o.Year.Value,

                                                                           }).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = ClassID.Value;
            dic["Check"] = "check";
            // Lay danh sach trong lop
            List<PupilOfClass> ListPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dic).ToList();

            // Lay thong tin cua xep hang hoc sinh
            List<PupilRanking> ListPupilRanking = new List<PupilRanking>();
            foreach (string rankingInfo in ArrPupilRanking)
            {
                string[] rankInfoSplit = rankingInfo.Split('_');
                int pupilID = int.Parse(rankInfoSplit[0]);

                // Chi tong ket doi voi cac hoc sinh o trang thai dang hoc
                IEnumerable<PupilOfClass> tempList = ListPupilOfClass.Where(p => p.PupilID == pupilID);
                DateTime? maxAssignedDate = tempList.Max(t => t.AssignedDate);
                PupilOfClass poc = tempList.Where(p => p.AssignedDate == maxAssignedDate).FirstOrDefault();
                if (poc == null)
                {
                    continue;
                }
                if (poc.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                    || poc.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL
                    || poc.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF)
                {
                    continue;
                }
                PupilRanking PupilRanking = new PupilRanking();
                PupilRanking.PupilID = pupilID;
                PupilRanking.AcademicYearID = _globalInfo.AcademicYearID.Value;
                PupilRanking.SchoolID = _globalInfo.SchoolID.Value;
                decimal? AverageMark = null;
                if (rankInfoSplit[1] != null && rankInfoSplit[1] != "")
                {
                    AverageMark = decimal.Parse(rankInfoSplit[1]);
                }
                PupilRanking.AverageMark = AverageMark;
                if (rankInfoSplit[2] != null && rankInfoSplit[2] != "")
                {
                    PupilRanking.CapacityLevelID = int.Parse(rankInfoSplit[2]);
                }
                if (rankInfoSplit[3] != null && rankInfoSplit[3] != "")
                {
                    PupilRanking.ConductLevelID = int.Parse(rankInfoSplit[3]);
                }
                PupilRanking.ClassID = ClassIDParam;
                PupilRanking.EducationLevelID = classProfile.EducationLevelID;
                PupilRanking.Semester = Semester.Value;
                PupilRanking.PeriodID = PeriodID;
                if (rankInfoSplit[4] != null && rankInfoSplit[4] != "")
                {
                    PupilRanking.Rank = int.Parse(rankInfoSplit[4]);
                }
                PupilRanking.CreatedAcademicYear = AcademicYear.Year;

                ListPupilRanking.Add(PupilRanking);
            }

            //Tamhm1
            //Date: 07/05/2013
            #region
            //Môn học của lớp lấy theo kỳ
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                listClassSubject = listClassSubject.Where(o => o.SectionPerWeekFirstSemester > 0 && o.SubjectCat.IsApprenticeshipSubject == false).ToList();
            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                listClassSubject = listClassSubject.Where(o => o.SectionPerWeekSecondSemester > 0 && o.SubjectCat.IsApprenticeshipSubject == false).ToList();
            }
            #endregion


            List<ExemptedSubject> lstExemptedSubject = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID.Value, Semester.Value).ToList();

            // Lay thong tin nam hoc
            AcademicYear academicYear = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID);
            // Lấy thông tin cấu hình xếp loại hạnh kiểm
            int rankingType = academicYear.ConductEstimationType; //GetRankingType();
                                                                  // gan gia tri mac dinh cho rankingType la xep loai theo hoc luc
                                                                  //rankingType = rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_VIOLATION || rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY ? rankingType : SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY;

            // Lấy thông tin cấu hình tiêu chí xếp hạng

            JsonResult message = null;
            /* Nếu cấu hình xếp loại hạnh kiểm theo vi phạm (rankingType = 2) hoặc cấu hình xếp loại hạnh kiểm theo học lực (rankingType = 1) và 
                không chọn tiêu chí xếp hạng theo hạnh kiểm thì button Tổng kết sẽ thực hiện cả nhiệm vụ tổng kết lẫn xếp hạng */
            if (rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_VIOLATION
                || (rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY
                    && (academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_MARK
                        || academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_CAPACITY
                        || academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY)))
            {
                #region Tổng kết sẽ thực hiện cả nhiệm vụ xếp hạng
                try
                {
                    if (isMovedHistory)
                    {
                        this.PupilRankingHistoryBusiness.RankingPupilOfClassHistory(_globalInfo.UserAccountID, ListSummedUpRecord, ListPupilRanking, PeriodID, listClassSubject, false, GlobalConstants.NOT_AUTO_MARK, lstRegisterSubjectBO);
                    }
                    else
                    {
                        this.PupilRankingBusiness.RankingPupilOfClass(_globalInfo.UserAccountID, ListSummedUpRecord, ListPupilRanking, PeriodID, listClassSubject, false, GlobalConstants.NOT_AUTO_MARK, lstRegisterSubjectBO);
                    }
                }
                finally
                {
                    SummedUpRecordClass summedUpRecordClass = new SummedUpRecordClass
                    {
                        AcademicYearID = AcademicYearID,
                        Semester = Semester.Value,
                        PeriodID = PeriodID,
                        ClassID = ClassIDParam,
                        SchoolID = SchoolID,
                        Type = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1,
                        EducationLevelID = classProfile.EducationLevelID,
                        Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE,
                        SynchronizeID = 0,
                        NumberOfPupil = ListPupilOfClass.Count,

                    };
                    SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(summedUpRecordClass);
                }
                #endregion

                message = Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_SumupSuccess")));
            }
            // Nếu cấu hình xếp loại hạnh kiểm theo học lực (rankingType = 1) và Cấu hình tiêu chí xếp hạng có chọn tiêu chí Hạnh kiểm thì sau khi tổng kết xong,
            // enable button xếp hạng và hiển thị message nhắc nhở người dùng xếp loại hạnh kiểm trước khi xếp hạng.
            else if (rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY
                && (academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CONDUCT
                    || academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_CONDUCT
                    || academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY_CONDUCT))
            {
                #region Chỉ thực hiện tổng kết
                try
                {
                    if (isMovedHistory)
                    {
                        this.PupilRankingHistoryBusiness.RankingPupilOfClassNotRankHistory(_globalInfo.UserAccountID, ListSummedUpRecord, ListPupilRanking, PeriodID, listClassSubject, false, lstRegisterSubjectBO);
                    }
                    else
                    {
                        this.PupilRankingBusiness.RankingPupilOfClassNotRank(_globalInfo.UserAccountID, ListSummedUpRecord, ListPupilRanking, PeriodID, listClassSubject, false, lstRegisterSubjectBO);
                    }
                }
                finally
                {
                    SummedUpRecordClass summedUpRecordClass = new SummedUpRecordClass
                    {
                        AcademicYearID = AcademicYearID,
                        Semester = Semester.Value,
                        PeriodID = PeriodID,
                        ClassID = ClassIDParam,
                        SchoolID = SchoolID,
                        Type = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1,
                        Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE,
                        EducationLevelID = classProfile.EducationLevelID,
                        NumberOfPupil = ListPupilOfClass.Count
                    };
                    SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(summedUpRecordClass);
                }



                #endregion

                message = Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_SumupRankWarning")));
            }
            else
            {
                objSummedUpRecordClass.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE;
                objSummedUpRecordClass.NumberOfPupil = ListPupilRanking.Count;
                SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);
                message = Json("Trường chưa cấu hình xếp loại hạnh kiểm.");
            }
            return message;

        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult Rank(int ClassID, int Semester, int PeriodID)
        {
            SummedUpRecordClass objSummedUpRecordClass = new SummedUpRecordClass();
            try
            {
                AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

                // Lay cau hinh xep loai hanh kiem
                int rankingType = academicYear.ConductEstimationType; //GetRankingType();
                                                                      // gan gia tri mac dinh cho rankingType la xep loai theo hoc luc
                rankingType = rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_VIOLATION || rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY ? rankingType : SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY;

                /* Nếu cấu hình xếp loại hạnh kiểm theo vi phạm (rankingType = 1) hoặc cấu hình xếp loại hạnh kiểm theo học lực (rankingType = 0) và 
                    không chọn tiêu chí xếp hạng theo hạnh kiểm thì button Tổng kết sẽ thực hiện cả nhiệm vụ tổng kết lẫn xếp hạng */
                if (rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_VIOLATION
                    || (rankingType == SMAS.Web.Constants.GlobalConstants.CONDUCT_ESTIMATION_TYPE_CAPACITY
                        && (academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_MARK
                            || academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_CAPACITY
                            || academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY)))
                {
                    // Neu khong phai cau hinh xep hang co lien quan den hanh kiem thi khong cho thuc hien chuc nang nay           
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //Chiendd1: 01/10/2015, bo sung nhan xet hanh kiem
                // Lay danh sach hoc sinh da tong ket de xep hang
                List<PupilOfClassRanking> lstPupilRanking = PupilRankingBusiness.GetPupilRankingOfClass(_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, Semester, ClassID, PeriodID).ToList();
                List<PupilRanking> lstPupil = (from lst in lstPupilRanking
                                               where lst.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING && lst.PupilRankingID.HasValue
                                               select new PupilRanking
                                               {
                                                   PupilRankingID = lst.PupilRankingID.Value,
                                                   PupilID = lst.PupilID,
                                                   ClassID = lst.ClassID.Value,
                                                   SchoolID = lst.SchoolID,
                                                   AcademicYearID = lst.AcademicYearID,
                                                   CreatedAcademicYear = lst.Year == null ? 0 : lst.Year.Value,
                                                   Last2digitNumberSchool = lst.SchoolID % 100,
                                                   Semester = lst.Semester,
                                                   AverageMark = lst.AverageMark,
                                                   CapacityLevelID = lst.CapacityLevelID,
                                                   ConductLevelID = lst.ConductLevelID,
                                                   PeriodID = lst.PeriodID,
                                                   EducationLevelID = lst.EducationLevelID.Value,
                                                   RankingDate = DateTime.Now,
                                                   PupilRankingComment = lst.PupilRankingComment
                                               }).ToList();
                int count = lstPupil.Count;
                ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);


                objSummedUpRecordClass.AcademicYearID = academicYear.AcademicYearID;
                objSummedUpRecordClass.ClassID = ClassID;
                objSummedUpRecordClass.PeriodID = PeriodID;
                objSummedUpRecordClass.Semester = Semester;
                objSummedUpRecordClass.CreatedDate = DateTime.Now;
                objSummedUpRecordClass.EducationLevelID = classProfile.EducationLevelID;
                objSummedUpRecordClass.SchoolID = academicYear.SchoolID;
                objSummedUpRecordClass.NumberOfPupil = count;
                objSummedUpRecordClass.Type = SystemParamsInFile.SUMMED_UP_RECORD_CLASS_TYPE_1;
                objSummedUpRecordClass.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETING;
                objSummedUpRecordClass.SynchronizeID = 0;

                //12.02.2018 Kiem tra lop co dang thuc hien xep loai HK, tong ket, xep loai khong

                if (SummedUpRecordClassBusiness.CheckExistsSummedExcuting(objSummedUpRecordClass))
                {
                    return Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_Summeding"), JsonMessage.ERROR));
                }
                //Danh dau lop dang thuc hien tinh tong ket, xep loai
                SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);



                JsonResult message = Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_RankSuccess")));
                // Du lieu khong co hanh kiem thi bao loi
                if (lstPupil.Where(o => o.ConductLevelID.HasValue).Count() == 0)
                {
                    message = Json(new JsonMessage(Res.Get("PupilRankingSemester_Label_SumupRankWarningConduct"), JsonMessage.ERROR)); // Thầy/cô thực hiện xếp loại hạnh kiểm để xếp hạng cho học sinh
                }

                if (count == 0)
                {
                    throw new BusinessException("PupilRankingSemester_Label_NoPupilRanking"); // Thầy/cô thực hiện xếp loại hạnh kiểm để xếp hạng cho học sinh
                }
                // Neu la truong GDTX thi tu dong lay hanh kiem Tot doi voi HS ko thuoc dien xep loai HK
                #region GDTX - Gan lai hanh kiem
                SchoolProfile schoolProfile = academicYear.SchoolProfile;
                bool isGDTX = schoolProfile.TrainingTypeID.HasValue && schoolProfile.TrainingType.Resolution == "GDTX";
                // Lay danh sach hoc sinh de kiem tra dieu kien co thuoc dien xep hanh kiem hay khong                
                int applyLevel = _globalInfo.AppliedLevel.Value;
                Dictionary<int, bool> dicNotConductRank = new Dictionary<int, bool>();
                if (isGDTX)
                {
                    List<int> listPupilID = lstPupil.Select(o => o.PupilID).ToList();
                    var listPupilGDTX = PupilProfileBusiness.All.Where(o => o.CurrentSchoolID == _globalInfo.SchoolID && o.CurrentAcademicYearID == _globalInfo.AcademicYearID
                                 && o.CurrentClassID == ClassID && listPupilID.Contains(o.PupilProfileID))
                                 .Select(o => new
                                 {
                                     o.PupilProfileID,
                                     o.BirthDate,
                                     o.PupilLearningType
                                 })
                                 .ToList();

                    for (int i = count - 1; i >= 0; i--)
                    {
                        var item = lstPupil[i];
                        bool isNotConductRank = false;
                        var pupilInfo = listPupilGDTX.FirstOrDefault(o => o.PupilProfileID == item.PupilID);
                        if (pupilInfo != null)
                        {
                            if (pupilInfo.PupilLearningType.HasValue
                                && (pupilInfo.PupilLearningType.Value == SystemParamsInFile.PUPIL_LEARNING_TYPE_SELF_LEARNING
                                || pupilInfo.PupilLearningType.Value == SystemParamsInFile.PUPIL_LEARNING_TYPE_WORKING_AND_LEARNING))
                            {
                                isNotConductRank = true;
                            }
                            else
                            {
                                if (applyLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY && (DateTime.Now - pupilInfo.BirthDate).Days / 365 >= 20)
                                {
                                    isNotConductRank = true;
                                }
                                else if (applyLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY && (DateTime.Now - pupilInfo.BirthDate).Days / 365 >= 25)
                                {
                                    isNotConductRank = true;
                                }
                            }
                        }
                        if (isNotConductRank)
                        {
                            // Thuoc dien khong xep loai hanh kiem thi gan la HK Tot nhung ko luu vao CSDL                           
                            if (applyLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                            {
                                item.ConductLevelID = SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY;
                            }
                            else if (applyLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            {
                                item.ConductLevelID = SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY;
                            }
                        }
                        dicNotConductRank[item.PupilID] = isNotConductRank;
                    }
                }
                #endregion

                // Thực hiện xếp hạng
                PupilRankingBusiness.Rank(academicYear.RankingCriteria, lstPupil, false);
                VPupilRankingBusiness.UpdateRankPeriod(dicNotConductRank, lstPupil, academicYear);

                return message;
            }
            finally
            {
                //dan dau da thu hien thanh cong
                if (objSummedUpRecordClass != null && objSummedUpRecordClass.ClassID > 0)
                {
                    objSummedUpRecordClass.Status = SystemParamsInFile.STATUS_SUR_CLASS_COMPLETE;
                    SummedUpRecordClassBusiness.InsertOrSummedUpRecordClass(objSummedUpRecordClass);
                }

            }
        }

        public FileResult DownloadReport()
        {

            List<PupilOfClassBO> ListPupilOfClass = (List<PupilOfClassBO>)Session["ListPupilOfClass"];
            ListPupilOfClass = ListPupilOfClass.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.PupilFullName).ToList();
            List<ClassSubjectBO> ListClassSubject = (List<ClassSubjectBO>)Session["ListClassSubject"];
            List<PupilOfClassRanking> LstPupilRanking = (List<PupilOfClassRanking>)Session["LstPupilRanking"];
            LstPupilRanking = LstPupilRanking.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.FullName).ToList();
            List<SummedUpRecordBO> ListSummedUpRecord = (List<SummedUpRecordBO>)Session["ListSummedUpRecord"];
            ListSummedUpRecord = ListSummedUpRecord.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.PupilFullName).ToList();
            int ClassID = (int)Session["ClassID"];
            int semesterId = (int)Session["semesterId"];
            int periodId = (int)Session["periodId"];
            int AcademicYearId = new GlobalInfo().AcademicYearID.Value;

            PeriodDeclaration firstPeriod = PeriodDeclarationBusiness.Find(periodId);
            ClassProfile ClassProfile = ClassProfileBusiness.Find(ClassID);
            AcademicYear ay = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(new GlobalInfo().SchoolID.Value);
            string semester = ReportUtils.ConvertSemesterForReportName(semesterId);

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.TEMPLATE_TK_DIEMTHEODOT);
            //Tạo tên file BDTongKet_[Period]_[Semester]_[ClassName]
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[Period]", ReportUtils.StripVNSign(firstPeriod.Resolution));
            outputNamePattern = outputNamePattern.Replace("[ClassName]", ReportUtils.StripVNSign(ClassProfile.DisplayName));
            string ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;


            //lay ra danh sách học sinh dang ky mon chuyen/mon tu chon
            IDictionary<string, object> dicRegister = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SemesterID",semesterId},
                {"YearID",ay.Year}
            };
            List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegister);

            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet Template = oBook.GetSheet(1);

            IVTWorksheet Sheet = oBook.CopySheetToLast(Template, "R10");

            Sheet.SetCellValue("A2", sp.SupervisingDept.SupervisingDeptName.ToUpper());
            Sheet.SetCellValue("A3", sp.SchoolName.ToUpper());
            Sheet.SetCellValue("F2", "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM");
            Sheet.SetCellValue("F3", "Độc lập - Tự do - Hạnh phúc");
            Sheet.SetCellValue("F4", sp.Province.ProvinceName + ",ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
            Sheet.SetCellValue("A6", "BẢNG ĐIỂM TỔNG KẾT " + firstPeriod.Resolution + " (" + firstPeriod.FromDate.Value.ToString("dd/MM/yyyy")
                            + "-" + firstPeriod.EndDate.Value.ToString("dd/MM/yyyy") + ") " + "LỚP " + ClassProfile.DisplayName.ToUpper().Replace("LỚP", "").Replace(" ", ""));
            Sheet.SetCellValue("A7", "Học kỳ " + semesterId + " Năm học " + ay.DisplayTitle);

            // Tạo header template
            IVTRange rangeHeader = Template.GetRange("D9", "D9");
            IVTRange rangeHeaderButtom = Template.GetRange("D10", "D10");
            IVTRange rangeHeaderLast = Template.GetRange("E9", "E10");

            int startcolhearder = 4;


            for (int i = 0; i < ListClassSubject.Count(); i++)
            {
                string titleHeaarder = "";
                if (i == 0)
                {
                    titleHeaarder = "ĐIỂM TRUNG BÌNH CÁC MÔN";
                }
                Sheet.CopyPasteSameSize(rangeHeader, 9, startcolhearder);
                Sheet.SetCellValue(9, startcolhearder, titleHeaarder);
                Sheet.CopyPasteSameSize(rangeHeaderButtom, 10, startcolhearder);
                Sheet.SetCellValue(10, startcolhearder, ListClassSubject[i].DisplayName);
                startcolhearder++;

            }

            Sheet.MergeRow(9, 4, startcolhearder - 1);

            Sheet.CopyPasteSameSize(rangeHeaderLast, 9, startcolhearder);
            Sheet.SetCellValue(9, startcolhearder, "TBcm");
            startcolhearder++;
            Sheet.CopyPasteSameSize(rangeHeaderLast, 9, startcolhearder);
            Sheet.SetCellValue(9, startcolhearder, "Học lực");
            startcolhearder++;
            Sheet.CopyPasteSameSize(rangeHeaderLast, 9, startcolhearder);
            Sheet.SetCellValue(9, startcolhearder, "Hạnh kiểm");
            startcolhearder++;
            Sheet.CopyPasteSameSize(rangeHeaderLast, 9, startcolhearder);
            Sheet.SetCellValue(9, startcolhearder, "Xếp hạng");


            IVTRange range = Template.GetRange("A12", "C12");
            IVTRange rangeLast = Template.GetRange("A15", "C15");
            IVTRange rangeError = Template.GetRange("A13", "C13");
            IVTRange rangeErrorLast = Template.GetRange("A11", "C11");

            IVTRange rangeSubject = Template.GetRange("D12", "D12");
            IVTRange rangeSubjectLast = Template.GetRange("D15", "D15");

            int StartRow = 11;
            int LastCol = 0;
            // Lay them thong tin hien thi ve mon hoc mien giam
            List<ExemptedSubject> listExempteds = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, semesterId).ToList();
            int countSubjectRegister = 0;
            PupilOfClassBO objPupilOfClassBO = null;
            SummedUpRecordBO sur = null;

            for (int j = 0; j < ListPupilOfClass.Count(); j++)
            {
                IVTRange rangeExcel = range;
                IVTRange rangeSubjectExcel = rangeSubject;
                objPupilOfClassBO = ListPupilOfClass[j];
                int StartCol = 4;
                if (j > 0 && (j + 1) % 5 == 0 || j == ListPupilOfClass.Count() - 1)
                {
                    rangeExcel = rangeLast;
                    rangeSubjectExcel = rangeSubjectLast;
                }
                if (objPupilOfClassBO.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL
                    || objPupilOfClassBO.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                    || objPupilOfClassBO.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF)
                {
                    if (j > 0 && (j + 1) % 5 == 0 || j == ListPupilOfClass.Count() - 1)
                    {
                        rangeExcel = rangeErrorLast;
                    }
                    else
                    {
                        rangeExcel = rangeError;
                    }

                }

                Sheet.SetCellValue(StartRow, 1, j + 1);
                Sheet.SetCellValue(StartRow, 2, objPupilOfClassBO.PupilFullName);
                Sheet.SetCellValue(StartRow, 3, objPupilOfClassBO.PupilCode);
                Sheet.CopyPaste(rangeExcel, StartRow, 1, true);

                // Lay thong tin diem
                string MarkInfo = string.Empty;
                foreach (var item in ListClassSubject)
                {

                    if (objPupilOfClassBO.isShow == true)
                    {
                        bool isExemptedSubject = listExempteds.Any(o => o.PupilID == objPupilOfClassBO.PupilID && o.SubjectID == item.SubjectID);
                        if (isExemptedSubject)
                        {
                            MarkInfo = Res.Get("Common_Label_MG");
                        }
                        else
                        {
                            countSubjectRegister = lstRegisterSubjectBO.Where(p => p.PupilID == objPupilOfClassBO.PupilID && p.SubjectID == item.SubjectID && p.SubjectTypeID == item.AppliedType && (semesterId < 2 ? p.SemesterID == semesterId : true)).Count();
                            if (((item.Is_Specialize_Subject.HasValue && item.Is_Specialize_Subject.Value) || (item.AppliedType == SMAS.Business.Common.GlobalConstants.APPLIED_SUBJECT_ELECTIVE || item.AppliedType == SMAS.Business.Common.GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE || item.AppliedType == SMAS.Business.Common.GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE))
                                 && countSubjectRegister == 0)
                            {
                                MarkInfo = Res.Get("KH");
                            }
                            else
                            {
                                sur = ListSummedUpRecord.Where(o => o.PupilID == objPupilOfClassBO.PupilID && o.SubjectID == item.SubjectID).FirstOrDefault();
                                if (sur == null)
                                {
                                    MarkInfo = Res.Get("Common_Label_CT");
                                }
                                else
                                {
                                    if (sur.SummedUpMark.HasValue)
                                    {
                                        if (sur.SummedUpMark != 10.0m)
                                        {
                                            MarkInfo = sur.SummedUpMark.Value.ToString("0.0#");
                                        }
                                        else
                                        {
                                            MarkInfo = sur.SummedUpMark.Value.ToString("0");
                                        }
                                    }
                                    else if (sur.JudgementResult != null)
                                    {
                                        MarkInfo = sur.JudgementResult;
                                    }
                                    else
                                    {
                                        MarkInfo = Res.Get("Common_Label_CT");
                                    }
                                }
                            }
                        }
                    }
                    MarkInfo = MarkInfo.Replace(",", ".");
                    Sheet.CopyPasteSameSize(rangeSubjectExcel, StartRow, StartCol);
                    Sheet.SetCellValue(StartRow, StartCol, MarkInfo);
                    StartCol++;
                }

                var pr = LstPupilRanking.Where(o => o.PupilID == objPupilOfClassBO.PupilID).FirstOrDefault();
                Sheet.CopyPasteSameSize(rangeSubjectExcel, StartRow, StartCol);
                if (objPupilOfClassBO.isShow == true)
                {
                    string averageM;
                    if (pr != null)
                    {
                        if (pr.AverageMark.HasValue)
                        {
                            if (pr.AverageMark != 10.0m)
                            {
                                averageM = pr.AverageMark.Value.ToString("0.0#");
                            }
                            else
                            {
                                averageM = pr.AverageMark.Value.ToString("0");
                            }
                        }
                        else
                        {
                            averageM = null;
                        }
                    }
                    else
                    {
                        averageM = null;
                    }
                    if (averageM != null)
                        averageM = averageM.Replace(",", ".");
                    Sheet.SetCellValue(StartRow, StartCol, averageM);
                    //Sheet.SetCellValue(StartRow, StartCol, pr.Count() != 0 ? pr.FirstOrDefault().AverageMark.ToString() : null);
                }
                StartCol++;
                Sheet.CopyPasteSameSize(rangeSubjectExcel, StartRow, StartCol);
                if (objPupilOfClassBO.isShow == true)
                {
                    Sheet.SetCellValue(StartRow, StartCol, pr != null ? pr.CapacityLevel : null);
                }
                StartCol++;
                Sheet.CopyPasteSameSize(rangeSubjectExcel, StartRow, StartCol);
                if (objPupilOfClassBO.isShow == true)
                {
                    Sheet.SetCellValue(StartRow, StartCol, pr != null ? pr.ConductLevel : null);
                }
                StartCol++;
                Sheet.CopyPasteSameSize(rangeSubjectExcel, StartRow, StartCol);
                if (objPupilOfClassBO.isShow == true)
                {
                    Sheet.SetCellValue(StartRow, StartCol, pr != null ? pr.Rank.ToString() : null);
                }
                StartRow++;
                LastCol = StartCol;
            }

            Sheet.CopyPaste(Template.GetRange("D18", "D18"), StartRow + 2, LastCol - 2);
            Sheet.FitAllColumnsOnOnePage = true;
            Template.Delete();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportName;
            return result;
        }

        private int GetRankingType()
        {
            GlobalInfo glo = new GlobalInfo();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("SchoolID", glo.SchoolID.Value);
            dic.Add("AppliedLevel", glo.AppliedLevel.Value);
            dic.Add("IsActive", true);

            var configByCapacity = this.ConductConfigByCapacityBu.Search(dic).FirstOrDefault();
            if (configByCapacity != null) return 0;

            var configByViolation = this.ConductConfigByViolationBu.Search(dic).FirstOrDefault();
            if (configByViolation != null) return 1;

            return -1;
        }
    }
}
