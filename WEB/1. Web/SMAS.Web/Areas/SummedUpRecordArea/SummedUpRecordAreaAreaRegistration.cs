﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SummedUpRecordArea
{
    public class SummedUpRecordAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SummedUpRecordArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SummedUpRecordArea_default",
                "SummedUpRecordArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
