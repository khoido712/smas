/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.SummedUpRecordArea.Models
{
    public class SummedUpRecordViewModel
    {
		public System.Int32 SummedUpRecordID { get; set; }								
		public System.Int32 PupilID { get; set; }								
		public System.Int32 ClassID { get; set; }								
		public System.Int32 SchoolID { get; set; }								
		public System.Int32 AcademicYearID { get; set; }								
		public System.Int32 SubjectID { get; set; }								
		public System.Int32 IsCommenting { get; set; }								
		public System.Int32 Year { get; set; }								
		public System.Nullable<System.Int32> Semester { get; set; }								
		public System.Nullable<System.Int32> PeriodID { get; set; }								
		public System.Nullable<System.Decimal> SummedUpMark { get; set; }								
		public System.String JudgementResult { get; set; }								
		public System.String Comment { get; set; }								
		public System.Nullable<System.DateTime> SummedUpDate { get; set; }								
		public System.Nullable<System.Decimal> ReTestMark { get; set; }								
		public System.String ReTestJudgement { get; set; }								
	       
    }
}


