/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.SummedUpRecordArea
{
    public class SummedUpRecordConstants
    {
        public const string LIST_SUMMEDUPRECORD = "listSummedUpRecord";
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATION_LEVEL = "listEducationLevel";
        public const string LIST_PERIOD = "listPeriod";
        public const string LIST_CLASS = "listClass";
        public const string LIST_SUBJECT = "listSubject";
        public const string LIST_PUPIL_CLASS = "listPupilOfClass";
        public const string LIST_PUPIL_RANKING = "listPupilRanking";
        public const string TITLE = "Title";
        public const string DIC_EXEMPTED_SUBJECT = "DicExemptedSubject";
        public const int TYPE_CT = 1;
        public const int TYPE_MG = 2;
        public const int TYPE_SUMMEDUPMARK = 3;
        public const int TYPE_JUDGEMENTRESULT = 4;
        public const string ENABLE_SUMMEDUP = "ENABLE_SUMMEDUP";
        public const string TITLE_PAGE = "TITLE_PAGE";
        public const string ENABLE_EXCEL = "ENABLE_EXCEL";
        public const string ENABLE_RANK = "ENABLE_RANK";
        public const string LIST_REGISTER_SUBJECT = "listRegisterSubject";
    }
}