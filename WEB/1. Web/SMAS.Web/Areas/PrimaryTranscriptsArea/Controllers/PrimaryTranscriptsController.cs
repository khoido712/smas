﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.PrimaryTranscriptsArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.PrimaryTranscriptsArea.Controllers
{
    public class PrimaryTranscriptsController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly ITranscriptsBusiness TranscriptsBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;

        public PrimaryTranscriptsController(IClassProfileBusiness ClassProfileBusiness,
            IProcessedReportBusiness ProcessedReportBusiness,
            ITranscriptsBusiness TranscriptsBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness,
            IPupilProfileBusiness PupilProfileBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.TranscriptsBusiness = TranscriptsBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.PupilProfileBusiness = PupilProfileBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region SetViewData

        private void SetViewData()
        {
            List<EducationLevel> lstEducationLevel = _globalInfo.EducationLevels;

            if (lstEducationLevel != null && lstEducationLevel.Count > 0)
            {
                ViewData[PrimaryTranscriptsConstants.LIST_EDUCATIONLEVEL] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");
                List<ClassProfile> lstClass = GetClassFromEducationLevel(lstEducationLevel.FirstOrDefault().EducationLevelID);
                if (lstClass != null && lstClass.Count > 0)
                {
                    ViewData[PrimaryTranscriptsConstants.LIST_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
                }
                else
                {
                    ViewData[PrimaryTranscriptsConstants.LIST_CLASS] = new SelectList(new string[] { });
                }
            }
            else
            {
                ViewData[PrimaryTranscriptsConstants.LIST_EDUCATIONLEVEL] = new SelectList(new string[] { });
                ViewData[PrimaryTranscriptsConstants.LIST_CLASS] = new SelectList(new string[] { });
            }
        }

        #endregion

        #region AjaxLoadCombobox

        private List<ClassProfile> GetClassFromEducationLevel(int EducationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;

            if (!_globalInfo.IsRolePrincipal && !_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager && !_globalInfo.IsViewAll)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            if (_globalInfo.IsViewAll)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
            }

            List<ClassProfile> lsCP = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(p => p.EducationLevelID == EducationLevelID).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
            return lsCP;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue)
            {
                List<ClassProfile> lsCP = GetClassFromEducationLevel(EducationLevelID.Value);
                if (lsCP.Count() != 0)
                {
                    return Json(new SelectList(lsCP, "ClassProfileID", "DisplayName"));
                }
                else
                {
                    return Json(new SelectList(new string[] { }));
                }
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        private IQueryable<PupilOfClass> GetPupilFromClass(int ClassID)
        {
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            List<int> listStatus = new List<int>() { GlobalConstants.PUPIL_STATUS_STUDYING };
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                { "ClassID", ClassID },
                { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                { "Status", listStatus }
            };

            IQueryable<PupilOfClass> lsPOC = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Where(o => o.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS);
            return lsPOC;
        }

        public ActionResult AjaxLoadPupil(int? ClassID)
        {
            List<PupilProfileBO> lsPP = new List<PupilProfileBO>();
            if (ClassID.HasValue)
            {
                IQueryable<PupilOfClass> lsCP = GetPupilFromClass(ClassID.Value);
                lsPP = lsCP.Select(o => new PupilProfileBO
                {
                    OrderInClass = o.OrderInClass,
                    PupilProfileID = o.PupilID,
                    FullName = o.PupilProfile.FullName,
                    Name = o.PupilProfile.Name

                }).Distinct().ToList().OrderBy(o => o.OrderInClass).ThenBy(o => o.FullName).ToList();
            }
            else
            {
                return new JsonResult { Data = new SelectList(new string[] { }) };
            }

            return new JsonResult { Data = new SelectList(lsPP, "PupilProfileID", "FullName") };
        }

        /*public PartialViewResult AjaxLoadPupil(int? ClassID)
        {
            List<PupilProfileBO> lsPP = new List<PupilProfileBO>();
            if (ClassID.HasValue)
            {
                IQueryable<PupilOfClass> lsCP = GetPupilFromClass(ClassID.Value);
                lsPP = lsCP.Select(o => new PupilProfileBO
                {
                    OrderInClass = o.OrderInClass,
                    PupilProfileID = o.PupilID,
                    FullName = o.PupilProfile.FullName,
                    Name = o.PupilProfile.Name

                }).Distinct().ToList().OrderBy(o => o.OrderInClass).ThenBy(o => o.FullName).ToList();
            }

            PupilProfileBO pp = new PupilProfileBO();
            pp.PupilProfileID = 0;
            pp.FullName = Res.Get("PrimaryTranscripts_PlaceHolder_Pupil");
            lsPP.Insert(0, pp);
            ViewData[PrimaryTranscriptsConstants.LIST_PUPIL] = lsPP;

            return PartialView("_ComboboxPupil");
        }*/

        #endregion

        #region Report

        #region GetReport
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(SearchViewModel form)
        {
            PupilProfile pupil = PupilProfileBusiness.Find(form.Pupil);
            if (pupil != null)
            {
                IDictionary<string, object> info = new Dictionary<string, object>();
                info["AppliedLevel"] = _globalInfo.AppliedLevel.GetValueOrDefault();
                info["SchoolID"] = _globalInfo.SchoolID.GetValueOrDefault();
                info["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                info["EducationLevelID"] = form.EducationLevel;
                info["ClassID"] = form.Class;
                info["PupilProfileID"] = form.Pupil;

                string reportCode = SystemParamsInFile.REPORT_PRIMARY_TRANSCRIPTS;

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                string type = JsonReportMessage.NEW;
                ProcessedReport processedReport = null;

                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = TranscriptsBusiness.GetReportPrimaryTranscripts(info);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = TranscriptsBusiness.CreateReportPrimaryTranscripts(info);
                    processedReport = TranscriptsBusiness.InsertReportPrimaryTranscripts(info, excel);
                    excel.Close();
                }

                return Json(new JsonReportMessage(processedReport, type));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("PrimaryTranscripts_Error_Pupil"), "error"));
            }
        }

        #endregion GetReport

        #region GetNewReport
        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(SearchViewModel form)
        {
            PupilProfile pupil = PupilProfileBusiness.Find(form.Pupil);
            if (pupil != null)
            {
                IDictionary<string, object> info = new Dictionary<string, object>();
                info["AppliedLevel"] = _globalInfo.AppliedLevel.GetValueOrDefault();
                info["SchoolID"] = _globalInfo.SchoolID.GetValueOrDefault();
                info["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                info["EducationLevelID"] = form.EducationLevel;
                info["ClassID"] = form.Class;
                info["PupilProfileID"] = form.Pupil;

                Stream excel = TranscriptsBusiness.CreateReportPrimaryTranscripts(info);
                ProcessedReport processedReport = TranscriptsBusiness.InsertReportPrimaryTranscripts(info, excel);
                excel.Close();

                return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("PrimaryTranscripts_Error_Pupil"), "error"));
            }
        }

        #endregion GetNewReport

        #region DownloadReport
        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        #endregion DownloadReport

        #endregion
    }
}
