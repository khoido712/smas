﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.PrimaryTranscriptsArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("PrintSchoolReport_Label_EducationLevelSearch")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", PrimaryTranscriptsConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadClass(this)")]
        public long EducationLevel { get; set; }

        [ResourceDisplayName("PrintSchoolReport_Label_ClassSearch")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", PrimaryTranscriptsConstants.LIST_CLASS)]
        [AdditionalMetadata("Placeholder", "0")]
        [AdditionalMetadata("OnChange", "AjaxLoadPupil(this)")]
        public long Class { get; set; }

        [ResourceDisplayName("PrintSchoolReport_Label_PupilSearch")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", PrimaryTranscriptsConstants.LIST_PUPIL)]
        [AdditionalMetadata("Placeholder", "null")]
        public long Pupil { get; set; }
    }
}