﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PrimaryTranscriptsArea
{
    public class PrimaryTranscriptsAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PrimaryTranscriptsArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PrimaryTranscriptsArea_default",
                "PrimaryTranscriptsArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
