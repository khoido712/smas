﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PrimaryTranscriptsArea
{
    public class PrimaryTranscriptsConstants
    {
        public const string LIST_EDUCATIONLEVEL = "ListEducationsLevel";
        public const string LIST_CLASS = "ListClass";
        public const string LIST_PUPIL = "ListPupil";
    }
}