﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.EmployeeSeveranceArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.EmployeeSeveranceArea.Controllers
{
    public class EmployeeSeveranceController : BaseController
    {
        private readonly IEmployeeWorkMovementBusiness EmployeeWorkMovementBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        public EmployeeSeveranceController(IEmployeeWorkMovementBusiness employeeworkmovementBusiness, ISchoolFacultyBusiness SchoolFacultyBusiness, IEmployeeBusiness EmployeeBusiness)
        {
            this.EmployeeWorkMovementBusiness = employeeworkmovementBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            EmployeeWorkMovement employeeworkmovement = new EmployeeWorkMovement();
            TryUpdateModel(employeeworkmovement);
            Utils.Utils.TrimObject(employeeworkmovement);
            employeeworkmovement.FromSchoolID = new GlobalInfo().SchoolID.Value;
            employeeworkmovement.MovementType = SystemParamsInFile.WORK_MOVEMENT_SEVERANCEMENT;

            this.EmployeeWorkMovementBusiness.InsertSeverance(employeeworkmovement);
            this.EmployeeWorkMovementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int EmployeeWorkMovementID)
        {
            EmployeeWorkMovement employeeworkmovement = this.EmployeeWorkMovementBusiness.Find(EmployeeWorkMovementID);
            EmployeeSeveranceViewModel ervm = new EmployeeSeveranceViewModel();
            TryUpdateModel(employeeworkmovement);
            TryUpdateModel(ervm);
            if (!ervm.MovedDate.HasValue)
            {
                return Json(new JsonMessage(Res.Get("EmployeeSeverance_Label_DateFormat"), "error"));
            }
            Utils.Utils.TrimObject(employeeworkmovement);
            employeeworkmovement.FromSchoolID = new GlobalInfo().SchoolID.Value;
            employeeworkmovement.MovementType = SystemParamsInFile.WORK_MOVEMENT_SEVERANCEMENT;
            employeeworkmovement.MovedDate = ervm.MovedDate;

            this.EmployeeWorkMovementBusiness.UpdateSeverance(employeeworkmovement, new GlobalInfo().SchoolID.Value);
            this.EmployeeWorkMovementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.EmployeeWorkMovementBusiness.DeleteSeverance(id, new GlobalInfo().SchoolID.Value);
            this.EmployeeWorkMovementBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
    }
}





