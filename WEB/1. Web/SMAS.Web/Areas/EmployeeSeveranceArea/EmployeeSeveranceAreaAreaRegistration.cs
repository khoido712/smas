﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EmployeeSeveranceArea
{
    public class EmployeeSeveranceAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EmployeeSeveranceArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EmployeeSeveranceArea_default",
                "EmployeeSeveranceArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
