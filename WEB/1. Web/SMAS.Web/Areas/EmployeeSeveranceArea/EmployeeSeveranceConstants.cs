﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EmployeeSeveranceArea
{
    public class EmployeeSeveranceConstants
    {
        public const string LIST_EMPLOYEEWORKMOVEMENT = "listEmployeeWorkMovement";
        public const string LIST_SCHOOLFACULTY = "listSchoolFaculty";
        public const string LIST_EMPLOYEE = "listEmployee";
        public const int MOVE_TYPE_RETIRE = 3;

        public const string IS_CURRENT_YEAR = "IS_CURRENT_YEAR";
    }
}