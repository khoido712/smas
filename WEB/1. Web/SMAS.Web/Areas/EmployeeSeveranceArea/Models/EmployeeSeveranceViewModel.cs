﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using SMAS.Models.CustomAttribute;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using SMAS.Web.Areas.EmployeeSeveranceArea;
namespace SMAS.Web.Areas.EmployeeSeveranceArea.Models
{
    public class EmployeeSeveranceViewModel
    {
        [ScaffoldColumn(false)]
        public Int32 EmployeeWorkMovementID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("EmployeeWorkMovement_Label_SchoolFaculty")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public Int32 SchoolFacultyID { get; set; }

        [ResourceDisplayName("EmployeeWorkMovement_Label_SchoolFaculty")]
        [ScaffoldColumn(false)]
        public string SchoolFacultyName { get; set; }

        [ResourceDisplayName("EmployeeWorkMovement_Label_Employee")]
        [ScaffoldColumn(false)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public Int32 TeacherID { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        [ScaffoldColumn(false)]
        public string TeacherName { get; set; }

        [ResourceDisplayName("Employee_Label_EmployeeCode")]
        [ScaffoldColumn(false)]
        public string TeacherCode { get; set; }

        [ResourceDisplayName("WorkMovement_Label_MoveType")]
        [ScaffoldColumn(false)]
        public int MovementType { get; set; }

        [ResourceDisplayName("WorkMovement_Label_MoveType")]
        [ScaffoldColumn(false)]
        public string MoveName { get; set; }

        [ScaffoldColumn(false)]
        public Int32 FromSchoolID { get; set; }

        [ResourceDisplayName("EmployeeWorkMovement_Label_ResolutionSeveranceDocument")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ResolutionDocument { get; set; }

        [ScaffoldColumn(false)]
        public Nullable<Int32> ToSupervisingDeptID { get; set; }

        [ResourceDisplayName("EmployeeWorkMovement_Label_SeveranceDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("DateTimePicker")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        public Nullable<DateTime> MovedDate { get; set; }

        [ResourceDisplayName("WorkMovement_Label_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [ScaffoldColumn(false)]
        public bool? IsAccepted { get; set; }

    }
}


