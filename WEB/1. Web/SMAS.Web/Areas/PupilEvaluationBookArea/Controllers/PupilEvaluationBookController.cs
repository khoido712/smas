﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.PupilEvaluationBookArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Filter;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Text;

namespace SMAS.Web.Areas.PupilEvaluationBookArea.Controllers
{
    public class PupilEvaluationBookController:BaseController
    {
        #region Properties
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness;
        private readonly ITeacherNoteBookMonthBusiness TeacherNoteBookMonthBusiness;
        private readonly IReviewBookPupilBusiness ReviewBookPupilBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness;
        private List<ClassSubject> lstSubject;
        private List<Tab1ViewModel> lstTab1ViewModel;
        private List<Tab2ViewModel> lstTab2ViewModel;
        private string log_Comment1 = "Biểu hiện nổi bật: ";
        private string log_Comment2 = "Nội dung chưa hoàn thành: ";
        private string log_Capacity = "Xếp loại năng lực: ";
        private string log_CQ = "Xếp loại phẩm chất: ";
        private string log_CapacitySummer = "Rèn luyện năng lực trong hè: ";
        private string log_CQSummer = "Rèn luyện phẩm chất trong hè: ";
        private string log_space = "; ";

        #endregion

        #region Contructor
        public PupilEvaluationBookController(IClassProfileBusiness ClassProfileBusiness, IAcademicYearBusiness AcademicYearBusiness, IClassSubjectBusiness ClassSubjectBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness, ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness, IReviewBookPupilBusiness ReviewBookPupilBusiness,
            ISubjectCatBusiness SubjectCatBusiness, ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness, ISummedUpRecordBusiness SummedUpRecordBusiness,
            ITeacherNoteBookMonthBusiness TeacherNoteBookMonthBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness,
            IExemptedSubjectBusiness ExemptedSubjectBusiness, IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness, IPupilProfileBusiness pupilProfileBusiness,
            ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.TeacherNoteBookSemesterBusiness = TeacherNoteBookSemesterBusiness;
            this.TeacherNoteBookMonthBusiness = TeacherNoteBookMonthBusiness;
            this.ReviewBookPupilBusiness = ReviewBookPupilBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SummedUpRecordHistoryBusiness = SummedUpRecordHistoryBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ExemptedSubjectBusiness = ExemptedSubjectBusiness;
            this.RegisterSubjectSpecializeBusiness = RegisterSubjectSpecializeBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.LockRatedCommentPupilBusiness = LockRatedCommentPupilBusiness;
        }
        #endregion

        #region Action methods
        public ActionResult Index(int? classID)
        {
            SetViewData(classID);

            return View("Index");
        }

        [HttpPost]
        public PartialViewResult SearchTab1(int classId, int semester)
        {
            _SearchTab1(classId, semester);
            ViewData[PupilEvaluationBookConstants.LIST_SUBJECT] = lstSubject;
            ViewData[PupilEvaluationBookConstants.LIST_TAB_1] = lstTab1ViewModel;
            ViewData[PupilEvaluationBookConstants.DEFAULT_SEMESTER] = semester;
            ViewData[PupilEvaluationBookConstants.CLASS_ID] = classId;
            ViewData[PupilEvaluationBookConstants.PERMISSION_BUTTON] = CheckButtonPermission(classId, semester);
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                string LockTitle = string.Empty;
                IDictionary<string, object> dicLock = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID},
                    {"ClassID",classId},
                    {"lstEvaluationID",new List<int>(){5,6,7,8}}
                };
                LockTitle = LockRatedCommentPupilBusiness.GetLockTitleVNEN(dicLock);
                ViewData[PupilEvaluationBookConstants.LOCK_TITLE] = LockTitle;
                return PartialView("_EvaluationBook");
            }
            else
            {
                return PartialView("_Tab1");
            }
            
        }

        [HttpPost]
        public PartialViewResult SearchTab2(int classId, int semester)
        {
            _SearchTab2(classId, semester);
            ViewData[PupilEvaluationBookConstants.LIST_TAB_2] = lstTab2ViewModel;
            ViewData[PupilEvaluationBookConstants.DEFAULT_SEMESTER] = semester;
            ViewData[PupilEvaluationBookConstants.CLASS_ID] = classId;
            ViewData[PupilEvaluationBookConstants.PERMISSION_BUTTON] = CheckButtonPermission(classId, semester);
            //lay khoa NX DG
            string LockTitle = string.Empty;
            IDictionary<string, object> dicLock = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"ClassID",classId},
                {"lstEvaluationID",new List<int>(){5,6,7,8}}
            };
            LockTitle = LockRatedCommentPupilBusiness.GetLockTitleVNEN(dicLock);
            ViewData[PupilEvaluationBookConstants.LOCK_TITLE] = LockTitle;
            return PartialView("_Tab2");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Evaluation(int classId)
        {
            
            //Lay danh sach mon hoc
            int semester=GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            if (!CheckButtonPermission(classId, semester))
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            _SearchTab1(classId, GlobalConstants.SEMESTER_OF_YEAR_ALL);
            List<Tab1ViewModel> lstResult = lstTab1ViewModel;
            List<int> lstPupilID = lstResult.Select(p => p.PupilID).Distinct().ToList();
            Tab1ViewModel objResult = new Tab1ViewModel();
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<ReviewBookPupil> lstReviewBook = ReviewBookPupilBusiness.All
                .Where(o => o.SchoolID == _globalInfo.SchoolID
                && o.AcademicYearID == _globalInfo.AcademicYearID
                && o.ClassID == classId
                && o.SemesterID == semester
                && lstPupilID.Contains(o.PupilID)
                && o.PartitionID == partition).ToList();
            int StudyResult = 0;
            int RateAndYear = 0;
            int Equal = 0;
            PupilSubjectEvaluationModel objPSEM = null;
            List<decimal?> lstMark = new List<decimal?>();
            List<string> lstJudge = new List<string>();
            for (int i = 0; i < lstResult.Count; i++)
            {
                objResult = lstResult[i];
                if (objResult.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                List<PupilSubjectEvaluationModel> lsttmp = objResult.lstSubjectEvaluation;
                if (lsttmp == null || lsttmp.Count == 0)
                {
                    continue;
                }
                lstMark = new List<decimal?>();
                lstJudge = new List<string>();
                RateAndYear = 0;
                StudyResult = 0;
                List<PupilSubjectEvaluationModel> lstSubjectEvaluation = lsttmp.Where(p => !p.IsExempted && !p.IsNotLearning).ToList();
                for (int j = 0; j < lstSubjectEvaluation.Count; j++)
                {
                    objPSEM = lstSubjectEvaluation[j];
                    if (objPSEM.IsVnenSubject)
                    {
                        if (objPSEM.IsCommenting == 1)
                        {
                            lstJudge.Add(objPSEM.AVERAGE_JUDGE);
                        }
                        else
                        {
                            lstMark.Add(objPSEM.AVERAGE_MARK);
                        }
                    }
                    else
                    {
                        if (objPSEM.IsCommenting == 1)
                        {
                            lstJudge.Add(objPSEM.JudgementResult);
                        }
                        else
                        {
                            lstMark.Add(objPSEM.SummedUpMark);
                        }
                    }
                }

                if (lstMark.Count(p=>!p.HasValue) > 0 || lstJudge.Count(p=>string.IsNullOrEmpty(p)) > 0)
                {
                    StudyResult = 0;
                }
                else
                {
                    Equal = int.Parse(Math.Ceiling((decimal)lstMark.Count * 2 / 3).ToString());
                    if (lstMark.Count > 0)
                    {
                        if (lstMark.Count(p => p.Value >= (decimal)8.0) >= Equal && lstMark.Count(p => p.Value < (decimal)6.5) == 0 && lstJudge.Count(p => p.Equals("CĐ")) == 0)
                        {
                            StudyResult = 1;//HHT
                        }
                        else if (lstMark.Count(p => p.Value < (decimal)5.0) == 0 && lstJudge.Count(p => p.Equals("CĐ")) == 0)
                        {
                            StudyResult = 2;//HT
                        }
                        else
                        {
                            StudyResult = 3;//CHT
                        }
                    }
                    else if (lstJudge.Count > 0)
                    {
                        if (lstJudge.Count(p => p.Equals("CĐ")) == 0)
                        {
                            StudyResult = 2;//HT
                        }
                        else
                        {
                            StudyResult = 3;//CHT
                        }
                    }
                    else
                    {
                        StudyResult = 0;
                    }
                    

                    //Tinh ket qua danh gia cuoi nam
                    if (StudyResult > 0)
                    {
                        if (objResult.Capacity.HasValue && objResult.Quality.HasValue)
                        {
                            if ((StudyResult == 1 || StudyResult == 2) && ((objResult.Capacity == 1 || objResult.Capacity == 2) && (objResult.Quality == 1 || objResult.Quality == 2)))
                            {
                                RateAndYear = 1;
                            }
                            else
                            {
                                RateAndYear = 2;
                            }
                        }
                        else
                        {
                            RateAndYear = 0;
                        }
                    }
                    else
                    {
                        RateAndYear = 0;
                    }
                }
                ReviewBookPupil rbp = lstReviewBook.Where(o => o.PupilID == objResult.PupilID).FirstOrDefault();
                if (rbp != null)//update
                {
                    rbp.StudyResult = StudyResult;
                    rbp.RateAndYear = RateAndYear;
                    rbp.UpdateTime = DateTime.Now;
                    if (RateAndYear == 1)
                    {
                        rbp.RateAdd = null;
                    }
                    ReviewBookPupilBusiness.Update(rbp);
                }
                else//insert
                {
                    ReviewBookPupil objInsert = new ReviewBookPupil();
                    objInsert.SchoolID = _globalInfo.SchoolID.Value;
                    objInsert.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objInsert.ClassID = classId;
                    objInsert.PupilID = objResult.PupilID;
                    objInsert.SemesterID = semester;
                    objInsert.StudyResult = StudyResult;
                    objInsert.RateAndYear = RateAndYear;
                    objInsert.CreateTime = DateTime.Now;
                    objInsert.PartitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                    ReviewBookPupilBusiness.Insert(objInsert);
                }
            }
            ReviewBookPupilBusiness.Save();
            return Json(new JsonMessage(Res.Get("PupilEvaluationBook_Msg_Evaluation")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public ActionResult SaveAdditionRate(List<Tab1ViewModel> lstTab1ViewModel, int classId)
        {
            if (!CheckButtonPermission(classId, GlobalConstants.SEMESTER_OF_YEAR_SECOND))
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            //Lay ra cac hoc sinh co nhap danh gia bo sung
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<ReviewBookPupil> lstReviewBook = ReviewBookPupilBusiness.All
                .Where(o => o.SchoolID == _globalInfo.SchoolID
                && o.AcademicYearID == _globalInfo.AcademicYearID
                && o.ClassID == classId
                && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND
                && o.PartitionID == partition).ToList();
            string LockTitle = string.Empty;
            IDictionary<string, object> dicLock = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"ClassID",classId},
                {"lstEvaluationID",new List<int>(){5,6,7,8}}
            };
            LockTitle = LockRatedCommentPupilBusiness.GetLockTitleVNEN(dicLock);
            if (LockTitle.Contains("DGBS"))
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Save")));
            }
            lstTab1ViewModel = lstTab1ViewModel.Where(o => o.Status == GlobalConstants.PUPIL_STATUS_STUDYING).ToList();
            List<int> lstPupilID = lstTab1ViewModel.Select(p => p.PupilID).Distinct().ToList();

            #region ghi log du lieu
            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();

            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsmeterStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            bool isChange = false;
            ClassProfile objCP = ClassProfileBusiness.Find(classId);
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                           where poc.AcademicYearID == _globalInfo.AcademicYearID
                                           && poc.SchoolID == _globalInfo.SchoolID
                                           && poc.ClassID == classId
                                           && lstPupilID.Contains(poc.PupilID)
                                           && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               PupilCode = pf.PupilCode,
                                               ClassID = poc.ClassID,
                                               ClassName = cp.DisplayName,
                                               PupilFullName = pf.FullName
                                           }).ToList();
            PupilOfClassBO objPOC = new PupilOfClassBO();

            #endregion

            for (int i = 0; i < lstTab1ViewModel.Count; i++)
            {
                isChange = false;
                Tab1ViewModel model = lstTab1ViewModel[i];
                ReviewBookPupil rbp = lstReviewBook.Where(o => o.PupilID == model.PupilID).FirstOrDefault();
                objPOC = lstPOC.Where(p => p.PupilID == model.PupilID).FirstOrDefault();
                objectIDStrtmp.Append(model.PupilID);
                descriptionStrtmp.Append("Cập nhật sổ đánh giá HS");
                paramsStrtmp.Append(model.PupilID);
                userFuntionsStrtmp.Append("Sổ đánh giá HS");
                userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                userDescriptionsStrtmp.Append("Cập nhật đánh giá bổ sung HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + "Học kỳ II"  + "/Năm học " + objAca.Year + "-" + (objAca.Year + 1) + ". ");
                if (rbp != null)
                {
                    if (model.InputRateAdd == "HT")
                    {
                        if (rbp.RateAdd != PupilEvaluationBookConstants.HT)
                        {
                            oldObjectStrtmp.Append(rbp.RateAdd == PupilEvaluationBookConstants.CHT ? "CHT" : "");
                            rbp.RateAdd = PupilEvaluationBookConstants.HT;
                            isChange = true;
                        }
                    }
                    else if (model.InputRateAdd == "CHT")
                    {
                        if (rbp.RateAdd != PupilEvaluationBookConstants.CHT)
                        {
                            oldObjectStrtmp.Append(rbp.RateAdd == PupilEvaluationBookConstants.HT ? "HT" : "");
                            rbp.RateAdd = PupilEvaluationBookConstants.CHT;
                            isChange = true;
                        }
                    }
                    else
                    {
                        if (rbp.RateAdd.HasValue)
                        {
                            oldObjectStrtmp.Append(rbp.RateAdd == PupilEvaluationBookConstants.HT ? "HT" : rbp.RateAdd == PupilEvaluationBookConstants.CHT ? "CHT" : "");
                            rbp.RateAdd = null;
                            isChange = true;
                        }
                    }
                    if (isChange)
                    {
                        newObjectStrtmp.Append(model.InputRateAdd);
                        rbp.UpdateTime = DateTime.Now;
                        ReviewBookPupilBusiness.Update(rbp);
                    }
                }
                if (isChange)
                {
                    objectIDStr.Append(objectIDStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsmeterStr.Append(paramsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionStr.Append(descriptionStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsStr.Append(userFuntionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsStr.Append(userActionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    oldObjectStr.Append("(Giá trị trước khi sửa): {ĐGBS: ").Append(oldObjectStrtmp).Append("}");
                    newObjectStr.Append("(Giá trị sau khi sửa): {ĐGBS: ").Append(newObjectStrtmp).Append("}");
                    oldObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    newObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                    userDescriptionsStr.Append(userDescriptionsStrtmp).Append("Giá trị cũ: ").Append(oldObjectStrtmp).Append(". ");
                    userDescriptionsStr.Append("Giá trị mới: ").Append(newObjectStrtmp);
                    userDescriptionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                }
                objectIDStrtmp = new StringBuilder();
                descriptionStrtmp = new StringBuilder();
                paramsStrtmp = new StringBuilder();
                oldObjectStrtmp = new StringBuilder();
                newObjectStrtmp = new StringBuilder();
                userFuntionsStrtmp = new StringBuilder();
                userActionsStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
            }
            IDictionary<string,object> dicLogAction = new Dictionary<string, object>()
            {
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsmeterStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
            };
            SetViewDataActionAudit(dicLogAction);
            ReviewBookPupilBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_Save")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public ActionResult DeleteTab1(int classId)
        {
            if (!CheckButtonPermission(classId, GlobalConstants.SEMESTER_OF_YEAR_SECOND))
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            //Lay ra cac hoc sinh co nhap danh gia bo sung
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            
            List<ReviewBookPupil> lstReviewBook = ReviewBookPupilBusiness.All
                .Where(o => o.SchoolID == _globalInfo.SchoolID
                && o.AcademicYearID == _globalInfo.AcademicYearID
                && o.ClassID == classId
                && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND
                && o.PartitionID == partition).ToList();
            List<int> lstPupilID = lstReviewBook.Select(p => p.PupilID).Distinct().ToList();
            #region khai bao doi tuong luu log
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsmeterStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            ClassProfile objCP = ClassProfileBusiness.Find(classId);
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            #endregion
            for (int i = 0; i < lstReviewBook.Count; i++)
            {
                ReviewBookPupil rbp = lstReviewBook[i];
                rbp.RateAndYear = null;
                rbp.RateAdd = null;
                rbp.StudyResult = null;
                rbp.UpdateTime = DateTime.Now;
                ReviewBookPupilBusiness.Update(rbp);
            }
            objectIDStr.Append(classId);
            descriptionStr.Append("Xóa KQ đánh giá HS");
            paramsmeterStr.Append(classId);
            userFuntionsStr.Append("Sổ đánh giá HS");
            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
            userDescriptionsStr.Append("Xóa KQ đánh giá HS ");
            userDescriptionsStr.Append("Lớp ").Append(objCP.DisplayName + "/" + "Học kỳ II" + "/Năm học " + objAca.Year + "-" + (objAca.Year + 1) + ". ");

            ViewData[CommonKey.AuditActionKey.OldJsonObject] = "";
            ViewData[CommonKey.AuditActionKey.NewJsonObject] = "";
            ViewData[CommonKey.AuditActionKey.ObjectID] = objectIDStr;
            ViewData[CommonKey.AuditActionKey.Description] = descriptionStr;
            ViewData[CommonKey.AuditActionKey.Parameter] = paramsmeterStr;
            ViewData[CommonKey.AuditActionKey.userAction] = userActionsStr;
            ViewData[CommonKey.AuditActionKey.userFunction] = userFuntionsStr;
            ViewData[CommonKey.AuditActionKey.userDescription] = userDescriptionsStr;

            ReviewBookPupilBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public ActionResult SaveTab2(List<Tab2ViewModel> lstTab2ViewModel, int classId, int semester)
        {
            if (!CheckButtonPermission(classId, semester))
            {
                throw new BusinessException("Validate_Permission_Teacher");
            } 

            //Lay ra cac hoc sinh duoc check chon
            List<Tab2ViewModel> lstModel = lstTab2ViewModel.Where(o => o.IsCheck == true).ToList();
            
            if (lstModel.Count == 0)
            {
                throw new BusinessException("PupilEvaluationBook_Msg_NoPupil");
            }
            List<int> lstPupilID = lstModel.Select(o=>o.PupilID).ToList();
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            //Lay ra cac danh gia cu
            List<ReviewBookPupil> lstReviewBook = ReviewBookPupilBusiness.All
                .Where(o => o.SchoolID == _globalInfo.SchoolID
                && o.AcademicYearID == _globalInfo.AcademicYearID
                && o.ClassID == classId
                && o.SemesterID == semester
                && o.PartitionID == partition
                && lstPupilID.Contains(o.PupilID)).ToList();
            string LockTitle = string.Empty;
            IDictionary<string, object> dicLock = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"ClassID",classId},
                {"lstEvaluationID",new List<int>(){5,6,7,8}}
            };
            LockTitle = LockRatedCommentPupilBusiness.GetLockTitleVNEN(dicLock);
            #region ghi log du lieu
            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();

            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsmeterStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            bool isChange = false;
            ClassProfile objCP = ClassProfileBusiness.Find(classId);
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                           where poc.AcademicYearID == _globalInfo.AcademicYearID
                                           && poc.SchoolID == _globalInfo.SchoolID
                                           && poc.ClassID == classId
                                           && lstPupilID.Contains(poc.PupilID)
                                           && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               PupilCode = pf.PupilCode,
                                               ClassID = poc.ClassID,
                                               ClassName = cp.DisplayName,
                                               PupilFullName = pf.FullName
                                           }).ToList();
            PupilOfClassBO objPOC = new PupilOfClassBO();

            #endregion

            string strOldCapSummer = string.Empty;
            string strOldQualitySummer = string.Empty;
            for (int i = 0; i < lstModel.Count; i++)
            {
                isChange = false;
                Tab2ViewModel model = lstModel[i];
                //Tim kiem danh gia cua hoc sinh
                ReviewBookPupil reviewOfPupil = lstReviewBook.Where(o => o.PupilID == model.PupilID).FirstOrDefault();
                objPOC = lstPOC.Where(p => p.PupilID == model.PupilID).FirstOrDefault();
                objectIDStrtmp.Append(model.PupilID);
                descriptionStrtmp.Append("Cập nhật đánh giá HS");
                paramsStrtmp.Append(model.PupilID);
                userFuntionsStrtmp.Append("Sổ đánh giá HS");
                userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                userDescriptionsStrtmp.Append("Cập nhật đánh giá HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + "Học kỳ " + semester + "/Năm học " + objAca.Year + "-" + (objAca.Year + 1) + ". ");
                //Neu chua co thi insert
                if (reviewOfPupil == null)
                {
                    if (String.IsNullOrEmpty(model.Comment1) && String.IsNullOrEmpty(model.Comment2) && String.IsNullOrEmpty(model.InputCapacity)
                        && String.IsNullOrEmpty(model.InputCapacitySummer) && String.IsNullOrEmpty(model.InputQuality) && String.IsNullOrEmpty(model.InputQualitySummer))
                    {
                        continue;
                    }
                    else
                    {
                        ReviewBookPupil rbp = new ReviewBookPupil();

                        rbp.ReviewBookPupilID = ReviewBookPupilBusiness.GetNextSeq<int>();
                        rbp.PupilID = model.PupilID;
                        rbp.ClassID = classId;
                        rbp.PartitionID = partition;
                        rbp.SchoolID = _globalInfo.SchoolID.Value;
                        rbp.SemesterID = semester;
                        rbp.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        rbp.CreateTime = DateTime.Now;

                        if (!string.IsNullOrEmpty(model.Comment1) && !LockTitle.Contains("NXHK" + semester))
                        {
                            newObjectStrtmp.Append(log_Comment1).Append(model.Comment1).Append(log_space);
                            oldObjectStrtmp.Append(log_Comment1).Append(rbp.CQComment).Append(log_space);
                            rbp.CQComment = GetSubString1500(model.Comment1);
                            isChange = true;
                        }

                        if (!string.IsNullOrEmpty(model.Comment2) && !LockTitle.Contains("NXHK" + semester))
                        {
                            newObjectStrtmp.Append(log_Comment2).Append(model.Comment2).Append(log_space);
                            oldObjectStrtmp.Append(log_Comment2).Append(rbp.CommentAdd).Append(log_space);
                            rbp.CommentAdd = GetSubString1500(model.Comment2);
                            isChange = true;
                        }

                        if (!string.IsNullOrEmpty(model.InputCapacity) && !LockTitle.Contains("DGHK" + semester))
                        {
                            newObjectStrtmp.Append(log_Capacity).Append(model.InputCapacity).Append(log_space);
                            oldObjectStrtmp.Append(log_Capacity).Append(GetRateFromInt(rbp.CapacityRate)).Append(log_space);
                            rbp.CapacityRate = GetRateFromString(model.InputCapacity);
                            isChange = true;
                        }

                        if (!string.IsNullOrEmpty(model.InputQuality) && !LockTitle.Contains("DGHK" + semester))
                        {
                            newObjectStrtmp.Append(log_CQ).Append(model.InputQuality).Append(log_space);
                            oldObjectStrtmp.Append(log_CQ).Append(GetRateFromInt(rbp.QualityRate)).Append(log_space);
                            rbp.QualityRate = GetRateFromString(model.InputQuality);
                            isChange = true;
                        }

                        if (isChange)
                        {
                            ReviewBookPupilBusiness.Insert(rbp);
                        }
                    }
                }
                    //Neu co thi update
                else
                {
                    if ((!string.IsNullOrEmpty(reviewOfPupil.CQComment) || !string.IsNullOrEmpty(model.Comment1)) && reviewOfPupil.CQComment != model.Comment1 && !LockTitle.Contains("NXHK" + semester))
                    {
                        newObjectStrtmp.Append(log_Comment1).Append(GetSubString1500(model.Comment1)).Append(log_space);
                        oldObjectStrtmp.Append(log_Comment1).Append(reviewOfPupil.CQComment).Append(log_space);
                        reviewOfPupil.CQComment = GetSubString1500(model.Comment1);
                        isChange = true;
                    }

                    if ((!string.IsNullOrEmpty(reviewOfPupil.CommentAdd) || !string.IsNullOrEmpty(model.Comment2)) && reviewOfPupil.CommentAdd != model.Comment2 && !LockTitle.Contains("NXHK" + semester))
                    {
                        newObjectStrtmp.Append(log_Comment2).Append(GetSubString1500(model.Comment2)).Append(log_space);
                        oldObjectStrtmp.Append(log_Comment2).Append(reviewOfPupil.CommentAdd).Append(log_space);
                        reviewOfPupil.CommentAdd = GetSubString1500(model.Comment2);
                        isChange = true;
                    }

                    if (reviewOfPupil.CapacityRate != GetRateFromString(model.InputCapacity) && !LockTitle.Contains("DGHK" + semester))//danh gia nang luc
                    {
                        newObjectStrtmp.Append(log_Capacity).Append(model.InputCapacity).Append(log_space);
                        oldObjectStrtmp.Append(log_Capacity).Append(GetRateFromInt(reviewOfPupil.CapacityRate)).Append(log_space);
                        reviewOfPupil.CapacityRate = GetRateFromString(model.InputCapacity);
                        if (reviewOfPupil.CapacityRate == PupilEvaluationBookConstants.T || reviewOfPupil.CapacityRate == PupilEvaluationBookConstants.D || !reviewOfPupil.CapacityRate.HasValue)
                        {
                            strOldCapSummer = GetRateFromInt(reviewOfPupil.CapacitySummer);
                            reviewOfPupil.CapacitySummer = null;
                        }
                        isChange = true;
                    }

                    if (reviewOfPupil.QualityRate != GetRateFromString(model.InputQuality) && !LockTitle.Contains("DGHK" + semester))//danh gia pham chat
                    {
                        newObjectStrtmp.Append(log_CQ).Append(model.InputQuality).Append(log_space);
                        oldObjectStrtmp.Append(log_CQ).Append(GetRateFromInt(reviewOfPupil.QualityRate)).Append(log_space);
                        reviewOfPupil.QualityRate = GetRateFromString(model.InputQuality);
                        if (reviewOfPupil.QualityRate == PupilEvaluationBookConstants.T || reviewOfPupil.QualityRate == PupilEvaluationBookConstants.D || !reviewOfPupil.QualityRate.HasValue)
                        {
                            strOldQualitySummer = GetRateFromInt(reviewOfPupil.QualitySummer);
                            reviewOfPupil.QualitySummer = null;
                        }
                        isChange = true;
                    }

                    if (reviewOfPupil.CapacitySummer != GetRateFromString(model.InputCapacitySummer) && !LockTitle.Contains("RLTH"))//ren luyen nang luc trong he
                    {
                        if (string.IsNullOrEmpty(strOldCapSummer))
                        {
                            newObjectStrtmp.Append(log_CapacitySummer).Append(model.InputCapacitySummer).Append(log_space);
                            oldObjectStrtmp.Append(log_CapacitySummer).Append(GetRateFromInt(reviewOfPupil.CapacitySummer)).Append(log_space);
                            reviewOfPupil.CapacitySummer = GetRateFromString(model.InputCapacitySummer);
                        }
                        else
                        {
                            newObjectStrtmp.Append(log_CapacitySummer).Append(log_space);
                            oldObjectStrtmp.Append(log_CapacitySummer).Append(strOldCapSummer).Append(log_space);
                        }
                        
                        isChange = true;
                    }

                    if (reviewOfPupil.QualitySummer != GetRateFromString(model.InputQualitySummer) && !LockTitle.Contains("RLTH"))//ren luyen pham chat trong he
                    {
                        if (string.IsNullOrEmpty(strOldQualitySummer))
                        {
                            newObjectStrtmp.Append(log_CQSummer).Append(model.InputQualitySummer).Append(log_space);
                            oldObjectStrtmp.Append(log_CQSummer).Append(GetRateFromInt(reviewOfPupil.QualitySummer)).Append(log_space);
                            reviewOfPupil.QualitySummer = GetRateFromString(model.InputQualitySummer);
                        }
                        else
                        {
                            newObjectStrtmp.Append(log_CQSummer).Append(log_space);
                            oldObjectStrtmp.Append(log_CQSummer).Append(strOldQualitySummer).Append(log_space);
                        }
                        
                        isChange = true;
                    }

                    if (isChange)
                    {
                        reviewOfPupil.UpdateTime = DateTime.Now;
                        ReviewBookPupilBusiness.Update(reviewOfPupil);
                    }
                }
                if (isChange)//chi luu log nhung hoc sinh co thay doi thong tin
                {
                    objectIDStr.Append(objectIDStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsmeterStr.Append(paramsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionStr.Append(descriptionStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsStr.Append(userFuntionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsStr.Append(userActionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                    string tmpOld = string.Empty;
                    string tmpNew = string.Empty;
                    tmpOld = oldObjectStrtmp.Length > 0 ? oldObjectStrtmp.ToString().Substring(0, oldObjectStrtmp.Length - 2) : "";
                    tmpNew = newObjectStrtmp.Length > 0 ? newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) : "";

                    oldObjectStr.Append("(Giá trị trước khi sửa): {").Append(tmpOld).Append("}");
                    newObjectStr.Append("(Giá trị sau khi sửa): {").Append(tmpNew).Append("}");
                    oldObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    newObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                    userDescriptionsStr.Append(userDescriptionsStrtmp).Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                    userDescriptionsStr.Append("Giá trị mới {").Append(tmpNew).Append("}");
                    userDescriptionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                }
                objectIDStrtmp = new StringBuilder();
                descriptionStrtmp = new StringBuilder();
                paramsStrtmp = new StringBuilder();
                oldObjectStrtmp = new StringBuilder();
                newObjectStrtmp = new StringBuilder();
                userFuntionsStrtmp = new StringBuilder();
                userActionsStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
            }
            IDictionary<string, object> dicLogAction = new Dictionary<string, object>()
            {
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsmeterStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
            };
            SetViewDataActionAudit(dicLogAction);
            ReviewBookPupilBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_Save")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public ActionResult DeleteTab2(List<Tab2ViewModel> lstTab2ViewModel, int classId, int semester)
        {
            if (!CheckButtonPermission(classId, semester))
            {
                throw new BusinessException("Validate_Permission_Teacher");
            } 
            //Lay ra cac hoc sinh duoc check chon
            List<Tab2ViewModel> lstModel = lstTab2ViewModel.Where(o => o.IsCheck == true).ToList();

            if (lstModel.Count == 0)
            {
                throw new BusinessException("PupilEvaluationBook_Msg_Delete_NoPupil");
            }
            List<int> lstPupilID = lstModel.Select(o => o.PupilID).ToList();
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            //Lay ra cac danh gia cu
            List<ReviewBookPupil> lstReviewBook = ReviewBookPupilBusiness.All
                .Where(o => o.SchoolID == _globalInfo.SchoolID
                && o.AcademicYearID == _globalInfo.AcademicYearID
                && o.ClassID == classId
                && o.SemesterID == semester
                && o.PartitionID == partition
                && lstPupilID.Contains(o.PupilID)).ToList();
            string LockTitle = string.Empty;
            IDictionary<string, object> dicLock = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"ClassID",classId},
                {"lstEvaluationID",new List<int>(){5,6,7,8}}
            };
            LockTitle = LockRatedCommentPupilBusiness.GetLockTitleVNEN(dicLock);

            #region ghi log du lieu
            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();

            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsmeterStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            bool isChange = false;
            ClassProfile objCP = ClassProfileBusiness.Find(classId);
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                           where poc.AcademicYearID == _globalInfo.AcademicYearID
                                           && poc.SchoolID == _globalInfo.SchoolID
                                           && poc.ClassID == classId
                                           && lstPupilID.Contains(poc.PupilID)
                                           && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               PupilCode = pf.PupilCode,
                                               ClassID = poc.ClassID,
                                               ClassName = cp.DisplayName,
                                               PupilFullName = pf.FullName
                                           }).ToList();
            PupilOfClassBO objPOC = new PupilOfClassBO();

            #endregion

            int pupilID = 0;
            for (int i = 0; i < lstModel.Count; i++)
            {
                isChange = false;
                Tab2ViewModel model = lstModel[i];
                ReviewBookPupil reviewOfPupil = lstReviewBook.Where(o => o.PupilID == model.PupilID).FirstOrDefault();
                pupilID = model.PupilID;

                objPOC = lstPOC.Where(p => p.PupilID == model.PupilID).FirstOrDefault();
                objectIDStrtmp.Append(model.PupilID);
                descriptionStrtmp.Append("Xóa đánh giá HS");
                paramsStrtmp.Append(model.PupilID);
                userFuntionsStrtmp.Append("Sổ đánh giá HS");
                userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                userDescriptionsStrtmp.Append("Xóa đánh giá NLPC HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + "Học kỳ II" + "/Năm học " + objAca.Year + "-" + (objAca.Year + 1) + ". ");

                if (reviewOfPupil != null)
                {
                    if (!string.IsNullOrEmpty(reviewOfPupil.CQComment) && !LockTitle.Contains("NXHK" + semester))
                    {
                        oldObjectStrtmp.Append(log_Comment1).Append(reviewOfPupil.CQComment).Append(log_space);
                        reviewOfPupil.CQComment = null;
                        isChange = true;
                    }

                    if (!string.IsNullOrEmpty(reviewOfPupil.CommentAdd) && !LockTitle.Contains("NXHK" + semester))
                    {
                        oldObjectStrtmp.Append(log_Comment2).Append(reviewOfPupil.CommentAdd).Append(log_space);
                        reviewOfPupil.CommentAdd = null;
                        isChange = true;
                    }

                    if (reviewOfPupil.CapacityRate.HasValue && !LockTitle.Contains("DGHK" + semester))
                    {
                        oldObjectStrtmp.Append(log_Capacity).Append(GetRateFromInt(reviewOfPupil.CapacityRate)).Append(log_space);
                        reviewOfPupil.CapacityRate = null;
                        isChange = true;
                    }

                    if (reviewOfPupil.QualityRate.HasValue && !LockTitle.Contains("DGHK" + semester))
                    {
                        oldObjectStrtmp.Append(log_CQ).Append(GetRateFromInt(reviewOfPupil.QualityRate)).Append(log_space);
                        reviewOfPupil.QualityRate = null;
                        isChange = true;
                    }

                    if (reviewOfPupil.CapacitySummer.HasValue && !LockTitle.Contains("RLTH"))
                    {
                        oldObjectStrtmp.Append(log_CapacitySummer).Append(GetRateFromInt(reviewOfPupil.CapacitySummer)).Append(log_space);
                        reviewOfPupil.CapacitySummer = null;
                        isChange = true;
                    }

                    if (reviewOfPupil.QualitySummer.HasValue && !LockTitle.Contains("RLTH"))
                    {
                        oldObjectStrtmp.Append(log_CQSummer).Append(GetRateFromInt(reviewOfPupil.QualitySummer)).Append(log_space);
                        reviewOfPupil.QualitySummer = null;
                        isChange = true;
                    }
                    
                    if (isChange)
                    {
                        ReviewBookPupilBusiness.Update(reviewOfPupil);
                    }
                }

                if (isChange)//chi luu log nhung hoc sinh co thay doi thong tin
                {
                    objectIDStr.Append(objectIDStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsmeterStr.Append(paramsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionStr.Append(descriptionStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsStr.Append(userFuntionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsStr.Append(userActionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                    string tmpOld = string.Empty;
                    tmpOld = oldObjectStrtmp.Length > 0 ? oldObjectStrtmp.ToString().Substring(0, oldObjectStrtmp.Length - 2) : "";
                    oldObjectStr.Append("(Giá trị trước khi xóa): {").Append(tmpOld).Append("}");
                    oldObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userDescriptionsStr.Append(userDescriptionsStrtmp).Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                    userDescriptionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                }
                objectIDStrtmp = new StringBuilder();
                descriptionStrtmp = new StringBuilder();
                paramsStrtmp = new StringBuilder();
                oldObjectStrtmp = new StringBuilder();
                newObjectStrtmp = new StringBuilder();
                userFuntionsStrtmp = new StringBuilder();
                userActionsStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
            }
            IDictionary<string, object> dicLogAction = new Dictionary<string, object>()
            {
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsmeterStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
            };
            SetViewDataActionAudit(dicLogAction);
            ReviewBookPupilBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetTab1Report(int classId, int semester)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ClassID"] = classId;
            dic["Semester"] = semester;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
          
            string reportCode = SystemParamsInFile.REPORT_PUPIL_EVALUATION_HDGD;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            Stream excel = ReviewBookPupilBusiness.CreatePupilEvaluationHDGDReport(dic);
            processedReport = ReviewBookPupilBusiness.InsertPupilEvaluationHDGDReport(dic, excel);
            excel.Close();
            
            return Json(new JsonReportMessage(processedReport, type));
        }

        public JsonResult GetTab2Report(int classId, int semester)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ClassID"] = classId;
            dic["Semester"] = semester;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;

            string reportCode = SystemParamsInFile.REPORT_PUPIL_EVALUATION_NLPC;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            Stream excel = ReviewBookPupilBusiness.CreatePupilEvaluationNLPCReport(dic);
            processedReport = ReviewBookPupilBusiness.InsertPupilEvaluationNLPCReport(dic, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, type));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
        #endregion

        #region Import
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments, int classId, int semester)
        {
            if (!CheckButtonPermission(classId, semester))
            {
                throw new BusinessException("Validate_Permission_Teacher");
            } 
            if (attachments == null || attachments.Count() <= 0)
            {
                JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
                res.ContentType = "text/plain";
                return res;
            }
            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res.ContentType = "text/plain";
                    return res;
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID +"-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;

                string Error;
                List<Tab2ViewModel> lstModel = getDataToFileImport(physicalPath,classId,semester, out Error);
                //Xoa hoc sinh khac dang hoc
                
                if (Error.Trim().Length > 0)
                {
                    JsonResult res = Json(new JsonMessage(Error, JsonMessage.ERROR));
                    res.ContentType = "text/plain";
                    return res;
                }

                if (lstModel.Where(o => o.IsImportError).Count() > 0)
                {
                    Session["ListModel"] = lstModel;

                    JsonResult res = Json(new { type = "ImportError", classId = classId, semester = semester, ListPupil = lstModel });
                    res.ContentType = "text/plain";
                    return res;
                }

                //Thuc hien luu vao DB
                int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                List<ReviewBookPupil> lstReviewBook = ReviewBookPupilBusiness.All
                    .Where(o => o.SchoolID == _globalInfo.SchoolID
                    && o.AcademicYearID == _globalInfo.AcademicYearID
                    && o.ClassID == classId
                    && o.SemesterID == semester
                    && o.PartitionID == partition).ToList();
                lstModel = lstModel.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING).ToList();
                string LockTitle = string.Empty;
                IDictionary<string, object> dicLock = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID},
                    {"ClassID",classId},
                    {"lstEvaluationID",new List<int>(){5,6,7,8}}
                };
                LockTitle = LockRatedCommentPupilBusiness.GetLockTitleVNEN(dicLock);
                #region ghi log du lieu
                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();

                StringBuilder objectIDStr = new StringBuilder();
                StringBuilder descriptionStr = new StringBuilder();
                StringBuilder paramsmeterStr = new StringBuilder();
                StringBuilder oldObjectStr = new StringBuilder();
                StringBuilder newObjectStr = new StringBuilder();
                StringBuilder userFuntionsStr = new StringBuilder();
                StringBuilder userActionsStr = new StringBuilder();
                StringBuilder userDescriptionsStr = new StringBuilder();
                bool isChange = false;
                ClassProfile objCP = ClassProfileBusiness.Find(classId);
                AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
                List<int> lstPupilID = lstModel.Select(p => p.PupilID).Distinct().ToList();

                List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                               join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                               join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                               where poc.AcademicYearID == _globalInfo.AcademicYearID
                                               && poc.SchoolID == _globalInfo.SchoolID
                                               && poc.ClassID == classId
                                               && lstPupilID.Contains(poc.PupilID)
                                               && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                               select new PupilOfClassBO
                                               {
                                                   PupilID = poc.PupilID,
                                                   PupilCode = pf.PupilCode,
                                                   ClassID = poc.ClassID,
                                                   ClassName = cp.DisplayName,
                                                   PupilFullName = pf.FullName
                                               }).ToList();
                PupilOfClassBO objPOC = new PupilOfClassBO();
                #endregion
                for (int i = 0; i < lstModel.Count; i++)
                {
                    isChange = false;
                    Tab2ViewModel model = lstModel[i];
                    //Tim kiem danh gia cua hoc sinh
                    ReviewBookPupil reviewOfPupil = lstReviewBook.Where(o => o.PupilID == model.PupilID).FirstOrDefault();
                    objPOC = lstPOC.Where(p => p.PupilID == model.PupilID).FirstOrDefault();
                    objectIDStrtmp.Append(model.PupilID);
                    descriptionStrtmp.Append("Import đánh giá NLPC HS");
                    paramsStrtmp.Append(model.PupilID);
                    userFuntionsStrtmp.Append("Sổ đánh giá HS");
                    userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_IMPORT);
                    userDescriptionsStrtmp.Append("Import đánh giá NLPC HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                    userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + "Học kỳ " + semester + "/Năm học " + objAca.Year + "-" + (objAca.Year + 1) + ". ");
                    //Neu chua co thi insert
                    if (reviewOfPupil == null)
                    {
                        if (String.IsNullOrEmpty(model.Comment1) && String.IsNullOrEmpty(model.Comment2) && String.IsNullOrEmpty(model.InputCapacity) && String.IsNullOrEmpty(model.InputQuality))
                        {
                            continue;
                        }
                        else
                        {
                            ReviewBookPupil rbp = new ReviewBookPupil();
                            rbp.ReviewBookPupilID = ReviewBookPupilBusiness.GetNextSeq<int>();
                            rbp.PupilID = model.PupilID;
                            rbp.ClassID = classId;
                            rbp.PartitionID = partition;
                            rbp.SchoolID = _globalInfo.SchoolID.Value;
                            rbp.SemesterID = semester;
                            rbp.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            rbp.CreateTime = DateTime.Now;

                            if (!string.IsNullOrEmpty(model.Comment1) && !LockTitle.Contains("NXHK" + semester))
                            {
                                newObjectStrtmp.Append(log_Comment1).Append(model.Comment1).Append(log_space);
                                oldObjectStrtmp.Append(log_Comment1).Append(rbp.CQComment).Append(log_space);
                                rbp.CQComment = GetSubString1500(model.Comment1);
                                isChange = true;
                            }

                            if (!string.IsNullOrEmpty(model.Comment2) && !LockTitle.Contains("NXHK" + semester))
                            {
                                newObjectStrtmp.Append(log_Comment2).Append(model.Comment2).Append(log_space);
                                oldObjectStrtmp.Append(log_Comment2).Append(rbp.CommentAdd).Append(log_space);
                                rbp.CommentAdd = GetSubString1500(model.Comment2);
                                isChange = true;
                            }

                            if (!string.IsNullOrEmpty(model.InputCapacity) && !LockTitle.Contains("DGHK" + semester))
                            {
                                newObjectStrtmp.Append(log_Capacity).Append(model.InputCapacity).Append(log_space);
                                oldObjectStrtmp.Append(log_Capacity).Append(GetRateFromInt(rbp.CapacityRate)).Append(log_space);
                                rbp.CapacityRate = GetRateFromString(model.InputCapacity);
                                isChange = true;
                            }

                            if (!string.IsNullOrEmpty(model.InputQuality) && !LockTitle.Contains("DGHK" + semester))
                            {
                                newObjectStrtmp.Append(log_CQ).Append(model.InputQuality).Append(log_space);
                                oldObjectStrtmp.Append(log_CQ).Append(GetRateFromInt(rbp.QualityRate)).Append(log_space);
                                rbp.QualityRate = GetRateFromString(model.InputQuality);
                                isChange = true;
                            }

                            if (isChange)
                            {
                                ReviewBookPupilBusiness.Insert(rbp);
                            }
                        }
                    }
                    //Neu co thi update
                    else
                    {
                        if ((!string.IsNullOrEmpty(reviewOfPupil.CQComment) || !string.IsNullOrEmpty(model.Comment1)) && reviewOfPupil.CQComment != model.Comment1 && !LockTitle.Contains("NXHK" + semester))
                        {
                            newObjectStrtmp.Append(log_Comment1).Append(GetSubString1500(model.Comment1)).Append(log_space);
                            oldObjectStrtmp.Append(log_Comment1).Append(reviewOfPupil.CQComment).Append(log_space);
                            reviewOfPupil.CQComment = GetSubString1500(model.Comment1);
                            isChange = true;
                        }

                        if ((!string.IsNullOrEmpty(reviewOfPupil.CommentAdd) || !string.IsNullOrEmpty(model.Comment2)) && reviewOfPupil.CommentAdd != model.Comment2 && !LockTitle.Contains("NXHK" + semester))
                        {
                            newObjectStrtmp.Append(log_Comment2).Append(GetSubString1500(model.Comment2)).Append(log_space);
                            oldObjectStrtmp.Append(log_Comment2).Append(reviewOfPupil.CommentAdd).Append(log_space);
                            reviewOfPupil.CommentAdd = GetSubString1500(model.Comment2);
                            isChange = true;
                        }

                        if (reviewOfPupil.CapacityRate != GetRateFromString(model.InputCapacity) && !LockTitle.Contains("DGHK" + semester))//danh gia nang luc
                        {
                            newObjectStrtmp.Append(log_Capacity).Append(model.InputCapacity).Append(log_space);
                            oldObjectStrtmp.Append(log_Capacity).Append(GetRateFromInt(reviewOfPupil.CapacityRate)).Append(log_space);
                            reviewOfPupil.CapacityRate = GetRateFromString(model.InputCapacity);
                            if (reviewOfPupil.CapacityRate == PupilEvaluationBookConstants.T || reviewOfPupil.CapacityRate == PupilEvaluationBookConstants.D)
                            {
                                reviewOfPupil.CapacitySummer = null;
                            }
                            isChange = true;
                        }

                        if (reviewOfPupil.QualityRate != GetRateFromString(model.InputQuality) && !LockTitle.Contains("DGHK" + semester))//danh gia pham chat
                        {
                            newObjectStrtmp.Append(log_CQ).Append(model.InputQuality).Append(log_space);
                            oldObjectStrtmp.Append(log_CQ).Append(GetRateFromInt(reviewOfPupil.QualityRate)).Append(log_space);
                            reviewOfPupil.QualityRate = GetRateFromString(model.InputQuality);
                            if (reviewOfPupil.QualityRate == PupilEvaluationBookConstants.T || reviewOfPupil.QualityRate == PupilEvaluationBookConstants.D)
                            {
                                reviewOfPupil.QualitySummer = null;
                            }
                            isChange = true;
                        }

                        if (isChange)
                        {
                            reviewOfPupil.UpdateTime = DateTime.Now;
                            ReviewBookPupilBusiness.Update(reviewOfPupil);
                        }
                    }
                    if (isChange)//chi luu log nhung hoc sinh co thay doi thong tin
                    {
                        objectIDStr.Append(objectIDStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramsmeterStr.Append(paramsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionStr.Append(descriptionStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFuntionsStr.Append(userFuntionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActionsStr.Append(userActionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                        string tmpOld = string.Empty;
                        string tmpNew = string.Empty;
                        tmpOld = oldObjectStrtmp.Length > 0 ? oldObjectStrtmp.ToString().Substring(0, oldObjectStrtmp.Length - 2) : "";
                        tmpNew = newObjectStrtmp.Length > 0 ? newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) : "";

                        oldObjectStr.Append("(Giá trị trước khi import): {").Append(tmpOld).Append("}");
                        newObjectStr.Append("(Giá trị sau khi import): {").Append(tmpNew).Append("}");
                        oldObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        newObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                        userDescriptionsStr.Append(userDescriptionsStrtmp).Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                        userDescriptionsStr.Append("Giá trị mới {").Append(tmpNew).Append("}");
                        userDescriptionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    }
                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                }
                IDictionary<string, object> dicLogAction = new Dictionary<string, object>()
                {
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsmeterStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
                SetViewDataActionAudit(dicLogAction);
                ReviewBookPupilBusiness.Save();

                return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage")));
            }

            JsonResult res1 = Json(new JsonMessage(Res.Get("Common_Label_NoFileSelected"), JsonMessage.ERROR));
            res1.ContentType = "text/plain";
            return res1;
        }

        public PartialViewResult GetViewListToImport(int classId, int semester)
        {

            List<Tab2ViewModel> lstModel = (List<Tab2ViewModel>)Session["ListModel"];
            ViewData[PupilEvaluationBookConstants.CLASS_ID] = classId;
            ViewData[PupilEvaluationBookConstants.DEFAULT_SEMESTER] = semester;

            return PartialView("_ImportError", lstModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveErrImport(int classId, int semester)
        {
            List<Tab2ViewModel> lstModel = (List<Tab2ViewModel>)Session["ListModel"];
            lstModel = lstModel.Where(o => o.IsImportError == false).ToList();
            //Thuc hien luu vao DB
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<ReviewBookPupil> lstReviewBook = ReviewBookPupilBusiness.All
                .Where(o => o.SchoolID == _globalInfo.SchoolID
                && o.AcademicYearID == _globalInfo.AcademicYearID
                && o.ClassID == classId
                && o.SemesterID == semester
                && o.PartitionID == partition).ToList();
            string LockTitle = string.Empty;
            IDictionary<string, object> dicLock = new Dictionary<string, object>()
                {
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID},
                    {"ClassID",classId},
                    {"lstEvaluationID",new List<int>(){5,6,7,8}}
                };
            LockTitle = LockRatedCommentPupilBusiness.GetLockTitleVNEN(dicLock);
            for (int i = 0; i < lstModel.Count; i++)
            {
                Tab2ViewModel model = lstModel[i];
                if (model.Status != GlobalConstants.PUPIL_STATUS_STUDYING) continue;
                //Tim kiem danh gia cua hoc sinh
                ReviewBookPupil reviewOfPupil = lstReviewBook.Where(o => o.PupilID == model.PupilID).FirstOrDefault();
                //Neu chua co thi insert
                if (reviewOfPupil == null)
                {
                    if (String.IsNullOrEmpty(model.Comment1) && String.IsNullOrEmpty(model.Comment2) && String.IsNullOrEmpty(model.InputCapacity) && String.IsNullOrEmpty(model.InputQuality))
                    {
                        continue;
                    }
                    else
                    {
                        ReviewBookPupil rbp = new ReviewBookPupil();

                        rbp.ReviewBookPupilID = ReviewBookPupilBusiness.GetNextSeq<int>();
                        rbp.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        rbp.CapacityRate = !LockTitle.Contains("DGHK" + semester) ? GetRateFromString(model.InputCapacity) : null;
                        rbp.ClassID = classId;
                        rbp.CommentAdd = !LockTitle.Contains("NXHK" + semester) ? GetSubString1500(model.Comment2) : "";
                        rbp.CQComment = !LockTitle.Contains("NXHK" + semester) ? GetSubString1500(model.Comment1) : "";
                        rbp.CreateTime = DateTime.Now;
                        rbp.PartitionID = partition;
                        rbp.PupilID = model.PupilID;
                        rbp.QualityRate = !LockTitle.Contains("DGHK" + semester) ? GetRateFromString(model.InputQuality) : null;
                        rbp.SchoolID = _globalInfo.SchoolID.Value;
                        rbp.SemesterID = semester;

                        ReviewBookPupilBusiness.Insert(rbp);
                    }
                }
                //Neu co thi update
                else
                {
                    reviewOfPupil.CQComment = !LockTitle.Contains("NXHK" + semester) ? GetSubString1500(model.Comment1) : "";
                    reviewOfPupil.CommentAdd = !LockTitle.Contains("NXHK" + semester) ? GetSubString1500(model.Comment2) : "";
                    reviewOfPupil.CapacityRate = !LockTitle.Contains("DGHK" + semester) ? GetRateFromString(model.InputCapacity) : null;
                    if (reviewOfPupil.CapacityRate == PupilEvaluationBookConstants.D && !LockTitle.Contains("RLTH"))
                    {
                        reviewOfPupil.CapacitySummer = null;
                    }
                    reviewOfPupil.QualityRate = !LockTitle.Contains("DGHK" + semester) ? GetRateFromString(model.InputQuality) : null;
                    if (reviewOfPupil.QualityRate == PupilEvaluationBookConstants.D && !LockTitle.Contains("RLTH"))
                    {
                        reviewOfPupil.QualitySummer = null;
                    }
                    reviewOfPupil.UpdateTime = DateTime.Now;

                    ReviewBookPupilBusiness.Update(reviewOfPupil);
                }

            }

            ReviewBookPupilBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage")));
        }
        #endregion

        #region Private methods
        private void SetViewData(int? classID)
        {
            
            //Lay danh sach khoi
            ViewData[PupilEvaluationBookConstants.LIST_EDUCATION_LEVEL] = _globalInfo.EducationLevels;
            //Lay danh sach lop
            List<ClassProfile> lstClass = GetListClas();
            ViewData[PupilEvaluationBookConstants.LIST_CLASS] = lstClass;
            //Lay lop mac dinh
            ClassProfile selectedCp = lstClass.Where(o => o.ClassProfileID == classID).FirstOrDefault();
            if (selectedCp == null)
            {
                selectedCp = lstClass.FirstOrDefault();
            }
            
            
            ViewData[PupilEvaluationBookConstants.SELECTED_CLASS] = selectedCp;
            
            //Lay danh sach hoc ky
            List<ViettelCheckboxList> lstSemester = new List<ViettelCheckboxList>();
            lstSemester.Add(new ViettelCheckboxList() { Label = Res.Get("Common_Label_Semester1"), Value = 1 });
            lstSemester.Add(new ViettelCheckboxList() { Label = Res.Get("Common_Label_Semester2"), Value = 2 });
            lstSemester.Add(new ViettelCheckboxList() { Label = Res.Get("Common_Label_AllYear"), Value = 3 });
            ViewData[PupilEvaluationBookConstants.LIST_SEMESTER] = lstSemester;
            //Lay hoc ky mac dinh
            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime nowDate = DateTime.Now.Date;
            int defaultSemester;
            //nam hoc hien tai
            if (nowDate > ay.SecondSemesterEndDate.GetValueOrDefault().Date)
            {
                defaultSemester = 2;
            }
            else if (DateTime.Compare(ay.SecondSemesterStartDate.GetValueOrDefault().Date, nowDate) <= 0 && DateTime.Compare(nowDate, ay.SecondSemesterEndDate.GetValueOrDefault().Date) <= 0)
            {
                defaultSemester = 2;
            }
            else if (DateTime.Compare(ay.FirstSemesterStartDate.GetValueOrDefault().Date, nowDate) <= 0 && DateTime.Compare(nowDate, ay.FirstSemesterEndDate.GetValueOrDefault().Date) <= 0)
            {
                defaultSemester = 1;
            }
            else
            {
                defaultSemester = 1;
            }

            ViewData[PupilEvaluationBookConstants.DEFAULT_SEMESTER] = defaultSemester;
        }

        private List<ClassProfile> GetListClas()
        {
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.getClassByRoleAppliedLevel(_globalInfo.AcademicYearID.GetValueOrDefault()
                , _globalInfo.SchoolID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID).Where(o=>o.IsVnenClass == true).ToList();

            return lstClassProfile;
        }

        private List<ClassSubject> GetSubjectOfClass(int classID, int semester)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = classID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                dic["Semester"] = semester;
            }
            IEnumerable<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList().OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName);

            List<ClassSubject> lstResult = new List<ClassSubject>();
            lstResult.AddRange(lstClassSubject.Where(o => o.IsSubjectVNEN == true && o.IsCommenting != 1));
            lstResult.AddRange(lstClassSubject.Where(o => o.IsSubjectVNEN == true && o.IsCommenting == 1));
            lstResult.AddRange(lstClassSubject.Where(o => o.IsSubjectVNEN != true && o.IsCommenting != 1));
            lstResult.AddRange(lstClassSubject.Where(o => o.IsSubjectVNEN != true && o.IsCommenting == 1));
            return lstResult;
        }

        private void _SearchTab1(int classId, int semester)
        {
            
            //Lay danh sach mon hoc
            lstSubject = GetSubjectOfClass(classId,semester);

            //Lay danh sach hoc sinh trong lop
            lstTab1ViewModel =new List<Tab1ViewModel>();

            //Danh sach cac mon hoc Vnen cua lop
            List<ClassSubject> lstVnenSubject = lstSubject.Where(o => o.IsSubjectVNEN == true).ToList();
            List<int> lstVnenSubjectId = lstVnenSubject.Select(o => o.SubjectID).ToList();
            //Danh sach cac mon khong phai mon Vnen
            List<ClassSubject> lstTT58Subject = lstSubject.Where(o => o.IsSubjectVNEN != true).ToList();
            List<int> lstTT58SubjectId = lstTT58Subject.Select(o => o.SubjectID).ToList();
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value;
            ClassProfile cp = ClassProfileBusiness.Find(classId);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = cp.EducationLevelID;
            dic["ClassID"] = classId;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            IQueryable<PupilOfClass> lstPoc = PupilOfClassBusiness.Search(dic).AddPupilStatus(isNotShowPupil).OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName);

            lstTab1ViewModel = lstPoc.Select(o => new Tab1ViewModel
            {
                PupilID = o.PupilID,
                PupilName = o.PupilProfile.FullName,
                Status = o.Status,
                EndDate = o.EndDate
            }).ToList();

            //Lay danh gia cac mon cua hoc sinh
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<TeacherNoteBookSemesterBO> lstNoteBook = (from tnbs in TeacherNoteBookSemesterBusiness.All
                                                           join cs in ClassSubjectBusiness.All on new {tnbs.ClassID,tnbs.SubjectID } equals new {cs.ClassID, cs.SubjectID }
                                                           where tnbs.SchoolID == _globalInfo.SchoolID
                                                           && tnbs.AcademicYearID == _globalInfo.AcademicYearID
                                                           && tnbs.ClassID == classId
                                                               //&& tnbs.SemesterID==semester
                                                           && tnbs.PartitionID == partition
                                                           && lstVnenSubjectId.Contains(tnbs.SubjectID)
                                                           select new TeacherNoteBookSemesterBO
                                                               {
                                                                   PupilID = tnbs.PupilID,
                                                                   SubjectID = tnbs.SubjectID,
                                                                   PERIODIC_SCORE_END = tnbs.PERIODIC_SCORE_END,
                                                                   PERIODIC_SCORE_END_JUDGLE = tnbs.PERIODIC_SCORE_END_JUDGLE,
                                                                   AVERAGE_MARK = tnbs.AVERAGE_MARK,
                                                                   AVERAGE_MARK_JUDGE = tnbs.AVERAGE_MARK_JUDGE,
                                                                   Rate = tnbs.Rate,
                                                                   SemesterID = tnbs.SemesterID,
                                                                   isCommenting = cs.IsCommenting,
                                                                   AppliedType = cs.AppliedType
                                                               }).ToList();
            //Lay diem mon hoc theo TT58
            dic = new Dictionary<string, object>();
            dic["ClassID"] = classId;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = semester;

            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(ay);

            List<SummedUpRecordBO> lstSummedUpRecord;
            if (isMovedHistory)
            {
                lstSummedUpRecord = SummedUpRecordHistoryBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                    .Where(o=>o.PeriodID==null)
                    .Where(o => lstTT58SubjectId.Contains(o.SubjectID))
                    .Select(o => new SummedUpRecordBO()
                    {
                        PupilID=o.PupilID,
                        SubjectID = o.SubjectID,
                        SummedUpMark = o.SummedUpMark,
                        JudgementResult = o.JudgementResult,
                        Semester = o.Semester
                    }).ToList();

            }
            else
            {
                lstSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                    .Where(o => o.PeriodID == null)
                    .Where(o => lstTT58SubjectId.Contains(o.SubjectID))
                    .Select(o => new SummedUpRecordBO()
                    {
                        PupilID = o.PupilID,
                        SubjectID = o.SubjectID,
                        SummedUpMark = o.SummedUpMark,
                        JudgementResult = o.JudgementResult,
                        Semester = o.Semester
                    }).ToList();
            }

            //Join voi ClassSubject de lay isCommenting
            lstSummedUpRecord = (from sur in lstSummedUpRecord
                                 join cs in lstSubject on sur.SubjectID equals cs.SubjectID
                                 select new SummedUpRecordBO
                                 {
                                     PupilID = sur.PupilID,
                                     SubjectID = sur.SubjectID,
                                     SummedUpMark = sur.SummedUpMark,
                                     JudgementResult = sur.JudgementResult,
                                     Semester = sur.Semester,
                                     IsCommenting = cs.IsCommenting,
                                     AppliedType = cs.AppliedType
                                 }).ToList();

            //Lay thong tin mien giam
            dic = new Dictionary<string, object>();
            dic["ClassID"] = classId;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;

            List<ExemptedSubject> lstExemptedSubjects = ExemptedSubjectBusiness.Search(dic).ToList();

            List<ExemptedSubject> lstExemptedSubjectsSemester1 = lstExemptedSubjects.Where(o => o.FirstSemesterExemptType.HasValue).ToList();
            List<ExemptedSubject> lstExemptedSubjectsSemester2 = lstExemptedSubjects.Where(o => o.SecondSemesterExemptType.HasValue).ToList();
            

            //Lay thong tin dang ky mon tu chon
            dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["ClassID"] = classId;

            List<RegisterSubjectSpecialize> lstSubjectSpecializes = RegisterSubjectSpecializeBusiness.Search(dic).ToList();
            List<RegisterSubjectSpecialize> lstSubjectSpecializeSemester1 = lstSubjectSpecializes.Where(o => o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST).ToList();
            List<RegisterSubjectSpecialize> lstSubjectSpecializeSemester2 = lstSubjectSpecializes.Where(o => o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND).ToList();
            

            //Nang luc pham chat
            IQueryable<ReviewBookPupil> iqReviewBook = ReviewBookPupilBusiness.All
                .Where(o => o.SchoolID == _globalInfo.SchoolID
                && o.AcademicYearID == _globalInfo.AcademicYearID
                && o.ClassID == classId
                && o.PartitionID == partition);
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                iqReviewBook = iqReviewBook.Where(p => p.SemesterID == semester);
            }
            else
            {
                iqReviewBook = iqReviewBook.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND);
            }
            List<ReviewBookPupil> lstReviewBook = iqReviewBook.ToList();

            DateTime endsemester = semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? ay.FirstSemesterEndDate.Value : ay.SecondSemesterEndDate.Value;
            for (int i = 0; i < lstTab1ViewModel.Count; i++)
            {
                Tab1ViewModel model = lstTab1ViewModel[i];
                bool isShow = model.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || model.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED;
                isShow = isShow || ((model.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF
                    || model.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                    || model.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                    && model.EndDate.HasValue && model.EndDate.Value > endsemester);

                if (!isShow)
                {
                    model.IsShowData = false;
                    continue;
                }

                model.IsShowData = true;
                
                //Lay ra cac mon mien giam cua hoc sinh nay
                List<int> lstExemptedSubjectIDSemester1 = lstExemptedSubjectsSemester1.Where(o => o.PupilID == model.PupilID).Select(o => o.SubjectID).ToList();
                List<int> lstExemptedSubjectIDSemester2 = lstExemptedSubjectsSemester2.Where(o => o.PupilID == model.PupilID).Select(o => o.SubjectID).ToList();

                //Lay ra cac mon tu chon hoc sinh nay hoc
                List<int> lstSubjectSpecializeIDSemester1 = lstSubjectSpecializeSemester1.Where(o => o.PupilID == model.PupilID).Select(o => o.SubjectID).ToList();
                List<int> lstSubjectSpecializeIDSemester2 = lstSubjectSpecializeSemester2.Where(o => o.PupilID == model.PupilID).Select(o => o.SubjectID).ToList();

                //Danh gia cac mon
                model.lstSubjectEvaluation = new List<PupilSubjectEvaluationModel>();
                List<TeacherNoteBookSemesterBO> lstNoteBoolOfPupil = lstNoteBook.Where(o => o.PupilID == model.PupilID).ToList();
                for (int j = 0; j < lstVnenSubject.Count; j++)
                {
                    ClassSubject cs = lstVnenSubject[j];
                    PupilSubjectEvaluationModel subjectModel = new PupilSubjectEvaluationModel();

                    subjectModel.SubjectID = cs.SubjectID;
                    subjectModel.IsVnenSubject = true;

                    TeacherNoteBookSemesterBO tnbs;
                    if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        subjectModel.IsExempted = lstExemptedSubjectIDSemester1.Contains(cs.SubjectID);
                        subjectModel.IsNotLearning = (cs.AppliedType == 1 || cs.AppliedType == 2 || cs.AppliedType == 3) && !lstSubjectSpecializeIDSemester1.Contains(cs.SubjectID);

                        tnbs = lstNoteBoolOfPupil.Where(o => o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && o.SubjectID == cs.SubjectID).FirstOrDefault();
        
                    }
                    else if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                    {
                        subjectModel.IsExempted = lstExemptedSubjectIDSemester2.Contains(cs.SubjectID);
                        subjectModel.IsNotLearning = (cs.AppliedType == 1 || cs.AppliedType == 2 || cs.AppliedType == 3) && !lstSubjectSpecializeIDSemester2.Contains(cs.SubjectID);
                        tnbs = lstNoteBoolOfPupil.Where(o => o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && o.SubjectID == cs.SubjectID).FirstOrDefault();
                    }
                    else
                    {
                        subjectModel.IsExempted = lstExemptedSubjectIDSemester1.Contains(cs.SubjectID) && lstExemptedSubjectIDSemester2.Contains(cs.SubjectID);
                        subjectModel.IsNotLearning = (cs.AppliedType == 1 || cs.AppliedType == 2 || cs.AppliedType == 3) && !lstSubjectSpecializeIDSemester1.Contains(cs.SubjectID)
                            && !lstSubjectSpecializeIDSemester2.Contains(cs.SubjectID);

                        tnbs = lstNoteBoolOfPupil.Where(o => o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_ALL && o.SubjectID == cs.SubjectID).FirstOrDefault();
                    }

                    if (tnbs != null)
                    {
                        subjectModel.AVERAGE_MARK = tnbs.AVERAGE_MARK;
                        subjectModel.AVERAGE_JUDGE = tnbs.AVERAGE_MARK_JUDGE;
                        subjectModel.IsCommenting = tnbs.isCommenting;
                    }

                    model.lstSubjectEvaluation.Add(subjectModel);
                }

                List<SummedUpRecordBO> lstSummedUpRecordOfPupil = lstSummedUpRecord.Where(o => o.PupilID == model.PupilID).ToList();
                model.lstSubjectEvaluation.AddRange(lstSummedUpRecordOfPupil.Select(o => new PupilSubjectEvaluationModel
                {
                    SubjectID = o.SubjectID,
                    IsCommenting = o.IsCommenting,
                    IsVnenSubject = false,
                    JudgementResult = o.JudgementResult,
                    SummedUpMark = o.SummedUpMark,
                    IsExempted = semester == 1 ? lstExemptedSubjectIDSemester1.Contains(o.SubjectID.GetValueOrDefault()) :
                                 semester == 2 ? lstExemptedSubjectIDSemester2.Contains(o.SubjectID.GetValueOrDefault()) :
                                 lstExemptedSubjectIDSemester1.Contains(o.SubjectID.GetValueOrDefault()) && lstExemptedSubjectIDSemester2.Contains(o.SubjectID.GetValueOrDefault()),
                    IsNotLearning = semester == 1 ? (o.AppliedType == 1 || o.AppliedType == 2 || o.AppliedType == 3) && !lstSubjectSpecializeIDSemester1.Contains(o.SubjectID.GetValueOrDefault()) :
                                    semester == 2 ? (o.AppliedType == 1 || o.AppliedType == 2 || o.AppliedType == 3) && !lstSubjectSpecializeIDSemester2.Contains(o.SubjectID.GetValueOrDefault()) :
                                    (o.AppliedType == 1 || o.AppliedType == 2 || o.AppliedType == 3) && !lstSubjectSpecializeIDSemester1.Contains(o.SubjectID.GetValueOrDefault())
                                    && !lstSubjectSpecializeIDSemester2.Contains(o.SubjectID.GetValueOrDefault())
                }).ToList());

                //Lay ra cac mon chua duoc add vao model
                List<int> lstSubjectID=model.lstSubjectEvaluation.Select(o=>o.SubjectID.GetValueOrDefault()).ToList();
                List<ClassSubject> lstSubjectNoResult = lstSubject.Where(o => !lstSubjectID.Contains(o.SubjectID)).ToList();
                //Add ket qua gia cho cac mon chua co ket qua
                for (int j = 0; j < lstSubjectNoResult.Count; j++)
                {
                    ClassSubject cs = lstSubjectNoResult[j];
                    PupilSubjectEvaluationModel subjectEvaluationModel = new PupilSubjectEvaluationModel();

                    subjectEvaluationModel.IsCommenting = cs.IsCommenting;
                    subjectEvaluationModel.IsVnenSubject = cs.IsSubjectVNEN == true ? true : false;
                    subjectEvaluationModel.SubjectID = cs.SubjectID;
                    subjectEvaluationModel.IsExempted = semester == 1 ? lstExemptedSubjectIDSemester1.Contains(cs.SubjectID) :
                                                        semester == 2 ? lstExemptedSubjectIDSemester2.Contains(cs.SubjectID) :
                                                        lstExemptedSubjectIDSemester1.Contains(cs.SubjectID) && lstExemptedSubjectIDSemester2.Contains(cs.SubjectID);
                    subjectEvaluationModel.IsNotLearning = semester == 1 ? (cs.AppliedType == 1 || cs.AppliedType == 2 || cs.AppliedType == 3) && !lstSubjectSpecializeIDSemester1.Contains(cs.SubjectID) :
                                                            semester == 2 ? (cs.AppliedType == 1 || cs.AppliedType == 2 || cs.AppliedType == 3) && !lstSubjectSpecializeIDSemester2.Contains(cs.SubjectID) :
                                                            (cs.AppliedType == 1 || cs.AppliedType == 2 || cs.AppliedType == 3) && !lstSubjectSpecializeIDSemester1.Contains(cs.SubjectID)
                                                                && !lstSubjectSpecializeIDSemester2.Contains(cs.SubjectID);

                    model.lstSubjectEvaluation.Add(subjectEvaluationModel);
                }
                //Nang luc, pham chat
                ReviewBookPupil rvp = lstReviewBook.Where(o => o.PupilID == model.PupilID).FirstOrDefault();
                if (rvp != null)
                {
                       
                    model.Capacity = rvp.CapacityRate;
                    if (rvp.CapacitySummer != null)
                    {
                        model.Capacity = rvp.CapacitySummer;
                    }
                    model.Quality = rvp.QualityRate;
                    if (rvp.QualitySummer != null)
                    {
                        model.Quality = rvp.QualitySummer;
                    }
                    model.StudyResult = rvp.StudyResult;
                    model.RateEndYear = rvp.RateAndYear;
                    model.RateAdd = rvp.RateAdd;
                }
            }

        }

        private void _SearchTab2(int classId, int semester)
        {
            lstTab2ViewModel = new List<Tab2ViewModel>();
            //Lay danh sach hoc sinh trong lop
            ClassProfile cp = ClassProfileBusiness.Find(classId);
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = cp.EducationLevelID;
            dic["ClassID"] = classId;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            IQueryable<PupilOfClass> lstPoc = PupilOfClassBusiness.Search(dic).AddPupilStatus(isNotShowPupil).OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName);

            lstTab2ViewModel = lstPoc.Select(o => new Tab2ViewModel
            {
                PupilID = o.PupilID,
                PupilName = o.PupilProfile.FullName,
                Status=o.Status,
                EndDate=o.EndDate
            }).ToList();

            //Lay thong tin danh gia nang luc pham chat
            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<ReviewBookPupil> lstReviewBook = ReviewBookPupilBusiness.All
                .Where(o => o.SchoolID == _globalInfo.SchoolID
                && o.AcademicYearID == _globalInfo.AcademicYearID
                && o.ClassID == classId
                && o.SemesterID == semester
                && o.PartitionID == partition).ToList();

            //Lay thong tin nhan xet cac mon 
            int monthId = semester == 1 ? 15 : 16;
            List<TeacherNoteBookMonthBO> lstTeacherNoteBookMonth = (from tnbm in TeacherNoteBookMonthBusiness.All
                                                                    join sc in SubjectCatBusiness.All on tnbm.SubjectID equals sc.SubjectCatID
                                                                    join cs in ClassSubjectBusiness.All on new { tnbm.ClassID, tnbm.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                                    where tnbm.SchoolID == _globalInfo.SchoolID
                                                                    && tnbm.AcademicYearID == _globalInfo.AcademicYearID
                                                                    && tnbm.ClassID == classId
                                                                    && tnbm.MonthID == monthId
                                                                    && cs.IsSubjectVNEN.HasValue && cs.IsSubjectVNEN.Value
                                                                    select new TeacherNoteBookMonthBO
                                                                    {
                                                                        PupilID = tnbm.PupilID,
                                                                        SubjectID = tnbm.SubjectID,
                                                                        SubjectName = sc.DisplayName,
                                                                        CommentCQ = tnbm.CommentCQ,
                                                                        OrderInSubject = sc.OrderInSubject
                                                                    }).OrderBy(o => o.OrderInSubject).ThenBy(o => o.SubjectName).ToList();

            AcademicYear ay = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime endsemester = semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? ay.FirstSemesterEndDate.Value : ay.SecondSemesterEndDate.Value;

            for (int i = 0; i < lstTab2ViewModel.Count; i++)
            {
                Tab2ViewModel model = lstTab2ViewModel[i];
                
                bool isShow = model.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || model.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED;
                isShow = isShow || ((model.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF
                    || model.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                    || model.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                    && model.EndDate.HasValue && model.EndDate.Value > endsemester);

                if (!isShow)
                {
                    model.IsShowData = false;
                    continue;
                }

                model.IsShowData = true;

                ReviewBookPupil review = lstReviewBook.Where(o => o.PupilID == model.PupilID).FirstOrDefault();
                if (review != null)
                {
                    model.Comment1 = review.CQComment;
                    model.Comment2 = review.CommentAdd;
                    model.Capacity = review.CapacityRate;
                    model.Quality = review.QualityRate;
                    model.CapacitySummer = review.CapacitySummer;
                    model.QualitySummer = review.QualitySummer;
                }

                //Neu khong co bieu hien noi bat thi load tu so tay giao vien
                if (String.IsNullOrEmpty(model.Comment1))
                {
                    List<TeacherNoteBookMonthBO> lstTnbOfPupil = lstTeacherNoteBookMonth.Where(o => o.PupilID == model.PupilID).ToList();
                    string comment = String.Empty;
                    for (int j = 0; j < lstTnbOfPupil.Count; j++)
                    {
                        TeacherNoteBookMonthBO tnbm = lstTnbOfPupil[j];
                        if (!string.IsNullOrEmpty(tnbm.CommentCQ))
                        {
                            comment += tnbm.SubjectName + ": " + tnbm.CommentCQ;
                            if (j < lstTnbOfPupil.Count - 1)
                            {
                                comment += "\n";
                            }    
                        }
                    }
                    model.Comment1 = comment;
                }
            }
        }

        private int? GetRateFromString(string s)
        {
            if (s == "T")
            {
               return PupilEvaluationBookConstants.T;
            }
            else if (s == "Đ")
            {
                return PupilEvaluationBookConstants.D;
            }
            else if (s == "C")
            {
                return PupilEvaluationBookConstants.C;
            }
            return null;
        }

        private string GetRateFromInt(int? s)
        {
            if (s == PupilEvaluationBookConstants.T)
            {
                return "T";
            }
            else if (s == PupilEvaluationBookConstants.D)
            {
                return "Đ";
            }
            else if (s == PupilEvaluationBookConstants.C)
            {
                return "C";
            }
            return string.Empty;
        }

        private string GetSubString1500(string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                return s;
            }
            if (s.Length > 1500)
            {
                return s.Substring(0, 1500);
            }
            else
            {
                return s;
            }
        }

        public List<Tab2ViewModel> getDataToFileImport(string filename, int classId, int semester, out string Error)
        {
           
            int startRow = 7;

            string FilePath = filename;

            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
            IVTWorksheet sheet = oBook.GetSheet(1);

            ClassProfile classProfile = ClassProfileBusiness.Find(classId);

            #region Kiem tra du lieu chon dau vao
            Error = String.Empty;
            string ClassName = classProfile.DisplayName;

            string Title = (string)sheet.GetCellValue("A3");

            if (string.IsNullOrEmpty(Title) || !Title.Contains("BẢNG TỔNG HỢP NHẬN XÉT ĐANH GIÁ NĂNG LỰC, PHẨM CHẤT"))
            {
                Error = "File Excel không hợp lệ";
                return (null);
            }

            if (!Title.ToUpper().Contains(ClassName.ToUpper()))
            {
                Error = "File excel không phải là file tổng hợp nhận xét đánh giá của lớp " + ClassName;
                return (null);
            }

            Title = sheet.GetCellValue("A4").ToString();
            string semesterName = semester == 1 ? "HỌC KỲ I" : "HỌC KỲ II";
            if (!Title.ToUpper().Contains(semesterName))
            {
                Error = "Học kỳ trong file Excel không đúng với học kỳ đang thao tác ";
                return null;
            }

            
            #endregion
            //kiem tra cac cot trong bang csdl  xem da chuan chua
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = classProfile.EducationLevelID;
            dic["ClassID"] = classId;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            List<PupilOfClass> lstPoc = PupilOfClassBusiness.Search(dic).AddPupilStatus(isNotShowPupil).ToList();

            List<Tab2ViewModel> lstModel = new List<Tab2ViewModel>();
            List<string> lstPupilCode = new List<string>();

            while (sheet.GetCellValue(startRow, 1) != null || sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null)
            {
                string error = String.Empty;
                bool isErr = false;
                Tab2ViewModel model = new Tab2ViewModel();

                #region validate 
                string PupilCode = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : "";

                if (string.IsNullOrEmpty(PupilCode))
                {
                    error += "Mã học sinh không được để trống. ";
                    isErr = true;
                }
                else
                {
                    PupilOfClass poc = lstPoc.Where(p => p.PupilProfile.PupilCode.Equals(PupilCode)).FirstOrDefault();
                    if (poc == null)
                    {
                        error += "Mã học sinh không tồn tại. ";
                        isErr = true;
                    }
                    else
                    {
                        if (!lstPupilCode.Contains(PupilCode))
                        {
                            lstPupilCode.Add(PupilCode);
                            model.PupilID = poc.PupilID;
                        }
                        else
                        {
                            error += "Trùng mã học sinh. ";
                            isErr = true;
                        }
                        model.Status = poc.Status;
                    }
                }

                string PupilName = sheet.GetCellValue(startRow, 3) != null ? sheet.GetCellValue(startRow, 3).ToString() : "";
                string Comment1 = sheet.GetCellValue(startRow, 4) != null ? sheet.GetCellValue(startRow, 4).ToString() : "";
                if (Utils.Utils.IsDangerousString(Comment1))
                {
                    error += "Biểu hiện nổi bật, mức độ hình thành phẩm chất, năng lực chứa ký tự nguy hiểm. ";
                    isErr = true;
                }
                string Comment2 = sheet.GetCellValue(startRow, 5) != null ? sheet.GetCellValue(startRow, 5).ToString() : "";
                if (Utils.Utils.IsDangerousString(Comment2))
                {
                    error += "Những nội dung chưa hoàn thành chương trình; những điều cần khắc phục, giúp đỡ, bổ sung, rèn luyện thêm chứa ký tự nguy hiểm. ";
                    isErr = true;
                }
                #endregion
                model.PupilCode = PupilCode;
                model.PupilName = PupilName;
                model.Comment1 = Comment1;
                model.Comment2 = Comment2;

                string capacity = sheet.GetCellValue(startRow, 6) != null ? sheet.GetCellValue(startRow, 6).ToString() : "";
                string quality = sheet.GetCellValue(startRow, 7) != null ? sheet.GetCellValue(startRow, 7).ToString() : "";

                if (!string.IsNullOrEmpty(capacity))
                {
                    if (capacity.Equals("T"))
                    {
                        model.Capacity = PupilEvaluationBookConstants.T;
                    }
                    else if (capacity.Equals("Đ"))
                    {
                        model.Capacity = PupilEvaluationBookConstants.D;
                    }
                    else if (capacity.Equals("C"))
                    {
                        model.Capacity = PupilEvaluationBookConstants.C;
                    }
                    else
                    {
                        error += "Xếp loại năng lực không hợp lệ. ";
                        isErr = true;
                    }
                    model.InputCapacity = capacity;
                }

                if (!string.IsNullOrEmpty(quality))
                {
                    if (quality.Equals("T"))
                    {
                        model.Quality = PupilEvaluationBookConstants.T;
                    }
                    else if (quality.Equals("Đ"))
                    {
                        model.Quality = PupilEvaluationBookConstants.D;
                    }
                    else if (quality.Equals("C"))
                    {
                        model.Quality = PupilEvaluationBookConstants.C;
                    }
                    else
                    {
                        error += "Xếp loại phẩm chất không hợp lệ. ";
                        isErr = true;
                    }
                    model.InputQuality = quality;
                }

                model.ErrorLabel = !string.IsNullOrEmpty(error) ? error.Substring(0, error.Length - 1) : "";
                model.IsImportError = isErr;
                lstModel.Add(model);
                startRow++;
            }
            return lstModel;
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        private bool CheckButtonPermission(int classId, int semester)
        {
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classId);
            DateTime semesterStartDate;
            DateTime semesterEndDate;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                semesterStartDate = aca.FirstSemesterStartDate.GetValueOrDefault().Date;
                semesterEndDate = aca.FirstSemesterEndDate.GetValueOrDefault().Date;
            }
            else
            {
                semesterStartDate = aca.SecondSemesterStartDate.GetValueOrDefault().Date;
                semesterEndDate = aca.SecondSemesterEndDate.GetValueOrDefault().Date;
            }
            DateTime dateNow = DateTime.Now.Date;
            bool isInSemester = DateTime.Compare(dateNow, semesterStartDate) >= 0 && DateTime.Compare(dateNow, semesterEndDate) <= 0;

            return ((isGVCN && isInSemester) || _globalInfo.IsAdmin) && _globalInfo.IsCurrentYear;
        }
        #endregion

    }
}