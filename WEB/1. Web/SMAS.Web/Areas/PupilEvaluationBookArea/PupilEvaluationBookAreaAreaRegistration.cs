﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilEvaluationBookArea
{
    public class PupilEvaluationBookAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilEvaluationBookArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilEvaluationBookArea_default",
                "PupilEvaluationBookArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
