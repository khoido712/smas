﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilEvaluationBookArea
{
    public class PupilEvaluationBookConstants
    {
        public const string LIST_CLASS = "list_class";
        public const string LIST_EDUCATION_LEVEL = "list_education_level";
        public const string LIST_SEMESTER = "list_semester";
        public const string LIST_IMPORT_ERROR = "list_import_error";
        public const string DEFAULT_SEMESTER = "default_semester";
        public const string CLASS_ID = "class_id";
        public const string SELECTED_CLASS = "selected_class";
        public const string LIST_SUBJECT = "list_subject";
        public const string LIST_TAB_1 = "list_tab_1";
        public const string LIST_TAB_2 = "list_tab_2";
        public const string LOCK_TITLE = "LockTitle";
        public const string PERMISSION_BUTTON = "PERMISSION_BUTTON";
        public const int TAB_1 = 1;
        public const int TAB_2 = 2;

        //Dat, Chua dat
        public const int T = 1;
        public const int D = 2;
        public const int C = 3;

        //Ket qua hoc tap
        public const int STUDY_RESULT_HTT = 1;
        public const int STUDY_RESULT_HT = 2;
        public const int STUDY_RESULT_CHT = 3;

        //HT,CHT
        public const int CDG = 0;
        public const int HT = 1;
        public const int CHT = 2;
    }
}