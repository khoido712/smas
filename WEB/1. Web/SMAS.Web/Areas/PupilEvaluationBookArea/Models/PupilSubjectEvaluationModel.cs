﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilEvaluationBookArea.Models
{
    public class PupilSubjectEvaluationModel
    {
        public int? SubjectID { get; set; }
        public decimal? KTDK_CK { get; set; }
        public string KTDK_CK_JUDGE { get; set; }
        public int? Rate { get; set; }
        public int? IsCommenting { get; set; }
        public bool IsVnenSubject { get; set; }
        public decimal? SummedUpMark { get; set; }
        public decimal? AVERAGE_MARK { get; set; }
        public string AVERAGE_JUDGE { get; set; }
        public string JudgementResult { get; set; }
        public bool IsExempted { get; set; }
        public bool IsNotLearning { get; set; }
        public string StrDisplay
        {
            get
            {
                string str = String.Empty;
                if (IsNotLearning)
                {
                    return "<span style='color:#2078cc'><b>KH</b></span>";
                }
                if (IsExempted)
                {
                    return "<span style='color:#36c241'><b>MG</b></span>";
                }

                if (IsVnenSubject)
                {
                    if (IsCommenting == 1)
                    {
                        if (string.IsNullOrEmpty(AVERAGE_JUDGE))
                        {
                            str = "<span style='color:red'><b>CN</b></span><br/>";
                        }
                        else
                        {
                            string strKTDK_CK_JUDGE = "<b>" + AVERAGE_JUDGE + "</b>";
                            if (AVERAGE_JUDGE == "CĐ")
                            {
                                strKTDK_CK_JUDGE = "<span style='color:red'>" + strKTDK_CK_JUDGE + "</span>";
                            }
                            str = strKTDK_CK_JUDGE;
                        }
                    }
                    else
                    {
                        string strKTDK_CK = String.Empty;
                        if (AVERAGE_MARK.HasValue)
                        {
                            strKTDK_CK = AVERAGE_MARK.Value != 0 && AVERAGE_MARK.Value != 10 ? String.Format("{0:0.0}", AVERAGE_MARK.Value) : String.Format("{0:0}", AVERAGE_MARK.Value);
                            strKTDK_CK = "<b>" + strKTDK_CK + "</b>";
                            if (AVERAGE_MARK.Value < 5)
                            {
                                strKTDK_CK = "<span style='color:red'>" + strKTDK_CK + "</span>";
                            }
                        }
                        else
                        {
                            strKTDK_CK = "<span style='color:red'><b>CN</b></span><br/>";
                        }
                        str = strKTDK_CK;
                    }
                }
                else
                {
                    string strMark = String.Empty;
                    if (SummedUpMark != null)
                    {
                        strMark = SummedUpMark != 0 && SummedUpMark != 10 ? String.Format("{0:0.0}", SummedUpMark) : String.Format("{0:0}", SummedUpMark);
                        strMark = "<b>" + strMark + "</b>";
                        if (SummedUpMark < 5)
                        {
                            strMark = "<span style='color:red'>" + strMark + "</span>";
                        }
                    }
                    else
                    {
                        strMark = "<span style='color:red'><b>CN</b></span><br/>";
                    }

                    string strJudge = string.Empty;
                    if (string.IsNullOrEmpty(JudgementResult))
                    {
                        strJudge = "<span style='color:red'><b>CN</b></span><br/>";
                    }
                    else
                    {
                        strJudge = "<b>" + JudgementResult + "</b>";
                        if (JudgementResult == "CĐ")
                        {
                            strJudge = "<span style='color:red'>" + strJudge + "</span>";
                        }
                    }
                    if (IsCommenting == 1)
                    {
                        str = strJudge;

                    }
                    else
                    {
                        str = strMark;
                    }
                }
                return str;
            }
        }
    }
}