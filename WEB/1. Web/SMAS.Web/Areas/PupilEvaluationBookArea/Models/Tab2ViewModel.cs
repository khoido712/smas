﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilEvaluationBookArea.Models
{
    public class Tab2ViewModel
    {
        public int PupilID { get; set; }
        [ResourceDisplayName("PupilEvaluationBook_Pupil_Name")]
        public string PupilName { get; set; }
        [ResourceDisplayName("Common_Label_PupilCode")]
        public string PupilCode { get; set; }
        public int Status { get; set; }
        public DateTime? EndDate { get; set; }
        [ResourceDisplayName("PupilEvaluationBook_Comment1")]
        public string Comment1 { get; set; }
        [ResourceDisplayName("PupilEvaluationBook_Comment2")]
        public string Comment2 { get; set; }
        //Nang luc
        public int? Capacity { get; set; }
        //Pham chat
        public int? Quality { get; set; }
        //Nang luc trong he
        public int? CapacitySummer { get; set; }
        //Pham chat trong he
        public int? QualitySummer { get; set; }
        [ResourceDisplayName("PupilEvaluationBook_Capacity_2")]
        public string StrCapacity
        {
            get
            {
                string str = String.Empty;
                if (Capacity == PupilEvaluationBookConstants.T)
                {
                    str = "T";
                }
                else if (Capacity == PupilEvaluationBookConstants.D)
                {
                    str = "Đ";
                }
                else if (Capacity == PupilEvaluationBookConstants.C)
                {
                    str = "C";
                }
                return str;

            }
        }
        [ResourceDisplayName("PupilEvaluationBook_Quality_2")]
        public string StrQuality
        {
            get
            {
                string str = String.Empty;
                if (Quality == PupilEvaluationBookConstants.T)
                {
                    str = "T";
                }
                else if (Quality == PupilEvaluationBookConstants.D)
                {
                    str = "Đ";
                }
                else if (Quality == PupilEvaluationBookConstants.C)
                {
                    str = "C";
                }
                return str;
            }
        }
        [ResourceDisplayName("PupilEvaluationBook_Capacity_Summer")]
        public string StrCapacitySummer
        {
            get
            {
                string str = String.Empty;
                if (CapacitySummer == PupilEvaluationBookConstants.T)
                {
                    str = "T";
                }
                else if (CapacitySummer == PupilEvaluationBookConstants.D)
                {
                    str = "Đ";
                }
                else if (CapacitySummer == PupilEvaluationBookConstants.C)
                {
                    str = "C";
                }
                return str;

            }
        }
        [ResourceDisplayName("PupilEvaluationBook_Quality_Summer")]
        public string StrQualitySummer
        {
            get
            {
                string str = String.Empty;
                if (QualitySummer == PupilEvaluationBookConstants.T)
                {
                    str = "T";
                }
                else if (QualitySummer == PupilEvaluationBookConstants.D)
                {
                    str = "Đ";
                }
                else if (QualitySummer == PupilEvaluationBookConstants.C)
                {
                    str = "C";
                }
                return str;
            }
        }
        [ResourceDisplayName("PupilEvaluationBook_Capacity_2")]
        public string InputCapacity { get; set; }
        [ResourceDisplayName("PupilEvaluationBook_Quality_2")]
        public string InputQuality { get; set; }
        public string InputCapacitySummer { get; set; }
        public string InputQualitySummer { get; set; }
        public bool IsCheck { get; set; }
        [ResourceDisplayName("Common_Label_Note")]
        public string ErrorLabel { get; set; }
        public bool IsImportError { get; set; }
        public bool IsShowData { get; set; }
    }
}