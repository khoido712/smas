﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.PupilEvaluationBookArea.Models
{
    public class Tab1ViewModel
    {
        public int PupilID { get; set; }
        public int Status { get; set; }
        public DateTime? EndDate { get; set; }
        [ResourceDisplayName("PupilEvaluationBook_Pupil_Name")]
        public string PupilName { get; set; }
        public List<PupilSubjectEvaluationModel> lstSubjectEvaluation { get; set; }
        //Nang luc
        public int? Capacity { get; set; }
        //Pham chat
        public int? Quality { get; set; }
        //Danh gia cuoi nam
        public int? RateEndYear { get; set; }
        //Danh gia bo sung
        public int? RateAdd { get; set; }
        public int? StudyResult { get; set; }
        public string StrStudyResult 
        { 
            get
            {
                string str = string.Empty;
                if (StudyResult == PupilEvaluationBookConstants.STUDY_RESULT_HTT)
                {
                    str = "<b>HTT</b>";
                }
                else if (StudyResult == PupilEvaluationBookConstants.STUDY_RESULT_HT)
                {
                    str = "<b>HT</b>";
                }
                else if (StudyResult == PupilEvaluationBookConstants.STUDY_RESULT_CHT)
                {
                    str = "<span style='color:red'><b>CHT</b></span>";
                }
                return str;
            }
        }
        [ResourceDisplayName("PupilEvaluationBook_Capacity")]
        public string StrCapacity
        {
            get
            {
                string str=String.Empty;
                if (Capacity == PupilEvaluationBookConstants.T)
                {
                    str = "<b>T</b>";
                }
                else if (Capacity == PupilEvaluationBookConstants.D)
                {
                    str = "<b>Đ</b>";
                }
                else if (Capacity == PupilEvaluationBookConstants.C)
                {
                    str="<span style='color:red'><b>C</b></span>";
                }
                return str;
                
            }
        }
        [ResourceDisplayName("PupilEvaluationBook_Quality")]
        public string StrQuality
        {
            get
            {
                string str = String.Empty;
                if (Quality == PupilEvaluationBookConstants.T)
                {
                    str = "<b>T</b>";
                }
                else if (Quality == PupilEvaluationBookConstants.D)
                {
                    str = "<b>Đ</b>";
                }
                else if (Quality == PupilEvaluationBookConstants.C)
                {
                    str = "<span style='color:red'><b>C</b></span>";
                }
                return str;
            }
        }
        [ResourceDisplayName("PupilEvaluationBook_RateEndYear")]
        public string StrRateEndYear
        {
            get
            {
                string str = String.Empty;
                if (RateEndYear == PupilEvaluationBookConstants.HT)
                {
                    str = "<b>HT</b>";
                }
                else if (RateEndYear == PupilEvaluationBookConstants.CHT)
                {
                    str = "<span style='color:red'><b>CHT</b></span>";
                }
                return str;
            }
        }
        [ResourceDisplayName("PupilEvaluationBook_RateAdd")]
        public string StrRateAdd
        {
            get
            {
                string str = String.Empty;
                if (RateAdd == PupilEvaluationBookConstants.HT)
                {
                    str = "HT";
                }
                else if (RateAdd == PupilEvaluationBookConstants.CHT)
                {
                    str = "CHT";
                }
                return str;
            }
        }
        public string InputRateAdd { get; set; }
        public bool IsShowData { get; set; }

    }
}