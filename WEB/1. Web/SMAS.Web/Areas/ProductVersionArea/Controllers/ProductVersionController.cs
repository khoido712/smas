﻿using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.ProductVersionArea;
using System.IO;
using SMAS.Business.Common;

namespace SMAS.Web.Areas.ProductVersionArea.Controllers
{
    public class ProductVersionController:BaseController
    {
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        public ProductVersionController(ISchoolProfileBusiness schoolProfileBusiness)
        {
            this.SchoolProfileBusiness = schoolProfileBusiness;
        }
        public ActionResult Index()
        {
            ViewData[ProductVersionConstants.PRODUCT_VERSION] = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value).ProductVersion;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ChooseVersionAction(int ProductVersionID)
        {
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            objSP.ProductVersion = ProductVersionID;
            SchoolProfileBusiness.Update(objSP);
            SchoolProfileBusiness.Save();
            _globalInfo.ProductVersionID = ProductVersionID;
            return Json(new JsonMessage(Res.Get("Lựa chọn phiên bản thành công")));
        }
        public FileResult DownloadDocument()
        {
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + SystemParamsInFile.FILE_DOWNLOAD_DOCUMENT;
            Stream excel = Config.DownloadDocumentFile(templatePath);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string ReportName = "PhienBan.pdf";
            result.FileDownloadName = ReportName;
            return result;
        }
    }
}