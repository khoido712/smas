﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ProductVersionArea
{
    public class ProductVersionAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ProductVersionArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ProductVersionArea_default",
                "ProductVersionArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
