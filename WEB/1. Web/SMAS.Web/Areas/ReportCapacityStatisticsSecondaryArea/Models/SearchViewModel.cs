﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportCapacityStatisticsSecondaryArea.Models
{
    public class SearchViewModel   
    {
        public int ReportType { get; set; }
        public int? DistrictID { get; set; }
        public int? EducationLevelID { get; set; }
        public int? TrainingTypeID { get; set; }
        public int AcademicYearID { get; set; }
        public int Semester { get; set; }

    }
}
