﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportCapacityStatisticsSecondaryArea
{
    public class ReportCapacityStatisticsSecondaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportCapacityStatisticsSecondaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportCapacityStatisticsSecondaryArea_default",
                "ReportCapacityStatisticsSecondaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
