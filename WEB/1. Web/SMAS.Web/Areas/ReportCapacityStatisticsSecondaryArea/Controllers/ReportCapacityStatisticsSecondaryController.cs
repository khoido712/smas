﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Models.Models;
using SMAS.Web.Areas.ReportCapacityStatisticsSecondaryArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.Web.Areas.ReportConductStatisticsSecondaryArea;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.ReportCapacityStatisticsSecondaryArea.Controllers
{
    public class ReportCapacityStatisticsSecondaryController : BaseController
    {
        IDistrictBusiness DistrictBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        ITrainingTypeBusiness TrainingTypeBusiness;
        ISubCommitteeBusiness SubCommitteeBusiness;
        ICapacityStatisticBusiness CapacityStatisticBusiness;
        IProcessedReportBusiness ProcessedReportBusiness;
        IConductStatisticBusiness ConductStatisticBusiness;
        IEducationLevelBusiness EducationLevelBusiness;
        public ReportCapacityStatisticsSecondaryController(IDistrictBusiness districtBusiness,
                                                            IAcademicYearBusiness academicYearBusiness,
                                                            ITrainingTypeBusiness trainingTypeBusiness,
                                                            IEducationLevelBusiness educationlevelBusiness,
                                                            ISubCommitteeBusiness subCommitteeBusiness,
            ICapacityStatisticBusiness capacityStatisticBusiness, IProcessedReportBusiness processedReportBusiness,
            IConductStatisticBusiness conductStatisticBusiness)
        {
            this.DistrictBusiness = districtBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
            this.SubCommitteeBusiness = subCommitteeBusiness;
            this.EducationLevelBusiness = educationlevelBusiness;
            this.CapacityStatisticBusiness = capacityStatisticBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ConductStatisticBusiness = conductStatisticBusiness;
        }


        private void GetViewData()
        {
            ViewData[ReportCapacityStatisticsSecondaryConstants.DISABLE_COMBOBOX] = false;
            GlobalInfo glo = new GlobalInfo();
            List<int> listAcademicYear = AcademicYearBusiness.GetListYearForSupervisingDept_THCS(glo.SupervisingDeptID.Value);
            ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_ACADEMIC_YEAR] = listAcademicYear.Select(u => new SelectListItem { Text = u + " - " + (u + 1), Value = u.ToString(), Selected = false }).ToList();

            ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_SEMESTER] = new SelectList(CommonList.SemesterAndAll(), "key", "value");

            List<EducationLevel> lstEducationLevel = this.EducationLevelBusiness.All.Where(o => o.Grade == 2).ToList();
            ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_EDUCATION_LEVEL] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");

            List<TrainingType> listTrainingType = TrainingTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_TRAINING_TYPE] = new SelectList(listTrainingType, "TrainingTypeID", "Resolution");
 
            if (glo.IsSuperVisingDeptRole)
            {
                ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_TYPESTATICS] = new List<ViettelCheckboxList>() { 
                                                    new ViettelCheckboxList{Label=Res.Get("ReportCapacityStatisticsSecondary_Label_ByLevel"), Value=2, cchecked=true, disabled= false}, 
                                                    new ViettelCheckboxList{Label=Res.Get("ReportConductStatisticsSecondary_Label_ByLevel"), Value=4, cchecked=false, disabled= false}, 
                                                    new ViettelCheckboxList{Label=Res.Get("ReportCapacityStatisticsSecondary_Label_ByDictrict"), Value=3, cchecked=false, disabled= false},
                                                    new ViettelCheckboxList{Label=Res.Get("ReportConductStatisticsSecondary_Label_ByDictrict"), Value=5, cchecked=false, disabled= false}
                                                };
                List<District> listDistrict = DistrictBusiness.Search(new Dictionary<string, object> { { "ProvinceID", glo.ProvinceID.Value }, { "IsActive", true } }).ToList();
                ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_DISTRICT] = new SelectList(listDistrict, "DistrictID", "DistrictName");
            }
            //Neu la account phong thi disable cbb district
            else if (glo.IsSubSuperVisingDeptRole)
            {
                ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_TYPESTATICS] = new List<ViettelCheckboxList>() { 
                                                    new ViettelCheckboxList{Label=Res.Get("ReportCapacityStatisticsSecondary_Label_ByLevel"), Value=2, cchecked=true, disabled= false}, 
                                                    new ViettelCheckboxList{Label=Res.Get("ReportConductStatisticsSecondary_Label_ByLevel"), Value=4, cchecked=false, disabled= false}, 
                                                };
                District District = DistrictBusiness.Find(glo.DistrictID);
                List<District> listDistrict = new List<SMAS.Models.Models.District>();
                listDistrict.Add(District);
                ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_DISTRICT] = new SelectList(listDistrict, "DistrictID", "DistrictName", glo.DistrictID);
                ViewData[ReportCapacityStatisticsSecondaryConstants.DISABLE_COMBOBOX] = true;

            }

        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }

        public ActionResult Search(SearchViewModel model, FormCollection col)
        {
            GlobalInfo glo = new GlobalInfo();
            int reportType = int.Parse(col["ReportType"]);
            #region Thống kê học lực theo khối
            if (reportType == 2)
            {
                //I. Nếu là account cấp Sở
                if (glo.IsSuperVisingDeptRole)
                {
                    CapacityConductReport capacityConductReport = new CapacityConductReport();
                    capacityConductReport.EducationLevelID = 0;
                    capacityConductReport.Year = model.AcademicYearID;
                    capacityConductReport.Semester = model.Semester;
                    capacityConductReport.ProvinceID = glo.ProvinceID.Value;                    
                    if (model.DistrictID == null) model.DistrictID = 0;
                    capacityConductReport.DistrictID = model.DistrictID.Value;                    
                    string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    string ReportCode = SystemParamsInFile.REPORT_SGD_THCS_THONGKEHOCLUCTHEOKHOI;
                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                    if (entity == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;
                        return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                    }
                }
                //Sua lai ham IsSubSuperVisingDeptRole
               else
                {
                    CapacityConductReport capacityConductReport = new CapacityConductReport();
                    capacityConductReport.EducationLevelID = 0;
                    capacityConductReport.Year = model.AcademicYearID;
                    capacityConductReport.Semester = model.Semester;
                    capacityConductReport.ProvinceID = glo.ProvinceID.Value;
                    capacityConductReport.DistrictID = glo.DistrictID.Value;


                    string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    string ReportCode = SystemParamsInFile.REPORT_PGD_THCS_THONGKEHOCLUCTHEOKHOI;
                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                    if (entity == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;
                        return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                    }
                }
            }
            #endregion
            #region Thống kê học lực theo quận huyện
            if (reportType == 3)
            {
                //I. Nếu là account cấp Sở
                if (glo.IsSuperVisingDeptRole)
                {
                    CapacityConductReport capacityConductReport = new CapacityConductReport();
                    capacityConductReport.Year = model.AcademicYearID;
                    if (model.EducationLevelID == null) model.EducationLevelID = 0;
                    capacityConductReport.EducationLevelID = model.EducationLevelID.Value;
                    capacityConductReport.Semester = model.Semester;
                    if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                    capacityConductReport.ProvinceID = glo.ProvinceID.Value;
                    capacityConductReport.DistrictID = 0;

                    string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    string ReportCode = SystemParamsInFile.REPORT_SGD_THCS_THONGKEHOCLUCTHEOQUANHUYEN;
                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                    if (entity == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;
                        return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                    }
                }
            }

            #endregion
            #region Thống kê theo trường
            if (reportType == 1)
            {
                //Neu la account cap So
                if (glo.IsSuperVisingDeptRole)
                {
                    CapacityConductReport CapacityConductReport = new CapacityConductReport();
                    CapacityConductReport.Year = model.AcademicYearID;
                    if (model.EducationLevelID == null) model.EducationLevelID = 0;
                    CapacityConductReport.EducationLevelID = model.EducationLevelID.Value;
                    CapacityConductReport.Semester = model.Semester;
                    if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                    CapacityConductReport.TrainingTypeID = model.TrainingTypeID.Value;
                    CapacityConductReport.SubcommitteeID = 0;
                    CapacityConductReport.ProvinceID = glo.ProvinceID.Value;
                    if (model.DistrictID == null) model.DistrictID = 0;
                    CapacityConductReport.DistrictID = model.DistrictID.Value;
                    string inputHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(CapacityConductReport);
                    string reportCode = SystemParamsInFile.REPORT_THONG_KE_HOC_LUC_SO;
                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(reportCode, inputHashKey);
                    object o = entity;
                    if (o == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;
                        return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                    }
                }
                //Neu la nguoi dung cap phong UserInfo.IsSubSuperVisingDeptRole() == true
                else if (glo.IsSubSuperVisingDeptRole)
                {
                    CapacityConductReport CapacityConductReport = new CapacityConductReport();
                    CapacityConductReport.Year = model.AcademicYearID;
                    if (model.EducationLevelID == null) model.EducationLevelID = 0;
                    CapacityConductReport.EducationLevelID = model.EducationLevelID.Value;
                    CapacityConductReport.Semester = model.Semester;
                    if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                    CapacityConductReport.TrainingTypeID = model.TrainingTypeID.Value;
                    CapacityConductReport.SubcommitteeID = 0;
                    CapacityConductReport.ProvinceID = glo.ProvinceID.Value;
                    CapacityConductReport.DistrictID = glo.DistrictID.Value;
                    string inputHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(CapacityConductReport);
                    string reportCode = SystemParamsInFile.REPORT_THONG_KE_HOC_LUC_PHONG;
                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(reportCode, inputHashKey);
                    object o = entity;
                    if (o == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;
                        return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                    }
                }
            }
            #endregion
            #region Thống hạnh kiểm kê theo khối
            if (reportType == 4)
            {
                //I. Nếu là account cấp Sở
                if (glo.IsSuperVisingDeptRole)
                {
                    CapacityConductReport conductSecondaryrep = new CapacityConductReport();
                    conductSecondaryrep.Year = model.AcademicYearID;
                    conductSecondaryrep.EducationLevelID = 0;
                    conductSecondaryrep.Semester = model.Semester;
                    conductSecondaryrep.ProvinceID = glo.ProvinceID.Value;
                    if (model.DistrictID == null) model.DistrictID = 0;
                    conductSecondaryrep.DistrictID = model.DistrictID.Value;
                    string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(conductSecondaryrep);
                    string ReportCode = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_KHOI_CAP2;
                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                    if (entity == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;
                        return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                    }
                }
                //Sua lai ham IsSubSuperVisingDeptRole
                if (glo.IsSuperVisingDeptRole == false)
                {
                    CapacityConductReport conductSecondaryrep = new CapacityConductReport();
                    conductSecondaryrep.Year = model.AcademicYearID;

                    conductSecondaryrep.EducationLevelID = 0;
                    conductSecondaryrep.Semester = model.Semester;
                    if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                    conductSecondaryrep.TrainingTypeID = model.TrainingTypeID.Value;
                    conductSecondaryrep.SubcommitteeID = 0;
                    conductSecondaryrep.ProvinceID = glo.ProvinceID.Value;

                    conductSecondaryrep.DistrictID = glo.DistrictID.Value;

                    string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(conductSecondaryrep);
                    string ReportCode = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_KHOI_CAP2;
                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                    if (entity == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;
                        return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                    }
                }
            }
            #endregion
            #region Thống kê hạnh kiểm theo quận huyện
            if (reportType == 5)
            {
                //I. Nếu là account cấp Sở

                if (glo.IsSuperVisingDeptRole)
                {
                    CapacityConductReport conductSecondaryrep = new CapacityConductReport();

                    conductSecondaryrep.Year = model.AcademicYearID;

                    if (model.EducationLevelID == null) model.EducationLevelID = 0;

                    conductSecondaryrep.EducationLevelID = model.EducationLevelID.Value;

                    conductSecondaryrep.Semester = model.Semester;


                    conductSecondaryrep.ProvinceID = glo.ProvinceID.Value;

                    conductSecondaryrep.DistrictID = 0;

                    string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(conductSecondaryrep);

                    string ReportCode = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_QUAN_HUYEN_CAP2;

                    ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);

                    if (entity == null)
                    {
                        return Json(new { ProcessedReportID = 0 });
                    }
                    else
                    {
                        int ProcessedReportID = entity.ProcessedReportID;

                        return Json(new { ProcessedReportID = ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                    }

                }

            }
            #endregion
            return null;
        }

        public FileResult DownloadReport(int ProcessedReportID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SupervisingDeptID", GlobalInfo.SupervisingDeptID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_SGD_THCS_THONGKEHOCLUCTHEOKHOI, 
                SystemParamsInFile.REPORT_PGD_THCS_THONGKEHOCLUCTHEOKHOI,
                SystemParamsInFile.REPORT_SGD_THCS_THONGKEHOCLUCTHEOQUANHUYEN,
                SystemParamsInFile.REPORT_THONG_KE_HOC_LUC_PHONG,
                SystemParamsInFile.REPORT_THONG_KE_HOC_LUC_SO,
                SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_KHOI_CAP2, 
                SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_QUAN_HUYEN_CAP2,
                SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_SO,
                SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_PHONG
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(ProcessedReportID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(SearchViewModel model)
        {
            #region Thong ke Hoc luc theo khoi
            GlobalInfo glo = new GlobalInfo();
            if (model.ReportType == 2)
            {

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = model.AcademicYearID;
                dic["Semester"] = model.Semester;
                dic["EducationLevelID"] = 0;
                dic["ReportCode"] = SystemParamsInFile.THONGKEHOCLUCCAP2;
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = glo.ProvinceID.Value;
                dic["SupervisingDeptID"] = glo.SupervisingDeptID;
                CapacityConductReport capacityConductReport = new CapacityConductReport();
                capacityConductReport.Year = model.AcademicYearID;
                capacityConductReport.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;
                capacityConductReport.Semester = model.Semester;
                capacityConductReport.ProvinceID = glo.ProvinceID.Value;

                int FileID = 0;
                if (glo.IsSuperVisingDeptRole)
                {
                    dic["DistrictID"] = model.DistrictID == null ? 0 : model.DistrictID.Value;
                    capacityConductReport.DistrictID = model.DistrictID == null ? 0 : model.DistrictID.Value;
                    string input = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    List<CapacityStatisticsBO> lstCapacityStatisticsOfProvinceByLevelSecondary = CapacityStatisticBusiness.CreateSGDCapacityStatisticsByLevelSecondary(input, dic, out FileID);
                    ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_CAPACITYSTATISTIC] = lstCapacityStatisticsOfProvinceByLevelSecondary;
                    ViewData[ReportCapacityStatisticsSecondaryConstants.GRID_REPORT_ID] = FileID;
                }
                else
                {
                    dic["DistrictID"] = glo.DistrictID;
                    capacityConductReport.DistrictID = glo.DistrictID.Value;
                    string input = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    List<CapacityStatisticsBO> lstCapacityStatisticsOfProvinceByLevelSecondary = CapacityStatisticBusiness.CreatePGDCapacityStatisticsByLevelSecondary(input, dic, out FileID);
                    ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_CAPACITYSTATISTIC] = lstCapacityStatisticsOfProvinceByLevelSecondary;
                    ViewData[ReportCapacityStatisticsSecondaryConstants.GRID_REPORT_ID] = FileID;
                }
                return PartialView("_GridCapacityByLevel");

            }
            #endregion 
            #region Thong Ke hoc luc theo Quan/Huyen

            if (model.ReportType == 3)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = model.AcademicYearID;
                dic["Semester"] = model.Semester;
                dic["EducationLevelID"] = model.EducationLevelID;
                dic["ReportCode"] = SystemParamsInFile.THONGKEHOCLUCCAP2;
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = glo.ProvinceID.Value;
                dic["DistrictID"] = 0;
                dic["SupervisingDeptID"] = glo.SupervisingDeptID.Value;
                dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
                int FileID = 0;
                CapacityConductReport capacityConductReport = new CapacityConductReport();


                capacityConductReport.Year = model.AcademicYearID;
                if (model.EducationLevelID == null) model.EducationLevelID = 0;
                capacityConductReport.EducationLevelID = model.EducationLevelID.HasValue ? model.EducationLevelID.Value : 0;
                capacityConductReport.Semester = model.Semester;
                capacityConductReport.ProvinceID = glo.ProvinceID.Value;
                capacityConductReport.DistrictID = 0;
                

                string input = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);

                List<CapacityStatisticsBO> lstCapacityStatisticsOfProvinceByDistrictSecondary = CapacityStatisticBusiness.CreateSGDCapacityStatisticsByDistrictSecondary(input, dic, out FileID);
                ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_CAPACITYSTATISTICDistrict] = lstCapacityStatisticsOfProvinceByDistrictSecondary;
                ViewData[ReportCapacityStatisticsSecondaryConstants.GRID_REPORT_ID] = FileID;
                return PartialView("_GridCapacityByDistrict");
                //ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_DATA] = CapacityStatisticBusiness.SearchByProvince23(dic);
            }
            #endregion
            #region Thong ke Hanh Kiem theo khoi
            //Báo cáo Hạnh kiểm theo khối
            if (model.ReportType == 4)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = model.AcademicYearID;
                dic["Semester"] = model.Semester;
                dic["EducationLevelID"] = 0;
                dic["ReportCode"] = "ThongKeHanhKiemCap2";
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = glo.ProvinceID.Value;
                dic["SupervisingDeptID"] = glo.SupervisingDeptID;
                CapacityConductReport capacityConductReport = new CapacityConductReport();

                capacityConductReport.Year = model.AcademicYearID;
                if (model.EducationLevelID == null) model.EducationLevelID = 0;
                capacityConductReport.EducationLevelID = model.EducationLevelID.Value;
                capacityConductReport.Semester = model.Semester;
                capacityConductReport.ProvinceID = glo.ProvinceID.Value;

                int FileID = 0;
                if (glo.IsSuperVisingDeptRole)
                {
                    if (model.DistrictID == null) model.DistrictID = 0;
                    capacityConductReport.DistrictID = model.DistrictID.Value;
                    dic["DistrictID"] = glo.DistrictID.Value;
                    string input = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    List<ConductStatisticsOfProvinceGroupByLevel23BO> lstConductStatisticsOfProvinceByLevelSecondary = ConductStatisticBusiness.CreateSGDConductStatisticsByLevelSecondary(dic, input, out FileID);
                    ViewData[ReportConductStatisticsSecondaryConstants.LIST_CONDUCTSTATISTIC_BY_LEVEL] = lstConductStatisticsOfProvinceByLevelSecondary;
                    ViewData[ReportConductStatisticsSecondaryConstants.GRID_REPORT_ID] = FileID;
                }
                else
                {
                    capacityConductReport.DistrictID = glo.DistrictID.Value;
                    dic["DistrictID"] = glo.DistrictID.Value;
                    string input = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    List<ConductStatisticsOfProvinceGroupByLevel23BO> lstConductStatisticsOfProvinceByLevelSecondary = ConductStatisticBusiness.CreatePGDConductStatisticsByLevelSecondary(dic, input, out FileID);
                    ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_CONDUCTSTATISTIC_BY_LEVEL] = lstConductStatisticsOfProvinceByLevelSecondary;
                    ViewData[ReportCapacityStatisticsSecondaryConstants.GRID_REPORT_ID] = FileID;
                }
                return PartialView("_GridConductsByLevel");
            }
            #endregion 
            #region Thong ke Hanh Kiem Theo Quan/Huyen
            if (model.ReportType == 5)
                {
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["Year"] = model.AcademicYearID;
                    dic["Semester"] = model.Semester;
                    dic["EducationLevelID"] = model.EducationLevelID;
                    dic["ReportCode"] = "ThongKeHanhKiemCap2";
                    dic["SentToSupervisor"] = true;
                    dic["ProvinceID"] = glo.ProvinceID.Value;
                    dic["DistrictID"] = 0;
                    dic["SupervisingDeptID"] = glo.SupervisingDeptID.Value;

                    int FileID = 0;
                    CapacityConductReport capacityConductReport = new CapacityConductReport();

                    capacityConductReport.Year = model.AcademicYearID;
                    if (model.EducationLevelID == null) model.EducationLevelID = 0;
                    capacityConductReport.EducationLevelID = model.EducationLevelID.Value;
                    capacityConductReport.Semester = model.Semester;
                    if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                    capacityConductReport.TrainingTypeID = model.TrainingTypeID.Value;
                    capacityConductReport.SubcommitteeID = 0;
                    capacityConductReport.ProvinceID = glo.ProvinceID.Value;
                    capacityConductReport.DistrictID = 0;


                    string input = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);

                    List<ConductStatisticsOfProvinceGroupByDistrict23BO> lstConductStatisticsOfProvinceByDistrictSecondary = ConductStatisticBusiness.CreateSGDConductStatisticsByDistrictSecondary(dic, input, out FileID);
                    ViewData[ReportConductStatisticsSecondaryConstants.LIST_CONDUCTSTATISTIC] = lstConductStatisticsOfProvinceByDistrictSecondary;
                    ViewData[ReportConductStatisticsSecondaryConstants.GRID_REPORT_ID] = FileID;
                    return PartialView("_GridConductsByDistrict");
                    //ViewData[ReportCapacityStatisticsTertiaryConstants.GRID_REPORT_DATA] = CapacityStatisticBusiness.SearchByProvince23(dic);
                }
            #endregion
            #region
            if (model.ReportType == 1)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["Year"] = model.AcademicYearID;
                dic["Semester"] = model.Semester;
                dic["EducationLevelID"] = model.EducationLevelID;
                dic["TrainingTypeID"] = model.TrainingTypeID;
                dic["ReportCode"] = SystemParamsInFile.THONGKEHOCLUCCAP2;
                dic["SupervisingDeptID"] = glo.SupervisingDeptID;
                dic["SentToSupervisor"] = true;
                dic["ProvinceID"] = glo.ProvinceID.Value;

                CapacityConductReport capacityConductReport = new CapacityConductReport();
                capacityConductReport.Year = model.AcademicYearID;
                if (model.EducationLevelID == null) model.EducationLevelID = 0;
                capacityConductReport.EducationLevelID = model.EducationLevelID.Value;
                capacityConductReport.Semester = model.Semester;
                if (model.TrainingTypeID == null) model.TrainingTypeID = 0;
                capacityConductReport.TrainingTypeID = model.TrainingTypeID.Value;
                capacityConductReport.SubcommitteeID = 0;
                capacityConductReport.ProvinceID = glo.ProvinceID.Value;

                int FileID = 0;

                if (glo.IsSuperVisingDeptRole)
                {
                    if (model.DistrictID == null) model.DistrictID = 0;
                    capacityConductReport.DistrictID = model.DistrictID.Value;
                    dic["DistrictID"] = model.DistrictID;
                    string inputHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    List<CapacityStatisticsBO> lsCapacityStatistics = CapacityStatisticBusiness.CreateSGDCapacityStatisticsSecondary(dic, inputHashKey, out FileID);
                    ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_CAPACITYSTATISTIC_BY_SCHOOL] = lsCapacityStatistics;
                    ViewData[ReportCapacityStatisticsSecondaryConstants.FILE_ID] = FileID;
                }
                else if (glo.IsSubSuperVisingDeptRole)
                {
                    capacityConductReport.DistrictID = glo.DistrictID.Value;
                    dic["DistrictID"] = glo.DistrictID;
                    string inputHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(capacityConductReport);
                    List<CapacityStatisticsBO> lsCapacityStatistics = CapacityStatisticBusiness.CreatePGDCapacityStatisticsSecondary(dic, inputHashKey, out FileID);
                    ViewData[ReportCapacityStatisticsSecondaryConstants.LIST_CAPACITYSTATISTIC_BY_SCHOOL] = lsCapacityStatistics;
                    ViewData[ReportCapacityStatisticsSecondaryConstants.FILE_ID] = FileID;

                }
                return PartialView("_ListCapacityBySchool");
            }
#endregion
            return PartialView("");
        }
    }
}
