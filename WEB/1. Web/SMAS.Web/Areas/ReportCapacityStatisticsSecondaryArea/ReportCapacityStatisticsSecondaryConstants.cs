﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportCapacityStatisticsSecondaryArea
{
    public class ReportCapacityStatisticsSecondaryConstants 
    {
        public const string LIST_TYPESTATICS = "list_typestatics";
        public const string LIST_DISTRICT = "list_district";
        public const string LIST_ACADEMIC_YEAR = "list_academic_year";
        public const string LIST_SEMESTER = "list_semester";
        public const string LIST_EDUCATION_LEVEL = "list_education_level";
        public const string LIST_TRAINING_TYPE = "list_training_type";
        public const string LIST_SUB_COMMITTEE = "list_sub_committee";
        public const string LIST_CAPACITYSTATISTIC = "list_capacitystatistic";
        public const string LIST_CAPACITYSTATISTICDistrict = "list_capacitystatisticdistrict";
        public const string LIST_CONDUCTSTATISTIC = "list_conduct_statistic";
        public const string LIST_CONDUCTSTATISTIC_BY_LEVEL = "list_conduct_statistic_by_level";
        public const string GRID_REPORT_DATA = "grid_report_data";
        public const string GRID_REPORT_COMITTEE = "grid_report_comittee";
        public const string GRID_REPORT_COMITTEE_EDUCATIONLEVEL = "grid_report_comittee_educationlevel";
        public const string GRID_REPORT_ID = "grid_report_id";
        public const string LIST_CAPACITYSTATISTIC_BY_SCHOOL = "listSchool";
        public const string FILE_ID = "fileid";
        public const string DISABLE_COMBOBOX = "disablecbb";
    }
}
