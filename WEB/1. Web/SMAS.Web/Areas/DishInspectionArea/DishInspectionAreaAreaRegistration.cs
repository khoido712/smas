﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DishInspectionArea
{
    public class DishInspectionAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DishInspectionArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DishInspectionArea_default",
                "DishInspectionArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
