﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.DishInspectionArea.Models;

using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Web.Areas.DishInspectionArea.Controllers
{
    public class DishInspectionController : BaseController
    {
        private readonly IDishInspectionBusiness DishInspectionBusiness;
        private readonly IEatingGroupBusiness EatingGroupBusiness;
        private readonly IDailyMenuBusiness DailyMenuBusiness;
        private readonly INotEatingChildrenBusiness NotEatingChildrenBusiness;
        private readonly IDailyMenuDetailBusiness DailyMenuDetailBusiness;
        private readonly IDailyDishCostBusiness DailyDishCostBusiness;
        private readonly IDailyMenuFoodBusiness DailyMenuFoodBusiness;
        private readonly IDailyFoodInspectionBusiness DailyFoodInspectionBusiness;
        private readonly IFoodCatBusiness FoodCatBusiness;
        private readonly IFoodGroupBusiness FoodGroupBusiness;
        private readonly INutritionalNormBusiness NutritionalNormBusiness;
        private readonly IDailyOtherServiceInspectionBusiness DailyOtherServiceInspectionBusiness;
        private readonly IDishCatBusiness DishCatBusiness;
        public DishInspectionController(IDishInspectionBusiness dishinspectionBusiness,
                                    IEatingGroupBusiness eatingGroupBusiness,
                                    IDailyMenuBusiness dailyMenuBusiness,
                                    INotEatingChildrenBusiness notEatingChildrenBusiness,
                                    IDailyMenuDetailBusiness dailyMenuDetailBusiness,
                                    IDailyDishCostBusiness dailyDishCostBusiness,
                                    IDailyMenuFoodBusiness dailyMenuFoodBusiness,
                                    IDailyFoodInspectionBusiness dailyFoodInspectionBusiness,
            IFoodCatBusiness foodcatbusiness, IFoodGroupBusiness foodgroupbusiness, INutritionalNormBusiness nutritionalnormbusiness,
            IDailyOtherServiceInspectionBusiness dailyotherserviceinspectionbusiness, IDishCatBusiness dishCatBusiness)
        {
            this.DishInspectionBusiness = dishinspectionBusiness;
            this.EatingGroupBusiness = eatingGroupBusiness;
            this.DailyMenuBusiness = dailyMenuBusiness;
            this.NotEatingChildrenBusiness = notEatingChildrenBusiness;
            this.DailyMenuDetailBusiness = dailyMenuDetailBusiness;
            this.DailyDishCostBusiness = dailyDishCostBusiness;
            this.DailyMenuFoodBusiness = dailyMenuFoodBusiness;
            this.DailyFoodInspectionBusiness = dailyFoodInspectionBusiness;
            this.FoodCatBusiness = foodcatbusiness;
            this.FoodGroupBusiness = foodgroupbusiness;
            this.NutritionalNormBusiness = nutritionalnormbusiness;
            this.DailyOtherServiceInspectionBusiness = dailyotherserviceinspectionbusiness;
            this.DishCatBusiness = dishCatBusiness;
        }

        #region Add by namdv


        [ValidateAntiForgeryToken]
        public ActionResult Food(string date, int? dailymenuid, int? numberofchildren, int? eatinggroupid, string type)
        {
            GlobalInfo glo = new GlobalInfo();
            DateTime? InspectionDate = null;
            int SchoolID = glo.SchoolID.Value;
            if (!string.IsNullOrEmpty(date))
            {
                var dates = date.Split('-');
                InspectionDate = new DateTime(int.Parse(dates[2].Trim()), int.Parse(dates[1].Trim()), int.Parse(dates[0].Trim()));
            }
            if (InspectionDate.HasValue)
            {
                /*DishInspectionBusiness.SearchBySchool(UserInfo.SchoolID, dtpInspectedDate.Value),
                 lấy DailyMenuID thông qua cboDailyMenuCode.Value)*/
                IDictionary<string, object> dicDishInspection = new Dictionary<string, object>();
                dicDishInspection["InspectedDate"] = InspectionDate.Value;
                dicDishInspection["DailyMenuID"] = dailymenuid;
                DishInspection dishinspection = DishInspectionBusiness.SearchBySchool(SchoolID, dicDishInspection).FirstOrDefault();
                /* Lấy danh sách các dịch vụ: gọi hàm DishInspectionBusiness.GetListOtherService(UserInfo.SchoolID, dtpDate.Value)
                 * => Trả về lstOtherService */
                List<DishInspectionBO> lstOtherService = new List<DishInspectionBO>();
                var listOtherSv = DishInspectionBusiness.GetListOtherService(SchoolID, InspectionDate.Value).ToList();
                if (listOtherSv != null && listOtherSv.Count > 0)
                    lstOtherService = listOtherSv;
                /* Lấy giá của trẻ: gọi hàm DailyDishCostBusiness.MaxDailyDishCost(UserInfo.SchoolID, dtpDate.Value) 
                 * => Trả về DailyPrice (giá của trẻ/ngày) */
                int DailyPrice = DailyDishCostBusiness.MaxDailyDishCost(InspectionDate.Value, SchoolID);
                /*DailyFoodInspectionBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary) với
                + Dictionary[“DishInspectionID”] = DishInspectionID
                lstDailyFoodInspection
                Nếu lstDailyFoodInspection.Count != 0. Load dữ liệu lên grdListFood
                */
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["DishInspectionID"] = ViewData[DishInspectionConstants.DISHINSPECTIONID] = dishinspection.DishInspectionID;
                List<DailyFoodInspection> lstDailyFoodInspection = new List<DailyFoodInspection>();
                var listdfi = DailyFoodInspectionBusiness.SearchBySchool(SchoolID, dic).ToList();
                if (listdfi != null && listdfi.Count > 0)
                {
                    lstDailyFoodInspection = listdfi;
                }
                ViewData[DishInspectionConstants.LISTDAILYFOODINSPECTION] = lstDailyFoodInspection;
                ViewData[DishInspectionConstants.LIST_OTHERSERVICE] = lstOtherService;
                ViewData[DishInspectionConstants.DAILYPRICE] = DailyPrice;
            }
            List<SelectListItem> selectlistitems = new List<SelectListItem>();
            var lstFodd = FoodCatBusiness.Search(new Dictionary<string, object>()).ToList();
            if (lstFodd != null && lstFodd.Count > 0)
            {
                var i = 0;
                foreach (var item in lstFodd)
                {
                    selectlistitems.Add(new SelectListItem { Text = item.FoodName, Value = item.FoodID.ToString(), Selected = i == 0 ? true : false });
                    if (i == 0)
                    {
                        var dic_foodcat = new Dictionary<string, object>();
                        dic_foodcat["FoodID"] = item.FoodID;
                        var FoodCat = FoodCatBusiness.Search(dic_foodcat).FirstOrDefault();
                        ViewData[DishInspectionConstants.PRICEPERONE] = FoodCat.Price;
                        ViewData[DishInspectionConstants.DISCARDEDRATE] = FoodCat.DiscardedRate;
                    }
                    i++;
                }
            }
            List<SelectListItem> selectlistFoodGroup = new List<SelectListItem>();
            List<FoodGroup> lstFoodGroup = FoodGroupBusiness.All.ToList();
            if (lstFoodGroup != null && lstFoodGroup.Count > 0)
            {
                var i = 0;
                foreach (var item in lstFoodGroup)
                {
                    selectlistFoodGroup.Add(new SelectListItem { Text = item.FoodGroupName, Value = item.FoodGroupID.ToString(), Selected = i == 0 ? true : false });
                    i++;
                }
            }
            ViewData[DishInspectionConstants.LIST_FOOD] = selectlistitems;
            ViewData[DishInspectionConstants.NUMBEROFCHILDREN] = numberofchildren;
            ViewData[DishInspectionConstants.FOOD_ACTION_TYPE] = type;
            ViewData[DishInspectionConstants.LISTFOODGROUP] = selectlistFoodGroup;

            var dic_dmd = new Dictionary<string, object>();
            dic_dmd["DailyMenuID"] = dailymenuid;
            if (DailyMenuDetailBusiness.Search(dic_dmd).FirstOrDefault() != null)
            {
                if (DailyMenuDetailBusiness.Search(dic_dmd).FirstOrDefault().DishID.HasValue)
                {
                    var lstRateByMeal = FoodCatBusiness.CaculatorRateByMeal(dailymenuid.Value, SchoolID, glo.AcademicYearID.Value, eatinggroupid.Value);
                    ViewData[DishInspectionConstants.LSTRATEBYMEAL] = lstRateByMeal;
                }
                else ViewData[DishInspectionConstants.LSTRATEBYMEAL] = new List<RateByMealBO>();
            }
            var lstFoodCat = FoodCatBusiness.CaculatorContent(new List<int>(), new List<decimal>(), eatinggroupid.Value, SchoolID, glo.AcademicYearID.Value);
            ViewData[DishInspectionConstants.LIST_FOODCAT] = lstFoodCat;
            IDictionary<string, object> dic_nn = new Dictionary<string, object>();
            dic_nn["AcademicYearID"] = glo.AcademicYearID.Value;
            //dic_nn["EatingGroupID"] = eatinggroupid;
            NutritionalNorm nn = NutritionalNormBusiness.SearchBySchool(SchoolID, dic_nn).Where(o => o.EffectDate <= DateTime.Now).OrderByDescending(o => o.EffectDate).FirstOrDefault();
            ViewData[DishInspectionConstants.NUTRITIONALNORM] = nn;
            ViewData[DishInspectionConstants.EATINGGROUPID] = eatinggroupid;
            /*DailyDishCostBusiness.MaxDailyDishCost(UserInfo.SchoolID, dtpDate.Value)*/
            ViewData[DishInspectionConstants.MAXDAILYDISHCOST] = DailyDishCostBusiness.MaxDailyDishCost(InspectionDate.Value, SchoolID);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Food(FormCollection col)
        {
            string type = col["txtType"];
            string[] lstStt = col["lstSTT"].Split(',');
            string format_food = "Food_{0}";
            string format_foodgrp = "FoodGroup_{0}";
            string format_g = "G_{0}";
            string format_perone = "PricePerOne_{0}";
            int dishInspectionID = int.Parse(col["txtdishinspectionid"]);

            List<DailyFoodInspection> lstDailyFoodInspection = new List<DailyFoodInspection>();
            decimal weight = 0;

            for (var i = 0; i < lstStt.Length; i++)
            {
                if (!string.IsNullOrWhiteSpace(lstStt[i]) && int.Parse(lstStt[i]) > 0)
                {
                    try
                    {
                        weight = decimal.Parse(col[string.Format(format_g, lstStt[i])].Trim());
                        if (weight > 0)
                        {
                            DailyFoodInspection dailyfoodinspection = new DailyFoodInspection();
                            dailyfoodinspection.Weight = weight;
                            dailyfoodinspection.FoodID = int.Parse(col[string.Format(format_food, lstStt[i])].Trim());
                            dailyfoodinspection.PricePerOnce = int.Parse(col[string.Format(format_perone, lstStt[i])].Trim());
                            dailyfoodinspection.FoodGroupID = int.Parse(col[string.Format(format_foodgrp, lstStt[i])].Trim());
                            dailyfoodinspection.DishInspectionID = dishInspectionID;
                            lstDailyFoodInspection.Add(dailyfoodinspection);
                        }
                    }
                    catch (Exception ex) { }
                }
            }
            DailyFoodInspectionBusiness.Insert(lstDailyFoodInspection);
            DailyFoodInspectionBusiness.Save();
            #region insert other service
            string _listOtherServiceID = col["listOtherServiceID"];
            string _listOtherServicePrice = col["listOtherServicePrice"];
            if (!string.IsNullOrWhiteSpace(_listOtherServiceID) && !string.IsNullOrWhiteSpace(_listOtherServicePrice))
            {
                string[] listOtherServiceID = _listOtherServiceID.Split('_');
                string[] listOtherServicePrice = _listOtherServicePrice.Split('_');
                if (listOtherServiceID.Count() == listOtherServicePrice.Count())
                {
                    List<DailyOtherServiceInspection> lstDailyOtherServiceInspection = new List<DailyOtherServiceInspection>();
                    for (var i = 0; i < listOtherServiceID.Count(); i++)
                    {
                        if (!string.IsNullOrWhiteSpace(listOtherServiceID[i]) && !string.IsNullOrWhiteSpace(listOtherServicePrice[i]))
                        {
                            if (int.Parse(listOtherServiceID[i]) > 0 && int.Parse(listOtherServicePrice[i]) > 0)
                            {
                                DailyOtherServiceInspection dailyotherserviceinspection = new DailyOtherServiceInspection();
                                dailyotherserviceinspection.Price = int.Parse(listOtherServicePrice[i]);
                                dailyotherserviceinspection.OtherServiceID = int.Parse(listOtherServiceID[i]);
                                dailyotherserviceinspection.DishInspectionID = dishInspectionID;
                                lstDailyOtherServiceInspection.Add(dailyotherserviceinspection);
                            }
                        }
                    }
                    DailyOtherServiceInspectionBusiness.Insert(lstDailyOtherServiceInspection);
                    DailyOtherServiceInspectionBusiness.Save();
                }
            }
            #endregion
            string resource = "";
            if (type == DishInspectionConstants.FOOD_ACTION_TYPE_CREATE)
                resource = "Common_Label_AddNewMessage";
            if (type == DishInspectionConstants.FOOD_ACTION_TYPE_EDIT)
                resource = "Common_Label_UpdateSuccessMessage";
            return Json(new JsonMessage(Res.Get(resource)));
        }


        [HttpPost]
        public JsonResult LoadPricePerOne()
        {
            var id = int.Parse(Request["id"]);
            var FoodCat = FoodCatBusiness.All.Where(o => o.FoodID == id).FirstOrDefault();
            return Json(new JsonMessage(string.Format("{0},{1}", FoodCat.Price.ToString(), FoodCat.DiscardedRate.HasValue ? FoodCat.DiscardedRate.Value.ToString() : "0")));
        }

        [HttpPost]
        public JsonResult LoadNutritionalBalance()
        {
            var r = Request;
            var lstFood = r["foods"];
            var lstWeight = r["kls"];
            var eatinggroupid = int.Parse(r["eatinggroupid"]);
            GlobalInfo glo = new GlobalInfo();
            #region
            List<int> listFood = new List<int>();
            if (!string.IsNullOrWhiteSpace(lstFood))
            {
                var foods = lstFood.Split('_');
                if (foods.Count() > 0)
                {
                    foreach (var f in foods)
                    {
                        if (!string.IsNullOrWhiteSpace(f) && int.Parse(f) > 0)
                        {
                            listFood.Add(int.Parse(f));
                        }
                    }
                }
            }
            List<decimal> listWeight = new List<decimal>();
            if (!string.IsNullOrWhiteSpace(lstWeight))
            {
                var weights = lstWeight.Split('_');
                if (weights.Count() > 0)
                {
                    foreach (var w in weights)
                    {

                        if (!string.IsNullOrWhiteSpace(w))
                        {
                            decimal new_w = decimal.Parse(w.Replace(".", ","));
                            if (new_w > 0)
                                listWeight.Add(new_w);
                        }
                    }
                }
            }
            #endregion
            var lstFoodCat = FoodCatBusiness.CaculatorContent(listFood, listWeight, eatinggroupid, glo.SchoolID.Value, glo.AcademicYearID.Value);
            lstFoodCat.ProteinAnimal = Math.Round(lstFoodCat.ProteinAnimal, 1);
            lstFoodCat.ProteinPlant = Math.Round(lstFoodCat.ProteinPlant, 1);
            lstFoodCat.FatAnimal = Math.Round(lstFoodCat.FatAnimal, 1);
            lstFoodCat.FatPlant = Math.Round(lstFoodCat.FatPlant, 1);
            lstFoodCat.Sugar = Math.Round(lstFoodCat.Sugar, 1);
            lstFoodCat.Calo = Math.Round(lstFoodCat.Calo, 1);

            lstFoodCat.ProteinAnimalMin = Math.Round(lstFoodCat.ProteinAnimalMin, 2);
            lstFoodCat.ProteinPlantMin = Math.Round(lstFoodCat.ProteinPlantMin, 2);
            lstFoodCat.FatAnimalMin = Math.Round(lstFoodCat.FatAnimalMin, 2);
            lstFoodCat.FatPlantMin = Math.Round(lstFoodCat.FatPlantMin, 2);
            lstFoodCat.SugarMin = Math.Round(lstFoodCat.SugarMin, 2);
            lstFoodCat.CaloMin = Math.Round(lstFoodCat.CaloMin, 2);

            lstFoodCat.ProteinAnimalMax = Math.Round(lstFoodCat.ProteinAnimalMax, 2);
            lstFoodCat.ProteinPlantMax = Math.Round(lstFoodCat.ProteinPlantMax, 2);
            lstFoodCat.FatAnimalMax = Math.Round(lstFoodCat.FatAnimalMax, 2);
            lstFoodCat.FatPlantMax = Math.Round(lstFoodCat.FatPlantMax, 2);
            lstFoodCat.SugarMax = Math.Round(lstFoodCat.SugarMax, 2);
            lstFoodCat.CaloMax = Math.Round(lstFoodCat.CaloMax, 2);
            List<FoodMineralMenuBO> lstFoodMineral = new List<FoodMineralMenuBO>();
            if (lstFoodCat.lstFoodMineral != null && lstFoodCat.lstFoodMineral.Count > 0)
            {
                foreach (var item in lstFoodCat.lstFoodMineral)
                {
                    FoodMineralMenuBO foodmineral = new FoodMineralMenuBO();
                    foodmineral = item;
                    foodmineral.Value = Math.Round(foodmineral.Value, 2);
                    foodmineral.ValueFrom = Math.Round(foodmineral.ValueFrom, 2);
                    foodmineral.ValueTo = Math.Round(foodmineral.ValueTo, 2);
                    lstFoodMineral.Add(foodmineral);
                }
            }
            lstFoodCat.lstFoodMineral = lstFoodMineral;
            return Json(lstFoodCat);
        }

        #endregion
        public ActionResult Index()
        {
            SearchViewModel smodel = new SearchViewModel();
            smodel.FromDate = DateTime.Now.StartOfWeek().Date;
            smodel.ToDate = DateTime.Now;
            ViewData[DishInspectionConstants.SEARCH_VIEW_MODEL] = smodel;

            IEnumerable<DishInspectionViewModel> lst = this._Search(smodel.FromDate, smodel.ToDate);
            ViewData[DishInspectionConstants.LIST_DISHINSPECTION] = lst;

            //GetViewModel();

            return View();
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            IEnumerable<DishInspectionViewModel> lst = this._Search(frm.FromDate, frm.ToDate);
            ViewData[DishInspectionConstants.LIST_DISHINSPECTION] = lst;

            return PartialView("_List");
        }

        [HttpGet]
        
        public ActionResult Create()
        {
            GetViewModel();
            return View(new DishInspectionViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(DishInspectionViewModel model, string DailyMenuName = "")
        {
            GlobalInfo glo = new GlobalInfo();
            DishInspection dishinspection = new DishInspection();
            TryUpdateModel(dishinspection);
            Utils.Utils.TrimObject(dishinspection);

            if (dishinspection.InspectedDate > DateTime.Now)
                return Json(new JsonMessage(Res.Get("DishInspection_Label_InvalidInspectionDate"), JsonMessage.ERROR));

            if (DishInspectionBusiness.CheckDishInspection(glo.SchoolID.Value, dishinspection.InspectedDate, model.DailyMenuID)) //dishinspection.InspectedDate.ToString("dd/MM/yyyy")
                return Json(new JsonMessage(string.Format(Res.Get("Label_Daily_Dish") + " " + DailyMenuName + " " + Res.Get("Label_Day_Select") + " " + dishinspection.InspectedDate.ToString("dd/MM/yyyy") + " " + Res.Get("Label_existing_Dish")), JsonMessage.ERROR));

            dishinspection.SchoolID = glo.SchoolID.Value;
            dishinspection.DailyCostOfChildren = DailyDishCostBusiness.MaxDailyDishCost(dishinspection.InspectedDate, glo.SchoolID.Value);
            dishinspection.IsActive = true;

            List<DailyMenuFood> lstDailyMenuFood = DailyMenuFoodBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "DailyMenuID", dishinspection.DailyMenuID } }).ToList();
            foreach (DailyMenuFood dmf in lstDailyMenuFood)
            {
                DailyFoodInspection dfi = new DailyFoodInspection();
                dfi.FoodID = dmf.FoodID;
                dfi.Weight = dmf.Weight;
                dfi.PricePerOnce = dmf.PricePerOnce;
                dfi.FoodGroupID = dmf.FoodCat.FoodGroupID.Value;
                dishinspection.DailyFoodInspections.Add(dfi);
            }

            this.DishInspectionBusiness.Insert(dishinspection);
            this.DishInspectionBusiness.Save();
            GetViewModel();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        [HttpGet]
        
        public ActionResult Edit(int idDishInspection)
        {
            GlobalInfo glo = new GlobalInfo();
            DishInspectionViewModel model = null;
            DishInspection dishinspection = this.DishInspectionBusiness.Find(idDishInspection);

            if (dishinspection != null)
            {
                model = new DishInspectionViewModel();
                model.DishInspectionID = idDishInspection;
                model.EatingGroupID = dishinspection.EatingGroupID;
                model.MenuType = dishinspection.MenuType;
                model.DailyMenuID = dishinspection.DailyMenuID;
                model.NumberOfChildren = dishinspection.NumberOfChildren;
                model.InspectedDate = dishinspection.InspectedDate;
                GetViewModel();
                ViewData[DishInspectionConstants.LIST_DAILY_MENU_DETAIL] = DailyMenuDetailBusiness
                                                                                .SearchDishInspection(glo.SchoolID.Value,
                                                                                    new Dictionary<string, object> { { "DailyMenuID", dishinspection.DailyMenuID } })
                                                                                .ToList();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int DishInspectionID, DishInspectionViewModel model)
        {
            CheckPermissionForAction(DishInspectionID, "DishInspection");
            GlobalInfo glo = new GlobalInfo();
            DishInspection dishinspection = this.DishInspectionBusiness.Find(DishInspectionID);
            TryUpdateModel(dishinspection);

            if (dishinspection.InspectedDate > DateTime.Now)
                return Json(new JsonMessage(Res.Get("DishInspection_Label_InvalidInspectionDate"), JsonMessage.ERROR));

            Utils.Utils.TrimObject(dishinspection);

            dishinspection.SchoolID = glo.SchoolID.Value;
            dishinspection.DailyCostOfChildren = DailyDishCostBusiness.MaxDailyDishCost(dishinspection.InspectedDate, glo.SchoolID.Value);
            dishinspection.IsActive = true;

            dishinspection.DailyFoodInspections.Clear();

            List<DailyMenuFood> lstDailyMenuFood = DailyMenuFoodBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "DailyMenuID", dishinspection.DailyMenuID } }).ToList();
            foreach (DailyMenuFood dmf in lstDailyMenuFood)
            {
                DailyFoodInspection dfi = new DailyFoodInspection();
                dfi.FoodID = dmf.FoodID;
                dfi.Weight = dmf.Weight;
                dfi.PricePerOnce = dmf.PricePerOnce;
                dfi.FoodGroupID = dmf.FoodCat.FoodGroupID.Value;
                dishinspection.DailyFoodInspections.Add(dfi);
            }

            this.DishInspectionBusiness.Update(dishinspection);
            this.DishInspectionBusiness.Save();
            GetViewModel();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }


        [ValidateAntiForgeryToken]
        public ActionResult Detail(int idDishInspection)
        {
            GlobalInfo glo = new GlobalInfo();
            DishInspection dishinspection = this.DishInspectionBusiness.Find(idDishInspection);
            DishInspectionViewModel model = new DishInspectionViewModel();
            model.DishInspectionID = idDishInspection;
            model.EatingGroupName = dishinspection.EatingGroup.EatingGroupName;
            model.MenuType = dishinspection.MenuType;
            model.DailyMenuCode = dishinspection.DailyMenu.DailyMenuCode;
            model.NumberOfChildren = dishinspection.NumberOfChildren;
            model.InspectedDate = dishinspection.InspectedDate;
            model.DailyMenuID = dishinspection.DailyMenuID;
            model.EatingGroupID = dishinspection.EatingGroupID;
            ViewData[DishInspectionConstants.LIST_DAILY_MENU_DETAIL] = DailyMenuDetailBusiness
                                                                            .SearchDishInspection(glo.SchoolID.Value,
                                                                                new Dictionary<string, object> { { "DailyMenuID", dishinspection.DailyMenuID } })
                                                                            .ToList();
            return View(model);
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            CheckPermissionForAction(id, "DishInspection");
            this.DishInspectionBusiness.Delete(id, true);
            this.DishInspectionBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadDailyMenu(int eatingGroupID, int menuType)
        {
            GlobalInfo glo = new GlobalInfo();
            var lstDailyMenu = DailyMenuBusiness.SearchBySchool(glo.SchoolID.Value,
                                                                    new Dictionary<string, object> { 
                                                                        { "AcademicYearID", glo.AcademicYearID.Value }, 
                                                                        { "EatingGroupID", eatingGroupID },
                                                                        { "MenuType", menuType }
                                                                    })
                                                .ToList();
            List<ComboObject> lstComboObject = lstDailyMenu.Select(u => new ComboObject(u.DailyMenuID.ToString(), u.DailyMenuCode)).ToList();
            return Json(lstComboObject);
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadDailyMenuDetail(int dailyMenuID)
        {
            GlobalInfo glo = new GlobalInfo();
            var listDailyDetail = DailyMenuDetailBusiness.SearchDishInspection(glo.SchoolID.Value, new Dictionary<string, object> { { "DailyMenuID", dailyMenuID } }).ToList();
            //var list = from lstdaily in listDailyDetail
            //           join lstdishcat in DishCatBusiness.All on lstdaily.DishID equals lstdishcat.DishCatID
            //           select new DailyMenuDetailsModel
            //           {
            //               DailyMenuDetailID = lstdaily.DailyMenuDetailID,
            //               DailyMenuID = lstdaily.DailyMenuID,
            //               MealName = lstdaily.MealCat.MealName,
            //               DishID = lstdaily.DishID,
            //               DishName = lstdaily.DishName,
            //               MealCatID = lstdaily.MealID,
            //               Dish_Name = lstdishcat.DishName,
            //               MealCat = lstdaily.MealCat
            //           };

            ViewData[DishInspectionConstants.LIST_DAILY_MENU_DETAIL] = listDailyDetail;
            return PartialView("_GridDailyMenuDetail");
        }


        [ValidateAntiForgeryToken]
        public JsonResult CountChildrenEating(DateTime? date, int eatingGroupID, int menuType)
        {
            if (date.HasValue)
            {
                GlobalInfo glo = new GlobalInfo();

                var lstDailyMenu = DailyMenuBusiness.SearchBySchool(glo.SchoolID.Value,
                                                                        new Dictionary<string, object> { 
                                                                        { "AcademicYearID", glo.AcademicYearID.Value }, 
                                                                        { "EatingGroupID", eatingGroupID  },
                                                                        { "MenuType", menuType }
                                                                    });

                var lstChildrenNotEating = NotEatingChildrenBusiness.SearchBySchool(glo.SchoolID.Value,
                                                                        new Dictionary<string, object> { 
                                                                        { "AcademicYearID", glo.AcademicYearID.Value }, 
                                                                        { "NotEatingDate", DateTime.Now.Date } 
                                                                    });

                int totalChild = lstDailyMenu.Count() > 0 ? lstDailyMenu.Sum(u => u.NumberOfChildren) : 0;

                int totalChildEating = totalChild > 0 ? totalChild - lstChildrenNotEating.Count() : 0;

                if (totalChildEating > 0)
                    return Json(totalChildEating);
                else
                    return Json("");
            }
            return Json("");
        }

        private IEnumerable<DishInspectionViewModel> _Search(DateTime fromDate, DateTime toDate)
        {
            GlobalInfo glo = new GlobalInfo();
            IQueryable<DishInspection> query = this.DishInspectionBusiness.GetInspectedDate(glo.SchoolID.Value, fromDate, toDate);
            IQueryable<DishInspectionViewModel> lst = query.Select(o => new DishInspectionViewModel
            {
                DishInspectionID = o.DishInspectionID,
                InspectedDate = o.InspectedDate,
                DailyCostOfChildren = o.DailyCostOfChildren,
                Expense = o.Expense,
                EatingGroupID = o.EatingGroupID,
                SchoolID = o.SchoolID,
                DailyMenuID = o.DailyMenuID,
                NumberOfChildren = o.NumberOfChildren,
                DailyMenuCode = o.DailyMenu.DailyMenuCode,
                MenuType = o.MenuType,
                EatingGroupName = o.EatingGroup.EatingGroupName
            });

            return lst.ToList();
        }

        private void GetViewModel()
        {
            GlobalInfo glo = new GlobalInfo();
            var lstQEatingGroup = EatingGroupBusiness.SearchBySchool(glo.SchoolID.Value,
                                                                        new Dictionary<string, object> { 
                                                                            { "AcademicYearID", glo.AcademicYearID.Value } 
                                                                        })
                                                                    .Select(u => new { u.EatingGroupID, u.EatingGroupName }).Distinct().ToList();
            ViewData[DishInspectionConstants.LIST_EATING_GROUP] = new SelectList(lstQEatingGroup.Select(u => new ComboObject(u.EatingGroupID.ToString(), u.EatingGroupName)).ToList(), "key", "value");

            var eatingGroup = lstQEatingGroup.FirstOrDefault();

            if (eatingGroup == null)
                throw new BusinessException(Res.Get("Common_Label_Error_Parameter"));

            List<ComboObject> lstMenuType = CommonList.MenuType();
            if (lstMenuType != null)
            {
                ViewData[DishInspectionConstants.LIST_MENU_TYPE] = new SelectList(lstMenuType, "key", "value");
            }
            var menuType = lstMenuType.FirstOrDefault();

            if (menuType == null)
                throw new BusinessException(Res.Get("Common_Label_Error_Parameter"));

            var lstDailyMenu = DailyMenuBusiness.SearchBySchool(glo.SchoolID.Value,
                                                                    new Dictionary<string, object> { 
                                                                        { "AcademicYearID", glo.AcademicYearID.Value }, 
                                                                        //{ "EatingGroupID", eatingGroup.EatingGroupID.Value  },
                                                                        { "MenuType", Convert.ToInt32(menuType.key) }
                                                                    });
            ViewData[DishInspectionConstants.LIST_DAILY_MENU] = new SelectList(lstDailyMenu, "DailyMenuID", "DailyMenuCode");

            var dailyMenu = lstDailyMenu.FirstOrDefault();

            if (dailyMenu == null)
                throw new BusinessException(Res.Get("Common_Label_Error_Parameter"));

            ViewData[DishInspectionConstants.LIST_DAILY_MENU_DETAIL] = DailyMenuDetailBusiness.SearchDishInspection(glo.SchoolID.Value,
                                                                                new Dictionary<string, object> { { "DailyMenuID", dailyMenu.DailyMenuID } })
                                                                            .ToList();
        }


        public FileResult ExportExcel(DishInspectionViewModel dishInspectionViewModel)
        {
            GlobalInfo global = new GlobalInfo();

            Stream excel = DishInspectionBusiness.CreateReportDishInspection(global.SchoolID.Value, global.AcademicYearID.Value, dishInspectionViewModel.DishInspectionID);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            //reportName BDT_[ReportDate]
            string ReportName = "BDT_[ReportDate].xls";
            ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);
            result.FileDownloadName = ReportName;
            return result;
        }


    }
}





