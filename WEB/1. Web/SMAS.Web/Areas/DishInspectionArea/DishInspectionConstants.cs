/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.DishInspectionArea
{
    public class DishInspectionConstants
    {
        public const string LIST_DISHINSPECTION = "listDishInspection";
        public const string LIST_EATING_GROUP = "listEatingGroup";
        public const string LIST_DAILY_MENU = "listDailyMenu";
        public const string LIST_MENU_TYPE = "listMenuType";
        public const string LIST_DAILY_MENU_DETAIL = "listDailyMenuDetail";

        public const string SEARCH_VIEW_MODEL = "searchViewModel";

        public const string COUNT_EATING_CHILDREN = "CountEatingChildren";
        public const string LISTDAILYFOODINSPECTION = "listDailyFoodInspection";

        public const string PRICEPERONE = "pricePerOne";
        public const string DISCARDEDRATE = "discardedrate";
        public const string LIST_FOOD = "list_food";
        public const string LIST_OTHERSERVICE = "list_otherservice";
        public const string DAILYPRICE = "DailyPrice";
        public const string NUMBEROFCHILDREN = "numberofchildren";
        public const string FOOD_ACTION_TYPE = "food_action_type";
        public const string FOOD_ACTION_TYPE_CREATE = "create";
        public const string FOOD_ACTION_TYPE_EDIT = "edit";
        public const string FOOD_ACTION_TYPE_DETAIL = "detail";
        public const string LISTFOODGROUP = "lstFoodGroup";
        public const string LSTRATEBYMEAL = "lstratebymeal";
        public const string LIST_FOODCAT = "list_foodcat";
        public const string DAILYMENUID = "DailyMenuID";
        public const string DAILYDISHCOST = "dailydishcost";
        public const string NUTRITIONALNORM = "nutritionalnorm";
        public const string EATINGGROUPID = "EatingGroupID";
        public const string MAXDAILYDISHCOST = "MaxDailyDishCost";
        public const string DISHINSPECTIONID = "DishInspectionID";
    }
}