using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;
using System.Collections.Generic;
using System.Linq;

namespace SMAS.Web.Areas.DishInspectionArea.Models
{
    public class DishInspectionViewModel
    {
        [ScaffoldColumn(false)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int DishInspectionID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("DishInspection_Label_InspectedDate")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [UIHint("DateTimePicker")]
        public DateTime InspectedDate { get; set; }

        [ResourceDisplayName("EatingGroup_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DishInspectionConstants.LIST_EATING_GROUP)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int EatingGroupID { get; set; }

        [ResourceDisplayName("DishInspection_Label_MenuType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DishInspectionConstants.LIST_MENU_TYPE)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int MenuType { get; set; }

        [ResourceDisplayName("DishInspection_Label_MenuType")]
        public string MenuTypeName
        {
            get
            {
                List<ComboObject> lst = CommonList.MenuType();
                ComboObject c = lst.Where(u => u.key.Equals(MenuType.ToString())).FirstOrDefault();
                return c == null ? string.Empty : c.value;
            }
        }

        [ResourceDisplayName("DailyMenu_Label_DailyMenuCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DishInspectionConstants.LIST_DAILY_MENU)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int DailyMenuID { get; set; }
       
        [ResourceDisplayName("DishInspection_Label_NumberOfChildren")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int NumberOfChildren { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("EatingGroup_Label_AllTitle")]
        public string EatingGroupName{get;set;}

        [ScaffoldColumn(false)]
        [ResourceDisplayName("DishInspection_Label_DailyMenuCode")]
        public string DailyMenuCode { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("DishInspection_Label_DailyCostOfChildren")]
        public int DailyCostOfChildren { get; set; }

        [ResourceDisplayName("DishInspection_Label_Expense")]
        [ScaffoldColumn(false)]
        public int Expense { get; set; }

        [ResourceDisplayName("SchoolProfile_Label_SchoolProfileID")]
        [ScaffoldColumn(false)]
        public int SchoolID { get; set; }
    }
}


