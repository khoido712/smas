using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.DishInspectionArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("DishInspectionArea_SearchViewModel_StartDate")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [UIHint("DateTimePicker")]
        public DateTime FromDate { get; set; }

        [ResourceDisplayName("DishInspectionArea_SearchViewModel_DueDate")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [UIHint("DateTimePicker")]
        public DateTime ToDate { get; set; }
    }
}