﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportAbsencePupilArea
{
    public class ReportAbsencePupilAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportAbsencePupilArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportAbsencePupilArea_default",
                "ReportAbsencePupilArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
