﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.WorkGroupTypeArea
{
    public class WorkGroupTypeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "WorkGroupTypeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "WorkGroupTypeArea_default",
                "WorkGroupTypeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
