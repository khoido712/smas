﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.WorkGroupTypeArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.WorkGroupTypeArea.Controllers
{
    public class WorkGroupTypeController : BaseController
    {        
        private readonly IWorkGroupTypeBusiness WorkGroupTypeBusiness;
		
		public WorkGroupTypeController (IWorkGroupTypeBusiness workgrouptypeBusiness)
		{
			this.WorkGroupTypeBusiness = workgrouptypeBusiness;
		}
		
		//
        // GET: /WorkGroupType/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here

            IEnumerable<WorkGroupTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[WorkGroupTypeConstants.LIST_WORKGROUPTYPE] = lst;
            return View();
        }

		//
        // GET: /WorkGroupType/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;
			//add search info
			//

            IEnumerable<WorkGroupTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[WorkGroupTypeConstants.LIST_WORKGROUPTYPE] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            WorkGroupType workgrouptype = new WorkGroupType();
            TryUpdateModel(workgrouptype); 
            Utils.Utils.TrimObject(workgrouptype);
            workgrouptype.IsActive = true;

            this.WorkGroupTypeBusiness.Insert(workgrouptype);
            this.WorkGroupTypeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int WorkGroupTypeID)
        {
            WorkGroupType workgrouptype = this.WorkGroupTypeBusiness.Find(WorkGroupTypeID);
            TryUpdateModel(workgrouptype);
            Utils.Utils.TrimObject(workgrouptype);
            this.WorkGroupTypeBusiness.Update(workgrouptype);
            this.WorkGroupTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.WorkGroupTypeBusiness.Delete(id);
            this.WorkGroupTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<WorkGroupTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<WorkGroupType> query = this.WorkGroupTypeBusiness.Search(SearchInfo);
            IQueryable<WorkGroupTypeViewModel> lst = query.Select(o => new WorkGroupTypeViewModel {               
						WorkGroupTypeID = o.WorkGroupTypeID,								
						Resolution = o.Resolution,								
						Description = o.Description,								
											
					
				
            });

            return lst.OrderBy(o=>o.Resolution).ToList();
        }        
    }
}





