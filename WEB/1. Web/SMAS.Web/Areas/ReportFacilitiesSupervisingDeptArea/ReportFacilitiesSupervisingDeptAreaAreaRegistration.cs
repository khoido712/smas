﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportFacilitiesSupervisingDeptArea
{
    public class ReportFacilitiesSupervisingDeptAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportFacilitiesSupervisingDeptArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportFacilitiesSupervisingDeptArea_default",
                "ReportFacilitiesSupervisingDeptArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
