﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportFacilitiesSupervisingDeptArea
{
    public class ReportFacilitiesSupervisingDeptConstant
    {
        public const string LIST_YEAR = "ListYear";
        public const string LIST_PERIOD = "ListPeriod";
        public const string PERIOD_ID = "PeriodID";
    }
}