﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportFacilitiesSupervisingDeptArea.Controllers
{
    public class ReportFacilitiesSupervisingDeptController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IIndicatorBusiness IndicatorBusiness;
        private readonly IIndicatorDataBusiness IndicatorDataBusiness;
        public ReportFacilitiesSupervisingDeptController(
            IAcademicYearBusiness AcademicYearBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness,
            IIndicatorBusiness IndicatorBusiness,
            IIndicatorDataBusiness IndicatorDataBusiness)
        {
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.IndicatorBusiness = IndicatorBusiness;
            this.IndicatorDataBusiness = IndicatorDataBusiness;
        }

        public ActionResult Index()
        {
            // Năm học
            List<int> lstYear = AcademicYearBusiness.GetListYearForSupervisingDept(_globalInfo.SupervisingDeptID.Value);
            List<SelectListItem> lstCbYear = new List<SelectListItem>();
            if (lstYear != null && lstYear.Count > 0)
            {
                foreach (var year in lstYear)
                {
                    string text = year.ToString() + "-" + (year + 1).ToString();
                    lstCbYear.Add(new SelectListItem { Text = text, Value = year.ToString() });
                }
            }
            ViewData[ReportFacilitiesSupervisingDeptConstant.LIST_YEAR] = lstCbYear;
            // Kỳ số liệu
            //int period = IndicatorBusiness.GetCurrentPeriodReport(_globalInfo.AcademicYearID.Value);
            List<SelectListItem> listPeriod = new List<SelectListItem>();
            listPeriod.Add(new SelectListItem { Text = Res.Get("Lbl_First_Semester"), Value = "1" });
            listPeriod.Add(new SelectListItem { Text = Res.Get("Lbl_Mid_Semester"), Value = "2" });
            listPeriod.Add(new SelectListItem { Text = Res.Get("Lbl_End_Semester"), Value = "3" });
            //ViewData[ReportFacilitiesSupervisingDeptConstant.PERIOD_ID] = period;
            ViewData[ReportFacilitiesSupervisingDeptConstant.LIST_PERIOD] = listPeriod;
            return View();
        }

        public FileResult ExportExcel(FormCollection fr)
        {
            int year = fr["Year"] != null ? Int32.Parse(fr["Year"]) : 0;
            int periodId = fr["PeriodID"] != null ? Int32.Parse(fr["PeriodID"]) : 0;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ProvinceID", _globalInfo.ProvinceID);
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                dic.Add("DistrictID", _globalInfo.DistrictID);
                dic.Add("SupervisingDeptID", _globalInfo.SupervisingDeptID);
            }
            dic.Add("Period", periodId);
            dic.Add("Year", year);
            List<PhysicalFacilitiesBO> listResult = IndicatorDataBusiness.SynthesizeDataforSupervisingDept(dic, _globalInfo.AppliedLevel);
            listResult = listResult.GroupBy(x => x.IndicatorID).Select(x=>x.FirstOrDefault()).ToList();
            List<PhysicalFacilitiesBO> listResultPrimaryLevel = IndicatorDataBusiness.SynthesizeDataPrimaryLevelforSupervisingDept(dic);
            var test = listResult.Where(x => x.CellAddress == "B158" || x.CellAddress == "B42").ToList();
            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", "PS_BCCSVC.xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            // Phòng/Sở
            SupervisingDept sp = SupervisingDeptBusiness.Find(_globalInfo.SupervisingDeptID);
            sheet.SetCellValue("A2", sp.SupervisingDeptName.ToUpper());
            sheet.SetCellValue("A4", string.Format("Kỳ số liệu {0} - Năm học {1}-{2}", CommonConvert.GetNameByPeriodId(periodId), year, year + 1));
            int iresult = 0; 
            bool isInteger = true;
            var listFieldMapping = this.GetListFieldMapping();
            var listFieldSpec = new List<string>() { "B110","B111","B112","B113","B114","B118"};
            var value1 = 0; var value2 = 0; var value3 = 0; var value4 = 0; var value5 = 0;
            var listFieldSpecExclude = new List<string>();
            listFieldSpecExclude.AddRange(listFieldSpec);
            var listFieldSpecDept = new List<string>() { "B193", "C193", "D193", "E193", "F193", "B195", "C195", "B197", "C197", "B199", "C199", "B201", "C201", "B203", "C203", "D203", "B205", "C205", "D205", "E205" };
            var listFieldAppoinmentRoomIncremental = new List<string>() { "B52", "C52", "D52", "E52", "F52", "G52", "B51", "C51", "D51", "E51", "F51", "G51" };
            var listFieldMappingConvert = listFieldMapping.ToList();
            var ListExcludeField = new List<string>();
            if (listResult.Count == 0 && listResultPrimaryLevel.Count > 0)
            {
                listResult.Add(new PhysicalFacilitiesBO()
                {
                    CellAddress = "A5",
                    CellValue = ""
                });
            }
            for (int i = 0; i < listResult.Count; i++)
            {
                iresult = 0;
                isInteger = false;
                #region Code comment
                //foreach (var objFieldMap in listFieldMappingConvert.ToList())
                //{
                //    iresult = 0;
                //    isInteger = false;
                //    IsHasValuePrimary = false;
                //    isValuePrimary = false;
                //    _cellValuePrimary = 0;
                //    if (!string.IsNullOrEmpty(objFieldMap.Key))
                //    {

                //        if (isValuePrimary)
                //            continue;
                //        var listFieldPrimaryLevel = listResultPrimaryLevel.Where(x => x.CellAddress == objFieldMap.Key && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //        //var objValueResult = listResult.Where(x => x.CellAddress == objFieldMap.Value).FirstOrDefault();
                //        //if (listFieldPrimaryLevel.Count > 0
                //        //    //&& !string.IsNullOrEmpty(objFieldPrimaryLevel.CellValue)
                //        //    //&& !listFieldSpecDept.Contains(listResult[i].CellAddress)
                //        //    )
                //        //{
                //            //IsHasValuePrimary = Int32.TryParse(listFieldPrimaryLevel.Sum(x => int.Parse(x.CellValue)), out _cellValuePrimary);
                //        sheet.SetCellValue(objFieldMap.Value, listFieldPrimaryLevel.Sum(x => int.Parse(x.CellValue)));
                //            //if (IsHasValuePrimary)
                //            //{
                //            //    if (objValueResult != null)
                //            //        isInteger = Int32.TryParse(objValueResult.CellValue, out iresult);
                //            //}
                //            //if (isInteger)
                //            //{
                //            //    if (IsHasValuePrimary)
                //            //        iresult += _cellValuePrimary;
                //            //    sheet.SetCellValue(objFieldMap.Value, iresult);
                //            //}
                //            //else
                //            //{
                //            //    sheet.SetCellValue(objFieldMap.Value, _cellValuePrimary);
                //            //}
                //            //isValuePrimary = true;

                //        //}
                //        //else if (objValueResult != null) {
                //        //    sheet.SetCellValue(objValueResult.CellAddress, objValueResult.CellValue);
                //        //}
                //    }
                //}
                //_cellValuePrimary = 0;
                //if (isValuePrimary)
                //    continue;
                //if (IsHasValuePrimary)
                //{
                //    isInteger = Int32.TryParse(listResult[i].CellValue, out iresult);
                //}
                #endregion
                
                #region Các field đặc biệt của Nhà vệ sinh
                foreach (var item in listFieldSpec.ToList())
                {
                    #region WaterSource
                    if (item == "B110")
                    {
                        var listValueMap = listResult.Where(x => (x.CellAddress == "B193"
                                                                || x.CellAddress == "C193"
                                                                || x.CellAddress == "D193"
                                                                || x.CellAddress == "E193"
                                                                || x.CellAddress == "F193")
                                                                && !string.IsNullOrEmpty(x.CellValue)).ToList();
                        if (listValueMap.Count > 0)
                        {
                            value1 = listValueMap.Where(x => x.CellValue == "1").Count();
                            value2 = listValueMap.Where(x => x.CellValue == "2").Count();
                            value3 = listValueMap.Where(x => x.CellValue == "3").Count();
                            value4 = listValueMap.Where(x => x.CellValue == "4").Count();
                            value5 = listValueMap.Where(x => x.CellValue == "5").Count();
                            
                            listFieldSpec.Remove(item);
                            //int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B110"));
                            //if (pos1 > -1)
                            //    listFieldMappingConvert.RemoveAt(pos1);
                            //
                            #region Code comment
                            //if ((objMap.CellValue == "1" && objWaterSource1 != null) || (objWaterSource1 != null && Int32.TryParse(objWaterSource1.CellValue, out iresult) && iresult > 0))
                            //{
                            //    isInteger = Int32.TryParse(objWaterSource1.CellValue, out iresult);
                            //    if (isInteger && iresult > 0)
                            //    {
                            //        if (objMap.CellValue == "1")
                            //            iresult += 1;
                            //        sheet.SetCellValue(objWaterSource1.CellAddress, iresult);
                            //    }
                            //    else
                            //    {
                            //        sheet.SetCellValue(objWaterSource1.CellAddress, 1);
                            //    }
                            //    int pos = listResult.FindIndex(x => x.CellAddress == "B193");
                            //    if (pos > -1)
                            //        listResult.RemoveAt(pos);
                            //    isValuePrimary = true;
                            //    listFieldSpec.Remove(item);
                            //    int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B110"));
                            //    if (pos1 > -1)
                            //        listFieldMappingConvert.RemoveAt(pos1);
                            //    //continue;
                            //}
                            //var objWaterSource2 = listResult.Where(x => x.CellAddress == "C193").FirstOrDefault();
                            //if ((objMap.CellValue == "2" && objWaterSource2 != null) || (objWaterSource2 != null && Int32.TryParse(objWaterSource2.CellValue, out iresult) && iresult > 0))
                            //{
                            //    isInteger = Int32.TryParse(objWaterSource2.CellValue, out iresult);
                            //    if (isInteger && iresult > 0)
                            //    {
                            //        if (objMap.CellValue == "2")
                            //            iresult += 1;
                            //        sheet.SetCellValue(objWaterSource2.CellAddress, iresult);
                            //    }
                            //    else
                            //    {
                            //        sheet.SetCellValue(objWaterSource2.CellAddress, 1);
                            //    }
                            //    int pos = listResult.FindIndex(x => x.CellAddress == "C193");
                            //    if (pos > -1)
                            //        listResult.RemoveAt(pos);
                            //    isValuePrimary = true;
                            //    listFieldSpec.Remove(item);
                            //    int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B110"));
                            //    if (pos1 > -1)
                            //        listFieldMappingConvert.RemoveAt(pos1);
                            //    //continue;
                            //}
                            //var objWaterSource3 = listResult.Where(x => x.CellAddress == "D193").FirstOrDefault();
                            //if ((objMap.CellValue == "3" && objWaterSource3 != null) || (objWaterSource3 != null && Int32.TryParse(objWaterSource3.CellValue, out iresult) && iresult > 0))
                            //{
                            //    isInteger = Int32.TryParse(objWaterSource3.CellValue, out iresult);
                            //    if (isInteger && iresult > 0)
                            //    {
                            //        if (objMap.CellValue == "3")
                            //            iresult += 1;
                            //        sheet.SetCellValue(objWaterSource3.CellAddress, iresult);
                            //    }
                            //    else
                            //    {
                            //        sheet.SetCellValue(objWaterSource3.CellAddress, 1);
                            //    }
                            //    int pos = listResult.FindIndex(x => x.CellAddress == "D193");
                            //    if (pos > -1)
                            //        listResult.RemoveAt(pos);
                            //    isValuePrimary = true;
                            //    listFieldSpec.Remove(item);
                            //    int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B110"));
                            //    if (pos1 > -1)
                            //        listFieldMappingConvert.RemoveAt(pos1);
                            //    // continue;
                            //}
                            //var objWaterSource4 = listResult.Where(x => x.CellAddress == "E193").FirstOrDefault();
                            //if ((objMap.CellValue == "4" && objWaterSource4 != null) || (objWaterSource4 != null && Int32.TryParse(objWaterSource4.CellValue, out iresult) && iresult > 0))
                            //{
                            //    isInteger = Int32.TryParse(objWaterSource4.CellValue, out iresult);
                            //    if (isInteger && iresult > 0)
                            //    {
                            //        if (objMap.CellValue == "4")
                            //            iresult += 1;
                            //        sheet.SetCellValue(objWaterSource4.CellAddress, iresult);
                            //    }
                            //    else
                            //    {
                            //        sheet.SetCellValue(objWaterSource4.CellAddress, 1);
                            //    }
                            //    int pos = listResult.FindIndex(x => x.CellAddress == "E193");
                            //    if (pos > -1)
                            //        listResult.RemoveAt(pos);
                            //    isValuePrimary = true;
                            //    listFieldSpec.Remove(item);
                            //    int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B110"));
                            //    if (pos1 > -1)
                            //        listFieldMappingConvert.RemoveAt(pos1);
                            //    //continue;
                            //}
                            //var objWaterSource5 = listResult.Where(x => x.CellAddress == "F193").FirstOrDefault();
                            //if ((objMap.CellValue == "5" && objWaterSource5 != null) || (objWaterSource5 != null && Int32.TryParse(objWaterSource5.CellValue, out iresult) && iresult > 0))
                            //{
                            //    isInteger = Int32.TryParse(objWaterSource5.CellValue, out iresult);
                            //    if (isInteger && iresult > 0)
                            //    {
                            //        if (objMap.CellValue == "5")
                            //            iresult += 1;
                            //        sheet.SetCellValue(objWaterSource5.CellAddress, iresult);
                            //    }
                            //    else
                            //    {
                            //        sheet.SetCellValue(objWaterSource5.CellAddress, 1);
                            //    }
                            //    int pos = listResult.FindIndex(x => x.CellAddress == "F193");
                            //    if (pos > -1)
                            //        listResult.RemoveAt(pos);
                            //    isValuePrimary = true;
                            //    listFieldSpec.Remove(item);
                            //    int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B110"));
                            //    if (pos1 > -1)
                            //        listFieldMappingConvert.RemoveAt(pos1);
                            //    //continue;
                            //}
                            #endregion
                        }
                        if (listFieldMapping.Keys.Contains(item))
                        {
                            var CellPrimary = listFieldMapping.Where(x => x.Key == item).FirstOrDefault();

                            var listValueMapPrimaryLevel = listResultPrimaryLevel.Where(x => (x.CellAddress == "B110"
                                                                || x.CellAddress == "C110"
                                                                || x.CellAddress == "D110"
                                                                || x.CellAddress == "E110"
                                                                || x.CellAddress == "F110") 
                                && !string.IsNullOrEmpty(x.CellValue)).ToList();
                            value1 += listValueMapPrimaryLevel.Where(x => x.CellValue == "1").Count() > 0 ? listValueMapPrimaryLevel.Where(x => x.CellValue == "1").Count() : 0;
                            value2 += listValueMapPrimaryLevel.Where(x => x.CellValue == "2").Count() > 0 ? listValueMapPrimaryLevel.Where(x => x.CellValue == "2").Count() : 0;
                            value3 += listValueMapPrimaryLevel.Where(x => x.CellValue == "3").Count() > 0 ? listValueMapPrimaryLevel.Where(x => x.CellValue == "3").Count() : 0;
                            value4 += listValueMapPrimaryLevel.Where(x => x.CellValue == "4").Count() > 0 ? listValueMapPrimaryLevel.Where(x => x.CellValue == "4").Count() : 0;
                            value5 += listValueMapPrimaryLevel.Where(x => x.CellValue == "5").Count() > 0 ? listValueMapPrimaryLevel.Where(x => x.CellValue == "5").Count() : 0;
                        }
                        if (value1 > 0)
                            sheet.SetCellValue("B193", value1);
                        if (value2 > 0)
                            sheet.SetCellValue("C193", value2);
                        if (value3 > 0)
                            sheet.SetCellValue("D193", value3);
                        if (value4 > 0)
                            sheet.SetCellValue("E193", value4);
                        if (value5 > 0)
                            sheet.SetCellValue("F193", value5);
                    }
                    #endregion

                    #region IsCleanWater
                    if (item == "B111")
                    {
                        var objMap = listResultPrimaryLevel.Where(x => x.CellAddress == item && x.CellAddress == "1").FirstOrDefault();
                        if (objMap != null && !string.IsNullOrEmpty(objMap.CellValue))
                        {
                            if (!string.IsNullOrEmpty(objMap.CellValue))
                            {
                                var objCleanWater = listResult.Where(x => x.CellAddress == "B195" && x.CellAddress == "1").FirstOrDefault();
                                if (objCleanWater != null)
                                {
                                    isInteger = Int32.TryParse(objCleanWater.CellValue, out iresult);
                                    if (isInteger && iresult > 0)
                                    {
                                        iresult += 1;
                                        sheet.SetCellValue(objCleanWater.CellAddress, iresult);
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(objCleanWater.CellAddress, 1);
                                    }
                                    int pos = listResult.FindIndex(x => x.CellAddress == "B195" && x.CellAddress == "1");
                                    if (pos > -1)
                                        listResult.RemoveAt(pos);
                                    listFieldSpec.Remove(item);
                                    //int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B111"));
                                    //if (pos1 > -1)
                                    //    listFieldMappingConvert.RemoveAt(pos1);
                                    continue;
                                }
                            }
                            else
                            {
                                var objCleanWater = listResult.Where(x => x.CellAddress == "C195" && x.CellAddress == "1").FirstOrDefault();
                                if (objCleanWater != null)
                                {
                                    isInteger = Int32.TryParse(objCleanWater.CellValue, out iresult);
                                    if (isInteger && iresult > 0)
                                    {
                                        iresult += 1;
                                        sheet.SetCellValue(objCleanWater.CellAddress, iresult);
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(objCleanWater.CellAddress, 1);
                                    }
                                    int pos = listResult.FindIndex(x => x.CellAddress == "C195" && x.CellAddress == "1");
                                    if (pos > -1)
                                        listResult.RemoveAt(pos);
                                    listFieldSpec.Remove(item);
                                    //int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B111"));
                                    //if (pos1 > -1)
                                    //    listFieldMappingConvert.RemoveAt(pos1);
                                    continue;
                                }
                            }
                        }
                    }
                    #endregion

                    #region ElectricSource
                    if (item == "B112")
                    {
                        var objMap = listResultPrimaryLevel.Where(x => x.CellAddress == item && x.CellAddress == "1").FirstOrDefault();
                        if (objMap != null && !string.IsNullOrEmpty(objMap.CellValue))
                        {
                            if (!string.IsNullOrEmpty(objMap.CellValue))
                            {
                                var objElectricSource = listResult.Where(x => x.CellAddress == "B197" && x.CellAddress == "1").FirstOrDefault();
                                if (objElectricSource != null)
                                {
                                    isInteger = Int32.TryParse(objElectricSource.CellValue, out iresult);
                                    if (isInteger && iresult > 0)
                                    {
                                        iresult += 1;
                                        sheet.SetCellValue(objElectricSource.CellAddress, iresult);
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(objElectricSource.CellAddress, 1);
                                    }
                                    int pos = listResult.FindIndex(x => x.CellAddress == "B197" && x.CellAddress == "1");
                                    if (pos > -1)
                                        listResult.RemoveAt(pos);
                                    listFieldSpec.Remove(item);
                                    //int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B112"));
                                    //if (pos1 > -1)
                                    //    listFieldMappingConvert.RemoveAt(pos1);
                                    continue;
                                }
                            }
                            else
                            {
                                var objElectricSource = listResult.Where(x => x.CellAddress == "C197" && x.CellAddress == "1").FirstOrDefault();
                                if (objElectricSource != null)
                                {
                                    isInteger = Int32.TryParse(objElectricSource.CellValue, out iresult);
                                    if (isInteger && iresult > 0)
                                    {
                                        iresult += 1;
                                        sheet.SetCellValue(objElectricSource.CellAddress, iresult);
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(objElectricSource.CellAddress, 1);
                                    }
                                    int pos = listResult.FindIndex(x => x.CellAddress == "C197" && x.CellAddress == "1");
                                    if (pos > -1)
                                        listResult.RemoveAt(pos);
                                    listFieldSpec.Remove(item);
                                    //int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B112"));
                                    //if (pos1 > -1)
                                    //    listFieldMappingConvert.RemoveAt(pos1);
                                    continue;
                                }
                            }
                        }
                    }
                    #endregion

                    #region Kitchen
                    if (item == "B113")
                    {
                        var objMap = listResultPrimaryLevel.Where(x => x.CellAddress == item && x.CellAddress == "1").FirstOrDefault();
                        if (objMap != null && !string.IsNullOrEmpty(objMap.CellValue))
                        {
                            if (!string.IsNullOrEmpty(objMap.CellValue))
                            {
                                var objKitchen = listResult.Where(x => x.CellAddress == "B199" && x.CellAddress == "1").FirstOrDefault();
                                if (objKitchen != null)
                                {
                                    isInteger = Int32.TryParse(objKitchen.CellValue, out iresult);
                                    if (isInteger && iresult > 0)
                                    {
                                        iresult += 1;
                                        sheet.SetCellValue(objKitchen.CellAddress, iresult);
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(objKitchen.CellAddress, 1);
                                    }
                                    int pos = listResult.FindIndex(x => x.CellAddress == "B199" && x.CellAddress == "1");
                                    if (pos > -1)
                                        listResult.RemoveAt(pos);
                                    listFieldSpec.Remove(item);
                                    //int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B113"));
                                    //if (pos1 > -1)
                                    //    listFieldMappingConvert.RemoveAt(pos1);
                                    continue;
                                }
                            }
                            else
                            {
                                var objKitchen = listResult.Where(x => x.CellAddress == "C199" && x.CellAddress == "1").FirstOrDefault();
                                if (objKitchen != null)
                                {
                                    isInteger = Int32.TryParse(objKitchen.CellValue, out iresult);
                                    if (isInteger && iresult > 0)
                                    {
                                        iresult += 1;
                                        sheet.SetCellValue(objKitchen.CellAddress, iresult);
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(objKitchen.CellAddress, 1);
                                    }
                                    int pos = listResult.FindIndex(x => x.CellAddress == "C199" && x.CellAddress == "1");
                                    if (pos > -1)
                                        listResult.RemoveAt(pos);
                                    listFieldSpec.Remove(item);
                                    //int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B113"));
                                    //if (pos1 > -1)
                                    //    listFieldMappingConvert.RemoveAt(pos1);
                                    continue;
                                }
                            }
                        }
                    }
                    #endregion

                    #region SchoolGate
                    if (item == "B114")
                    {
                        var objMap = listResultPrimaryLevel.Where(x => x.CellAddress == item).FirstOrDefault();
                        if (objMap != null && !string.IsNullOrEmpty(objMap.CellValue))
                        {
                            if (!string.IsNullOrEmpty(objMap.CellValue))
                            {
                                var objSchoolGate = listResult.Where(x => x.CellAddress == "B201").FirstOrDefault();
                                if (objSchoolGate != null)
                                {
                                    isInteger = Int32.TryParse(objSchoolGate.CellValue, out iresult);
                                    if (isInteger && iresult > 0)
                                    {
                                        iresult += 1;
                                        sheet.SetCellValue(objSchoolGate.CellAddress, iresult);
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(objSchoolGate.CellAddress, 1);
                                    }
                                    int pos = listResult.FindIndex(x => x.CellAddress == "B201");
                                    if (pos > -1)
                                        listResult.RemoveAt(pos);
                                    listFieldSpec.Remove(item);
                                    //int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B114"));
                                    //if (pos1 > -1)
                                    //    listFieldMappingConvert.RemoveAt(pos1);
                                    continue;
                                }
                            }
                            else
                            {
                                var objSchoolGate = listResult.Where(x => x.CellAddress == "C201").FirstOrDefault();
                                if (objSchoolGate != null)
                                {
                                    isInteger = Int32.TryParse(objSchoolGate.CellValue, out iresult);
                                    if (isInteger && iresult > 0)
                                    {
                                        if (objMap.CellValue == objSchoolGate.CellValue)
                                            iresult += 1;
                                        sheet.SetCellValue(objSchoolGate.CellAddress, iresult);
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(objSchoolGate.CellAddress, 1);
                                    }
                                    int pos = listResult.FindIndex(x => x.CellAddress == "C201");
                                    if (pos > -1)
                                        listResult.RemoveAt(pos);
                                    listFieldSpec.Remove(item);
                                    //int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B114"));
                                    //if (pos1 > -1)
                                    //    listFieldMappingConvert.RemoveAt(pos1);
                                    continue;
                                }
                            }
                        }
                    }
                    #endregion

                    #region Fence
                    if (item == "B118")
                    {
                        var listValueMapFence = listResultPrimaryLevel.Where(x => (x.CellAddress == "B203"
                                                                            || x.CellAddress == "C203"
                                                                            || x.CellAddress == "D203")
                                                                            && !string.IsNullOrEmpty(x.CellValue)
                                                                            ).ToList();
                        value1 = listValueMapFence.Where(x => x.CellValue == "1").Count();
                        value2 = listValueMapFence.Where(x => x.CellValue == "2").Count();
                        value3 = listValueMapFence.Where(x => x.CellValue == "3").Count();
                        if (listFieldMapping.Keys.Contains(item))
                        {
                            var CellPrimary = listFieldMapping.Where(x => x.Value == item).FirstOrDefault();
                            var listValueMapPrimaryLevel = listResultPrimaryLevel.Where(x => (x.CellAddress == "B118"
                                                                            || x.CellAddress == "C118"
                                                                            || x.CellAddress == "D118")
                                && !string.IsNullOrEmpty(x.CellValue)).ToList();
                            value1 += listValueMapPrimaryLevel.Where(x => x.CellValue == "1").Count() > 0 ? listValueMapPrimaryLevel.Where(x => x.CellValue == "1").Count() : 0;
                            value2 += listValueMapPrimaryLevel.Where(x => x.CellValue == "2").Count() > 0 ? listValueMapPrimaryLevel.Where(x => x.CellValue == "2").Count() : 0;
                            value3 += listValueMapPrimaryLevel.Where(x => x.CellValue == "3").Count() > 0 ? listValueMapPrimaryLevel.Where(x => x.CellValue == "3").Count() : 0;
                        }
                        if (value1 > 0)
                            sheet.SetCellValue("B203", value1);
                        if (value2 > 0)
                            sheet.SetCellValue("C203", value2);
                        if (value3 > 0)
                            sheet.SetCellValue("D203", value3);
                        listFieldSpec.Remove(item);
                        //int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B118"));
                        //if (pos1 > -1)
                        //    listFieldMappingConvert.RemoveAt(pos1);
                        #region Code Comment
                        //if (objMap != null && !string.IsNullOrEmpty(objMap.CellValue))
                        //{
                        //    var objFence = listResult.Where(x => x.CellAddress == "B203").FirstOrDefault();
                        //    if ((objMap.CellValue == "1" && objFence != null) || (objFence != null && Int32.TryParse(objFence.CellValue, out iresult) && iresult > 0))
                        //    {
                        //        isInteger = Int32.TryParse(objFence.CellValue, out iresult);
                        //        if (isInteger && iresult > 0)
                        //        {
                        //            if (objMap.CellValue == "1")
                        //                iresult += 1;
                        //            sheet.SetCellValue(objFence.CellAddress, iresult);
                        //        }
                        //        else
                        //        {
                        //            sheet.SetCellValue(objFence.CellAddress, 1);
                        //        }
                        //        int pos = listResult.FindIndex(x => x.CellAddress == "B203");
                        //        if (pos > -1)
                        //            listResult.RemoveAt(pos);
                        //        isValuePrimary = true;
                        //        listFieldSpec.Remove(item);
                        //        int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B118"));
                        //        if (pos1 > -1)
                        //            listFieldMappingConvert.RemoveAt(pos1);
                        //        // continue;
                        //    }
                        //    var objFence2 = listResult.Where(x => x.CellAddress == "C203").FirstOrDefault();
                        //    if ((objMap.CellValue == "2" && objFence2 != null) || (objFence2 != null && Int32.TryParse(objFence2.CellValue, out iresult) && iresult > 0))
                        //    {
                        //        isInteger = Int32.TryParse(objFence2.CellValue, out iresult);
                        //        if (isInteger && iresult > 0)
                        //        {
                        //            if (objMap.CellValue == "2")
                        //                iresult += 1;
                        //            sheet.SetCellValue(objFence2.CellAddress, iresult);
                        //        }
                        //        else
                        //        {
                        //            sheet.SetCellValue(objFence2.CellAddress, 1);
                        //        }
                        //        int pos = listResult.FindIndex(x => x.CellAddress == "C203");
                        //        if (pos > -1)
                        //            listResult.RemoveAt(pos);
                        //        isValuePrimary = true;
                        //        listFieldSpec.Remove(item);
                        //        int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B118"));
                        //        if (pos1 > -1)
                        //            listFieldMappingConvert.RemoveAt(pos1);
                        //        //continue;
                        //    }
                        //    var objFence3 = listResult.Where(x => x.CellAddress == "D203").FirstOrDefault();
                        //    if ((objMap.CellValue == "3" && objFence3 != null) || (objFence3 != null && Int32.TryParse(objFence3.CellValue, out iresult) && iresult > 0))
                        //    {
                        //        isInteger = Int32.TryParse(objFence3.CellValue, out iresult);
                        //        if (isInteger && iresult > 0)
                        //        {
                        //            if (objMap.CellValue == "3")
                        //                iresult += 1;
                        //            sheet.SetCellValue(objFence3.CellAddress, iresult);
                        //        }
                        //        else
                        //        {
                        //            sheet.SetCellValue(objFence3.CellAddress, 1);
                        //        }
                        //        int pos = listResult.FindIndex(x => x.CellAddress == "D203");
                        //        if (pos > -1)
                        //            listResult.RemoveAt(pos);
                        //        isValuePrimary = true;
                        //        listFieldSpec.Remove(item);
                        //        int pos1 = listFieldMappingConvert.FindIndex((x => x.Key == "B118"));
                        //        if (pos1 > -1)
                        //            listFieldMappingConvert.RemoveAt(pos1);
                        //        //continue;
                        //    }
                        //}
                        #endregion

                    }
                    #endregion

                    listFieldSpec.Remove(item);
                }
                #endregion
                #region Increment filed
                foreach (var item in listFieldMapping)
                {
                    if (ListExcludeField.Contains(item.Value) || listFieldSpecExclude.Contains(item.Key))
                        continue;
                    ListExcludeField.Add(item.Value);
                    var listPrimary = listResultPrimaryLevel.Where(x => x.CellAddress == item.Key && !string.IsNullOrEmpty(x.CellValue)).ToList();
                    var valuePrimary = listPrimary.Sum(x => int.Parse(x.CellValue)) > 0 ? listPrimary.Sum(x => int.Parse(x.CellValue)) : 0;
                    var listResultMap = listResult.Where(x => x.CellAddress == item.Value && !string.IsNullOrEmpty(x.CellValue)).ToList();
                    var valueResultMap = listResultMap.Sum(x => int.Parse(x.CellValue)) > 0 ? listResultMap.Sum(x => int.Parse(x.CellValue)) : 0;
                    sheet.SetCellValue(item.Value, valuePrimary + valueResultMap);
                }
                #endregion
                if (!ListExcludeField.Contains(listResult[i].CellAddress) && !listFieldMapping.Values.Contains(listResult[i].CellAddress))
                {
                    var listValueByCellAddress = listResult.Where(x => x.CellAddress == listResult[i].CellAddress
                                                    && !string.IsNullOrEmpty(x.CellValue)).ToList();
                    iresult = listValueByCellAddress.Sum(x => int.Parse(x.CellValue));
                    sheet.SetCellValue(listResult[i].CellAddress, iresult);
                }
            }
            #region Các filed cộng dồn giá trị từ các filed khác
            var listAppointmentValueFieldPrimary = listResultPrimaryLevel.Where(x => listFieldAppoinmentRoomIncremental.Contains(x.CellAddress)).ToList();
            if (listAppointmentValueFieldPrimary.Count > 0)
            {
                var listMapField = new List<string>();
                var dicMapField = new Dictionary<string, List<string>>();
                dicMapField.Add("B73", new List<string>() { "B51", "B52", "B73",string.Empty });
                dicMapField.Add("C73", new List<string>() { "C51", "C52", "C73",string.Empty });
                dicMapField.Add("D73", new List<string>() { "D51", "D52", "D73",string.Empty });
                dicMapField.Add("E73", new List<string>() { "E51", "E52", "E73",string.Empty });
                dicMapField.Add("F73", new List<string>() { "F51", "F52", "F73",string.Empty });
                dicMapField.Add("G73", new List<string>() { "G51", "G52", "G73",string.Empty });
                ProcessIncrementalField(listResult, listResultPrimaryLevel, dicMapField);
                foreach (var itemMap in dicMapField)
                {
                    sheet.SetCellValue(itemMap.Key, itemMap.Value[3]);
                }
                #region Comment code
                //iresult = 0; iresult1 = 0; iresult2 = 0;
                //var listTotal = listAppointmentValueFieldPrimary.Where(x => x.CellAddress == "B52" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //var listTotal1 = listAppointmentValueFieldPrimary.Where(x => x.CellAddress == "B51" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //if (listTotal.Count > 0)
                //{
                //    iresult = listTotal.Sum(x => int.Parse(x.CellValue));
                //}
                //if (listTotal1.Count > 0)
                //{
                //    iresult1 = listTotal1.Sum(x => int.Parse(x.CellValue));
                //}
                //var listTotalValueField = listResult.Where(x => x.CellAddress == "B73" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //if (listTotalValueField.Count > 0)
                //{
                //    iresult2 = listTotalValueField.Sum(x => int.Parse(x.CellValue));
                //    foreach (var item in listTotalValueField)
                //    {
                //        int pos = listResult.FindIndex(x => x.CellAddress == item.CellAddress);
                //        if (pos > -1)
                //            listResult.RemoveAt(pos);
                //    }
                //}
                //sheet.SetCellValue("B73", iresult + iresult1 + iresult2);
                ///////
                //iresult = 0; iresult1 = 0; iresult2 = 0;
                //var listTotalDetermine = listAppointmentValueFieldPrimary.Where(x => x.CellAddress == "C52" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //var listTotalDetermine1 = listAppointmentValueFieldPrimary.Where(x => x.CellAddress == "C51" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //if (listTotalDetermine.Count > 0)
                //{
                //    iresult = listTotalDetermine.Sum(x => int.Parse(x.CellValue));
                //}
                //if (listTotalDetermine1.Count > 0)
                //{
                //    iresult1 = listTotalDetermine1.Sum(x => int.Parse(x.CellValue));
                //}
                //var listDetermineValueField = listResult.Where(x => x.CellAddress == "C73" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //if (listDetermineValueField.Count > 0)
                //{
                //    iresult2 = listDetermineValueField.Sum(x => int.Parse(x.CellValue));
                //    foreach (var item in listDetermineValueField)
                //    {
                //        int pos = listResult.FindIndex(x => x.CellAddress == item.CellAddress);
                //        if (pos > -1)
                //            listResult.RemoveAt(pos);
                //    }
                //}
                //sheet.SetCellValue("C73", iresult + iresult1 + iresult2);
                ///////
                //iresult = 0; iresult1 = 0; iresult2 = 0;
                //var listTotalDetermineNew = listAppointmentValueFieldPrimary.Where(x => x.CellAddress == "D52" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //var listTotalDetermine1New = listAppointmentValueFieldPrimary.Where(x => x.CellAddress == "D51" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //if (listTotalDetermineNew.Count > 0)
                //{
                //    iresult = listTotalDetermineNew.Sum(x => int.Parse(x.CellValue));
                //}
                //if (listTotalDetermine1New.Count > 0)
                //{
                //    iresult1 = listTotalDetermine1New.Sum(x => int.Parse(x.CellValue));
                //}
                //var listDetermineValueFieldNew = listResult.Where(x => x.CellAddress == "D73" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //if (listDetermineValueFieldNew.Count > 0)
                //{
                //    iresult2 = listDetermineValueFieldNew.Sum(x => int.Parse(x.CellValue));
                //    foreach (var item in listDetermineValueFieldNew)
                //    {
                //        int pos = listResult.FindIndex(x => x.CellAddress == item.CellAddress);
                //        if (pos > -1)
                //            listResult.RemoveAt(pos);
                //    }
                //}
                //sheet.SetCellValue("D73", iresult + iresult1 + iresult2);
                ///////
                //iresult = 0; iresult1 = 0; iresult2 = 0;
                //var listTotalDetermineNone = listAppointmentValueFieldPrimary.Where(x => x.CellAddress == "E52" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //var listTotalDetermine1None = listAppointmentValueFieldPrimary.Where(x => x.CellAddress == "E51" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //if (listTotalDetermineNone.Count > 0)
                //{
                //    iresult = listTotalDetermineNone.Sum(x => int.Parse(x.CellValue));
                //}
                //if (listTotalDetermine1None.Count > 0)
                //{
                //    iresult1 = listTotalDetermine1None.Sum(x => int.Parse(x.CellValue));
                //}
                //var listDetermineValueFieldNone = listResult.Where(x => x.CellAddress == "E73" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //if (listDetermineValueFieldNone != null)
                //{
                //    iresult2 = listDetermineValueFieldNone.Sum(x => int.Parse(x.CellValue));
                //    foreach (var item in listDetermineValueFieldNone)
                //    {
                //        int pos = listResult.FindIndex(x => x.CellAddress == item.CellAddress);
                //        if (pos > -1)
                //            listResult.RemoveAt(pos);
                //    }
                //}
                //sheet.SetCellValue("E73", iresult + iresult1 + iresult2);
                ///////
                //iresult = 0; iresult1 = 0; iresult2 = 0;
                //var listTotalDetermineNoneNew = listAppointmentValueFieldPrimary.Where(x => x.CellAddress == "F52" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //var listTotalDetermine1NoneNew = listAppointmentValueFieldPrimary.Where(x => x.CellAddress == "F51" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //if (listTotalDetermineNoneNew.Count > 0)
                //{
                //    iresult = listTotalDetermineNoneNew.Sum(x => int.Parse(x.CellValue));
                //}
                //if (listTotalDetermine1NoneNew.Count > 0)
                //{
                //    iresult1 = listTotalDetermine1NoneNew.Sum(x => int.Parse(x.CellValue));
                //}
                //var listDetermineValueFieldNoneNew = listResult.Where(x => x.CellAddress == "F73" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //if (listDetermineValueFieldNoneNew.Count > 0)
                //{
                //    iresult2 = listDetermineValueFieldNoneNew.Sum(x => int.Parse(x.CellValue));
                //    foreach (var item in listDetermineValueFieldNoneNew)
                //    {
                //        int pos = listResult.FindIndex(x => x.CellAddress == item.CellAddress);
                //        if (pos > -1)
                //            listResult.RemoveAt(pos);
                //    }
                //}
                //sheet.SetCellValue("F73", iresult + iresult1 + iresult2);
                ///////
                //iresult = 0; iresult1 = 0; iresult2 = 0;
                //var listTotalTemp = listAppointmentValueFieldPrimary.Where(x => x.CellAddress == "G52" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //var listTotalTemp1 = listAppointmentValueFieldPrimary.Where(x => x.CellAddress == "G51" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //if (listTotalTemp.Count > 0)
                //{
                //    iresult = listTotalTemp.Sum(x => int.Parse(x.CellValue));
                //}
                //if (listTotalTemp1.Count > 0)
                //{
                //    iresult1 = listTotalTemp1.Sum(x => int.Parse(x.CellValue));
                //}
                //var listDetermineValueFieldTemp = listResult.Where(x => x.CellAddress == "G73" && !string.IsNullOrEmpty(x.CellValue)).ToList();
                //if (listDetermineValueFieldTemp != null)
                //{
                //    iresult2 = listDetermineValueFieldTemp.Sum(x => int.Parse(x.CellValue));
                //    foreach (var item in listDetermineValueFieldTemp)
                //    {
                //        int pos = listResult.FindIndex(x => x.CellAddress == item.CellAddress);
                //        if (pos > -1)
                //            listResult.RemoveAt(pos);
                //    }
                //}
                //sheet.SetCellValue("G73", iresult + iresult1 + iresult2);
                #endregion
            }
            #endregion
            sheet.SetCellValue("A5", string.Empty);
            string fileName = string.Format("PS_BCCSVC_{0}.xls", Utils.Utils.StripVNSignAndSpace(CommonConvert.GetNameByPeriodId(periodId)));
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = fileName;
            return result;
        }
        #region Process For Incremental Field
        /// <summary>
        /// Process For Incremental Field
        /// </summary>
        /// <param name="listIndicatorDataOrderLevel"></param>
        /// <param name="ListIndicatorDataPrimary"></param>
        /// <param name="DicMapField"> <Key,List<string>value> value[0]&[1] : Primary field </param>
        /// <param name="DicMapField"> <Key,List<string>value> value[2] : Order level field </param>
        private void ProcessIncrementalField(List<PhysicalFacilitiesBO> listIndicatorDataOrderLevel, List<PhysicalFacilitiesBO> ListIndicatorDataPrimary, Dictionary<string, List<string>> DicMapField)
        {
            int iresult = 0; int iresult1 = 0; int iresult2 = 0;
            foreach (var dicMapField in DicMapField)
            {
                //Get indicator primary level
                var listTotalDetermineNoneNew = ListIndicatorDataPrimary.Where(x => x.CellAddress == dicMapField.Value[0] && !string.IsNullOrEmpty(x.CellValue)).ToList();
                var listTotalDetermine1NoneNew = ListIndicatorDataPrimary.Where(x => x.CellAddress == dicMapField.Value[1] && !string.IsNullOrEmpty(x.CellValue)).ToList();
                if (listTotalDetermineNoneNew.Count > 0)
                {
                    iresult = listTotalDetermineNoneNew.Sum(x => int.Parse(x.CellValue));
                }
                if (listTotalDetermine1NoneNew.Count > 0)
                {
                    iresult1 = listTotalDetermine1NoneNew.Sum(x => int.Parse(x.CellValue));
                }
                // Get indicator data Order level
                var listDetermineValueFieldNoneNew = listIndicatorDataOrderLevel.Where(x => x.CellAddress == dicMapField.Value[2] && !string.IsNullOrEmpty(x.CellValue)).ToList();
                if (listDetermineValueFieldNoneNew.Count > 0)
                {
                    iresult2 = listDetermineValueFieldNoneNew.Sum(x => int.Parse(x.CellValue));
                    foreach (var item in listDetermineValueFieldNoneNew)
                    {
                        int pos = listIndicatorDataOrderLevel.FindIndex(x => x.CellAddress == item.CellAddress);
                        if (pos > -1)
                            listIndicatorDataOrderLevel.RemoveAt(pos);
                    }
                }
                dicMapField.Value[3] = (iresult + iresult1 + iresult2).ToString();
            }
        }
        #endregion
        /// <summary>
        /// dirFieldMapping: Key = PrimaryLevel field, Value = OrderLevel field 
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetListFieldMapping()
        {
            var dirFieldMapping = new Dictionary<string, string>();
            dirFieldMapping.Add("B28", "B42");
            dirFieldMapping.Add("C28", "C42");
            dirFieldMapping.Add("D28", "D42");
            dirFieldMapping.Add("E28", "E42");
            dirFieldMapping.Add("F28", "F42");
            dirFieldMapping.Add("G28", "G42");

            dirFieldMapping.Add("B34", "B59");
            dirFieldMapping.Add("C34", "C59");
            dirFieldMapping.Add("D34", "D59");
            dirFieldMapping.Add("E34", "E59");
            dirFieldMapping.Add("F34", "F59");
            dirFieldMapping.Add("G34", "G59");

            dirFieldMapping.Add("B36", "B63");
            dirFieldMapping.Add("C36", "C63");
            dirFieldMapping.Add("D36", "D63");
            dirFieldMapping.Add("E36", "E63");
            dirFieldMapping.Add("F36", "F63");
            dirFieldMapping.Add("G36", "G63");

            dirFieldMapping.Add("B42", "B49");
            dirFieldMapping.Add("C42", "C49");
            dirFieldMapping.Add("D42", "D49");
            dirFieldMapping.Add("E42", "E49");
            dirFieldMapping.Add("F42", "F49");
            dirFieldMapping.Add("G42", "G49");

            dirFieldMapping.Add("B43", "B50");
            dirFieldMapping.Add("C43", "C50");
            dirFieldMapping.Add("D43", "D50");
            dirFieldMapping.Add("E43", "E50");
            dirFieldMapping.Add("F43", "F50");
            dirFieldMapping.Add("G43", "G50");

            dirFieldMapping.Add("B49", "B70");
            dirFieldMapping.Add("C49", "C70");
            dirFieldMapping.Add("D49", "D70");
            dirFieldMapping.Add("E49", "E70");
            dirFieldMapping.Add("F49", "F70");
            dirFieldMapping.Add("G49", "G70");

            dirFieldMapping.Add("B50", "B71");
            dirFieldMapping.Add("C50", "C71");
            dirFieldMapping.Add("D50", "D71");
            dirFieldMapping.Add("E50", "E71");
            dirFieldMapping.Add("F50", "F71");
            dirFieldMapping.Add("G50", "G71");

            dirFieldMapping.Add("B56", "B75");
            dirFieldMapping.Add("C56", "C75");
            dirFieldMapping.Add("D56", "D75");
            dirFieldMapping.Add("E56", "E75");
            dirFieldMapping.Add("F56", "F75");
            dirFieldMapping.Add("G56", "G75");

            dirFieldMapping.Add("B54", "B76");
            dirFieldMapping.Add("C54", "C76");
            dirFieldMapping.Add("D54", "D76");
            dirFieldMapping.Add("E54", "E76");
            dirFieldMapping.Add("F54", "F76");
            dirFieldMapping.Add("G54", "G76");

            dirFieldMapping.Add("B55", "B77");
            dirFieldMapping.Add("C55", "C77");
            dirFieldMapping.Add("D55", "D77");
            dirFieldMapping.Add("E55", "E77");
            dirFieldMapping.Add("F55", "F77");
            dirFieldMapping.Add("G55", "G77");

            dirFieldMapping.Add("B57", "B79");
            dirFieldMapping.Add("C57", "C79");
            dirFieldMapping.Add("D57", "D79");
            dirFieldMapping.Add("E57", "E79");
            dirFieldMapping.Add("F57", "F79");
            dirFieldMapping.Add("G57", "G79");

            dirFieldMapping.Add("B63", "B86");
            dirFieldMapping.Add("C63", "C86");
            dirFieldMapping.Add("D63", "D86");
            dirFieldMapping.Add("E63", "E86");
            dirFieldMapping.Add("F63", "F86");
            dirFieldMapping.Add("G63", "G86");

            dirFieldMapping.Add("B64", "B88");
            dirFieldMapping.Add("C64", "C88");
            dirFieldMapping.Add("D64", "D88");
            dirFieldMapping.Add("E64", "E88");
            dirFieldMapping.Add("F64", "F88");
            dirFieldMapping.Add("G64", "G88");

            dirFieldMapping.Add("F87", "B158");
            dirFieldMapping.Add("F88", "B159");
            dirFieldMapping.Add("F89", "B160");
            dirFieldMapping.Add("F90", "B163");

            dirFieldMapping.Add("F92", "B165");
            dirFieldMapping.Add("F93", "B166");
            dirFieldMapping.Add("F94", "B167");
            dirFieldMapping.Add("F95", "B168");
            dirFieldMapping.Add("F96", "B169");
            dirFieldMapping.Add("F97", "B170");
            dirFieldMapping.Add("F98", "B171");
            dirFieldMapping.Add("F99", "B172");
            dirFieldMapping.Add("F100", "B173");

            dirFieldMapping.Add("E105", "B188");
            dirFieldMapping.Add("F105", "C188");
            dirFieldMapping.Add("G105", "D188");

            dirFieldMapping.Add("E106", "B189");
            dirFieldMapping.Add("F106", "C189");
            dirFieldMapping.Add("G106", "D189");

            dirFieldMapping.Add("E107", "B190");
            dirFieldMapping.Add("F107", "C190");
            dirFieldMapping.Add("G107", "D190");

            dirFieldMapping.Add("B110", "B193");
            dirFieldMapping.Add("B111", "B195");
            dirFieldMapping.Add("B112", "B197");
            dirFieldMapping.Add("B113", "B199");
            dirFieldMapping.Add("B114", "B201");
            dirFieldMapping.Add("B118", "B203");

            return dirFieldMapping;
        }

    }
}