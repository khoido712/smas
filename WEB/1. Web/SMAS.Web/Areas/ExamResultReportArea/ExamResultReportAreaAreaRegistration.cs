﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamResultReportArea
{
    public class ExamResultReportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamResultReportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamResultReportArea_default",
                "ExamResultReportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
