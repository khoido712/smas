﻿using Ionic.Zip;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.ExamResultReportArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamResultReportArea.Controllers
{
    public class PaperContestController : BaseController
    {

        #region properties
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamPupilBusiness ExamPupilBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        #endregion

        #region Constructor
        public PaperContestController(
            IExaminationsBusiness ExaminationsBusiness,
            IExamGroupBusiness ExamGroupBusiness,
            IExamPupilBusiness ExamPupilBusiness,
            IExamRoomBusiness ExamRoomBusiness,
            IReportDefinitionBusiness ReportDefinitionBusiness,
            IProcessedReportBusiness ProcessedReportBusiness,
            IExamSubjectBusiness ExamSubjectBusiness,
            ISubjectCatBusiness SubjectCatBusiness,
            IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IClassProfileBusiness ClassProfileBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamPupilBusiness = ExamPupilBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.ExamSupervisoryAssignmentBusiness = ExamSupervisoryAssignmentBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
        }
        #endregion
        //
        // GET: /ExamResultReportArea/PaperContest/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        private void SetViewData()
        {
            List<Examinations> lstExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                                        .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                                        .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                                        .OrderByDescending(o => o.CreateTime).ToList();
            ViewData[ExamResultReportConstants.CBO_EXAMINATIONS] = new SelectList(lstExaminations, "ExaminationsID", "ExaminationsName");

            List<EducationLevel> lstEducation = _globalInfo.EducationLevels.ToList();
            ViewData[ExamResultReportConstants.CBO_EDUCATION_LEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");

            List<ClassProfile> lstClass = new List<ClassProfile>();
            ViewData[ExamResultReportConstants.CBO_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
        }

        public List<ExamSubjectBO> GetListExamSubject(long? examinationsID, int? educationLevelID)
        {
            long examinationsIDTemp = Business.Common.Utils.GetLong(examinationsID);
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("ExaminationsID", examinationsIDTemp);
            search.Add("EducationLevelID", educationLevelID);
            return ExamSubjectBusiness.GetListExamSubject(search).ToList();
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            List<SelectListItem> lstClass = new List<SelectListItem>();
            if (educationLevelID <= 0)
                return Json(lstClass);
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);
            dicClass.Add("EducationLevelID", educationLevelID);

            List<ClassProfile> lstClassProflie = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).OrderBy(u => u.DisplayName).ToList();
            lstClass = lstClassProflie.Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false }).ToList();
            return Json(lstClass);
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ReportInfoViewModel form)
        {
            if (!form.ExaminationsID.HasValue)
            {
                throw new BusinessException("Thầy/cô vui lòng chọn Kỳ thi");
            }

            int? educationLevelID = !string.IsNullOrEmpty(form.EducationLevelID2.ToString()) ? int.Parse(form.EducationLevelID2.ToString()) : 0;
            int? classID = !string.IsNullOrEmpty(form.ClassID.ToString()) ? int.Parse(form.ClassID.ToString()) : 0;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            dic["EducationLevelID"] = educationLevelID;
            dic["ClassID"] = classID;

            string reportCode = "";
            if (educationLevelID == 0)
                reportCode = SystemParamsInFile.REPORT_PHIEU_BAO_THI_ZIP;
            else
                reportCode = SystemParamsInFile.REPORT_PHIEU_BAO_THI;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (educationLevelID == 0)
            {
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = ExamPupilBusiness.GetPaperContestReport(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    string FolderSaveFile = string.Empty;
                    ExamPupilBusiness.DownloadZipFilePagerContest(dic, out FolderSaveFile);
                    if (FolderSaveFile == "")
                    {
                        return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                    }
                    DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddDirectory(Path.Combine(FolderSaveFile));
                        using (MemoryStream output = new MemoryStream())
                        {
                            zip.Save(output);
                            di.Delete(true);
                            processedReport = ExamPupilBusiness.InsertPaperContestReport(dic, output, reportCode, true);
                        }
                    }
                }
            }
            else
            {
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = ExamPupilBusiness.GetPaperContestReport(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ExamPupilBusiness.CreatePaperContestReport(dic);
                    processedReport = ExamPupilBusiness.InsertPaperContestReport(dic, excel, reportCode);
                    excel.Close();
                }
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ReportInfoViewModel form)
        {
            if (!form.ExaminationsID.HasValue)
            {
                throw new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExaminationsID"));
            }

            int? educationLevelID = !string.IsNullOrEmpty(form.EducationLevelID2.ToString()) ? int.Parse(form.EducationLevelID2.ToString()) : 0;
            int? classID = !string.IsNullOrEmpty(form.ClassID.ToString()) ? int.Parse(form.ClassID.ToString()) : 0;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            dic["EducationLevelID"] = educationLevelID;
            dic["ClassID"] = classID;

            ProcessedReport processedReport = null;
            if (educationLevelID == 0)
            {
                string FolderSaveFile = string.Empty;
                ExamPupilBusiness.DownloadZipFilePagerContest(dic, out FolderSaveFile);
                if (FolderSaveFile == "")
                {
                    return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                }
                DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(Path.Combine(FolderSaveFile));
                    using (MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        di.Delete(true);
                        processedReport = ExamPupilBusiness.InsertPaperContestReport(dic, output, SystemParamsInFile.REPORT_PHIEU_BAO_THI_ZIP, true);                     
                    }
                }
            }
            else
            {
                Stream excel = ExamPupilBusiness.CreatePaperContestReport(dic);
                processedReport = ExamPupilBusiness.InsertPaperContestReport(dic, excel, SystemParamsInFile.REPORT_PHIEU_BAO_THI);
                excel.Close();
            }

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;

            if (SystemParamsInFile.REPORT_PHIEU_BAO_THI_ZIP.Equals(processedReport.ReportCode))
            {
                excel = new MemoryStream(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/zip");
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/octet-stream");
            }

            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;
            if (SystemParamsInFile.REPORT_PHIEU_BAO_THI_ZIP.Equals(processedReport.ReportCode))
            {
                excel = new MemoryStream(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                //Duong dan thu muc chua file excel
                string inFolder = objFile.CreateFolder();
                //Duong dan thu muc chua file zip
                string outFolder = objFile.CreateFolder();

                string outFile = outFolder + "\\" + processedReport.ReportName;
                try
                {
                    //1. Extract file tu stream ra thu muc
                    objFile.UnzipFromStream(excel, inFolder);
                    //2. Chuyen file .xls thanh pdf
                    objFile.ConvertFileInFolderToPDF(inFolder, 0);
                    //3 Zip thu muc PDF
                    objFile.ZipFolder(outFile, inFolder);
                    //4. Chuyen file zip sang stream
                    result = new FileStreamResult(objFile.ConvertZipFileToStream(outFile), PdfExport.CONTENT_TYPE_ZIP);
                    result.FileDownloadName = processedReport.ReportName;
                }
                finally
                {
                    objFile.DeleteFolder(inFolder);
                    objFile.DeleteFolder(outFolder);
                }
                return result;
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
                return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
            }
        }
    }
}
