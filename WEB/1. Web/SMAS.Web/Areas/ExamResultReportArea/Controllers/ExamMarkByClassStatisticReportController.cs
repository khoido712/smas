﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.ExamResultReportArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamResultReportArea.Controllers
{
    [SkipCheckRole]
    public class ExamMarkByClassStatisticReportController : BaseController
    {
        #region properties
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly IExamInputMarkBusiness ExamInputMarkBusiness;
        private readonly IStatisticLevelReportBusiness StatisticLevelReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        #endregion

        #region Constructor
        public ExamMarkByClassStatisticReportController(IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness, IExamSubjectBusiness ExamSubjectBusiness,
            IExamInputMarkBusiness ExamInputMarkBusiness, IStatisticLevelReportBusiness StatisticLevelReportBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness,
            IClassSubjectBusiness ClassSubjectBusiness,
            ISchoolSubjectBusiness SchoolSubjectBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;
            this.ExamInputMarkBusiness = ExamInputMarkBusiness;
            this.StatisticLevelReportBusiness = StatisticLevelReportBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
        }
        #endregion

        #region Actions
        //
        // GET: /ExamResultReportArea/ExamMarkByClassReport/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        public JsonResult AjaxLoadSubject(long examinationsID)
        {
            //Lay danh sach mon thi cua ky thi
            IDictionary<string, object> dic = new Dictionary<string, object>() { { "ExaminationsID", examinationsID } };
            List<ExamSubjectBO> listExamSubject = ExamSubjectBusiness.GetExamSubject(dic, _globalInfo.AppliedLevel.Value);
            return Json(new SelectList(listExamSubject, "SubjectID", "SubjectName"));
        }
        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ReportInfoViewModel form)
        {
            if (form.ExaminationsID == null)
            {
                throw new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExaminationsID"));
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["SubjectID"] = form._SubjectID;
            dic["StatisticLevelReportID"] = form.StatisticLevelReportID;
            dic["FemalePupil"] = form.FemalePupil;
            dic["EthnicPupil"] = form.EthnicPupil;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;

            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_LOP;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ExamInputMarkBusiness.GetExamMarkByClassStatisticReport(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ExamInputMarkBusiness.CreateExamMarkByClassStatisticReport(dic);
                processedReport = ExamInputMarkBusiness.InsertExamMarkByClassStatisticReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ReportInfoViewModel form)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["SubjectID"] = form._SubjectID;
            dic["StatisticLevelReportID"] = form.StatisticLevelReportID;
            dic["FemalePupil"] = form.FemalePupil;
            dic["EthnicPupil"] = form.EthnicPupil;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            Stream excel = ExamInputMarkBusiness.CreateExamMarkByClassStatisticReport(dic);
            ProcessedReport processedReport = ExamInputMarkBusiness.InsertExamMarkByClassStatisticReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_THONG_KE_DIEMTHI_THEO_LOP,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            List<Examinations> listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                            .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamResultReportConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
            long? defaultExamID = null;
            if (listExaminations.Count > 0) defaultExamID = listExaminations.First().ExaminationsID;

            //Lay danh sach mon thi cua ky thi
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
            if (defaultExamID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>() { { "ExaminationsID", defaultExamID } };
                listExamSubject = ExamSubjectBusiness.GetExamSubject(dic,_globalInfo.AppliedLevel.Value);
            }
            ViewData[ExamResultReportConstants.CBO_EXAM_SUBJECT] = new SelectList(listExamSubject, "SubjectID", "SubjectName");

            //Lay danh sach muc thong ke
            List<StatisticLevelReport> lstStatisticLevel = StatisticLevelReportBusiness.All.Where(o => (o.SchoolID == _globalInfo.SchoolID || o.StatisticLevelReportID == 1) && o.IsActive == true)
                                                                                           .OrderBy(o => o.Resolution).ToList();
            ViewData[ExamResultReportConstants.CBO_STATISTIC_LEVEL] = new SelectList(lstStatisticLevel, "StatisticLevelReportID", "Resolution", 1);

            //Cap
            ViewData[ExamResultReportConstants.APPLIED_LEVEL] = _globalInfo.AppliedLevel;
        }

        #endregion
    }
}