﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ExamResultReportArea.Models;
using System.IO;
using SMAS.Business.Common;
using SMAS.Web.Utils;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Pdf;

namespace SMAS.Web.Areas.ExamResultReportArea.Controllers
{
    [SkipCheckRole]
    public class ExamPupilByRoomReportController : BaseController
    {
        #region properties
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamPupilBusiness ExamPupilBusiness ;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;

        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        private List<ExamRoom> listExamRoom;
        private List<ClassProfile> listClassProfile;
        #endregion

        #region Constructor
        public ExamPupilByRoomReportController(IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness, IExamPupilBusiness ExamPupilBusiness,
            IExamRoomBusiness ExamRoomBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness,
            IClassProfileBusiness ClassProfileBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamPupilBusiness = ExamPupilBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
        }
        #endregion

        #region Actions
        //
        // GET: /ExamResultReportArea/ExamPupilByRoom/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        [HttpPost]
        public JsonResult AjaxLoadExamGroup(long examinationsID)
        {
            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
                                        .OrderBy(o => o.ExamGroupCode).ToList();

            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }
        [HttpPost]
        public JsonResult AjaxLoadExamRoom(long examinationsID, long? examGroupID)
        {
            if (examGroupID == null)
            {
                return Json(new SelectList(new List<ExamRoom>(), "ExamRoomID", "ExamRoomCode"));
            }

            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                  .Where(o => o.ExamGroupID == examGroupID)
                                                  .OrderBy(o => o.ExamRoomCode)
                                                  .ToList();

            return Json(new SelectList(listExamRoom, "ExamRoomID", "ExamRoomCode"));
        }
        [HttpPost]
        public JsonResult AjaxLoadClass(int? educationLevelID, long? examinationsID, long? examGroupID)
        {
            if (examGroupID == null || educationLevelID == null)
            {
                return Json(new SelectList(new List<ClassProfile>(), "ClassProfileID", "DisplayName"));
            }

            int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = examinationsID;
            dic["ExamGroupID"] = examGroupID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["EducationLevelID"] = educationLevelID;

            List<ExamPupilBO> lstExamPupil = ExamPupilBusiness.Search(dic, _globalInfo.AcademicYearID.Value, partition).ToList();
            List<int> lstClassID = lstExamPupil.Select(o => o.ClassID).Distinct().ToList();

            //Lay danh sach lop thuoc khoi
            dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["EducationLevelID"] = educationLevelID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;

            List<ClassProfile> listClassProfile = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic).ToList();
            listClassProfile = listClassProfile.Where(o => lstClassID.Contains(o.ClassProfileID)).ToList();

            return Json(new SelectList(listClassProfile, "ClassProfileID", "DisplayName"));
        }

        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ReportInfoViewModel form, int reportType)
        {
            if (form.ExaminationsID == null)
            {
                throw new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExaminationsID"));
            }
            if (form.ExamGroupID == null)
            {
                throw new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExamGroupID"));
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel.Value;
            if (reportType == 1)
            {
                dic["ExamRoomID"] = form.ExamRoomID;

                string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_THEO_PHONG_THI;

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                string type = JsonReportMessage.NEW;
                ProcessedReport processedReport = null;

                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = ExamPupilBusiness.GetExamPupilByRoomReport(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ExamPupilBusiness.CreateExamPupilByRoomReport(dic);
                    processedReport = ExamPupilBusiness.InsertExamPupilByRoomReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                    excel.Close();
                }
                return Json(new JsonReportMessage(processedReport, type));
            }
            else
            {
                dic["EducationLevelID"] = form.EducationLevelID;
                dic["ClassID"] = form.ClassID;

                string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_THEO_LOP;

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                string type = JsonReportMessage.NEW;
                ProcessedReport processedReport = null;

                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = ExamPupilBusiness.GetExamPupilByClassReport(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ExamPupilBusiness.CreateExamPupilByClassReport(dic);
                    processedReport = ExamPupilBusiness.InsertExamPupilByClassReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                    excel.Close();
                }
                return Json(new JsonReportMessage(processedReport, type));
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ReportInfoViewModel form, int reportType)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            if (reportType == 1)
            {
                dic["ExamRoomID"] = form.ExamRoomID;

                Stream excel = ExamPupilBusiness.CreateExamPupilByRoomReport(dic);
                ProcessedReport processedReport = ExamPupilBusiness.InsertExamPupilByRoomReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();


                return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
            }
            else
            {
                dic["EducationLevelID"] = form.EducationLevelID;
                dic["ClassID"] = form.ClassID;

                Stream excel = ExamPupilBusiness.CreateExamPupilByClassReport(dic);
                ProcessedReport processedReport = ExamPupilBusiness.InsertExamPupilByClassReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();


                return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
            }
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_THEO_PHONG_THI,
                SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_THEO_LOP
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }

        #endregion

        #region Private methods
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                            .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamResultReportConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
            long? defaultExamID = null;
            if (listExaminations.Count > 0) defaultExamID = listExaminations.First().ExaminationsID;

            //Lay danh sach nhom thi
            if (defaultExamID != null)
            {
                listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID.Value)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
            }
            else
            {
                listExamGroup = new List<ExamGroup>();
            }
            ViewData[ExamResultReportConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0) defaultExamGroupID = listExamGroup.First().ExamGroupID;

            //Lay danh sach phong thi
            if (defaultExamGroupID != null)
            {
                listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == defaultExamID.Value)
                                                 .Where(o => o.ExamGroupID == defaultExamGroupID.Value)
                                                 .OrderBy(o => o.ExamRoomCode)
                                                 .ToList();
            }
            else
            {
                listExamRoom = new List<ExamRoom>();
            }
            ViewData[ExamResultReportConstants.CBO_EXAM_ROOM] = new SelectList(listExamRoom, "ExamRoomID", "ExamRoomCode");

            //Lay danh sach khoi
            ViewData[ExamResultReportConstants.CBO_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");

            //Lay danh sach lop
            int? defaultEducationLevel = null;
            if (_globalInfo.EducationLevels.Count > 0)
            {
                defaultEducationLevel = _globalInfo.EducationLevels.First().EducationLevelID;
            }
            if (defaultEducationLevel != null && defaultExamGroupID!=null)
            {
                int partition = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                IDictionary<string,object> dic=new Dictionary<string,object>();
                dic["ExaminationsID"] = defaultExamID;
                dic["ExamGroupID"] = defaultExamGroupID;
                dic["SchoolID"] = _globalInfo.SchoolID;
                dic["EducationLevelID"] = defaultEducationLevel;

                List<ExamPupilBO> lstExamPupil = ExamPupilBusiness.Search(dic, _globalInfo.AcademicYearID.Value, partition).ToList();
                List<int> lstClassID=lstExamPupil.Select(o=>o.ClassID).Distinct().ToList();

                //Lay danh sach lop thuoc khoi
                dic = new Dictionary<string, object>();
                dic["AppliedLevel"] = _globalInfo.AppliedLevel;
                dic["EducationLevelID"] = defaultEducationLevel;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                dic["SchoolID"] = _globalInfo.SchoolID;

                listClassProfile = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic).ToList() ;
                listClassProfile = listClassProfile.Where(o => lstClassID.Contains(o.ClassProfileID)).ToList();
            }
            else
            {
                listClassProfile = new List<ClassProfile>();
            }
            ViewData[ExamResultReportConstants.CBO_CLASS] = new SelectList(listClassProfile, "ClassProfileID", "DisplayName");


        }

        #endregion
    }
}