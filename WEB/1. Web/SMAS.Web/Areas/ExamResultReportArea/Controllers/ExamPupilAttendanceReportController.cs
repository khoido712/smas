﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.ExamResultReportArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamResultReportArea.Controllers
{
    [SkipCheckRole]
    public class ExamPupilAttendanceReportController:BaseController
    {
        #region properties
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamPupilBusiness ExamPupilBusiness ;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        private List<ExamRoom> listExamRoom;
        #endregion

        #region Constructor
        public ExamPupilAttendanceReportController(IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness, IExamPupilBusiness ExamPupilBusiness,
            IExamRoomBusiness ExamRoomBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamPupilBusiness = ExamPupilBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
        }
        #endregion

        #region Actions
        //
        // GET: /ExamResultReportArea/ExamPupilAttendanceReport/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        public JsonResult AjaxLoadExamGroup(long examinationsID)
        {
            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
                                        .OrderBy(o => o.ExamGroupCode).ToList();

            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        public JsonResult AjaxLoadExamRoom(long examinationsID, long? examGroupID)
        {
            if (examGroupID == null)
            {
                return Json(new SelectList(new List<ExamRoom>(), "ExamRoomID", "ExamRoomCode"));
            }

            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                  .Where(o => o.ExamGroupID == examGroupID)
                                                  .OrderBy(o => o.ExamRoomCode)
                                                  .ToList();

            return Json(new SelectList(listExamRoom, "ExamRoomID", "ExamRoomCode"));
        }
        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ReportInfoViewModel form)
        {
            if (form.ExaminationsID == null)
            {
                throw new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExaminationsID"));
            }
            if (form.ExamGroupID == null)
            {
                throw new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExamGroupID"));
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["ExamRoomID"] = form.ExamRoomID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEM_DANH;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ExamPupilBusiness.GetExamPupilAttendanceReport(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ExamPupilBusiness.CreateExamPupilAttendanceReport(dic);
                processedReport = ExamPupilBusiness.InsertExamPupilAttendanceReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ReportInfoViewModel form)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["ExamRoomID"] = form.ExamRoomID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            Stream excel = ExamPupilBusiness.CreateExamPupilAttendanceReport(dic);
            ProcessedReport processedReport = ExamPupilBusiness.InsertExamPupilAttendanceReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_DANH_SACH_DIEM_DANH,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                            .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamResultReportConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
            long? defaultExamID = null;
            if (listExaminations.Count > 0) defaultExamID = listExaminations.First().ExaminationsID;

            //Lay danh sach nhom thi
            if (defaultExamID != null)
            {
                listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID.Value)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
            }
            else
            {
                listExamGroup = new List<ExamGroup>();
            }
            ViewData[ExamResultReportConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0) defaultExamGroupID = listExamGroup.First().ExamGroupID;

            //Lay danh sach phong thi
            if (defaultExamGroupID != null)
            {
                listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == defaultExamID.Value)
                                                 .Where(o => o.ExamGroupID == defaultExamGroupID.Value)
                                                 .OrderBy(o => o.ExamRoomCode)
                                                 .ToList();
            }
            else
            {
                listExamRoom = new List<ExamRoom>();
            }
            ViewData[ExamResultReportConstants.CBO_EXAM_ROOM] = new SelectList(listExamRoom, "ExamRoomID", "ExamRoomCode");
        }

        #endregion
    }
}