﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.ExamResultReportArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamResultReportArea.Controllers
{
    [SkipCheckRole]
    public class ExamMarkByClassReportController : BaseController
    {
        #region properties
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamInputMarkBusiness ExamInputMarkBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness; 

        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        #endregion

        #region Constructor
        public ExamMarkByClassReportController(IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness, IExamInputMarkBusiness ExamInputMarkBusiness,
            IExamRoomBusiness ExamRoomBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness,
            IClassProfileBusiness ClassProfileBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamInputMarkBusiness = ExamInputMarkBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
        }
        #endregion

        #region Actions
        //
        // GET: /ExamResultReportArea/ExamMarkByClassReport/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        //public JsonResult AjaxLoadExamGroup(long examinationsID)
        //{
        //    List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
        //                                .OrderBy(o => o.ExamGroupCode).ToList();

        //    return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        //}
        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ReportInfoViewModel form)
        {
            if (form.ExaminationsID == null)
            {
                throw new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExaminationsID"));
            }
            //if (form.ExamGroupID == null)
            //{
            //    throw new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExamGroupID"));
            //}

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID == null ? 0 : form.ExamGroupID;
            dic["EducationLevelID"] = SMAS.Business.Common.Utils.GetInt(form.EducationLevelIDVM);
            dic["ClassID"] = SMAS.Business.Common.Utils.GetInt(form.ClassIDVM);
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            dic["checkSumMark"] = form.checkSumMark;
            dic["checkClassLevel"] = form.checkClassLevel;
            dic["checkSchoolLevel"] = form.checkSchoolLevel;

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEMTHI_THEO_LOP;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ExamInputMarkBusiness.GetExamMarkByClassReport(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                //if (form.ExamGroupID == null)
                //{
                    Stream excel = ExamInputMarkBusiness.CreateExamMarkByClassReportAll(dic);
                    processedReport = ExamInputMarkBusiness.InsertExamMarkByClassReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                    excel.Close();
                //}
                //else
                //{
                //    Stream excel = ExamInputMarkBusiness.CreateExamMarkByClassReport(dic);
                //    processedReport = ExamInputMarkBusiness.InsertExamMarkByClassReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                //    excel.Close();
                //}
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ReportInfoViewModel form)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID == null ? 0 : form.ExamGroupID;
            dic["EducationLevelID"] = SMAS.Business.Common.Utils.GetInt(form.EducationLevelIDVM);
            dic["ClassID"] = SMAS.Business.Common.Utils.GetInt(form.ClassIDVM);
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            dic["checkSumMark"] = form.checkSumMark;
            dic["checkClassLevel"] = form.checkClassLevel;
            dic["checkSchoolLevel"] = form.checkSchoolLevel;
            //if (form.ExamGroupID == null)
            //{
                Stream excel = ExamInputMarkBusiness.CreateExamMarkByClassReportAll(dic);
                ProcessedReport processedReport = ExamInputMarkBusiness.InsertExamMarkByClassReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();
                return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
            //}
            //else
            //{
            //    Stream excel = ExamInputMarkBusiness.CreateExamMarkByClassReport(dic);
            //    ProcessedReport processedReport = ExamInputMarkBusiness.InsertExamMarkByClassReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
            //    excel.Close();
            //    return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
            //}
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_DANH_SACH_DIEMTHI_THEO_LOP,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                            .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamResultReportConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
            long? defaultExamID = null;
            if (listExaminations.Count > 0) defaultExamID = listExaminations.First().ExaminationsID;

            //Lấy danh sách Khối
            var lstEducation = _globalInfo.EducationLevels.ToList();
            ViewData[ExamResultReportConstants.CBO_EDUCATION_LEVEL] = new SelectList(lstEducation, "EducationLevelID", "Resolution");

            //Lấy danh sách Lớp
            var lstClass = new List<ClassProfile>();
            ViewData[ExamResultReportConstants.CBO_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");

            //Lay danh sach nhom thi
            //if (defaultExamID != null)
            //{
            //    listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID.Value)
            //                                            .OrderBy(o => o.ExamGroupCode).ToList();
            //}
            //else
            //{
            //    listExamGroup = new List<ExamGroup>();
            //}
            //ViewData[ExamResultReportConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");
        }

        public ActionResult LoadClass(int educationLevelVM)
        {
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            List<SelectListItem> lstClass = new List<SelectListItem>();
            dicClass.Add("AcademicYearID", _globalInfo.AcademicYearID);
            dicClass.Add("AppliedLevel", _globalInfo.AppliedLevel);

            if (educationLevelVM != 0)
            {
                dicClass.Add("EducationLevelID", educationLevelVM);

                lstClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicClass).Where(x => x.EducationLevelID >= 1 && x.EducationLevelID <= 12).OrderBy(u => u.DisplayName).ToList()
                                                        .Select(u => new SelectListItem { Value = u.ClassProfileID.ToString(), Text = u.DisplayName, Selected = false })
                                                        .ToList();
            }

            return Json(lstClass);
        }

        #endregion
    }
}