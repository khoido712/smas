﻿using Ionic.Zip;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.ExamResultReportArea.Models;
using SMAS.Web.Areas.ExamSubjectArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamResultReportArea.Controllers
{
    public class TicketTakeExamPaperController : BaseController
    {
        #region properties
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamPupilBusiness ExamPupilBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IExamSubjectBusiness ExamSubjectBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;

        private List<Examinations> listExaminations;
        private List<ExamGroup> listExamGroup;
        private List<ExamRoom> listExamRoom;
        private List<ExamSubjectBO> listExamSubject;
        #endregion

        #region Constructor
        public TicketTakeExamPaperController(IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness, IExamPupilBusiness ExamPupilBusiness,
            IExamRoomBusiness ExamRoomBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness, IExamSubjectBusiness ExamSubjectBusiness, ISubjectCatBusiness SubjectCatBusiness,
            IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamPupilBusiness = ExamPupilBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.ExamSubjectBusiness = ExamSubjectBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.ExamSupervisoryAssignmentBusiness = ExamSupervisoryAssignmentBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
        }
        #endregion

        //
        // GET: /ExamResultReportArea/TicketTakeExamPaper/

        #region Actions
        //
        // GET: /ExamResultReportArea/ExamPupilAttendanceReport/
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        public JsonResult AjaxLoadExamGroup(long examinationsID)
        {
            List<ExamGroup> listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(examinationsID)
                                        .OrderBy(o => o.ExamGroupCode).ToList();

            return Json(new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName"));
        }

        public JsonResult AjaxLoadExamRoom(long examinationsID, long? examGroupID)
        {
            if (examGroupID == null)
            {
                return Json(new SelectList(new List<ExamRoom>(), "ExamRoomID", "ExamRoomCode"));
            }

            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                  .Where(o => o.ExamGroupID == examGroupID)
                                                  .OrderBy(o => o.ExamRoomCode)
                                                  .ToList();

            return Json(new SelectList(listExamRoom, "ExamRoomID", "ExamRoomCode"));
        }
        
        public JsonResult AjaxLoadExamSubject(long? examinationsID, long? examGroupID)
        {
            if (examGroupID == null)
            {
                return Json(new SelectList(new List<ExamSubjectBO>(), "SubjectID", "SubjectName"));
            }

            List<ExamSubjectBO> listExamSubject = GetListExamSubject(examinationsID, examGroupID, 0);

            return Json(new SelectList(listExamSubject, "SubjectID", "SubjectName"));
        }
        #endregion

        public List<ExamSubjectBO> GetListExamSubject(long? examinationsID, long? examinationGroupID, int subjectID) 
        {
            long examinationsIDTemp = Business.Common.Utils.GetLong(examinationsID);
            long examinationGroupIDTemp = Business.Common.Utils.GetLong(examinationGroupID);
            int subjectIDTemp = Business.Common.Utils.GetInt(subjectID);
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("ExaminationsID", examinationsIDTemp);
            search.Add("ExamGroupID", examinationGroupIDTemp);
            search.Add("SubjectID", subjectIDTemp);
            return ExamSubjectBusiness.GetListExamSubject(search).ToList();
        }

        #region Private methods
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                            .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamResultReportConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
            long? defaultExamID = null;
            if (listExaminations.Count > 0) defaultExamID = listExaminations.First().ExaminationsID;

            //Lay danh sach nhom thi
            if (defaultExamID != null)
            {
                listExamGroup = ExamGroupBusiness.GetListExamGroupByExaminationsID(defaultExamID.Value)
                                                        .OrderBy(o => o.ExamGroupCode).ToList();
            }
            else
            {
                listExamGroup = new List<ExamGroup>();
            }
            ViewData[ExamResultReportConstants.CBO_EXAM_GROUP] = new SelectList(listExamGroup, "ExamGroupID", "ExamGroupName");

            long? defaultExamGroupID = null;
            if (listExamGroup.Count > 0) 
                defaultExamGroupID = listExamGroup.First().ExamGroupID;
           
            if (defaultExamGroupID != null)
            {
                listExamSubject = GetListExamSubject(defaultExamID, defaultExamGroupID, 0);
            }
            else
            {
                listExamSubject = new List<ExamSubjectBO>();
            }
            ViewData[ExamResultReportConstants.CBO_EXAM_SUBJECT] = new SelectList(listExamSubject, "SubjectID", "SubjectName");
        }

        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ReportInfoViewModel form)
        {

            int subjectID = !string.IsNullOrEmpty(form.SubjectIDCustom.ToString()) ? int.Parse(form.SubjectIDCustom.ToString()) : 0;
            if (form.ExaminationsID == null)
            {
                //return Json( new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExaminationsID")));
                throw new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExaminationsID"));
               
            }
            if (form.ExamGroupID == null)
            {
                throw new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExamGroupID"));
                
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            dic["SubjectID"] = subjectID;

            // Lấy danh sách môn học
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
            if (subjectID == 0)
            {
                listExamSubject = GetListExamSubject(form.ExaminationsID, form.ExamGroupID, 0);          
            }

            SubjectCat SubjectName = SubjectCatBusiness.Find(subjectID);

            string reportCode = "";
            if (subjectID == 0)
                reportCode = SystemParamsInFile.REPORT_PHIEU_THU_BAI_THI_ZIP;
            else
                reportCode = SystemParamsInFile.REPORT_PHIEU_THU_BAI_THI;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (subjectID == 0)
            {
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = ExamPupilBusiness.GetTicketTakeExamPaperReport(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    if (subjectID == 0)
                    {
                        string FolderSaveFile = string.Empty;
                        DownloadZipFileTranscript(dic, out FolderSaveFile, listExamSubject);
                        if (FolderSaveFile == "")
                        {
                            return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                        }
                        DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                        using (ZipFile zip = new ZipFile())
                        {
                            zip.AddDirectory(Path.Combine(FolderSaveFile));
                            using (MemoryStream output = new MemoryStream())
                            {
                                zip.Save(output);
                                di.Delete(true);
                                processedReport = ExamPupilBusiness.InsertTicketTakeExamPaperReportZip(dic, output, _globalInfo.AppliedLevel.GetValueOrDefault(), SubjectName != null ? SubjectName.DisplayName : "");
                            }
                        }
                    }
                }
            }
            else
            {
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = ExamPupilBusiness.GetTicketTakeExamPaperReport(dic);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }

                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = CreateTicketTakeExamPaperReport(dic, subjectID);
                    processedReport = ExamPupilBusiness.InsertTicketTakeExamPaperReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault(), SubjectName != null ? SubjectName.DisplayName : "");
                    excel.Close();
                }
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ReportInfoViewModel form)
        {
            int subjectID = !string.IsNullOrEmpty(form.SubjectIDCustom.ToString()) ? int.Parse(form.SubjectIDCustom.ToString()) : 0;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel.Value;
            dic["SubjectID"] = subjectID;

            //Lấy danh sách môn học
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
            if (subjectID == 0) {
                listExamSubject = GetListExamSubject(form.ExaminationsID, form.ExamGroupID, 0); 
            }

            SubjectCat SubjectName = SubjectCatBusiness.Find(subjectID);
            ProcessedReport processedReport = null;
            if (subjectID == 0)
            {
                string FolderSaveFile = string.Empty;
                DownloadZipFileTranscript(dic, out FolderSaveFile, listExamSubject);
                if (FolderSaveFile == "")
                {
                    return Json(new { Type = "error", Message = "Không có dữ liệu học sinh" });
                }
                DirectoryInfo di = new DirectoryInfo(FolderSaveFile);
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddDirectory(Path.Combine(FolderSaveFile));
                    using(MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        di.Delete(true);
                        processedReport = ExamPupilBusiness.InsertTicketTakeExamPaperReportZip(dic, output, _globalInfo.AppliedLevel.GetValueOrDefault(), SubjectName != null ? SubjectName.DisplayName : "");
                    }
                }
            }
            else
            {            
                Stream excel = CreateTicketTakeExamPaperReport(dic, subjectID);
                processedReport = ExamPupilBusiness.InsertTicketTakeExamPaperReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault(), SubjectName != null ? SubjectName.DisplayName : "");
                excel.Close();
            }

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;

            if (SystemParamsInFile.REPORT_PHIEU_THU_BAI_THI_ZIP.Equals(processedReport.ReportCode))
            {
                excel = new MemoryStream(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/zip");
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                result = new FileStreamResult(excel, "application/octet-stream");
            }

            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = null;
            FileStreamResult result = null;
            if (SystemParamsInFile.REPORT_PHIEU_THU_BAI_THI_ZIP.Equals(processedReport.ReportCode))
            {
                excel = new MemoryStream(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                //Duong dan thu muc chua file excel
                string inFolder = objFile.CreateFolder();
                //Duong dan thu muc chua file zip
                string outFolder = objFile.CreateFolder();

                string outFile = outFolder + "\\" + processedReport.ReportName;
                try
                {
                    //1. Extract file tu stream ra thu muc
                    objFile.UnzipFromStream(excel, inFolder);
                    //2. Chuyen file .xls thanh pdf
                    objFile.ConvertFileInFolderToPDF(inFolder, 0);
                    //3 Zip thu muc PDF
                    objFile.ZipFolder(outFile, inFolder);
                    //4. Chuyen file zip sang stream
                    result = new FileStreamResult(objFile.ConvertZipFileToStream(outFile), PdfExport.CONTENT_TYPE_ZIP);
                    result.FileDownloadName = processedReport.ReportName;
                }
                finally
                {
                    objFile.DeleteFolder(inFolder);
                    objFile.DeleteFolder(outFolder);
                }
                return result;
            }
            else
            {
                excel = ReportUtils.Decompress(processedReport.ReportData);
                IPdfExport objFile = new PdfExport();
                var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
                return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
            }
        }



        
        public Stream CreateTicketTakeExamPaperReport(IDictionary<string, object> dic, int subjectID)
        {
            int academicYearID = Business.Common.Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Business.Common.Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Business.Common.Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Business.Common.Utils.GetLong(dic["ExamGroupID"]);
            //long examRoomID = Utils.GetLong(dic["ExamRoomID"]);
            int appliedLevelID = Business.Common.Utils.GetInt(dic["AppliedLevelID"]);
            
            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_PHIEU_THU_BAI_THI;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet đầu tiên
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            //IVTWorksheet secondSheet = oBook.GetSheet(2);

            //Dien thong tin chung vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);
            SubjectCat examSubject = SubjectCatBusiness.Find(subjectID);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "NĂM HỌC: " + academicYear.DisplayTitle;
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;

            //Dien du lieu vao phan tieu de
            firstSheet.SetCellValue("A2", examinationsName);
            firstSheet.SetCellValue("A3", strAcademicYear);
            //firstSheet.SetCellValue("A3", schoolName);

            firstSheet.SetCellValue("I4", provinceAndDate);

            //Lay danh sach phong thi cua nhom thi
            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                 .Where(o => o.ExamGroupID == examGroupID)
                                                 .OrderBy(o => o.ExamRoomCode)
                                                 .ToList();
            //Lay danh sach thi sinh cua nhom thi
            List<ExamPupilBO> listExamPupil = new List<ExamPupilBO>();

            if (examinationsID != 0 && examGroupID != 0)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = schoolID;
                SearchInfo["ExaminationsID"] = examinationsID;
                SearchInfo["ExamGroupID"] = examGroupID;

                listExamPupil = ExamPupilBusiness.GetExamPupilWithSubject(examinationsID, schoolID, academicYearID, partition).Where(x => x.ExamGroupID == examGroupID).ToList();
            }

            //Lay danh sach mon thi cua nhom thi
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
            if (examinationsID != 0 && examGroupID != 0)
            {
                IDictionary<string, object> tmpDic = new Dictionary<string, object>();
                dic["ExaminationsID"] = examinationsID;
                dic["ExamGroupID"] = examGroupID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).ToList();
            }

            //Lấy danh sách giám thị được phân công
            IDictionary<string, object> Info = new Dictionary<string, object>();
            List<ExamSupervisoryViewModel> lstExamSuper = _Search(examinationsID, examGroupID, subjectID, 0).ToList();

            //So mon thi
            int subjectNum = listExamSubject.Count;

            //Tao tung sheet cho moi phong thi
            for (int i = 0; i < listExamRoom.Count; i++)
            {
                ExamRoom examRoom = listExamRoom[i];

                //Fill du lieu
                //Danh sach thi sinh cua phong thi
                List<ExamPupilBO> listResult = listExamPupil.Where(o => o.ExamRoomID == examRoom.ExamRoomID && o.SubjectID == subjectID)
                                                             .OrderBy(o => o.ExamineeNumber).ToList();

                int startRow = 8;
                int lastRow = startRow + listResult.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 7 + subjectNum;
                int curColumn = startColumn;

                //Tao sheet cho phong
                IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "J" + (lastRow).ToString());
                //IVTWorksheet infoExam = oBook
                IVTRange rang = firstSheet.GetRange("B115", "H117");
                IVTRange supervisory1 = firstSheet.GetRange("A119", "D119");
                IVTRange supervisory2 = firstSheet.GetRange("E119", "J119");
                IVTRange supervisoryName1 = firstSheet.GetRange("A123", "D123");
                IVTRange supervisoryName2 = firstSheet.GetRange("E123", "J123");

                //Fill tieu de Môn thi: TOÁN - Phòng thi: P01
                curSheet.Name = examRoom.ExamRoomCode;
                curSheet.SetCellValue("A5", "Môn thi: " + examSubject.DisplayName + " - Phòng thi: " + examRoom.ExamRoomCode);

                for (int j = 0; j < listResult.Count; j++)
                {
                    ExamPupilBO obj = listResult[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //SBD
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                    curColumn++;
                    //Ho va ten
                    curSheet.SetCellValue(curRow, curColumn, obj.PupilFullName);
                    curColumn++;
                    //Ngay sinh
                    curSheet.SetCellValue(curRow, curColumn, ("'" + obj.BirthDate.ToString("dd/MM/yyyy")));
                    curColumn++;
                    //Gioi tinh
                    curSheet.SetCellValue(curRow, curColumn, obj.DisplayGenre);
                    curColumn++;
                    //Lop
                    curSheet.SetCellValue(curRow, curColumn, obj.ClassName);
                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, 10);

                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        if (j == listResult.Count - 1)
                        {
                            style = VTBorderStyle.Solid;
                            weight = VTBorderWeight.Medium;
                        }
                        else
                        {
                            style = VTBorderStyle.Dotted;
                            weight = VTBorderWeight.Thin;
                        }
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Medium;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);
                    curRow++;
                    curColumn = startColumn;
                }

                curSheet.CopyPasteSameSize(rang, curRow + 1, 2);

                List<ExamSupervisoryViewModel> lstExamSuperTemp = lstExamSuper.Where(x => x.ExamRoomID == examRoom.ExamRoomID && x.SubjectID == subjectID).ToList();
                if (lstExamSuperTemp.Count == 0)
                {
                    curSheet.CopyPasteSameSize(supervisory2, curRow + 5, 5);
                    curSheet.SetCellValue(curRow + 5, 5, "Giám thị 1");
                }
                else if (lstExamSuperTemp.Count == 1)
                {
                    curSheet.CopyPasteSameSize(supervisory2, curRow + 5, 5);
                    curSheet.SetCellValue(curRow + 5, 5, "Giám thị 1");
                    curSheet.CopyPasteSameSize(supervisoryName2, curRow + 9, 5);
                    curSheet.SetCellValue(curRow + 9, 5, lstExamSuperTemp.First().EmployeeFullName);
                }
                else if (lstExamSuperTemp.Count >= 2)
                {
                    curSheet.CopyPasteSameSize(supervisory1, curRow + 5, 1);
                    curSheet.CopyPasteSameSize(supervisory2, curRow + 5, 5);
                    curSheet.CopyPasteSameSize(supervisoryName1, curRow + 9, 1);
                    curSheet.CopyPasteSameSize(supervisoryName2, curRow + 9, 5);
                    curSheet.SetCellValue(curRow + 9, 1, lstExamSuperTemp[0].EmployeeFullName);
                    curSheet.SetCellValue(curRow + 9, 5, lstExamSuperTemp[1].EmployeeFullName);
                }

                
                curSheet.FitAllColumnsOnOnePage = true;
            }


            //Xoa sheet dau tien
            firstSheet.Delete();
                
            //templateSheet.Delete();

            return oBook.ToStream();
        }

        public Stream CreateTicketTakeExamPaperReportZip(IDictionary<string, object> dic, 
            List<ExamRoom> listExamRoom,
            List<ExamPupilBO> lstExamPupil,
            List<ExamSubjectBO> lstExamSubject,
            List<ExamSupervisoryViewModel> lstExamSuper,
            Examinations exam, 
            ExamGroup examGroup,
            AcademicYear academicYear,
            int subjectID, 
            IVTWorkbook oBook)
        {
            int academicYearID = Business.Common.Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Business.Common.Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Business.Common.Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Business.Common.Utils.GetLong(dic["ExamGroupID"]);
            int appliedLevelID = Business.Common.Utils.GetInt(dic["AppliedLevelID"]);

            //Lấy ra sheet đầu tiên           
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Dien thong tin chung vao tieu de bao cao      
            SubjectCat examSubject = SubjectCatBusiness.Find(subjectID);

            string examinationsName = exam.ExaminationsName.ToUpper();
            string strAcademicYear = "NĂM HỌC: " + academicYear.DisplayTitle;

            //Dien du lieu vao phan tieu de
            firstSheet.SetCellValue("A2", examinationsName);
            firstSheet.SetCellValue("A3", strAcademicYear);

            //Lay danh sach thi sinh cua nhom thi
            List<ExamPupilBO> listExamPupil = new List<ExamPupilBO>();
            if (examinationsID != 0 && examGroupID != 0)
                listExamPupil = lstExamPupil.ToList();

            //Lay danh sach mon thi cua nhom thi
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
            if (examinationsID != 0 && examGroupID != 0)
                listExamSubject = lstExamSubject.ToList();
            
            //So mon thi
            int subjectNum = listExamSubject.Count;

            //Tao tung sheet cho moi phong thi
            for (int i = 0; i < listExamRoom.Count; i++)
            {
                ExamRoom examRoom = listExamRoom[i];

                //Fill du lieu
                //Danh sach thi sinh cua phong thi
                List<ExamPupilBO> listResult = listExamPupil.Where(o => o.ExamRoomID == examRoom.ExamRoomID && o.SubjectID == subjectID)
                                                             .OrderBy(o => o.ExamineeNumber).ToList();

                int startRow = 8;
                int lastRow = startRow + listResult.Count - 1;
                int curRow = startRow;
                int startColumn = 1;
                int lastColumn = 7 + subjectNum;
                int curColumn = startColumn;

                //Tao sheet cho phong
                IVTWorksheet curSheet = oBook.CopySheetToLast(firstSheet, "J" + (lastRow).ToString());
                //IVTWorksheet infoExam = oBook
                IVTRange rang = firstSheet.GetRange("B115", "H117");
                IVTRange supervisory1 = firstSheet.GetRange("A119", "D119");
                IVTRange supervisory2 = firstSheet.GetRange("E119", "J119");
                IVTRange supervisoryName1 = firstSheet.GetRange("A123", "D123");
                IVTRange supervisoryName2 = firstSheet.GetRange("E123", "J123");

                //Fill tieu de Môn thi: TOÁN - Phòng thi: P01
                curSheet.Name = examRoom.ExamRoomCode;
                curSheet.SetCellValue("A5", "Môn thi: " + examSubject.DisplayName + " - Phòng thi: " + examRoom.ExamRoomCode);

                for (int j = 0; j < listResult.Count; j++)
                {
                    ExamPupilBO obj = listResult[j];
                    //STT
                    curSheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;
                    //SBD
                    curSheet.SetCellValue(curRow, curColumn, obj.ExamineeNumber);
                    curColumn++;
                    //Ho va ten
                    curSheet.SetCellValue(curRow, curColumn, obj.PupilFullName);
                    curColumn++;
                    //Ngay sinh
                    curSheet.SetCellValue(curRow, curColumn, ("'" + obj.BirthDate.ToString("dd/MM/yyyy")));
                    curColumn++;
                    //Gioi tinh
                    curSheet.SetCellValue(curRow, curColumn, obj.DisplayGenre);
                    curColumn++;
                    //Lop
                    curSheet.SetCellValue(curRow, curColumn, obj.ClassName);
                    curColumn++;

                    //Ve khung cho dong
                    IVTRange range = curSheet.GetRange(curRow, startColumn, curRow, 10);
                    VTBorderStyle style;
                    VTBorderWeight weight;
                    if ((curRow - startRow + 1) % 5 != 0)
                    {
                        if (j == listResult.Count - 1)
                        {
                            style = VTBorderStyle.Solid;
                            weight = VTBorderWeight.Medium;
                        }
                        else
                        {
                            style = VTBorderStyle.Dotted;
                            weight = VTBorderWeight.Thin;
                        }
                    }
                    else
                    {
                        style = VTBorderStyle.Solid;
                        weight = VTBorderWeight.Medium;
                    }
                    range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);
                    curRow++;
                    curColumn = startColumn;
                }

                curSheet.CopyPasteSameSize(rang, curRow + 1, 2);

                List<ExamSupervisoryViewModel> lstExamSuperTemp = lstExamSuper.Where(x => x.ExamRoomID == examRoom.ExamRoomID && x.SubjectID == subjectID).ToList();
                if (lstExamSuperTemp.Count == 0) {
                    curSheet.CopyPasteSameSize(supervisory2, curRow + 5, 5);
                    curSheet.SetCellValue(curRow + 5, 5, "Giám thị 1");
                }
                else if (lstExamSuperTemp.Count == 1)
                {
                    curSheet.CopyPasteSameSize(supervisory2, curRow + 5, 5);
                    curSheet.SetCellValue(curRow + 5, 5, "Giám thị 1");
                    curSheet.CopyPasteSameSize(supervisoryName2, curRow + 9, 5);
                    curSheet.SetCellValue(curRow + 9, 5, lstExamSuperTemp.First().EmployeeFullName);
                }
                else if (lstExamSuperTemp.Count >= 2)
                {
                    curSheet.CopyPasteSameSize(supervisory1, curRow + 5, 1);
                    curSheet.CopyPasteSameSize(supervisory2, curRow + 5, 5);
                    curSheet.CopyPasteSameSize(supervisoryName1, curRow + 9, 1);
                    curSheet.CopyPasteSameSize(supervisoryName2, curRow + 9, 5);
                    curSheet.SetCellValue(curRow + 9, 1, lstExamSuperTemp[0].EmployeeFullName);
                    curSheet.SetCellValue(curRow + 9, 5, lstExamSuperTemp[1].EmployeeFullName);
                }

                
                curSheet.FitAllColumnsOnOnePage = true;
            }

            firstSheet.Delete();

            return oBook.ToStream();
        }

        public void DownloadZipFileTranscript(IDictionary<string, object> dic, out string folderSaveFile, List<ExamSubjectBO> lstSubjectCat)
        {
            #region lay danh sach hoc sinh cua lop
            int academicYearID = Business.Common.Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Business.Common.Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            long examinationsID = Business.Common.Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Business.Common.Utils.GetLong(dic["ExamGroupID"]);
            int appliedLevelID = Business.Common.Utils.GetInt(dic["AppliedLevelID"]);
            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevelID);

            string fileName = "THI_{0}_PhieuThuBaiThi_{1}_{2}.xls";//THI_THPT_PhieuThuBaiThi_[NhomThi]_[MonThi]

            string tmp = string.Empty;
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + SystemParamsInFile.UPLOAD_FILE + "\\" + Guid.NewGuid().ToString());
            Directory.CreateDirectory(folder);
            string reportCode = SystemParamsInFile.REPORT_PHIEU_THU_BAI_THI;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);
            //Lay danh sach phong thi cua nhom thi
            List<ExamRoom> listExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                 .Where(o => o.ExamGroupID == examGroupID)
                                                 .OrderBy(o => o.ExamRoomCode)
                                                 .ToList();
            //Lấy danh sách thí sinh
            List<ExamPupilBO> listExamPupil = new List<ExamPupilBO>();
            if (examinationsID != 0 && examGroupID != 0)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = schoolID;
                SearchInfo["ExaminationsID"] = examinationsID;
                SearchInfo["ExamGroupID"] = examGroupID;
                listExamPupil = ExamPupilBusiness.GetExamPupilWithSubject(examinationsID, schoolID, academicYearID, partition).Where(x => x.ExamGroupID == examGroupID).ToList();
            }

            //Lấy danh sách môn học
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
            if (examinationsID != 0 && examGroupID != 0)
            {
                IDictionary<string, object> tmpDic = new Dictionary<string, object>();
                dic["ExaminationsID"] = examinationsID;
                dic["ExamGroupID"] = examGroupID;
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).ToList();
            }

            //Lấy danh sách giám thị
            List<ExamSupervisoryViewModel> lstExamSuper = _Search(examinationsID, examGroupID, 0, 0).ToList();

            for (int i = 0; i < lstSubjectCat.Count; i++)
            {
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                CreateTicketTakeExamPaperReportZip(dic, listExamRoom, listExamPupil, listExamSubject, lstExamSuper, exam, examGroup, academicYear, lstSubjectCat[i].SubjectID, oBook);
                oBook.CreateZipFile(string.Format(fileName, strAppliedLevel, ReportUtils.StripVNSign(examGroup.ExamGroupName), ReportUtils.StripVNSign(lstSubjectCat[i].SubjectName)), folder);               
            }
           
            folderSaveFile = folder;
            #endregion
        }

        // Lấy danh sách giám thị được phân công
        private IEnumerable<ExamSupervisoryViewModel> _Search(long examinationsID, long examGroupID, long subjectID, long examRoomID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExamRoomID"] = examRoomID;

            List<ExamSupervisoryAssignmentBO> listAssignment = ExamSupervisoryAssignmentBusiness.SearchByExamSubject
                              (_globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, examinationsID, examGroupID, subjectID, SearchInfo).ToList();

            //Lay danh sach phong thi cua nhom thi
            List<ExamRoom> listExamRoom = this.ExamRoomBusiness.All.Where(o => o.ExaminationsID == examinationsID)
                                                                   .Where(o => o.ExamGroupID == examGroupID)
                                                                   .OrderBy(o => o.ExamRoomCode).ToList();
            if (examRoomID > 0)
            {
                listExamRoom = listExamRoom.Where(o => o.ExamRoomID == examRoomID).ToList();
            }
            //Lay so thi sinh da duoc sap xep vao cac phong
            int partitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<ExamPupil> listExamPupil = this.ExamPupilBusiness.GetExamPupilOfExamGroup(examinationsID, examGroupID, _globalInfo.SchoolID.GetValueOrDefault()
                                            , _globalInfo.AcademicYearID.GetValueOrDefault(), partitionID).ToList();

            var listCountByRoom = (from q in listExamPupil
                                   group q by q.ExamRoomID into qg
                                   select new
                                   {
                                       ExamRoomID = qg.Key,
                                       ExamPupilCount = qg.Count()
                                   }).ToList();
            var examRoomWithCount = (from er in listExamRoom
                                     join cbr in listCountByRoom
                                         on er.ExamRoomID equals cbr.ExamRoomID into des
                                     from x in des.DefaultIfEmpty()
                                     select new
                                     {
                                         ExamRoomID = er.ExamRoomID,
                                         SettedNumber = x != null ? x.ExamPupilCount : 0,
                                         ExamRoomCode = er.ExamRoomCode
                                     }).ToList();

            List<ExamSupervisoryViewModel> ret = (from erwc in examRoomWithCount
                                                    join la in listAssignment
                                                        on erwc.ExamRoomID equals la.ExamRoomID into des
                                                    from x in des.DefaultIfEmpty()
                                                    select new ExamSupervisoryViewModel
                                                    {
                                                        EmployeeCode = x != null ? x.EmployeeCode : String.Empty,
                                                        EmployeeFullName = x != null ? x.EmployeeFullName : String.Empty,
                                                        EthnicCode = x != null ? x.EthnicCode : null,
                                                        ExamRoomCode = erwc.ExamRoomCode,
                                                        ExamRoomID = erwc.ExamRoomID,
                                                        ExamSupervisoryID = x != null ? x.ExamSupervisoryID : 0,
                                                        SchoolFacultyID = x != null ? x.SchoolFacultyID : 0,
                                                        SchoolFacultyName = x != null ? x.SchoolFacultyName : String.Empty,
                                                        ExamSupervisoryAssignmentID = x != null ? x.ExamSupervisoryAssignmentID : 0,
                                                        TeacherID = x != null ? x.TeacherID : 0,
                                                        ExamRoomSettedNumber = erwc.SettedNumber,
                                                        SubjectID = x != null ? x.SubjectID : 0                                                     
                                                    }).OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.EmployeeFullName.getOrderingName(o.EthnicCode))).ToList();
            
            return ret;
        }
        #endregion
    }

}
