﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Pdf;
using SMAS.Web.Areas.ExamResultReportArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamResultReportArea.Controllers
{
    [SkipCheckRole]
    public class ExamPupilAbsenceReportController:BaseController
    {
       #region properties
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        private List<Examinations> listExaminations;
     
        #endregion

        #region Constructor
        public ExamPupilAbsenceReportController(IExaminationsBusiness ExaminationsBusiness, IExamGroupBusiness ExamGroupBusiness, IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness,
            IExamRoomBusiness ExamRoomBusiness, IReportDefinitionBusiness ReportDefinitionBusiness, IProcessedReportBusiness ProcessedReportBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            this.ExamPupilAbsenceBusiness = ExamPupilAbsenceBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
        }
        #endregion

        #region Actions
        //
        // GET: /ExamResultReportArea/ExamPupilAbsenceReport/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }
        #endregion

        #region Report
        [ValidateAntiForgeryToken]
        public JsonResult GetReport(ReportInfoViewModel form)
        {
            if (form.ExaminationsID == null)
            {
                throw new BusinessException("ExamResultReport_Validate_Required", Res.Get("ExamResultReport_Label_ExaminationsID"));
            }
           
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_VANG_THI;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;

            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ExamPupilAbsenceBusiness.GetExamPupilAbsenceReport(dic);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }

            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ExamPupilAbsenceBusiness.CreateExamPupilAbsenceReport(dic);
                processedReport = ExamPupilAbsenceBusiness.InsertExamPupilAbsenceReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
                excel.Close();
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(ReportInfoViewModel form)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ExaminationsID"] = form.ExaminationsID;
            dic["ExamGroupID"] = form.ExamGroupID;
            dic["ExamRoomID"] = form.ExamRoomID;
            dic["AppliedLevelID"] = _globalInfo.AppliedLevel;
            Stream excel = ExamPupilAbsenceBusiness.CreateExamPupilAbsenceReport(dic);
            ProcessedReport processedReport = ExamPupilAbsenceBusiness.InsertExamPupilAbsenceReport(dic, excel, _globalInfo.AppliedLevel.GetValueOrDefault());
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        public FileResult DownloadPDFReport(int idProcessedReport)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", _globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_VANG_THI,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);
            IPdfExport objFile = new PdfExport();
            var objRetVal = objFile.ConvertPdf(excel, processedReport.ReportName, 0);
            return File(objRetVal.ContentFile, PdfExport.CONTENT_TYPE_PDF);
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            //Lấy danh sách kỳ thi
            listExaminations = ExaminationsBusiness.GetListExamination(_globalInfo.AcademicYearID.GetValueOrDefault())
                                            .Where(o => o.SchoolID == _globalInfo.SchoolID)
                                            .Where(o => o.AppliedLevel == _globalInfo.AppliedLevel)
                                            .OrderByDescending(o => o.CreateTime).ToList();

            ViewData[ExamResultReportConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
        }

        #endregion
    }
}