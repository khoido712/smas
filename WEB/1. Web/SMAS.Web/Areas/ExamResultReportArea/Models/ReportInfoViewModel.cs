﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace SMAS.Web.Areas.ExamResultReportArea.Models
{
    public class ReportInfoViewModel
    {
        [ResourceDisplayName("ExamResultReport_Label_ExaminationsID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamResultReportConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExaminationsChange()")]
        public long? ExaminationsID { get; set; }

        [ResourceDisplayName("ExamResultReport_Label_ExamGroupID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamResultReportConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("OnChange", "onExamGroupChange()")]
        public long? ExamGroupID { get; set; }

        [ResourceDisplayName("ExamResultReport_Label_ExamSubjectID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamResultReportConstants.CBO_EXAM_SUBJECT)]
        [AdditionalMetadata("OnChange", "onExamSubjectChange()")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? SubjectID { get; set; }

        [ResourceDisplayName("ExamResultReport_Label_ExamSubjectID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamResultReportConstants.CBO_EXAM_SUBJECT)]
        [AdditionalMetadata("OnChange", "onExamSubjectChange()")]
        public int? SubjectIDCustom { get; set; }

        [ResourceDisplayName("ExamResultReport_Label_ExamSubjectID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamResultReportConstants.CBO_EXAM_SUBJECT)]
        public int? _SubjectID { get; set; }

        [ResourceDisplayName("ExamResultReport_Label_ExamRoomID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamResultReportConstants.CBO_EXAM_ROOM)]
        [AdditionalMetadata("OnChange", "onExamRoomChange()")]
        public long? ExamRoomID { get; set; }

        [ResourceDisplayName("ExamResultReport_Label_StatisticLevelReportID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamResultReportConstants.CBO_STATISTIC_LEVEL)]
        public int? StatisticLevelReportID { get; set; }

        [ResourceDisplayName("ExamResultReport_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", ExamResultReportConstants.CBO_EDUCATION_LEVEL)]
        [AdditionalMetadata("OnChange", "onEducationLevelChange()")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("ExamResultReport_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamResultReportConstants.CBO_EDUCATION_LEVEL)]
        [AdditionalMetadata("OnChange", "onEducationLevelChange2()")]
        public int? EducationLevelID2 { get; set; }

        [ResourceDisplayName("ExamResultReport_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamResultReportConstants.CBO_CLASS)]
        public int? ClassID { get; set; }

        [ResourceDisplayName("ExamResultReport_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamResultReportConstants.CBO_EDUCATION_LEVEL)]
        [AdditionalMetadata("OnChange", "LoadClass()")]
        public int? EducationLevelIDVM { get; set; }

        [ResourceDisplayName("ExamResultReport_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("ViewDataKey", ExamResultReportConstants.CBO_CLASS)]
        public int? ClassIDVM { get; set; }

        public bool FemalePupil { get; set; }
        public bool EthnicPupil { get; set; }
        public bool checkSumMark { get; set; }
        public bool checkClassLevel { get; set; }
        public bool checkSchoolLevel { get; set; }
    }
}