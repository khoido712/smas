﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamResultReportArea
{
    public class ExamResultReportConstants
    {
        //Viewdata
        public const string CBO_EXAMINATIONS = "cbo_examinations";
        public const string CBO_EXAM_GROUP = "cbo_exam_group";
        public const string CBO_EXAM_SUBJECT = "cbo_exam_subject";
        public const string CBO_EXAM_ROOM = "cbo_exam_room";
        public const string CBO_STATISTIC_LEVEL = "cbo_statistic_level";
        public const string APPLIED_LEVEL = "applied_level";
        public const string CBO_EDUCATION_LEVEL = "cbo_education_level";
        public const string CBO_CLASS = "cbo_class";
    }
}