﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ImportPupilArea
{
    public class ImportPupilConstants
    {
        public const string LIST_EDUCATION_LEVEL = "list_education_level";

        public const string LIST_IMPORTED_PUPIL = "LIST_IMPORTED_PUPIL";
        public const string DISABLE_BTNSAVE = "DISABLE_BTNSAVE";
    }
}