﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Common;
using System.Globalization;
using SMAS.Web.Areas.ImportPupilArea.Models;
using System.Text;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.ComponentModel;
using SMAS.Business.Business;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.ImportPupilArea.Controllers
{
    public class ImportPupilController : BaseController
    {      
        IPupilProfileBusiness PupilProfileBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        IEducationLevelBusiness EducationLevelBusiness;
        IClassProfileBusiness ClassProfileBusiness;
        IProvinceBusiness ProvinceBusiness;
        IPolicyTargetBusiness PolicyTargetBusiness;
        IEthnicBusiness EthnicBusiness;
        IReligionBusiness ReligionBusiness;
        IDistrictBusiness DistrictBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        IPupilOfClassBusiness PupilOfClassBusiness;
        ICommuneBusiness CommuneBusiness;
        IVillageBusiness VillageBusiness;
        IMarkRecordBusiness MarkRecordBusiness;
        IJudgeRecordBusiness JudgeRecordBusiness;
        IPupilRankingBusiness PupilRankingBusiness;
        ISummedUpRecordBusiness SummedUpRecordBusiness;
        IPupilAbsenceBusiness PupilAbsenceBusiness;
        IPupilFaultBusiness PupilFaultBusiness;
        ISchoolProfileBusiness SchoolProfileBusiness;
        IPolicyRegimeBusiness PolicyRegimeBusiness;
        IAreaBusiness AreaBusiness;
        IDisabledTypeBusiness DisabledTypeBusiness;

        private readonly ICodeConfigBusiness CodeConfigBusiness;


        public ImportPupilController(IPupilProfileBusiness pupilProfileBusiness,
                                        IAcademicYearBusiness academicYearBusiness,
                                        IEducationLevelBusiness educationLevelBusiness,
                                        IClassProfileBusiness classProfileBusiness,
                                        IProvinceBusiness provinceBusiness,
                                        IPolicyTargetBusiness policyTargetBusiness,
                                        IEthnicBusiness ethnicBusiness,
                                        IReligionBusiness religionBusiness,
                                        IDistrictBusiness districtBusiness,
                                        IReportDefinitionBusiness reportDefinitionBusiness,
                                        IPupilOfClassBusiness pupilOfClassBusiness,
                                        ICodeConfigBusiness CodeConfigBusiness,
                                        ICommuneBusiness communeBusiness,
                                        IVillageBusiness villageBusiness, 
            IMarkRecordBusiness MarkRecordBusiness,
            IJudgeRecordBusiness JudgeRecordBusiness,
            IPupilRankingBusiness PupilRankingBusiness,
            ISummedUpRecordBusiness SummedUpRecordBusiness, 
            IPupilAbsenceBusiness PupilAbsenceBusiness, 
            IPupilFaultBusiness PupilFaultBusiness, 
            ISchoolProfileBusiness SchoolProfileBusiness,
            IPolicyRegimeBusiness PolicyRegimeBusiness,
            IAreaBusiness AreaBusiness,
            IDisabledTypeBusiness DisabledTypeBusiness
            )
        {
            this.CodeConfigBusiness = CodeConfigBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.ProvinceBusiness = provinceBusiness;
            this.PolicyTargetBusiness = policyTargetBusiness;
            this.EthnicBusiness = ethnicBusiness;
            this.ReligionBusiness = religionBusiness;
            this.DistrictBusiness = districtBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;

            this.CommuneBusiness = communeBusiness;
            this.VillageBusiness = villageBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.JudgeRecordBusiness = JudgeRecordBusiness;
            this.SummedUpRecordBusiness = SummedUpRecordBusiness;
            this.PupilRankingBusiness = PupilRankingBusiness;
            this.PupilAbsenceBusiness = PupilAbsenceBusiness;
            this.PupilFaultBusiness = PupilFaultBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PolicyRegimeBusiness = PolicyRegimeBusiness;
            this.AreaBusiness = AreaBusiness;
            this.DisabledTypeBusiness = DisabledTypeBusiness;
        }

        #region Constants
        private const string SEMICOLON_SPACE = "; ";
        #endregion
        public ActionResult Index()
        {
            SetViewDataPermission("ImportPupil", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            ViewData[ImportPupilConstants.LIST_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");
            if (!new GlobalInfo().IsCurrentYear)
            {
                ViewData[ImportPupilConstants.DISABLE_BTNSAVE] = true;
            }
            else
            {
                ViewData[ImportPupilConstants.DISABLE_BTNSAVE] = false;
            }

            return View();
        }

        [ValidateAntiForgeryToken]
        public JsonResult Import(int? EducationLevelID, FormCollection frm)
        {
            try
            {
                EducationLevelID = EducationLevelID.HasValue ? EducationLevelID : 0;

                bool isAddPupil = "on".Equals(frm["rdoAddPupil"]);
                bool isUpdatePupil = "on".Equals(frm["rdoUpdatePupil"]);
                HttpPostedFileBase file = Request.Files.Count > 0 ? Request.Files[0] : null;

                if (file == null)
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("InputExaminationMark_Label_NoFileSelected"), "notSuccess"));
                    return res;
                }

                if (!file.FileName.EndsWith(".xls") && !file.FileName.EndsWith(".xlsx"))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExtensionError"), "notSuccess"));
                    return res;
                }
                int maxSize = 3145728;
                if (file.ContentLength > maxSize)
                {
                    JsonResult res = Json(new JsonMessage(string.Format(Res.Get("Common_Label_MaxSizeExceeded"), "3 MB"), "notSuccess"));
                    return res;
                }

                IVTWorkbook book = VTExport.OpenWorkbook(file.InputStream);
                IVTWorksheet sheet = book.GetSheet(1);
                string SchoolName = sheet.GetCellValue(2, VTVector.dic['A']) != null ? (string)sheet.GetCellValue(2, VTVector.dic['A']) : "";
                SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
                if (school.SchoolName.Trim().ToUpper() != SchoolName.Trim().ToUpper())
                {
                    JsonResult res = Json(new JsonMessage("Tên trường trong file excel không đúng với tên trường trong hệ thống", "notSuccess"));
                    return res;
                }
                List<Village> lstVillageImport = new List<Village>();

                List<ImportPupilViewModel> lstPupilImported = this.ReadExcelFile(sheet, EducationLevelID.Value, isAddPupil, isUpdatePupil, ref lstVillageImport);
                Session["LIST_IMPORTED_PUPIL"] = lstPupilImported;
                Session["lstVillageImport"] = lstVillageImport;

                if (lstPupilImported.Count == 0)
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("ImportPupil_Label_NoPupilImported"), "notSuccess"));
                    return res;
                }
              
                int countAllPupil = lstPupilImported.Count;
                int countPupilValid = lstPupilImported.Count(p => p.IsValid);
                string retMsg;
                if (countPupilValid > 0 && countPupilValid > 0 && countAllPupil == countPupilValid)
                {
                    SaveData(isAddPupil);
                    retMsg = String.Format("{0} {1}/{2} học sinh", Res.Get("Common_Label_ImportSuccessMessage"), countPupilValid, countAllPupil);
                    return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS), "text/html");
                }
                JsonResult resx = Json(new JsonMessage(Res.Get("ImportPupil_Label_ImportFailure"), JsonMessage.ERROR), "text/html");
                return resx;
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "Import", "EducationLevelID=" + EducationLevelID, ex);
                JsonResult res = Json(new JsonMessage("Không đọc được file excel", "Permisstion"), "text/html");
                return res;
            }
        }

        public FileResult Export(int EducationLevelID)
        {
            string reportName = string.Empty;
            Stream excel = this.SetDataToFileExcel(EducationLevelID, ref reportName);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = reportName;
            return result;
        }

        public FileResult DowloadFileErr(int EducationLevelID)
        {
            string reportName = string.Empty;
            Stream excel = this.SetDataToFileExcel(EducationLevelID, ref reportName, true);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = reportName;
            return result;
        }

        public PartialViewResult LoadImportedGrid()
        {
            return PartialView("_GridImported");
        }

        private List<ImportPupilViewModel> ReadExcelFile(IVTWorksheet sheet, int educationLevelID, bool isAddPupil, bool isUpdatePupil, ref List<Village> lstVillageImport)
        {
            Session["EducationLevelID"] = educationLevelID;
            List<ImportPupilViewModel> listPupil = new List<ImportPupilViewModel>();
            List<ImportPupilViewModel> listPupilTemp = new List<ImportPupilViewModel>();
            // Congnv: Mẫu cũ
            //int rowIndex = 10;
            int rowIndex = 5;

            #region // const
            // Congnv: Thêm các tên cột trong bảng Excel (Hỗ trợ sửa đổi mẫu sau này)
            //DucPT1: sua bieu mau 08/2016 bieu mau: hs_import_thongtinhocsinh_2016
            const string indexCol = "A";      // STT
            const string pupilCodeCol = "B";  // Mã học sinh
            const string fullNameCol = "C";   // Tên học sinh
            const string birthDateCol = "D"; // Ngày sinh
            const string genreNameCol = "E"; // Giới tính
            const string classNameCol = "F"; // Lớp
            const string enrolmentTypeNameCol = "G"; // Hình thức trúng tuyển
            const string enrolmentDateCol = "H"; // Ngày vào trường
            const string profileStatusNameCol = "I"; // trạng thái học sinh
            const string provinceNameCol = "J"; //  Tỉnh thành
            const string districtNameCol = "K"; // Quận huyện
            const string communeNameCol = "L"; // Xã phường
            const string villageNameCol = "M"; // Thôn xóm
            const string StorageNumberCol = "N"; // số đăng bộ
            const string birthPlaceCol = "O"; // nơi sinh
            const string homeTownCol = "P"; // quê quán
            const string permanentResidentalAddressCol = "Q"; // địa chỉ thường trú
            const string tempResidentalAddressCol = "R";  // địa chỉ tạm trú
            const string foreignLanguageTrainingNameCol = "S"; // hệ học ngoại ngữ
            const string LearningTypeNameCol = "T"; // Hình thức học

            string youngPioneerJoinedDate = string.Empty; // Ngày vào đội
            string youngLeagueJoinedDate = string.Empty; // Ngày vào đoàn
            string communistPartyJoinedDate = string.Empty; // Ngày vào đảng

            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                youngPioneerJoinedDate = "U";
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                youngPioneerJoinedDate = "U";
                youngLeagueJoinedDate = "V";
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                youngPioneerJoinedDate = "U";
                youngLeagueJoinedDate = "V";
                communistPartyJoinedDate = "W";
            }

            const string ethnicNameCol = "X"; // Dân tộc
            const string religionNameCol = "Y"; // Tôn giáo
            const string policyTargetNameCol = "Z"; // đối tượng chính sách
            const string policyRegimeCol = "AA"; // Chế độ chính sách
            const string isSupportForLearningCol = "AB"; //  Được hỗ trợ kinh phí học tập
            const string isResettlementTargetCol = "AC"; // Thuộc diện tái định cư
            const string areaNameCol = "AD"; // Khu vực
            const string classTypeCol = "AE"; // Diện học sinh
            const string disabilityTypeCol = "AF"; // Loại khuyết tật
            const string mobileCol = "AG"; // sdt di động
            const string identifyNumberCol = "AH"; // CMND
            const string emailCol = "AI"; // Email
            const string bloodTypeNameCol = "AJ"; // nhóm máu
            const string IsUsedMoetProgram = "AK"; // học chương trình giá dục của Bộ
            const string IsSwimmingCol = "AL"; // Biết bơi
            const string fatherFullNameCol = "AM"; // Tên cha
            const string fatherBirthDateCol = "AN"; // Năm sinh của cha 
            const string fatherJobCol = "AO"; // Nghề nghiệp của cha
            const string fatherMobileCol = "AP"; // SDt của cha
            const string fatherEmailCol = "AQ"; // Email của cha
            const string motherFullNameCol = "AR"; // Tên mẹ
            const string motherBirthDateStrCol = "AS"; // Năm sinh của mẹ
            const string motherJobCol = "AT"; // Nghề nghiệp của mẹ
            const string motherMobileCol = "AU"; // SDT của mẹ
            const string motherEmailCol = "AV"; // Email của mẹ
            const string sponsorFullNameCol = "AW"; // Tên người bảo hộ
            const string sponsorBirthDateStrCol = "AX"; // Ngày sinh người bảo hộ
            const string sponsorJobCol = "AY"; // Nghề nghiepj người bảo hộ
            const string sponsorMobilCol = "AZ"; // SDT người bảo hộ
            const string sponsorEmailCol = "BA"; // Email người bảo hộ
            const string isMinorityFatherCol = "BB"; // bố là dân tộc
            const string isMinorityMotherCol = "BC"; //Mẹ là dân tộc
            const string pupilCodeNew = "BD"; // Mã học sinh mới
            #endregion

            #region // get data
            // Khu vực
            var lstArea = AreaBusiness.All.Where(x => x.IsActive.HasValue && x.IsActive.Value == true)
                                                        .Select(x => new { x.AreaID, x.AreaName }).ToList()
                                                        .Select(x => new ComboObject(x.AreaID.ToString(), x.AreaName)).ToList();
            // Chế độ chính sách
            var lstPolicyRegimes = PolicyRegimeBusiness.All.Where(x => x.IsActive).Select(x => new { x.PolicyRegimeID, x.Resolution }).ToList()
                                                        .Select(x => new ComboObject(x.PolicyRegimeID.ToString(), x.Resolution)).ToList();
            // Loại khuyết tật
            var lstDisabilityType = DisabledTypeBusiness.All.Where(x => x.IsActive).Select(x => new { x.DisabledTypeID, x.Resolution }).ToList()
                                                        .Select(x => new ComboObject(x.DisabledTypeID.ToString(), x.Resolution)).ToList();

            // Danh sách Tỉnh/thành, Quận/Huyện, Xã/Phường
            List<Province> lstProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            List<District> lstDistrict = DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            List<Commune> listCom = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            IQueryable<Village> lstVillage = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });

            var listClass = ClassProfileBusiness.Search(new Dictionary<string, object> { { "AcademicYearID", _globalInfo.AcademicYearID.Value },
                                                        { "EducationLevelID", educationLevelID }, { "IsActive", true } })
                                                        .Where(x => x.EducationLevel.Grade == _globalInfo.AppliedLevel)
                                                .Select(u => new { u.ClassProfileID, u.DisplayName }).ToList()
                                                .Select(u => new ComboObject(u.ClassProfileID.ToString(), u.DisplayName.ToLower())).ToList();
            var listEthnic = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.EthnicID, u.EthnicName }).ToList().Select(u => new ComboObject(u.EthnicID.ToString(), u.EthnicName.ToLower())).ToList();
            var listPolicyTarget = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.PolicyTargetID, u.Resolution }).ToList().Select(u => new ComboObject(u.PolicyTargetID.ToString(), u.Resolution.ToLower())).ToList();
            //List<string> lstPolicyTargetNameTemp = listPolicyTarget.Select(o => o.value).ToList();
            var listReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.ReligionID, u.Resolution }).ToList().Select(u => new ComboObject(u.ReligionID.ToString(), u.Resolution.ToLower())).ToList();
            int tempCount = 0;
            bool isAutoGenCode = CodeConfigBusiness.IsAutoGenCode(_globalInfo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            bool isSchoolModify = CodeConfigBusiness.IsSchoolModify(_globalInfo.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_PUPIL);
            string originalPupilCode = string.Empty;
            int maxOrderNumber = 0;
            int numberLength = 0;
            //int partitionID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
            List<PupilOfClassBO> lstpoc = (from poc in PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { { "AcademicYearID", _globalInfo.AcademicYearID.Value } })
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               ClassID = poc.ClassID,
                                               PupilCode = pf.PupilCode,
                                               AssignedDate = poc.AssignedDate,
                                               Status = poc.Status
                                           }).ToList();
            //List<int> lstPupilID = lstpoc.Select(p => p.PupilID).Distinct().ToList();
            List<int> LstClassIDInCurrentYear = new List<int>();
            List<int> LstClassIDNotInCurrentYear = new List<int>();
            List<string> lstPupilCodeOtherYear = new List<string>();
            List<string> lstPupilCodeCurrentYear = new List<string>();

            var lstEducationLevel = _globalInfo.EducationLevels.Select(x => x.EducationLevelID).ToList();
            if (educationLevelID > 0)
            {
                lstEducationLevel = lstEducationLevel.Where(x => x == educationLevelID).ToList();
            }

            var lstEducationLevelWithClass = ClassProfileBusiness.All.Where(x => lstEducationLevel.Contains(x.EducationLevelID))
                                            .Where(x => x.SchoolID == _globalInfo.SchoolID.Value)
                                            .Where(x => x.AcademicYearID == _globalInfo.AcademicYearID)
                                            .Select(x => new { EducationLevelID = x.EducationLevelID, ClassName = x.DisplayName }).Distinct().ToList();

            List<AutoGenericCode> lstOutGenericCode = new List<AutoGenericCode>();
            if (isAutoGenCode)
            {
                AutoGenericCode objAutoGen = null;
                foreach (var item in lstEducationLevel)
                {
                    CodeConfigBusiness.GetPupilCodeForImport(_globalInfo.AcademicYearID.Value, item, _globalInfo.SchoolID.Value, out originalPupilCode, out maxOrderNumber, out numberLength);

                    objAutoGen = new AutoGenericCode();
                    objAutoGen.Index = 0;
                    objAutoGen.OriginalPupilCode = originalPupilCode;
                    objAutoGen.MaxOrderNumber = maxOrderNumber;
                    objAutoGen.NumberLength = numberLength;
                    objAutoGen.EducationLevelID = item;
                    lstOutGenericCode.Add(objAutoGen);
                }
            }

            // Thông tin Mã HS của các năm học phải là năm hiện tại
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel);
            dic.Add("SchoolID", _globalInfo.SchoolID);
            dic.Add("AcademicYearID", _globalInfo.AcademicYearID); // Năm hiện tại
            // Danh sách ClassId trong năm hiện tại
            LstClassIDInCurrentYear = ClassProfileBusiness.Search(dic).Select(o => o.ClassProfileID).ToList();
            // Danh sách lớp trong các năm khác - chỉ xét trong cùng cấp
            LstClassIDNotInCurrentYear = this.ClassProfileBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID && o.EducationLevel.Grade == _globalInfo.AppliedLevel
                                                            && !LstClassIDInCurrentYear.Contains(o.ClassProfileID) && o.IsActive.Value).Select(o => o.ClassProfileID).ToList();
            // Lấy danh sách HS trong các năm khác
            lstPupilCodeOtherYear = lstpoc.Where(p => LstClassIDNotInCurrentYear.Contains(p.ClassID)).Select(p => p.PupilCode).ToList();
            lstPupilCodeCurrentYear = lstpoc.Where(p => LstClassIDInCurrentYear.Contains(p.ClassID)).Select(p => p.PupilCode).ToList();

            var listCheckIndex = new List<string>();

            //Tìm kiếm học sinh trong năm học
            List<PupilProfile> lstPP = null;

            lstPP = (from pf in PupilProfileBusiness.All
                     join poc in PupilOfClassBusiness.All on pf.PupilProfileID equals poc.PupilID
                     where poc.SchoolID == _globalInfo.SchoolID.Value
                     && pf.IsActive
                     select pf).ToList();

            //lstPP = PupilProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { { "IsActive", true } }).ToList();

            int modSchoolID = _globalInfo.SchoolID.Value % 100;
            int year = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value).Year;
            #endregion

            // Biến lưu dữ liệu tận dụng lại để giảm thời gian import
            // Tỉnh thành trước đó
            string ProviceNameBefore = string.Empty;
            int ProvinceIdBefore = 0;
            // Quận/Huyện trước đó
            List<District> lstDistrictInProvince = new List<District>();
            string DistrictNameBefore = string.Empty;
            int DistrictIdBefore = 0;

            var culture = new CultureInfo("vi-VN");
            StringBuilder ErrorMessage = new StringBuilder();
            bool chkCheck = true;
            List<string> lstPupilCodeNew = new List<string>();

            while (!string.IsNullOrEmpty(GetCellString(sheet, indexCol + rowIndex))
                || !string.IsNullOrEmpty(GetCellString(sheet, fullNameCol + rowIndex))
                || !string.IsNullOrEmpty(GetCellString(sheet, classNameCol + rowIndex)))
            {
                ErrorMessage = new StringBuilder();
                ImportPupilViewModel pupil = new ImportPupilViewModel();
                pupil.checkInsertVilliage = false;
                pupil.IsValid = true;
                //STT
                pupil.IndexStr = GetCellString(sheet, indexCol + rowIndex);
                int? index = GetCellIndex(pupil.IndexStr);

                if (index.HasValue)
                {
                    pupil.Index = index.Value;
                }
                else
                {
                    pupil.Index = 0;
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_STTNotNumber")).Append(SEMICOLON_SPACE);
                }

                #region // Thông tin học sinh
                //Mã HS
                pupil.PupilCode = GetCellString(sheet, pupilCodeCol + rowIndex);
                // Ho va ten
                pupil.FullName = GetCellString(sheet, fullNameCol + rowIndex);
                pupil.Name = GetPupilName(pupil.FullName);
                if (string.IsNullOrEmpty(pupil.FullName))
                {
                    ErrorMessage.Append(Res.Get("Họ và tên học sinh không được để trống")).Append(SEMICOLON_SPACE);
                }
                // Ngay sinh
                pupil.BirthDateStr = GetCellString(sheet, birthDateCol + rowIndex);
                string birthDateTemp = GetCellString(sheet, birthDateCol + rowIndex);
                DateTime birthDate;
                #region // check ngày sinh
                if (!string.IsNullOrEmpty(birthDateTemp))
                {
                    if (DateTime.TryParse(birthDateTemp, culture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out birthDate))
                    {
                        pupil.BirthDate = birthDate;
                        if (pupil.BirthDate.Value > DateTime.Now.Date)
                        {
                            ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidBirthDate_DateTimeNow")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.BirthDate.Value <= new DateTime(1900, 1, 1))
                        {
                            ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidBirthDate")).Append(SEMICOLON_SPACE);
                        }
                    }
                    else
                    {
                        ErrorMessage.Append("Ngày sinh không đúng định dạng").Append(SEMICOLON_SPACE);
                    }
                }
                else
                {
                    ErrorMessage.Append("Ngày sinh không được để trống").Append(SEMICOLON_SPACE);
                }
                #endregion

                // Gioi tinh
                pupil.GenreName = GetCellString(sheet, genreNameCol + rowIndex);
                pupil.GenreNameReal = pupil.GenreName == "" ? SystemParamsInFile.REPORT_LOOKUPINFO_MALE : pupil.GenreName.ToUpper() == "X" ? SystemParamsInFile.REPORT_LOOKUPINFO_FEMALE : pupil.GenreName;
                pupil.Genre = pupil.GenreName == "" ? SystemParamsInFile.GENRE_MALE : SystemParamsInFile.GENRE_FEMALE;
                if (pupil.GenreName != "" && pupil.GenreName.ToUpper() != "X")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidGenre")).Append(SEMICOLON_SPACE);
                }
                // Lớp
                string className = GetCellString(sheet, classNameCol + rowIndex);
                int classID = SetFromList(listClass, className);
                pupil.ClassName = className;
                pupil.CurrentClassID = classID;

                if (string.IsNullOrEmpty(pupil.ClassName))
                {
                    ErrorMessage.Append(Res.Get("Tên lớp không được để trống")).Append(SEMICOLON_SPACE);
                }
                else
                {
                    if (classID <= 0)
                    {
                        ErrorMessage.Append(Res.Get("Tên lớp không hợp lệ")).Append(SEMICOLON_SPACE);
                    }
                }

                // Hình thức trúng tuyển
                pupil.EnrolmentTypeName = GetCellString(sheet, enrolmentTypeNameCol + rowIndex);
                pupil.EnrolmentType = SetEnrolementType(pupil.EnrolmentTypeName);
                // Ngày vào trường
                pupil.EnrolmentDateStr = GetCellString(sheet, enrolmentDateCol + rowIndex);
                string enrolmentDateTemp = GetCellString(sheet, enrolmentDateCol + rowIndex);
                DateTime enrolmentDate;
                #region // check ngày vào trường
                if (!String.IsNullOrEmpty(enrolmentDateTemp))
                {
                    if (DateTime.TryParse(enrolmentDateTemp, culture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out enrolmentDate))
                    {
                        pupil.EnrolmentDate = enrolmentDate;
                        if (pupil.EnrolmentDate.Value.Date > DateTime.Now.Date)
                        {
                            ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidEnrolmentDate_DateTimaNow")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.EnrolmentDate.Value < new DateTime(1900, 1, 1))
                        {
                            ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidEnrolmentDate")).Append(SEMICOLON_SPACE);
                        }
                    }
                    else
                    {

                        ErrorMessage.Append("Ngày vào trường không đúng định dạng").Append(SEMICOLON_SPACE);
                    }
                }
                else
                {
                    ErrorMessage.Append("Ngày vào trường không được để trống").Append(SEMICOLON_SPACE);
                }
                #endregion
                // Trạng thái học sinh
                pupil.ProfileStatusName = GetCellString(sheet, profileStatusNameCol + rowIndex);
                pupil.ProfileStatus = pupil.ProfileStatusName == "" ? SystemParamsInFile.PUPIL_STATUS_STUDYING : SetProfileStatus(pupil.ProfileStatusName);
                // Tỉnh/thành
                pupil.ProvinceName = GetCellString(sheet, provinceNameCol + rowIndex);
                #region // Cho phép nhập Tên hoặc Mã Tỉnh thành
                if (!string.IsNullOrEmpty(pupil.ProvinceName))
                {
                    if (pupil.ProvinceName.Equals(ProviceNameBefore))
                    {
                        pupil.ProvinceID = ProvinceIdBefore;
                    }
                    else if (lstProvince.Exists(o => o.ProvinceName.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())))
                    {
                        Province provinceObj = lstProvince.Where(o => o.ProvinceName.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())).FirstOrDefault();
                        if (provinceObj != null)
                        {
                            pupil.ProvinceID = provinceObj.ProvinceID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            lstDistrictInProvince = lstDistrict.Where(o => o.ProvinceID == pupil.ProvinceID).ToList();
                            ProviceNameBefore = pupil.ProvinceName;
                            ProvinceIdBefore = provinceObj.ProvinceID;
                        }
                    }
                    else if (lstProvince.Exists(o => o.ProvinceCode.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())))
                    {
                        Province provinceObj = lstProvince.Where(o => o.ProvinceCode.ToLower().Equals(pupil.ProvinceName.Trim().ToLower())).FirstOrDefault();
                        if (provinceObj != null)
                        {
                            pupil.ProvinceID = provinceObj.ProvinceID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            lstDistrictInProvince = lstDistrict.Where(o => o.ProvinceID == pupil.ProvinceID).ToList();
                            ProviceNameBefore = pupil.ProvinceName;
                            ProvinceIdBefore = provinceObj.ProvinceID;
                        }
                    }
                }
                #endregion

                if (string.IsNullOrEmpty(pupil.ProvinceName))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidProvinceNameNull")).Append(SEMICOLON_SPACE);
                }

                if (pupil.ProvinceName != null && pupil.ProvinceName.Trim() != "" && pupil.ProvinceID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidProvinceName")).Append(SEMICOLON_SPACE);
                }
                // Quận/Huyện
                pupil.DistrictName = GetCellString(sheet, districtNameCol + rowIndex);
                #region // Check Quận/Huyện
                if (!string.IsNullOrEmpty(pupil.DistrictName))
                {
                    if (pupil.DistrictName.Equals(DistrictNameBefore))
                    {
                        pupil.DistrictID = DistrictIdBefore;
                    }
                    else if (lstDistrictInProvince.Exists(o => o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower()) || o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower())))
                    {
                        District obj = lstDistrictInProvince.Where(o => o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower()) || o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower())).FirstOrDefault();
                        if (obj != null)
                        {
                            pupil.DistrictID = obj.DistrictID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            DistrictNameBefore = pupil.DistrictName;
                            DistrictIdBefore = obj.DistrictID;
                        }
                    }
                    else if (lstDistrict.Exists(o => o.ProvinceID == pupil.ProvinceID && (o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower()) || o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower()))))
                    {
                        District obj = lstDistrict.Where(o => o.ProvinceID == pupil.ProvinceID && ((o.DistrictName != null && o.DistrictName.ToLower().Equals(pupil.DistrictName.ToLower())) || (o.DistrictCode != null && o.DistrictCode.ToLower().Equals(pupil.DistrictName.ToLower()))))
                                        .FirstOrDefault();
                        if (obj != null)
                        {
                            pupil.DistrictID = obj.DistrictID;
                            // Set lai gia tri gan nhat dung cho cac lan sau
                            lstDistrictInProvince = lstDistrict.Where(o => o.DistrictID == pupil.DistrictID).ToList();
                            DistrictNameBefore = pupil.DistrictName;
                            DistrictIdBefore = pupil.DistrictID.Value;
                        }
                    }
                }

                if (pupil.DistrictName != null && pupil.DistrictName.Trim() != "" && pupil.DistrictID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidDistrictName")).Append(SEMICOLON_SPACE);
                }
                #endregion

                // Xã/Phường
                pupil.CommuneName = GetCellString(sheet, communeNameCol + rowIndex);
                #region // Xã/Phường
                if (!string.IsNullOrEmpty(pupil.CommuneName))
                {
                    if (listCom.Exists(o => o.DistrictID == pupil.DistrictID && ((o.CommuneName != null && o.CommuneName.ToLower().Equals(pupil.CommuneName.ToLower())) || (o.CommuneCode != null && o.CommuneCode.ToLower().Equals(pupil.CommuneName.ToLower())))))
                    {
                        Commune comObj = listCom.Where(o => o.DistrictID == pupil.DistrictID && ((o.CommuneName != null && o.CommuneName.ToLower().Equals(pupil.CommuneName.ToLower())) || (o.CommuneCode != null && o.CommuneCode.ToLower().Equals(pupil.CommuneName.ToLower()))))
                                        .FirstOrDefault();
                        if (comObj != null)
                        {
                            pupil.CommuneID = comObj.CommuneID;
                        }
                    }
                }
                #endregion
                // Thôm/Xóm
                pupil.VillageName = GetCellString(sheet, villageNameCol + rowIndex);
                bool checkDistrict = checkCompareDistrict(pupil.ProvinceID, pupil.DistrictID, lstDistrict);
                bool checkCommune = checkCompareCommune(pupil.DistrictID, pupil.CommuneID, listCom);
                //Check Nhập tỉnh thành trước khi nhập quận huyện
                if (pupil.DistrictID != null && pupil.ProvinceID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidDistrictProvince")).Append(SEMICOLON_SPACE);
                }
                if (pupil.CommuneID != null && (pupil.DistrictID == null || pupil.ProvinceID == null))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidCommuneDistrict")).Append(SEMICOLON_SPACE);
                }
                if (!checkDistrict)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidDistrict")).Append(SEMICOLON_SPACE);
                }
                if (pupil.CommuneName != null && pupil.CommuneName.Trim() != "" && pupil.CommuneID == null)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidCommuneName")).Append(SEMICOLON_SPACE);
                }

                if (!checkCommune)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidCommune")).Append(SEMICOLON_SPACE);
                }

                if (!checkCompareVillage(pupil.VillageID, pupil.CommuneID))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidVilliageName")).Append(SEMICOLON_SPACE);
                }

                if (pupil.CommuneID == null || pupil.DistrictID == null || pupil.ProvinceID == null)
                {
                    if (pupil.VillageName != null && pupil.VillageName.Trim() != "")
                    {
                        ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidVilliage")).Append(SEMICOLON_SPACE);
                    }
                }
                if (pupil.VillageName != null && pupil.VillageName.Trim().Length > 50)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidVilliageLength")).Append(SEMICOLON_SPACE);
                }

                // Số đăng bộ
                pupil.StorageNumber = GetCellString(sheet, StorageNumberCol + rowIndex);
                //Nơi sinh
                pupil.BirthPlace = GetCellString(sheet, birthPlaceCol + rowIndex);
                // Quê quán
                pupil.HomeTown = GetCellString(sheet, homeTownCol + rowIndex);
                // Địa chỉ thường trú
                pupil.PermanentResidentalAddress = GetCellString(sheet, permanentResidentalAddressCol + rowIndex);
                // Địa chỉ tạm trú
                pupil.TempResidentalAddress = GetCellString(sheet, tempResidentalAddressCol + rowIndex);
                // Hệ học ngoại ngữ
                pupil.ForeignLanguageTrainingName = GetCellString(sheet, foreignLanguageTrainingNameCol + rowIndex);
                pupil.ForeignLanguageTraining = SetForeignLanguageTraining(pupil.ForeignLanguageTrainingName);
                // Hình thức học
                pupil.PupilLearningTypeName = GetCellString(sheet, LearningTypeNameCol + rowIndex);
                pupil.PupilLearningType = SetPupilLearningType(pupil.PupilLearningTypeName);
                // Ngày vào đội, đoàn, đảng
                string youngPioneerTemp = string.Empty;
                string youngLeagueTemp = string.Empty;
                string communistPartyTemp = string.Empty;
                DateTime youngPioneerDate;
                DateTime youngLeagueDate;
                DateTime communistPartyDate;

                if (_globalInfo.AppliedLevel <= SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    youngPioneerTemp = GetCellString(sheet, youngPioneerJoinedDate + rowIndex);
                }
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY
                    || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    youngLeagueTemp = GetCellString(sheet, youngLeagueJoinedDate + rowIndex);
                }
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    communistPartyTemp = GetCellString(sheet, communistPartyJoinedDate + rowIndex);
                }
                #region // kiểm tra thông tin ngày đội cấp 1, 2, 3
                if (_globalInfo.AppliedLevel <= SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    if (DateTime.TryParse(youngPioneerTemp, culture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out youngPioneerDate))
                    {
                        pupil.YoungPioneerJoinedDate = youngPioneerDate;
                        pupil.YoungPioneerJoinedDateStr = youngPioneerDate.ToShortDateString();
                        if (pupil.YoungPioneerJoinedDate.Value > DateTime.Now.Date)
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đội phải nhỏ hơn ngày hiện tại.")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.YoungPioneerJoinedDate.Value < new DateTime(1900, 1, 1))
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đội phải lớn hơn hoặc bằng ngày 01/01/1900.")).Append(SEMICOLON_SPACE);
                        }
                    }
                    else if (youngPioneerTemp != string.Empty)
                    {
                        ErrorMessage.Append("Ngày vào đội không đúng định dạng").Append(SEMICOLON_SPACE);
                    }
                }
                #endregion
                #region //kiểm tra ngày vào đoàn cấp 2,3
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    if (DateTime.TryParse(youngLeagueTemp, culture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out youngLeagueDate))
                    {
                        pupil.YoungLeagueJoinedDate = youngLeagueDate;
                        pupil.YoungLeagueJoinedDateStr = youngLeagueDate.ToShortDateString();
                        if (pupil.YoungLeagueJoinedDate.Value > DateTime.Now.Date)
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đoàn phải nhỏ hơn ngày hiện tại.")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.YoungLeagueJoinedDate.Value < new DateTime(1900, 1, 1))
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đoàn phải lớn hơn hoặc bằng ngày 01/01/1900.")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.YoungPioneerJoinedDate >= youngLeagueDate)//Nhập ngày vào đội > ngày vào đoàn
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đoàn phải lớn hơn ngày vào đội.")).Append(SEMICOLON_SPACE);
                        }
                    }
                    else if (youngLeagueTemp != string.Empty)
                    {
                        ErrorMessage.Append("Ngày vào đoàn không đúng định dạng").Append(SEMICOLON_SPACE);
                    }
                }
                #endregion
                #region //kiểm tra ngày vào đảng cấp 3
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    if (DateTime.TryParse(communistPartyTemp, culture, DateTimeStyles.AssumeLocal | DateTimeStyles.AllowWhiteSpaces, out communistPartyDate))
                    {
                        pupil.CommunistPartyJoinedDate = communistPartyDate;
                        pupil.CommunistPartyJoinedDateStr = communistPartyDate.ToShortDateString();
                        if (pupil.CommunistPartyJoinedDate.Value > DateTime.Now.Date)
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đảng phải nhỏ hơn ngày hiện tại.")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.CommunistPartyJoinedDate.Value < new DateTime(1900, 1, 1))
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đảng phải lớn hơn hoặc bằng ngày 01/01/1900.")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.YoungPioneerJoinedDate >= communistPartyDate)//Ngày vào đội > ngày vào đảng
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đảng phải lớn hơn ngày vào đội")).Append(SEMICOLON_SPACE);
                        }
                        else if (pupil.YoungLeagueJoinedDate >= communistPartyDate)//Ngày vào đoàn > ngày vào đảng
                        {
                            ErrorMessage.Append(Res.Get("Ngày vào đảng phải lớn hơn ngày vào đoàn")).Append(SEMICOLON_SPACE);
                        }
                    }
                    else if (communistPartyTemp != string.Empty)
                    {
                        ErrorMessage.Append("Ngày vào đảng không đúng định dạng").Append(SEMICOLON_SPACE);
                    }
                }
                #endregion
                // Dân tộc
                pupil.EthnicName = GetCellString(sheet, ethnicNameCol + rowIndex);
                pupil.EthnicID = (int?)SetNulableFromList(listEthnic, pupil.EthnicName);//mac dinh dan toc kinh
                if (pupil.EthnicID == -1 && pupil.EthnicName != null && !string.IsNullOrEmpty(pupil.EthnicName))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_EthnicExisted")).Append(SEMICOLON_SPACE);
                }
                // Tôn giáo
                pupil.ReligionName = GetCellString(sheet, religionNameCol + rowIndex);
                pupil.ReligionID = (int?)SetNulableFromList(listReligion, pupil.ReligionName);
                if (pupil.ReligionID == -1 && pupil.ReligionName != null && !string.IsNullOrEmpty(pupil.ReligionName))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_ReligionExisted")).Append(SEMICOLON_SPACE);
                }
                // Đối tượng chính sách
                pupil.PolicyTargetName = GetCellString(sheet, policyTargetNameCol + rowIndex);
                pupil.PolicyTargetID = (int?)SetNulableFromList(listPolicyTarget, pupil.PolicyTargetName);
                // Chế độ chính sách
                if (!string.IsNullOrEmpty(pupil.PolicyTargetName) && pupil.PolicyTargetID > 0)
                {
                    pupil.PolicyRegimeName = GetCellString(sheet, policyRegimeCol + rowIndex);
                    if (!string.IsNullOrEmpty(pupil.PolicyRegimeName))
                    {
                        pupil.PolicyRegimeID = (int?)SetNulableFromList(lstPolicyRegimes, pupil.PolicyRegimeName);
                    }
                }
                //Ho tro chi phi học tap
                pupil.IsSupportForLearning = GetCellString(sheet, isSupportForLearningCol + rowIndex).ToUpper() == "X" ? true : false;
                //tai dinh cu
                pupil.IsResettlementTarget = GetCellString(sheet, isResettlementTargetCol + rowIndex).ToUpper() == "X" ? true : false;
                // Khu vực
                pupil.AreaName = GetCellString(sheet, areaNameCol + rowIndex);
                pupil.AreaID = (int?)SetNulableFromList(lstArea, pupil.AreaName);
                //dien hoc sinh;
                pupil.ClassTypeName = GetCellString(sheet, classTypeCol + rowIndex);
                pupil.ClassType = SetNulableFromList(CommonList.ClassType(), GetCellString(sheet, classTypeCol + rowIndex));
                if (pupil.ClassTypeName != "" && pupil.ClassType == -1)
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidClassType")).Append(SEMICOLON_SPACE);
                }
                //Loại khuyết tật
                pupil.DisabilityTypeName = GetCellString(sheet, disabilityTypeCol + rowIndex);
                if (!string.IsNullOrEmpty(pupil.DisabilityTypeName))
                {
                    pupil.DisabledTypeID = (int?)SetNulableFromList(lstDisabilityType, pupil.DisabilityTypeName);
                }

                if (!string.IsNullOrEmpty(pupil.DisabilityTypeName) && pupil.DisabledTypeID > 0)
                {
                    pupil.IsDisabled = true;
                }
                else
                {
                    pupil.IsDisabled = false;
                }
                // Số điện thoại
                pupil.Mobile = GetCellString(sheet, mobileCol + rowIndex);
                // CMND
                pupil.IdentityNumber = GetCellString(sheet, identifyNumberCol + rowIndex);
                // Email
                pupil.Email = GetCellString(sheet, emailCol + rowIndex);
                #region //check Email
                if (!string.IsNullOrEmpty(pupil.Email))
                {
                    string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Match match = Regex.Match(pupil.Email.Trim(), pattern, RegexOptions.IgnoreCase);

                    if (!match.Success)
                        ErrorMessage.Append(Res.Get("Common_Validate_Email")).Append(SEMICOLON_SPACE);
                    else if (pupil.Email.Length > 50)
                    {
                        ErrorMessage.Append(string.Format(Res.Get("Common_Label_MaxlengthEmail"), "")).Append(SEMICOLON_SPACE);
                    }
                }
                #endregion
                // nhóm máu
                pupil.BloodTypeName = GetCellString(sheet, bloodTypeNameCol + rowIndex);
                pupil.BloodType = SetBloodType(pupil.BloodTypeName);
                // học chương trình giá dục của bộ
                pupil.IsUsedMoetProgram = GetCellString(sheet, IsUsedMoetProgram + rowIndex).ToUpper() == "X" ? true : false;
                // Biết bơi
                pupil.IsSwimming = GetCellString(sheet, IsSwimmingCol + rowIndex).ToUpper() == "X" ? true : false;
                // Mã mới học sinh
                pupil.PupilCodeNew = GetCellString(sheet, pupilCodeNew + rowIndex);
                // Trường hiện tại
                pupil.CurrentSchoolID = _globalInfo.SchoolID.Value;
                #endregion

                #region // Thông tin của cha
                // Họ tên cha 
                pupil.FatherFullName = GetCellString(sheet, fatherFullNameCol + rowIndex);
                // Năm sinh của cha
                int result = 0;
                DateTime? fatherBirthDate = new DateTime?();
                string fatherDateTemp = "";
                string fatherDate = GetCellString(sheet, fatherBirthDateCol + rowIndex);

                if ((int.TryParse(fatherDate, out result) == false || fatherDate.Trim().Length != 4) && fatherDate.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidFatherBirthDate")).Append(SEMICOLON_SPACE);
                }

                if (fatherDate != null && fatherDate.Trim() != "" && int.TryParse(fatherDate, out result) && fatherDate.Trim().Length == 4)
                {
                    fatherDateTemp = "01/01/" + fatherDate;
                }
                if (fatherDateTemp != null && fatherDateTemp.Trim() != "")
                {
                    fatherBirthDate = DateTime.Parse(fatherDateTemp);
                }
                pupil.FatherBirthDateStr = fatherDate;
                pupil.FatherBirthDate = fatherBirthDate;
                // Nghề nghiệp của cha
                pupil.FatherJob = GetCellString(sheet, fatherJobCol + rowIndex);
                // SDT của cha
                pupil.FatherMobile = GetCellString(sheet, fatherMobileCol + rowIndex);
                // Email của cha
                pupil.FatherEmail = GetCellString(sheet, fatherEmailCol + rowIndex);
                #region //check Email Father
                if (!string.IsNullOrEmpty(pupil.FatherEmail))
                {
                    string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Match match = Regex.Match(pupil.FatherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                    if (!match.Success)
                        ErrorMessage.Append(string.Format(Res.Get("Common_Validate_EmailErr"), "cha")).Append(SEMICOLON_SPACE);
                    else if (pupil.FatherEmail.Length > 50)
                    {
                        ErrorMessage.Append(string.Format(Res.Get("Common_Label_MaxlengthEmail"), "cha")).Append(SEMICOLON_SPACE);
                    }
                }
                #endregion
                // Bố là dân tộc
                pupil.IsMinorityFather = GetCellString(sheet, isMinorityFatherCol + rowIndex).ToUpper() == "X" ? true : false;
                #endregion

                #region // Thông tin của mẹ
                // Họ và tên mẹ
                pupil.MotherFullName = GetCellString(sheet, motherFullNameCol + rowIndex);
                // Năm sinh của mẹ
                DateTime? motherBirthDate = new DateTime?();
                string motherDateTemp = "";
                string motherDate = GetCellString(sheet, motherBirthDateStrCol + rowIndex);

                if ((int.TryParse(motherDate, out result) == false || motherDate.Trim().Length != 4) && motherDate.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidMotherBirthDate")).Append(SEMICOLON_SPACE);
                }
                if (motherDate != null && motherDate.Trim() != "" && int.TryParse(motherDate, out result) && motherDate.Trim().Length == 4)
                {
                    motherDateTemp = "01/01/" + motherDate;
                }
                if (motherDateTemp != null && motherDateTemp.Trim() != "")
                {
                    motherBirthDate = DateTime.Parse(motherDateTemp);
                }
                pupil.MotherBirthDateStr = motherDate;
                pupil.MotherBirthDate = motherBirthDate;
                // Nghề nghiệp của mẹ
                pupil.MotherJob = GetCellString(sheet, motherJobCol + rowIndex);
                // SDT của mẹ
                pupil.MotherMobile = GetCellString(sheet, motherMobileCol + rowIndex);
                // Email của mẹ
                pupil.MotherEmail = GetCellString(sheet, motherEmailCol + rowIndex);
                #region //check Email Mother
                if (!string.IsNullOrEmpty(pupil.MotherEmail))
                {
                    string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Match match = Regex.Match(pupil.MotherEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                    if (!match.Success)
                        ErrorMessage.Append(string.Format(Res.Get("Common_Validate_EmailErr"), "mẹ")).Append(SEMICOLON_SPACE);
                    else if (pupil.MotherEmail.Length > 50)
                    {
                        ErrorMessage.Append(string.Format(Res.Get("Common_Label_MaxlengthEmail"), "mẹ")).Append(SEMICOLON_SPACE);
                    }
                }
                #endregion
                // Mẹ là dân tộc
                pupil.IsMinorityMother = GetCellString(sheet, isMinorityMotherCol + rowIndex).ToUpper() == "X" ? true : false;
                #endregion

                #region // Thông tin của người bảo hộ
                // Họ tên người bảo hộ
                pupil.SponsorFullName = GetCellString(sheet, sponsorFullNameCol + rowIndex);
                // Năm sinh người bảo hộ
                DateTime? sponsorBirthDate = new DateTime?();
                string sponsorDateTemp = "";
                string sponsorDate = GetCellString(sheet, sponsorBirthDateStrCol + rowIndex);
                if ((int.TryParse(sponsorDate, out result) == false || sponsorDate.Trim().Length != 4) && sponsorDate.Trim() != "")
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidSponsorBirthDate")).Append(SEMICOLON_SPACE);
                }
                if (sponsorDate != null && sponsorDate.Trim() != "" && int.TryParse(sponsorDate, out result) && sponsorDate.Trim().Length == 4)
                {
                    sponsorDateTemp = "01/01/" + sponsorDate;
                }
                if (sponsorDateTemp != null && sponsorDateTemp.Trim() != "")
                {
                    sponsorBirthDate = DateTime.Parse(sponsorDateTemp);
                }
                pupil.SponsorBirthDateStr = sponsorDate;
                pupil.SponsorBirthDate = sponsorBirthDate;
                // NGhề nghiệp người bảo hộ
                pupil.SponsorJob = GetCellString(sheet, sponsorJobCol + rowIndex);
                // SDt người bảo hộ
                pupil.SponsorMobile = GetCellString(sheet, sponsorMobilCol + rowIndex);
                // Email người bảo hộ
                pupil.SponsorEmail = GetCellString(sheet, sponsorEmailCol + rowIndex);
                #region //check Email Sponsor
                if (!string.IsNullOrEmpty(pupil.SponsorEmail))
                {
                    string pattern = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
                    Match match = Regex.Match(pupil.SponsorEmail.Trim(), pattern, RegexOptions.IgnoreCase);

                    if (!match.Success)
                        ErrorMessage.Append(string.Format(Res.Get("Common_Validate_EmailErr"), "người bảo hộ")).Append(SEMICOLON_SPACE);
                    else if (pupil.SponsorEmail.Length > 50)
                    {
                        ErrorMessage.Append(string.Format(Res.Get("Common_Label_MaxlengthEmail"), "người bảo hộ")).Append(SEMICOLON_SPACE);
                    }
                }
                if (!string.IsNullOrEmpty(pupil.StorageNumber) && pupil.StorageNumber.Length > 30)
                {
                    ErrorMessage.Append(Res.Get("Common_Label_Maxlength_StorageNumber")).Append(SEMICOLON_SPACE);
                }
                #endregion
                #endregion


                #region
                #region validate du lieu Update hoặc Insert
                if (isUpdatePupil)
                {
                    if (lstPP.Any(o => o.PupilCode == pupil.PupilCode))
                    {
                        PupilProfile pupilProfile = lstPP.Where(o => o.PupilCode == pupil.PupilCode).OrderBy(o => o.EnrolmentDate).LastOrDefault();
                        List<PupilOfClassBO> lstpoc_temp = lstpoc.Where(o => o.PupilID == pupilProfile.PupilProfileID).ToList();
                        if (lstpoc_temp.Count() >= 2)
                        {
                            PupilOfClassBO pocAssignMax = lstpoc_temp.OrderBy(o => o.AssignedDate).LastOrDefault();
                            if (pupil.EnrolmentDate >= pocAssignMax.AssignedDate)
                            {
                                ErrorMessage.Append(Res.Get("PupilProfile_Validate_AssignDate")).Append(SEMICOLON_SPACE);
                            }
                        }
                        if (lstpoc_temp.Any(pp => pp.Status != GlobalConstants.PUPIL_STATUS_STUDYING && pp.ClassID == classID))
                        {
                            ErrorMessage.Append("Học sinh ở trạng thái khác đang học ").Append(SEMICOLON_SPACE);
                        }
                        if (!string.IsNullOrEmpty(pupil.PupilCodeNew))
                        {
                            lstPupilCodeNew.Add(pupil.PupilCodeNew);
                            if (lstPP.Any(o => o.PupilCode == pupil.PupilCodeNew))
                            {
                                ErrorMessage.Append(string.Format("Mã học sinh {0} đã tồn tại.", pupil.PupilCodeNew)).Append(SEMICOLON_SPACE);
                            }
                            if (lstPupilCodeNew.Count(p => p == pupil.PupilCodeNew) > 1)
                            {
                                ErrorMessage.Append(string.Format("Mã học sinh mới {0} không được trùng nhau.", pupil.PupilCodeNew)).Append(SEMICOLON_SPACE);
                            }
                        }

                        if (pupilProfile.CurrentClassID != classID)
                        {
                            Dictionary<string, object> dicSearch = new Dictionary<string, object>()
                        {
                            {"SchoolID",_globalInfo.SchoolID},
                            {"AcademicYearID",_globalInfo.AcademicYearID},
                            {"Last2digitNumberSchool",modSchoolID},
                            {"Year",year}
                        };
                            IQueryable<MarkRecord> lstMarkRecord = null;
                            IQueryable<JudgeRecord> lstJudgeRecord = null;
                            IQueryable<PupilRanking> lstPupilRanking = null;
                            IQueryable<SummedUpRecord> lstSUR = null;
                            IQueryable<PupilAbsence> lstPA = null;
                            IQueryable<PupilFault> lstPF = null;
                            if (chkCheck)
                            {
                                //list diem mon tinh diem cua hoc sinh
                                lstMarkRecord = MarkRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch);
                                //list diem mon nhan xet cua hoc sinh
                                lstJudgeRecord = JudgeRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch);
                                //list danh gia xep loai
                                lstPupilRanking = PupilRankingBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch);
                                //list TBM
                                lstSUR = SummedUpRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch);
                                //list diem danh
                                lstPA = PupilAbsenceBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch);
                                //list vi pham
                                lstPF = PupilFaultBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch);
                                chkCheck = false;
                            }

                            bool MarkRecordContrainst = lstMarkRecord != null && lstMarkRecord.Where(p => p.PupilID == pupilProfile.PupilProfileID && p.ClassID == pupilProfile.CurrentClassID).Any();
                            if (MarkRecordContrainst)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupil_Label_MarkRecordExist")).Append(SEMICOLON_SPACE);
                            }
                            bool JudgeRecordContrainst = lstJudgeRecord != null && lstJudgeRecord.Where(p => p.PupilID == pupilProfile.PupilProfileID && p.ClassID == pupilProfile.CurrentClassID).Any();
                            if (!MarkRecordContrainst && JudgeRecordContrainst)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupil_Label_MarkRecordExist")).Append(SEMICOLON_SPACE);
                            }
                            bool PupilRankingContrainst = lstPupilRanking != null && lstPupilRanking.Where(p => p.PupilID == pupilProfile.PupilProfileID && p.ClassID == pupilProfile.CurrentClassID).Count() > 0;
                            if (PupilRankingContrainst)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupil_Label_PupilRankingExist")).Append(SEMICOLON_SPACE);
                            }
                            bool SummedUpRecordContrainst = lstSUR != null && lstSUR.Where(p => p.PupilID == pupilProfile.PupilProfileID && p.ClassID == pupilProfile.CurrentClassID).Count() > 0;
                            if (SummedUpRecordContrainst)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupil_Label_SummedUpRecordExist")).Append(SEMICOLON_SPACE);
                            }
                            bool PupilAbsenceContrainst = lstPA != null && lstPA.Where(p => p.PupilID == pupilProfile.PupilProfileID && p.ClassID == pupilProfile.CurrentClassID).Count() > 0;
                            if (PupilAbsenceContrainst)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupil_Label_PupilAbsenceExist")).Append(SEMICOLON_SPACE);
                            }
                            bool PupilFaultContrainst = lstPF != null && lstPF.Where(p => p.PupilID == pupilProfile.PupilProfileID && p.ClassID == pupilProfile.CurrentClassID).Count() > 0;
                            if (PupilFaultContrainst)
                            {
                                ErrorMessage.Append(Res.Get("ImportPupil_Label_PupilFaultExist")).Append(SEMICOLON_SPACE);
                            }
                        }
                    }
                    else
                    {
                        ErrorMessage.Append(Res.Get("Học sinh không tồn tại để cập nhật thông tin.")).Append(SEMICOLON_SPACE);
                        pupil.IsValid = false;
                        pupil.ErrorMessage += ErrorMessage.ToString();
                        listPupil.Add(pupil);
                        rowIndex++;
                        tempCount++;
                        continue;
                    }
                }
                else if (isAddPupil)
                {
                    pupil.PupilCodeNew = string.Empty;
                    if (lstPP.Any(o => o.PupilCode == pupil.PupilCode))
                    {
                        ErrorMessage.Append(Res.Get("Đã tồn tại học sinh này, Thầy/cô không thể thêm."));
                        pupil.IsValid = false;
                        pupil.ErrorMessage += ErrorMessage.ToString();
                        listPupil.Add(pupil);
                        rowIndex++;
                        tempCount++;
                        continue;
                    }
                }
                #endregion

                #region AnhVD9 20140918 - Kiểm tra không cho Import trùng mã trong năm học trước. Nếu mã đã có trong năm học này thì vẫn cho cập nhật
                if (!string.IsNullOrWhiteSpace(pupil.PupilCode))
                {
                    if (lstPupilCodeOtherYear.Contains(pupil.PupilCode)) // năm học cũ đã tồn tại mã
                    {
                        if (!lstPupilCodeCurrentYear.Contains(pupil.PupilCode)) // năm học hiện tại chưa có mã này
                        {
                            ErrorMessage.Append(Res.Get("ImportPupill_PupilCode_InOtherYear")).Append(SEMICOLON_SPACE);
                        }
                    }
                }
                #endregion

                #region // isAutoGenCode

                AutoGenericCode objAutoGenericCode = new AutoGenericCode();             
                var tempt = lstEducationLevelWithClass.Where(x => x.ClassName.ToUpper().Equals(className.ToUpper())).FirstOrDefault();
                if (tempt == null && educationLevelID != 0)
                {
                    ErrorMessage.Append(className + " không thuộc khối đã chọn");
                    pupil.IsValid = false;
                    pupil.ErrorMessage += ErrorMessage.ToString();
                    listPupil.Add(pupil);
                    rowIndex++;
                    tempCount++;
                    continue;
                }
                else if (tempt != null)
                {
                    if (isAutoGenCode)
                    {
                        objAutoGenericCode = lstOutGenericCode.Where(x => x.EducationLevelID == tempt.EducationLevelID).FirstOrDefault();
                        originalPupilCode = objAutoGenericCode.OriginalPupilCode;
                        maxOrderNumber = objAutoGenericCode.MaxOrderNumber;
                        numberLength = objAutoGenericCode.NumberLength;
                        tempCount = objAutoGenericCode.Index;
                        string pupilCode = originalPupilCode + ToStringWithFixedLength(maxOrderNumber + 1 + tempCount, numberLength);
                        if (isSchoolModify)
                        {
                            if (string.IsNullOrEmpty(pupil.PupilCode))
                            {
                                pupil.PupilCode = pupilCode;
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(pupil.PupilCode))
                            {
                                pupil.PupilCode = pupilCode;
                            }
                            else
                            {
                                int temp = 0;
                                if (pupil.PupilCode.Length - numberLength > 0)
                                {
                                    int.TryParse(pupil.PupilCode.Substring((pupil.PupilCode.Length - numberLength), numberLength), out temp);
                                }
                                if (!pupil.PupilCode.StartsWith(originalPupilCode) || pupil.PupilCode.Length != pupilCode.Length || temp == 0)
                                {
                                    ErrorMessage.Append(Res.Get("ImportPupil_Label_OriginalPupilCode")).Append(SEMICOLON_SPACE);
                                }
                            }
                        }
                    }
                }

                #endregion

                ValidateImportedPupil(pupil);

                if (listPupil.Any(u => u.PupilCode.ToLower().Equals(pupil.PupilCode.ToLower())))
                {
                    ErrorMessage.Append(string.Format(Res.Get("ImportPupil_Label_PupilCodeExisted"), pupil.PupilCode)).Append(SEMICOLON_SPACE);
                }

                if (pupil.Index > 0 && pupil.CurrentClassID > 0 && listPupil.Any(u => u.Index == pupil.Index && u.CurrentClassID == pupil.CurrentClassID))
                {
                    ErrorMessage.Append(string.Format(Res.Get("ImportPupil_Label_IndexClassExisted"), pupil.ClassName, pupil.Index)).Append(SEMICOLON_SPACE);
                }

                if (!string.IsNullOrEmpty(pupil.ClassName) && !listClass.Any(u => u.key.Equals(pupil.CurrentClassID.ToString())))
                {
                    ErrorMessage.Append(string.Format(Res.Get("ImportPupil_Label_ClassNotExist"), pupil.ClassName)).Append(SEMICOLON_SPACE);
                }

                if (pupil.PolicyTargetID.HasValue && !listPolicyTarget.Any(o => pupil.PolicyTargetID != null && o.key == pupil.PolicyTargetID.Value.ToString()))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidPolicyTargetName")).Append(SEMICOLON_SPACE);
                }

                if (string.IsNullOrEmpty(pupil.ClassName))
                {
                    ErrorMessage.Append(Res.Get("ImportPupil_Label_InvalidClassNameNull")).Append(SEMICOLON_SPACE);
                }

                //Kiểm tra thôn xóm có chưa?
                if (checkDistrict == true && checkCommune == true && pupil.IsValid == true)
                {
                    if (pupil.CommuneID != null && pupil.DistrictID != null && pupil.ProvinceID != null)
                    {
                        bool isCheck = listPupilTemp.Where(o => o.VillageName.Trim().ToUpper() == pupil.VillageName.Trim().ToUpper() && o.CommuneID == pupil.CommuneID).Count() > 0 ? true : false;

                        if (pupil.VillageID == null && pupil.VillageName != null && pupil.VillageName.Trim() != "" && !isCheck)
                        {
                            bool isCheck1 = lstVillage.Where(o => o.VillageName.Trim().ToUpper() == pupil.VillageName.Trim().ToUpper() && o.CommuneID == pupil.CommuneID).Count() > 0 ? true : false;
                            if (!isCheck1)
                            {
                                pupil.checkInsertVilliage = true;
                                //them doi tuong thon xom de insert
                                Village objvl = new Village();
                                objvl.VillageName = pupil.VillageName;
                                objvl.ShortName = pupil.VillageName;
                                objvl.CommuneID = pupil.CommuneID;
                                objvl.CreateDate = DateTime.Now;
                                objvl.IsActive = true;
                                lstVillageImport.Add(objvl);
                            }
                        }
                    }
                }
                #endregion

                // Gán lỗi
                if (ErrorMessage.Length > 0)
                {
                    pupil.IsValid = false;
                    pupil.ErrorMessage += ErrorMessage.ToString();
                }
                if (pupil.IsValid)
                    listPupilTemp.Add(pupil);
                else // Xóa bỏ ký tự ; cuối cùng
                {
                    if (!String.IsNullOrEmpty(pupil.ErrorMessage) && pupil.ErrorMessage.Length > 2)
                    {
                        pupil.ErrorMessage = pupil.ErrorMessage.Remove(pupil.ErrorMessage.Length - 2);
                    }
                }

                listPupil.Add(pupil);
                rowIndex++;

                if (tempt != null)
                {
                    objAutoGenericCode.Index++;
                    lstOutGenericCode.RemoveAll(x => x.EducationLevelID == objAutoGenericCode.EducationLevelID);
                    lstOutGenericCode.Add(objAutoGenericCode);
                }
                tempCount++;
            }

            return listPupil;
        }

        private Stream SetDataToFileExcel(int? EducationLevelID, ref string ReportName, bool isErrFile = false)
        {
            EducationLevel edu = EducationLevelID > 0 ? EducationLevelBusiness.Find(EducationLevelID) : new EducationLevel();
            int year = AcademicYearBusiness.Find(_globalInfo.AcademicYearID).Year;

            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.IMPORT_THONGTINHOCSINH_2018);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            ReportName = ReportUtils.RemoveSpecialCharacters(reportDefinition.OutputNamePattern) + "." + reportDefinition.OutputFormat;

            IVTWorkbook book = VTExport.OpenWorkbook(template);
            IVTWorksheet sheet = book.GetSheet(1);
            IVTWorksheet refSheet = book.GetSheet(2); // Tỉnh thành
            IVTWorksheet refSheetDis = book.GetSheet(3); // Quận Huyện
            IVTWorksheet refSheetComm = book.GetSheet(4); // Xã phường

            IVTWorksheet refTemp = book.GetSheet(5);
            IVTWorksheet refHDC = book.GetSheet(6);

            IVTWorksheet sheetProvince = book.CopySheetToLast(refSheet, "C3");
            IVTWorksheet sheetDistrict = book.CopySheetToLast(refSheetDis, "D3");
            IVTWorksheet sheetCommune = book.CopySheetToLast(refSheetComm, "E3");

            IVTRange rangeProvince = refSheet.GetRange("A4", "C4");
            IVTRange rangeDistrict = refSheetDis.GetRange("A4", "D4");

            IVTRange rangeProOfCommune = refSheetComm.GetRange("A4", "E4");
            IVTRange rangeCommune = refSheetComm.GetRange("A5", "E5");

            IVTWorksheet sheetHDC = book.CopySheetToLast(refHDC, "A20");

            #region fill data for sheet
            Dictionary<string, object> dicSheet = new Dictionary<string, object>();
            dicSheet["SuperVisingDeptName"] = _globalInfo.SuperVisingDeptName;
            dicSheet["SchoolName"] = _globalInfo.SchoolName.ToUpper();
            dicSheet["DateTime"] = string.Format(Res.Get("Common_Label_DayMonYear"), DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            //dicSheet["EducationLevel"] = edu.Resolution;
            sheet.FillVariableValue(dicSheet);
            #endregion

            #region fill data for combobox values
            Dictionary<string, object> dicRef = new Dictionary<string, object>();
            // Khối - Lớp
            List<object> listClass = new List<object>();
            ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value,
                                                new Dictionary<string, object>
                                                    {
                                                        {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                                                        {"EducationLevelID", EducationLevelID},
                                                        {"IsActive", true}
                                                    }).Where(x => x.EducationLevel.Grade == _globalInfo.AppliedLevel).ToList().ForEach(u => listClass.Add(new { u.DisplayName }));
            dicRef["Classes"] = listClass;

            // Diện học sinh
            List<object> listClassType = new List<object>();
            listClassType.Add(new { Value = "Nội trú" });
            listClassType.Add(new { Value = "Bán trú" });
            listClassType.Add(new { Value = "Nội trú dân nuôi" });
            listClassType.Add(new { Value = "Bán trú dân nuôi" });
            dicRef["ClassTypes"] = listClassType;

            // Hình thức trúng tuyển
            List<object> listEnrolmentType = new List<object>();
            CommonList.EnrolmentTypeAvailable().ForEach(u => listEnrolmentType.Add(new { Value = u.value }));
            dicRef["EnrolmentTypes"] = listEnrolmentType;


            // Sheet Tỉnh/Thành
            List<Province> lstPro = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.ProvinceName).ToList();
            List<object> listProvince_Temp = new List<object>();
            int i = 1;
            int startRow = 4;
            foreach (var pr in lstPro)
            {
                sheetProvince.CopyPasteSameSize(rangeProvince, startRow, 1);
                listProvince_Temp.Add(new Dictionary<string, object>
                    {
                        {"Order", i++},
                        {"ProvinceCode", pr.ProvinceCode},
                        {"ProvinceName", pr.ProvinceName},

                    });
                startRow++;
            }
            // Fill Tỉnh/Thành
            Dictionary<string, object> dicSheetPro = new Dictionary<string, object>();
            dicSheetPro.Add("list", listProvince_Temp);
            sheetProvince.FillVariableValue(dicSheetPro);


            // Sheet Quận/Huyện           
            var lstDis = (from p in DistrictBusiness.Search(new Dictionary<string, object> { { "IsActive", true } })
                          join q in ProvinceBusiness.All on p.ProvinceID equals q.ProvinceID
                          select new
                          {
                              DistrictID = p.DistrictID,
                              DistrictName = p.DistrictName,
                              DistrictCode = p.DistrictCode,
                              ProvinceID = q.ProvinceID,
                              ProvinceName = q.ProvinceName
                          }).ToList();
            // Fill Quận/Huyện
            int startRow_Dis = 4;
            int order = 1;
            int beginRow_Dis = 4;
            foreach (var pr in lstPro)
            {
                int rowFirstByDis = 1;
                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count();
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        sheetDistrict.CopyPasteSameSize(rangeDistrict, startRow_Dis, 1);
                        sheetDistrict.SetCellValue(startRow_Dis, 1, order++);
                        if (rowFirstByDis == 1)
                        {
                            sheetDistrict.SetCellValue(startRow_Dis, 2, dis.ProvinceName);
                        }
                        sheetDistrict.SetCellValue(startRow_Dis, 3, dis.DistrictCode);
                        sheetDistrict.SetCellValue(startRow_Dis, 4, dis.DistrictName);
                        if (rowFirstByDis == numberOfDis)
                        {
                            IVTRange range = sheetDistrict.GetRange(beginRow_Dis, 2, beginRow_Dis + numberOfDis - 1, 2);
                            range.Merge();
                        }
                        startRow_Dis++;
                        rowFirstByDis++;
                    }
                    beginRow_Dis += numberOfDis;
                }
            }


            // Sheet Xã/Phường
            var lstCommune = CommuneBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).ToList();
            // Fill Xã/Phường
            int numProvince = 1;
            int startRow_Pro = 4;

            int startRow_Com = 5;
            order = 1;

            int beginRow_Com = 5;
            foreach (var pr in lstPro)
            {
                // Thông tin tỉnh
                sheetCommune.CopyPasteSameSize(rangeProOfCommune, startRow_Pro, 1);
                sheetCommune.SetCellValue(startRow_Pro, 1, numProvince++);
                sheetCommune.SetCellValue(startRow_Pro, 2, pr.ProvinceName);

                var lstDisByPro = lstDis.Where(o => o.ProvinceID == pr.ProvinceID).OrderBy(o => o.DistrictName).ToList();
                int numberOfDis = lstDisByPro.Count;
                if (lstDisByPro != null && numberOfDis > 0)
                {
                    foreach (var dis in lstDisByPro)
                    {
                        int rowFirstOfDis = 1;
                        var lstCommByDis = lstCommune.Where(o => o.DistrictID == dis.DistrictID).OrderBy(o => o.CommuneName).ToList();
                        int numOfComm = lstCommByDis.Count;
                        if (numOfComm > 0)
                        {
                            foreach (var cm in lstCommByDis)
                            {
                                sheetCommune.CopyPasteSameSize(rangeCommune, startRow_Com, 1);
                                sheetCommune.SetCellValue(startRow_Com, 1, order++);
                                if (rowFirstOfDis == 1)
                                {
                                    sheetCommune.SetCellValue(startRow_Com, 2, dis.DistrictCode);
                                    sheetCommune.SetCellValue(startRow_Com, 3, dis.DistrictName);
                                }
                                sheetCommune.SetCellValue(startRow_Com, 4, cm.CommuneCode);
                                sheetCommune.SetCellValue(startRow_Com, 5, cm.CommuneName);
                                if (rowFirstOfDis == numOfComm)
                                {
                                    sheetCommune.GetRange(beginRow_Com, 2, beginRow_Com + numOfComm - 1, 2).Merge();
                                    sheetCommune.GetRange(beginRow_Com, 3, beginRow_Com + numOfComm - 1, 3).Merge();
                                }
                                startRow_Com++;
                                rowFirstOfDis++;
                            }
                        }
                        beginRow_Com += numOfComm; // dòng tiến hành merge
                    }
                }
                // set lại biến đếm cho tỉnh tiếp theo
                startRow_Pro += order;
                startRow_Com++;
                beginRow_Com++;
                order = 1;
            }

            // Đối tượng chính sách  
            List<object> listPolicyTarget = new List<object>();
            var lstPOT = PolicyTargetBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            lstPOT.ForEach(u => listPolicyTarget.Add(new { u.Resolution }));
            dicRef["PolicyTargets"] = listPolicyTarget;
            // Chế độ chính sách
            List<object> lstPolicyRegimes = new List<object>();
            var lstPR = PolicyRegimeBusiness.All.Where(x => x.IsActive).OrderBy(o => o.Resolution).ToList();
            lstPR.ForEach(u => lstPolicyRegimes.Add(new { u.Resolution }));
            dicRef["PolicyRegimes"] = lstPolicyRegimes;
            // Khu vực
            List<object> lstAreas = new List<object>();
            var listArea = AreaBusiness.All.Where(x => x.IsActive.HasValue && x.IsActive.Value == true).ToList();
            listArea.ForEach(u => lstAreas.Add(new { u.AreaName }));
            dicRef["Areas"] = lstAreas;
            // Loại khuyết tật 
            List<object> lstDisabilityType = new List<object>();
            var listDisabilityType = DisabledTypeBusiness.All.Where(x => x.IsActive).OrderBy(o => o.Resolution).ToList();
            listDisabilityType.ForEach(u => lstDisabilityType.Add(new { u.Resolution }));
            dicRef["DisabilityType"] = lstDisabilityType;

            // Dân tộc
            List<object> listEthnic = new List<object>();
            List<Ethnic> lst1 = EthnicBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.EthnicName).ToList();
            Ethnic e1 = lst1.FirstOrDefault(o => o.EthnicName.Contains("Kinh"));
            lst1.Remove(e1);
            lst1.Insert(0, e1);
            lst1.ForEach(u => listEthnic.Add(new { u.EthnicName }));
            dicRef["Ethnics"] = listEthnic;

            // Tôn giáo
            List<object> listReligionObj = new List<object>();
            List<Religion> lstReligion = ReligionBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
            Religion r1 = lstReligion.FirstOrDefault(o => o.Resolution.Contains("Không"));
            lstReligion.Remove(r1);
            lstReligion.Insert(0, r1);
            lstReligion.ForEach(u => listReligionObj.Add(new { u.Resolution }));
            dicRef["Religions"] = listReligionObj;

            List<object> listBloodType = new List<object>();
            CommonList.BloodType().ForEach(u => listBloodType.Add(new { Value = u.value }));
            dicRef["BloodTypes"] = listBloodType;

            List<object> listForeignLanguageTraining = new List<object>();
            CommonList.ForeignLanguageTraining().ForEach(u => listForeignLanguageTraining.Add(new { Value = u.value }));
            dicRef["ForeignLanguageTrainings"] = listForeignLanguageTraining;

            List<object> listProfileStatus = new List<object>();
            CommonList.PupilStatus().Where(u => u.key == SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString()).ToList().ForEach(u => listProfileStatus.Add(new { Value = u.value }));
            dicRef["ProfileStatuses"] = listProfileStatus;

            List<object> listGenres = new List<object>();
            CommonList.GenreAndSelect().ForEach(u => listGenres.Add(new { Value = u.value }));
            dicRef["Genres"] = listGenres;

            refTemp.FillVariableValue(dicRef);
            // Set lai ten cac Sheet
            sheetProvince.Name = "TinhThanh";
            refSheet.Delete();

            sheetDistrict.Name = "QuanHuyen";
            refSheetDis.Delete();

            sheetCommune.Name = "XaPhuong";
            refSheetComm.Delete();

            sheetHDC.Name = "HuongDanChung";
            refHDC.Delete();

            #endregion

            //Neu la cap 1 thi an cot he hoc ngoai ngu
            //if (_globalInfo.AppliedLevel.Value == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)

            //an cot mo ta neu nhu download template mau                
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                ReportName = "Mau_Import_TH_ThongTinHocSinh.xls";
                sheet.HideColumn(19);// he ngoai ngu
                sheet.HideColumn(22);// doan
                sheet.HideColumn(23);// dang
                sheet.HideColumn(34);//cmnd
                sheet.HideColumn(35);//email
            }
            else
            {
                if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    ReportName = "Mau_Import_THCS_ThongTinHocSinh.xls";
                    sheet.HideColumn(23);// dang
                    sheet.HideColumn(34);//cmnd
                    sheet.HideColumn(35);//email
                }
                else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                {
                    ReportName = "Mau_Import_THPT_ThongTinHocSinh.xls";
                }
            }
            if (_globalInfo.TrainingType != 3)
            {
                sheet.HideColumn(20);// hinh thuc hoc
            }

            if (isErrFile)
            {
                List<ImportPupilViewModel> lstPupilImportedErr = new List<ImportPupilViewModel>();
                lstPupilImportedErr = (List<ImportPupilViewModel>)Session["LIST_IMPORTED_PUPIL"];
                if (lstPupilImportedErr != null)
                {
                    lstPupilImportedErr = lstPupilImportedErr.Where(p => p.IsValid == false).ToList();
                    ImportPupilViewModel objPupilErr = null;
                    int startRowErr = 5;
                    int startcol = 1;
                    for (int j = 0; j < lstPupilImportedErr.Count; j++)
                    {
                        objPupilErr = lstPupilImportedErr[j];
                        startcol = 1;
                        //STT
                        sheet.SetCellValue(startRowErr, startcol, j + 1);
                        //Ma HS
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.PupilCode);
                        //Ho ten
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.FullName);
                        //Ngay sinh
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.BirthDateStr);
                        //Gioi tinh
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.GenreName);
                        //Lop
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ClassName);
                        //Hinh thuc trung tuyen
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.EnrolmentTypeName);
                        //Ngay vao truong
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.EnrolmentDateStr);
                        //Trang thai hoc sinh
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ProfileStatusName);
                        //Tinh thanh
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ProvinceName);
                        //Quan huyen
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.DistrictName);
                        //Xa,Phuong
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.CommuneName);
                        //Thon xom
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.VillageName);
                        //So dang bo
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.StorageNumber);
                        //Noi sinh
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.BirthPlace);
                        //Que quan
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.HomeTown);
                        //Dia chi thuong tru
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.PermanentResidentalAddress);
                        //Dia chi tam tru
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.PermanentResidentalAddress);
                        //He hoc ngoai ngu              
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ForeignLanguageTrainingName);
                        //Hinh thuc hoc               
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.PupilLearningTypeName);
                        //Ngay vao doi
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.YoungPioneerJoinedDate);
                        //Ngay vao doan
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.YoungLeagueJoinedDate);
                        //Ngay vao dang
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.CommunistPartyJoinedDate);
                        //Dan toc
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.EthnicName);
                        //Ton giao
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ReligionName);
                        //Doi tuong chinh sach
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.PolicyTargetName);
                        // Che do chinh sach
                        sheet.SetCellValue(startRowErr, ++startcol, "");
                        //Ho tro chi phi hoc tap
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IsSupportForLearning == true ? "x" : "");
                        //Thuoc tai dinh cu
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IsResettlementTarget == true ? "x" : "");
                        // Khu vuc
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.AreaName);
                        //Dien hoc sinh
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ClassTypeName);
                        //Loai khuyet tat
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.DisabilityTypeName);
                        //So dien thoai di dong
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.Mobile);
                        //So CMND
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IdentityNumber);
                        //Email
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.Email);
                        //Nhom mau
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.BloodTypeName);
                        //Hoc chuong trinh bo gia duc cua Bo
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IsUsedMoetProgram == true ? "x" : "");
                        //Biet boi
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IsSwimming == true ? "x" : "");
                        //Ho ten cha
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.FatherFullName);
                        //Nam sinh cua cha
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.FatherBirthDateStr);
                        //Nghe nghiep cua cha
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.FatherJob);
                        //SDT cua cha
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.FatherMobile);
                        //Email cha
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.FatherEmail);
                        //Ho ten me
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.MotherFullName);
                        //Nam sinh cua me
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.MotherBirthDateStr);
                        //Nghe nghiep cua me
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.MotherJob);
                        //SDT cua me
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.MotherMobile);
                        //Email me
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.MotherEmail);
                        //Ho ten ng bao tro
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.SponsorFullName);
                        //Nam sinh cua ng bao tro
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.SponsorBirthDateStr);
                        //Nghe nghiep cua ng bao tro
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.SponsorJob);
                        //SDT cua ng bao tro
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.SponsorMobile);
                        //Email nguoi bao tro
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.SponsorEmail);
                        // co bo dan toc
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IsMinorityFather == true ? "x" : "");
                        // co me dan toc
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.IsMinorityMother == true ? "x" : "");
                        //Ma HS moi
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.PupilCodeNew);
                        //Mo ta
                        sheet.SetCellValue(startRowErr, ++startcol, objPupilErr.ErrorMessage);

                        if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                        {
                            sheet.HideColumn(19);// he ngoai ngu
                            sheet.HideColumn(22);// doan
                            sheet.HideColumn(23);// dang
                            sheet.HideColumn(34);//cmnd
                            sheet.HideColumn(35);//email
                        }
                        else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                        {
                            sheet.HideColumn(23);// dang
                            sheet.HideColumn(34);//cmnd
                            sheet.HideColumn(35);//email
                        }
                        startRowErr++;
                    }
                    sheet.UnHideColumn(57);
                    sheet.GetRange(5, 1, 5 + lstPupilImportedErr.Count - 1, 57).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }
            }

            return book.ToStream();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(FormCollection frm)
        {
            bool isAddPupil = frm["hdfIsAddPupil"] != null && "1".Equals(frm["hdfIsAddPupil"]) ? true : false;
            List<ImportPupilViewModel> listPupil = (List<ImportPupilViewModel>)Session[ImportPupilConstants.LIST_IMPORTED_PUPIL];
            SaveData(isAddPupil);

            int countAllPupil = listPupil != null ? listPupil.Count : 0;
            int countPupilValid = listPupil != null ? listPupil.Count(p => p.IsValid) : 0;
            string retMsg = String.Format("{0} {1}/{2} học sinh", Res.Get("Common_Label_ImportSuccessMessage"), countPupilValid, countAllPupil);
            return Json(new JsonMessage(retMsg, JsonMessage.SUCCESS), "text/html");
        }

        private void SaveData(bool IsAddPupil)
        {
            List<ImportPupilViewModel> listPupil = (List<ImportPupilViewModel>)Session[ImportPupilConstants.LIST_IMPORTED_PUPIL];
            if (listPupil == null)
            {
                return;
            }
            listPupil = listPupil.Where(p => p.IsValid).ToList();
            List<Village> lstVillageImport = (List<Village>)Session["lstVillageImport"];
            if (listPupil == null || !listPupil.Any())
            {
                return;
            }

            List<string> lstPupilCode = listPupil.Select(o => o.PupilCode).ToList();
            List<PupilProfile> lstPupilUpdatePP = null;
            List<string> listPupilCodeUpdate = new List<string>();
            List<ImportPupilViewModel> lstPupilUpdate = null;

            if (!IsAddPupil)
            {
                lstPupilUpdatePP = PupilProfileBusiness.All
                                                   .Where(o => o.CurrentSchoolID == _globalInfo.SchoolID.Value
                                                       && o.IsActive
                                                       && lstPupilCode.Contains(o.PupilCode)).ToList();

                listPupilCodeUpdate = lstPupilUpdatePP.Select(o => o.PupilCode).ToList();
                lstPupilUpdate = listPupil.Where(o => listPupilCodeUpdate.Any(t => t.ToLower().Equals(o.PupilCode.ToLower()))).ToList();
            }

            //Lay danh sach lop
            int academicYearId = _globalInfo.AcademicYearID.Value;
            List<int> lstClassID = listPupil.Select(o => o.CurrentClassID).Distinct().ToList();
            List<PupilProfile> lstPupilInsertPP = new List<PupilProfile>();
            List<PupilProfile> lstPupilUpdateAll = new List<PupilProfile>();
            List<int> lstEnrollmentTypeInsert = new List<int>();
            List<int> lstEnrollmentTypeUpdate = new List<int>();
            List<int> lstOrderInsert = new List<int>();
            List<int> lstOrderUpdate = new List<int>();
            List<int> lstClassIDUpdate = new List<int>();
            List<ImportPupilViewModel> lstPupilUpdateClass = new List<ImportPupilViewModel>();
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = academicYearId;
            if (lstVillageImport != null && lstVillageImport.Count > 0)
            {
                for (int i = 0; i < lstVillageImport.Count; i++)
                {
                    VillageBusiness.Insert(lstVillageImport[i]);
                }
                VillageBusiness.Save();
            }

            List<string> lstVilliageName = listPupil.Where(p => p.VillageName != "").Select(p => p.VillageName).Distinct().ToList();
            List<Village> lstvil = new List<Village>();
            if (lstVilliageName.Count > 0)
            {
                lstvil = VillageBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Where(p => lstVilliageName.Contains(p.VillageName)).ToList();
            }

            if (IsAddPupil)
            {
                foreach (ImportPupilViewModel pupil in listPupil)
                {

                    var pupilProfile = new PupilProfile
                    {
                        CurrentAcademicYearID = academicYearId,
                        IsActive = true,
                        CreatedDate = DateTime.Now
                    };
                    if (pupil.VillageName != null && pupil.VillageName.Trim() != "")
                    {
                        Village vil = lstvil.FirstOrDefault(p => p.VillageName.Equals(pupil.VillageName) && p.CommuneID == pupil.CommuneID);
                        if (vil != null)
                        {
                            pupil.VillageID = vil.VillageID;
                        }
                    }
                    else
                    {
                        pupil.VillageID = null;

                    }
                    CopyValue(pupilProfile, pupil);
                    lstPupilInsertPP.Add(pupilProfile);
                    lstEnrollmentTypeInsert.Add(pupil.EnrolmentType);
                    lstOrderInsert.Add(pupil.Index);
                }
            }
            else
            {
                //lstPupilUpdateClass = lstPupilUpdate.Where(o => o.CurrentClassID == ClassID).ToList();
                foreach (ImportPupilViewModel pupil in lstPupilUpdate)
                {
                    PupilProfile pupilProfile = lstPupilUpdatePP.FirstOrDefault(o => o.PupilCode == pupil.PupilCode);
                    if (pupilProfile != null)
                    {
                        int currentClassID = pupilProfile.CurrentClassID;
                        pupilProfile.CurrentAcademicYearID = academicYearId;
                        pupilProfile.IsActive = true;
                        if (pupil.VillageName != null && pupil.VillageName.Trim() != "")
                        {
                            Village vil = lstvil.FirstOrDefault(p => p.VillageName.Equals(pupil.VillageName) && p.CommuneID == pupil.CommuneID);
                            if (vil != null)
                            {
                                pupil.VillageID = vil.VillageID;
                            }
                        }
                        else
                        {
                            pupil.VillageID = null;

                        }
                        CopyValue(pupilProfile, pupil);
                        lstPupilUpdateAll.Add(pupilProfile);
                        lstEnrollmentTypeUpdate.Add(pupil.EnrolmentType);
                        lstClassIDUpdate.Add(currentClassID);
                    }
                    lstOrderUpdate.Add(pupil.Index);
                }
            }


            if (lstPupilInsertPP.Count > 0)
            {
                PupilProfileBusiness.ImportPupilProfile(_globalInfo.UserAccountID, lstPupilInsertPP, lstEnrollmentTypeInsert, lstOrderInsert, lstClassID);
            }

            if (lstPupilUpdateAll.Count > 0)
            {
                PupilProfileBusiness.UpdateImportPupilProfile(_globalInfo.UserAccountID, lstPupilUpdateAll, lstEnrollmentTypeUpdate, lstOrderUpdate, lstClassIDUpdate, lstClassID);
            }

            //VillageBusiness.Save();

        }

        #region Util Functions

        private void CopyValue(PupilProfile pupilProfile, ImportPupilViewModel pupil)
        {
            pupilProfile.BirthDate = pupil.BirthDate.Value;
            pupilProfile.BirthPlace = pupil.BirthPlace;
            pupilProfile.BloodType = pupil.BloodType;
            pupilProfile.CreatedDate = DateTime.Now;
            pupilProfile.CurrentClassID = pupil.CurrentClassID;
            pupilProfile.CurrentSchoolID = pupil.CurrentSchoolID;
            pupilProfile.EnrolmentDate = pupil.EnrolmentDate.Value;
            pupilProfile.EthnicID = pupil.EthnicID;
            pupilProfile.FatherBirthDate = pupil.FatherBirthDate;
            pupilProfile.FatherEmail = pupil.FatherEmail;
            pupilProfile.FatherFullName = pupil.FatherFullName;
            pupilProfile.FatherJob = pupil.FatherJob;
            pupilProfile.FatherMobile = pupil.FatherMobile;
            pupilProfile.ForeignLanguageTraining = pupil.ForeignLanguageTraining;
            pupilProfile.PupilLearningType = pupil.PupilLearningType;
            pupilProfile.FullName = pupil.FullName;
            pupilProfile.Genre = pupil.Genre;
            pupilProfile.HomeTown = pupil.HomeTown;
            pupilProfile.IdentifyNumber = pupil.IdentityNumber;
            pupilProfile.Email = pupil.Email;
            pupilProfile.MotherBirthDate = pupil.MotherBirthDate;
            pupilProfile.MotherEmail = pupil.MotherEmail;
            pupilProfile.MotherFullName = pupil.MotherFullName;
            pupilProfile.MotherJob = pupil.MotherJob;
            pupilProfile.MotherMobile = pupil.MotherMobile;
            pupilProfile.Mobile = pupil.Mobile;
            pupilProfile.Name = pupil.Name;
            pupilProfile.StorageNumber = pupil.StorageNumber;
            pupilProfile.PermanentResidentalAddress = pupil.PermanentResidentalAddress;
            pupilProfile.PolicyTargetID = pupil.PolicyTargetID;
            pupilProfile.ProfileStatus = pupil.ProfileStatus;
            if (pupil.ProvinceID != null) pupilProfile.ProvinceID = (int)pupil.ProvinceID;
            pupilProfile.PupilCode = !string.IsNullOrEmpty(pupil.PupilCodeNew) ? pupil.PupilCodeNew : pupil.PupilCode;
            pupilProfile.ReligionID = pupil.ReligionID;
            pupilProfile.TempResidentalAddress = pupil.TempResidentalAddress;
            //Do import moi cap nhat lai ProvinceID nen de DistrictID và CommuneID la null
            pupilProfile.CommuneID = pupil.CommuneID;
            pupilProfile.DistrictID = pupil.DistrictID;
            pupilProfile.VillageID = pupil.VillageID;
            //them doi, doan, dang
            if (pupil.YoungPioneerJoinedDate != null)
            {
                pupilProfile.YoungPioneerJoinedDate = pupil.YoungPioneerJoinedDate;
                pupilProfile.IsYoungPioneerMember = true;
            }
            if (pupil.YoungLeagueJoinedDate != null)
            {
                pupilProfile.YouthLeagueJoinedDate = pupil.YoungLeagueJoinedDate;
                pupilProfile.IsYouthLeageMember = true;
            }
            if (pupil.CommunistPartyJoinedDate != null)
            {
                pupilProfile.CommunistPartyJoinedDate = pupil.CommunistPartyJoinedDate;
                pupilProfile.IsCommunistPartyMember = true;
            }
            //Cap nhat 08/2016
            pupilProfile.ClassType = pupil.ClassType;
            pupilProfile.IsSupportForLearning = pupil.IsSupportForLearning;
            pupilProfile.IsResettlementTarget = pupil.IsResettlementTarget;
            pupilProfile.IsSwimming = pupil.IsSwimming;

            pupilProfile.SponsorBirthDate = pupil.SponsorBirthDate;
            pupilProfile.SponsorFullName = pupil.SponsorFullName;
            pupilProfile.SponsorJob = pupil.SponsorJob;
            pupilProfile.SponsorMobile = pupil.SponsorMobile;
            pupilProfile.SponsorEmail = pupil.SponsorEmail;

            pupilProfile.MinorityFather = pupil.IsMinorityFather;
            pupilProfile.MinorityMother = pupil.IsMinorityMother;
            pupilProfile.UsedMoetProgram = pupil.IsUsedMoetProgram;
            pupilProfile.IsDisabled = pupil.IsDisabled;
            pupilProfile.DisabledTypeID = pupil.DisabledTypeID;
            pupilProfile.AreaID = pupil.AreaID;
            pupilProfile.PolicyRegimeID = pupil.PolicyRegimeID;
        }

        private string GetCellString(IVTWorksheet sheet, string cellName)
        {
            object o = sheet.GetRange(cellName, cellName).Value;
            return o == null ? string.Empty : o.ToString().Trim();
        }

        private DateTime? GetCellDate(IVTWorksheet sheet, string cellName)
        {
            object o = sheet.GetRange(cellName, cellName).Value;
            var culture = new CultureInfo("vi-VN");
            if (o != null)
            {
                try
                {
                    if (o is DateTime)
                        return (DateTime)o;
                    var oStr = o.ToString().Trim();
                    if (oStr.EndsWith(".00"))
                        oStr = oStr.Remove(oStr.Length - 4, 3);
                    if (oStr.EndsWith(".0"))
                        oStr = oStr.Remove(oStr.Length - 3, 2);

                    DateTime dt;

                    DateTime.TryParse(oStr, out dt);
                    return dt;
                }
                catch { }
            }
            return null;
        }

        private int SetGenre(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();
            ComboObject c = CommonList.GenreAndSelect().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? 0 : Convert.ToByte(c.key);
        }

        private int SetEnrolementType(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            ComboObject c = CommonList.EnrolmentType().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? 0 : Convert.ToByte(c.key);
        }

        private int? SetBloodType(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty) return null;

            ComboObject c = CommonList.BloodType().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetForeignLanguageTraining(string value)
        {
            GlobalInfo glo = new GlobalInfo();
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty)
                return (int)SystemParamsInFile.FOREIGN_LANGUAGE_TRAINING_UNIDENTIFIED;
            ComboObject c = CommonList.ForeignLanguageTraining().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetPupilLearningType(string value)
        {
            GlobalInfo glo = new GlobalInfo();
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty)
                return 0;
            ComboObject c = CommonList.PupilLearningType().SingleOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetProfileStatus(string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();
            ComboObject c = CommonList.PupilStatus().SingleOrDefault(u => u.key == SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString() && u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToByte(c.key);
        }

        private int SetFromList(List<ComboObject> list, string value)
        {
            value = (value == null ? string.Empty : value.Trim().ToLower());
            ComboObject c = list.FirstOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? 0 : Convert.ToInt32(c.key);
        }

        private int SetFromListCommune(List<Village> listVillage, int? communeID, string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();
            Village village = listVillage.Where(o => o.VillageName.Trim().ToLower() == value && o.CommuneID == communeID).FirstOrDefault();
            if (village != null)
                return village.VillageID;
            else
                return 0;
        }

        private int? SetNulableFromList(List<ComboObject> list, string value)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty) return null;

            ComboObject c = list.FirstOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToInt32(c.key);
        }
        private int? SetNulableFromListOrDefault(List<ComboObject> list, string value, int valueDefault)
        {
            value = value == null ? string.Empty : value.Trim().ToLower();

            if (value == string.Empty || value == "") return valueDefault;

            ComboObject c = list.FirstOrDefault(u => u.value.ToLower().Equals(value));
            return c == null ? -1 : Convert.ToInt32(c.key);
        }
        private void ValidateImportedPupil(ImportPupilViewModel pupil)
        {
            ValidationContext vc = new ValidationContext(pupil, null, null);
            List<string> messages = new List<string>();
            foreach (PropertyInfo pi in pupil.GetType().GetProperties())
            {
                string resKey = string.Empty;
                if (pi.Name == "FatherBirthDate" || pi.Name == "MotherBirthDate" || pi.Name == "BirthDate" || pi.Name == "EnrolmentDate"
                    || pi.Name == "YoungPioneerJoinedDate" || pi.Name == "YoungLeagueJoinedDate" || pi.Name == "CommunistPartyJoinedDate"
                    || pi.Name == "Email" || pi.Name == "FatherEmail" || pi.Name == "MotherEmail" || pi.Name == "SponsorEmail" || pi.Name == "ForeignLanguageTraining")
                {
                    continue;
                }
                foreach (var att in pi.GetCustomAttributes(true))
                {
                    if (att is DisplayAttribute)
                    {
                        resKey = ((DisplayAttribute)att).Name;
                        break;
                    }

                    if (att is DisplayNameAttribute)
                    {
                        resKey = ((DisplayAttribute)att).Name;
                        break;
                    }
                }

                foreach (var att in pi.GetCustomAttributes(true))
                {
                    if (att is ValidationAttribute)
                    {
                        ValidationAttribute attV = (ValidationAttribute)att;
                        vc.MemberName = pi.Name;
                        if (attV.GetValidationResult(pi.GetValue(pupil, null), vc) != ValidationResult.Success)
                        {
                            List<object> Params = new List<object>();
                            Params.Add(Res.Get(resKey));

                            if (att is StringLengthAttribute)
                            {
                                StringLengthAttribute attS = (StringLengthAttribute)att;
                                Params.Add(attS.MaximumLength);
                            }

                            messages.Add(string.Format(Res.Get(attV.ErrorMessageResourceName), Params.ToArray()));
                        }
                    }
                }
            }

            if (messages.Count > 0)
            {
                pupil.IsValid = false;
                pupil.ErrorMessage += string.Join(SEMICOLON_SPACE, messages);
                pupil.ErrorMessage += SEMICOLON_SPACE;
            }
        }

        private string GetPupilName(string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
                return string.Empty;

            fullName = fullName.Trim();

            int index = fullName.LastIndexOf(' ');

            return index > 0 ? fullName.Substring(index + 1) : fullName;
        }

        private int? GetCellIndex(string value)
        {
            int val = 0;
            if (string.IsNullOrEmpty(value) || value.Trim() == "" || !int.TryParse(value.Trim(), out val) || !Regex.IsMatch(value.Trim(), "^\\d+$"))
                return null;
            if (val > 0 && val < 10000)
            {
                return val;
            }
            return null;
        }

        private string ToStringWithFixedLength(int number, int length)
        {
            if (number < 0) return string.Empty;

            if (number.ToString().Length > length) return string.Empty;

            return number.ToString().PadLeft(length, '0');
        }

        private bool checkCompareDistrict(int? provinceID, int? districtID, List<District> lstDistrict)
        {
            if (districtID == null) return true;
            if (districtID != null && provinceID == null) return false;
            int check = lstDistrict.Where(p => p.ProvinceID == provinceID && p.DistrictID == districtID).Count();
            if (check > 0)
                return true;
            return false;
        }
        private bool checkCompareCommune(int? districtID, int? communeID, List<Commune> lstCommune)
        {
            if (communeID == null || districtID == null)
                return true;
            if (communeID != null && districtID == null) return false;
            int check = lstCommune.Where(p => p.DistrictID == districtID && p.CommuneID == communeID).Count();
            if (check > 0)
                return true;
            return false;
        }
        private bool checkCompareVillage(int? villiageID, int? communeID)
        {
            if (villiageID == null || communeID == null) return true;
            if (villiageID != null && communeID == null) return false;
            var check = VillageBusiness.Search(new Dictionary<string, object> { { "CommuneID", communeID }, { "Villiage", villiageID }, { "IsActive", true } });
            if (check.Count() > 0)
                return true;
            return false;
        }
        #endregion
    }
}
