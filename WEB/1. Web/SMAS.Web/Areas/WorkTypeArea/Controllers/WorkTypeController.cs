﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.WorkTypeArea.Models;
using SMAS.Web.Areas.WorkTypeArea;
using SMAS.Models.Models;

namespace SMAS.Web.Areas.WorkTypeArea.Controllers
{
    public class WorkTypeController : BaseController
    {        
        private readonly IWorkTypeBusiness WorkTypeBusiness;
        private readonly IWorkGroupTypeBusiness WorkGroupTypeBusiness;
		
		public WorkTypeController (IWorkTypeBusiness worktypeBusiness,IWorkGroupTypeBusiness workGroupTypeBusiness)
		{
			this.WorkTypeBusiness = worktypeBusiness;
            this.WorkGroupTypeBusiness = workGroupTypeBusiness;
		}
		
		//
        // GET: /WorkType/

        public ActionResult Index()
        {
            //Danh sach nhom cong viec
            IDictionary<string, object> WorkGroupTypeSearchInfo = new Dictionary<string, object>();
            WorkGroupTypeSearchInfo["IsActive"] = true;

            List<WorkGroupType> ListWorkGroupType = new List<WorkGroupType>();
            ListWorkGroupType = this.WorkGroupTypeBusiness.Search(WorkGroupTypeSearchInfo).ToList();
            ViewData[WorkTypeConstants.LIST_WORKGROUPTYPE] = new SelectList(ListWorkGroupType, "WorkGroupTypeID", "Resolution");


            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //Get view data here



            IEnumerable<WorkTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[WorkTypeConstants.LIST_WORKTYPE] = lst;
            return View();
        }

		//
        // GET: /WorkType/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;
            SearchInfo["WorkGroupTypeID"] = frm.WorkGroupTypeID;

			//add search info
			//

            IEnumerable<WorkTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[WorkTypeConstants.LIST_WORKTYPE] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            WorkType worktype = new WorkType();
            TryUpdateModel(worktype); 
            Utils.Utils.TrimObject(worktype);
            worktype.IsActive = true;

            this.WorkTypeBusiness.Insert(worktype);
            this.WorkTypeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int WorkTypeID)
        {
            WorkType worktype = this.WorkTypeBusiness.Find(WorkTypeID);
            TryUpdateModel(worktype);
            Utils.Utils.TrimObject(worktype);
            this.WorkTypeBusiness.Update(worktype);
            this.WorkTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.WorkTypeBusiness.Delete(id);
            this.WorkTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<WorkTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<WorkType> query = this.WorkTypeBusiness.Search(SearchInfo);
            IQueryable<WorkTypeViewModel> lst = query.Select(o => new WorkTypeViewModel {               
						WorkTypeID = o.WorkTypeID,								
						Resolution = o.Resolution,								
						WorkGroupTypeID = o.WorkGroupTypeID,																		
                        WorkGroupTypeName = o.WorkGroupType.Resolution,
						Description = o.Description												
						
					
				
            });

            return lst.OrderBy(o=>o.Resolution).ToList();
        }        
    }
}





