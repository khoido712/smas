/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.WorkTypeArea
{
    public class WorkTypeConstants
    {
        public const string LIST_WORKTYPE = "listWorkType";
        public const string LIST_WORKGROUPTYPE = "listWorkGroupType";
    }
}