/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.WorkTypeArea.Models
{
    public class WorkTypeViewModel
    {
        [ScaffoldColumn(false)]
		public int WorkTypeID { get; set; }

        [ResourceDisplayName("WorkType_Label_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]			
		public string Resolution { get; set; }

        [ResourceDisplayName("WorkGroupType_Label_Resolution")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]        
        [AdditionalMetadata("ViewDataKey", WorkTypeConstants.LIST_WORKGROUPTYPE)]
        public Nullable<int> WorkGroupTypeID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("WorkGroupType_Label_Resolution")]
        public string WorkGroupTypeName { get; set; }

        [ResourceDisplayName("WorkType_Column_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
		public string Description { get; set; }						
						
	       
    }
}


