﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DetachableHeadBagArea
{
    public class DetachableHeadBagAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DetachableHeadBagArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DetachableHeadBagArea_default",
                "DetachableHeadBagArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
