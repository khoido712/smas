﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DetachableHeadBagArea
{
    public class DetachableHeadBagContants
    {
        public const string LIST_EXAMINATION = "listExamination";
        public const string LIST_EDUCATION_LEVEL = "listEducationLevel";
        public const string LIST_EXAMINATION_SUBJECT = "listExaminationSubject";
        public const string LIST_DETACHABLEHEADBAG = "ListDetachableHeadBag";

        public const string LIST_MANUAL_DETACHABLEHEADBAG = "ListManualDetachableHeadBag";
        public const string LIST_AUTO_DETACHABLEHEADBAG = "ListAutoDetachableHeadBag";

        public const string ENABLE_EXPORT = "enable_export";
    }
}