﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.DetachableHeadBagArea.Models
{
    public class AutoGenViewModel
    {
        [ResourceDisplayName("SubjectCat_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ExaminationSubjectID { get; set; }

        [ResourceDisplayName("DetachableHeadBag_Lable_DetachableHeadNumber")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [Range(1, 99, ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public int DetachableHeadNumber { get; set; }

        [ResourceDisplayName("DetachableHeadBag_Label_Prefix")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Prefix { get; set; }

        [ResourceDisplayName("DetachableHeadBag_Lable_StartingNumber")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [Range(1, 999, ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public int StartingNumber { get; set; }
    }
}