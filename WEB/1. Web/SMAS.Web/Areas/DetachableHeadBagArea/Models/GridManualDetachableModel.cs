﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DetachableHeadBagArea.Models
{
    public class GridManualDetachableModel
    {
        public int? DetachableHeadMappingID { get; set; }

        public int CandidateID { get; set; }
        
        public string NameListCode { get; set; }

        public int? NameListNumber { get; set; }

        public string DetachableHeadNumber { get; set; }

        public int? DetachableHeadBagID { get; set; }

        public string BagTitle { get; set; }
    }
}