﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.DetachableHeadBagArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Examination_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ExaminationID { get; set; }

        [ResourceDisplayName("DetachableHeadBag_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("Candidate_Label_ExaminationSubject")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ExaminationSubjectID { get; set; }

        [ResourceDisplayName("Candidate_Label_NamedListCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [Range(0, 1000, ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public string FromNumber { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [Range(0, 1000, ErrorMessageResourceName = "Common_Validate_NotInRange")]
        public string ToNumber { get; set; }
    }
}