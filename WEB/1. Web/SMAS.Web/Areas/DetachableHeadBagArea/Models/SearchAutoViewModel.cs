﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.DetachableHeadBagArea.Models
{
    public class SearchAutoViewModel
    {
        [ResourceDisplayName("Examination_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ExaminationID { get; set; }

        [ResourceDisplayName("DetachableHeadBag_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int EducationLevelID { get; set; }

        [ResourceDisplayName("Candidate_Label_ExaminationSubject")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int ExaminationSubjectID { get; set; }

        //[ResDisplayName("DetachableHeadBag_Lable_DetachableHead")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public int DetachableHeadBagID { get; set; }

    }
}