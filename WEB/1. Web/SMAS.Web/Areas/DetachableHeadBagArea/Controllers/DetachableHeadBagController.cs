﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.DetachableHeadBagArea.Models;
using SMAS.Web.Controllers;
using System.IO;
using SMAS.Business.Common;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.DetachableHeadBagArea.Controllers
{
    public class DetachableHeadBagController : BaseController
    {
        private readonly IDetachableHeadBagBusiness DetachableHeadBagBusiness;
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly ICandidateBusiness CandidateBusiness;
        private readonly IDetachableHeadMappingBusiness DetachableHeadMappingBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        public DetachableHeadBagController(IDetachableHeadBagBusiness detachableHeadBagBusiness,
                                            IExaminationBusiness examinationBusiness,
                                            IExaminationSubjectBusiness examinationSubjectBusiness,
                                            ICandidateBusiness candidateBusiness,
                                            IDetachableHeadMappingBusiness detachableHeadMappingBusiness,
                                            IAcademicYearBusiness academicYearBusiness)
        {
            this.DetachableHeadBagBusiness = detachableHeadBagBusiness;
            this.ExaminationBusiness = examinationBusiness;
            this.ExaminationSubjectBusiness = examinationSubjectBusiness;
            this.CandidateBusiness = candidateBusiness;
            this.DetachableHeadMappingBusiness = detachableHeadMappingBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
        }

        public ActionResult Index()
        {
            GetViewData();
            return View();
        }

        

        [ValidateAntiForgeryToken]
        public ActionResult ManualSave(SearchManualViewModel model, FormCollection form, string[] NameListCodes)
        {
            //Với một học sinh(dòng) sẽ có túi phách và số phách tương ứng => số túi phách bằng số số phách
            if (ModelState.IsValid || NameListCodes == null)
            {
                GlobalInfo glo = new GlobalInfo();
                if (GetMenupermission("DetachableHeadBag", glo.UserAccountID, glo.IsAdmin) < SystemParamsInFile.PER_CREATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
                if (NameListCodes == null || NameListCodes.Count() == 0)
                {
                    return Json(new JsonMessage(string.Format("Không có thí sinh nào được đánh số phách."), JsonMessage.ERROR));
                }
                //Tat ca cac headnumber truyen len
                string[] DetachableHeadBags = NameListCodes.Select(u => GetFormString(form, "DetachableHeadBag_" + u)).ToArray();
                string[] DetachableHeadNumbers = NameListCodes.Select(u => GetFormString(form, "DetachableHeadNumber_" + u)).ToArray();

                //Neu danh sach so bao danh truyen len bi trong
                if (NameListCodes.Any(u => u == null || u.Trim() == string.Empty))
                    return Json(new JsonMessage(string.Format("Tồn tại thí sinh chưa đánh số báo danh. Vui lòng thực hiện đánh số báo danh trước."), JsonMessage.ERROR));

                //Neu danh sach tui phach truyen len bi trong
                if (DetachableHeadBags.Any(u => string.IsNullOrEmpty(u)))
                    return Json(new JsonMessage(string.Format(Res.Get("requireResourceKey"), Res.Get("DetachableHeadBag_Label_BagTitle")), JsonMessage.ERROR));

                //Neu danh sach so phach truyen len bi trong
                if (DetachableHeadNumbers.Any(u => string.IsNullOrEmpty(u)))
                    return Json(new JsonMessage(string.Format(Res.Get("requireResourceKey"), Res.Get("DetachableHeadMapping_Label_DetachableHeadNumber")), JsonMessage.ERROR));

                //Neu danh sach so phach truyen len trung nhau
                if (DetachableHeadNumbers.Any(u => DetachableHeadNumbers.Count(v => v == u) > 1))
                    return Json(new JsonMessage(Res.Get("DetachableHeadBag_Validate_DuplicateHeadBagNumber"), JsonMessage.ERROR));

                List<GridManualDetachableModel> lstManualMapping = GetManualMappingList(model);

                if (NameListCodes.Any(u => !lstManualMapping.Any(v => v.NameListCode.Equals(u))))
                    return Json(new JsonMessage(Res.Get("Candidate_Label_NamedListCodeNotExists"), JsonMessage.ERROR));

                DetachableHeadBagBusiness.ManualGenDetachHeadNumber(glo.SchoolID.Value, (int)glo.AppliedLevel.Value, model.ExaminationSubjectID, NameListCodes, DetachableHeadBags, DetachableHeadNumbers);
                DetachableHeadBagBusiness.Save();

                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
            else
            {
                string errorMessage = string.Join(",", ModelState.Values.Where(u => u.Errors.Count > 0).Select(u => string.Join(",", u.Errors.Select(v => v.ErrorMessage))));
                return Json(new JsonMessage(errorMessage, JsonMessage.ERROR));
            }
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult AutoSave(AutoGenViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                if (GetMenupermission("DetachableHeadBag", glo.UserAccountID, glo.IsAdmin) < SystemParamsInFile.PER_CREATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
                DetachableHeadBagBusiness.AutoGenDetachableHeadNumber(glo.SchoolID.Value, (int)glo.AppliedLevel, model.ExaminationSubjectID,
                                                                        model.DetachableHeadNumber, model.Prefix, model.StartingNumber);
                DetachableHeadBagBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_AutoGenSuccess")));
            }
            else
            {
                string errorMessage = string.Join(",", ModelState.Values.Where(u => u.Errors.Count > 0).Select(u => string.Join(",", u.Errors.Select(v => v.ErrorMessage))));
                return Json(new JsonMessage(errorMessage, JsonMessage.ERROR));
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadPrefix(int examinationSubjectID)
        {
            ExaminationSubject es = ExaminationSubjectBusiness.Find(examinationSubjectID);
            if (es != null)
            {
                return Json(es.OrderNumberPrefix != null ? es.OrderNumberPrefix : string.Empty);
            }
            else
            {
                return Json(string.Empty);
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadDetachableHeadBag(int? examinationID, int? examinationSubjectID, int? educationLevelID)
        {
            GlobalInfo glo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = glo.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = glo.AppliedLevel.Value;
            if (examinationID.HasValue && examinationID.Value > 0)
                SearchInfo["ExaminationID"] = examinationID;
            if (examinationSubjectID.HasValue && examinationSubjectID.Value > 0)
                SearchInfo["ExaminationSubjectID"] = examinationSubjectID;
            if (educationLevelID.HasValue && educationLevelID.Value > 0)
                SearchInfo["EducationLevelID"] = educationLevelID;

            List<DetachableHeadBag> lstDetachableHeadBag = DetachableHeadBagBusiness.SearchBySchool(glo.SchoolID.Value, SearchInfo).ToList();
            if (lstDetachableHeadBag != null)
            {
                return Json(lstDetachableHeadBag.Select(u => new ComboObject(u.DetachableHeadBagID.ToString(), u.BagTitle.ToString())));
            }
            else
            {
                return Json(string.Empty);
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult CheckDisplayExcel(int examinationID)
        {
            Examination exam = ExaminationBusiness.Find(examinationID);
            int currentStage = GetCurrentSatageByExminationID(examinationID);
            bool isShow = exam != null
                        && _globalInfo.IsCurrentYear
                        && (exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_CREATED
                //|| exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED
                            || exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_INVILIGATOR_ASSIGNED
                            || exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_ROOM_ASSIGNED
                            || exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_CANDIDATE_LISTED);
            return Json(isShow);
        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();
            SetViewDataPermission("DetachableHeadBag", glo.UserAccountID, glo.IsAdmin);
            string examID = Request["ExaminationID"];
            int ExaminationID = 0;
            Dictionary<string, object> dicExamination = new Dictionary<string, object>();
            dicExamination.Add("AcademicYearID", glo.AcademicYearID.Value);
            dicExamination.Add("AppliedLevel", glo.AppliedLevel.Value);
            IQueryable<Examination> lstExam = ExaminationBusiness.SearchBySchool(glo.SchoolID.Value, dicExamination).OrderByDescending(o => o.ToDate);
            if (examID == null || examID.Trim().Equals(string.Empty) || !int.TryParse(examID, out ExaminationID))
            {
                ViewData[DetachableHeadBagContants.LIST_EXAMINATION] = new SelectList(lstExam, "ExaminationID", "Title");
                List<EducationLevel> lsEL = new List<EducationLevel>();
                ViewData[DetachableHeadBagContants.LIST_EDUCATION_LEVEL] = new SelectList(lsEL, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[DetachableHeadBagContants.LIST_EXAMINATION] = new SelectList(lstExam.ToList(), "ExaminationID", "Title", ExaminationID);
                List<EducationLevel> lsEducationLevel = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID",glo.AcademicYearID.Value},
                    {"AppliedLevel",glo.AppliedLevel.Value},
                    {"ExaminationID",ExaminationID}
                }).Select(o => o.EducationLevel).Distinct().OrderBy(o => o.EducationLevelID).ToList();
                ViewData[DetachableHeadBagContants.LIST_EDUCATION_LEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");

            }
            List<ExaminationSubject> lstExamSubject = new List<ExaminationSubject>();
            ViewData[DetachableHeadBagContants.LIST_EXAMINATION_SUBJECT] = new SelectList(lstExamSubject.Select(u => new { u.ExaminationSubjectID, u.SubjectCat.DisplayName }).ToList(), "ExaminationSubjectID", "DisplayName");
            ViewData[DetachableHeadBagContants.LIST_DETACHABLEHEADBAG] = new SelectList(new List<DetachableHeadBag>(), "DetachableHeadBagID", "BagTitle");
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadExamSubject(int? educationLevelID, int? examinationID)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            dicExaminationSubject.Add("AcademicYearID", glo.AcademicYearID.Value);
            dicExaminationSubject.Add("AppliedLevel", glo.AppliedLevel.Value);

            if (educationLevelID.HasValue && educationLevelID.Value > 0)
                dicExaminationSubject.Add("EducationLevelID", educationLevelID);

            if (examinationID.HasValue && examinationID.Value > 0)
                dicExaminationSubject.Add("ExaminationID", examinationID);
            List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            if (lstExamSubject != null && lstExamSubject.Count > 0)
            {
                lstExamSubject = lstExamSubject.OrderBy(p => p.SubjectCat.OrderInSubject).ToList();
            }
            return Json(lstExamSubject.Select(u => new ComboObject(u.ExaminationSubjectID.ToString(), u.SubjectCat.DisplayName)).ToList());
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadManualGrid(SearchManualViewModel model)
        {
            if (ModelState.IsValid)
            {
                var temp = this.GetManualMappingList(model);
                if (temp != null)
                {
                    var lst = temp.OrderBy(p=>p.NameListCode).ToList();
                ViewData[DetachableHeadBagContants.LIST_MANUAL_DETACHABLEHEADBAG] = lst;
            }
            }
            else
            {
                throw new BusinessException(string.Join(",", ModelState.Where(u => u.Value.Errors.Count > 0).SelectMany(u => u.Value.Errors).Select(u => u.ErrorMessage)));
            }
            return PartialView("_GridManualDetachable", model);
        }

        public PartialViewResult LoadManualGridSelectSubject(SearchManualViewModel model)
        {
            model.FromNumber = null;
            model.ToNumber = null;
            ViewData[DetachableHeadBagContants.LIST_MANUAL_DETACHABLEHEADBAG] = GetManualMappingList(model);
            return PartialView("_GridManualDetachable", model);
        }



        [ValidateAntiForgeryToken]
        public PartialViewResult LoadAutoGrid(SearchAutoViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            //Load data into grid here
            if (model.DetachableHeadBagID == 0)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = glo.AcademicYearID.Value;
                SearchInfo["AppliedLevel"] = glo.AppliedLevel.Value;
                SearchInfo["ExaminationID"] = model.ExaminationID;
                SearchInfo["EducationLevelID"] = model.EducationLevelID;
                SearchInfo["ExaminationSubjectID"] = model.ExaminationSubjectID;
                List<Candidate> lstCandidate = CandidateBusiness.SearchBySchool(glo.SchoolID.Value, SearchInfo)
                                                                        .Where(u => u.DetachableHeadMappings.Count == 0)
                                                                        .ToList();
                ViewData[DetachableHeadBagContants.LIST_AUTO_DETACHABLEHEADBAG] = lstCandidate.Select(u => new GridAutoDetachableModel
                                                                                   {
                                                                                       CandidateID = u.CandidateID,
                                                                                       NameListCode = u.NamedListCode,
                                                                                       DetachableHeadNumber = "",
                                                                                       NameListNumber = u.NamedListNumber
                                                                                   }).OrderBy(u => u.NameListNumber);
            }
            else
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = glo.AcademicYearID.Value;
                SearchInfo["AppliedLevel"] = glo.AppliedLevel.Value;
                SearchInfo["DetachableHeadBagID"] = model.DetachableHeadBagID;

                ViewData[DetachableHeadBagContants.LIST_AUTO_DETACHABLEHEADBAG] = DetachableHeadMappingBusiness
                                                                                    .SearchBySchool(glo.SchoolID.Value, SearchInfo)
                                                                                    .Select(u => new GridAutoDetachableModel
                                                                                    {
                                                                                        CandidateID = u.CandidateID,
                                                                                        DetachableHeadNumber = u.DetachableHeadNumber,
                                                                                        NameListCode = u.Candidate.NamedListCode,
                                                                                        NameListNumber = u.Candidate.NamedListNumber
                                                                                    }).ToList().OrderBy(o => o.NameListNumber);
            }
            return PartialView("_GridAutoDetachable");
        }

        public FileResult Export(SearchManualViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                Dictionary<string, object> dicGrid = new Dictionary<string, object>();

                dicGrid["SchoolID"] = glo.SchoolID.Value;
                dicGrid["AcademicYearID"] = glo.AcademicYearID.Value;
                dicGrid["AppliedLevel"] = glo.AppliedLevel.Value;
                dicGrid["ExaminationID"] = model.ExaminationID;
                dicGrid["ExaminationSubjectID"] = model.ExaminationSubjectID;
                dicGrid["EducationLevelID"] = model.EducationLevelID;

                string fileName = string.Empty;
                Stream stream = DetachableHeadMappingBusiness.CreateDetachableHeadMappingReport(dicGrid, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }

        public int GetCurrentSatageByExminationID(int? examinationID)
        {
            int examina = ExaminationBusiness.Find(examinationID).CurrentStage;
            return examina;
        }

        private string GetFormString(FormCollection form, string key)
        {
            string value = form.Get(key);
            if (value == null) return value;

            return value.Trim();
        }

        private List<GridManualDetachableModel> GetManualMappingList(SearchManualViewModel model)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicGrid = new Dictionary<string, object>();
            dicGrid["DetachableHeadBagID"] = model.DetachableHeadBagID;
            dicGrid["AcademicYearID"] = glo.AcademicYearID.Value;
            dicGrid["AppliedLevel"] = glo.AppliedLevel.Value;
            dicGrid["FromNamedListNumber"] = model.FromNumber;
            dicGrid["ToNamedListNumber"] = model.ToNumber;
            dicGrid["ExaminationSubjectID"] = model.ExaminationSubjectID;

            IQueryable<Candidate> lstQCandidate = CandidateBusiness.SearchBySchool(glo.SchoolID.Value, dicGrid);
            if (model.FromNumber.HasValue)
                lstQCandidate = lstQCandidate.Where(u => u.NamedListNumber >= model.FromNumber.Value);

            if (model.ToNumber.HasValue)
                lstQCandidate = lstQCandidate.Where(u => u.NamedListNumber <= model.ToNumber.Value);

            List<Candidate> lstCandidate = lstQCandidate.ToList();
            List<DetachableHeadMapping> lstDetachBagMapping = DetachableHeadMappingBusiness.SearchBySchool(glo.SchoolID.Value, dicGrid).ToList();

            var lstCandidateModel = from c in lstCandidate
                                    join dhm in lstDetachBagMapping on c.CandidateID equals dhm.CandidateID into g1
                                    from j1 in g1.DefaultIfEmpty()
                                    where !string.IsNullOrEmpty(c.NamedListCode)
                                    select new GridManualDetachableModel
                                    {
                                        CandidateID = c.CandidateID,
                                        NameListCode = c.NamedListCode,
                                        NameListNumber = c.NamedListNumber,
                                        DetachableHeadMappingID = j1 != null ? j1.DetachableHeadMappingID : new Nullable<int>(),
                                        BagTitle = j1 != null ? j1.DetachableHeadBag.BagTitle : null,
                                        DetachableHeadBagID = j1 != null ? j1.DetachableHeadBagID : new Nullable<int>(),
                                        DetachableHeadNumber = j1 != null ? j1.DetachableHeadNumber : null
                                    };
            return lstCandidateModel.ToList();
        }

        #region AjaxLoadEducationLevel

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadEducationLevel(int? idExamination)
        {
            GlobalInfo global = new GlobalInfo();
            if (!idExamination.HasValue)
            {
                return Json(new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            IQueryable<EducationLevel> lsEducationLevel = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"AppliedLevel",global.AppliedLevel.Value},
                {"ExaminationID",idExamination}
            }).Select(o => o.EducationLevel).Distinct();
            lsEducationLevel = lsEducationLevel.OrderBy(o => o.EducationLevelID);
            return Json(new SelectList(lsEducationLevel, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);

        }
        #endregion AjaxLoadEducationLevel

    }
}
