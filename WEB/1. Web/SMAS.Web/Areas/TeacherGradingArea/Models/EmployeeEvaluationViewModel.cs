﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.TeacherGradingArea.Models
{
    public class EmployeeEvaluationViewModel
    {
        public int EmployeeID { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string EmployeeCode { get; set; }
        public List<EvaluationFieldModel> lstEvaluationField { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsError { get; set; }
        
        
    }
    public class EvaluationFieldModel
    {
        public int EmployeeID { get; set; }
        public int FieldType { get; set; }
        public int EvaluationFieldID { get; set; }
        public bool IsField { get; set; }
        public int EvaluationLevelID { get; set; }
        public string EvaluationText { get; set; }
        public string EvaluationLevelName { get; set; }
    }
}