/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.TeacherGradingArea.Models
{
    public class TeacherGradingViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32? TeacherGradingID { get; set; }
        
        [ScaffoldColumn(false)]
        public System.Int32 AcademicYearID { get; set; }
        [ScaffoldColumn(false)]
        public System.Int32? SchoolID { get; set; }
        [ScaffoldColumn(false)]
        [ResourceDisplayName("Employee_Label_Sex")]
        public string GenreName { get; set; }
        [ScaffoldColumn(false)]
        public int TeacherID { get; set; }

        public bool IsError { get; set; }

        public string ErrorMessage { get; set; }
        
       

        [ScaffoldColumn(false)]
        [UIHint("DateTimePicker")]
        [ResourceDisplayName("TeacherGrading_Label_AssignedDate")]
        public DateTime? GradingDate { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("TeacherGrading_Label_EmployeeCode")]
        public string EmployeeCode { get; set; }

        
        [ScaffoldColumn(false)]
        [ResourceDisplayName("TeacherGrading_Label_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string FacultyName { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("TeacherGrading_Label_FullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public string FullName { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("TeacherGrading_Label_FullName")]
        public string Name { get; set; }


        [ScaffoldColumn(false)]
        [ResourceDisplayName("TeacherGrading_Label_TeacherGrade")]
        public string GradeResolution { get; set; }

        [ResourceDisplayName("TeacherGrading_Label_FacultyName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", TeacherGradingConstants.LIST_FACULTY)]
        [AdditionalMetadata("OnChange", "AjaxLoadTeacherOfFaculty(this)")]
        public int? SchoolFacultyID { get; set; }

        [ResourceDisplayName("TeacherGrading_Label_TeacherOfFaculty")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", TeacherGradingConstants.LIST_TEACHEROFFACULTY)]
        public int TeacherOfFacultyID { get; set; }

        [ResourceDisplayName("TeacherGrading_Label_TeacherGrade")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? TeacherGradeID { get; set; }

        [ResourceDisplayName("TeacherGrading_Label_TeacherGrade")]
        [ScaffoldColumn(false)]
        public string TeacherGradeName { get; set; }

        [ResourceDisplayName("TeacherGrading_Label_Description")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

    }
}


