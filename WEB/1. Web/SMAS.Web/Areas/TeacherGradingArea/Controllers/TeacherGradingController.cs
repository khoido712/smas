﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.TeacherGradingArea.Models;
using System.Transactions;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.TeacherGradingArea.Controllers
{
    public class TeacherGradingController : BaseController
    {        
        private readonly ITeacherGradingBusiness TeacherGradingBusiness;
		private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly ITeacherGradeBusiness TeacherGradeBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IEmployeeHistoryStatusBusiness EmployeeHistoryStatusBusiness;
        private readonly IEmployeeEvaluationBusiness EmployeeEvaluationBusiness;
        private readonly IEvaluationLevelBusiness EvaluationLevelBusiness;
        private readonly IEvaluationFieldBusiness EvaluationFieldBusiness;
        public TeacherGradingController(ITeacherGradingBusiness teachergradingBusiness, ISchoolFacultyBusiness schoolFacultyBusiness, 
            ITeacherGradeBusiness teacherGradeBusiness, IEmployeeBusiness employeeBusiness,
            ISchoolProfileBusiness schoolProfileBusiness, IAcademicYearBusiness academicYearBusiness,
            IEmployeeHistoryStatusBusiness employeeHistoryStatusBusiness, IEmployeeEvaluationBusiness employeeEvaluationBusiness,
            IEvaluationLevelBusiness evaluationLevelBusiness, IEvaluationFieldBusiness evaluationFieldBusiness)
		{
			this.TeacherGradingBusiness = teachergradingBusiness;
            this.SchoolFacultyBusiness = schoolFacultyBusiness;
            this.TeacherGradeBusiness = teacherGradeBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.EmployeeHistoryStatusBusiness = employeeHistoryStatusBusiness;
            this.EmployeeEvaluationBusiness = employeeEvaluationBusiness;
            this.EvaluationLevelBusiness = evaluationLevelBusiness;
            this.EvaluationFieldBusiness = evaluationFieldBusiness;
		}
		
		//
        // GET: /TeacherGrading/
        #region Index
        public ActionResult Index()
        {
            SetViewData();
            Search(0);
            return View();
        }
        #endregion
        //
        // GET: /TeacherGrading/Search
        #region Search
        
        public ActionResult Search(int? FacultyID)
        {
            if (FacultyID == null)
            {
                FacultyID = 0;
            }
            // Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["SchoolFacultyID"] = FacultyID;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            ViewData[TeacherGradingConstants.HAS_PERMISSION] = GetMenupermission("TeacherGrading", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            IEnumerable<EmployeeEvaluationViewModel> lst = this._Search(FacultyID);
            ViewData[TeacherGradingConstants.LIST_TEACHERGRADING] = lst;

            //SetViewData();

            return PartialView("_List");
        }
        #endregion
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(FormCollection frm)
        {
            List<int> lstTeacherID = new List<int>();
            int FacultyID = !string.IsNullOrEmpty(frm["hdfFacultyID"]) ? int.Parse(frm["hdfFacultyID"]) : 0;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);

            //lay danh sach giao vien cua truong
            List<Employee> lstEmployee = (from e in EmployeeBusiness.All
                                          join ehs in EmployeeHistoryStatusBusiness.All on e.EmployeeID equals ehs.EmployeeID
                                          where e.SchoolID == _globalInfo.SchoolID
                                          && (e.EmploymentStatus == SMAS.Web.Constants.GlobalConstants.EMPLOYEE_STATUS_WORKING
                                              || e.EmploymentStatus == SMAS.Web.Constants.GlobalConstants.EMPLOYEE_STATUS_BREATHER)//dang lam viec,tam nghi
                                          && (e.SchoolFacultyID == FacultyID || FacultyID == 0)
                                          && (ehs.FromDate <= objAca.SecondSemesterEndDate && (ehs.ToDate == null || ehs.ToDate >= objAca.FirstSemesterStartDate))
                                          && e.IsActive
                                          select e).ToList();

            List<EvaluationField> lstEvaluationField = EvaluationFieldBusiness.All.Where(p => p.IsActive).OrderBy(p => p.OrderID).ToList();
            EvaluationField objEvaluationField = null;
            List<int> lstEvaluationFieldID = lstEvaluationField.Select(p => p.EvaluationFieldId).Distinct().ToList();
            List<EvaluationLevel> lstEvaluationLevel = EvaluationLevelBusiness.All.Where(p => lstEvaluationFieldID.Contains(p.EvaluationField) && p.IsActive).ToList();

            Employee objEmployee = null;
            List<EmployeeEvaluation> lstTeacherGrading = new List<EmployeeEvaluation>();
            EmployeeEvaluation objTeacherGrading = null;
            int teacherID = 0;
            string chk_TeacherID = string.Empty;
            int SupervisingDeptID = 0;
            if (_globalInfo.EmployeeID.HasValue)
            {
                SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                SupervisingDeptID = objSP.SupervisingDeptID.Value;
            }
            else
            {
                SupervisingDeptID = _globalInfo.SupervisingDeptID.Value;
            }

            for (int i = 0; i < lstEvaluationField.Count; i++)
            {
                objEvaluationField = lstEvaluationField[i];
                for (int j = 0; j < lstEmployee.Count; j++)
                {
                    objEmployee = lstEmployee[j];
                    teacherID = objEmployee.EmployeeID;
                    chk_TeacherID = frm["chk_" + teacherID.ToString()];
                    if (chk_TeacherID == null)
                    {
                        continue;
                    }
                    else
                    {
                        objTeacherGrading = new EmployeeEvaluation();
                        objTeacherGrading.SchoolId = _globalInfo.SchoolID.Value;
                        objTeacherGrading.AcademicYearId = _globalInfo.AcademicYearID.Value;
                        objTeacherGrading.EmployeeId = teacherID;
                        objTeacherGrading.EvaluaionFieldId = objEvaluationField.EvaluationFieldId;
                        objTeacherGrading.CreatedDate = DateTime.Now;
                        objTeacherGrading.IsActive = true;
                        objTeacherGrading.SupervisingDeptId = SupervisingDeptID;
                        if (objEvaluationField.FieldType == 1)
                        {
                            objTeacherGrading.EvaluationLevelId = frm[objEvaluationField.EvaluationFieldId + "_" + objEmployee.EmployeeID] != null ?
                            int.Parse(frm[objEvaluationField.EvaluationFieldId + "_" + objEmployee.EmployeeID]) : 0;
                            objTeacherGrading.EvaluationText = "";
                        }
                        else
                        {
                            objTeacherGrading.EvaluationText = frm[objEvaluationField.EvaluationFieldId + "_" + objEmployee.EmployeeID];
                        }
                        lstTeacherGrading.Add(objTeacherGrading);
                    }
                }
            }

            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
            };
            EmployeeEvaluationBusiness.InsertOrUpdateEmployeeEvaluation(lstTeacherGrading, dic);

            return Json(new JsonMessage(Res.Get("Common_Label_TeacherGrading_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int TeacherGradingID)
        {
            this.CheckPermissionForAction(TeacherGradingID, "TeacherGrading");
            if (this.GetMenupermission("TeacherGrading", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            using (TransactionScope scopce = new TransactionScope())
            {
                TeacherGrading teachergrading = this.TeacherGradingBusiness.Find(TeacherGradingID);
                TryUpdateModel(teachergrading);
                Utils.Utils.TrimObject(teachergrading);
                this.TeacherGradingBusiness.Update(teachergrading);
                this.TeacherGradingBusiness.Save();
                scopce.Complete();
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(string strEmployeeID)
        {
            List<int> lstEmployeeID = strEmployeeID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<Employee> lstEmployee = (from e in EmployeeBusiness.All
                                          join ehs in EmployeeHistoryStatusBusiness.All on e.EmployeeID equals ehs.EmployeeID
                                          where e.SchoolID == _globalInfo.SchoolID
                                          && lstEmployeeID.Contains(e.EmployeeID)
                                          && (e.EmploymentStatus == SMAS.Web.Constants.GlobalConstants.EMPLOYEE_STATUS_WORKING
                                              || e.EmploymentStatus == SMAS.Web.Constants.GlobalConstants.EMPLOYEE_STATUS_BREATHER)//dang lam viec,tam nghi
                                          && (ehs.FromDate <= objAca.SecondSemesterEndDate && (ehs.ToDate == null || ehs.ToDate >= objAca.FirstSemesterStartDate))
                                          && e.IsActive
                                          select e).ToList();
            lstEmployeeID = lstEmployee.Select(p => p.EmployeeID).Distinct().ToList();
            IDictionary<string, object> dicSearch = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"lstEmployeeID",lstEmployeeID}
            };
            EmployeeEvaluationBusiness.DeleteEmployeeEvaluation(dicSearch);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private List<EmployeeEvaluationViewModel> _Search(int? FacultyID)
        {
            if (!FacultyID.HasValue)
            {
                FacultyID = 0;
            }
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            //lay danh sach giao vien cua truong
            List<Employee> lstEmployee = (from e in EmployeeBusiness.All
                                          join ehs in EmployeeHistoryStatusBusiness.All on e.EmployeeID equals ehs.EmployeeID
                                          where e.SchoolID == _globalInfo.SchoolID
                                              //&& e.AppliedLevel == _globalInfo.AppliedLevel
                                          && (e.EmploymentStatus == SMAS.Web.Constants.GlobalConstants.EMPLOYEE_STATUS_WORKING
                                              || e.EmploymentStatus == SMAS.Web.Constants.GlobalConstants.EMPLOYEE_STATUS_BREATHER)//dang lam viec,tam nghi
                                          && (e.SchoolFacultyID == FacultyID || FacultyID == 0)
                                          && (ehs.FromDate <= objAca.SecondSemesterEndDate && (ehs.ToDate == null || ehs.ToDate >= objAca.FirstSemesterStartDate))
                                          && e.IsActive
                                          select e).ToList();
            List<int> lstEmployeeID = lstEmployee.Select(p=>p.EmployeeID).Distinct().ToList();
            //lay danh sach danh gia giao vien
            var lstTeacherEvaluation = (from te in EmployeeEvaluationBusiness.All
                                        where te.SchoolId == _globalInfo.SchoolID
                                        && te.AcademicYearId == _globalInfo.AcademicYearID
                                        && lstEmployeeID.Contains(te.EmployeeId)
                                        select new
                                        {
                                            EmployeeID = te.EmployeeId,
                                            EvaluationLevelID = te.EvaluationLevelId,
                                            EvaluationFielID = te.EvaluaionFieldId,
                                            EvaluationText = te.EvaluationText
                                        }).ToList();
            List<EvaluationField> lstEvaluationField = EvaluationFieldBusiness.All.Where(p => p.IsActive).OrderBy(p=>p.OrderID).ToList();
            EvaluationField objEvaluationField = null;
            List<int> lstEvaluationFieldID = lstEvaluationField.Select(p => p.EvaluationFieldId).Distinct().ToList();
            List<EvaluationLevel> lstEvaluationLevel = EvaluationLevelBusiness.All.Where(p => lstEvaluationFieldID.Contains(p.EvaluationField) && p.IsActive).ToList();
            List<EmployeeEvaluationViewModel> lstResult = new List<EmployeeEvaluationViewModel>();
            EmployeeEvaluationViewModel objResult = null;
            Employee objEmployee = null;
            List<EvaluationFieldModel> lstEvaluationFielModel = new List<EvaluationFieldModel>();
            EvaluationFieldModel objEFM = null;
            for (int i = 0; i < lstEmployee.Count; i++)
            {
                objResult = new EmployeeEvaluationViewModel();
                objEmployee = lstEmployee[i];
                objResult.EmployeeID = objEmployee.EmployeeID;
                objResult.FullName = objEmployee.FullName;
                objResult.Name = objEmployee.Name;
                objResult.EmployeeCode = objEmployee.EmployeeCode;
                lstEvaluationFielModel = new List<EvaluationFieldModel>();
                for (int j = 0; j < lstEvaluationField.Count; j++)
                {
                    objEvaluationField = lstEvaluationField[j];
                    var objTeacherEvaluation = lstTeacherEvaluation.Where(p => p.EmployeeID == objEmployee.EmployeeID 
                        && p.EvaluationFielID == objEvaluationField.EvaluationFieldId).FirstOrDefault();
                    if (objTeacherEvaluation != null)
                    {
                        objEFM = new EvaluationFieldModel();
                        objEFM.EmployeeID = objEmployee.EmployeeID;
                        objEFM.EvaluationFieldID = objEvaluationField.EvaluationFieldId;
                        objEFM.EvaluationLevelID = objTeacherEvaluation.EvaluationLevelID;
                        objEFM.EvaluationText = objTeacherEvaluation.EvaluationText;
                        lstEvaluationFielModel.Add(objEFM);
                    }
                }
                objResult.lstEvaluationField = lstEvaluationFielModel;
                lstResult.Add(objResult);
            }

            if (lstResult.Count > 0 && lstResult != null)
            {
                lstResult = lstResult.OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.Name + " " + o.FullName)).ToList();
            }
            ViewData[TeacherGradingConstants.LIST_EVALUATION_FIELD] = lstEvaluationField;
            ViewData[TeacherGradingConstants.LIST_EVALUATION_LEVEL] = lstEvaluationLevel;
            return lstResult;
        }

        #region SetViewData
        private void SetViewData()
        {
            IDictionary<string, object> FacultySearchInfo = new Dictionary<string, object>();
            FacultySearchInfo["IsActive"] = true;
            FacultySearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            List<SchoolFaculty> lsSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, FacultySearchInfo).OrderBy(o => o.FacultyName).ToList();

            ViewData[TeacherGradingConstants.HAS_PERMISSION] = GetMenupermission("TeacherGrading", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            ViewData[TeacherGradingConstants.LIST_FACULTY] = new SelectList(lsSchoolFaculty, "SchoolFacultyID", "FacultyName");

            //cbb TeacherGrade
            List<TeacherGradeBO> lsTeacherGrade = this.GetListTeacherGrade();
            ViewData[TeacherGradingConstants.LIST_TEACHERGRADE] = new SelectList(lsTeacherGrade, "TeacherGradeID", "GradeResolution");
        
        }
        private List<TeacherGradeBO> GetListTeacherGrade()
        {
            IDictionary<string, object> TeacherGradeSearchInfo = new Dictionary<string, object>();
            TeacherGradeSearchInfo["IsActive"] = true;
            List<TeacherGradeBO> lsTeacherGrade = (from t in TeacherGradeBusiness.Search(TeacherGradeSearchInfo)
                                                   select new TeacherGradeBO
                                                   {
                                                       TeacherGradeID = t.TeacherGradeID,
                                                       GradeResolution = t.GradeResolution,
                                                       Description = t.Description,
                                                       IsActive = t.IsActive
                                                   }).ToList();
            foreach (TeacherGradeBO item in lsTeacherGrade)
            {
                if (item.GradeResolution.ToUpper().Equals("TỐT"))
                {
                    item.Order = 1;
                }
                else if (item.GradeResolution.ToUpper().Equals("KHÁ"))
                {
                    item.Order = 2;
                }
                else if (item.GradeResolution.ToUpper().Equals("TB"))
                {
                    item.Order = 3;
                }
                else if (item.GradeResolution.ToUpper().Equals("YẾU"))
                {
                    item.Order = 4;
                }
                else
                {
                    item.Order = 5;
                }
            }
            lsTeacherGrade = lsTeacherGrade.OrderBy(p => p.Order).ToList();
            return lsTeacherGrade;
        }
        #endregion

        #region Load Combobox

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadTeacherOfFaculty(int? SchoolFacultyId)
        {
            if (_globalInfo.SchoolID == null)
            {
                _globalInfo.SchoolID = 4;
            }
            IEnumerable<Employee> lst = new List<Employee>();
            if (SchoolFacultyId.ToString() != "")
            {
                lst = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(_globalInfo.SchoolID.Value, SchoolFacultyId.Value).OrderBy(o => o.Name).ThenBy(o => o.FullName);
            }
            if (lst == null)
                lst = new List<Employee>();
            return Json(new SelectList(lst, "EmployeeID", "FullName"),JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Export + Import
        
        public FileResult ExportExcel(int FacultyID)
        {
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/GV/" + "XepLoaiCanBo.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            //fill thông tin chung
            sheet.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value).ToUpper());
            sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            SchoolProfile objSchoolProfile = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            SchoolFaculty objSchoolFaculty = SchoolFacultyBusiness.Find(FacultyID);
            string DateName = (objSchoolProfile.District != null ? objSchoolProfile.District.DistrictName : "") + ", ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Date.Month + " năm " + DateTime.Now.Date.Year;
            sheet.SetCellValue("F4", DateName);
            string FacultyName = "";
            if (FacultyID > 0)
            {
                FacultyName = "Tổ " + objSchoolFaculty.FacultyName + ", Năm học " + objAcademicYear.DisplayTitle;
            }
            else
            {
                FacultyName = "Năm học " + objAcademicYear.DisplayTitle;
            }
            sheet.SetCellValue("A7", FacultyName);
            List<EmployeeEvaluationViewModel> lstEmployeeEvaluation = new List<EmployeeEvaluationViewModel>();
            EmployeeEvaluationViewModel objEmpoyeeEvaluation = null;
            lstEmployeeEvaluation = this._Search(FacultyID);
            List<EvaluationField> lstEvaluationField = EvaluationFieldBusiness.All.Where(p => p.IsActive).OrderBy(p => p.OrderID).ToList();
            EvaluationField objEvaluationField = null;
            List<int> lstEvaluationFieldID = lstEvaluationField.Select(p => p.EvaluationFieldId).Distinct().ToList();
            List<EvaluationLevel> lstEvaluationLevel = EvaluationLevelBusiness.All.Where(p => lstEvaluationFieldID.Contains(p.EvaluationField) && p.IsActive).ToList();
            List<EvaluationLevel> lstELtmp = new List<EvaluationLevel>();
            EvaluationLevel objELtmp = null;
            EvaluationLevel objEL = null;
            List<EvaluationFieldModel> lsttmp = new List<EvaluationFieldModel>();
            EvaluationFieldModel objtmp = null;
            int startRow = 10;
            int startCol = 4;
            string[] cd = new string[] { "" };
            List<VTDataValidation> lstValidation = new List<VTDataValidation>();
            VTDataValidation objValidation = null;
            //Fill tieu de
            for (int j = 0; j < lstEvaluationField.Count; j++)
            {
                objEvaluationField = lstEvaluationField[j];
                sheet.SetCellValue(9,startCol, objEvaluationField.Resolution);
                sheet.SetCellValue(10000, startCol, objEvaluationField.EvaluationFieldId);//dung cho import
                if (objEvaluationField.FieldType == 1)
                {
                    lstELtmp = lstEvaluationLevel.Where(p => p.EvaluationField == objEvaluationField.EvaluationFieldId).ToList();
                    cd = new string[lstELtmp.Count];
                    for (int k = 0; k < lstELtmp.Count; k++)
                    {
                        objELtmp = lstELtmp[k];
                        cd[k] = objELtmp.Resolution;
                    }
                    objValidation = new VTDataValidation()
                    {
                        Contrains = cd,
                        SheetIndex = 1,
                        FromRow = 9,
                        ToRow = 9 + lstEmployeeEvaluation.Count,
                        FromColumn = startCol,
                        ToColumn = startCol,
                        Type = 3
                    };
                    lstValidation.Add(objValidation);
                }
                startCol++;
            }
            sheet.SetRowHeight(10000,0);
            for (int i = 0; i < lstEmployeeEvaluation.Count; i++)
            {
                objEmpoyeeEvaluation = lstEmployeeEvaluation[i];
                //STT
                sheet.SetCellValue(startRow, 1, i + 1);
                //Ma can bo
                sheet.SetCellValue(startRow, 2, objEmpoyeeEvaluation.EmployeeCode);
                //Ho va ten
                sheet.SetCellValue(startRow, 3, objEmpoyeeEvaluation.FullName);
                lsttmp = objEmpoyeeEvaluation.lstEvaluationField;
                startCol = 4;
                for (int j = 0; j < lstEvaluationField.Count; j++)
                {
                    objEvaluationField = lstEvaluationField[j];
                    sheet.GetRange(startRow, startCol, startCol, startCol).IsLock = false;
                    objtmp = lsttmp.Where(p => p.EvaluationFieldID == objEvaluationField.EvaluationFieldId).FirstOrDefault();
                    if (objtmp != null)
                    {
                        if (objEvaluationField.FieldType == 1)
                        {
                            objEL = lstEvaluationLevel.Where(p => p.EvaluationLevelId == objtmp.EvaluationLevelID).FirstOrDefault();
                            sheet.SetCellValue(startRow, startCol, objEL.Resolution);
                        }
                        else
                        {
                            sheet.SetCellValue(startRow, startCol, objtmp.EvaluationText);
                        }
                    }
                    startCol++;
                }
                startRow++;
            }
            //Ke khung
            sheet.GetRange(9, 1, startRow - 1, 3 + lstEvaluationField.Count).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            
            ////Fill Sheet2
            //startRow = 4;
            //startCol = 1;
            //for (int i = 0; i < lstEvaluationField.Count; i++)
            //{
            //    startRow = 4;
            //    objEvaluationField = lstEvaluationField[i];
            //    sheet2.SetCellValue(2, startCol, objEvaluationField.Resolution);
            //    lstELtmp = lstEvaluationLevel.Where(p => p.EvaluationField == objEvaluationField.EvaluationFieldId).ToList();
            //    for (int j = 0; j < lstELtmp.Count; j++)
            //    {
            //        objELtmp = lstELtmp[j];
            //        sheet2.SetCellValue(startRow, startCol, objELtmp.Resolution);
            //        startRow++;
            //    }
            //    startCol++;
            //}
            
            sheet.ProtectSheet();
            Stream excel = oBook.ToStreamValidationData(lstValidation);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "XepLoaiCanBo.xls";
            return result;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(int FacultyID)
        {
            // Luu file len server
            HttpPostedFileBase file = (HttpPostedFileBase)Session["stream"];
            if (file != null)
            {
                String FileName = string.Format("{0}-{1}-{2}{3}", Path.GetFileNameWithoutExtension(file.FileName), _globalInfo.UserAccountID, Guid.NewGuid().ToString(), Path.GetExtension(file.FileName));
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), FileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
            }

            string FilePath = (string)Session["FilePath"];
            IVTWorkbook oBook = VTExport.OpenWorkbook(FilePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            //check file
            if (FacultyID > 0)
            {
                string title = string.Empty;
                string FacultyName = string.Empty;
                title = sheet.GetCellValue("A7").ToString();
                SchoolFaculty objFaculty = SchoolFacultyBusiness.Find(FacultyID);
                FacultyName = objFaculty != null ? objFaculty.FacultyName : "";
                if (!title.ToUpper().Contains(FacultyName.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("File Excel không phải là file của " + FacultyName), "error"));
                }
            }
            List<EmployeeEvaluationViewModel> lstEvaluationViewModel = new List<EmployeeEvaluationViewModel>();
            EmployeeEvaluationViewModel objViewModel = null;
            List<EvaluationFieldModel> lstEvaluationFiledViewModel = new List<EvaluationFieldModel>();
            EvaluationFieldModel objEFVM = null;
            //lay danh sach giao vien cua truong
            List<Employee> lstEmployee = (from e in EmployeeBusiness.All
                                          join ehs in EmployeeHistoryStatusBusiness.All on e.EmployeeID equals ehs.EmployeeID
                                          where e.SchoolID == _globalInfo.SchoolID
                                          && (e.EmploymentStatus == SMAS.Web.Constants.GlobalConstants.EMPLOYEE_STATUS_WORKING
                                              || e.EmploymentStatus == SMAS.Web.Constants.GlobalConstants.EMPLOYEE_STATUS_BREATHER)//dang lam viec,tam nghi
                                          && (e.SchoolFacultyID == FacultyID || FacultyID == 0)
                                          && (ehs.FromDate <= objAca.SecondSemesterEndDate && (ehs.ToDate == null || ehs.ToDate >= objAca.FirstSemesterStartDate))
                                          && e.IsActive
                                          select e).ToList();
            List<EvaluationField> lstEvaluationField = EvaluationFieldBusiness.All.Where(p => p.IsActive).OrderBy(p => p.OrderID).ToList();
            EvaluationField objEvaluationField = null;
            List<int> lstEvaluationFieldID = lstEvaluationField.Select(p => p.EvaluationFieldId).Distinct().ToList();
            List<EvaluationLevel> lstEvaluationLevel = EvaluationLevelBusiness.All.Where(p => lstEvaluationFieldID.Contains(p.EvaluationField) && p.IsActive).ToList();
            List<EvaluationLevel> lstELtmp = new List<EvaluationLevel>();
            EvaluationLevel objELtmp = null;

            Employee objEmployee = null;
            string EmployeeCode = string.Empty;
            string ErrMessage = string.Empty;
            bool isErr = false;
            int EvaluationFieldID = 0;
            string EvaluationLevelName = string.Empty;
            List<string> lstEmployeeCode = new List<string>();
            int startRow = 10;
            int startCol = 4;
            //Doc du lieu tu file Excel
            while (sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null)
            {
                objViewModel = new EmployeeEvaluationViewModel();
                ErrMessage = string.Empty;
                isErr = false;
                EmployeeCode = sheet.GetCellValue(startRow, 2) != null ? sheet.GetCellValue(startRow, 2).ToString() : "";//ma can bo
                if (string.IsNullOrEmpty(EmployeeCode))
                {
                    ErrMessage += "Mã cán bộ không được để trống.<br/>";
                    isErr = true;
                }
                else if (!lstEmployeeCode.Contains(EmployeeCode))
                {
                    objEmployee = lstEmployee.Where(p => p.EmployeeCode.Equals(EmployeeCode)).FirstOrDefault();
                    if (objEmployee == null)
                    {
                        ErrMessage += "Mã cán bộ không tồn tại.<br/>";
                        isErr = true;
                    }
                    else
                    {
                        objViewModel.EmployeeID = objEmployee.EmployeeID;
                    }
                    lstEmployeeCode.Add(EmployeeCode);
                }
                else
                {
                    ErrMessage += "Mã cán bộ không được trùng.<br/>";
                    isErr = true;
                }

                objViewModel.FullName = sheet.GetCellValue(startRow, 3) != null ? sheet.GetCellValue(startRow, 3).ToString() : "";
                objViewModel.EmployeeCode = EmployeeCode;
                startCol = 4;
                lstEvaluationFiledViewModel = new List<EvaluationFieldModel>();
                for (int i = 0; i < lstEvaluationField.Count; i++)
                {
                    objEvaluationField = lstEvaluationField[i];
                    objEFVM = new EvaluationFieldModel();
                    EvaluationLevelName = sheet.GetCellValue(startRow, startCol) != null ? sheet.GetCellValue(startRow, startCol).ToString() : "";
                    EvaluationFieldID = sheet.GetCellValue(10000, startCol) != null ? int.Parse(sheet.GetCellValue(10000, startCol).ToString()) : 0;
                    objEFVM.EmployeeID = objEmployee.EmployeeID;
                    objEFVM.EvaluationLevelName = EvaluationLevelName;
                    objEFVM.IsField = true;
                    if (EvaluationFieldID == 0 || !lstEvaluationFieldID.Contains(EvaluationFieldID))
                    {
                        return Json(new JsonMessage(Res.Get("File Excel không đúng định dạng"), "error"));
                    }
                    if (objEvaluationField.FieldType == 1)
                    {
                        lstELtmp = lstEvaluationLevel.Where(p => p.EvaluationField == objEvaluationField.EvaluationFieldId).ToList();
                        if (!string.IsNullOrEmpty(EvaluationLevelName))
                        {
                            objELtmp = lstELtmp.Where(p => p.Resolution.Equals(EvaluationLevelName)).FirstOrDefault();
                            if (objELtmp != null)
                            {
                                objEFVM.EvaluationFieldID = objEvaluationField.EvaluationFieldId;
                                objEFVM.EvaluationLevelID = objELtmp.EvaluationLevelId;
                            }
                            else
                            {
                                ErrMessage += "Giá trị " + objEvaluationField.Resolution + " không tồn tại";
                                isErr = true;
                                objEFVM.EvaluationFieldID = objEvaluationField.EvaluationFieldId;
                                objEFVM.IsField = false;
                            }
                        }
                    }
                    else
                    {
                        objEFVM.EvaluationFieldID = objEvaluationField.EvaluationFieldId;
                        objEFVM.EvaluationText = EvaluationLevelName;
                    }
                    objEFVM.FieldType = objEvaluationField.FieldType;
                    lstEvaluationFiledViewModel.Add(objEFVM);
                    startCol++;
                }
                objViewModel.ErrorMessage = ErrMessage;
                objViewModel.IsError = isErr;
                objViewModel.lstEvaluationField = lstEvaluationFiledViewModel;
                lstEvaluationViewModel.Add(objViewModel);
                startRow++;
            }

            if (lstEvaluationViewModel.Where(p => p.IsError == true).Count() > 0)
            {
                ViewData[TeacherGradingConstants.LIST_TEACHERGRADING_ERR] = lstEvaluationViewModel;
                ViewData[TeacherGradingConstants.LIST_EVALUATION_FIELD] = lstEvaluationField;
                return Json(new JsonMessage(RenderPartialViewToString("_ViewImportError", null), "ImportError"));
            }
            else
            {
                //tao doi tuong TeacherGrading
                List<EmployeeEvaluation> lstEmployeeEvaluation = new List<EmployeeEvaluation>();
                EmployeeEvaluation objEmployeeEvaluation = null;
                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    objEmployee = lstEmployee[i];
                    objViewModel = lstEvaluationViewModel.Where(p => p.EmployeeID == objEmployee.EmployeeID).FirstOrDefault();
                    if (objViewModel != null)
                    {
                        lstEvaluationFiledViewModel = objViewModel.lstEvaluationField;
                        for (int j = 0; j < lstEvaluationFiledViewModel.Count; j++)
                        {
                            objEFVM = lstEvaluationFiledViewModel[j];
                            if (objEFVM.EvaluationLevelID > 0 || !string.IsNullOrEmpty(objEFVM.EvaluationText))
                            {
                                objEmployeeEvaluation = new EmployeeEvaluation();
                                objEmployeeEvaluation.SchoolId = _globalInfo.SchoolID.Value;
                                objEmployeeEvaluation.AcademicYearId = _globalInfo.AcademicYearID.Value;
                                objEmployeeEvaluation.EmployeeId = objEmployee.EmployeeID;
                                objEmployeeEvaluation.EvaluaionFieldId = objEFVM.EvaluationFieldID;
                                objEmployeeEvaluation.CreatedDate = DateTime.Now;
                                objEmployeeEvaluation.IsActive = true;
                                objEmployeeEvaluation.SupervisingDeptId = _globalInfo.SupervisingDeptID.Value;
                                if (objEFVM.FieldType == 1)
                                {
                                    objEmployeeEvaluation.EvaluationLevelId = objEFVM.EvaluationLevelID;
                                    objEmployeeEvaluation.EvaluationText = "";
                                }
                                else
                                {
                                    objEmployeeEvaluation.EvaluationText = objEFVM.EvaluationText;
                                }
                                lstEmployeeEvaluation.Add(objEmployeeEvaluation);
                            }
                        }
                    }
                }

                if (lstEmployeeEvaluation.Count > 0)
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>()
                    {
                        {"SchoolID",_globalInfo.SchoolID},
                        {"AcademicYearID",_globalInfo.AcademicYearID}
                    };
                    EmployeeEvaluationBusiness.InsertOrUpdateEmployeeEvaluation(lstEmployeeEvaluation, dic);
                }
                return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage"), "success"));
            }
            
        }

        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            var file = attachments.FirstOrDefault();
            Session["stream"] = file;
            if (file != null)
            {
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res.ContentType = "text/plain";
                    return res;
                }
                JsonResult res1 = Json(new JsonMessage());
                res1.ContentType = "text/plain";
                return res1;

            }
            JsonResult res2 = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            res2.ContentType = "text/plain";
            return res2;
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion
    }
}





