/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.TeacherGradingArea
{
    public class TeacherGradingConstants
    {
        public const string LIST_TEACHERGRADING = "listTeacherGrading";
        public const string LIST_TEACHERGRADING_ERR = "listTeacherGradingErr";
        public const string LIST_FACULTY = "listFaculty";
        public const string LIST_TEACHEROFFACULTY = "listTeacherOfFaculty";
        
        public const string LIST_TEACHERGRADE = "listTeacherGrade";
        public const string HAS_PERMISSION = "HAS_PERMISSION";
        public const string LIST_EVALUATION_FIELD = "listEvaluationField";
        public const string LIST_EVALUATION_LEVEL = "listEvaluationLevel";
    }
}