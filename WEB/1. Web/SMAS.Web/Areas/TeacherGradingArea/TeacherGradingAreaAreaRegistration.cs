﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TeacherGradingArea
{
    public class TeacherGradingAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TeacherGradingArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TeacherGradingArea_default",
                "TeacherGradingArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
