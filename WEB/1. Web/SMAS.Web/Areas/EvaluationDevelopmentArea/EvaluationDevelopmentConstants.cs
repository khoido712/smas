/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.EvaluationDevelopmentArea
{
    public class EvaluationDevelopmentConstants
    {
        public const string LIST_EVALUATIONDEVELOPMENT = "listEvaluationDevelopment";
        public const string LIST_EducationLevel = "LIST_EducationLevel";
        public const string List_EvaluationDevelopmentGroup = "EvaluationDevelopmentGroup";
        public const string List_Input = "List_Input";
        public const string InputType = "InputType";
        public const int ChildCare = 4;
        public const int PresSchool = 5;
    }
}