﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.EvaluationDevelopmentArea.Models;

using SMAS.Models.Models;
using SMAS.VTUtils.HtmlHelpers;

namespace SMAS.Web.Areas.EvaluationDevelopmentArea.Controllers
{
    public class EvaluationDevelopmentController : BaseController
    {
        private readonly IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness;

        public EvaluationDevelopmentController(IEvaluationDevelopmentBusiness evaluationdevelopmentBusiness, IEducationLevelBusiness educationLevelBusiness,
            IEvaluationDevelopmentGroupBusiness evaluationDevelopmentGroupBusiness)
        {
            this.EvaluationDevelopmentBusiness = evaluationdevelopmentBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.EvaluationDevelopmentGroupBusiness = evaluationDevelopmentGroupBusiness;
        }

        //
        // GET: /EvaluationDevelopment/

        public ActionResult Index()
        {
            //Đưa dữ liệu ra combobox khối học
            IDictionary<string, object> EducationLevelSearchInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            EducationLevelSearchInfo["Grade"] = new GlobalInfo().AppliedLevel;
            int ChildCare = EvaluationDevelopmentConstants.ChildCare;
            int PresSchool = EvaluationDevelopmentConstants.PresSchool;
            List<EducationLevel> LstEducationLevel = EducationLevelBusiness.Search(EducationLevelSearchInfo).Where(p=>p.Grade == ChildCare || p.Grade == PresSchool).ToList();
            ViewData[EvaluationDevelopmentConstants.LIST_EducationLevel] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            //Đưa dữ liệu ra combobox nhóm tiêu chí
            IDictionary<string, object> EvaluationDevelopmentGroupSearchInfo = new Dictionary<string, object>();
            EvaluationDevelopmentGroupSearchInfo["IsActive"] = true;
            EvaluationDevelopmentGroupSearchInfo["AppliedLevel"] = new GlobalInfo().AppliedLevel;
            List<EvaluationDevelopmentGroup> LstEvaluationDevelopmentGroup = EvaluationDevelopmentGroupBusiness.Search(EvaluationDevelopmentGroupSearchInfo).ToList();
            ViewData[EvaluationDevelopmentConstants.List_EvaluationDevelopmentGroup] = new SelectList(LstEvaluationDevelopmentGroup, "EvaluationDevelopmentGroupID", "EvaluationDevelopmentGroupName");

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            IEnumerable<EvaluationDevelopmentViewModel> lst = this._Search(SearchInfo);
            ViewData[EvaluationDevelopmentConstants.LIST_EVALUATIONDEVELOPMENT] = lst;
            ViewData[EvaluationDevelopmentConstants.InputType] = null;
            return View();
        }

        //
        // GET: /EvaluationDevelopment/Search


        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["EducationLevelID"] = frm.EducationLevelID;
            SearchInfo["EvaluationDevelopmentGroupID"] = frm.EvaluationDevelopmentGroupID;
            SearchInfo["EvaluationDevelopmentName"] = frm.EvaluationDevelopmentName;
            SearchInfo["EvaluationDevelopmentCode"] = frm.EvaluationDevelopmentCode;
            IEnumerable<EvaluationDevelopmentViewModel> lst = this._Search(SearchInfo);
            ViewData[EvaluationDevelopmentConstants.LIST_EVALUATIONDEVELOPMENT] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(EvaluationDevelopmentViewModel frm)
        {
            //if (ModelState.IsValid)
            //{ 
            if (frm.Description != null)
            {
                if (frm.Description.Length > 400)
                {
                    return Json(new JsonMessage(Res.Get("EvaluationDevelopment_Label_DescriptionMaxlength"), "error"));
                }
            }
            if (frm.EducationLevelID == 0)
            {
                return Json(new JsonMessage(Res.Get("EvaluationDevelopment_Label_EducationLevelRequired"), "error"));
            }
            if (frm.EvaluationDevelopmentGroupID == 0)
            {
                return Json(new JsonMessage(Res.Get("EvaluationDevelopment_Label_EvaluationDevelopmentGroupRequired"), "error"));
            }
            EvaluationDevelopment evaluationdevelopment = new EvaluationDevelopment();
            TryUpdateModel(evaluationdevelopment);
            Utils.Utils.TrimObject(evaluationdevelopment);
            evaluationdevelopment.CreatedDate = DateTime.Now;
            evaluationdevelopment.IsActive = true;
            //evaluationdevelopment.InputType = (int)frm.rdoInput;
            this.EvaluationDevelopmentBusiness.Insert(evaluationdevelopment);
            this.EvaluationDevelopmentBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            //}
            //string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            //return Json(new JsonMessage(jsonErrList, "error"));
        }
        public PartialViewResult _Create()
        {
            //Đưa dữ liệu ra combobox khối học
            IDictionary<string, object> EducationLevelSearchInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            EducationLevelSearchInfo["Grade"] = new GlobalInfo().AppliedLevel;
            int ChildCare = EvaluationDevelopmentConstants.ChildCare;
            int PresSchool = EvaluationDevelopmentConstants.PresSchool;
            List<EducationLevel> LstEducationLevel = EducationLevelBusiness.Search(EducationLevelSearchInfo).Where(p=>p.Grade == ChildCare || p.Grade == PresSchool).ToList();
            ViewData[EvaluationDevelopmentConstants.LIST_EducationLevel] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            //Đưa dữ liệu ra combobox nhóm tiêu chí
            //IDictionary<string, object> EvaluationDevelopmentGroupSearchInfo = new Dictionary<string, object>();
            //EvaluationDevelopmentGroupSearchInfo["IsActive"] = true;
            //EvaluationDevelopmentGroupSearchInfo["AppliedLevel"] = new GlobalInfo().AppliedLevel;
            //List<EvaluationDevelopmentGroup> LstEvaluationDevelopmentGroup = EvaluationDevelopmentGroupBusiness.Search(EvaluationDevelopmentGroupSearchInfo).ToList();
            ViewData[EvaluationDevelopmentConstants.List_EvaluationDevelopmentGroup] = new SelectList(new List<EvaluationDevelopmentGroup>(), "EvaluationDevelopmentGroupID", "EvaluationDevelopmentGroupName");
            //Dua ra Radio input                            
            List<ViettelCheckboxList> listInput = new List<ViettelCheckboxList>();
            listInput.Add(new ViettelCheckboxList(Res.Get("EvaluationDevelopment_Label_StringType"), SystemParamsInFile.EVALUATION_DEVELOPMENT_TYPE_STRING, true, false));
            listInput.Add(new ViettelCheckboxList(Res.Get("EvaluationDevelopment_Label_NumberType"), SystemParamsInFile.EVALUATION_DEVELOPMENT_TYPE_NUMBER, false, false));
            listInput.Add(new ViettelCheckboxList(Res.Get("EvaluationDevelopment_Label_DateType"), SystemParamsInFile.EVALUATION_DEVELOPMENT_TYPE_DATE, false, false));
            ViewData[EvaluationDevelopmentConstants.List_Input] = listInput;

            return PartialView("_Create");
        }		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int EvaluationDevelopmentID, EvaluationDevelopmentViewModel frm)
        {
            //if (ModelState.IsValid)
            //{
                EvaluationDevelopment evaluationdevelopment = this.EvaluationDevelopmentBusiness.Find(EvaluationDevelopmentID);

                TryUpdateModel(evaluationdevelopment);
                Utils.Utils.TrimObject(evaluationdevelopment);
                evaluationdevelopment.ModifiedDate = DateTime.Now;
                if (evaluationdevelopment.InputType != SystemParamsInFile.EVALUATION_DEVELOPMENT_TYPE_NUMBER)
                {
                    evaluationdevelopment.InputMin = null;
                    evaluationdevelopment.InputMax = null;
                }
                if (frm.Description != null)
                {
                    if (frm.Description.Length > 400)
                    {
                        return Json(new JsonMessage(Res.Get("EvaluationDevelopment_Label_DescriptionMaxlength"), "error"));
                    }
                }
                if (frm.EducationLevelID == 0)
                {
                    return Json(new JsonMessage(Res.Get("EvaluationDevelopment_Label_EducationLevelRequired"), "error"));
                }
                if (frm.EvaluationDevelopmentGroupID == 0)
                {
                    return Json(new JsonMessage(Res.Get("EvaluationDevelopment_Label_EvaluationDevelopmentGroupRequired"), "error"));
                }
                this.EvaluationDevelopmentBusiness.Update(evaluationdevelopment);
                this.EvaluationDevelopmentBusiness.Save();

                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            //}
            //string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            //return Json(new JsonMessage(jsonErrList, "error"));
        }
        public PartialViewResult _Edit(int id)
        {

            return PartialView("_Edit", PrepareEdit(id));
        }
        private EvaluationDevelopmentViewModel PrepareEdit(int id)
        {
            GlobalInfo global = new GlobalInfo();
            EvaluationDevelopment EvaluationDevelopment = this.EvaluationDevelopmentBusiness.Find(id);
            //Đưa dữ liệu ra combobox khối học
            int grade = EducationLevelBusiness.Find(EvaluationDevelopment.EducationLevelID).Grade;
            IDictionary<string, object> EducationLevelSearchInfo = new Dictionary<string, object>();
            EducationLevelSearchInfo["IsActive"] = true;
            EducationLevelSearchInfo["Grade"] = grade;
            int ChildCare = EvaluationDevelopmentConstants.ChildCare;
            int PresSchool = EvaluationDevelopmentConstants.PresSchool;
            List<EducationLevel> LstEducationLevel = EducationLevelBusiness.Search(EducationLevelSearchInfo).Where(p=>p.Grade == ChildCare || p.Grade == PresSchool).ToList();
            ViewData[EvaluationDevelopmentConstants.LIST_EducationLevel] = new SelectList(LstEducationLevel, "EducationLevelID", "Resolution");
            //Đưa dữ liệu ra combobox nhóm tiêu chí
            IDictionary<string, object> EvaluationDevelopmentGroupSearchInfo = new Dictionary<string, object>();
            EvaluationDevelopmentGroupSearchInfo["IsActive"] = true;
            EvaluationDevelopmentGroupSearchInfo["AppliedLevel"] = grade;
            List<EvaluationDevelopmentGroup> LstEvaluationDevelopmentGroup = EvaluationDevelopmentGroupBusiness.Search(EvaluationDevelopmentGroupSearchInfo).ToList();
            ViewData[EvaluationDevelopmentConstants.List_EvaluationDevelopmentGroup] = new SelectList(LstEvaluationDevelopmentGroup, "EvaluationDevelopmentGroupID", "EvaluationDevelopmentGroupName");
            //Dua ra Radio input                            
            List<ViettelCheckboxList> listInput = new List<ViettelCheckboxList>();
            bool TypeString = false;
            bool TypeNumber = false;
            bool TypeDate = false;
            if (EvaluationDevelopment.InputType == SystemParamsInFile.EVALUATION_DEVELOPMENT_TYPE_STRING)
            { TypeString = true; }
            else
                if (EvaluationDevelopment.InputType == SystemParamsInFile.EVALUATION_DEVELOPMENT_TYPE_NUMBER)
                { TypeNumber = true; }
                else { TypeDate = true; }
            listInput.Add(new ViettelCheckboxList(Res.Get("EvaluationDevelopment_Label_StringType"), SystemParamsInFile.EVALUATION_DEVELOPMENT_TYPE_STRING, TypeString, false));
            listInput.Add(new ViettelCheckboxList(Res.Get("EvaluationDevelopment_Label_NumberType"), SystemParamsInFile.EVALUATION_DEVELOPMENT_TYPE_NUMBER, TypeNumber, false));
            listInput.Add(new ViettelCheckboxList(Res.Get("EvaluationDevelopment_Label_DateType"), SystemParamsInFile.EVALUATION_DEVELOPMENT_TYPE_DATE, TypeDate, false));
            ViewData[EvaluationDevelopmentConstants.List_Input] = listInput;
            ViewData[EvaluationDevelopmentConstants.InputType] = EvaluationDevelopment.InputType.ToString();
            EvaluationDevelopmentViewModel frm = new EvaluationDevelopmentViewModel();
            Utils.Utils.BindTo(EvaluationDevelopment, frm);
            return frm;
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.EvaluationDevelopmentBusiness.Delete(id);
            this.EvaluationDevelopmentBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<EvaluationDevelopmentViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            int AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            IQueryable<EvaluationDevelopment> query = this.EvaluationDevelopmentBusiness.Search(SearchInfo);
            IQueryable<EvaluationDevelopmentViewModel> lst = query.Select(o => new EvaluationDevelopmentViewModel
            {
                EvaluationDevelopmentID = o.EvaluationDevelopmentID,
                EvaluationDevelopmentCode = o.EvaluationDevelopmentCode,
                EvaluationDevelopmentName = o.EvaluationDevelopmentName,
                Description = o.Description,
                IsRequired = o.IsRequired,
                InputType = o.InputType,
                InputMin = o.InputMin,
                InputMax = o.InputMax,
                EducationLevelID = o.EducationLevelID,
                EvaluationDevelopmentGroupID = o.EvaluationDevelopmentGroupID,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate
            }).OrderBy(x=>x.EvaluationDevelopmentName.ToLower());
            List<EvaluationDevelopmentViewModel> lst1 = lst.ToList();
            foreach (var item in lst1)
            {
                if (item.InputType == SystemParamsInFile.EVALUATION_DEVELOPMENT_TYPE_STRING)
                {
                    item.InputTypeName = Res.Get("EvaluationDevelopment_Label_StringType");
                }
                if (item.InputType == SystemParamsInFile.EVALUATION_DEVELOPMENT_TYPE_NUMBER)
                {
                    item.InputTypeName = Res.Get("EvaluationDevelopment_Label_NumberType");
                }
                if (item.InputType == SystemParamsInFile.EVALUATION_DEVELOPMENT_TYPE_DATE)
                {
                    item.InputTypeName = Res.Get("EvaluationDevelopment_Label_DateType");
                }
            }
            return lst1.ToList();
        }
        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadEvaluationGroup(int? EducationLevelID)
        {
            if (EducationLevelID.HasValue && EducationLevelID > 0)
            {
                int AppliedLevel = EducationLevelBusiness.Find(EducationLevelID).Grade;
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AppliedLevel"] = AppliedLevel;
                dic["IsActive"] = true;
                IQueryable<EvaluationDevelopmentGroup> lst = EvaluationDevelopmentGroupBusiness.Search(dic);
                return Json(new SelectList(lst.ToList(), "EvaluationDevelopmentGroupID", "EvaluationDevelopmentGroupName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new List<EvaluationDevelopmentGroup>(), "EvaluationDevelopmentGroupID", "EvaluationDevelopmentGroupName"), JsonRequestBehavior.AllowGet);
            }
        }
    }
}





