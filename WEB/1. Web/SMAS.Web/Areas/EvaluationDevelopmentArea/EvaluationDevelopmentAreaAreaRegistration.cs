﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EvaluationDevelopmentArea
{
    public class EvaluationDevelopmentAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EvaluationDevelopmentArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EvaluationDevelopmentArea_default",
                "EvaluationDevelopmentArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
