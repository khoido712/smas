/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.EvaluationDevelopmentArea.Models
{
    public class EvaluationDevelopmentViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 EvaluationDevelopmentID { get; set; }

        [ResourceDisplayName("EvaluationDevelopment_Label_EvaluationDevelopmentCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String EvaluationDevelopmentCode { get; set; }

        [ResourceDisplayName("EvaluationDevelopment_Label_EvaluationDevelopmentName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String EvaluationDevelopmentName { get; set; }

        [ResourceDisplayName("EvaluationDevelopment_Label_Description")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Boolean IsRequired { get; set; }

        [ResourceDisplayName("EvaluationDevelopment_Label_InputType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 InputType { get; set; }

        [ResourceDisplayName("EvaluationDevelopment_Label_InputMin")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [RegularExpression(@"^[0-9]+(\,[0-9]{1})?$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public System.Decimal? InputMin { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [ResourceDisplayName("EvaluationDevelopment_Label_InputMax")]
        [RegularExpression(@"^[0-9]+(\,[0-9]{1})?$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotNumber")]
        public System.Decimal? InputMax { get; set; }


        [ResourceDisplayName("EvaluationDevelopment_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 EducationLevelID { get; set; }

        [ResourceDisplayName("EvaluationDevelopment_Label_EvaluationDevelopmentGroup")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Int32 EvaluationDevelopmentGroupID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.DateTime CreatedDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Boolean IsActive { get; set; }
        public System.Nullable<System.DateTime> ModifiedDate { get; set; }
        [ResourceDisplayName("EvaluationDevelopment_Label_InputType")]
        public System.String InputTypeName { get; set; }


    }
}


