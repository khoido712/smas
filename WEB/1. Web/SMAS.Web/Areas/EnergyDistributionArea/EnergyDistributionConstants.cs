/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.EnergyDistributionArea
{
    public class EnergyDistributionConstants
    {
        public const string LIST_ENERGYDISTRIBUTION = "listEnergyDistribution";
        public const string LIST_EATINGGROUP = "LIST_EATINGGROUP";
        public const string LIST_MEALCAT = "LIST_MEALCAT";
    }
}