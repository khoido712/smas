﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.EnergyDistributionArea.Models;

using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.EnergyDistributionArea.Controllers
{
    public class EnergyDistributionController : BaseController
    {        
        private readonly IEnergyDistributionBusiness EnergyDistributionBusiness;
        private readonly IEatingGroupBusiness EatingGroupBusiness;
        private readonly IMealCatBusiness MealCatBusiness;

        public EnergyDistributionController(IEnergyDistributionBusiness energydistributionBusiness, IEatingGroupBusiness EatingGroupBusiness, IMealCatBusiness MealCatBusiness)
		{
			this.EnergyDistributionBusiness = energydistributionBusiness;
            this.EatingGroupBusiness = EatingGroupBusiness;
            this.MealCatBusiness = MealCatBusiness;
		}
		
		//
        // GET: /EnergyDistribution/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here

            //Danh sach nhom an
             SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            IEnumerable<EatingGroupBO> listEatingGroupFull = EatingGroupBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            IEnumerable<int?> listEating = listEatingGroupFull.Select(o=>o.EatingGroupID).Distinct();
            List<EatingGroupBO> listEatingGroup = new List<EatingGroupBO>();
            foreach (int? item in listEating)
            {
                EatingGroupBO EatingGroupBO = listEatingGroupFull.Where(o => o.EatingGroupID == item.Value).FirstOrDefault();
                listEatingGroup.Add(EatingGroupBO);
            }

            ViewData[EnergyDistributionConstants.LIST_EATINGGROUP] = listEatingGroup;


            //-	Danh sách các bữa ăn: 

            IDictionary<string, object> dic = new Dictionary<string, object>();
            //IEnumerable<MealCat> listMealCat = MealCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic);
            List<MealCat> listMealCat = MealCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).ToList();

            ViewData[EnergyDistributionConstants.LIST_MEALCAT] = listMealCat;

            //	Danh sách phân bổ năng lượng theo bữa ăn được lấy từ hàm 
            IEnumerable<EnergyDistribution> listEnergyDistribution = EnergyDistributionBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);

            List<EnergyDistributionViewModel> listEnergyDistributionViewModel= new List<EnergyDistributionViewModel>();
            if (listEatingGroup != null && listEatingGroup.Count() > 0)
            {
                EatingGroupBO eatingGroup = null;
                MealCat mealCat = null;
                //sử dụng for() thay cho forach() cải thiện tốc độ
                for (int k = 0,tempMealCat = listMealCat.Count; k < tempMealCat; k++)
                {
                    mealCat = listMealCat[k];
                //}
                //foreach (MealCat MealCat in listMealCat)
                //{
                    EnergyDistributionViewModel EnergyDistributionViewModel = new EnergyDistributionViewModel();
                    EnergyDistributionViewModel.MealName = mealCat.MealName;
                    EnergyDistributionViewModel.MealID = mealCat.MealCatID;
                    EnergyDistributionViewModel.EnergyItem = new EnergyItem[listEatingGroup.Count()];
                    int i = 0;
                    for (int j = 0,tempEatingGroup = listEatingGroup.Count; j < tempEatingGroup; j++)
                    {
                        eatingGroup = listEatingGroup[j];
                    //}
                    //foreach (EatingGroupBO EatingGroup in listEatingGroup)
                    //{
                        IEnumerable<EnergyDistribution> listEnergyitem = listEnergyDistribution.Where(o => o.MealID == mealCat.MealCatID && o.EatingGroupID == eatingGroup.EatingGroupID);
                        EnergyDistributionViewModel.EnergyItem[i] = new EnergyItem();
                        if (listEnergyitem == null || listEnergyitem.Count() == 0)
                        {
                            EnergyDistributionViewModel.EnergyItem[i].Disable = "Disabled";
                            EnergyDistributionViewModel.EnergyItem[i].Checked = "";
                        }
                        else
                        {
                            EnergyDistribution EnergyDistribution = listEnergyitem.FirstOrDefault();
                            EnergyDistributionViewModel.EnergyItem[i].Disable = "";
                            EnergyDistributionViewModel.EnergyItem[i].EnergyDemandFrom = EnergyDistribution.EnergyDemandFrom;
                            EnergyDistributionViewModel.EnergyItem[i].EnergyDemandTo = EnergyDistribution.EnergyDemandTo;
                            EnergyDistributionViewModel.EnergyItem[i].Checked = "Checked";
                        }
                        i++;
                    }
                    listEnergyDistributionViewModel.Add(EnergyDistributionViewModel);
                }
            }

            ViewData[EnergyDistributionConstants.LIST_ENERGYDISTRIBUTION] = listEnergyDistributionViewModel;
            Search();

            return View();
        }

		//
        // GET: /EnergyDistribution/Search

        
        public PartialViewResult Search()
        {
            
            
			//add search info
			//


            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Create(FormCollection form)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Get view data here
            List<EnergyDistribution> listEnergyDistribution = new List<EnergyDistribution>();
            //Danh sach nhom an
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            IEnumerable<EatingGroupBO> listEatingGroupFull = EatingGroupBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            IEnumerable<int?> listEating = listEatingGroupFull.Select(o => o.EatingGroupID).Distinct();
            List<EatingGroupBO> listEatingGroup = new List<EatingGroupBO>();
            foreach (int? item in listEating)
            {
                EatingGroupBO EatingGroupBO = listEatingGroupFull.Where(o => o.EatingGroupID == item.Value).FirstOrDefault();
                listEatingGroup.Add(EatingGroupBO);
            }

            IDictionary<string, object> dic = new Dictionary<string, object>();
            //IEnumerable<MealCat> listMealCat = MealCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic);
            List<MealCat> listMealCat = MealCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).ToList();
            if (listEatingGroup != null && listEatingGroup.Count>0 && listMealCat!=null && listMealCat.Count()>0)
            {
                EatingGroupBO eatingGroupBO = null;
                MealCat mealCat = null;
                //sử dụng for() thay cho foreach() cải thiện tốc độ
                for (int i = 0,tempEatingGroup = listEatingGroup.Count; i < tempEatingGroup; i++)
                {
                    eatingGroupBO = listEatingGroup[i];
                //}

                //foreach (EatingGroupBO EatingGroupBO in listEatingGroup)
                //{
                    for (int j = 0,tempMealCat = listMealCat.Count; j < tempMealCat; j++)
                    {
                        mealCat = listMealCat[j];
                    //}
                    //foreach (MealCat MealCat in listMealCat)
                    //{
                        String obj = mealCat.MealCatID.ToString()+"_"+eatingGroupBO.EatingGroupID.Value.ToString();
                        String checkbox = form[obj + "_Check"];
                        if (checkbox == null)
                        {
                            continue;
                        }
                        else if (checkbox.Equals("on"))
                        {
                            Decimal EnergyDemandFrom = SMAS.Business.Common.Utils.GetDecimal(form[obj + "_EnergyDemandFrom"]);
                            Decimal EnergyDemandTo = SMAS.Business.Common.Utils.GetDecimal(form[obj + "_EnergyDemandTo"]);
                            EnergyDistribution ed = new EnergyDistribution();
                            ed.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
                            ed.EatingGroupID = eatingGroupBO.EatingGroupID.Value;
                            ed.EnergyDemandFrom = EnergyDemandFrom;
                            ed.EnergyDemandTo = EnergyDemandTo;
                            ed.SchoolID = new GlobalInfo().SchoolID.Value;
                            ed.MealID = mealCat.MealCatID;
                            ed.IsActive = true;
                            listEnergyDistribution.Add(ed);
                        }
                        
                    }
                }
            }
            string messagerView = "";
            if (_globalInfo.IsCurrentYear)
            {
                messagerView = "Common_Label_UpdateSuccessMessage";
            }
            else
            {
                messagerView = "ClassAssigment_Error_IsCurrentYearNoSave";
            }
            if (listEnergyDistribution != null && listEnergyDistribution.Count > 0)
            {
                if (_globalInfo.IsCurrentYear)
                {
                    EnergyDistributionBusiness.Insert(listEnergyDistribution, new GlobalInfo().SchoolID.Value);
                    EnergyDistributionBusiness.Save();
                }
                else
                {
                    return Json(new JsonMessage(Res.Get(messagerView)));
                }
            }
            return Json(new JsonMessage(Res.Get(messagerView)));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int EnergyDistributionID)
        {
            EnergyDistribution energydistribution = this.EnergyDistributionBusiness.Find(EnergyDistributionID);
            TryUpdateModel(energydistribution);
            Utils.Utils.TrimObject(energydistribution);
            this.EnergyDistributionBusiness.Update(energydistribution);
            this.EnergyDistributionBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.EnergyDistributionBusiness.Delete(id);
            this.EnergyDistributionBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		

    }
}





