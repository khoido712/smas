﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EnergyDistributionArea
{
    public class EnergyDistributionAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EnergyDistributionArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EnergyDistributionArea_default",
                "EnergyDistributionArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
