/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.EnergyDistributionArea.Models
{
    public class EnergyDistributionViewModel
    {
		public System.Int32 EnergyDistributionID { get; set; }

        public String MealName { get; set; }
        public EnergyItem[] EnergyItem { get; set; }
        public int MealID { get; set; }
    }
}


