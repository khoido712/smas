﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EnergyDistributionArea.Models
{
    public class EnergyItem
    {
        public System.Decimal? EnergyDemandFrom { get; set; }
        public System.Decimal? EnergyDemandTo { get; set; }
        public String Disable { get; set; }
        public String Checked { get; set; }
    }
}