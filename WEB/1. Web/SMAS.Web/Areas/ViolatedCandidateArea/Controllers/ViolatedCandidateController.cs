﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ViolatedCandidateArea.Models;
using SMAS.Web.Filter;

using SMAS.Models.Models;
using System.IO;

namespace SMAS.Web.Areas.ViolatedCandidateArea.Controllers
{
    public class ViolatedCandidateController : BaseController
    {

        private readonly IViolatedCandidateBusiness ViolatedCandidateBusiness;
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IExamViolationTypeBusiness ExamViolationTypeBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly ICandidateBusiness CandidateBusiness;
        private readonly IExaminationRoomBusiness ExaminationRoomBusiness;
        public readonly IReportDefinitionBusiness ReportDefinitionBusiness;

        public ViolatedCandidateController(IViolatedCandidateBusiness violatedcandidateBusiness,
                                            IExaminationBusiness examinationBusiness,
                                            IExamViolationTypeBusiness examViolationTypeBusiness,
                                            IExaminationSubjectBusiness examinationSubjectBusiness,
                                            ICandidateBusiness candidateBusiness,
                                            IExaminationRoomBusiness examinationRoomBusiness,
                                            IReportDefinitionBusiness reportDefinitionBusiness)
        {
            this.ViolatedCandidateBusiness = violatedcandidateBusiness;
            this.ExaminationBusiness = examinationBusiness;
            this.ExamViolationTypeBusiness = examViolationTypeBusiness;
            this.ExaminationSubjectBusiness = examinationSubjectBusiness;
            this.CandidateBusiness = candidateBusiness;
            this.ExaminationRoomBusiness = examinationRoomBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
        }

        public ActionResult Index()
        {
            ViewData[ViolatedCandidateConstants.LIST_VIOLATED_CANDIDATE] = new List<ViolatedCandidateViewModel>();
            GetViewData();
            return View();
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo glo = new GlobalInfo();
                Utils.Utils.TrimObject(frm);

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

                SearchInfo.Add("EducationLevelID", frm.EducationLevelID.HasValue && frm.EducationLevelID.Value > 0 ? frm.EducationLevelID.Value : 0);
                SearchInfo.Add("ExaminationID", frm.ExaminationID);
                SearchInfo.Add("ExaminationSubjectID", frm.ExaminationSubjectID.HasValue && frm.ExaminationSubjectID.Value > 0 ? frm.ExaminationSubjectID.Value : 0);
                SearchInfo.Add("ExamViolationTypeID", frm.ExamViolationTypeID.HasValue && frm.ExamViolationTypeID.Value > 0 ? frm.ExamViolationTypeID.Value : 0);
                SearchInfo.Add("NamedListCode", !string.IsNullOrEmpty(frm.NamedListCode) ? frm.NamedListCode : "");
                SearchInfo.Add("PupilCode", !string.IsNullOrEmpty(frm.PupilCode) ? frm.PupilCode : "");
                SearchInfo.Add("PupilName", !string.IsNullOrEmpty(frm.PupilName) ? frm.PupilName : "");
                SearchInfo.Add("AcademicYearID", glo.AcademicYearID.Value);

                ViewData[ViolatedCandidateConstants.LIST_VIOLATED_CANDIDATE] = this._Search(SearchInfo);

                Examination exam = ExaminationBusiness.Find(frm.ExaminationID);
                ViewData[ViolatedCandidateConstants.ENABLE_DELETE] = exam != null && (exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED);
            }
            else
            {
                ViewData[ViolatedCandidateConstants.LIST_VIOLATED_CANDIDATE] = new List<ViolatedCandidateViewModel>();
                ViewData[ViolatedCandidateConstants.ENABLE_DELETE] = false;
            }
            return PartialView("_List");
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Create(ViolatedCandidateCreateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.CandidateID != null && model.CandidateID.Length > 0)
                {
                    GlobalInfo glo = new GlobalInfo();
                    //Lấy các bản ghi được check trong grvRessult -> List<int>  ListCandidateID
                    ViolatedCandidateBusiness.InsertAll(glo.SchoolID.Value, glo.AppliedLevel.Value, model.CreateViolationDetail, model.CreateExamViolationTypeID, model.CandidateID.ToList());
                    ViolatedCandidateBusiness.Save();
                    return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("Violated_Candidate_NoRecord"), JsonMessage.ERROR));
                }
            }
            else
            {
                throw new BusinessException(string.Join(",", ModelState.Where(u => u.Value.Errors.Count > 0).SelectMany(u => u.Value.Errors).Select(u => u.ErrorMessage)));
            }
        }

        [HttpPost]
        public JsonResult CheckPermission()
        {
            GlobalInfo glo = new GlobalInfo();
            // Examination exam = ExaminationBusiness.Find(examinationID);
            bool canCreate = glo.IsCurrentYear;
            return Json(new { canCreate = canCreate });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ViolatedCandidateID)
        {
            ViolatedCandidate violatedcandidate = this.ViolatedCandidateBusiness.Find(ViolatedCandidateID);
            CheckPermissionForAction(violatedcandidate.ExaminationID.Value, "Examination");
            TryUpdateModel(violatedcandidate);
            Utils.Utils.TrimObject(violatedcandidate);
            this.ViolatedCandidateBusiness.Update(violatedcandidate);
            this.ViolatedCandidateBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.ViolatedCandidateBusiness.Delete(id);
            this.ViolatedCandidateBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadExamSubject(int? educationLevelID, int? examinationID)
        {
            GlobalInfo glo = new GlobalInfo();
            List<ExaminationSubject> lstExamSubject = new List<ExaminationSubject>();
            if (educationLevelID.HasValue && educationLevelID.Value > 0 && examinationID.HasValue && examinationID.Value > 0)
            {
                Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
                dicExaminationSubject.Add("AcademicYearID", glo.AcademicYearID.Value);
                dicExaminationSubject.Add("AppliedLevel", glo.AppliedLevel.Value);
                dicExaminationSubject.Add("EducationLevelID", educationLevelID);
                dicExaminationSubject.Add("ExaminationID", examinationID);
                lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            }
            return Json(lstExamSubject.Select(u => new ComboObject(u.ExaminationSubjectID.ToString(), u.SubjectCat.SubjectName)).ToList());
        }


        [ValidateAntiForgeryToken]
        public JsonResult LoadRoom(int examSubjectID)
        {
            List<ExaminationRoom> lstRooms = ExaminationRoomBusiness.All.Where(u => u.ExaminationSubjectID == examSubjectID).ToList();
            return Json(lstRooms.Select(u => new ComboObject(u.ExaminationRoomID.ToString(), u.RoomTitle)).ToList());
        }

        [ValidateAntiForgeryToken]
        public JsonResult LoadEducationLevel(int? examinationID)
        {
            GlobalInfo glo = new GlobalInfo();
            Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            if (examinationID.HasValue && examinationID.Value > 0)
                dicExaminationSubject.Add("ExaminationID", examinationID);
            else
            {
                Json(new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            var lstEdu = lstExamSubject.Select(u => new { u.EducationLevelID, u.EducationLevel.Resolution }).Distinct().OrderBy(p=>p.EducationLevelID);

            return Json(new SelectList(lstEdu, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
        }


        [ValidateAntiForgeryToken]
        public JsonResult GridAction(int[] ViolatedCandidates)
        {
            if (ViolatedCandidates != null && ViolatedCandidates.Length > 0)
            {
                GlobalInfo glo = new GlobalInfo();
                ViolatedCandidateBusiness.DeleteAll(glo.SchoolID.Value, glo.AppliedLevel.Value, ViolatedCandidates.ToList());
                this.ViolatedCandidateBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
            }
            else
            {
                return Json(new JsonMessage(Res.Get("Common_Label_DeleteFailureMessage")));
            }
        }


        [ValidateAntiForgeryToken]
        public JsonResult CheckPermision(int examinationID)
        {
            Examination exam = ExaminationBusiness.Find(examinationID);
            GlobalInfo glo = new GlobalInfo();
            bool enabled = exam != null && exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED && glo.IsCurrentYear;
            return Json(new { enabled = enabled });
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult LoadCandidate(int roomId)
        {
            GlobalInfo glo = new GlobalInfo();
            ViewData[ViolatedCandidateConstants.LIST_CANDIDATE] = CandidateBusiness.SearchBySchool(glo.SchoolID.Value,
                                                                                                    new Dictionary<string, object>
                                                                                                    {
                                                                                                        {"RoomID" , roomId},
                                                                                                        {"IsAbsence", false}
                                                                                                    }).Select(u => new CandidateGridViewModel
                                                                                                                    {
                                                                                                                        CandidateID = u.CandidateID,
                                                                                                                        NamedListNumber = u.NamedListNumber,
                                                                                                                        Name = u.PupilProfile.Name,
                                                                                                                        FullName = u.PupilProfile.FullName,
                                                                                                                        PupilCode = u.PupilCode
                                                                                                                    }).ToList().OrderBy(o => o.NamedListNumber).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            return PartialView("_Candidates");
        }

        private void GetViewData()
        {
            GlobalInfo glo = new GlobalInfo();

            Dictionary<string, object> dicExamination = new Dictionary<string, object>();
            dicExamination.Add("AcademicYearID", glo.AcademicYearID.Value);
            dicExamination.Add("AppliedLevel", glo.AppliedLevel.Value);
            IQueryable<Examination> lstQExam = ExaminationBusiness.SearchBySchool(glo.SchoolID.Value, dicExamination);
            List<Examination> lstExam = lstQExam.Where(u => u.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED
                                    || u.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED
                                    || u.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_FINISHED).OrderByDescending(u => u.ToDate).ToList();

            List<Examination> lstExamCreate = lstExam.Where(u => u.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED).OrderByDescending(u => u.ToDate).ToList();

            ViewData[ViolatedCandidateConstants.LIST_EDUCATION_LEVEL] = new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution");
            ViewData[ViolatedCandidateConstants.LIST_EXAMINATION] = new SelectList(lstExam, "ExaminationID", "Title");
            ViewData[ViolatedCandidateConstants.LIST_EXAMINATION_CREATE] = new SelectList(lstExamCreate, "ExaminationID", "Title");

            //Dictionary<string, object> dicExaminationSubject = new Dictionary<string, object>();
            //dicExaminationSubject.Add("AcademicYearID", glo.AcademicYearID.Value);
            //dicExaminationSubject.Add("AppliedLevel", glo.AppliedLevel.Value);

            //List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(glo.SchoolID.Value, dicExaminationSubject).ToList();
            List<ExaminationSubject> lstExamSubject = new List<ExaminationSubject>();
            ViewData[ViolatedCandidateConstants.LIST_EXAMINATION_SUBJECT] = new SelectList(lstExamSubject.Select(u => new { u.ExaminationSubjectID, u.SubjectCat.SubjectName }).ToList(), "ExaminationSubjectID", "SubjectName");

            List<ExamViolationType> lstExamViolationType = ExamViolationTypeBusiness.SearchBySchool(glo.SchoolID.Value, new Dictionary<string, object> { { "AppliedForCandidate", true } }).OrderBy(p=>p.Resolution).ToList();
            ViewData[ViolatedCandidateConstants.LIST_EXAMVIOLATION_TYPE] = new SelectList(lstExamViolationType, "ExamViolationTypeID", "Resolution");

            //List<ExaminationRoom> lstRooms = ExaminationRoomBusiness.All.ToList();
            //ViewData[ViolatedCandidateConstants.LIST_ROOM] = new SelectList(lstRooms, "ExaminationRoomID", "RoomTitle");

            Examination exam = lstExam.FirstOrDefault();
            ViewData[ViolatedCandidateConstants.ENABLE_CREATE] = exam != null && glo.IsCurrentYear && exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED;
            ViewData[ViolatedCandidateConstants.ENABLE_DELETE] = false;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadEducationLevel(int? idExamination)
        {
            GlobalInfo global = new GlobalInfo();
            if (!idExamination.HasValue)
            {
                return Json(new SelectList(new List<EducationLevel>(), "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            IQueryable<EducationLevel> lsEducationLevel = ExaminationSubjectBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",global.AcademicYearID.Value},
                {"AppliedLevel",global.AppliedLevel.Value},
                {"ExaminationID",idExamination.Value}
            }).Select(o => o.EducationLevel).Distinct();
            lsEducationLevel = lsEducationLevel.OrderBy(o => o.EducationLevelID);
            return Json(new SelectList(lsEducationLevel, "EducationLevelID", "Resolution"), JsonRequestBehavior.AllowGet);

        }

        private IEnumerable<ViolatedCandidateViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo glo = new GlobalInfo();
            IQueryable<ViolatedCandidate> query = this.ViolatedCandidateBusiness.SearchBySchool(glo.SchoolID.Value, SearchInfo);
            IQueryable<ViolatedCandidateViewModel> lst = query.Select(o => new ViolatedCandidateViewModel
            {
                BirthDay = o.PupilProfile.BirthDate,
                CandidateID = o.CandidateID,
                EducationLevelID = o.EducationLevelID.Value,
                EducationLevelResolution = o.EducationLevel.Resolution,
                ExaminationID = o.ExaminationID,
                ExaminationTitle = o.Examination.Title,
                ExamViolationTypeID = o.ExamViolationTypeID.Value,
                ExamViolationTypeResolution = o.ExamViolationType.Resolution,
                Genre = o.PupilProfile.Genre,
                NameListCode = o.Candidate.NamedListCode,
                PupilCode = o.PupilCode,
                PupilFullName = o.PupilProfile.FullName,
                NameListNumber = o.Candidate.NamedListNumber,
                Name = o.PupilProfile.Name,
                PupilID = o.PupilID.Value,
                RoomID = o.RoomID.Value,
                RoomTitle = o.ExaminationRoom.RoomTitle,
                SubjectID = o.SubjectID.Value,
                SubjectName = o.ExaminationSubject.SubjectCat.SubjectName,
                ViolatedCandidateID = o.ViolatedCandidateID,
                ViolationDetail = o.ViolationDetail
            });

            return lst.ToList().OrderBy(o => o.NameListNumber).ThenBy(o => o.Name).ThenBy(o => o.PupilFullName).ToList();
        }

        public FileResult Export(SearchViewModel model)
        {
            if (ModelState.IsValid)
            {
                GlobalInfo GlobalInfo = new GlobalInfo();

                Utils.Utils.TrimObject(model);

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["EducationLevelID"] = model.EducationLevelID.HasValue && model.EducationLevelID.Value > 0 ? model.EducationLevelID.Value : 0;
                SearchInfo["ExaminationID"] = model.ExaminationID;
                SearchInfo["ExaminationSubjectID"] = model.ExaminationSubjectID.HasValue && model.ExaminationSubjectID.Value > 0 ? model.ExaminationSubjectID.Value : 0;
                SearchInfo["ExamViolationTypeID"] = model.ExamViolationTypeID.HasValue && model.ExamViolationTypeID.Value > 0 ? model.ExamViolationTypeID.Value : 0;
                SearchInfo["NamedListCode"] = !string.IsNullOrEmpty(model.NamedListCode) ? model.NamedListCode : "";
                SearchInfo["PupilCode"] = !string.IsNullOrEmpty(model.PupilCode) ? model.PupilCode : "";
                SearchInfo["PupilName"] = !string.IsNullOrEmpty(model.PupilName) ? model.PupilName : "";

                SearchInfo["SchoolID"] = GlobalInfo.SchoolID.Value;
                SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;

                string fileName = string.Empty;
                Stream stream = ViolatedCandidateBusiness.CreateViolatedCandidateReport(SearchInfo, out fileName);
                FileStreamResult result = new FileStreamResult(stream, "application/octet-stream");
                result.FileDownloadName = fileName;
                return result;
            }
            else
            {
                return null;
            }
        }
    }
}
