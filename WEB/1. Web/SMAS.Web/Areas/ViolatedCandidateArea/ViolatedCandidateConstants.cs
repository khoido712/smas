/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ViolatedCandidateArea
{
    public class ViolatedCandidateConstants
    {
        public const string LIST_EXAMINATION = "listExamination";
        public const string LIST_EDUCATION_LEVEL = "listEducationLevel";
        public const string LIST_EXAMINATION_SUBJECT = "listExaminationSubject";
        public const string LIST_EXAMVIOLATION_TYPE = "listExamViolationType";
        public const string LIST_VIOLATED_CANDIDATE = "listViolatedCandidate";
        public const string LIST_ROOM = "listRoom";
        public const string LIST_CANDIDATE = "listCandidate";

        public const string ENABLE_CREATE = "enable_create";
        public const string ENABLE_DELETE = "enable_delete";

        public const string LIST_EXAMINATION_CREATE = "listExaminationCreate";
    }
}