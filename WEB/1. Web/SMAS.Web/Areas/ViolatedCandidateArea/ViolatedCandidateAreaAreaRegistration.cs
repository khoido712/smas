﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ViolatedCandidateArea
{
    public class ViolatedCandidateAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ViolatedCandidateArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ViolatedCandidateArea_default",
                "ViolatedCandidateArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
