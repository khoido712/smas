/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ViolatedCandidateArea.Models
{
    public class ViolatedCandidateViewModel
    {
        [ScaffoldColumn(false)]
        public int ViolatedCandidateID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("DetachableHeadBag_Label_EducationLevel")]
        public int EducationLevelID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("Examination_Label_Title")]
        public int? ExaminationID { get; set; }

        public string ExaminationTitle { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("Candidate_Label_AllTitle")]
        public int CandidateID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("ExamViolationType_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int ExamViolationTypeID { get; set; }

        [ResourceDisplayName("ExamViolationType_Label_AllTitle")]
        public string ExamViolationTypeResolution { get; set; }

        [ResourceDisplayName("ViolatedCandidate_ViolationDetail")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(160, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ViolationDetail { get; set; }

        [ResourceDisplayName("DetachableHeadBag_Label_EducationLevel")]
        public string EducationLevelResolution { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        public string PupilCode { get; set; }
        
        [ScaffoldColumn(false)]
        [ResourceDisplayName("ExaminationRoom_Label_AllTitle")]
        public int RoomID { get; set; }

        [ResourceDisplayName("ExaminationRoom_Label_AllTitle")]
        public string RoomTitle { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("Candidate_Label_ExaminationSubject")]
        public int ExaminationSubjectID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("SubjectCat_Label_AllTitle")]
        public int SubjectID { get; set; }

        [ResourceDisplayName("SubjectCat_Label_AllTitle")]
        public string SubjectName { get; set; }

        [ScaffoldColumn(false)]
        public int PupilID { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        public string PupilFullName { get; set; }

        [ScaffoldColumn(false)]
        public string Name { get; set; }

        [ResourceDisplayName("PupilProfile_Label_BirthDate")]
        public DateTime BirthDay { get; set; }

        [ResourceDisplayName("PupilProfile_Label_Genre")]
        public int Genre { get; set; }

        [ResourceDisplayName("Candidate_Label_NamedListCode")]
        public string NameListCode { get; set; }

        [ScaffoldColumn(false)]
        public int? NameListNumber { get; set; }
    }
}

