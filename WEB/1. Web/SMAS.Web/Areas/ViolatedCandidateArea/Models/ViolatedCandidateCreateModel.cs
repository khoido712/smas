using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;

namespace SMAS.Web.Areas.ViolatedCandidateArea.Models
{
    public class ViolatedCandidateCreateModel
    {
        [ResourceDisplayName("DetachableHeadBag_Label_EducationLevel")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int CreateEducationLevelID { get; set; }

        [ResourceDisplayName("Examination_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int? CreateExaminationID { get; set; }

        [ResourceDisplayName("Candidate_Label_ExaminationSubject")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int CreateExaminationSubjectID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("ExaminationRoom_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int CreateRoomID { get; set; }

        [ResourceDisplayName("ViolatedCandidate_ViolationDetail")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(160, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string CreateViolationDetail { get; set; }

        [ResourceDisplayName("ExamViolationType_Label_AllTitle")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int CreateExamViolationTypeID { get; set; }

        public int[] CandidateID { get; set; }
    }
}
