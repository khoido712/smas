/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  hieund9
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ViolatedCandidateArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Examination_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public int ExaminationID { get; set; }

        [ResourceDisplayName("DetachableHeadBag_Label_EducationLevel")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("Candidate_Label_ExaminationSubject")]
        public int? ExaminationSubjectID {get;set;}

        [ResourceDisplayName("ExamViolationType_Label_AllTitle")]
        public int? ExamViolationTypeID { get; set; }

        [ResourceDisplayName("Candidate_Label_NamedListCode")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
        public string NamedListCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilCode { get; set; }

        [ResourceDisplayName("PupilProfile_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PupilName { get; set; }
    }
}