﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ViolatedCandidateArea.Models
{
    public class CandidateGridViewModel
    {
        public int CandidateID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public int? NamedListNumber { get; set; }
    }
}