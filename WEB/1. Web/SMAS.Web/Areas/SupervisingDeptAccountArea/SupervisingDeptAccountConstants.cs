﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SupervisingDeptAccountArea
{
    public class SupervisingDeptAccountConstants
    {
        public const string LIST_SUPERVISINGDEFT = "listSupervisingDept";
        public const string SUPERVISINGDEFTDEFT = "SupervisingDeptDept";
        public const string STATUS = "Status";
        public const string LIST_GENRE = "listgenre";
        public const string LISTROLEOFUSER = "supervisingdeptlistroleofuser";
        public const string LISTEMPLOYEE = "supervisingdeptlistemployee";
        public const int MALE = 1;
        public const int FEMALE = 0;
        public const string DISABLE_SUPERVISINGDEFT = "disableSupervisingDept";
        public const string MESSAGETYPE = "messagetype";
        public const string FORBACK = "FORBACK";
        
    }
}