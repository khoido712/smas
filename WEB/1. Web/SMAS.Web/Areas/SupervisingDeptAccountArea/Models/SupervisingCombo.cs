﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SupervisingDeptAccountArea.Models
{
    public class SupervisingCombo
    {
        public int SupervisingDeptID { get; set; }
        public string SupervisingDeptName { get; set; }
    }
}