﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using Resources;
using SMAS.Models.Models;

namespace SMAS.Web.Areas.SupervisingDeptAccountArea.Models
{
    public class SupervisingDeptAccountViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 UserAccountID { get; set; }

        [ResourceDisplayName("SupervisingDeptAccount_Label_UserName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String UserName { get; set; }

        [ResourceDisplayName("SupervisingDeptAccount_Label_PassWord")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(250, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String PassWord { get; set; }

        [ResourceDisplayName("SupervisingDeptAccount_Label_FullName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String FullName { get; set; }

        [ResourceDisplayName("SupervisingDeptAccount_Label_EmployeeCode")]
        public System.String EmployeeCode { get; set; }

        [ResourceDisplayName("SupervisingDeptAccount_Label_BirthDate")]
        public Nullable<System.DateTime> BirthDate { get; set; }

        [ResourceDisplayName("SupervisingDeptAccount_Label_GenreName")]
        public System.String GenreName { get; set; }

        [ScaffoldColumn(false)]
        public Nullable<System.Boolean> Genre { get; set; }
        [ScaffoldColumn(false)]
        public Nullable<System.Int32> GenreID { get; set; }

        [ResourceDisplayName("SupervisingDeptAccount_Label_Mobile")]
        public System.String Mobile { get; set; }

        [ResourceDisplayName("SupervisingDeptAccount_Label_SupervisingDeptName")]
        public System.String SupervisingDeptName { get; set; }

        [ScaffoldColumn(false)]
        public Nullable<System.DateTime> FirstLoginDate { get; set; }

        [ScaffoldColumn(false)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Boolean IsActive { get; set; }

        [ScaffoldColumn(false)]
        public SelectList ListGenres { get; set; }

        [ScaffoldColumn(false)]
        public UserAccount UserAccount { get; set; }

        [ScaffoldColumn(false)]
        public IQueryable<GroupCat> ListGroupOfRole { get; set; }

        [ScaffoldColumn(false)]
        public IQueryable<UserGroup> ListGroupOfSupervisingDeptAccount { get; set; }

        [ScaffoldColumn(false)]
        public List<Employee> ListEmployee { get; set; }

        [ScaffoldColumn(false)]
        public IQueryable<UserAccount> ListUserByDept { get; set; }

        [ScaffoldColumn(false)]
        public List<SelectListItem> ListRoleOfUser { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("SupervisingDeptAccount_Label_BirthDate")]
        public string BirthDay { get; set; }
         [ScaffoldColumn(false)]
        public aspnet_Membership aspnet_Membership { get; set; }

         [ScaffoldColumn(false)]
         public DateTime? CreateDate { get; set; }

         [ScaffoldColumn(false)]
         public bool displayButton { get; set; }
         
         [ScaffoldColumn(false)]
         public List<SelectListItem> ListEmployeeSupervisingDept { get; set; }

        [ScaffoldColumn(false)]
         public string BirthDateStr { get; set; }

        [ScaffoldColumn(false)]
        public string StaffPositionStr { get; set; }
        [ScaffoldColumn(false)]
        public int tempEmployeeID { get; set; }
        [ScaffoldColumn(false)]
        public int tempSupervisingdeptID { get; set; }
        [ResourceDisplayName("SchoolProfile_Label_Status")]
        public string StatusName { get; set; }
    }
}