﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;

namespace SMAS.Web.Areas.SupervisingDeptAccountArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("SupervisingDeptAccount_Label_UserName")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Username { get; set; }

        [ResourceDisplayName("SupervisingDeptAccount_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string FullName { get; set; }

        [ResourceDisplayName("SupervisingDeptAccount_Label_SupervisingDeptName")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SupervisingDeptAccountConstants.SUPERVISINGDEFTDEFT)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public Nullable<int> Dept { get; set; }
        [ResourceDisplayName("Trạng thái TK")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SupervisingDeptAccountConstants.STATUS)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public Nullable<int> StatusAccount { get; set; }

    }
}