﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SupervisingDeptAccountArea
{
    public class SupervisingDeptAccountAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SupervisingDeptAccountArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SupervisingDeptAccountArea_default",
                "SupervisingDeptAccountArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
