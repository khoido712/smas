﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Web.Areas.SupervisingDeptAccountArea.Models;
using SMAS.Models.Models;
using System.Web.Security;
using SMAS.Models.Models.CustomModels;
using SMAS.Business.Common;
using System.Text.RegularExpressions;
using SMAS.Web.Filter;
using SMAS.VTUtils.Excel.Export;
using System.IO;
namespace SMAS.Web.Areas.SupervisingDeptAccountArea.Controllers
{
    /// <summary>
    /// author: namdv3
    /// </summary>
    public class SupervisingDeptAccountController : BaseController
    {
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IGroupCatBusiness GroupCatBusiness;
        private readonly IUserGroupBusiness UserGroupBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        public const int MinLengthPassword = 8;
        public const int MaxLengthPassword = 128;
        public SupervisingDeptAccountController(ISupervisingDeptBusiness prioritytypeBusiness, IUserAccountBusiness userAccountBusiness, IGroupCatBusiness groupCatBusiness, IUserGroupBusiness userGroupBusiness, IEmployeeBusiness employeeBusiness)
        {
            SupervisingDeptBusiness = prioritytypeBusiness;
            UserAccountBusiness = userAccountBusiness;
            GroupCatBusiness = groupCatBusiness;
            UserGroupBusiness = userGroupBusiness;
            EmployeeBusiness = employeeBusiness;
        }

        public ActionResult Index(int? id, string type, int? LoadSS)
        {
            SetViewDataPermission("SupervisingDeptAccount", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();
            int? SupervisingDeptID = global.SupervisingDeptID;
            SearchInfo["IsActive"] = true;
            if (!id.HasValue)
            {
                SearchInfo["SupervisingDeptID"] = 0;
            }
            else
            {
                //UserAccount ua = UserAccountBusiness.Find(id);
                //Employee employee = EmployeeBusiness.Find(ua.EmployeeID);
                //SearchInfo["SupervisingDeptID"] = employee.SupervisingDeptID;
                Dictionary<string, object> dicMoi = (Dictionary<string, object>)Session["forBackAccount"];
                ViewData[SupervisingDeptAccountConstants.FORBACK] = dicMoi;
                string userName = dicMoi != null ? SMAS.Business.Common.Utils.GetString(dicMoi["UserName"]) : "";
                string fullName = dicMoi != null ? SMAS.Business.Common.Utils.GetString(dicMoi["FullName"]) : "";
                int? supervisingDeptID = dicMoi != null ? SMAS.Business.Common.Utils.GetInt(dicMoi["SupervisingDeptID"]) : 0;
                SearchInfo["UserName"] = userName;
                SearchInfo["FullName"] = fullName;
                SearchInfo["SupervisingDeptID"] = supervisingDeptID;
            }
            if (LoadSS == 1)
            {
                Dictionary<string, object> dicMoi = (Dictionary<string, object>)Session["forBackAccount"];
                ViewData[SupervisingDeptAccountConstants.FORBACK] = dicMoi;
                string userName = dicMoi != null ? SMAS.Business.Common.Utils.GetString(dicMoi["UserName"]) : "";
                string fullName = dicMoi != null ? SMAS.Business.Common.Utils.GetString(dicMoi["FullName"]) : "";
                int? supervisingDeptID = dicMoi != null ? SMAS.Business.Common.Utils.GetInt(dicMoi["SupervisingDeptID"]) : 0;
                SearchInfo["UserName"] = userName;
                SearchInfo["FullName"] = fullName;
                SearchInfo["SupervisingDeptID"] = supervisingDeptID;
            }
            //var supervisingDeptOfUsers = SupervisingDeptBusiness.GetSupervisingDeptOfUser(new GlobalInfo().UserAccountID);
            //List<SupervisingDept> listSupervisingDept = new List<SupervisingDept>();
            //listSupervisingDept.Add(supervisingDeptOfUsers);
            List<SupervisingDept> listSupervisingDept = new List<SupervisingDept>();
            SupervisingDept spvd = SupervisingDeptBusiness.GetSupervisingDeptOfUser(global.UserAccountID);
            // Load danh sach phong ban truc thuoc cua supervisingdept Hungnd8 25/04/2013 
            List<SupervisingDept> lstFullSupervisingDept = new List<SupervisingDept>();
            if (spvd != null)
            {
                listSupervisingDept.Add(spvd);
                lstFullSupervisingDept = SupervisingDeptBusiness.GetFollwerSupervising(spvd.SupervisingDeptID).OrderBy(o => o.SupervisingDeptName).ToList();
                foreach (SupervisingDept objSupervisingDept in lstFullSupervisingDept)
                {
                    objSupervisingDept.SupervisingDeptName = "....." + objSupervisingDept.SupervisingDeptName;
                    listSupervisingDept.Add(objSupervisingDept);
                }
            }
            if (listSupervisingDept.Count() > 0)
            {
                //ViewData[SupervisingDeptAccountConstants.SUPERVISINGDEFTDEFT] = new SelectList(listSupervisingDept, "SupervisingDeptID", "SupervisingDeptName", SupervisingDeptID);
                ViewData[SupervisingDeptAccountConstants.SUPERVISINGDEFTDEFT] = new SelectList(listSupervisingDept, "SupervisingDeptID", "SupervisingDeptName");
            }
            else
            {
                ViewData[SupervisingDeptAccountConstants.SUPERVISINGDEFTDEFT] = new SelectList(new string[] { });
            }
            // end Load danh sach phong ban.....
            //ViewData[SupervisingDeptAccountConstants.SUPERVISINGDEFTDEFT] = new SelectList(listSupervisingDept, "SupervisingDeptID", "SupervisingDeptName", SupervisingDeptID);
            List<SupervisingDeptAccountViewModel> lst = this._Search(SearchInfo);
            if (id.HasValue)
            {
                //List<SupervisingDeptAccountViewModel> listReturn = new List<SupervisingDeptAccountViewModel>();
                //SupervisingDeptAccountViewModel model = lst.Where(o => o.UserAccountID == id.Value).FirstOrDefault();
                //listReturn.Add(model);
                //listReturn.AddRange(lst.Where(o => o.UserAccountID != id.Value));
                ViewData[SupervisingDeptAccountConstants.LIST_SUPERVISINGDEFT] = lst; // listReturn;
                ViewData[SupervisingDeptAccountConstants.MESSAGETYPE] = type == "0" ? Res.Get("Common_Label_AddNewMessage") : Res.Get("Common_Label_UpdateSuccessMessage");
            }
            else
            {
                ViewData[SupervisingDeptAccountConstants.LIST_SUPERVISINGDEFT] = lst;
                ViewData[SupervisingDeptAccountConstants.MESSAGETYPE] = "";
            }
            ViewData[SupervisingDeptAccountConstants.DISABLE_SUPERVISINGDEFT] = SupervisingDeptID.HasValue ? "1" : "0";

            List<ComboObject> listStatus = new List<ComboObject>();
            listStatus.Add(new ComboObject("1", "Mới tạo"));
            ViewData[SupervisingDeptAccountConstants.STATUS] = new SelectList(listStatus, "key", "value");

            ViewData[SystemParamsInFile.PERMISSION] = GetMenupermission("SupervisingDeptAccount", global.UserAccountID, global.IsAdmin);
            return View();
        }

        #region SupervisingDeptAccountArea/SupervisingDeptAccount/Create

        public ActionResult Create()
        {
            SupervisingDeptAccountViewModel model = new SupervisingDeptAccountViewModel();
            GlobalInfo globalInfo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            int? SupervisingDeptID = globalInfo.SupervisingDeptID;
            SearchInfo["SupervisingDeptID"] = SupervisingDeptID;

            /*DungVA - lấy lên danh sách nhân viên thuộc cấp sở và các phòng ban trực thuộc*/
            Dictionary<string, object> dicEmployee = new Dictionary<string, object>();
            dicEmployee["EmployeeType"] = SMAS.Web.Constants.GlobalConstants.EMPLOYEE_TYPE_DEPARTMENT;
            dicEmployee["IsActive"] = true;

            SupervisingDept svd = SupervisingDeptBusiness.Find(SupervisingDeptID.Value);
            string tp = svd.TraversalPath + svd.SupervisingDeptID.ToString() + "\\";
            dicEmployee["SupervisingDeptID"] = SupervisingDeptID.Value;
            dicEmployee["TraversalPath"] = tp;

            IQueryable<Employee> res = EmployeeBusiness.Search(dicEmployee);
            // Lấy ra cấp sở hoặc cấp phòng thuộc sở
            Dictionary<string, object> dicSupervisingDept = new Dictionary<string, object>();
            if (globalInfo.IsSubSuperVisingDeptRole)
            {
                dicSupervisingDept["ListHierachyLevel"] = new List<int> { SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE, SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT };
            }
            else if (globalInfo.IsSuperVisingDeptRole)
            {
                dicSupervisingDept["ListHierachyLevel"] = new List<int> { SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE, SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT };
            }
            // 
            IQueryable<SupervisingDept> superVisingDeptLst = SupervisingDeptBusiness.Search(dicSupervisingDept);

            //Lấy lên danh sách nhân viên đã được cấp Account
            var listUserByDept = UserAccountBusiness.SearchUserByDept(SearchInfo).ToList();
            List<int> lstUserAccountIDByEmployee = listUserByDept.Select(p => p.EmployeeID.Value).Distinct().ToList();

            var allEmployeeHasNoAccount = from g in res
                                          join j in superVisingDeptLst on g.SupervisingDeptID equals j.SupervisingDeptID
                                          where !lstUserAccountIDByEmployee.Contains(g.EmployeeID)
                                          select g;
            /*End of DungVA - lấy lên danh sách nhân viên thuộc cấp sở và các phòng ban trực thuộc*/

            //Lấy lên danh sách nhân viên
            //var listAllEmployee = EmployeeBusiness.SearchEmployee(new GlobalInfo().SupervisingDeptID.Value, SearchInfo).ToList();
            var listAllEmployee = allEmployeeHasNoAccount.ToList();
            listAllEmployee = listAllEmployee.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            
            List<SupervisingCombo> listSupervisingDept = new List<SupervisingCombo>();
            if (listAllEmployee != null && listAllEmployee.Count() > 0)
            {
                if (listUserByDept == null || listUserByDept.Count() == 0)
                    model.ListEmployee = listAllEmployee.ToList();
                else if (listUserByDept != null && listUserByDept.Count() > 0)
                {
                    List<Employee> listemployee = new List<Employee>();
                    List<int> userbydeptid = listUserByDept.Where(u => u.EmployeeID.HasValue).Select(u => u.EmployeeID.Value).ToList();
                    foreach (var employee in listAllEmployee)
                    {
                        if (!userbydeptid.Contains(employee.EmployeeID))
                            listemployee.Add(employee);
                    }
                    listemployee = listemployee.OrderBy(e => SMAS.Business.Common.Utils.SortABC(e.FullName.getLastWord() + " " + e.FullName)).ToList();
                    model.ListEmployee = listemployee;
                }

                else model.ListEmployee = null;
                //Lấy lên danh sách nhóm người dùng của phòng ban và fill vào từng Dropdownlist with checkbox trên Gridview
                int SuperVisingDeptAdmin = 0;
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID.Value);
                SupervisingDept supervisingDeptParent = new SupervisingDept();
                // neu la phong ban truc thuoc thi lay admin cua cap cha
                if (supervisingDept.HierachyLevel == SMAS.Business.Common.GlobalConstants.EDUCATION_HIERACHY_SUBLEVEL_DISTRICT_OFFICE ||
                    supervisingDept.HierachyLevel == SMAS.Business.Common.GlobalConstants.EDUCATION_HIERACHY_SUBLEVEL_PROVINCE_OFFICE)
                {
                    supervisingDeptParent = SupervisingDeptBusiness.Find(supervisingDept.ParentID);
                    SuperVisingDeptAdmin = supervisingDeptParent.AdminID.Value;
                }
                else
                {
                    SuperVisingDeptAdmin = supervisingDept.AdminID.Value;
                }
                var listrole = GroupCatBusiness.GetGroupByRoleAndAdminID(new GlobalInfo().RoleID, SuperVisingDeptAdmin).ToList();
                model.ListRoleOfUser = new List<SelectListItem>();
                foreach (var role in listrole)
                {
                    model.ListRoleOfUser.Add(new SelectListItem { Text = role.GroupName, Value = role.GroupCatID.ToString(), Selected = false });
                }
                /* Hệ thống gọi hàm SupervisingDept.GetSupervisingDeptOfUser(cboSupervisingDept) đê lấy lên danh sách phòng ban dưới dạng phân cấp 
                   và hiển thị lên cboSupervisingDept
                 */
                SupervisingDept spvd = SupervisingDeptBusiness.GetSupervisingDeptOfUser(new GlobalInfo().UserAccountID);
                // Load danh sach phong ban truc thuoc cua supervisingdept Hungnd8 25/04/2013 
                List<SupervisingDept> lstFullSupervisingDept = new List<SupervisingDept>();
                if (spvd != null)
                {
                    SupervisingCombo Supervisingcombo = new SupervisingCombo();
                    Supervisingcombo.SupervisingDeptName = spvd.SupervisingDeptName;
                    Supervisingcombo.SupervisingDeptID = spvd.SupervisingDeptID;
                    listSupervisingDept.Add(Supervisingcombo);
                    lstFullSupervisingDept = SupervisingDeptBusiness.GetFollwerSupervising(spvd.SupervisingDeptID).OrderBy(o => o.SupervisingDeptName).ToList();
                    foreach (SupervisingDept objSupervisingDept in lstFullSupervisingDept)
                    {
                        Supervisingcombo = new SupervisingCombo();
                        Supervisingcombo.SupervisingDeptName = "....." + objSupervisingDept.SupervisingDeptName;
                        Supervisingcombo.SupervisingDeptID = objSupervisingDept.SupervisingDeptID;
                        listSupervisingDept.Add(Supervisingcombo);
                    }
                }
                // end Load danh sach phong ban.....
                ViewData[SupervisingDeptAccountConstants.LISTROLEOFUSER] = model.ListRoleOfUser.OrderBy(o => o.Text).ToList();
                ViewData[SupervisingDeptAccountConstants.LISTEMPLOYEE] = model.ListEmployee;
            }
            ViewData[SupervisingDeptAccountConstants.SUPERVISINGDEFTDEFT] = new SelectList(listSupervisingDept, "SupervisingDeptID", "SupervisingDeptName");
            return View();
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult CreateUser(FormCollection col)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("SupervisingDeptAccount", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            var listchkemployee = col["rptGroupOfDept"];
            if (!string.IsNullOrWhiteSpace(listchkemployee))
            {
                string[] employeeids = listchkemployee.Split(new Char[] { ',' });
                string username = "";
                int userid = new GlobalInfo().UserAccountID;
                bool isadmin = false;
                int roleid = new GlobalInfo().RoleID;
                int? useraccountid = 0;
                string listgrp;
                string[] groupids;
                List<int> listgroupid = new List<int>();

                Dictionary<int, string> dicUserName = new Dictionary<int, string>();
                Dictionary<int, string> dicRole = new Dictionary<int, string>();
                List<int> Employees = new List<int>();
                int Employeeid = 0;
                foreach (var employeeid in employeeids)
                {
                    if (!string.IsNullOrWhiteSpace(employeeid))
                    {
                        Employeeid = int.Parse(employeeid);
                        username = col["UserName_" + employeeid];
                        listgrp = col["Role_" + employeeid];
                        if (string.IsNullOrEmpty(username))
                        {
                            return Json(new JsonMessage(Res.Get("Tồn tại nhân viên chưa được nhập đầy đủ thông tin"), "Error"));
                        }
                        Employees.Add(Employeeid);
                        dicUserName.Add(Employeeid, username.Trim().ToLower());
                        dicRole.Add(Employeeid, listgrp);
                    }
                }
                var checkIsDuplicateUserName = IsDuplicateUserName(Employees, dicUserName, dicRole);
                // DUNGVA - 10/06 - Check trùng trên dữ liệu người dùng nhập vào
                //bool hasDuplicates = dicUserName.Select(c => c.Value.Trim().ToLower()).Intersect(dicUserName.Select(c => c.Value.Trim().ToLower())).Any();
                bool hasDuplicates = false;
                var countOne = dicUserName.Count();
                var countDuplicate = dicUserName.Select(a => a.Value).Distinct().Count();

                if (countDuplicate < countOne)
                {
                    hasDuplicates = true;
                }
                if (hasDuplicates)
                {
                    return Json(new JsonMessage(Res.Get("SupervisingDept_Validation_Error_CheckDuplicate"), "Error"));
                }
                // End of DUNGVA
                JsonMessage jsonMessage = (JsonMessage)checkIsDuplicateUserName.Data;
                MembershipCreateStatus status;
                try
                {
                    bool message = bool.Parse(jsonMessage.Message);
                    if (message == true)
                    {
                        foreach (var employeeid in Employees)
                        {
                            username = col["UserName_" + employeeid];
                            listgrp = col["Role_" + employeeid];
                            useraccountid = UserAccountBusiness.CreateUser(username, userid, isadmin, employeeid, out status);
                            if (useraccountid.HasValue && useraccountid.Value > 0)
                            {
                                groupids = listgrp.Split(new Char[] { ',' });
                                foreach (var groupid in groupids)
                                {
                                    if (Int32.Parse(groupid) > 0)
                                        listgroupid.Add(Int32.Parse(groupid));
                                }
                                UserGroupBusiness.AssignGroupsForUser(userid, listgroupid, roleid, useraccountid.Value);
                            }
                            // Reset list group ID
                            listgroupid.Clear();
                        }
                        UserAccountBusiness.Save();
                        UserGroupBusiness.Save();
                        return Json(new JsonMessage(string.Format("{0}_{1}_{2}", Res.Get("SupervisingDept_Create_Done"), useraccountid, "0")));
                    }
                }
                catch
                {
                    return checkIsDuplicateUserName;
                }
            }
            return Json(new JsonMessage(Res.Get("SupervisingDept_Validation_Error_NoCheck"), "Error"));
        }

        private JsonResult IsDuplicateUserName(List<int> listEmployee, Dictionary<int, string> dicUserName, Dictionary<int, string> dicRole)
        {
            Regex objAlphaPattern = new Regex(@"^[a-zA-Z0-9\-_.]*$");
            Regex firstCharPattern = new Regex(@"[a-zA-Z]$");
            bool IsMatchUsername = false;
            string listgrp;
            string username = string.Empty;
            UserAccount userAccount = new UserAccount();
            foreach (var item in listEmployee)
            {
                //Hệ thống gọi hàm UserAccount.CreateUser(txtUserName, UserInfo.UserID, UserInfo.IsAdmin, EmployeeID): 
                //Với EmployeeID được lấy lên trên từng dòng của gridview
                //Hệ thống gọi hàm AssignGroupForUser(UserInfo.UserID, lstGroup, UserID)
                //Với lstGroup = ddlGroup, UserID = (UserAccount.CreateUser(txtUserName, UserInfo.UserID, UserInfo.IsAdmin, EmployeeID)).UserID
                username = dicUserName[item];
                listgrp = dicRole[item];
                if (!string.IsNullOrWhiteSpace(username))
                {
                    //check < 8, ki tu dac biet
                    if (username.Length <= 8)
                    {
                        return Json(new JsonMessage(string.Format("{0}_{1}", Res.Get("SupervisingDept_Validation_Error_LengthUserName"), item), "ErrorUserName"));
                    }
                    else if (!firstCharPattern.IsMatch(username.Substring(0, 1)))
                    {
                        return Json(new JsonMessage(string.Format("{0}_{1}", Res.Get("SupervisingDept_Validation_Error_FirstCharUserName"), item), "ErrorUserName"));
                    }
                    else
                    {
                        IsMatchUsername = objAlphaPattern.IsMatch(username);
                        if (!IsMatchUsername)
                        {
                            return Json(new JsonMessage(string.Format("{0}_{1}", Res.Get("SupervisingDept_Validation_Error_IsMatchUsername"), item), "ErrorUserName"));
                        }
                        else
                        {
                            if (listgrp != null && listgrp.Count() > 0)
                            {
                                userAccount = UserAccountBusiness.All.Where(o => o.aspnet_Users.UserName == username).FirstOrDefault();
                                if (userAccount != null)
                                {
                                    return Json(new JsonMessage(string.Format("{0}_{1}", Res.Get("SupervisingDept_Validation_Error_DuplicateUserName"), item), "ErrorUserName"));
                                }
                            }
                            else
                            {
                                //throw new BusinessException("SupervisingDept_Validation_Error");
                                return Json(new JsonMessage(string.Format("{0}_{1}", Res.Get("SupervisingDept_Validation_Error"), item), "Error"));
                            }
                        }
                    }
                }
                else
                {
                    return Json(new JsonMessage(string.Format("{0}_{1}", Res.Get("SupervisingDept_Validation_Error_UserName"), item), "ErrorUserName"));
                }
            }
            return Json(new JsonMessage("true"));
        }


        #endregion


        #region SupervisingDeptAccountArea/SupervisingDeptAccount/Edit/id
        public ActionResult Edit(int id)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic[SMASUtils.ConvertToOracle("CreatedUserID")] = new GlobalInfo().UserAccountID;

            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["UserAccountID"] = id;
            UserAccount user = UserAccountBusiness.SearchUser(SearchInfo).ToList().FirstOrDefault();
            CheckPermissionForAction(user.EmployeeID, "Employee", 0, dic);

            SupervisingDeptAccountViewModel model = new SupervisingDeptAccountViewModel();
            int SuperVisingDeptAdmin = 0;

            model.UserAccount = user;
            model.UserName = model.UserAccount.aspnet_Users == null ? "" : model.UserAccount.aspnet_Users.UserName;
            model.aspnet_Membership = model.UserAccount.aspnet_Users == null ? new aspnet_Membership() : model.UserAccount.aspnet_Users.aspnet_Membership;
            if (model.UserAccount.Employee != null && model.UserAccount.Employee.Genre)
                model.GenreName = Res.Get("Common_Label_Male");
            else if (model.UserAccount.Employee != null && !model.UserAccount.Employee.Genre)
                model.GenreName = Res.Get("Common_Label_Female");
            SupervisingDept supervisingDept = SupervisingDeptBusiness.GetSupervisingDeptOfUser(id);
            SupervisingDept supervisingDeptParent = new SupervisingDept();
            // neu la phong ban truc thuoc thi lay admin cua cap cha
            if (supervisingDept.HierachyLevel == SMAS.Business.Common.GlobalConstants.EDUCATION_HIERACHY_SUBLEVEL_DISTRICT_OFFICE ||
                supervisingDept.HierachyLevel == SMAS.Business.Common.GlobalConstants.EDUCATION_HIERACHY_SUBLEVEL_PROVINCE_OFFICE)
            {
                supervisingDeptParent = SupervisingDeptBusiness.Find(supervisingDept.ParentID);
                SuperVisingDeptAdmin = supervisingDeptParent.AdminID.Value;
            }
            else
            {
                SuperVisingDeptAdmin = supervisingDept.AdminID.Value;
            }
            model.ListGroupOfRole = GroupCatBusiness.GetGroupByRoleAndAdminID(new GlobalInfo().RoleID, SuperVisingDeptAdmin);
            model.ListGroupOfSupervisingDeptAccount = UserGroupBusiness.GetUserGroupsByUser(id, _globalInfo.RoleID);
            model.UserAccountID = id;

            /*DungVA - 17/06/2013 - Lấy danh sách nhân viên phòng sở chưa có account và gán vào cho textbox autocomplete*/

            /*DungVA - lấy lên danh sách nhân viên thuộc cấp sở và các phòng ban trực thuộc*/
            GlobalInfo globalInfo = new GlobalInfo();
            Dictionary<string, object> dicEmployee = new Dictionary<string, object>();
            dicEmployee["EmployeeType"] = SMAS.Web.Constants.GlobalConstants.EMPLOYEE_TYPE_DEPARTMENT;
            dicEmployee["IsActive"] = true;

            SupervisingDept svd = SupervisingDeptBusiness.Find(globalInfo.SupervisingDeptID.Value);
            string tp = svd.TraversalPath + svd.SupervisingDeptID.ToString() + "\\";
            dicEmployee["SupervisingDeptID"] = globalInfo.SupervisingDeptID.Value;
            dicEmployee["TraversalPath"] = tp;

            IQueryable<Employee> res = EmployeeBusiness.Search(dicEmployee);
            // Lấy ra cấp sở hoặc cấp phòng thuộc sở
            Dictionary<string, object> dicSupervisingDept = new Dictionary<string, object>();
            if (globalInfo.IsSubSuperVisingDeptRole)
            {
                dicSupervisingDept["ListHierachyLevel"] = new List<int> { SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE, SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT };
            }
            else if (globalInfo.IsSuperVisingDeptRole)
            {
                dicSupervisingDept["ListHierachyLevel"] = new List<int> { SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE, SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT };
            }
            // 
            IQueryable<SupervisingDept> superVisingDeptLst = SupervisingDeptBusiness.Search(dicSupervisingDept);

            var allEmployeeHasNoAccount = from g in res
                                          join j in superVisingDeptLst on g.SupervisingDeptID equals j.SupervisingDeptID
                                          join t in UserAccountBusiness.All on g.EmployeeID equals t.EmployeeID into t1
                                          from j1 in t1.DefaultIfEmpty()
                                          where j1.UserAccountID == null
                                          select g;
            /*End of DungVA - lấy lên danh sách nhân viên thuộc cấp sở và các phòng ban trực thuộc*/
            // Bắt đầu gán giá trị cho autocomplete
            var listAllEmployee = allEmployeeHasNoAccount.ToList();
            if (listAllEmployee != null && listAllEmployee.Count > 0)
            {
                List<SelectListItem> listEmpl = new List<SelectListItem>();
                foreach (var item in allEmployeeHasNoAccount)
                {
                    SelectListItem _mList = new SelectListItem();
                    _mList = new SelectListItem() { Text = item.FullName.Trim() + "-" + item.EmployeeCode, Value = item.EmployeeID.ToString() };
                    listEmpl.Add(_mList);
                }
                model.ListEmployeeSupervisingDept = listEmpl;
            }
            else
            {
                model.ListEmployeeSupervisingDept = new List<SelectListItem>();
            }
            /*End of DungVA - Lấy danh sách nhân viên phòng sở*/
            return View(model);
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult EditUser(FormCollection col)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("SupervisingDeptAccount", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }


            int id = string.IsNullOrWhiteSpace(col["id"]) ? 0 : int.Parse(col["id"].Trim());
            //Namta Check ATTT
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic[SMASUtils.ConvertToOracle("CreatedUserID")] = new GlobalInfo().UserAccountID;

            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["UserAccountID"] = id;
            UserAccount user = UserAccountBusiness.SearchUser(SearchInfo).ToList().FirstOrDefault();
            CheckPermissionForAction(user.EmployeeID, "Employee", 0, dic);
            //---end
            var usergroups = col["rptGroupOfDept"];
            string username = col["UserName"];

            /*DungVA - 18/06/2013 - lấy thông tin nhân viên dùng để update*/
            string employeeInfo = col["EmployeeNameSupervisingDept"];
            int tempEmployeeID = !string.IsNullOrWhiteSpace(col["tempEmployeeID"]) ? Int32.Parse(col["tempEmployeeID"]) : 0;
            int tempSupervisingdeptID = !string.IsNullOrWhiteSpace(col["tempSupervisingdeptID"]) ? Int32.Parse(col["tempSupervisingdeptID"]) : 0;

            if (!string.IsNullOrWhiteSpace(username))
            {
                Regex objAlphaPattern = new Regex(@"^[a-zA-Z0-9\-_.]*$");
                Regex firstCharPattern = new Regex(@"[a-zA-Z]$");
                bool IsMatchUsername = false;
                if (username.Length <= 8)
                {
                    return Json(new JsonMessage(Res.Get("SupervisingDept_Validation_Error_LengthUserName"), "ErrorUserName"));
                }
                else if (!firstCharPattern.IsMatch(username.Substring(0, 1)))
                {
                    return Json(new JsonMessage(Res.Get("SupervisingDept_Validation_Error_FirstCharUserName"), "ErrorUserName"));
                }
                else
                {
                    IsMatchUsername = objAlphaPattern.IsMatch(username);
                    if (!IsMatchUsername)
                    {
                        return Json(new JsonMessage(Res.Get("SupervisingDept_Validation_Error_IsMatchUsername"), "ErrorUserName"));
                    }
                    else
                    {
                        if (usergroups != null && usergroups.Length > 0)
                        {
                            UserAccount userAccount = UserAccountBusiness.Find(id);
                            bool isresetpass = string.IsNullOrWhiteSpace(col["chkResetPass"]) ? false : col["chkResetPass"].Trim() == "1" ? true : false;
                            bool isApproved = string.IsNullOrWhiteSpace(col["chkIsApproved"]) ? false : col["chkIsApproved"].Trim() == "1" ? true : false;
                            try
                            {
                                if (tempEmployeeID != 0)
                                {
                                    UserAccountBusiness.UpdateUser(userAccount.UserAccountID, username, isresetpass, tempEmployeeID, isApproved);
                                    UserAccountBusiness.Save();
                                }
                                else if (tempEmployeeID == 0)
                                {
                                    UserAccountBusiness.UpdateUser(userAccount.UserAccountID, username, isresetpass, userAccount.EmployeeID, isApproved);
                                    UserAccountBusiness.Save();
                                }
                                else
                                {
                                    return Json(new JsonMessage(Res.Get("SupervisingDept_Validation_Error_Employee"), "Error"));
                                }
                                //UserAccountBusiness.UpdateUser(userAccount.UserAccountID, username, isresetpass, userAccount.EmployeeID, isApproved);

                            }
                            catch (Exception ex)
                            {
                                return Json(new JsonMessage(Res.Get(ex.Message), "ErrorUserName"));
                            }
                            //Xoa du lieu map cua group va user
                            List<UserGroup> listUserGroup = UserGroupBusiness.GetUserGroupsByUser(id, GlobalInfo.RoleID).ToList();
                            UserGroupBusiness.DeleteAll(listUserGroup);

                            //insert du lieu map cua group va user

                            string[] groupids = usergroups.Split(new Char[] { ',' });
                            List<int> listGrp = new List<int>();
                            foreach (var groupid in groupids)
                            {
                                if (Int32.Parse(groupid) > 0)
                                {
                                    listGrp.Add(Int32.Parse(groupid));
                                }
                            }
                            UserGroupBusiness.AssignGroupsForUser(new GlobalInfo().UserAccountID, listGrp, new GlobalInfo().RoleID, id);
                            UserGroupBusiness.Save();
                            return Json(new JsonMessage(string.Format("{0}_{1}_{2}", Res.Get("Common_Label_UpdateSuccessMessage"), id, "1")));
                        }
                        else
                        {
                            return Json(new JsonMessage(Res.Get("SupervisingDept_Validation_Error_NoCheckGroup"), "Error"));
                        }
                    }
                }
            }
            else
            {
                return Json(new JsonMessage(Res.Get("SupervisingDept_Validation_Error_UserName"), "ErrorUserName"));
            }
            //return RedirectToAction("Index", "SupervisingDeptAccount", new { Area = "SupervisingDeptAccountArea", id = id, type = "1" });
        }

        #endregion


        public ActionResult GetListEmployeeBySupervisingDept(int? id)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            SupervisingDeptAccountViewModel model = new SupervisingDeptAccountViewModel();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SupervisingDeptID"] = id.HasValue && id.Value > 0 ? id.Value : new GlobalInfo().SupervisingDeptID;
            /*DungVA - lấy lên danh sách nhân viên thuộc cấp sở và các phòng ban trực thuộc*/
            Dictionary<string, object> dicEmployee = new Dictionary<string, object>();
            dicEmployee["EmployeeType"] = SMAS.Web.Constants.GlobalConstants.EMPLOYEE_TYPE_DEPARTMENT;
            dicEmployee["IsActive"] = true;

            if (id.HasValue)
            {
                SupervisingDept svd = SupervisingDeptBusiness.Find(id.Value);
                string tp = svd.TraversalPath + svd.SupervisingDeptID.ToString() + "\\";
                // Nếu lựa chọn đơn vị quản lý
                dicEmployee["SupervisingDeptID"] = id.Value;
                dicEmployee["TraversalPath"] = tp;
            }
            else
            {
                SupervisingDept svd = SupervisingDeptBusiness.Find(new GlobalInfo().SupervisingDeptID.Value);
                string tp = svd.TraversalPath + svd.SupervisingDeptID.ToString() + "\\";
                // Nếu không lựa chọn đơn vị quản lý thì lấy cấp quản lý và các cấp con
                dicEmployee["SupervisingDeptID"] = new GlobalInfo().SupervisingDeptID.Value;
                dicEmployee["TraversalPath"] = tp;
            }

            IQueryable<Employee> res = EmployeeBusiness.Search(dicEmployee);
            // Lấy ra cấp sở hoặc cấp phòng thuộc sở
            Dictionary<string, object> dicSupervisingDept = new Dictionary<string, object>();
            if (globalInfo.IsSubSuperVisingDeptRole)
            {
                dicSupervisingDept["ListHierachyLevel"] = new List<int> { SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE, SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT };
            }
            else if (globalInfo.IsSuperVisingDeptRole)
            {
                dicSupervisingDept["ListHierachyLevel"] = new List<int> { SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE, SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT };
            }
            IQueryable<SupervisingDept> superVisingDeptLst = SupervisingDeptBusiness.Search(dicSupervisingDept);
            // Join 2 lst để lấy ra số nhân viên thuộc cấp sở phòng
            var allEmployeeHasNoAccount = from g in res
                                          join j in superVisingDeptLst on g.SupervisingDeptID equals j.SupervisingDeptID
                                          join t in UserAccountBusiness.All on g.EmployeeID equals t.EmployeeID into t1
                                          from j1 in t1.DefaultIfEmpty()
                                          where j1.UserAccountID == null
                                          select g;
            /*End of DungVA - lấy lên danh sách nhân viên thuộc cấp sở và các phòng ban trực thuộc*/

            //Lấy lên danh sách nhân viên
            //var listAllEmployee = EmployeeBusiness.SearchEmployee(globalInfo.SupervisingDeptID.Value, SearchInfo).ToList();
            //var listAllEmployee = EmployeeBusiness.SearchEmployee(id.Value, SearchInfo).ToList();
            var listAllEmployee = allEmployeeHasNoAccount.ToList();
            //Lấy lên danh sách nhân viên đã được cấp Account
            var listUserByDept = UserAccountBusiness.SearchUserByDept(SearchInfo).ToList();
            if (listAllEmployee != null && listAllEmployee.Count() > 0)
            {
                if (listUserByDept == null || listUserByDept.Count() == 0)
                    model.ListEmployee = listAllEmployee.ToList();
                else if (listUserByDept != null && listUserByDept.Count() > 0)
                {
                    List<Employee> listemployee = new List<Employee>();
                    List<int> userbydeptid = listUserByDept.Where(u => u.EmployeeID.HasValue).Select(u => u.EmployeeID.Value).ToList();
                    foreach (var employee in listAllEmployee)
                    {
                        if (!userbydeptid.Contains(employee.EmployeeID))
                            listemployee.Add(employee);
                    }
                    model.ListEmployee = listemployee;
                }

                else model.ListEmployee = null;

                //Lấy lên danh sách nhóm người dùng của phòng ban và fill vào từng Dropdownlist with checkbox trên Gridview
                int SuperVisingDeptAdmin = 0;
                // nếu id truyền vào là tất cả id = null thì lấy supervisingdeptID = cấp cao nhất của account
                id = globalInfo.SupervisingDeptID.Value;
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(id.Value);
                SupervisingDept supervisingDeptParent = new SupervisingDept();
                // neu la phong ban truc thuoc thi lay admin cua cap cha
                if (supervisingDept.HierachyLevel == SMAS.Business.Common.GlobalConstants.EDUCATION_HIERACHY_SUBLEVEL_DISTRICT_OFFICE ||
                    supervisingDept.HierachyLevel == SMAS.Business.Common.GlobalConstants.EDUCATION_HIERACHY_SUBLEVEL_PROVINCE_OFFICE)
                {
                    supervisingDeptParent = SupervisingDeptBusiness.Find(supervisingDept.ParentID);
                    SuperVisingDeptAdmin = supervisingDeptParent.AdminID.Value;
                }
                else
                {
                    SuperVisingDeptAdmin = supervisingDept.AdminID.Value;
                }
                var listrole = GroupCatBusiness.GetGroupByRoleAndAdminID(new GlobalInfo().RoleID, SuperVisingDeptAdmin).ToList();
                model.ListRoleOfUser = new List<SelectListItem>();
                foreach (var role in listrole)
                {
                    model.ListRoleOfUser.Add(new SelectListItem { Text = role.GroupName, Value = role.GroupCatID.ToString(), Selected = false });
                }
                ViewData[SupervisingDeptAccountConstants.LISTROLEOFUSER] = model.ListRoleOfUser;
                ViewData[SupervisingDeptAccountConstants.LISTEMPLOYEE] = model.ListEmployee;
            }
            //return PartialView(VirtualPathUtility.ToAbsolute("~/Areas/SupervisingDeptAccountArea/Views/SupervisingDeptAccount/_ListEmployee.cshtml"));
            return PartialView("_ListEmployee");
        }


        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("SupervisingDeptAccount", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            //Xóa usergroup (chứa khóa ngoại useraccount) trc
            //List<UserGroup> listUserGroup = UserGroupBusiness.GetUserGroupsByUser(id).ToList();
            //UserGroupBusiness.DeleteAll(listUserGroup);
            //UserGroupBusiness.Save();

            //Namta Check ATTT
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic[SMASUtils.ConvertToOracle("CreatedUserID")] = new GlobalInfo().UserAccountID;

            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["UserAccountID"] = id;
            UserAccount user = UserAccountBusiness.SearchUser(SearchInfo).ToList().FirstOrDefault();
            CheckPermissionForAction(user.EmployeeID, "Employee", 0, dic);
            //---end

            UserAccountBusiness.DeleteAccount(id);
            UserAccountBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        public PartialViewResult Search(SearchViewModel frm, FormCollection col)
        {
            SetViewDataPermission("SupervisingDeptAccount", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            Utils.Utils.TrimObject(frm);
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["UserName"] = frm.Username;
            SearchInfo["FullName"] = frm.FullName;
            SearchInfo["SupervisingDeptID"] = frm.Dept.HasValue ? frm.Dept.Value : 0;// (frm.Dept == null && new GlobalInfo().SupervisingDeptID.HasValue) ? int.Parse(col["DeptID"]) : (int?)null;
            //SearchInfo["SupervisingDeptID"] = frm.Dept.HasValue ? frm.Dept.Value : global.SupervisingDeptID.Value;
            SearchInfo["isNewAccount"] = frm.StatusAccount == 1 ? true : false;
            List<SupervisingDeptAccountViewModel> lst = this._Search(SearchInfo);
            ViewData[SupervisingDeptAccountConstants.LIST_SUPERVISINGDEFT] = lst;
            ViewData[SupervisingDeptAccountConstants.MESSAGETYPE] = "";
            Dictionary<string, object> dicForBack = new Dictionary<string, object>();
            dicForBack["UserName"] = frm.Username;
            dicForBack["FullName"] = frm.FullName;
            dicForBack["SupervisingDeptID"] = frm.Dept.HasValue ? frm.Dept.Value : 0;
            dicForBack["IsActive"] = true;
            Session["forBackAccount"] = dicForBack;
            ViewData[SupervisingDeptAccountConstants.FORBACK] = dicForBack;
            return PartialView("_List");
        }
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private List<SupervisingDeptAccountViewModel> _Search(IDictionary<string, object> SearchInfo, bool isExportExcel = false)
        {
            SetViewDataPermission("SupervisingDeptAccount", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            GlobalInfo global = new GlobalInfo();
            int SupervisingDept = 0;
            if (SearchInfo.ContainsKey("SupervisingDeptID"))
            {
                SupervisingDept = (int)SearchInfo["SupervisingDeptID"];
            }

            SearchInfo["SupervisingDeptID"] = 0;

            IQueryable<UserAccount> query = UserAccountBusiness.SearchUserByDept(SearchInfo);
            IQueryable<UserAccount> userAccount = UserAccountBusiness.SearchUser(SearchInfo);

            if (SupervisingDept == 0)
            {
                List<int> dicSupervisingDept = new List<int>();
                if (global.IsSubSuperVisingDeptRole)
                {
                    dicSupervisingDept = new List<int> { SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE, SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT };
                }
                else if (global.IsSuperVisingDeptRole)
                {
                    dicSupervisingDept = new List<int> { SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE, SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT };
                }

                var sd = SupervisingDeptBusiness.Find(global.SupervisingDeptID.Value);
                //Lay len danh sach phong ban con cua Dept lay theo path
                var lsSD = SupervisingDeptBusiness.Search(new Dictionary<string, object>()
            {
                {"TraversalPath",sd.TraversalPath+sd.SupervisingDeptID+"\\"},
                {"ListHierachyLevel",dicSupervisingDept}
            }).Select(o => o.SupervisingDeptID).ToList();
                lsSD.Add(global.SupervisingDeptID.Value);
                //tim kiem nguoi dung thuoc danh sach phong ban tren (containt)
                query = query.Where(o => lsSD.Contains(o.Employee.SupervisingDeptID.Value));
                userAccount = userAccount.Where(o => lsSD.Contains(o.Employee.SupervisingDeptID.Value));
            }
            else
            {
                List<int> dicSupervisingDept = new List<int>();
                if (global.IsSubSuperVisingDeptRole)
                {
                    dicSupervisingDept = new List<int> { SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE, SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT };
                }
                else if (global.IsSuperVisingDeptRole)
                {
                    dicSupervisingDept = new List<int> { SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE, SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT };
                }

                var sd = SupervisingDeptBusiness.Find(SupervisingDept);
                //Lay len danh sach phong ban con cua Dept lay theo path
                var lsSD = SupervisingDeptBusiness.Search(new Dictionary<string, object>()
            {
                {"TraversalPath",sd.TraversalPath+sd.SupervisingDeptID+"\\"},
                {"ListHierachyLevel",dicSupervisingDept}
            }).Select(o => o.SupervisingDeptID).ToList();
                lsSD.Add(SupervisingDept);
                //tim kiem nguoi dung thuoc danh sach phong ban tren (containt)
                query = query.Where(o => lsSD.Contains(o.Employee.SupervisingDeptID.Value));
                userAccount = userAccount.Where(o => lsSD.Contains(o.Employee.SupervisingDeptID.Value));
            }

            var t = from q in query
                    where userAccount.Any(o => o.UserAccountID == q.UserAccountID)
                    join e in EmployeeBusiness.All on q.EmployeeID equals e.EmployeeID into f
                    from g in f.DefaultIfEmpty()
                    select new
                    {
                        userAccount = q,
                        g.FullName,
                        g.EmployeeCode,
                        g.BirthDate,
                        g.Mobile,
                        g.Genre,
                        g.SupervisingDept.SupervisingDeptName
                    };
            // Lấy ra cấp sở hoặc cấp phòng thuộc sở



            IQueryable<SupervisingDeptAccountViewModel> lst = t.Select(o => new SupervisingDeptAccountViewModel
            {
                UserAccountID = o.userAccount.UserAccountID,
                UserName = o.userAccount.aspnet_Users.UserName,
                PassWord = o.userAccount.aspnet_Users.aspnet_Membership.Password,
                FullName = o.FullName,
                EmployeeCode = o.EmployeeCode,
                BirthDate = o.BirthDate,
                Mobile = o.Mobile,
                Genre = o.Genre,
                FirstLoginDate = o.userAccount.FirstLoginDate,
                SupervisingDeptName = o.SupervisingDeptName,
                CreateDate = o.userAccount.CreatedDate,
                displayButton = o.userAccount.UserAccountID != global.UserAccountID,
                StatusName = o.userAccount.FirstLoginDate.HasValue ? "" : "Mới tạo"
            });
            List<SupervisingDeptAccountViewModel> listRes = lst.ToList();
            MembershipUser objUserMembership;
            List<SupervisingDeptAccountViewModel> listResWillBeRemove = new List<SupervisingDeptAccountViewModel>();
            foreach (var i in listRes)
            {
                if (i.Genre.HasValue && i.Genre.Value)
                    i.GenreName = Res.Get("Common_Label_Male");
                else i.GenreName = Res.Get("Common_Label_Female");
                if (i.FirstLoginDate.HasValue)
                {
                    i.PassWord = "******";
                }
                else
                {
                    if (i.UserName != null)
                    {
                        objUserMembership = Membership.GetUser(i.UserName, false);
                        if (objUserMembership == null)
                        {
                            // Khong tim thay Membership nen se luu lai de xoa khoi danh sach sau do
                            listResWillBeRemove.Add(i);
                            continue;
                        }
                        if (objUserMembership.IsLockedOut)
                        {
                            i.PassWord = "******";
                        }
                        else
                        {
                            if (isExportExcel)
                            {
                                i.PassWord = SMAS.Business.Common.Utils.GenPassRandom(SystemParamsInFile.MAX_PASS_LENTGTH);
                                //if (objUserMembership.GetPassword().Length < 7)
                                //    i.PassWord = objUserMembership.GetPassword().Substring(0, 7);
                                //else
                                //{
                                //    i.PassWord = objUserMembership.GetPassword();
                                //}
                            }
                            else
                            {
                                i.PassWord = "******";
                            }

                        }
                    }


                }
                i.BirthDay = String.Format("{0:dd/MM/yyyy}", i.BirthDate);
            }
            // Xoa cac tai khoan khong tim thay Membership
            listResWillBeRemove.ForEach(o => listRes.Remove(o));
            // Sap xep danh sach theo Ten va Ho
            listRes = listRes.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.FullName.Trim().getLastWord() + " " + p.FullName)).ToList();
            return listRes;
        }

        /// <summary>
        /// Get Employee Information
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public JsonResult GetEmployeeInfo(string employ)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            string employeeCode = "";
            if (employ != null)
            {
                for (int i = 0; i < employ.Length; i++)
                {
                    if (employ[i] == '-' && (employ.Length - i) > 1)
                    {
                        employeeCode = employ.Substring(i + 1, employ.Length - i - 1);
                        break;
                    }
                }
            }
            /* lấy lên danh sách nhân viên thuộc cấp sở và các phòng ban trực thuộc*/
            Dictionary<string, object> dicEmployee = new Dictionary<string, object>();
            dicEmployee["EmployeeType"] = SMAS.Web.Constants.GlobalConstants.EMPLOYEE_TYPE_DEPARTMENT;
            dicEmployee["IsActive"] = true;

            SupervisingDept svd = SupervisingDeptBusiness.Find(globalInfo.SupervisingDeptID.Value);
            string tp = svd.TraversalPath + svd.SupervisingDeptID.ToString() + "\\";
            dicEmployee["SupervisingDeptID"] = globalInfo.SupervisingDeptID.Value;
            dicEmployee["TraversalPath"] = tp;

            IQueryable<Employee> res = EmployeeBusiness.Search(dicEmployee);
            // Lấy ra cấp sở hoặc cấp phòng thuộc sở
            Dictionary<string, object> dicSupervisingDept = new Dictionary<string, object>();
            if (globalInfo.IsSubSuperVisingDeptRole)
            {
                dicSupervisingDept["ListHierachyLevel"] = new List<int> { SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE, SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT };
            }
            else if (globalInfo.IsSuperVisingDeptRole)
            {
                dicSupervisingDept["ListHierachyLevel"] = new List<int> { SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE, SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT };
            }
            // 
            IQueryable<SupervisingDept> superVisingDeptLst = SupervisingDeptBusiness.Search(dicSupervisingDept);

            var allEmployeeHasNoAccount = from g in res
                                          join j in superVisingDeptLst on g.SupervisingDeptID equals j.SupervisingDeptID
                                          join t in UserAccountBusiness.All on g.EmployeeID equals t.EmployeeID into t1
                                          from j1 in t1.DefaultIfEmpty()
                                          where j1.UserAccountID == null
                                          select g;
            /*End of lấy lên danh sách nhân viên thuộc cấp sở và các phòng ban trực thuộc*/

            // Sau khi tìm ra các nhân viên thuộc sở và phòng trực thuộc thì tìm kiếm nhân viên có employeeCode = employeeCode của nhân viên được trọn trong autocomplete
            SupervisingDeptAccountViewModel sav = new SupervisingDeptAccountViewModel();
            foreach (Employee item in allEmployeeHasNoAccount)
            {
                if (item.EmployeeCode == employeeCode)
                {
                    sav.GenreName = item.Genre != false ? Res.Get("Common_Label_Male") : Res.Get("Common_Label_Female");
                    sav.BirthDateStr = item.BirthDate != null ? item.BirthDate.Value.ToString("dd/MM/yyyy") : string.Empty;
                    sav.BirthDate = item.BirthDate;
                    sav.tempEmployeeID = item.EmployeeID;
                    sav.tempSupervisingdeptID = item.SupervisingDeptID.Value;
                    sav.StaffPositionStr = item.StaffPosition.Resolution;
                    sav.SupervisingDeptName = item.SupervisingDept.SupervisingDeptName;
                }
            }

            //// Tìm kiếm employee theo EmployeeCode và SupervisingDeptID
            //Dictionary<string, object> dicEmployeeNew = new Dictionary<string, object>();
            //dicEmployeeNew["EmployeeCode"] = employeeCode;
            //dicEmployeeNew["SupervisingDeptID"] = globalInfo.SupervisingDeptID.Value;

            //Employee employee = EmployeeBusiness.Search(dicEmployeeNew).FirstOrDefault();

            return Json(sav);
        }

        public FileResult ExportExcel(string Username, string FullName, int SupervisingDeptID, int StatusID, string strUserAccountID)
        {
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HT" + "/" + "DanhSachBanGiaoTaiKhoan_PS.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            List<int> lstUserAccountID = strUserAccountID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["UserName"] = Username;
            SearchInfo["FullName"] = FullName;
            SearchInfo["SupervisingDeptID"] = SupervisingDeptID;
            SearchInfo["isNewAccount"] = StatusID == 1 ? true : false;
            SearchInfo["lstUserAccountID"] = lstUserAccountID;
            List<SupervisingDeptAccountViewModel> listResult = this._Search(SearchInfo, true);
            SupervisingDeptAccountViewModel objVM = null;
            IDictionary<int, string> lstUserAccount = new Dictionary<int, string>();
            //Fill du lieu
            IVTWorksheet sheet = oBook.GetSheet(1);
            sheet.SetCellValue("A2", _globalInfo.SuperVisingDeptName.ToUpper());
            int startRow = 6;
            for (int i = 0; i < listResult.Count; i++)
            {
                objVM = listResult[i];
                sheet.SetCellValue(startRow, 1, (i + 1));
                sheet.SetCellValue(startRow, 2, objVM.FullName);
                sheet.SetCellValue(startRow, 3, objVM.BirthDay);
                sheet.SetCellValue(startRow, 4, objVM.SupervisingDeptName);
                sheet.SetCellValue(startRow, 5, objVM.UserName);
                sheet.SetCellValue(startRow, 6, objVM.PassWord);
                if (!string.IsNullOrEmpty(objVM.StatusName))
                {
                    lstUserAccount[objVM.UserAccountID] = objVM.PassWord;
                }
                startRow++;
            }
            UserAccountBusiness.ChangePasswordByListUseraccount(lstUserAccount);
            sheet.GetRange(6, 1, startRow - 1, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string ReportName = "DanhSachBanGiaoTaiKhoanPS.xls";
            result.FileDownloadName = ReportName;
            return result;
        }
        [ValidateAntiForgeryToken]
        public JsonResult ResetPassword(string strUserAccountID)
        {
            List<int> lstUserAccountID = strUserAccountID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            UserAccountBusiness.ResetPasswordByListUserAccount(lstUserAccountID);
            return Json(new JsonMessage("ResetPassword Success", "success"));
        }
        public bool CheckPassRequire(string Password)
        {
            bool character = false;
            bool number = false;

            //kiem tra co kytu khong
            foreach (char ch in Password)
            {
                if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122))
                {
                    character = true;
                }
                if (ch >= 48 && ch <= 64)
                {
                    number = true;
                }
            }
            if (character && number)
                return true;
            return false;
        }
    }
}
