﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ConcurrentWorkAssignmentArea
{
    public class ConcurrentWorkAssignmentAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ConcurrentWorkAssignmentArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ConcurrentWorkAssignmentArea_default",
                "ConcurrentWorkAssignmentArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
