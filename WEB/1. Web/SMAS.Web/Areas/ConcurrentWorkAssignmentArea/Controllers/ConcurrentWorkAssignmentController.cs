﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.ConcurrentWorkAssignmentArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

//using SMAS.Web.Areas.ConcurrentWorkReplacementArea.Models;

namespace SMAS.Web.Areas.ConcurrentWorkAssignmentArea.Controllers
{
    public class ConcurrentWorkAssignmentController : BaseController
    {        
        private readonly IConcurrentWorkAssignmentBusiness ConcurrentWorkAssignmentBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IConcurrentWorkTypeBusiness ConcurrentWorkTypeBusiness;
        private readonly IConcurrentWorkReplacementBusiness ConcurrentWorkReplacementBusiness;
		public ConcurrentWorkAssignmentController (IConcurrentWorkAssignmentBusiness concurrentworkassignmentBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness,
            IConcurrentWorkTypeBusiness ConcurrentWorkTypeBusiness,
            IEmployeeBusiness EmployeeBusiness,
            IConcurrentWorkReplacementBusiness ConcurrentWorkReplacementBusiness)
		{
			this.ConcurrentWorkAssignmentBusiness = concurrentworkassignmentBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ConcurrentWorkTypeBusiness = ConcurrentWorkTypeBusiness;
            this.ConcurrentWorkReplacementBusiness = ConcurrentWorkReplacementBusiness;
		}
		
		//
        // GET: /ConcurrentWorkAssignment/

        private void SetViewData()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();

            // Danh sách Loại công việc kiêm nhiệm
            List<ConcurrentWorkType> ListWorkType = new List<ConcurrentWorkType>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = GlobalInfo.SchoolID;
            dic["IsActive"] = true;
            //dic["AppliedLevel"] = GlobalInfo.AppliedLevel;
            ListWorkType = ConcurrentWorkTypeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            if (ListWorkType.Count == 0)
            {
                ViewData[ConcurrentWorkAssignmentConstants.IS_ENABLE_NOTE] = true;
            }
            else
            {
                ViewData[ConcurrentWorkAssignmentConstants.IS_ENABLE_NOTE] = false;
            }
            ViewData[ConcurrentWorkAssignmentConstants.LIST_WORKTYPE] = new SelectList(ListWorkType, "ConcurrentWorkTypeID", "Resolution");

            // Danh sách tổ bộ môn
            List<SchoolFaculty> ListFaculty = new List<SchoolFaculty>();
            if (GlobalInfo.SchoolID != null)
            {
                ListFaculty = SchoolFacultyBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, new Dictionary<string, object>()).OrderBy(o => o.FacultyName).ToList();
            }
            ViewData[ConcurrentWorkAssignmentConstants.LIST_FACULTY] = new SelectList(ListFaculty, "SchoolFacultyID", "FacultyName");
            ViewData[ConcurrentWorkAssignmentConstants.HAS_PERMISSION] = GetMenupermission("ConcurrentWorkAssignment", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin);
            // Danh sách giáo viên
            List<Employee> ListTeacher = new List<Employee>();
            ViewData[ConcurrentWorkAssignmentConstants.LIST_TEACHER] = ListTeacher;
        }
        private void SetViewDataWorkReplacement()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();

            // Danh sách Loại công việc kiêm nhiệm
            List<ConcurrentWorkType> ListWorkType = new List<ConcurrentWorkType>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = GlobalInfo.SchoolID;
            // dic["AppliedLevel"] = GlobalInfo.AppliedLevel;
            ListWorkType = ConcurrentWorkTypeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
            ViewData[ConcurrentWorkAssignmentConstants.LIST_WORKTYPEWORKREPLACEMENT] = new SelectList(ListWorkType, "ConcurrentWorkTypeID", "Resolution");

            // Danh sách tổ bộ môn
            List<SchoolFaculty> ListFaculty = new List<SchoolFaculty>();
            if (GlobalInfo.SchoolID != null)
            {
                ListFaculty = SchoolFacultyBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, new Dictionary<string, object>()).OrderBy(o => o.FacultyName).ToList();
            }
            ViewData[ConcurrentWorkAssignmentConstants.LIST_FACULTYWORKREPLACEMENT] = new SelectList(ListFaculty, "SchoolFacultyID", "FacultyName");

            // Danh sách giáo viên kiêm nhiệm
            List<Employee> ListTeacher = new List<Employee>();
            ViewData[ConcurrentWorkAssignmentConstants.LIST_TEACHERASSIGNED] = new SelectList(ListTeacher, "EmployeeID", "FullName");
            ViewData[ConcurrentWorkAssignmentConstants.HAS_PERMISSION_REPLACE] = GetMenupermission("ConcurrentWorkAssignment", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin);
            // Danh sách giáo viên thay thế
            List<Employee> ListReplacedTeacher = new List<Employee>();
            ViewData[ConcurrentWorkAssignmentConstants.LIST_REPLACEDTEACHER] = new SelectList(ListReplacedTeacher, "EmployeeID", "FullName");
        }
        public ActionResult Index()
        {
            /*Chuẩn bị dữ liệu cho tab phân công kiêm nhiệm*/
            GlobalInfo GlobalInfo = new GlobalInfo();

            SetViewData();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;

            IEnumerable<ConcurrentWorkAssignmentViewModel> lst1 = this._Search(SearchInfo);
            ViewData[ConcurrentWorkAssignmentConstants.LIST_CONCURRENTWORKASSIGNMENT] = lst1;
            /*End tab phân công kiêm nhiệm*/

            /*Chuẩn bị dữ liệu cho tab làm thay kiêm nhiệm*/
            SetViewDataWorkReplacement();
            IDictionary<string, object> SearchInfoWorkReplacement = new Dictionary<string, object>();

            //Get view data here
            SearchInfoWorkReplacement["AcademicYearID"] = GlobalInfo.AcademicYearID;
            SearchInfoWorkReplacement["EmploymentStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            IEnumerable<ConcurrentWorkReplacementViewModel> lst2 = this._SearchWorkReplacement(SearchInfoWorkReplacement);
            ViewData[ConcurrentWorkAssignmentConstants.LIST_CONCURRENTWORKREPLACEMENT] = lst2;
            /*End Chuẩn bị dữ liệu cho tab làm thay kiêm nhiệm*/
            return View();
        }
        private List<ConcurrentWorkReplacementViewModel> _SearchWorkReplacement(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ConcurrentWorkReplacement> query = this.ConcurrentWorkReplacementBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            List<ConcurrentWorkReplacementViewModel> lst = query.Select(o => new ConcurrentWorkReplacementViewModel
            {
                ConcurrentWorkReplacementID = o.ConcurrentWorkReplacementID,
                TeacherID = o.TeacherID,
                TeacherName = o.Employee.FullName,
                TeacherShortName = o.Employee.Name,
                SchoolFacultyID = o.SchoolFacultyID,
                ConcurrentWorkTypeID = o.ConcurrentWorkAssignment.ConcurrentWorkTypeID,
                ConcurrentWorkTypeName = o.ConcurrentWorkAssignment.ConcurrentWorkType.Resolution,
                ReplacedTeacherID = o.ReplacedTeacherID,
                ReplacedTeacherName = o.Employee1.FullName,
                FromDate = o.FromDate,
                ToDate = o.ToDate,
                Description = o.Description
            }).ToList();
         
            if (lst.Count > 0 && lst != null)
            {
                lst = lst.OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.TeacherShortName + " " + o.TeacherName)).ToList();
            }
            else
            {
                lst = new List<ConcurrentWorkReplacementViewModel>();
            }
            return lst;
        }
		//
        // GET: /ConcurrentWorkAssignment/Search

        public PartialViewResult Search()
        {
            SetViewData();
            GlobalInfo GlobalInfo = new GlobalInfo();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
			//

            IEnumerable<ConcurrentWorkAssignmentViewModel> lst = this._Search(SearchInfo);
            ViewData[ConcurrentWorkAssignmentConstants.LIST_CONCURRENTWORKASSIGNMENT] = lst;

            //Get view data here
            ViewData[ConcurrentWorkAssignmentConstants.HAS_PERMISSION] = GetMenupermission("ConcurrentWorkAssignment", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin);
            return PartialView("_List");
        }

        public PartialViewResult SearchWorkReplacement()
        {
            SetViewDataWorkReplacement();

            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            SearchInfo["EmploymentStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            //

            IEnumerable<ConcurrentWorkReplacementViewModel> lst = this._SearchWorkReplacement(SearchInfo);
            ViewData[ConcurrentWorkAssignmentConstants.LIST_CONCURRENTWORKREPLACEMENT] = lst;

            //Get view data here
            ViewData[ConcurrentWorkAssignmentConstants.HAS_PERMISSION_REPLACE] = GetMenupermission("ConcurrentWorkAssignment", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin);
            return PartialView("_ListWorkReplacement");
        }

        public PartialViewResult AjaxLoadTeacher()
        {
            GlobalInfo global = new GlobalInfo();
            int? FacultyID = SMAS.Business.Common.Utils.GetInt(Request["FacultyID"]);
            int? ConcurrentWorkTypeID = SMAS.Business.Common.Utils.GetInt(Request["ConcurrentWorkTypeID"]);
            if (FacultyID != null && FacultyID > 0 && ConcurrentWorkTypeID > 0)
            {
                IEnumerable<Employee> lst = this.EmployeeBusiness
                                                .SearchWorkingTeacherByFaculty(new GlobalInfo().SchoolID.Value, FacultyID.Value)
                                                .OrderBy(o => o.Name)
                                                .ThenBy(o=>o.FullName)
                                                .ToList();
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["FacultyID"] = FacultyID;
                SearchInfo["ConcurrentWorkTypeID"] = ConcurrentWorkTypeID;
                SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
                IEnumerable<ConcurrentWorkAssignment> lstConcurrentWorkAssignment = this.ConcurrentWorkAssignmentBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
                IEnumerable<Employee> lstEmployee = lstConcurrentWorkAssignment.Select(o => o.Employee);
                IEnumerable<Employee> lst1 = lst.Except(lstEmployee);
                //lst = lst.Where(o => o.AppliedLevel == global.AppliedLevel).ToList();
                ViewData[ConcurrentWorkAssignmentConstants.LIST_TEACHER] = lst1.ToList();
            }
            else
            {
                ViewData[ConcurrentWorkAssignmentConstants.LIST_TEACHER] = new List<Employee>();
            }
            return PartialView("_TeacherList");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(ConcurrentWorkAssignmentViewModel frm)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (this.GetMenupermission("ConcurrentWorkAssignment", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (frm.ListTeacherID == null || frm.ListTeacherID.Count == 0)
            {
                return Json(new JsonMessage(Res.Get("ConcurrentWorkAssignment_Label_NoTeacherSelected"), JsonMessage.ERROR));
            }
            ConcurrentWorkAssignment concurrentworkassignment = new ConcurrentWorkAssignment();

            concurrentworkassignment.AcademicYearID = GlobalInfo.AcademicYearID.Value;
            concurrentworkassignment.ConcurrentWorkTypeID = frm.ConcurrentWorkTypeID;
            concurrentworkassignment.FacultyID = frm.FacultyID;
            concurrentworkassignment.SchoolID = GlobalInfo.SchoolID.Value;
            this.ConcurrentWorkAssignmentBusiness.Insert(concurrentworkassignment, frm.ListTeacherID);
            this.ConcurrentWorkAssignmentBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateWorkReplacement(ConcurrentWorkReplacementViewModel frm)
        {
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("ConcurrentWorkAssignment", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            ConcurrentWorkReplacement concurrentworkreplacement = new ConcurrentWorkReplacement();
            TryUpdateModel(concurrentworkreplacement);
            Utils.Utils.TrimObject(concurrentworkreplacement);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ConcurrentWorkTypeID"] = frm.ConcurrentWorkTypeID;
            dic["AcademicYearID"] = global.AcademicYearID;
            dic["SchoolID"] = global.SchoolID;
            dic["TeacherID"] = frm.TeacherID;
            ConcurrentWorkAssignment concurrentWorkAssignment = this.ConcurrentWorkAssignmentBusiness.Search(dic).FirstOrDefault();
            concurrentworkreplacement.ConcurrentWorkAssignmentID = concurrentWorkAssignment.ConcurrentWorkAssignmentID;
            concurrentworkreplacement.IsActive = true;
            concurrentworkreplacement.AcademicYearID = new GlobalInfo().AcademicYearID;
            concurrentworkreplacement.SchoolID = new GlobalInfo().SchoolID;

            this.ConcurrentWorkReplacementBusiness.Insert(concurrentworkreplacement);
            this.ConcurrentWorkReplacementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int id, int ConcurrentWorkTypeID)
        {
            try
            {
                GlobalInfo global = new GlobalInfo();
                if (this.GetMenupermission("ConcurrentWorkAssignment", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_UPDATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
                this.CheckPermissionForAction(id, "ConcurrentWorkAssignment");
                ConcurrentWorkAssignment concurrentworkassignment = this.ConcurrentWorkAssignmentBusiness.Find(id);
                concurrentworkassignment.ConcurrentWorkTypeID = (int)ConcurrentWorkTypeID;
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = global.AcademicYearID;
                SearchInfo["TeacherID"] = concurrentworkassignment.TeacherID;
                SearchInfo["ConcurrentWorkTypeID"] = ConcurrentWorkTypeID;
                if (this._Search(SearchInfo).Where(o => o.ConcurrentWorkAssignmentID != id).Count() > 0)
                {
                    string TearcherName = concurrentworkassignment.Employee.FullName;
                    string ConcurrentWorkTypeName = concurrentworkassignment.ConcurrentWorkType.Resolution;
                    string str = Res.Get("ConcurrentWorkAssignment_Employee") + " " + TearcherName + " " + Res.Get("ConcurrentWorkAssignment_ConcurrentWorkType") + " " + ConcurrentWorkTypeName + Res.Get("ConcurrentWorkAssignment_Choose");
                    return Json(new JsonMessage(str,"error"));      
                }
                this.ConcurrentWorkAssignmentBusiness.Update(concurrentworkassignment);
                this.ConcurrentWorkAssignmentBusiness.Save();
            }
            catch (BusinessException be)
            {
                return Json(new JsonMessage(be));
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditWorkReplacement(ConcurrentWorkReplacementViewModel frm)
        {
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("ConcurrentWorkAssignment", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.CheckPermissionForAction(frm.ConcurrentWorkReplacementID, "ConcurrentWorkReplacement");
            ConcurrentWorkReplacement concurrentworkreplacement = this.ConcurrentWorkReplacementBusiness.Find(frm.ConcurrentWorkReplacementID);
            TryUpdateModel(concurrentworkreplacement);
            Utils.Utils.TrimObject(concurrentworkreplacement);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ConcurrentWorkTypeID"] = frm.ConcurrentWorkTypeID;
            dic["AcademicYearID"] = global.AcademicYearID;
            dic["TeacherID"] = frm.TeacherID;
            dic["SchoolID"] = global.SchoolID;
            ConcurrentWorkAssignment concurrentWorkAssignment = this.ConcurrentWorkAssignmentBusiness.Search(dic).FirstOrDefault();
            concurrentworkreplacement.ConcurrentWorkAssignmentID = concurrentWorkAssignment.ConcurrentWorkAssignmentID;

            this.ConcurrentWorkReplacementBusiness.Update(concurrentworkreplacement);
            this.ConcurrentWorkReplacementBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("ConcurrentWorkAssignment", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.CheckPermissionForAction(id, "ConcurrentWorkAssignment");
            this.ConcurrentWorkAssignmentBusiness.Delete(global.SchoolID.Value, id);
            this.ConcurrentWorkAssignmentBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteWorkReplacement(int id)
        {
            GlobalInfo global = new GlobalInfo();
            if (this.GetMenupermission("ConcurrentWorkAssignment", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.CheckPermissionForAction(id, "ConcurrentWorkReplacement");
            this.ConcurrentWorkReplacementBusiness.Delete(global.SchoolID.Value, id);
            this.ConcurrentWorkReplacementBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        

		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private List<ConcurrentWorkAssignmentViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            
            IQueryable<ConcurrentWorkAssignment> query = this.ConcurrentWorkAssignmentBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            List<ConcurrentWorkAssignmentViewModel> lst = query.Select(o => new ConcurrentWorkAssignmentViewModel {               
						ConcurrentWorkAssignmentID = o.ConcurrentWorkAssignmentID,														
						FacultyID = o.FacultyID,								
						ConcurrentWorkTypeID = o.ConcurrentWorkTypeID,								
                        FacultyName = o.SchoolFaculty.FacultyName,
                        TeacherShortName = o.Employee.Name,
                        TeacherCode = o.Employee.EmployeeCode,
                        TeacherName = o.Employee.FullName,
                        WorkTypeName = o.ConcurrentWorkType.Resolution
            }).ToList();
            if (lst.Count > 0 && lst != null)
            {
                lst = lst.OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.TeacherShortName + " " + o.TeacherName)).ToList();
            }
            else
            {
                lst = new List<ConcurrentWorkAssignmentViewModel>();
            }

            return lst;
        }


        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadReplacedTeacher(int? SchoolFacultyID, int? teacherID)
        {
            IEnumerable<Employee> lst = new List<Employee>();
            GlobalInfo global = new GlobalInfo();
            if (SchoolFacultyID != null)
            {
                lst = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(new GlobalInfo().SchoolID.Value, SchoolFacultyID.Value).OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            }
            if (lst == null)
                lst = new List<Employee>();

            //lst = lst.Where(o => o.AppliedLevel == global.AppliedLevel).ToList();
            lst = lst.Where(o => o.EmployeeID != teacherID);
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }

        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadAsignedTeacher(int? ConcurrentWorkTypeID)
        {
            GlobalInfo global = new GlobalInfo();
            IEnumerable<Employee> lst = new List<Employee>();
            if (ConcurrentWorkTypeID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ConcurrentWorkTypeID"] = ConcurrentWorkTypeID;
                dic["AcademicYearID"] = new GlobalInfo().AcademicYearID;
                lst = this.ConcurrentWorkAssignmentBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).Where(o => o.Employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING).Select(o => o.Employee).OrderBy(o => o.FullName);
            }
            if (lst == null)
                lst = new List<Employee>();
            //lst = lst.Where(o => o.AppliedLevel == global.AppliedLevel).ToList();
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }
    }
}





