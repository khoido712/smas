/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
using System.ComponentModel;
namespace SMAS.Web.Areas.ConcurrentWorkAssignmentArea.Models
{
    public class ConcurrentWorkAssignmentViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 ConcurrentWorkAssignmentID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("ConcurrentWorkAssignment_Label_TeacherName")]
        [ReadOnly(true)]
        public string TeacherName { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("ConcurrentWorkAssignment_Label_TeacherName")]
        public string TeacherShortName { get; set; }


        [ScaffoldColumn(false)]
        [ResourceDisplayName("ConcurrentWorkAssignment_Label_TeacherCode")]
        [ReadOnly(true)]
        public string TeacherCode { get; set; }

        

        [ResourceDisplayName("ConcurrentWorkAssignment_Label_WorkType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("OnChange", "AjaxLoadTeacher()")]
        [AdditionalMetadata("ViewDataKey", ConcurrentWorkAssignmentConstants.LIST_WORKTYPE)]
		public System.Int32 ConcurrentWorkTypeID { get; set; }

        
        [ScaffoldColumn(false)]
        [ResourceDisplayName("ConcurrentWorkAssignment_Label_WorkType")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ConcurrentWorkAssignmentConstants.LIST_WORKTYPE)]
        public string WorkTypeName { get; set; }



        [ResourceDisplayName("ConcurrentWorkAssignment_Label_Faculty")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("OnChange", "AjaxLoadTeacher()")]
        [AdditionalMetadata("ViewDataKey", ConcurrentWorkAssignmentConstants.LIST_FACULTY)]
        public System.Nullable<System.Int32> FacultyID { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("ConcurrentWorkAssignment_Label_Faculty")]
        [ReadOnly(true)]
        public string FacultyName { get; set; }


        [ScaffoldColumn(false)]
        public List<int> ListTeacherID { get; set; }
    }
}


