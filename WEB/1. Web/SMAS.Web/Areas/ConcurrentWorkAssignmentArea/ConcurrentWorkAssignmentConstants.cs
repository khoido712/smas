/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ConcurrentWorkAssignmentArea
{
    public class ConcurrentWorkAssignmentConstants
    {
        public const string LIST_CONCURRENTWORKASSIGNMENT = "listConcurrentWorkAssignment";
        public const string LIST_TEACHER = "LIST_TEACHER";
        public const string LIST_FACULTY = "LIST_FACULTY";
        public const string LIST_WORKTYPE = "LIST_WORKTYPE";
        public const string IS_ENABLE_NOTE = "IsEnableNote";
        /**/
        public const string LIST_CONCURRENTWORKREPLACEMENT = "listConcurrentWorkReplacement";
        public const string LIST_WORKTYPEWORKREPLACEMENT = "LIST_WORKTYPEWORKREPLACEMENT";
        public const string LIST_TEACHERASSIGNED = "LIST_TEACHERASSIGNED";
        public const string LIST_FACULTYWORKREPLACEMENT = "LIST_FACULTYWORKREPLACEMENT";
        public const string LIST_REPLACEDTEACHER = "LIST_REPLACEDTEACHER";
        public const string HAS_PERMISSION = "haspermission";
        public const string HAS_PERMISSION_REPLACE = "haspermissionreplace"; 
        public const int PAGE_SIZE = 10;
    }
}