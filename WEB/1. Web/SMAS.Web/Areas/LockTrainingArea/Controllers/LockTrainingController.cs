﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.LockTrainingArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using Telerik.Web.Mvc;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.LockTrainingArea.Controllers
{
    /// <summary>
    /// QuangLM
    /// Edited 24/01/2014
    /// Khoa diem danh vi pham
    /// </summary>
    public class LockTrainingController : BaseController
    {
        private readonly ILockTrainingBusiness LockTrainingBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;

        public LockTrainingController(ILockTrainingBusiness LockTrainingBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IAcademicYearBusiness AcademicYearBusiness)
        {
            this.LockTrainingBusiness = LockTrainingBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }

        //
        // GET: /LockTrainingController/

        #region trang chu
        public ViewResult Index()
        {
            // lay du lieu load mac dinh
            SetViewData(null);
            return View();
        }
        #endregion

        /// <summary>
        /// Load du lieu khoa diem danh len grid
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        public PartialViewResult LoadGrid(int? EducationLevelID)
        {
            SetViewData(EducationLevelID);
            return PartialView("_List");
        }


        [SkipCheckRole]
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Save(LockTrainingModelSave model)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            if (GetMenupermission("LockTraining", globalInfo.UserAccountID, globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (!ModelState.IsValid)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            List<int> listClassAbID = model.IsAbsence != null ? model.IsAbsence.Select(o => int.Parse(o)).ToList() : new List<int>();
            List<int> listClassViID = model.IsViolation != null ? model.IsViolation.Select(o => int.Parse(o)).ToList() : new List<int>();
            List<int> listClassID = listClassAbID.Union(listClassViID).ToList();
            if (listClassID.Count == 0)
            {
                throw new BusinessException("LockTraining_Label_NotChoose");
            }
            // Kiem tra tu ngay khong duoc lop hon den ngay
            if (model.FromDateSave.HasValue && model.ToDateSave < model.FromDateSave)
            {
                throw new BusinessException("Common_Validate_FromDateGreaterToDate");
            }
            // Lon hon ngay bat dau nam hoc
            if (model.ToDateSave < AcademicYearBusiness.Find(_globalInfo.AcademicYearID).FirstSemesterStartDate.Value)
            {
                throw new BusinessException("LockTraining_Label_GreaterStartAca");
            }
            // Lay danh sach khoa da co
            List<LockTraining> listLockTraining = LockTrainingBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>(){
                {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                {"EducationLevelID",model.EducationLevelID}
            }).ToList();
            foreach (int classID in listClassID)
            {
                LockTraining lockTraining = listLockTraining.FirstOrDefault(o => o.ClassID == classID);
                if (lockTraining == null)
                {
                    lockTraining = new LockTraining();
                    listLockTraining.Add(lockTraining);
                }
                lockTraining.SchoolID = _globalInfo.SchoolID.Value;
                lockTraining.AcademicYearID = _globalInfo.AcademicYearID.Value;
                lockTraining.ClassID = classID;
                lockTraining.FromDate = model.FromDateSave;
                lockTraining.ToDate = model.ToDateSave;
                if (listClassAbID.Contains(classID))
                {
                    if (listClassViID.Contains(classID))
                    {
                        lockTraining.TrainingType = GlobalConstants.LOCK_TRAINING_ABSENCE_VIOLATION;
                    }
                    else
                    {
                        lockTraining.TrainingType = GlobalConstants.LOCK_TRAINING_ABSENCE;
                    }
                }
                else
                {
                    lockTraining.TrainingType = GlobalConstants.LOCK_TRAINING_VIOLATION;
                }
            }
            LockTrainingBusiness.UpdateOrInSert(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, listLockTraining);
            LockTrainingBusiness.Save();
            return Json(new JsonMessage(Res.Get("LockTraining_Label_SaveSuc")));
        }

        private void SetViewData(int? EducationLevelID)
        {
            IEnumerable<EducationLevel> lsEducationLevel = _globalInfo.EducationLevels;
            ViewData[LockTrainingConstant.LS_EDUCATION] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            List<LockTrainingModel> listLockTraining = new List<LockTrainingModel>();
            if (EducationLevelID.HasValue)
            {
                IQueryable<LockTraining> iqLockTraining = LockTrainingBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                    {"EducationLevelID", EducationLevelID.Value}
                });
                IQueryable<ClassProfile> IqClasProfile = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>()
                {
                    {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                    {"EducationLevelID", EducationLevelID.Value}
                });
                // Join de lay du lieu
                listLockTraining = (from c in IqClasProfile
                                    join l in iqLockTraining on c.ClassProfileID equals l.ClassID into g
                                    from o in g.DefaultIfEmpty()
                                    select new LockTrainingModel
                                    {
                                        ClassID = c.ClassProfileID,
                                        ClassName = c.DisplayName,
                                        ClassOrderNumber = c.OrderNumber.HasValue ? c.OrderNumber.Value : 0,
                                        FromDate = o.FromDate,
                                        ToDate = o.ToDate,
                                        IsAbsence = (o.TrainingType == GlobalConstants.LOCK_TRAINING_ABSENCE || o.TrainingType == GlobalConstants.LOCK_TRAINING_ABSENCE_VIOLATION),
                                        IsViolation = (o.TrainingType == GlobalConstants.LOCK_TRAINING_VIOLATION || o.TrainingType == GlobalConstants.LOCK_TRAINING_ABSENCE_VIOLATION)
                                    }).OrderBy(o => o.ClassOrderNumber).ThenBy(o => o.ClassName).ToList();
                // Thiet lap lai STT
                int count = listLockTraining.Count;
                for (int i = 0; i < count; i++)
                {
                    LockTrainingModel lockTraining = listLockTraining[i];
                    lockTraining.Index = i + 1;
                }
            }
            ViewData[LockTrainingConstant.LS_LOCKTRAINING] = listLockTraining;
            SetViewDataPermission("LockTraining", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
        }

        #region Xu ly tren grid
        #region _SelectAjaxEditing - Load du lieu

        [GridAction]
        public ActionResult _SelectAjaxEditing()
        {
            List<LockTrainingModel> listLockTraining = (List<LockTrainingModel>)ViewData[LockTrainingConstant.LS_LOCKTRAINING];
            return View(new GridModel(listLockTraining));
        }

        #endregion

        #region _SaveAjaxEditing - Update du lieu
        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        
        [ValidateAntiForgeryToken]
        public ActionResult _SaveAjaxEditing(int id)
        {
            if (id != 0)
            {
                if (GetMenupermission("LockTraining", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
            }
            else
            {
                if (GetMenupermission("LockTraining", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
                {
                    throw new BusinessException("Validate_Permission_Teacher");
                }
            }

            
            if (!_globalInfo.IsCurrentYear)
            {
                throw new BusinessException("ClassAssigment_Error_IsCurrentYear");
            }

            //
            LockTrainingModel lockTrainingModel = new LockTrainingModel();
            //truyen du lieu tren form vao PeriodDeclarationObject
            if (TryUpdateModel(lockTrainingModel))
            {
                // Neu khong chon vi pham hoac diem danh thi xoa
                if (!lockTrainingModel.IsAbsence && !lockTrainingModel.IsViolation)
                {
                    LockTraining lockTraining = LockTrainingBusiness.SearchByClass(id).FirstOrDefault();
                    if (lockTraining != null)
                    {
                        CheckPermissionForAction(lockTraining.LockTrainingID , "LockTraining");
                        LockTrainingBusiness.CheckAvailable(lockTraining.LockTrainingID, "Common_Validate_NotAvailable");
                        LockTrainingBusiness.Delete(lockTraining.LockTrainingID);
                        LockTrainingBusiness.Save();
                    }
                }
                else
                {
                    lockTrainingModel.ClassID = id;
                    IDictionary<string, object> dicSearch = new Dictionary<string, object>();
                    dicSearch["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                    dicSearch["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;

                    // Kiem tra tu ngay khong duoc lop hon den ngay
                    if (lockTrainingModel.FromDate.HasValue && lockTrainingModel.ToDate.HasValue && lockTrainingModel.ToDate < lockTrainingModel.FromDate)
                    {
                        throw new BusinessException("Common_Validate_FromDateGreaterToDate");
                    }
                    // Lon hon ngay bat dau nam hoc
                    if (lockTrainingModel.ToDate < AcademicYearBusiness.Find(_globalInfo.AcademicYearID).FirstSemesterStartDate.Value)
                    {
                        throw new BusinessException("LockTraining_Label_GreaterStartAca");
                    }
                    //du lieu hop le thi convert data va insert
                    LockTraining lockTraining = LockTrainingBusiness.SearchByClass(id).FirstOrDefault();
                    if (lockTraining == null)
                    {
                        lockTraining = new LockTraining();
                        lockTraining.SchoolID = _globalInfo.SchoolID.Value;
                        lockTraining.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    }
                    Utils.Utils.BindTo(lockTrainingModel, lockTraining);
                    // Lay them gia tri ve loai khoa
                    if (lockTrainingModel.IsAbsence)
                    {
                        if (lockTrainingModel.IsViolation)
                        {
                            lockTraining.TrainingType = GlobalConstants.LOCK_TRAINING_ABSENCE_VIOLATION;
                        }
                        else
                        {
                            lockTraining.TrainingType = GlobalConstants.LOCK_TRAINING_ABSENCE;
                        }
                    }
                    else
                    {
                        lockTraining.TrainingType = GlobalConstants.LOCK_TRAINING_VIOLATION;
                    }
                    // Luu
                    LockTrainingBusiness.UpdateOrInSert(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, new List<LockTraining>() { lockTraining });
                    LockTrainingBusiness.Save();
                }
                List<LockTrainingModel> listLockTraining = new List<LockTrainingModel>();
                return View(new GridModel(listLockTraining));
            }
            else
            {
                throw new BusinessException("Common_Error_DataInputError");
            }

        }
        #endregion _SaveAjaxEditing

        #region _DeleteAjaxEditing - Xoa thong tin khoa

        //[AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        [GridAction]
        
        [ValidateAntiForgeryToken]
        public ActionResult _DeleteAjaxEditing(int id)
        {
            if (GetMenupermission("LockTraining", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            
            if (!_globalInfo.IsCurrentYear)
            {
                throw new BusinessException("ClassAssigment_Error_IsCurrentYear");
            }
            LockTraining lockTraining = LockTrainingBusiness.SearchByClass(id).FirstOrDefault();
            if (lockTraining != null)
            {
                CheckPermissionForAction(lockTraining.LockTrainingID, "LockTraining");
                LockTrainingBusiness.CheckAvailable(lockTraining.LockTrainingID, "Common_Validate_NotAvailable");
                LockTrainingBusiness.Delete(lockTraining.LockTrainingID);
                LockTrainingBusiness.Save();
            }
            List<LockTrainingModel> listLockTraining = new List<LockTrainingModel>();
            return View(new GridModel(listLockTraining));
        }

        #endregion _DeleteAjaxEditing

        #endregion
    }
}
