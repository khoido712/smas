﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.LockTrainingArea
{
    public class LockTrainingAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LockTrainingArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LockTrainingArea_default",
                "LockTrainingArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}