using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;
using System.Web.Mvc;

namespace SMAS.Web.Areas.LockTrainingArea.Models
{
    public class LockTrainingModel
    {
        private const string ValidateDateTime = "Dữ liệu nhập vào không đúng định dạng ngày tháng";

        [ScaffoldColumn(false)]
        public int ClassID
        {
            get;
            set;
        }

        [ScaffoldColumn(false)]
        public int Index
        {
            get;
            set;
        }

        [ScaffoldColumn(false)]
        public int ClassOrderNumber
        {
            get;
            set;
        }

        [ResourceDisplayName("LockTraining_Label_ClassName")]
        public string ClassName
        {
            get;
            set;
        }

        [ResourceDisplayName("LockTraining_Label_Absence")]
        public bool IsAbsence
        {
            get;
            set;
        }

        [ResourceDisplayName("LockTraining_Label_Violation")]
        public bool IsViolation
        {
            get;
            set;
        }

        [ResourceDisplayName("LockTraining_Label_FromDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        public DateTime? FromDate
        {
            get;
            set;
        }

        [ResourceDisplayName("LockTraining_Label_ToDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]
        public DateTime? ToDate
        {
            get;
            set;
        }

    }
}
