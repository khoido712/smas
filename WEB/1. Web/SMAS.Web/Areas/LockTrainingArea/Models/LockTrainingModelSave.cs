using System;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;
using System.Web.Mvc;

namespace SMAS.Web.Areas.LockTrainingArea.Models
{
    public class LockTrainingModelSave
    {

        [ScaffoldColumn(false)]
        public int EducationLevelID
        {
            get;
            set;
        }

        [ScaffoldColumn(false)]
        public string[] IsAbsence
        {
            get;
            set;
        }

        [ScaffoldColumn(false)]
        public string[] IsViolation
        {
            get;
            set;
        }

        [ResourceDisplayName("LockTraining_Label_FromDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        public DateTime? FromDateSave
        {
            get;
            set;
        }

        [ResourceDisplayName("LockTraining_Label_ToDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]
        public DateTime ToDateSave
        {
            get;
            set;
        }

    }
}
