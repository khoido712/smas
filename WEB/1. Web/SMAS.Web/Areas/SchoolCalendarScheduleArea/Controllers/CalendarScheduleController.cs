﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.SchoolCalendarScheduleArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Web.Areas.ClassProfileArea;
using System.IO;
using SMAS.Business.Business;
using System.Web.Script.Serialization;

namespace SMAS.Web.Areas.SchoolCalendarScheduleArea.Controllers
{
    public class CalendarScheduleController : BaseController
    {
        //
        // GET: /SchoolCalendarScheduleArea/CalendarSchedule/
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IClassPropertyCatBusiness ClassPropertyCatBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISubCommitteeBusiness SubCommitteeBusiness;
        private readonly IPropertyOfClassBusiness PropertyOfClassBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ICalendarBusiness CalendarBusiness;
        private readonly ICalendarScheduleBusiness CalendarScheduleBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        //Contructor
        public CalendarScheduleController(IClassProfileBusiness classprofileBusiness, IEducationLevelBusiness EducationLevelBusiness,
            IClassPropertyCatBusiness ClassPropertyCatBusiness, ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness,
            IAcademicYearBusiness AcademicYearBusiness, ISubCommitteeBusiness SubCommitteeBusiness,
            IPropertyOfClassBusiness PropertyOfClassBusiness, IClassSubjectBusiness ClassSubjectBusiness,
            ISchoolFacultyBusiness SchoolFacultyBusiness, IEmployeeBusiness EmployeeBusiness,
            ITeachingAssignmentBusiness teachingAssignmentBusiness, ICalendarScheduleBusiness calendarScheduleBusiness,
            ISubjectCatBusiness SubjectCatBusiness, ICalendarBusiness calendarBusiness)
        {
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.CalendarBusiness = calendarBusiness;
            this.ClassProfileBusiness = classprofileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassPropertyCatBusiness = ClassPropertyCatBusiness;
            this.SchoolSubsidiaryBusiness = SchoolSubsidiaryBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.SubCommitteeBusiness = SubCommitteeBusiness;
            this.PropertyOfClassBusiness = PropertyOfClassBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.CalendarScheduleBusiness = calendarScheduleBusiness;
        }

        /// <summary>
        /// Index view with classID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Index()
        {
            int classID = 0;
            if (!string.IsNullOrEmpty(Request["classID"]))
            {
                int.TryParse(Request["classID"].ToString(), out classID);
                ViewData["requestClassID"] = classID;
            }          
            //Search condinations
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            //Lay danh sach lop hoc tuong ung voi user login
            List<ClassProfile> lstClass = null;
            if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal || _globalInfo.IsViewAll)
            {
                //Voi hieu truong va admin truong hoac cau hinh cho xem tat ca thi thay tat ca cac lop
                lstClass = ClassProfileBusiness.Search(dic).OrderBy(item => item.DisplayName).ToList();
            }
            else
            {
                //Chi lay cac lop thuoc GVBM + Giam thi
                var lstGVBM = ClassProfileBusiness.GetListClassBySubjectTeacher(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value);
                var lstGT = ClassProfileBusiness.GetListClassByOverSeeing(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value);
                lstClass = lstGVBM.Union(lstGT).OrderBy(item => item.DisplayName).ToList();
            }
            //Check for default class
            ClassProfile currClass = null;
            if (lstClass.Count > 0)
            {
                if (classID > 0)
                {
                    currClass = lstClass.Where(c => c.ClassProfileID == classID).FirstOrDefault();
                }
                else
                {
                    currClass = lstClass.FirstOrDefault();
                }
            }
            //Grant access to curr class
            if (currClass != null)
            {
                List<CalendarScheduleBusiness.ClassSection> lstSection = new List<CalendarScheduleBusiness.ClassSection>();
                CalendarScheduleBusiness.GetSection((currClass.Section.HasValue) ? currClass.Section.Value : 0, ref lstSection);
                
                ViewData["ScheduleList"] = LoadScheduleCalendar(currClass.ClassProfileID, DateTime.Now);
                ViewData["lstSectionSearch"] = lstSection;
                ViewData["SelectedClass"] = currClass.ClassProfileID;
            }
            ViewData["lstClass"] = new SelectList(lstClass, "ClassProfileID", "DisplayName", (currClass != null) ? currClass.ClassProfileID : 0);
            return View();
        }
        /// <summary>
        /// Load data
        /// </summary>
        /// <param name="classID"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public PartialViewResult LoadScheduleData(int? classID, DateTime? dateTime)
        {
            bool canEdit = true;
            List<ScheduleModel> lstData = null;
            if (classID.HasValue)
            {
                ClassProfile itmClass = null;
                dateTime = (dateTime.HasValue) ? dateTime.Value : DateTime.Now;
                if (_globalInfo.IsAdminSchoolRole || _globalInfo.IsRolePrincipal)
                {
                    itmClass = ClassProfileBusiness.Find(classID.Value);   
                }
                else
                {                    
                    //Check Teacher Role of Class
                    var lstGVBM = ClassProfileBusiness.GetListClassBySubjectTeacher(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value);
                    var lstGT = ClassProfileBusiness.GetListClassByOverSeeing(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value);
                    itmClass = lstGVBM.Union(lstGT).Where(a => a.ClassProfileID == classID.Value).FirstOrDefault();
                    if (itmClass == null && _globalInfo.IsViewAll)
                    {
                        //Neu như không phai quyen cua giao vien, nhung duoc phep xem toan truong
                        itmClass = ClassProfileBusiness.Find(classID.Value);
                        canEdit = false;
                    }
                }
                if (itmClass != null)
                {
                    List<CalendarScheduleBusiness.ClassSection> lstSection = new List<CalendarScheduleBusiness.ClassSection>();
                    CalendarScheduleBusiness.GetSection((itmClass.Section.HasValue) ? itmClass.Section.Value : 0, ref lstSection);
                    ViewData["lstSectionSearch"] = lstSection;
                    ViewData["canEdit"] = canEdit;
                    lstData = LoadScheduleCalendar(classID.Value, dateTime.Value);
                }
            }
            return PartialView("_Schedule", lstData);
        }
        #region Load Schedule
        private List<ScheduleModel> LoadScheduleCalendar(int classID, DateTime applyDate)
        {
            List<ScheduleModel> lstRes = new List<ScheduleModel>();
            //Lay thong tin calendar cua ngay hien tai
            //Search condinations
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
            dic["ClassID"] = classID;
            dic["DayOfWeek"] = getDayOfWeek(applyDate);
            dic["DaySelected"] = applyDate;
            dic["Semester"] = getSemester(applyDate);
            List<Calendar> lstCalendar = CalendarBusiness.Search(dic).ToList();
            List<CalendarSchedule> lstSchedule = CalendarScheduleBusiness.Search(dic).ToList();
            Calendar tmpCalendar = null;
            CalendarSchedule tmpSchedule = null;
            ScheduleModel tmpMdl = null;
            for (int i = 0; i < lstCalendar.Count; i++)
            {
                tmpCalendar = lstCalendar[i];
                tmpSchedule = lstSchedule.Where(a => a.CalendarID == tmpCalendar.CalendarID).FirstOrDefault();
                if (tmpCalendar != null)
                {
                    //Bind Schedule to Model
                    tmpMdl = new ScheduleModel();
                    tmpMdl.CalendarID = tmpCalendar.CalendarID;
                    tmpMdl.NumberOfPeriod = (tmpCalendar.NumberOfPeriod.HasValue) ? tmpCalendar.NumberOfPeriod.Value : 0;
                    tmpMdl.Section = (tmpCalendar.Section.HasValue) ? tmpCalendar.Section.Value : 0;
                    tmpMdl.SubjectID = tmpCalendar.SubjectID;
                    tmpMdl.SubjectName = tmpCalendar.SubjectCat.DisplayName;
                    if (tmpSchedule != null)
                    {
                        tmpMdl.Preparation = tmpSchedule.Preparation;
                        tmpMdl.HomeWork = tmpSchedule.HomeWork;
                        tmpMdl.CalendarScheduleID = tmpSchedule.CalendarScheduleID;
                        tmpMdl.SubjectTitle = tmpSchedule.SubjectTitle;
                    }
                    lstRes.Add(tmpMdl);
                }
            }
            return lstRes;
        }
        /// <summary>
        /// Save schedule calendar
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [SMAS.Web.Filter.ActionAudit]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveSchedule( DateTime? applyDate, int? classID)
        {
            List<ScheduleModel> model = new List<ScheduleModel>();
            model = new JavaScriptSerializer().Deserialize<List<ScheduleModel>>(Request["model"]);
            int updateCounter = 0;
            if(!applyDate.HasValue)
            {
                return Json(new JsonMessage(Res.Get("CalendarSchedule_Update_Time_Error"), "ERROR"));
            }
            if (!_globalInfo.IsCurrentYear)
            {
                return Json(new JsonMessage(Res.Get("CalendarSchedule_Update_Year_Error"), "ERROR"));
            }
            if(!AcademicYearBusiness.CheckTimeInYear(_globalInfo.AcademicYearID.Value, applyDate.Value))
            {
                return Json(new JsonMessage(Res.Get("CalendarSchedule_Update_Time_Error"), "ERROR"));
            }
            if (!classID.HasValue)
            {
                return Json(new JsonMessage(Res.Get("CalendarSchedule_Update_Class_Error"), "ERROR"));
            }
            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsRolePrincipal)
            {
                //Check Teacher Role of Class
                var lstGVBM = ClassProfileBusiness.GetListClassBySubjectTeacher(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value);
                var lstGT = ClassProfileBusiness.GetListClassByOverSeeing(_globalInfo.EmployeeID.Value, _globalInfo.AcademicYearID.Value);
                var itmClass = lstGVBM.Union(lstGT).Where(a => a.ClassProfileID == classID.Value).FirstOrDefault();
                if (itmClass == null)
                {
                    return Json(new JsonMessage(Res.Get("CalendarSchedule_Update_Class_Permission_Error"), "ERROR"));
                }
            }
            CalendarSchedule scheduleItm = null;
            foreach (ScheduleModel item in model)
            {
                bool updated = false;
                if (item.CalendarScheduleID > 0)
                {
                    //Update Calendar Schedule
                    scheduleItm = CalendarScheduleBusiness.Find(item.CalendarScheduleID);
                    if (scheduleItm != null)
                    {
                        scheduleItm.HomeWork = item.HomeWork;
                        scheduleItm.Preparation = item.Preparation;
                        scheduleItm.SubjectTitle = item.SubjectTitle;
                        CalendarScheduleBusiness.Update(scheduleItm);
                        updated = true;
                        updateCounter++;
                    }
                }
                if (!updated)
                {                    
                    scheduleItm = new CalendarSchedule();
                    scheduleItm.ApplyDate = applyDate.Value;
                    scheduleItm.CalendarID = item.CalendarID;
                    scheduleItm.CreatedDate = DateTime.Now;
                    scheduleItm.HomeWork = item.HomeWork;
                    scheduleItm.Preparation = item.Preparation;
                    scheduleItm.SubjectTitle = item.SubjectTitle;
                    CalendarScheduleBusiness.Insert(scheduleItm);
                    updateCounter++;
                }
            }
            if (updateCounter > 0)
            {
                //Save calendar data
                CalendarScheduleBusiness.Save();
            }
            SetViewDataActionAudit(string.Empty, string.Empty, string.Empty, "Cập nhật thông tin sổ báo bài.");
            return Json(new JsonMessage(Res.Get("CalendarSchedule_Update_OK")));
        }
        /// <summary>
        /// Lay hoc ky
        /// </summary>
        /// <param name="curDate"></param>
        /// <returns></returns>
        private int getSemester(DateTime curDate)
        {
            if(AcademicYearBusiness.IsSecondSemesterTime(_globalInfo.AcademicYearID.Value, curDate)) return 2;            
            return 1;
        }
        /// <summary>
        /// Get dayofWeek in calendar
        /// </summary>
        /// <param name="curDate"></param>
        /// <returns></returns>
        private int getDayOfWeek(DateTime curDate)
        {
            DayOfWeek dayofWeek = curDate.DayOfWeek;
            switch (dayofWeek)
            {
                case DayOfWeek.Sunday: return 1;
                case DayOfWeek.Monday: return 2;
                case DayOfWeek.Tuesday: return 3;
                case DayOfWeek.Wednesday: return 4;
                case DayOfWeek.Thursday: return 5;
                case DayOfWeek.Friday: return 6;
                case DayOfWeek.Saturday: return 7;
            }
            return 0;
        }
        #endregion
    }
}
