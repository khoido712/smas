﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.SchoolCalendarScheduleArea
{
    public class CalendarScheduleContants
    {
        /// <summary>
        /// Ngay bat dau cua tuan (thứ 2)
        /// </summary>
        public const int DAY_IN_WEEK_START = 2;
        /// <summary>
        /// Ngày kết thúc của tuần (thứ 7)
        /// </summary>
        public const int DAY_IN_WEEK_END = 7;
        /// <summary>
        /// Số tiết trên một buổi
        /// </summary>
        public const int NUMBER_OF_TIME_UNIT_IN_SECTION = 5;
    }
}