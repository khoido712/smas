﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SchoolCalendarScheduleArea;
using SMAS.Models.Models;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.SchoolCalendarScheduleArea.Models
{
    public class SearchModel
    {
        /// <summary>
        /// Ngay chon
        /// </summary>
        [ResourceDisplayName("")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [DataConstraint(DataConstraintAttribute.LESS_EQUALS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [UIHint("DateTimePicker")]
        [AdditionalMetadata("OnChange", "LoadSchedule(this)")]
        [RegularExpression(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$")]
        public DateTime? DaySelected { get; set; }
        /// <summary>
        /// Lớp
        /// </summary>
        [ResourceDisplayName("Calendar_Label_ClassID")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", "lstClass")]
        [AdditionalMetadata("OnChange", "LoadSchedule(this)")]
        [AdditionalMetadata("class", "MySelectBoxClass")]
        [AdditionalMetadata("style", "opacity: 0; height: 20px;")]
        public int? ClassID { get; set; }
    }
}