﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.SchoolCalendarArea;
using SMAS.Models.Models;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.SchoolCalendarScheduleArea.Models
{
    public class ScheduleModel
    {
        /// <summary>
        /// Tiet hoc
        /// </summary>
        public int NumberOfPeriod { get; set; }
        /// <summary>
        /// Buoi hoc
        /// </summary>
        public int Section { get; set; }
        /// <summary>
        /// ID cua calendar Object
        /// </summary>
        public int CalendarID { get; set; }
        /// <summary>
        /// ID Mon hoc duoc chon
        /// </summary>
        public int SubjectID { get; set; }
        /// <summary>
        /// Ten mon hoc
        /// </summary>
        public string SubjectName { get; set; }
        /// <summary>
        /// Shedule ID
        /// </summary>
        public int CalendarScheduleID { get; set; }
        /// <summary>
        /// Ten bai hoc
        /// </summary>
        public string SubjectTitle { get; set; }
        /// <summary>
        /// Noi dung chuan bi
        /// </summary>
        public string Preparation { get; set; }
        /// <summary>
        /// Bai tap ve nha
        /// </summary>
        public string HomeWork { get; set; }
        /// <summary>
        /// Ngay ap dung
        /// </summary>
        public ScheduleModel()
        {
            NumberOfPeriod = Section = CalendarID  = SubjectID = CalendarScheduleID = 0;
            SubjectTitle = Preparation = HomeWork = SubjectName = string.Empty;
        }
    }
}