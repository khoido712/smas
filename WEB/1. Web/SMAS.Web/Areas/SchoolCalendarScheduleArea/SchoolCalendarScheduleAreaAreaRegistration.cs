﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SchoolCalendarScheduleArea
{
    public class SchoolCalendarScheduleAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SchoolCalendarScheduleArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SchoolCalendarScheduleArea_default",
                "SchoolCalendarScheduleArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
