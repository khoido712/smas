﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ConfigConductRankingArea
{
    public class ConfigConductRankingAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ConfigConductRankingArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ConfigConductRankingArea_default",
                "ConfigConductRankingArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
