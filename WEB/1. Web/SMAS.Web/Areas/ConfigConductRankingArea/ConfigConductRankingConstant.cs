﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ConfigConductRankingArea
{
    public class ConfigConductRankingConstant
    {
        public const string LS_EDUCATIONLEVEL = "lstEducationLevel";
        public const string LS_CONDUCFFORSEMESTER_PERIOD = "lstConductForSemsterOrPeriod";
        public const string LS_GIRD_CONDUCT_SEMESTER = "listGirdConductSePe";
        public const string CHECK_LOAD_GIRD = "ConductID";
        public const string EDUCATIONLEVELID = "EducationLevelID";
        public const int CONDUCT_SEMESTER = 0;
        public const string LS_PERIOD_SEMESTER = "lstPeriodSemester";
        public const string ROLE_OF_USER = "ROLE_OF_USER";
      
    }
}