﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Models.Models;
using Telerik.Web.Mvc.UI;
using System.Data.Objects.SqlClient;
using System.Web.Security;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using Telerik.Web.Mvc;
using System.Transactions;
using System.IO;
using SMAS.Web.Filter;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.Web.Areas.ConfigConductRankingArea.Models;

namespace SMAS.Web.Areas.ConfigConductRankingArea.Controllers
{
    public class ConfigConductRankingController : BaseController
    {
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IConfigConductRankingBusiness ConfigConductRankingBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;

        public ConfigConductRankingController(IEducationLevelBusiness educationLevelBusiness,
            IConfigConductRankingBusiness ConfigConductRankingBusiness,
            IClassProfileBusiness ClassProfileBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IPeriodDeclarationBusiness PeriodDeclarationBusiness)
        {
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ConfigConductRankingBusiness = ConfigConductRankingBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
        }

        //
        // GET: /ConfigConductRankingArea/ConfigConductRanking/

        public ActionResult Index()
        {
            SetViewData();
            GlobalInfo Global = new GlobalInfo();
            SetViewDataPermission("ConfigConductRanking", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin, Global.RoleID);           

            return View();
        }

        public void SetViewData()
        {
            //Đổ dữ liệu vào EducationLevelcombobox
            IEnumerable<EducationLevel> lsEducationLevel = new GlobalInfo().EducationLevels;
            ViewData[ConfigConductRankingConstant.LS_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");

            //Đổ dữ liệu vào ConductSemesterOrPeriodCombobox
            //Quy định 1: Đợt của HKI _ 2: Đợt của học kỳ 2 _ 3: Học kỳ
            List<ComboObject> lstConductForSemsterOrPeriod = new List<ComboObject>();
            lstConductForSemsterOrPeriod.Add(new ComboObject("0", "Học kỳ"));
            lstConductForSemsterOrPeriod.Add(new ComboObject("1", "Đợt của HKI"));
            lstConductForSemsterOrPeriod.Add(new ComboObject("2", "Đợt của HKII"));
            ViewData[ConfigConductRankingConstant.LS_CONDUCFFORSEMESTER_PERIOD] = new SelectList(lstConductForSemsterOrPeriod, "key", "value", 3);

            //Load default hạnh kiểm học kỳ
            List<ConductSeOrPeViewModel> lstGirdConductSePe = _GetGirdConductSemesterOrPeriod(null, ConfigConductRankingConstant.CONDUCT_SEMESTER);
            ViewData[ConfigConductRankingConstant.LS_GIRD_CONDUCT_SEMESTER] = lstGirdConductSePe.OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderNumber).ToList();
            ViewData[ConfigConductRankingConstant.CHECK_LOAD_GIRD] = ConfigConductRankingConstant.CONDUCT_SEMESTER;
            ViewData[ConfigConductRankingConstant.EDUCATIONLEVELID] = null;
        }

        /// <summary>
        /// Lấy danh sách khóa hạnh kiểm các lớp theo học kì
        /// </summary>
        /// <param name="educationlevelID"></param>
        /// <param name="semester"></param>
        public List<ConductSeOrPeViewModel> _GetGirdConductSemesterOrPeriod(int? educationlevelID, int conductSPID)
        {
            IDictionary<string, object> dicSearch = new Dictionary<string, object>()
            {
                {"SchoolID", _globalInfo.SchoolID.Value},
                {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                {"EducationLevelID", educationlevelID},
                {"AppliedLevel", _globalInfo.AppliedLevel}
            };
            List<PeriodDeclaration> lstPD = new List<PeriodDeclaration>();
            PeriodDeclaration objPD = null;
            //neu lay theo dot cua hoc kỳ
            if (conductSPID != 0)
            {
                dicSearch.Add("SemesterID", conductSPID);
                dicSearch.Add("Semester", conductSPID);
                //lấy danh sách các đợt của học kỳ
                lstPD = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch).ToList();
                ViewData[ConfigConductRankingConstant.LS_PERIOD_SEMESTER] = lstPD;
            }

            List<ConductSeOrPeViewModel> lstGirdConductSePe = new List<ConductSeOrPeViewModel>();
            ConductSeOrPeViewModel objCSPView = null;
            List<ConfigConductRanking> lsttempCCR = null; //khoi tao list khoa lop de cac record cung lop

            //lấy danh sách lớp đang học của trường
            List<ClassProfile> lstCP = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch).ToList();

            //lấy danh sách lớp có khóa hạnh kiểm 
            IDictionary<string, object> dicSearchCCR = new Dictionary<string, object>()
            {
                {"SchoolID", _globalInfo.SchoolID.Value},
                {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                {"EducationLevelID", educationlevelID},
                {"AppliedLevel", _globalInfo.AppliedLevel}
            };
            List<ConfigConductRanking> lstConfigCR = ConfigConductRankingBusiness.GetAllConfigConductRanking(dicSearchCCR).ToList();              

            //duyệt qua danh sách lớp của trường
            //kiểm tra lớp đang học của trường có tồn tại trong lớp có khóa hạnh kiểm hay hk nếu có thì add thêm các thuộc tính semster và periodc vào
            foreach (var item in lstCP)
            {
                objCSPView = new ConductSeOrPeViewModel();
                objCSPView.DicPeriodOfSemester = new Dictionary<int, int>();

                objCSPView.ClassID = item.ClassProfileID;
                objCSPView.ClassName = item.DisplayName;
                objCSPView.SchoolID = _globalInfo.SchoolID.Value;
                objCSPView.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objCSPView.EducationLevelID = item.EducationLevelID;

                //lay ra lop co khoa hanh kiem 
                lsttempCCR = lstConfigCR.Where(p => p.ClassID == item.ClassProfileID).ToList();
                if (lsttempCCR != null && lsttempCCR.Count > 0)
                {
                    for (int i = 0; i < lsttempCCR.Count; i++)
                    {
                        //kiem tra lop co khoa hanh kiem hoc kỳ
                        if (lsttempCCR[i].SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            objCSPView.SemesterHKI = lsttempCCR[i].SemesterID;
                        }
                        else if (lsttempCCR[i].SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            objCSPView.SemesterHKII = lsttempCCR[i].SemesterID;
                        }
                        else if (lsttempCCR[i].SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_ALL){
                            objCSPView.AllYear = lsttempCCR[i].SemesterID;
                        }

                        //kiem tra lop co khoa hanh kiem theo dot
                        objPD = lstPD.Where(p => p.PeriodDeclarationID == lsttempCCR[i].PeriodID).FirstOrDefault();
                        if (objPD != null)
                        {
                            objCSPView.DicPeriodOfSemester.Add(objPD.PeriodDeclarationID, objPD.PeriodDeclarationID);
                        }
                    }
                }

                
                lstGirdConductSePe.Add(objCSPView);
            }
            ViewData[ConfigConductRankingConstant.EDUCATIONLEVELID] = educationlevelID;
            return lstGirdConductSePe;
        }

        public PartialViewResult Search()
        {
            SetViewDataPermission("ConfigConductRanking", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin, new GlobalInfo().RoleID); 
            int? educationLevelID = SMAS.Business.Common.Utils.GetInt(Request["EducationLevelID"]);
            int? conductSPID = SMAS.Business.Common.Utils.GetInt(Request["ConductSPID"]);
            
            List<ConductSeOrPeViewModel> lstGirdConductSePe = _GetGirdConductSemesterOrPeriod(educationLevelID.Value, conductSPID.Value);
            ViewData[ConfigConductRankingConstant.LS_GIRD_CONDUCT_SEMESTER] = lstGirdConductSePe.OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderNumber).ToList();
            ViewData[ConfigConductRankingConstant.CHECK_LOAD_GIRD] = conductSPID;
            return PartialView("_List");
        }

        [ValidateAntiForgeryToken]
        public JsonResult InsertOrDelete(FormCollection frm)
        {
            #region Khai bao + lay dl
            int conductSPID = frm["HfConductID"] != null ? int.Parse(frm["HfConductID"]) : 0;
            int educationlevelID = (frm["HfEducationLevelID"] != null && frm["HfEducationLevelID"] != "") ? int.Parse(frm["HfEducationLevelID"]) : 0;
            IDictionary<string, object> dicSearch = new Dictionary<string, object>()
            {
                {"SchoolID", _globalInfo.SchoolID.Value},
                {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                {"EducationLevelID", educationlevelID},
                {"AppliedLevel", _globalInfo.AppliedLevel}
            };
            List<PeriodDeclaration> lstPD = new List<PeriodDeclaration>();
            PeriodDeclaration objPD = null;
            //neu lay theo dot cua hoc kỳ
            if (conductSPID != 0)
            {
                dicSearch.Add("SemesterID", conductSPID);
                dicSearch.Add("Semester", conductSPID);
                //lấy danh sách các đợt của học kỳ
                lstPD = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch).ToList();
                ViewData[ConfigConductRankingConstant.LS_PERIOD_SEMESTER] = lstPD;
            }

            List<ConductSeOrPeViewModel> lstGirdConductSePe = new List<ConductSeOrPeViewModel>();         

            //lấy danh sách lớp đang học của trường
            List<ClassProfile> lstCP = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSearch).ToList();

            //lấy danh sách lớp có khóa hạnh kiểm 
            List<ConfigConductRanking> lstConfigCR = ConfigConductRankingBusiness.GetAllConfigConductRanking(dicSearch).ToList();    
   
            //khoi tao list Insert & Delete
            List<ConfigConductRanking> lstInsertCCR = new List<ConfigConductRanking>();
            List<ConfigConductRanking> lstDeleteCCR = new List<ConfigConductRanking>();
            ConfigConductRanking objCCR = null;
            #endregion 

            //duyet danh sach lop
            foreach (var item in lstCP)
            {
                if (conductSPID == 0)
                {
                    #region xu ly khoa hanh kiem theo hoc ki
                    //kiem tra check o hoc ki 1
                    if ("on".Equals(frm["chkI_1_" + item.ClassProfileID]))
                    {
                        objCCR = new ConfigConductRanking();
                        objCCR.SchoolID = _globalInfo.SchoolID.Value;
                        objCCR.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objCCR.EducationLevelID = item.EducationLevelID;
                        objCCR.ClassID = item.ClassProfileID;
                        objCCR.SemesterID = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                        objCCR.LastDigitSchool = _globalInfo.SchoolID.Value % 100;
                        objCCR.CreateTime = DateTime.Now;
                        lstInsertCCR.Add(objCCR);
                    }
                    else
                    {
                        objCCR = new ConfigConductRanking();
                        objCCR.SchoolID = _globalInfo.SchoolID.Value;
                        objCCR.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objCCR.EducationLevelID = item.EducationLevelID;
                        objCCR.ClassID = item.ClassProfileID;
                        objCCR.SemesterID = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                        lstDeleteCCR.Add(objCCR);
                    }

                    //kiem tra check o hoc ki 2
                    if ("on".Equals(frm["chkII_2_" + item.ClassProfileID]))
                    {
                        objCCR = new ConfigConductRanking();
                        objCCR.SchoolID = _globalInfo.SchoolID.Value;
                        objCCR.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objCCR.EducationLevelID = item.EducationLevelID;
                        objCCR.ClassID = item.ClassProfileID;
                        objCCR.SemesterID = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                        objCCR.LastDigitSchool = _globalInfo.SchoolID.Value % 100;
                        objCCR.CreateTime = DateTime.Now;
                        lstInsertCCR.Add(objCCR);
                    }
                    else
                    {
                        objCCR = new ConfigConductRanking();
                        objCCR.SchoolID = _globalInfo.SchoolID.Value;
                        objCCR.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objCCR.EducationLevelID = item.EducationLevelID;
                        objCCR.ClassID = item.ClassProfileID;
                        objCCR.SemesterID = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                        lstDeleteCCR.Add(objCCR);
                    }

                    //kiem tra check o ca nam
                    if ("on".Equals(frm["chkAllYear_3_" + item.ClassProfileID]))
                    {
                        objCCR = new ConfigConductRanking();
                        objCCR.SchoolID = _globalInfo.SchoolID.Value;
                        objCCR.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objCCR.EducationLevelID = item.EducationLevelID;
                        objCCR.ClassID = item.ClassProfileID;
                        objCCR.SemesterID = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                        objCCR.LastDigitSchool = _globalInfo.SchoolID.Value % 100;
                        objCCR.CreateTime = DateTime.Now;
                        lstInsertCCR.Add(objCCR);
                    }
                    else
                    {
                        objCCR = new ConfigConductRanking();
                        objCCR.SchoolID = _globalInfo.SchoolID.Value;
                        objCCR.AcademicYearID = _globalInfo.AcademicYearID.Value;
                        objCCR.EducationLevelID = item.EducationLevelID;
                        objCCR.ClassID = item.ClassProfileID;
                        objCCR.SemesterID = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                        lstDeleteCCR.Add(objCCR);
                    }
                    #endregion

                }
                else
                {
                    #region xu ly khoa hanh kiem theo dot
                    foreach (var temp in lstPD)
                    {
                        if ("on".Equals(frm["chk_" + item.ClassProfileID + "_" + temp.PeriodDeclarationID]))
                        {
                            objCCR = new ConfigConductRanking();
                            objCCR.SchoolID = _globalInfo.SchoolID.Value;
                            objCCR.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            objCCR.EducationLevelID = item.EducationLevelID;
                            objCCR.ClassID = item.ClassProfileID;
                            objCCR.PeriodID = temp.PeriodDeclarationID;
                            objCCR.LastDigitSchool = _globalInfo.SchoolID.Value % 100;
                            objCCR.CreateTime = DateTime.Now;
                            lstInsertCCR.Add(objCCR);
                        }
                        else
                        {
                            objCCR = new ConfigConductRanking();
                            objCCR.SchoolID = _globalInfo.SchoolID.Value;
                            objCCR.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            objCCR.EducationLevelID = item.EducationLevelID;
                            objCCR.ClassID = item.ClassProfileID;
                            objCCR.PeriodID = temp.PeriodDeclarationID;
                            lstDeleteCCR.Add(objCCR);
                        }

                    }
                    #endregion
                }
            }

            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID},
                    {"conductSPID",conductSPID}
                };
            if (lstInsertCCR != null && lstInsertCCR.Count > 0)
            {                
                ConfigConductRankingBusiness.Insert(dic, lstInsertCCR);
            }

            if (lstDeleteCCR != null && lstDeleteCCR.Count > 0)
            {
                ConfigConductRankingBusiness.Delete(dic, lstDeleteCCR);
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }
    }
}
