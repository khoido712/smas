﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ConfigConductRankingArea.Models
{
    public class SearchViewModel
    {
        [DisplayName("Khối: ")]       
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ConfigConductRankingConstant.LS_EDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "OncbEducationLevelChange(this)")]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? cboEducationLevel { get; set; }

        [DisplayName("Hạnh kiểm: ")]    
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ConfigConductRankingConstant.LS_CONDUCFFORSEMESTER_PERIOD)]
        [AdditionalMetadata("OnChange", "OncbConductSemesterOrPeriodChange(this)")]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? cbConductSemesterOrPeriod { get; set; }
    }
}