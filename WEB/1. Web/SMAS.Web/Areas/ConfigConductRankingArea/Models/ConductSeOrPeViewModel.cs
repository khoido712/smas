﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ConfigConductRankingArea.Models
{
    public class ConductSeOrPeViewModel
    {
        public int ConfigConductRankingID { get; set;}

        public int ClassID { get; set; }

        public string ClassName { get; set; }

        public int SchoolID { get; set; }

        public int AcademicYearID { get; set; }

        public int EducationLevelID { get; set; }

        public int? SemesterHKI { get; set; }

        public int? SemesterHKII { get; set; }

        public int? AllYear { get; set; }

        public IDictionary<int, int> DicPeriodOfSemester { get; set; }

        public int? OrderNumber { get; set; }
    }
}