﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamRoomArea
{
    public class ExamRoomAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExamRoomArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExamRoomArea_default",
                "ExamRoomArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
