﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamRoomArea.Models
{
    public class ExamRoomViewModel
    {
        public long ExamRoomID { get; set; }

        [ResourceDisplayName("ExamSubject_Label_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamRoomConstants.LIST_EXAMINATIONS)]
        [AdditionalMetadata("Placeholder", "null")]
        public long ExaminationsID { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamRoomConstants.LIST_EXAM_GROUP)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_ExamGroupID")]
        [AdditionalMetadata("Placeholder", "null")]
        public long ExamGroupID { get; set; }

        [ResourceDisplayName("ExamRoom_Column_ExamRoomCode")]
        [UIHint("Textbox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamRoom_Error_ExamRoomCode")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ExamRoomCode { get; set; }

        [ResourceDisplayName("ExamRoom_Column_SeatNumber")]
        [UIHint("Textbox")]
        [RegularExpression(@"^\d+$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "OtherService_Price_Error")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamRoom_Error_SeatNumber")]
        [StringLength(3, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public int SeatNumber { get; set; }

        [ResourceDisplayName("ExamRoom_Column_Note")]
        [UIHint("Textbox")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Note { get; set; }

        [ResourceDisplayName("ExamRoom_Label_ExamRoomNumber")]
        [UIHint("Textbox")]
        [RegularExpression(@"^\d+$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "OtherService_Price_Error")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_ExamRoomNumber")]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public int ExamRoomNumber { get; set; }

        [ResourceDisplayName("ExamRoom_Label_PrefixExamRoomCode")]
        [UIHint("Textbox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_PrefixExamRoomCode")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string PrefixExamRoomCode { get; set; }

        public DateTime CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// Check check box xoa khi hien tren grid
        /// </summary>
        public bool CheckExist { get; set; }
    }
}