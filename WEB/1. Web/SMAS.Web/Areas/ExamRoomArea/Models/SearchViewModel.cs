﻿using Resources;
using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.ExamRoomArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ExamSubject_Label_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamRoomConstants.LIST_EXAMINATIONS)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamGroup(this)")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "NutritionalNorm_Validate_Require")]
        public long Examinations { get; set; }

        [ResourceDisplayName("ExamSubject_Label_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ExamRoomConstants.LIST_EXAM_GROUP)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadExamRoom(this)")]
        public long? ExamGroup { get; set; }
    }
}