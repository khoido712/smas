﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ExamRoomArea
{
    public class ExamRoomConstants
    {
        public const string IS_CURRENT_YEAR = "IsCurrentYear";
        public const string CURRENT_SEMESTER = "CurrentSemester";

        public const string LIST_EXAM_ROOM = "ListExamRoom";
        public const string LIST_EXAM_GROUP = "ListExamGroup";
        public const string LIST_EXAMINATIONS = "ListExaminations";
        public const string DATA_EXAMINATIONS = "DataExaminations";
        public const int PAGE_SIZE = 20;
    }
}