﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using System.Collections.Generic;
using SMAS.Web.Areas.ExamRoomArea.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.ExamRoomArea.Controllers
{
    public class ExamRoomController : BaseController
    {
        private readonly IExaminationsBusiness ExaminationsBusiness;
        private readonly IExamGroupBusiness ExamGroupBusiness;
        private readonly IExamRoomBusiness ExamRoomBusiness;
        private readonly IExamPupilBusiness ExamPupilBusiness;
        private readonly IExamBagBusiness ExamBagBusiness;
        private readonly IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness;
        private readonly IExamPupilViolateBusiness ExamPupilViolateBusiness;
        private readonly IExamSupervisoryViolateBusiness ExamSupervisoryViolateBusiness;
        private readonly IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness;

        public ExamRoomController(IExaminationsBusiness ExaminationsBusiness,
            IExamGroupBusiness ExamGroupBusiness,
            IExamRoomBusiness ExamRoomBusiness,
            IExamPupilBusiness ExamPupilBusiness,
            IExamBagBusiness ExamBagBusiness,
             IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness,
            IExamPupilViolateBusiness ExamPupilViolateBusiness,
            IExamSupervisoryViolateBusiness ExamSupervisoryViolateBusiness,
            IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness)
        {
            this.ExaminationsBusiness = ExaminationsBusiness;
            this.ExamGroupBusiness = ExamGroupBusiness;
            this.ExamRoomBusiness = ExamRoomBusiness;
            this.ExamPupilBusiness = ExamPupilBusiness;
            this.ExamBagBusiness = ExamBagBusiness;
            this.ExamSupervisoryAssignmentBusiness = ExamSupervisoryAssignmentBusiness;
            this.ExamPupilViolateBusiness = ExamPupilViolateBusiness;
            this.ExamSupervisoryViolateBusiness = ExamSupervisoryViolateBusiness;
            this.ExamPupilAbsenceBusiness = ExamPupilAbsenceBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region SetViewData

        public void SetViewData()
        {
            SetViewDataPermission("ExamRoomArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("AcademicYearID", _globalInfo.AcademicYearID.Value);
            search.Add("SchoolID", _globalInfo.SchoolID.Value);
            search.Add("AppliedLevel", _globalInfo.AppliedLevel);

            ViewData[ExamRoomConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            ViewData[ExamRoomConstants.CURRENT_SEMESTER] = _globalInfo.Semester;

            List<Examinations> listExam = ExaminationsBusiness.Search(search).OrderByDescending(o => o.CreateTime).ToList();
            ViewData[ExamRoomConstants.LIST_EXAMINATIONS] = new SelectList(listExam, "ExaminationsID", "ExaminationsName");
            if (listExam != null && listExam.Count > 0)
            {
                Examinations exam = listExam.FirstOrDefault();
                List<ExamGroup> listGroup = GetExamGroupFromExaminations(exam.ExaminationsID).ToList();
                ViewData[ExamRoomConstants.LIST_EXAM_GROUP] = new SelectList(listGroup, "ExamGroupID", "ExamGroupName");
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("Examinations", exam.ExaminationsID);
                ViewData[ExamRoomConstants.LIST_EXAM_ROOM] = this._Search(dic).ToList();
            }
            else
            {
                ViewData[ExamRoomConstants.LIST_EXAM_GROUP] = new SelectList(new string[] { });
                ViewData[ExamRoomConstants.LIST_EXAM_ROOM] = new List<ExamRoomViewModel>();
            }
        }

        #endregion SetViewData

        #region LoadCombobox

        private IEnumerable<ExamGroup> GetExamGroupFromExaminations(long ExaminationsID)
        {
            GlobalInfo global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"ExaminationsID", ExaminationsID},
                };

            IEnumerable<ExamGroup> listGroup = ExamGroupBusiness.Search(dic).OrderBy(o => o.ExamGroupCode.ToUpper()).ToList();
            return listGroup;
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadExamGroup(long? ExaminationsID)
        {
            if (ExaminationsID.HasValue)
            {
                IEnumerable<ExamGroup> listGroup = GetExamGroupFromExaminations(ExaminationsID.Value);
                return Json(new SelectList(listGroup, "ExamGroupID", "ExamGroupName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        #endregion LoadCombobox

        #region Search

        
        public PartialViewResult Search(SearchViewModel form)
        {
            ViewData[ExamRoomConstants.IS_CURRENT_YEAR] = _globalInfo.IsCurrentYear;
            IDictionary<string, object> search = new Dictionary<string, object>();
            search.Add("Examinations", form.Examinations);
            search.Add("ExamGroupID", form.ExamGroup.HasValue ? form.ExamGroup.Value : 0);

            IEnumerable<ExamRoomViewModel> list = this._Search(search);
            ViewData[ExamRoomConstants.LIST_EXAM_ROOM] = list;

            return PartialView("_List");
        }

        private IEnumerable<ExamRoomViewModel> _Search(IDictionary<string, object> search)
        {
            SetViewDataPermission("ExamRoomArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);

            long examinatiosID = SMAS.Business.Common.Utils.GetLong(search, "Examinations");
            int schoolId = GlobalInfo.getInstance().SchoolID.Value;
            int partitionId = UtilsBusiness.GetPartionId(schoolId, 100);
            // Danh sach thi sinh theo phong thi
            List<ExamPupil> exampupilList = ExamPupilBusiness.All.Where(p => p.ExaminationsID == examinatiosID && p.LastDigitSchoolID == partitionId).ToList();
            //Danh ma tui thi
            List<ExamBag> examBagList = ExamBagBusiness.All.Where(p => p.ExaminationsID == examinatiosID).ToList();
            // Phan cong giam thi
            List<ExamSupervisoryAssignment> examSupervisoryAssignmentList = ExamSupervisoryAssignmentBusiness.All.Where(p => p.ExaminationsID == examinatiosID).ToList();
            //thi sinh vi pham quy che
            List<ExamPupilViolate> examPupilViolateList = ExamPupilViolateBusiness.All.Where(p => p.ExaminationsID == examinatiosID).ToList();
            //giam thi vi pham quy che thi
            List<ExamSupervisoryViolate> examSupervisoryViolateList = ExamSupervisoryViolateBusiness.All.Where(p => p.ExaminationsID == examinatiosID).ToList();
            //thi sinh vang thi
            List<ExamPupilAbsence> exampupilAbsenceList = ExamPupilAbsenceBusiness.All.Where(p => p.ExaminationsID == examinatiosID).ToList();
            // kiem tra ki thi
            ExaminationsBO ExaminationsObj = ExaminationsBusiness.GetExaminationsByID(examinatiosID);
            ViewData[ExamRoomConstants.DATA_EXAMINATIONS] = ExaminationsObj;
            List<ExamRoom> listExamRoom = ExamRoomBusiness.Search(search).ToList();
            List<ExamRoomViewModel> listResult = new List<ExamRoomViewModel>();
            ExamRoomViewModel examroomObj = null;
            ExamRoom examRoomTmp = null;
            bool currentYear = _globalInfo.IsCurrentYear;
            for (int i = 0; i < listExamRoom.Count; i++)
            {
                examRoomTmp = listExamRoom[i];
                examroomObj = new ExamRoomViewModel();
                examroomObj.ExamRoomID = examRoomTmp.ExamRoomID;
                examroomObj.ExamRoomCode = examRoomTmp.ExamRoomCode;
                examroomObj.ExamGroupID = examRoomTmp.ExamGroupID;
                examroomObj.ExaminationsID = examRoomTmp.ExaminationsID;
                examroomObj.SeatNumber = examRoomTmp.SeatNumber;
                examroomObj.Note = examRoomTmp.Note;
                examroomObj.CreateTime = examRoomTmp.CreateTime;
                examroomObj.UpdateTime = examRoomTmp.UpdatTime;
                //check hien thi button xoa
                if (exampupilList.Exists(p => p.ExamRoomID == examRoomTmp.ExamRoomID)
                    || examBagList.Exists(p => p.ExamRoomID == examRoomTmp.ExamRoomID)
                    || examSupervisoryAssignmentList.Exists(p => p.ExamRoomID == examRoomTmp.ExamRoomID)
                    || examPupilViolateList.Exists(p => p.ExamRoomID == examRoomTmp.ExamRoomID)
                    || examSupervisoryViolateList.Exists(p => p.ExamRoomID == examRoomTmp.ExamRoomID)
                    || exampupilAbsenceList.Exists(p => p.ExamRoomID == examRoomTmp.ExamRoomID)
                    || (ExaminationsObj != null && (ExaminationsObj.MarkClosing == true && ExaminationsObj.MarkInput == true))
                    || !currentYear
                    )
                {
                    examroomObj.CheckExist = true;
                }
                else
                {
                    examroomObj.CheckExist = false;
                }
                listResult.Add(examroomObj);
            }
            return listResult.OrderBy(p=>p.ExamRoomCode).ToList();
        }

        #endregion Search

        #region Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(FormCollection form)
        {
            if (GetMenupermission("ExamRoomArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                return Json(new JsonMessage(Res.Get("Validate_Permission_Teacher"), "error"));
            }
            long examinationsID = long.Parse(form["HiddenExaminationsID"]);
            long examGroupID = long.Parse(form["ExamGroupID"]);
            string examRoomCode = form["ExamRoomCode"].ToString();
            if (CheckExamRoomCodeExist(examRoomCode, examinationsID, examGroupID))
            {
                return Json(new JsonMessage(Res.Get("Validate_Exist_Room_Code"), "error"));
            }
            ExamRoom room = new ExamRoom();
            room.ExaminationsID = examinationsID;
            room.ExamGroupID = examGroupID;
            room.ExamRoomCode = examRoomCode;
            room.SeatNumber = int.Parse(form["SeatNumber"]);
            if (form["Note"] != null)
            {
                room.Note = form["Note"].ToString();
            }
            ExamRoomBusiness.InsertExamRoom(room);
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        #endregion Create

        #region FastCreate

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult FastCreate(FormCollection form)
        {
            if (GetMenupermission("ExamRoomArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            long examinationsID = long.Parse(form["HiddenExaminationsID"]);
            long examGroupID = long.Parse(form["ExamGroupID"]);
            int seatNumber = int.Parse(form["SeatNumber"]);
            int examRoomNumber = int.Parse(form["ExamRoomNumber"]);
            string prefixExamRoomCode = form["PrefixExamRoomCode"].ToString();
            //Chieu dai danh so - Mac dinh luon la 2
            int numLength = 2;

            List<ExamRoom> listExamRoom = new List<ExamRoom>();
            ExamRoom room = null;
            //Danh sach phong thi theo ky thi va nhom thi
            List<ExamRoom> examRoomListSource = ExamRoomBusiness.GetListExamRoom(examinationsID, examGroupID);
            for (int i = 1; i <= examRoomNumber; i++)
            {
                string examRoomCode = prefixExamRoomCode + i.ToString().PadLeft(numLength, '0');
               
                if (examRoomListSource.Exists(p => p.ExamRoomCode.ToLower() == examRoomCode.ToLower()))
                {
                    continue;
                }
                room = new ExamRoom();
                room.ExamRoomID = ExamRoomBusiness.GetNextSeq<long>();
                room.ExaminationsID = examinationsID;
                room.ExamGroupID = examGroupID;
                room.ExamRoomCode = examRoomCode;
                room.SeatNumber = seatNumber;
                room.Note = string.Empty;
                room.CreateTime = DateTime.Now;
                listExamRoom.Add(room);
            }

            if (listExamRoom != null && listExamRoom.Count > 0)
            {
                ExamRoomBusiness.InsertListExamRoom(listExamRoom);
            }
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        #endregion FastCreate

        #region Update

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Update(FormCollection form)
        {
            if (GetMenupermission("ExamRoomArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            long examinationsID = long.Parse(form["HiddenExaminationsID"]);
            long examGroupID = form["ExamGroupID"] != null ? long.Parse(form["ExamGroupID"]) : long.Parse(form["HiddenExamGroupID"]);
            string examRoomCode = form["ExamRoomCode"].ToString();
            long examRoomID = long.Parse(form["HiddenExamRoomID"]);
            if (CheckExamRoomCodeExistUpdate(examRoomCode, examinationsID, examGroupID, examRoomID))
            {
                return Json(new JsonMessage(Res.Get("Validate_Exist_Room_Code"), "error"));
            }
            ExamRoom room = new ExamRoom();
            room.ExamRoomID = examRoomID;
            room.ExaminationsID = examinationsID;
            room.ExamGroupID = examGroupID;
            room.ExamRoomCode = examRoomCode;
            room.SeatNumber = form["SeatNumber"] != null ? int.Parse(form["SeatNumber"]) : int.Parse(form["HiddenSeatNumber"]);
            if (form["Note"] != null)
            {
                room.Note = form["Note"].ToString();
            }
            ExamRoomBusiness.UpdateExamRoom(room);
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }


        public PartialViewResult GetDataUpdate(long? ExamRoomID)
        {
            SetViewDataPermission("ExamRoomArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            if (ExamRoomID.HasValue)
            {
                ExamRoom exam = ExamRoomBusiness.Find(ExamRoomID);
                ExamRoomViewModel examRoomViewModel = new ExamRoomViewModel();
                examRoomViewModel.ExaminationsID = exam.ExaminationsID;
                examRoomViewModel.ExamGroupID = exam.ExamGroupID;
                examRoomViewModel.ExamRoomCode = exam.ExamRoomCode;
                examRoomViewModel.SeatNumber = exam.SeatNumber;
                examRoomViewModel.Note = exam.Note;
            }
            return PartialView("_Edit");
        }

        #endregion Update

        #region Delete

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCheck(string stringExamRoom)
        {
            if (GetMenupermission("ExamRoomArea", _globalInfo.UserAccountID, _globalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            string[] listDelete = stringExamRoom.Split(',');
            List<long> listExamRoom = new List<long>();
            for (int i = 0; i < listDelete.Length - 1; i++)
            {
                listExamRoom.Add(long.Parse(listDelete[i]));
            }
            ExamRoomBusiness.DeleteExamRoom(listExamRoom);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        #endregion Delete

        /// <summary>
        /// Kiem tra RoomCode da ton tai chua
        /// </summary>
        /// <param name="ExamRoomCode"></param>
        /// <param name="ExaminationsID"></param>
        /// <param name="ExamGroupID"></param>
        /// <returns></returns>
        private bool CheckExamRoomCodeExist(string ExamRoomCode, long ExaminationsID, long ExamGroupID)
        {
            return ExamRoomBusiness.CheckExamRoomByExamCode(ExamRoomCode, ExaminationsID, ExamGroupID);
        }

        private bool CheckExamRoomCodeExistUpdate(string ExamRoomCode, long ExaminationsID, long ExamGroupID, long examRoomID)
        {
            return ExamRoomBusiness.CheckExamRoomByExamCodeUpdate(ExamRoomCode, ExaminationsID, ExamGroupID, examRoomID);
        }

        //Kiem tra du lieu da duoc su dung hay chua
        public bool CheckContraintDataDelete(long examRoomID, long examinatiosID)
        {
            // Danh sach thi sinh theo phong thi
            int schoolId = GlobalInfo.getInstance().SchoolID.Value;
            int partitionId = UtilsBusiness.GetPartionId(schoolId, 100);
            ExamPupil exampupilObj = ExamPupilBusiness.All.FirstOrDefault(p => p.ExamRoomID == examRoomID && p.ExaminationsID == examinatiosID && p.LastDigitSchoolID == partitionId);
            if (exampupilObj != null)
            {
                return true;
            }
            //Danh ma tui thi
            ExamBag examBagObj = ExamBagBusiness.All.FirstOrDefault(p => p.ExamRoomID == examRoomID && p.ExaminationsID == examinatiosID);
            if (examBagObj != null)
            {
                return true;
            }
            // Phan cong giam thi
            ExamSupervisoryAssignment examSupervisoryAssignmentObm = ExamSupervisoryAssignmentBusiness.All.FirstOrDefault(p => p.ExamRoomID == examRoomID && p.ExaminationsID == examinatiosID);
            if (examSupervisoryAssignmentObm != null)
            {
                return true;
            }
            //thi sinh vi pham quy che
            ExamPupilViolate examPupilViolate = ExamPupilViolateBusiness.All.FirstOrDefault(p => p.ExamRoomID == examRoomID && p.ExaminationsID == examinatiosID);
            if (examPupilViolate != null)
            {
                return true;
            }
            //giam thi vi pham quy che thi
            ExamSupervisoryViolate examSupervisoryViolateObj = ExamSupervisoryViolateBusiness.All.FirstOrDefault(p => p.ExamRoomID == examRoomID && p.ExaminationsID == examinatiosID);
            if (examSupervisoryViolateObj != null)
            {
                return true;
            }
            //thi sinh vang thi
            ExamPupilAbsence exampupilAbsenceObj = ExamPupilAbsenceBusiness.All.FirstOrDefault(p => p.ExamRoomID == examRoomID && p.ExaminationsID == examinatiosID);
            if (exampupilAbsenceObj != null)
            {
                return true;
            }
            return false;
        }
    }
}
