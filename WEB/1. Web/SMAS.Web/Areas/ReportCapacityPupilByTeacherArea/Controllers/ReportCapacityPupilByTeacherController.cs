﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.ReportCapacityPupilByTeacherArea.Models;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.ReportCapacityPupilByTeacherArea.Controllers
{
    public class ReportCapacityPupilByTeacherController : BaseController
    {

        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IReportCapacityPupilByTeacherBusiness ReportCapacityPupilByTeacherBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        public ReportCapacityPupilByTeacherController(ISchoolSubjectBusiness SchoolSubjectBusiness,
            ITeachingAssignmentBusiness TeachingAssignmentBusiness,
            IEmployeeBusiness EmployeeBusiness,
            IReportCapacityPupilByTeacherBusiness ReportCapacityPupilByTeacherBusiness,
            ISubjectCatBusiness SubjectCatBusiness)
        {
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ReportCapacityPupilByTeacherBusiness = ReportCapacityPupilByTeacherBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
        }

        //
        // GET: /ReportCapacityPupilByTeacherArea/ReportCapacityPupilByTeacher/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        public ActionResult _Index()
        {
            SetViewData();
            return PartialView();
        }

        public void SetViewData()
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            //Danh sách học kỳ
            if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                ViewData[ReportCapacityPupilByTeacherConstants.LS_SEMESTER] = CommonList.Semester();
                int defaultSemester = (_globalInfo.Semester == 0) ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : _globalInfo.Semester.GetValueOrDefault();
                ViewData[ReportCapacityPupilByTeacherConstants.DF_SEMESTER] = defaultSemester;
            }
            else if (_globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || _globalInfo.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                 ViewData[ReportCapacityPupilByTeacherConstants.LS_SEMESTER] = CommonList.SemesterAndAll();
                 int defaultSemester = (_globalInfo.Semester == 0) ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : _globalInfo.Semester.GetValueOrDefault();
                ViewData[ReportCapacityPupilByTeacherConstants.DF_SEMESTER] = defaultSemester;
            }
            //Danh sách môn học
            //Danh sách môn học
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["SchoolID"] = _globalInfo.SchoolID;
            List<SchoolSubject> iquSchoolSubject = SchoolSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
            var listTemp = iquSchoolSubject.Select(s => new { s.SubjectCat.SubjectName, s.SubjectCat.SubjectCatID }).Distinct().ToList();
            List<SelectListItem> lstSubjectCat = new List<SelectListItem>();
            if (listTemp != null && listTemp.Count > 0)
            {
                foreach (var subjectCat in listTemp)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = subjectCat.SubjectName;
                    item.Value = subjectCat.SubjectCatID.ToString();
                    lstSubjectCat.Add(item);
                }
            }
            ViewData[ReportCapacityPupilByTeacherConstants.LS_SUBJECT] = lstSubjectCat;
            //Grid
            ViewData[ReportCapacityPupilByTeacherConstants.LS_TEACHER] = new List<ReportCapacityPupilByTeacherViewModel>();
        }

        public ActionResult Search(SearchViewModel frm)
        {
            //int ASSIGNED = 2; //Đã phân công giảng dạy
            int Semester = frm.Semester;
            int SubjectID = frm.SubjectID;
            ViewData[ReportCapacityPupilByTeacherConstants.LS_TEACHER] = new List<ReportCapacityPupilByTeacherViewModel>();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                dic["Semester"] = Semester;
            }
            else //Neu ca nam thi lay HK2
            {
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            }
            dic["SubjectID"] = SubjectID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            //dic["Type"] = ASSIGNED;
            IQueryable<TeachingAssignment> iquTeachingAssignment = TeachingAssignmentBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic);
            List<TeachingAssignment> lst;
            List<ReportCapacityPupilByTeacherViewModel> lstTeacher = new List<ReportCapacityPupilByTeacherViewModel>();
            if (iquTeachingAssignment.Count() > 0)
            {
                lst = iquTeachingAssignment.OrderBy(o => o.Employee.Name).ThenBy(o => o.Employee.FullName).ToList();
                List<int> listSubjectId = lst.Select(p => p.SubjectID).Distinct().ToList();
                List<int> listClassId = lst.Where(p => p.ClassID.HasValue).Select(p => p.ClassID.Value).Distinct().ToList();
                List<int> listClassNotVNEN = TeachingAssignmentBusiness.GetListClassIdNotVNEN(SubjectID, listClassId);
                lst = lst.Where(p => p.ClassID.HasValue && listClassNotVNEN.Contains(p.ClassID.Value)).ToList();
                foreach (TeachingAssignment teacherBO in lst)
                {
                    ReportCapacityPupilByTeacherViewModel teacher = new ReportCapacityPupilByTeacherViewModel();
                    teacher.EmployeeID = teacherBO.TeacherID;
                    teacher.EmployeeName = teacherBO.Employee != null ? teacherBO.Employee.FullName : "";
                    teacher.BirthDate = teacherBO.Employee != null ? teacherBO.Employee.BirthDate : null;
                    if (lstTeacher.Where(o => o.EmployeeID == teacherBO.TeacherID).Count() == 0)
                    {
                        lstTeacher.Add(teacher);
                    }
                }
                ViewData[ReportCapacityPupilByTeacherConstants.LS_TEACHER] = lstTeacher;
            }
            ReportCapacityPupilByTeacherViewModel reportViewModel = new ReportCapacityPupilByTeacherViewModel();
            reportViewModel.Semester = Semester;
            reportViewModel.SubjectID = SubjectID;
            return PartialView("_ListTeacher", reportViewModel);
        }

        [HttpPost]
        public FileResult ExportExcel(FormCollection col, ReportCapacityPupilByTeacherViewModel reportViewModel)
        {
            int Semester = reportViewModel.Semester;
            int SubjectID = reportViewModel.SubjectID;
            var lstTeacher = col["chkSatus"];
            string[] listid = lstTeacher.Split(',');
            List<object> abc = new List<object>();
            List<int> lstTeacherID = new List<int>();
            foreach (var id in listid)
            {
                int ID = int.Parse(id);
                lstTeacherID.Add(ID);
                //lay thong tin cua doi tuong thong qua ID
            }

            Stream excel = ReportCapacityPupilByTeacherBusiness.CreateReportCapacityPupilByTeacher(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, Semester, SubjectID, lstTeacherID);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            //reportName GV_[SchoolLevel]_BCHLMTheoGV_[Semester]_[Subject]
            string ReportName = "GV_[SchoolLevel]_BCHLMTheoGV_[Semester]_[Subject].xls";
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(_globalInfo.AppliedLevel.Value);
            string semester = ReportUtils.ConvertSemesterForReportName(Semester);
            string subjectName = SubjectCatBusiness.Find(SubjectID).DisplayName;
            ReportName = ReportName.Replace("[SchoolLevel]", schoolLevel);
            ReportName = ReportName.Replace("[Semester]", semester);
            ReportName = ReportName.Replace("[Subject]", subjectName);
            ReportName = ReportUtils.RemoveSpecialCharacters(ReportName);
            result.FileDownloadName = ReportName;

            return result;
        }
    }
}
