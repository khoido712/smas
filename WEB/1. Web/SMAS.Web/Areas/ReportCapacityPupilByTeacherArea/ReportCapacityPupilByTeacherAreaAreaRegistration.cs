﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportCapacityPupilByTeacherArea
{
    public class ReportCapacityPupilByTeacherAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportCapacityPupilByTeacherArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportCapacityPupilByTeacherArea_default",
                "ReportCapacityPupilByTeacherArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
