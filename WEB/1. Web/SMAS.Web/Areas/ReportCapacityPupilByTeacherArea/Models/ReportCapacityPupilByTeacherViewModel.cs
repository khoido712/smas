﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ReportCapacityPupilByTeacherArea.Models
{
    public class ReportCapacityPupilByTeacherViewModel
    {
        #region Grid

        public int EmployeeID { get; set; }
        [ResourceDisplayName("Employee_Label_FullName")]
        public string EmployeeName { get; set; }
        [ResourceDisplayName("Employee_Label_BirthDate")]
        public DateTime? BirthDate { get; set; }
        
        #endregion
        public int Semester { get; set; }
        public int SubjectID { get; set; }
    }
}