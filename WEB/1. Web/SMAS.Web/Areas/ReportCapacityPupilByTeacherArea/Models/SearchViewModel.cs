﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportCapacityPupilByTeacherArea.Models
{
    public class SearchViewModel
    {
        public int Semester { get; set; }
        public int SubjectID { get; set; }
    }
}