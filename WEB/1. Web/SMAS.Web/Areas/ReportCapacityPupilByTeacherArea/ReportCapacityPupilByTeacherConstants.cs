﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportCapacityPupilByTeacherArea
{
    public class ReportCapacityPupilByTeacherConstants
    {
        public const string LS_SEMESTER = "lstSemester";
        public const string DF_SEMESTER = "defaultSemester";
        public const string LS_SUBJECT = "lstSubject";
        public const string LS_TEACHER = "lstTeacher";
    }
}