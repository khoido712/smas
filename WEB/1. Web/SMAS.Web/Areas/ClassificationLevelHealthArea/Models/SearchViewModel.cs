/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ClassificationLevelHealthArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ClassificationLevelHealth_Label_TypeConfig")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassificationLevelHealthConstants.List_TypeConfig)]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("OnChange", "search()")]
        public System.Nullable<System.Int32> TypeConfigID { get; set; }
    }
}
