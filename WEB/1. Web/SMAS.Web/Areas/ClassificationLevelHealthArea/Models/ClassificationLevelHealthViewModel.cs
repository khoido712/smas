﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ClassificationLevelHealthArea.Models
{
    public class ClassificationLevelHealthViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 ClassificationLevelHealthID { get; set; }

        [ResourceDisplayName("ClassificationLevelHealth_Label_TypeConfig")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassificationLevelHealthConstants.List_TypeConfig)]
        [AdditionalMetadata("OnChange", "AjaxLoadEvaluationConfig(this, this.options[this.selectedIndex].value);")]
        [AdditionalMetadata("Placeholder", "null")]   
        public System.Int32 TypeConfigID { get; set; }

        [ResourceDisplayName("ClassificationLevelHealth_Label_Level")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        //[IntegerValidation(ErrorMessage = "Không phải định dạng số")]
        [RegularExpression(@"^-?\d*\.{0,0}\d+$", ErrorMessageResourceName = "Common_Validate_NotIsNumber", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(2, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public int Level { get; set; }

        [ResourceDisplayName("ClassificationLevelHealth_Label_SymbolConfigName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassificationLevelHealthConstants.List_SymbolConfig)]  
        public System.Int32 SymbolConfigID { get; set; }
        [ResourceDisplayName("ClassificationLevelHealth_Label_EvaluationConfigName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassificationLevelHealthConstants.List_EvaluationConfig)]  
        public System.Int32 EvaluationConfigID { get; set; }
      
        [ScaffoldColumn(false)]
		public System.DateTime CreatedDate { get; set; }
        [ScaffoldColumn(false)]
		public System.Boolean IsActive { get; set; }
        [ScaffoldColumn(false)]
		public System.Nullable<System.DateTime> ModifiedDate { get; set; }
        [ScaffoldColumn(false)]
        [ResourceDisplayName("ClassificationLevelHealth_Label_SymbolConfigName")]	
        public System.String SymbolConfigName { get; set; }
        [ScaffoldColumn(false)]
        [ResourceDisplayName("ClassificationLevelHealth_Label_EvaluationConfigName")]	
        public System.String EvaluationConfigName { get; set; }

        [ScaffoldColumn(false)]
        public bool? DeleteAble { get; set; }    
    }
}


