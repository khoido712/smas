﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ClassificationLevelHealthArea
{
    public class ClassificationLevelHealthAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ClassificationLevelHealthArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ClassificationLevelHealthArea_default",
                "ClassificationLevelHealthArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
