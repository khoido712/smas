﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.ClassificationLevelHealthArea.Models;
using SMAS.Web.Areas.ClassificationLevelHealthArea;
using System.Transactions;
namespace SMAS.Web.Areas.ClassificationLevelHealthArea.Controllers
{
    public class ClassificationLevelHealthController : BaseController
    {
        private readonly IClassificationLevelHealthBusiness ClassificationLevelHealthBusiness;
        private readonly ITypeConfigBusiness TypeConfigBusiness;
        private readonly ISymbolConfigBusiness SymbolConfigBusiness;
        private readonly IEvaluationConfigBusiness EvaluationConfigBusiness;
        private readonly IClassificationCriteriaDetailBusiness ClassificationCriteriaDetailBusiness;
        public ClassificationLevelHealthController(IClassificationLevelHealthBusiness classificationlevelhealthBusiness, ITypeConfigBusiness typeConfigBusiness,
            ISymbolConfigBusiness symbolConfigBusiness, IEvaluationConfigBusiness evaluationConfigBusiness, IClassificationCriteriaDetailBusiness classificationCriteriaDetailBusiness)
        {
            this.ClassificationLevelHealthBusiness = classificationlevelhealthBusiness;
            this.TypeConfigBusiness = typeConfigBusiness;
            this.SymbolConfigBusiness = symbolConfigBusiness;
            this.EvaluationConfigBusiness = evaluationConfigBusiness;
            this.ClassificationCriteriaDetailBusiness = classificationCriteriaDetailBusiness;
        }

        //
        // GET: /ClassificationLevelHealth/

        public ActionResult Index()
        {
            //Đưa dữ liệu ra combobox TypeConfig
            IDictionary<string, object> TypeConfigSearchInfo = new Dictionary<string, object>();
            TypeConfigSearchInfo["IsActive"] = true;
            List<TypeConfig> LstTypeConfig = TypeConfigBusiness.Search(TypeConfigSearchInfo).ToList();
            ViewData[ClassificationLevelHealthConstants.List_TypeConfig] = new SelectList(LstTypeConfig, "TypeConfigID", "TypeConfigName");
            //Đưa dữ liệu ra combobox SymbolConfig
            IDictionary<string, object> SymbolConfigSearchInfo = new Dictionary<string, object>();
            SymbolConfigSearchInfo["IsActive"] = true;
            List<SymbolConfig> LstSymbolConfig = SymbolConfigBusiness.Search(SymbolConfigSearchInfo).ToList();
            ViewData[ClassificationLevelHealthConstants.List_SymbolConfig] = new SelectList(LstSymbolConfig, "SymbolConfigID", "SymbolConfigName");

            //Đưa dữ liệu ra combobox EvaluationConfig = rỗng
            ViewData[ClassificationLevelHealthConstants.List_EvaluationConfig] = null;

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //Get view data here

            IEnumerable<ClassificationLevelHealthViewModel> lst = this._Search(SearchInfo);
            ViewData[ClassificationLevelHealthConstants.LIST_CLASSIFICATIONLEVELHEALTH] = lst;
            return View();
        }

        //
        // GET: /ClassificationLevelHealth/Search


        public ActionResult Search(int? TypeConfigID)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["TypeConfigID"] = TypeConfigID;

            IEnumerable<ClassificationLevelHealthViewModel> lst = this._Search(SearchInfo);
            ViewData[ClassificationLevelHealthConstants.LIST_CLASSIFICATIONLEVELHEALTH] = lst;
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create()
        {
            ClassificationLevelHealth classificationlevelhealth = new ClassificationLevelHealth();
            TryUpdateModel(classificationlevelhealth);
            Utils.Utils.TrimObject(classificationlevelhealth);            
            classificationlevelhealth.IsActive = true;
            this.ClassificationLevelHealthBusiness.Insert(classificationlevelhealth);
            this.ClassificationLevelHealthBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int ClassificationLevelHealthID, FormCollection col)
        {
            //CheckPermissionForAction()
            if (ModelState.IsValid)
            {

                ClassificationLevelHealth classificationlevelhealth = this.ClassificationLevelHealthBusiness.Find(ClassificationLevelHealthID);
                //ClassificationLevelHealthViewModel classificationLevelHealthViewModel = new ClassificationLevelHealthViewModel();
                //TryUpdateModel(classificationLevelHealthViewModel);

                //TryUpdateModel(classificationlevelhealth);
               // Utils.Utils.BindTo(classificationLevelHealthViewModel,classificationlevelhealth);
                //Utils.Utils.TrimObject(classificationlevelhealth);
                //classificationlevelhealth.SymbolConfigID = classificationLevelHealthViewModel.SymbolConfigID;
                //classificationlevelhealth.EvaluationConfigID = classificationLevelHealthViewModel.EvaluationConfigID;
                
                classificationlevelhealth.SymbolConfigID = Convert.ToInt32(col["SymbolConfigID"]);
                classificationlevelhealth.EvaluationConfigID = Convert.ToInt32(col["EvaluationConfigID"]);
                this.ClassificationLevelHealthBusiness.Update(classificationlevelhealth);
                this.ClassificationLevelHealthBusiness.Save();

                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
            string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            return Json(new JsonMessage(jsonErrList, "error"));
        }
        public PartialViewResult _Edit(int id)
        {
            return PartialView("_Edit", PrepareEdit(id));
        }
        private ClassificationLevelHealthViewModel PrepareEdit(int id)
        {
            ClassificationLevelHealth ClassificationLevelHealth = this.ClassificationLevelHealthBusiness.Find(id);

            //Đưa dữ liệu ra combobox TypeConfig
            IDictionary<string, object> TypeConfigSearchInfo = new Dictionary<string, object>();
            TypeConfigSearchInfo["IsActive"] = true;
            List<TypeConfig> LstTypeConfig = TypeConfigBusiness.Search(TypeConfigSearchInfo).ToList();
            ViewData[ClassificationLevelHealthConstants.List_TypeConfig] = new SelectList(LstTypeConfig, "TypeConfigID", "TypeConfigName");
            //Đưa dữ liệu ra combobox SymbolConfig
            IDictionary<string, object> SymbolConfigSearchInfo = new Dictionary<string, object>();
            SymbolConfigSearchInfo["IsActive"] = true;
            List<SymbolConfig> LstSymbolConfig = SymbolConfigBusiness.Search(SymbolConfigSearchInfo).ToList();
            ViewData[ClassificationLevelHealthConstants.List_SymbolConfig] = new SelectList(LstSymbolConfig, "SymbolConfigID", "SymbolConfigName");

            //Đưa dữ liệu ra combobox EvaluationConfig
            IDictionary<string, object> EvaluationConfigInfo = new Dictionary<string, object>();
            EvaluationConfigInfo["TypeConfigID"] = ClassificationLevelHealth.TypeConfigID;
            EvaluationConfigInfo["IsActive"] = true;
            List<EvaluationConfig> ListEvaluationConfig = EvaluationConfigBusiness.Search(EvaluationConfigInfo).ToList();
            ViewData[ClassificationLevelHealthConstants.List_EvaluationConfig] = new SelectList(ListEvaluationConfig, "EvaluationConfigID", "EvaluationConfigName");

            ClassificationLevelHealthViewModel frm = new ClassificationLevelHealthViewModel();
            Utils.Utils.BindTo(ClassificationLevelHealth, frm);
            return frm;
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            var checkContrains = this.ClassificationCriteriaDetailBusiness.All.Where(o=>o.ClassificationLevelHealthID == id ).Select(o=>o.ClassificationLevelHealthID);
            if (checkContrains.Count() > 0)
            {
                return Json(new JsonMessage(Res.Get("Common_Label_NotDeleteSuccessMessage")));
            }
            this.ClassificationLevelHealthBusiness.Delete(id);
            this.ClassificationLevelHealthBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ClassificationLevelHealthViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ClassificationLevelHealth> query = this.ClassificationLevelHealthBusiness.Search(SearchInfo);
            IQueryable<ClassificationLevelHealthViewModel> lst = query.Select(o => new ClassificationLevelHealthViewModel
            {
                ClassificationLevelHealthID = o.ClassificationLevelHealthID,
                Level = (o.Level.HasValue? o.Level.Value:0),                
                SymbolConfigID = o.SymbolConfigID,
                TypeConfigID = o.TypeConfigID,
                EvaluationConfigID = o.EvaluationConfigID,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate,
                SymbolConfigName = o.SymbolConfig.SymbolConfigName,
                EvaluationConfigName = o.EvaluationConfig.EvaluationConfigName

            });
            IEnumerable<ClassificationCriteriaDetail> classificationCriteria = ClassificationCriteriaDetailBusiness.SearchBySchool(new Dictionary<string, object>() { { "IsActive", true } });
            List<ClassificationLevelHealthViewModel> list = new List<ClassificationLevelHealthViewModel>();
            foreach (ClassificationLevelHealthViewModel obj in lst)
            {
                var listContraint = classificationCriteria.Where(o => o.ClassificationLevelHealthID == obj.ClassificationLevelHealthID).FirstOrDefault();
                if (listContraint != null)
                {
                    obj.DeleteAble = false;
                }
                else
                {
                    obj.DeleteAble = true;
                }
                list.Add(obj);
            }

            return list.ToList();
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadEvaluationConfig(int? TypeConfigID)
        {
            IEnumerable<EvaluationConfig> lst = new List<EvaluationConfig>();
            if (TypeConfigID.ToString() != "")
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["TypeConfigID"] = TypeConfigID;
                dic["IsActive"] = true;
                lst = this.EvaluationConfigBusiness.Search(dic);
            }
            return Json(new SelectList(lst, "EvaluationConfigID", "EvaluationConfigName"));
        }
    }
}





