/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ClassificationLevelHealthArea
{
    public class ClassificationLevelHealthConstants
    {
        public const string LIST_CLASSIFICATIONLEVELHEALTH = "listClassificationLevelHealth";
        public const string List_TypeConfig = "ListTypeConfig";
        public const string List_SymbolConfig = "ListSymbolConfig";
        public const string List_EvaluationConfig = "ListEvaluationConfig";   
        
    }
}