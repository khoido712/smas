/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.BookMarkRecordArea
{
    public class BookMarkRecordConstants
    {
        public const string LIST_MARKRECORD = "listMarkRecord";
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "ListEducationLevel";
        public const string LIST_CLASS = "ListClass";
        public const string LIST_SUBJECT = "ListSubject";
        public const string LABEL_MES = "LabelMessage";
        public const string CHECK_ENTRY = "CheckEntry";
        //public const string LS_LockedMarkDetail = "ListLockedMarkDetail";
       // public const string LS_EducationLevel = "ListEducationLevel";
        //public const string LS_Class = "ListClass";
        //public const string LS_Semester = "ListSemester";
        public const string LIST_MARKTYPE = "ListMarkType";
        public const string LIST_CLASSSUBJECT = "ListClassSubject";
        public const string LIST_SEMESTERDECLARATION = "ListSemesterDeclaration";
        public const string M = "M";
        public const string CoefficientM = "CoefficientM";
        
        public const string P = "P";
        public const string CoefficientP = "CoefficientP";
        public const string V = "V";
        public const string CoefficientV = "CoefficientV";
        public const string TBM = "TBM";
        public const string HK = "HK";
        public const string CoefficientHK = "CoefficientHK";
        public const string LIST_MARK = "ListMark";
        public const string checkHK1 = "CloumnHK1";
        public const string checkHK2 = "ColumnHK2";
        public const string COMMENT = "ColumnComment";

        public const string titleImport = "TitleImport";
        public const string listMarkRecordViewModel = "listMarkRecordViewModel";
        public const string ListTypeMarkMImport = "ListTypeMarkMImport";
        public const string ListTypeMarkPImport = "ListTypeMarkPImport";
        public const string ListTypeMarkVImport = "ListTypeMarkVImport";

        public const string LIST_IMPORTDATA = "LIST_IMPORTDATA";
        public const string HAS_ERROR_DATA = "HAS_ERROR_DATA";
        public const string CLASS_12 = "CLASS_12";
        public const string SEMESTER_1 = "SEMESTER_1";

        public const string ERROR_BASIC_DATA = "ERROR_BASIC_DATA";
        public const string ERROR_IMPORT_MESSAGE = "ERROR_IMPORT_MESSAGE";

        public const string ERROR_IMPORT = "ERROR_IMPORT";

        public const string colComment = "colComment";

        public const string LIST_LOCK_MARK = "ListLockMark";

        public const string SUBJECT_ID = "SubjectID";

        public const string SUBJECT_ID_INCREASE = "SubjectIdInCrease";

        public const string PHISICAL_PATH = "PhisicalPath";
        public const string CELL_CHECK_TYPE_FILE_IMPORT = "B3";

        public const string SHOW_MESSAGE_NOTE = "ShowMessageNote";
        public const string ISMINSEMESTER = "IsMinSemester";
        public const string MMIN = "MMin";
        public const string PMIN = "PMin";
        public const string VMIN = "VMin";

        public const string LAST_UPDATE_STRING = "LAST_UPDATE_STRING";
        public const string LAST_UPDATE_MARK_RECORD = "LAST_UPDATE_MARK_RECORD";
        public const string TITLE_STRING = "TITLE_STRING";
        public const string LIST_CHANGE_MARK = "LIST_CHANGE_MARK";
        public const string IS_LOCK_INPUT = "IsLockInput";
        public const string LOCK_USER_NAME = "LockUserName";
    }
}