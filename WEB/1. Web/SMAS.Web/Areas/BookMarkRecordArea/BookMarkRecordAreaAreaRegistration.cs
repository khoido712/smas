﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.BookMarkRecordArea
{
    public class BookMarkRecordAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "BookMarkRecordArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "BookMarkRecordArea_default",
                "BookMarkRecordArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
