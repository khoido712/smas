﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.BookMarkRecordArea.Models
{
    public class MarkRecordViewObjModel
    {
        public string Error { get; set; }
        public List<MarkRecordViewModel> listMarkRecordViewModel { get; set; }
    }
}