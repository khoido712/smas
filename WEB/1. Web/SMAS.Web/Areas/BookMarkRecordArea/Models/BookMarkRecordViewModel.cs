/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.ComponentModel;
namespace SMAS.Web.Areas.BookMarkRecordArea.Models
{
    public class BookMarkRecordViewModel
    {
        public long? MarkRecordID { get; set; }
        public int? Semester { get; set; }
        public int? PupilID { get; set; }
        public int? ClassID { get; set; }
        public int? SubjectID { get; set; }
        public int? SchoolID { get; set; }
        public int? AcademicYearID { get; set; }
        public string Name { get; set; }
        public int? OrderInClass { get; set; }

        [ResourceDisplayName("MarkRecord_Label_PupilCode")]
        [UIHint("Display")]
        [ReadOnly(true)]
        public string PupilCode { get; set; }

        [ResourceDisplayName("MarkRecord_Label_FullName")]
        [UIHint("Display")]
        [ReadOnly(true)]
        public string FullName { get; set; }

        [ResourceDisplayName("MarkRecord_Label_KTHK")]
        [UIHint("Display")]
        public decimal? Mark { get; set; }

        public decimal?[] DiemM { get; set; }
        public decimal?[] DiemP { get; set; }

        public decimal?[] DiemV { get; set; }
        public decimal? KTHK { get; set; }
        public decimal? TBMTC { get; set; }
        public string Title { get; set; }
        public decimal? TBM1 { get; set; }
        public string strMarkTitle { get; set; }
        public decimal? TBMCN { get; set; }
        public int Status { get; set; }
        public bool chkSatus { get; set; }
        public string Comment { get; set; }
        public decimal? TBM2 { get; set; }

        public bool IsExempted { get; set; }
        public DateTime? TimeStatus { get; set; }
        public bool show { get; set; }
        public DateTime? DateClassMoving { get; set; }
        public DateTime? DatePupilLeavingOff { get; set; }
        public DateTime? DateSchoolMovement { get; set; }
        public string[] LogChangeM { get; set; }
        public string[] LogChangeP { get; set; }
        public string[] LogChangeV { get; set; }
        public string LogChangeHK { get; set; }
    }
}


