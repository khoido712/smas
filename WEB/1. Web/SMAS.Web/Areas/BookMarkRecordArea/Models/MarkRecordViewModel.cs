/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.BookMarkRecordArea.Models
{
    public class MarkRecordViewModel
    {
        public string PupilCode { get; set; }
        public int PupilID { get; set; }
        public string Fullname { get; set; }
        public string SummedUpMark { get; set; }
        public int Status { get; set; }
        public decimal[] InterviewMark { get; set; }
        public decimal[] WritingMark { get; set; }
        public decimal[] TwiceCoeffiecientMark { get; set; }
        public string MarkTBM { get; set; }
        public string Comment { get; set; }
        public string Disable { get; set; }
        public bool? Pass { get; set; }
        public string Note { get; set; }

        public int? SubjectID { get; set; }
        public int? ClassID { get; set; }
        public int? PeriodID { get; set; }
        public int? SemesterID { get; set; }

        public Dictionary<string, string> ListMark { get; set; }
        public Dictionary<string, bool> lstIsChangeMark { get; set; }

        public string SheetName { get; set; }

        public string MarkSemester { get; set; }
        public int AcademicYearID { get; set; }
        public int SchoolID { get; set; }
        public int Year { get; set; }
        public int IsCommenting { get; set; }


        //public System.Int32 MarkTypeID { get; set; }
        //public System.String Judgement { get; set; }
        //public System.Int32 OrderNumber { get; set; }
        //public System.Nullable<System.DateTime> MarkedDate { get; set; }
        //public System.Nullable<System.DateTime> CreatedDate { get; set; }
        //public System.Nullable<System.DateTime> ModifiedDate { get; set; }
        //public System.String Title { get; set; }
        //public bool? IsLegalBot { get; set; }
        //public System.Int32 flagPupil { get; set; }
    }
}


