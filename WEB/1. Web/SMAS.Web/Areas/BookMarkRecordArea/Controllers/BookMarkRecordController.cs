﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.VTUtils.LockAction;
using SMAS.Web.Areas.BookMarkRecordArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using SMAS.VTUtils.Log;
using Newtonsoft.Json;

namespace SMAS.Web.Areas.BookMarkRecordArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class BookMarkRecordController : BaseController
    {
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISemeterDeclarationBusiness SemesterDeclarationBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ILockedMarkDetailBusiness LockedMarkDetailBusiness;
        private readonly IMarkRecordHistoryBusiness MarkRecordHistoryBusiness;
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness;
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IRestoreDataBusiness RestoreDataBusiness;
        private readonly IRestoreDataDetailBusiness RestoreDataDetailBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;

        public BookMarkRecordController(IPupilProfileBusiness pupilProfileBusiness,
                                        ISummedUpRecordBusiness summedUpRecordBusiness,
                                        IAcademicYearBusiness academicYearBusiness,
                                        ISemeterDeclarationBusiness semeterDeclarationBusiness,
                                        IMarkRecordBusiness markrecordBusiness,
                                        ISubjectCatBusiness subjectCatBusiness,
                                        ITeachingAssignmentBusiness teachingAssignmentBusiness,
                                        IClassProfileBusiness classProfileBusiness,
                                        IClassSubjectBusiness classSubjectBusiness,

                                        IPupilOfClassBusiness PupilOfClassBusiness,
                                        IMarkTypeBusiness MarkTypeBusiness,
                                        IExemptedSubjectBusiness exemptedSubjectBusiness,
                                        IUserAccountBusiness UserAccountBusiness,
                                        IMarkRecordHistoryBusiness MarkRecordHistoryBusiness,
                                        ILockedMarkDetailBusiness LockedMarkDetailBusiness,
                                        ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness,
                                        IJudgeRecordBusiness JudgeRecordBusiness,
                                        IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness,
                                        IClassSupervisorAssignmentBusiness classSupervisorAssignmentBusiness,
                                        IEmployeeBusiness employeeBusiness,
                                        IRestoreDataBusiness RestoreDataBusiness,
                                        IRestoreDataDetailBusiness RestoreDataDetailBusiness,
                                        ISchoolProfileBusiness SchoolProfileBusiness
            )
        {
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.SummedUpRecordBusiness = summedUpRecordBusiness;
            this.SemesterDeclarationBusiness = semeterDeclarationBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.MarkRecordBusiness = markrecordBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.MarkTypeBusiness = MarkTypeBusiness;
            this.ExemptedSubjectBusiness = exemptedSubjectBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.LockedMarkDetailBusiness = LockedMarkDetailBusiness;
            this.MarkRecordHistoryBusiness = MarkRecordHistoryBusiness;
            this.SummedUpRecordHistoryBusiness = SummedUpRecordHistoryBusiness;
            this.JudgeRecordBusiness = JudgeRecordBusiness;
            this.JudgeRecordHistoryBusiness = JudgeRecordHistoryBusiness;
            this.ClassSupervisorAssignmentBusiness = classSupervisorAssignmentBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.RestoreDataBusiness = RestoreDataBusiness;
            this.RestoreDataDetailBusiness = RestoreDataDetailBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
        }

        public ActionResult Index()
        {
            //Get view data here
            SetViewData();
            List<MarkRecordViewModel> lst = null;
            ViewData[BookMarkRecordConstants.LIST_MARKRECORD] = lst;
            ViewData[BookMarkRecordConstants.HAS_ERROR_DATA] = false;
            ViewData[BookMarkRecordConstants.LIST_IMPORTDATA] = null;

            ViewData[BookMarkRecordConstants.CLASS_12] = false;
            ViewData[BookMarkRecordConstants.SEMESTER_1] = true;
            ViewData[BookMarkRecordConstants.ERROR_BASIC_DATA] = false;
            ViewData[BookMarkRecordConstants.ERROR_IMPORT_MESSAGE] = "";
            ViewData[BookMarkRecordConstants.ERROR_IMPORT] = true;

            return View();
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Create(FormCollection fc)
        {
            string keyNames = "";
            try
            {
                //MarkRecordBusiness.SetAutoDetectChangesEnabled(false);
                bool isDBChange = false;
                int idSubject = int.Parse(fc["SubjectID"]);
                int subjectIDForm = int.Parse(fc["frmSubjectID"]);
                if (fc["frmSubjectID"] == null || subjectIDForm != idSubject || subjectIDForm == 0)
                {
                    return Json(new JsonMessage(Res.Get("ExaminationSubject_Label_DataListError")));
                }

                GlobalInfo Global = new GlobalInfo();
                int InterviewMark = 0;
                int WritingMark = 0;
                int TwiceCoeffiecientMark = 0;
                int idSemester = int.Parse(fc["Semester"]);
                int idClass = int.Parse(fc["ClassID"]);


                //Lock action de tranh giao dich dong thoi
                List<string> lstKeyName = new List<string>();
                lstKeyName.Add("MarkRecord");
                lstKeyName.Add(_globalInfo.UserAccountID.ToString());
                lstKeyName.Add(idClass.ToString());//
                lstKeyName.Add(idSemester.ToString());
                lstKeyName.Add(idSubject.ToString());

                keyNames = LockManager.CreatedKey(lstKeyName);
                if (!LockManager.Lock(keyNames, Session.SessionID))
                {
                    return Json(new
                    {
                        Message = Res.Get("MarkRecord_Label_SavingMark"),
                        Type = JsonMessage.ERROR,
                        isDBChange = false
                    });
                }

                //int AutoSave = int.Parse(fc["hdAutoSave"]);
                List<string> lstChangeMark = new List<string>();
                string lockTitle = fc["LockTitle"];
                // Kiem tra co mo khoa diem hay khong de khong bi xoa nham diem vua duoc mo khoa
                LockedMarkDetailBusiness.CheckCurrentLockMark(lockTitle, new Dictionary<string, object>()
            {
                {"Semester", idSemester},
                {"ClassID", idClass},
                {"SubjectID", idSubject},
                {"SchoolID", Global.SchoolID.Value},
                {"AcademicYearID", Global.AcademicYearID},
            });


                bool flag = false; // kiểm tra xem có nhập con điểm nào không
                AcademicYear AcademicYear = AcademicYearBusiness.Find(Global.AcademicYearID.Value);
                bool isMovedHistory = UtilsBusiness.IsMoveHistory(AcademicYear);
                IDictionary<string, object> dicM = new Dictionary<string, object>();
                dicM["AcademicYearID"] = Global.AcademicYearID;
                dicM["SchoolID"] = Global.SchoolID;
                //dicM["Year"] = AcademicYear.Year; //Chiendd1: Bo nam de lay cac cau hinh khai bao truoc do
                dicM["Semester"] = idSemester;
                int Year = AcademicYear.Year;
                SemeterDeclaration SemeterDeclaration = SemesterDeclarationBusiness.Search(dicM).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
                if (SemeterDeclaration != null)
                {
                    InterviewMark = SemeterDeclaration.InterviewMark;
                    WritingMark = SemeterDeclaration.WritingMark;
                    TwiceCoeffiecientMark = SemeterDeclaration.TwiceCoeffiecientMark;
                }

                List<MarkType> listMarkType = MarkTypeBusiness.Search(new Dictionary<string, object>() { { "AppliedLevel", Global.AppliedLevel.Value } }).ToList();
                int markTypeIDM = listMarkType.FirstOrDefault(u => u.Title == "M").MarkTypeID;
                int markTypeIDP = listMarkType.FirstOrDefault(u => u.Title == "P").MarkTypeID;
                int markTypeIDV = listMarkType.FirstOrDefault(u => u.Title == "V").MarkTypeID;
                int markTypeIDHK = listMarkType.FirstOrDefault(u => u.Title == "HK").MarkTypeID;

                ClassSubject cs = ClassSubjectBusiness.SearchBySchool(Global.SchoolID.Value, new Dictionary<string, object> { { "ClassID", idClass }, { "SubjectID", idSubject } }).FirstOrDefault();
                string[] lsPupilID = fc["chkSatus"] != null ? fc["chkSatus"].Split(new Char[] { ',' }) : new string[] { };
                List<MarkRecord> lsMarkRecord = new List<MarkRecord>();
                List<SummedUpRecord> lstSummedUpRecord = new List<SummedUpRecord>();
                List<int> lstExemptedSubjectPupil = new List<int>();

                List<ExemptedSubject> lstExemptedSubject = ExemptedSubjectBusiness.GetListExemptedSubject(idClass, idSemester).Where(u => u.SubjectID == idSubject).ToList();

                IQueryable<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(Global.SchoolID.Value, new Dictionary<string, object> {
            { "AcademicYearID", Global.AcademicYearID }, { "ClassID", idClass }, { "Check", "Check" } }
                );

                List<int> lstPupilIdAll = lstPOC.Select(o => o.PupilID).Distinct().ToList();

                List<OldMark> lstOldMark = new List<OldMark>();

                // Tạo data ghi log
                StringBuilder objectIDStr = new StringBuilder();
                StringBuilder descriptionStr = new StringBuilder();
                StringBuilder paramsStr = new StringBuilder();
                StringBuilder oldObjectStr = new StringBuilder();
                StringBuilder newObjectStr = new StringBuilder();
                StringBuilder userFuntionsStr = new StringBuilder();
                StringBuilder userActionsStr = new StringBuilder();
                StringBuilder userDescriptionsStr = new StringBuilder();
                StringBuilder isInsertLogstr = new StringBuilder();
                StringBuilder inforLog = null;
                StringBuilder oldobjtmp = null;

                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                StringBuilder isInsertLogstrtmp = new StringBuilder();
                int iLog = 0;
                bool isInsertLog = false;

                List<MarkRecordHistory> listTempHistory = null;
                List<MarkRecord> listTemp = null;
                if (isMovedHistory)
                {
                    Dictionary<string, object> Dics = new Dictionary<string, object>();
                    Dics.Add("ClassID", idClass);
                    Dics.Add("AcademicYearID", AcademicYear.AcademicYearID);
                    Dics.Add("SchoolID", AcademicYear.SchoolID);
                    Dics.Add("SubjectID", idSubject);
                    Dics.Add("Semester", idSemester);
                    listTempHistory = MarkRecordHistoryBusiness.SearchMarkRecordPrimaryHistory(Dics).ToList();
                }
                else
                {
                    int modSchoolID = _globalInfo.SchoolID.Value % 100;
                    listTemp = (from m in MarkRecordBusiness.AllNoTracking
                                where
                                //lsPupilID.Contains(m.PupilID.ToString()) && 
                                m.Last2digitNumberSchool == modSchoolID
                               && m.AcademicYearID == AcademicYear.AcademicYearID
                               && m.Semester == idSemester && m.SubjectID == idSubject
                               && m.ClassID == idClass
                                select m).ToList();
                }

                IDictionary<string, decimal> dicMarkOld = new Dictionary<string, decimal>();

                foreach (var item in lsPupilID)
                {
                    var id = int.Parse(item);
                    isInsertLog = false;
                    lstOldMark = new List<OldMark>();
                    if (lstPupilIdAll.Contains(id))
                    {
                        // Tạo dữ liệu ghi log
                        PupilOfClass pop = lstPOC.FirstOrDefault(p => p.PupilID == id);
                        inforLog = new StringBuilder();
                        oldobjtmp = new StringBuilder();
                        inforLog.Append("Cập nhật điểm HS " + pop.PupilProfile.FullName);
                        inforLog.Append(", mã " + pop.PupilProfile.PupilCode);
                        inforLog.Append(", Lớp " + pop.ClassProfile.DisplayName);
                        inforLog.Append("/" + cs.SubjectCat.SubjectName);
                        inforLog.Append("/Học kỳ " + idSemester);
                        inforLog.Append("/" + pop.Year.Value + "-" + (pop.Year.Value + 1));
                        if (isMovedHistory)
                        {
                            if (listTempHistory != null)
                            {
                                List<MarkRecordHistory> listOldRecord = listTempHistory.Where(p => p.PupilID == id && p.ClassID == idClass && p.SubjectID == idSubject && p.Semester == idSemester).ToList();
                                oldobjtmp.Append("(Giá trị trước khi sửa): {");
                                for (int j = 0; j < listOldRecord.Count; j++)
                                {
                                    OldMark objOldMark = new OldMark();
                                    MarkRecordHistory record = listOldRecord[j];
                                    if (fc["isChange_" + id + record.Title] != null && "1".Equals(fc["isChange_" + id + record.Title]))
                                    {

                                        oldobjtmp.Append(record.Title + ":" + record.Mark);
                                        isInsertLog = true;
                                        objOldMark.MarkRecordID = record.MarkRecordID;
                                        objOldMark.Title = record.Title;
                                        objOldMark.Mark = record.Mark;
                                        lstOldMark.Add(objOldMark);
                                    }

                                    if (j < listOldRecord.Count - 1)
                                    {
                                        oldobjtmp.Append(", ");
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (listTemp != null && listTemp.Count > 0)
                            {
                                List<MarkRecord> listOldRecord = listTemp.Where(p => p.PupilID == id && p.ClassID == idClass && p.SubjectID == idSubject && p.Semester == idSemester).ToList();
                                oldobjtmp.Append("(Giá trị trước khi sửa): {");
                                for (int j = 0; j < listOldRecord.Count; j++)
                                {
                                    OldMark objOldMark = new OldMark();
                                    MarkRecord record = listOldRecord[j];
                                    if (fc["isChange_" + id + record.Title] != null && "1".Equals(fc["isChange_" + id + record.Title]))
                                    {
                                        oldobjtmp.Append(record.Title + ":" + record.Mark);
                                        isInsertLog = true;
                                        objOldMark.MarkRecordID = record.MarkRecordID;
                                        objOldMark.Title = record.Title;
                                        objOldMark.Mark = record.Mark;
                                        lstOldMark.Add(objOldMark);
                                        if (j < listOldRecord.Count - 1)
                                        {
                                            oldobjtmp.Append(", ");
                                        }
                                    }
                                }
                            }
                        }
                        descriptionStrtmp.Append("Update mark record pupil_id:" + id);
                        paramsStrtmp.Append("pupil_id:" + id);
                        userFuntionsStrtmp.Append("Sổ điểm");
                        userActionsStrtmp.Append(GlobalConstants.ACTION_UPDATE);
                        userDescriptionsStrtmp.Append(inforLog.ToString()).Append(": ");
                        objectIDStrtmp.Append(id);
                        oldObjectStrtmp.Append(oldobjtmp);
                        newObjectStrtmp.Append("(Giá trị sau khi sửa): {");

                        //Neu la hoc sinh duoc mien giam
                        if (lstExemptedSubject.Any(u => u.PupilID == id))
                        {
                            lstExemptedSubjectPupil.Add(id);
                        }
                        else //Neu hoc sinh khong duoc mien giam
                        {
                            MarkRecord mr = null;
                            OldMark objOM = null;
                            #region diem mieng
                            for (int i = 1; i <= InterviewMark; i++)
                            {
                                string strMark = id.ToString() + "M" + i.ToString();
                                //if (fc[strMark] != "" && fc[strMark] != null)
                                //{
                                mr = new MarkRecord();
                                mr.AcademicYearID = Global.AcademicYearID.Value;
                                mr.SchoolID = Global.SchoolID.Value;
                                mr.ClassID = idClass;
                                mr.PupilID = id;
                                mr.SubjectID = idSubject;
                                mr.MarkTypeID = markTypeIDM;
                                mr.OrderNumber = (int)i;
                                mr.Title = "M" + i.ToString();
                                mr.CreatedAcademicYear = AcademicYear.Year;
                                mr.Semester = idSemester;
                                if (fc["isChange_" + id + mr.Title] != null && "1".Equals(fc["isChange_" + id + mr.Title]))
                                {
                                    lstChangeMark.Add(fc["isChange_" + id + mr.Title]);
                                    if (lstOldMark.Count > 0)
                                    {
                                        objOM = lstOldMark.Where(p => p.Title.Equals(mr.Title)).FirstOrDefault();
                                        if (objOM != null)
                                        {
                                            userDescriptionsStrtmp.Append(mr.Title + "(" + objOM.Mark + ":");
                                            mr.MarkRecordID = objOM.MarkRecordID;
                                        }
                                        else
                                        {
                                            userDescriptionsStrtmp.Append(mr.Title + "(");
                                        }
                                    }
                                    else
                                    {
                                        userDescriptionsStrtmp.Append(mr.Title + "(");
                                    }
                                    if (fc[strMark] != "" && fc[strMark] != null)
                                    {
                                        mr.Mark = decimal.Parse(fc[strMark]);
                                        mr.Mark = decimal.Parse(Business.Common.Utils.FormatMark(mr.Mark));
                                        userDescriptionsStrtmp.Append(mr.Mark + "); ");
                                        newObjectStrtmp.Append(mr.Title + ":" + mr.Mark);

                                    }
                                    else
                                    {
                                        userDescriptionsStrtmp.Append("); ");
                                        newObjectStrtmp.Append(mr.Title + ":");
                                        mr.Mark = -1;
                                    }
                                    newObjectStrtmp.Append(", ");
                                    isInsertLog = true;
                                }
                                else
                                {
                                    mr.Mark = -1;
                                }

                                mr.CreatedDate = DateTime.Now;
                                lsMarkRecord.Add(mr);
                                flag = true;
                            }
                            #endregion

                            #region diem he so 1
                            for (int i = 1; i <= WritingMark; i++)
                            {
                                string strMark = id.ToString() + "P" + i.ToString();
                                //if (fc[strMark] != "" && fc[strMark] != null)
                                //{
                                mr = new MarkRecord();
                                mr.AcademicYearID = Global.AcademicYearID.Value;
                                mr.SchoolID = Global.SchoolID.Value;
                                mr.ClassID = idClass;
                                mr.PupilID = id;
                                mr.SubjectID = idSubject;
                                mr.MarkTypeID = markTypeIDP;
                                mr.OrderNumber = (int)i;
                                mr.Title = "P" + i.ToString();
                                mr.CreatedAcademicYear = AcademicYear.Year;
                                mr.Semester = idSemester;
                                if (fc["isChange_" + id + mr.Title] != null && "1".Equals(fc["isChange_" + id + mr.Title]))
                                {
                                    lstChangeMark.Add(fc["isChange_" + id + mr.Title]);
                                    if (lstOldMark.Count > 0)
                                    {
                                        objOM = lstOldMark.FirstOrDefault(p => p.Title.Equals(mr.Title));
                                        if (objOM != null)
                                        {
                                            userDescriptionsStrtmp.Append(mr.Title + "(" + decimal.Parse(Business.Common.Utils.FormatMark(objOM.Mark)) + ":");
                                            mr.MarkRecordID = objOM.MarkRecordID;
                                        }
                                        else
                                        {
                                            userDescriptionsStrtmp.Append(mr.Title + "(");
                                        }
                                    }
                                    else
                                    {
                                        userDescriptionsStrtmp.Append(mr.Title + "(");
                                    }
                                    if (fc[strMark] != "" && fc[strMark] != null)
                                    {
                                        mr.Mark = decimal.Parse(fc[strMark].Replace(".", ","));
                                        mr.Mark = decimal.Parse(Business.Common.Utils.FormatMark(mr.Mark));
                                        userDescriptionsStrtmp.Append(mr.Mark + "); ");
                                        newObjectStrtmp.Append(mr.Title + ":" + mr.Mark);

                                    }
                                    else
                                    {
                                        userDescriptionsStrtmp.Append("); ");
                                        newObjectStrtmp.Append(mr.Title + ":");
                                        mr.Mark = -1;
                                    }
                                    newObjectStrtmp.Append(", ");
                                    isInsertLog = true;
                                }
                                else
                                {
                                    mr.Mark = -1;
                                }
                                mr.CreatedDate = DateTime.Now;
                                lsMarkRecord.Add(mr);
                                flag = true;
                            }
                            #endregion

                            #region diem he so 2
                            for (int i = 1; i <= TwiceCoeffiecientMark; i++)
                            {
                                string strMark = id.ToString() + "V" + i.ToString();
                                //if (fc[strMark] != "" && fc[strMark] != null)
                                //{
                                mr = new MarkRecord();
                                mr.AcademicYearID = Global.AcademicYearID.Value;
                                mr.SchoolID = Global.SchoolID.Value;
                                mr.ClassID = idClass;
                                mr.PupilID = id;
                                mr.SubjectID = idSubject;
                                mr.MarkTypeID = markTypeIDV;
                                mr.OrderNumber = (int)i;
                                mr.Title = "V" + i.ToString();
                                mr.CreatedAcademicYear = AcademicYear.Year;
                                mr.Semester = idSemester;
                                if (fc["isChange_" + id + mr.Title] != null && "1".Equals(fc["isChange_" + id + mr.Title]))
                                {
                                    lstChangeMark.Add(fc["isChange_" + id + mr.Title]);
                                    if (lstOldMark.Count > 0)
                                    {
                                        objOM = lstOldMark.Where(p => p.Title.Equals(mr.Title)).FirstOrDefault();
                                        if (objOM != null)
                                        {
                                            userDescriptionsStrtmp.Append(mr.Title + "(" + decimal.Parse(Business.Common.Utils.FormatMark(objOM.Mark)) + ":");
                                            mr.MarkRecordID = objOM.MarkRecordID;
                                        }
                                        else
                                        {
                                            userDescriptionsStrtmp.Append(mr.Title + "(");
                                        }
                                    }
                                    else
                                    {
                                        userDescriptionsStrtmp.Append(mr.Title + "(");
                                    }
                                    if (fc[strMark] != "" && fc[strMark] != null)
                                    {
                                        mr.Mark = decimal.Parse(fc[strMark].Replace(".", ","));
                                        mr.Mark = decimal.Parse(Business.Common.Utils.FormatMark(mr.Mark));
                                        userDescriptionsStrtmp.Append(mr.Mark + "); ");
                                        newObjectStrtmp.Append(mr.Title + ":" + mr.Mark);

                                    }
                                    else
                                    {
                                        userDescriptionsStrtmp.Append("); ");
                                        newObjectStrtmp.Append(mr.Title + ":");
                                        mr.Mark = -1;
                                    }
                                    newObjectStrtmp.Append(", ");
                                    isInsertLog = true;
                                }
                                else
                                {
                                    mr.Mark = -1;
                                }
                                mr.CreatedDate = DateTime.Now;
                                lsMarkRecord.Add(mr);
                                flag = true;
                            }
                            #endregion

                            #region diem hoc ky
                            string strHK = id.ToString() + "HK";
                            //if (fc[strHK] != "" && fc[strHK] != null)
                            //{
                            mr = new MarkRecord();
                            mr.AcademicYearID = Global.AcademicYearID.Value;
                            mr.SchoolID = Global.SchoolID.Value;
                            mr.ClassID = idClass;
                            mr.PupilID = id;
                            mr.SubjectID = idSubject;
                            mr.MarkTypeID = markTypeIDHK;
                            mr.Title = "HK";
                            mr.CreatedAcademicYear = AcademicYear.Year;
                            mr.Semester = idSemester;
                            if (fc["isChange_" + id + mr.Title] != null && "1".Equals(fc["isChange_" + id + mr.Title]))
                            {
                                lstChangeMark.Add(fc["isChange_" + id + mr.Title]);
                                if (lstOldMark.Count > 0)
                                {
                                    objOM = lstOldMark.Where(p => p.Title.Equals(mr.Title)).FirstOrDefault();
                                    if (objOM != null)
                                    {
                                        userDescriptionsStrtmp.Append(mr.Title + "(" + decimal.Parse(Business.Common.Utils.FormatMark(objOM.Mark)) + ":");
                                        mr.MarkRecordID = objOM.MarkRecordID;
                                    }
                                    else
                                    {
                                        userDescriptionsStrtmp.Append(mr.Title + "(");
                                    }
                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append(mr.Title + "(");
                                }
                                if (fc[strHK] != "" && fc[strHK] != null)
                                {
                                    mr.Mark = decimal.Parse(fc[strHK].Replace(".", ","));
                                    mr.Mark = decimal.Parse(Business.Common.Utils.FormatMark(mr.Mark));
                                    userDescriptionsStrtmp.Append(mr.Mark + "); ");
                                    newObjectStrtmp.Append(mr.Title + ":" + mr.Mark);

                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append("); ");
                                    newObjectStrtmp.Append(mr.Title + ":");
                                    mr.Mark = -1;
                                }
                                newObjectStrtmp.Append(", ");
                                isInsertLog = true;
                            }
                            else
                            {
                                mr.Mark = -1;
                            }
                            mr.CreatedDate = DateTime.Now;
                            lsMarkRecord.Add(mr);
                            flag = true;
                            #endregion

                            #region diem tong ket
                            if (flag)
                            {
                                SummedUpRecord sur = new SummedUpRecord();
                                sur.PupilID = id;
                                sur.ClassID = idClass;
                                sur.AcademicYearID = Global.AcademicYearID.Value;
                                sur.SchoolID = Global.SchoolID.Value;
                                sur.SubjectID = idSubject;
                                sur.IsCommenting = cs.IsCommenting.Value;
                                sur.CreatedAcademicYear = AcademicYear.Year;
                                sur.Semester = idSemester;
                                if (cs.SubjectCat.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                                {
                                    if (lsMarkRecord.Count == 0 && !string.IsNullOrEmpty(fc[id.ToString() + "Cm"]))
                                        return Json(new JsonMessage(Res.Get("MarkRecordPeriod_Label_Noinput"), JsonMessage.ERROR));

                                    sur.Comment = fc[id.ToString() + "Cm"];
                                }
                                lstSummedUpRecord.Add(sur);
                            }
                            #endregion
                        }
                        // Tạo dữ liệu ghi log
                        if (isInsertLog)
                        {
                            string newObj = string.Empty;
                            newObj = newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) + "}";
                            newObj += GlobalConstants.WILD_LOG;
                            string tmp = string.Empty;
                            objectIDStrtmp.Append(GlobalConstants.WILD_LOG);
                            descriptionStrtmp.Append(GlobalConstants.WILD_LOG);
                            paramsStrtmp.Append(GlobalConstants.WILD_LOG);
                            oldObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                            newObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                            userFuntionsStrtmp.Append(GlobalConstants.WILD_LOG);
                            userActionsStrtmp.Append(GlobalConstants.WILD_LOG);
                            tmp = userDescriptionsStrtmp != null ? userDescriptionsStrtmp.ToString().Substring(0, userDescriptionsStrtmp.Length - 2) : "";
                            tmp += GlobalConstants.WILD_LOG;
                            //userDescriptionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                            objectIDStr.Append(objectIDStrtmp);
                            descriptionStr.Append(descriptionStrtmp);
                            paramsStr.Append(paramsStrtmp);
                            oldObjectStr.Append(oldObjectStrtmp);
                            newObjectStr.Append(newObj);
                            userFuntionsStr.Append(userFuntionsStrtmp);
                            userActionsStr.Append(userActionsStrtmp);
                            userDescriptionsStr.Append(tmp);
                        }

                        objectIDStrtmp = new StringBuilder();
                        descriptionStrtmp = new StringBuilder();
                        paramsStrtmp = new StringBuilder();
                        oldObjectStrtmp = new StringBuilder();
                        newObjectStrtmp = new StringBuilder();
                        userFuntionsStrtmp = new StringBuilder();
                        userActionsStrtmp = new StringBuilder();
                        userDescriptionsStrtmp = new StringBuilder();
                        isInsertLogstrtmp = new StringBuilder();
                        iLog++;
                    }
                }

                //Danh sách học sinh được miễn giảm. Nếu lấy trong vòng for ở trên thì không lấy được do học sinh không được check.
                lstExemptedSubjectPupil = lstExemptedSubject.Where(o => lstPupilIdAll.Contains(o.PupilID)).Select(o => o.PupilID).Distinct().ToList();
                if (lsMarkRecord.Any())
                {
                    //Du lieu lich su
                    if (isMovedHistory)
                    {
                        List<MarkRecordHistory> lstMarkRecordHistory = new List<MarkRecordHistory>();
                        List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                        MarkRecordHistory objMarkRecordHistory = null;
                        MarkRecord objMarkRecord = null;
                        SummedUpRecord objSummedUpRecord = null;
                        SummedUpRecordHistory objSummedUpRecordHistory = null;
                        for (int i = 0; i < lsMarkRecord.Count; i++)
                        {
                            objMarkRecordHistory = new MarkRecordHistory();
                            objMarkRecord = new MarkRecord();
                            objMarkRecord = lsMarkRecord[i];
                            objMarkRecordHistory.MarkRecordID = objMarkRecord.MarkRecordID;
                            objMarkRecordHistory.AcademicYearID = objMarkRecord.AcademicYearID;
                            objMarkRecordHistory.PupilID = objMarkRecord.PupilID;
                            objMarkRecordHistory.SchoolID = objMarkRecord.SchoolID;
                            objMarkRecordHistory.ClassID = objMarkRecord.ClassID;
                            objMarkRecordHistory.Mark = objMarkRecord.Mark;
                            objMarkRecordHistory.MarkTypeID = objMarkRecord.MarkTypeID;
                            objMarkRecordHistory.Semester = objMarkRecord.Semester;
                            objMarkRecordHistory.Title = objMarkRecord.Title;
                            objMarkRecordHistory.Last2digitNumberSchool = objMarkRecord.Last2digitNumberSchool;
                            objMarkRecordHistory.MarkedDate = objMarkRecord.MarkedDate;
                            objMarkRecordHistory.ModifiedDate = objMarkRecord.ModifiedDate;
                            objMarkRecordHistory.SubjectID = objMarkRecord.SubjectID;
                            objMarkRecordHistory.CreatedDate = objMarkRecord.CreatedDate;
                            objMarkRecordHistory.CreatedAcademicYear = objMarkRecord.CreatedAcademicYear;
                            objMarkRecordHistory.OrderNumber = objMarkRecord.OrderNumber;
                            lstMarkRecordHistory.Add(objMarkRecordHistory);
                        }
                        for (int i = 0; i < lstSummedUpRecord.Count; i++)
                        {
                            objSummedUpRecord = new SummedUpRecord();
                            objSummedUpRecordHistory = new SummedUpRecordHistory();
                            objSummedUpRecord = lstSummedUpRecord[i];
                            objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                            objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                            objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                            objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                            objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                            objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                            objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                            objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                            objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                            objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                            objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                            objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                            objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                            objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                            objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                            objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                            objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                            lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                        }
                        isDBChange = MarkRecordHistoryBusiness.InsertMarkRecordHistory(Global.UserAccountID, lstMarkRecordHistory, lstSummedUpRecordHistory, idSemester, null, lstExemptedSubjectPupil, Global.SchoolID.Value, Global.AcademicYearID.Value, Global.AppliedLevel.Value, idClass, idSubject, _globalInfo.EmployeeID, Year);
                    }
                    else // Du lieu hien tai
                    {
                        isDBChange = MarkRecordBusiness.InsertMarkRecord(Global.UserAccountID, lsMarkRecord, lstSummedUpRecord, idSemester, null, lstExemptedSubjectPupil, Global.SchoolID.Value, Global.AcademicYearID.Value, Global.AppliedLevel.Value, idClass, idSubject, _globalInfo.EmployeeID, Year);
                    }
                    // Tạo dữ liệu ghi log
                    IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
                    SetViewDataActionAudit(dicLog);
                    //SetViewDataActionAudit(oldObjectStr.ToString(), newObjectStr.ToString(), objectIDStr.ToString(), descriptionStr.ToString(), paramsStr.ToString(), userFuntionsStr.ToString(), userActionsStr.ToString(), userDescriptionsStr.ToString());

                }
                else
                {
                    string strListPupilID = !string.IsNullOrEmpty(fc.Get("chkSatus")) ? fc.Get("chkSatus") : string.Empty;
                    List<int> lstPupilID = strListPupilID.Split(',').Where(u => !string.IsNullOrEmpty(u)).Select(u => int.Parse(u)).ToList();
                    if (isMovedHistory)
                    {
                        MarkRecordHistoryBusiness.DeleteMarkRecordHistory(Global.UserAccountID, Global.SchoolID.Value, Global.AcademicYearID.Value, idClass, idSemester, null, idSubject, lstPupilID);
                        //Not del summed if mark not exits save Namta
                        List<int> listPupilID = lstPupilID.Where(o => lsMarkRecord.Select(u => u.PupilID).Contains(o)).ToList();
                        if (listPupilID != null && listPupilID.Count() > 0)
                        {
                            SummedUpRecordHistoryBusiness.DeleteSummedUpRecordHistory(Global.UserAccountID, Global.SchoolID.Value, Global.AcademicYearID.Value, idClass, idSemester, null, idSubject, listPupilID);
                        }
                        MarkRecordBusiness.Save();
                    }
                    else
                    {
                        MarkRecordBusiness.DeleteMarkRecord(Global.UserAccountID, Global.SchoolID.Value, Global.AcademicYearID.Value, idClass, idSemester, null, idSubject, lstPupilID);
                        SummedUpRecordBusiness.DeleteSummedUpRecord(Global.UserAccountID, Global.SchoolID.Value, Global.AcademicYearID.Value, idClass, idSemester, null, idSubject, lstPupilID);
                        //MarkRecordBusiness.Save();
                    }

                    // Tạo dữ liệu ghi log
                    SetViewDataActionAudit(oldObjectStr.ToString(), newObjectStr.ToString(), objectIDStr.ToString(), descriptionStr.ToString(), paramsStr.ToString(), userFuntionsStr.ToString(), userActionsStr.ToString(), userDescriptionsStr.ToString());
                }
                ///Luu danh sach cac o diem thay doi
                ViewData[BookMarkRecordConstants.LIST_CHANGE_MARK] = lstChangeMark;
                //return Json(new JsonMessage(Res.Get("Common_Label_UpdateMarkSuccess"),"success"));
                return Json(new
                {
                    Message = Res.Get("Common_Label_UpdateMarkSuccess"),
                    Type = "success",
                    isDBChange = isDBChange
                });
            }
            catch (Exception ex)
            {                
                LogExtensions.ErrorExt(logger, DateTime.Now, "BookMarkRecordController", "",ex);
                return Json(new
                {
                    Message = "Lỗi trong quá trình thực hiện lưu điểm",
                    Type = "success",
                    isDBChange = false
                });
            }
            finally
            {
                //Giai phong log action
                LockManager.ReleaseLock(keyNames, Session.SessionID);
                //MarkRecordBusiness.SetAutoDetectChangesEnabled(true);
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Delete(FormCollection fc)
        {
            GlobalInfo Global = new GlobalInfo();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(Global.AcademicYearID);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);

            int SchoolID = Global.SchoolID.Value;
            int AcademicYearID = Global.AcademicYearID.Value;
            int ClassID = int.Parse(fc["ClassID"]);
            int SubjectID = int.Parse(fc["SubjectID"]);
            int SemesterID = int.Parse(fc["Semester"]);
            string strListPupilID = !string.IsNullOrEmpty(fc.Get("chkSatus")) ? fc.Get("chkSatus") : string.Empty;
            List<int> lstPupilID = strListPupilID.Split(',').Where(u => !string.IsNullOrEmpty(u)).Select(u => int.Parse(u)).ToList();

            // Tạo dữ liệu ghi log
            List<PupilProfile> listPupilProfile = PupilProfileBusiness.All.Where(o => lstPupilID.Contains(o.PupilProfileID)).ToList();
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
            SubjectCat subject = SubjectCatBusiness.Find(SubjectID);
            StringBuilder oldObjecttmp = new StringBuilder();
            StringBuilder oldObject = new StringBuilder();
            StringBuilder objectID = new StringBuilder();
            StringBuilder descriptionObject = new StringBuilder();
            StringBuilder paramObject = new StringBuilder();
            StringBuilder userFunctions = new StringBuilder();
            StringBuilder userActions = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();
            StringBuilder userDescriptionstmp = new StringBuilder();
            StringBuilder markStr = null;
            int modSchoolID = SchoolID % 100;

            //Luu thong tin phuc vu viec phuc hoi du lieu
            //Lay danh sach lop de tao cot mo ta xoa du hs lop nao
            //string description = string.Format(" Xóa điểm môn {0} học sinh lớp {1}({2})", subject.DisplayName, classProfile.DisplayName, lstPupilID.Count);

            UserInfoBO userInfo = GlobalInfo.getInstance().GetUserLogin(User.Identity.Name);
            RESTORE_DATA objRes = new RESTORE_DATA
            {
                ACADEMIC_YEAR_ID = objAcademicYear.AcademicYearID,
                SCHOOL_ID = objAcademicYear.SchoolID,
                DELETED_DATE = DateTime.Now,
                DELETED_FULLNAME = userInfo.FullName,
                DELETED_USER = userInfo.UserName,
                RESTORED_DATE = DateTime.MinValue,
                RESTORED_STATUS = 0,
                RESTORE_DATA_ID = Guid.NewGuid(),
                RESTORE_DATA_TYPE_ID = RestoreDataConstant.RESTORE_DATA_TYPE_MARK,
                SHORT_DESCRIPTION = string.Format("Xóa điểm HS lớp: {0}, học kỳ:{1}, môn: {2}, ", classProfile.DisplayName, SemesterID, subject.DisplayName),
            };

            objRes.SHORT_DESCRIPTION += string.Format(" năm học: {0}, ", classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1));

            var query = from m in MarkRecordBusiness.All
                        where lstPupilID.Contains(m.PupilID) && m.Last2digitNumberSchool == modSchoolID
                        && m.AcademicYearID == AcademicYearID
                        && m.Semester == SemesterID && m.SubjectID == SubjectID
                        && m.ClassID == ClassID
                        orderby m.Title, m.MarkTypeID
                        select m;
            var queryHis = from m in MarkRecordHistoryBusiness.All
                           where lstPupilID.Contains(m.PupilID) && m.Last2digitNumberSchool == modSchoolID
                           && m.AcademicYearID == AcademicYearID
                           && m.Semester == SemesterID && m.SubjectID == SubjectID
                           && m.ClassID == ClassID
                           orderby m.Title, m.MarkTypeID
                           select m;
            List<MarkRecord> allMarks = null;
            List<MarkRecordHistory> allMarksHis = null;
            if (isMovedHistory)
            {
                allMarksHis = queryHis.ToList();
            }
            else
            {
                allMarks = query.ToList();
            }
            bool isCheckDelete = false;
            List<int> lstPupilDeleteID = new List<int>();
            List<RESTORE_DATA_DETAIL> lstRestoreDetail = new List<RESTORE_DATA_DETAIL>();
            for (int i = 0, size = listPupilProfile.Count; i < size; i++)
            {
                string strHK = string.Empty;
                isCheckDelete = false;
                oldObjecttmp = new StringBuilder();
                oldObjecttmp.Append("Giá trị trước khi xóa: ");
                markStr = new StringBuilder();
                if (isMovedHistory)
                {
                    if (allMarksHis != null)
                    {
                        MarkRecordHistory mark = null;
                        List<MarkRecordHistory> marks = allMarksHis.Where(a => a.PupilID == listPupilProfile[i].PupilProfileID).ToList();
                        if (marks.Count > 0)
                        {
                            for (int j = 0; j < marks.Count; j++)
                            {
                                mark = marks[j];
                                if ("HK".Equals(mark.Title))
                                {
                                    strHK = mark.Title + ":" + decimal.Parse(Business.Common.Utils.FormatMark(mark.Mark)) + "; ";
                                }
                                else
                                {
                                    oldObjecttmp.Append(mark.Title + ":" + decimal.Parse(Business.Common.Utils.FormatMark(mark.Mark)));
                                    oldObjecttmp.Append("; ");
                                }
                            }
                            oldObjecttmp.Append(strHK);
                            isCheckDelete = true;
                            lstPupilDeleteID.Add(listPupilProfile[i].PupilProfileID);
                        }
                    }
                }
                else
                {
                    if (allMarks != null)
                    {
                        MarkRecord mark = null;
                        List<MarkRecord> marks = allMarks.Where(a => a.PupilID == listPupilProfile[i].PupilProfileID).ToList();
                        ///Luu gia tri diem da xoa de phuc hoi diem                        
                        RESTORE_DATA_DETAIL objRestoreDetail;
                        if (marks.Count > 0)
                        {
                            for (int j = 0; j < marks.Count; j++)
                            {
                                mark = marks[j];
                                if ("HK".Equals(mark.Title))
                                {
                                    strHK = mark.Title + ":" + decimal.Parse(Business.Common.Utils.FormatMark(mark.Mark)) + "; ";
                                }
                                else
                                {
                                    oldObjecttmp.Append(mark.Title + ":" + decimal.Parse(Business.Common.Utils.FormatMark(mark.Mark)));
                                    oldObjecttmp.Append("; ");
                                }

                                //Luu thong tin khoi phuc diem
                                KeyDelMarkBO objKeyMark = new KeyDelMarkBO
                                {
                                    AcademicYearID = mark.AcademicYearID,
                                    ClassID = mark.ClassID,
                                    MarkTypeID = mark.MarkTypeID,
                                    PeriodID = mark.PeriodID,
                                    PupilID = mark.PupilID,
                                    SchoolID = mark.SchoolID,
                                    Semester = mark.Semester,
                                    SubjectID = mark.SubjectID
                                };

                                BakMarkRecordBO objDelMark = new BakMarkRecordBO
                                {
                                    AcademicYearID = mark.AcademicYearID,
                                    ClassID = mark.ClassID,
                                    CreatedAcademicYear = mark.CreatedAcademicYear,
                                    CreatedDate = mark.CreatedDate,
                                    IsOldData = mark.IsOldData,
                                    Last2digitNumberSchool = mark.Last2digitNumberSchool,
                                    LogChange = mark.LogChange,
                                    Mark = mark.Mark,
                                    MarkedDate = mark.MarkedDate,
                                    MarkRecordID = mark.MarkRecordID,
                                    MarkTypeID = mark.MarkTypeID,
                                    ModifiedDate = mark.ModifiedDate,
                                    MSourcedb = mark.MSourcedb,
                                    M_MarkRecord = mark.M_MarkRecord,
                                    M_OldID = mark.M_OldID,
                                    OldMark = mark.OldMark,
                                    OrderNumber = mark.OrderNumber,
                                    PeriodID = mark.PeriodID,
                                    PupilID = mark.PupilID,
                                    SchoolID = mark.SchoolID,
                                    Semester = mark.Semester,
                                    SubjectID = mark.SubjectID,
                                    SynchronizeID = mark.SynchronizeID,
                                    Title = mark.Title,
                                    Year = mark.Year
                                };
                                objRestoreDetail = new RESTORE_DATA_DETAIL
                                {
                                    ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                                    CREATED_DATE = DateTime.Now,
                                    END_DATE = null,
                                    IS_VALIDATE = 1,
                                    LAST_2DIGIT_NUMBER_SCHOOL = mark.Last2digitNumberSchool,
                                    ORDER_ID = 1,
                                    RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                                    RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                                    SCHOOL_ID = objRes.SCHOOL_ID,
                                    SQL_DELETE = JsonConvert.SerializeObject(objKeyMark),
                                    SQL_UNDO = JsonConvert.SerializeObject(objDelMark),
                                    TABLE_NAME = RestoreDataConstant.TABLE_MARK_RECORD
                                };

                                lstRestoreDetail.Add(objRestoreDetail);
                            }
                            oldObjecttmp.Append(strHK);
                            isCheckDelete = true;
                            lstPupilDeleteID.Add(listPupilProfile[i].PupilProfileID);
                        }
                    }
                }

                if (isCheckDelete)
                {
                    userDescriptionstmp = new StringBuilder();
                    string tmp = string.Empty;
                    tmp = oldObjecttmp != null ? oldObjecttmp.ToString().Substring(0, oldObjecttmp.Length - 2) : "";
                    objectID.Append(listPupilProfile[i].PupilProfileID.ToString());
                    descriptionObject.Append("Delete mark_record:" + listPupilProfile[i].PupilProfileID.ToString());
                    paramObject.Append(listPupilProfile[i].PupilProfileID.ToString());
                    userFunctions.Append("Sổ điểm");
                    userActions.Append(GlobalConstants.ACTION_DELETE);
                    userDescriptionstmp.Append("Xóa điểm HS " + listPupilProfile[i].FullName
                        + ", mã " + listPupilProfile[i].PupilCode
                        + ", Lớp " + classProfile.DisplayName
                        + "/" + subject.SubjectName
                        + "/Học kỳ " + SemesterID
                        + "/" + classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1)
                        + ". " + tmp);
                    if (i < size - 1)
                    {
                        oldObject.Append(tmp).Append(GlobalConstants.WILD_LOG);
                        descriptionObject.Append(GlobalConstants.WILD_LOG);
                        paramObject.Append(GlobalConstants.WILD_LOG);
                        objectID.Append(GlobalConstants.WILD_LOG);
                        userFunctions.Append(GlobalConstants.WILD_LOG);
                        userActions.Append(GlobalConstants.WILD_LOG);
                        userDescriptionstmp.Append(GlobalConstants.WILD_LOG);
                    }
                    userDescriptions.Append(userDescriptionstmp);
                    objRes.SHORT_DESCRIPTION += string.Format(" HS: {0}, mã: {1}", listPupilProfile[i].FullName, listPupilProfile[i].PupilCode);
                }
            }
            // end

            if (isMovedHistory)
            {
                // thực hiện nghiệp vụ
                MarkRecordHistoryBusiness.DeleteMarkRecordHistory(Global.UserAccountID, Global.SchoolID.Value, Global.AcademicYearID.Value, ClassID, SemesterID, null, SubjectID, lstPupilDeleteID);
                SummedUpRecordHistoryBusiness.DeleteSummedUpRecordHistory(Global.UserAccountID, Global.SchoolID.Value, Global.AcademicYearID.Value, ClassID, SemesterID, null, SubjectID, lstPupilDeleteID);
                // MarkRecordBusiness.Save();
                // end
            }
            else
            {
                //thực hiện nghiệp vụ
                MarkRecordBusiness.DeleteMarkRecord(Global.UserAccountID, Global.SchoolID.Value, Global.AcademicYearID.Value, ClassID, SemesterID, null, SubjectID, lstPupilDeleteID);
                List<SummedUpRecord> lstSummed = SummedUpRecordBusiness.DeleteSummedUpRecord(Global.UserAccountID, Global.SchoolID.Value, Global.AcademicYearID.Value, ClassID, SemesterID, null, SubjectID, lstPupilDeleteID);
                // MarkRecordBusiness.Save();
                //end
                if (lstSummed != null)
                {
                    List<RESTORE_DATA_DETAIL> lstResSummed = SummedUpRecordBusiness.BackUpSummedMark(lstSummed, objRes);
                    if (lstResSummed != null)
                    {
                        lstRestoreDetail.AddRange(lstResSummed);
                        if (lstRestoreDetail != null && lstRestoreDetail.Count > 0)
                        {
                            if (objRes.SHORT_DESCRIPTION.Length > 1000)
                            {
                                objRes.SHORT_DESCRIPTION = objRes.SHORT_DESCRIPTION.Substring(0, 1000) + "...";
                            }
                            RestoreDataBusiness.Insert(objRes);
                            RestoreDataBusiness.Save();
                            RestoreDataDetailBusiness.BulkInsert(lstRestoreDetail, ColumnMapping.Instance.RestoreDataDetail(), "RESTORE_DATA_DETAIL_ID");
                        }
                    }
                }
            }

            if (lstPupilDeleteID.Count > 0)
            {
                // Tạo dữ liệu ghi log         
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObject.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,""},
                    {Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectID.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionObject.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramObject.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActions.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFunctions.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptions.ToString()}
                };
                SetViewDataActionAudit(dicLog);
            }
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>

        public PartialViewResult Search()
        {
            return PartialView("_List");
        }

        public void SetViewData()
        {

            GlobalInfo GlobalInfo = new GlobalInfo();
            //Đổ dữ liệu list radio
            List<ViettelCheckboxList> lrSemester = new List<ViettelCheckboxList>();
            List<ViettelCheckboxList> lrEducationLevel = new List<ViettelCheckboxList>();
            List<ViettelCheckboxList> lrSubject = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio = new ViettelCheckboxList();
            ViewData[BookMarkRecordConstants.LIST_EDUCATIONLEVEL] = lrEducationLevel;
            ViewData[BookMarkRecordConstants.LIST_SUBJECT] = lrSubject;
            ViewData[BookMarkRecordConstants.LIST_CLASS] = new List<ClassProfile>();
            //TODO :hath8
            ViewData[BookMarkRecordConstants.LABEL_MES] = "";
            ViewData[BookMarkRecordConstants.LIST_MARKRECORD] = new List<BookMarkRecordViewModel>();

            ViewData[BookMarkRecordConstants.M] = null;
            ViewData[BookMarkRecordConstants.P] = null;
            ViewData[BookMarkRecordConstants.V] = null;

            ViewData[BookMarkRecordConstants.COMMENT] = null;

            //Học kỳ
            bool semester1 = false;
            bool semester2 = false;
            if (GlobalInfo.Semester.Value == 1)
                semester1 = true;
            else semester2 = true;
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = SystemParamsInFile.SEMESTER_I;
            viettelradio.cchecked = semester1;
            viettelradio.disabled = false;
            viettelradio.Value = 1;
            lrSemester.Add(viettelradio);
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = SystemParamsInFile.SEMESTER_II;
            viettelradio.cchecked = semester2;
            viettelradio.disabled = false;
            viettelradio.Value = 2;
            lrSemester.Add(viettelradio);
            ViewData[BookMarkRecordConstants.LIST_SEMESTER] = lrSemester;

            int i = 0;
            foreach (EducationLevel item in GlobalInfo.EducationLevels)
            {
                i++;
                bool checkEdu = false;
                if (i == 1) { checkEdu = true; }
                viettelradio = new ViettelCheckboxList();
                viettelradio.Label = item.Resolution;
                viettelradio.cchecked = checkEdu;
                viettelradio.disabled = false;
                viettelradio.Value = item.EducationLevelID;
                lrEducationLevel.Add(viettelradio);
            }
            ViewData[BookMarkRecordConstants.LIST_EDUCATIONLEVEL] = lrEducationLevel;

            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = GlobalInfo.EducationLevels.FirstOrDefault().EducationLevelID;
            dicClass["AcademicYearID"] = GlobalInfo.AcademicYearID;
            if (GlobalInfo.IsAdminSchoolRole == false && !GlobalInfo.IsViewAll)
            {
                dicClass["UserAccountID"] = GlobalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dicClass).OrderBy(o => o.DisplayName);
            if (lsClass.Count() != 0)
                ViewData[BookMarkRecordConstants.LIST_CLASS] = lsClass;
            ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "true";
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult ClickSemeter(int? idSemester, int? idEducationLevel, int? idClass, int? idSubject)
        {

            return PartialView("_List");
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadClass(int? idEducationLevel)
        {
            Nullable<Int32> nullableInt = null;
            if (idEducationLevel != nullableInt)
            {
                Session["idEducationLevel"] = idEducationLevel;
            }
            else
            {
                idEducationLevel = Convert.ToInt32(Session["idEducationLevel"] ?? 0);
            }
            SearchViewModel svm = new SearchViewModel();
            ViewData[BookMarkRecordConstants.LIST_CLASS] = new List<ClassProfile>();
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = Session["idEducationLevel"];
            dicClass["AcademicYearID"] = GlobalInfo.AcademicYearID;
            if (GlobalInfo.IsAdminSchoolRole == false && !GlobalInfo.IsViewAll)
            {
                dicClass["UserAccountID"] = GlobalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dicClass);
            if (lsClass.Count() != 0)
            {


                ViewData[BookMarkRecordConstants.LIST_CLASS] = lsClass.OrderBy(o => o.DisplayName).ToList();

                svm.EducationLevelID = Convert.ToInt32(Session["idEducationLevel"]);
                svm.ClassProfileID = lsClass.ToList().FirstOrDefault().ClassProfileID;
            }

            return PartialView("_ClassPanel", svm);
        }


        [ValidateAntiForgeryToken]
        //[CacheFilter(Duration = 60)]
        [CompressFilter(Order = 1)]
        public PartialViewResult AjaxLoadSubject(int? idClassProfile, int? idSemester)
        {
            if (idClassProfile == null) idClassProfile = 0;
            if (idSemester == null) idSemester = 0;

            SearchViewModel svm = new SearchViewModel();
            svm.ClassProfileID = idClassProfile;
            svm.SemesterID = idSemester;

            GlobalInfo GlobalInfo = new GlobalInfo();
            List<ViettelCheckboxList> lrSubject = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio = new ViettelCheckboxList();

            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            if (GlobalInfo.AcademicYearID.HasValue && GlobalInfo.SchoolID.HasValue && idClassProfile.HasValue && idSemester.HasValue)
            {
                if (GlobalInfo.Semester == Constants.GlobalConstants.FIRST_SEMESTER)
                {
                    lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(GlobalInfo.UserAccountID,
                        GlobalInfo.AcademicYearID.Value, GlobalInfo.SchoolID.Value,
                        idSemester.Value, idClassProfile.Value, GlobalInfo.IsViewAll)
                    .Where(o => o.SubjectCat.IsActive == true)
                    .Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE
                    && o.SectionPerWeekFirstSemester > 0
                    )
                    .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName)
                    .ToList();
                }
                if (GlobalInfo.Semester == Constants.GlobalConstants.SECOND_SEMESTER)
                {
                    lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(GlobalInfo.UserAccountID,
                        GlobalInfo.AcademicYearID.Value, GlobalInfo.SchoolID.Value,
                        idSemester.Value, idClassProfile.Value, GlobalInfo.IsViewAll)
                    .Where(o => o.SubjectCat.IsActive == true)
                    .Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE
                    && o.SectionPerWeekSecondSemester > 0
                    )
                    .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName)
                    .ToList();
                }
            }
            foreach (ClassSubject cs in lsClassSubject)
            {
                viettelradio = new ViettelCheckboxList();
                viettelradio.Label = cs.SubjectCat.DisplayName;
                viettelradio.cchecked = false;
                viettelradio.disabled = false;
                viettelradio.Value = cs.SubjectID;
                lrSubject.Add(viettelradio);
            }


            ViewData[BookMarkRecordConstants.LIST_SUBJECT] = lrSubject;
            if (lrSubject.Count != 0 && idSemester != 0)
            {
                string Semester = "";
                if (idSemester == 1)
                    Semester = SystemParamsInFile.SEMESTER_I;
                if (idSemester == 2)
                    Semester = SystemParamsInFile.SEMESTER_II;
                if (ClassProfileBusiness.Find(idClassProfile.Value) != null && SubjectCatBusiness.Find(lrSubject.FirstOrDefault().Value).DisplayName.ToUpper() != null)
                    ViewData[BookMarkRecordConstants.LABEL_MES] = Res.Get("BookMarkRecord_Label_Message").ToUpper() + " - " + SubjectCatBusiness.Find(lrSubject.FirstOrDefault().Value).DisplayName.ToUpper() + "-" + ClassProfileBusiness.Find(idClassProfile.Value).DisplayName.ToUpper() + "-" + Semester.ToUpper();
            }

            return PartialView("_SubjectPanel", svm);
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadGrid(int? idSubject, int? idSemester, int? idClassProfile)
        {
            ViewData[BookMarkRecordConstants.HAS_ERROR_DATA] = false;
            ViewData[BookMarkRecordConstants.LIST_IMPORTDATA] = null;
            ViewData[BookMarkRecordConstants.CLASS_12] = false;
            ViewData[BookMarkRecordConstants.SEMESTER_1] = true;
            ViewData[BookMarkRecordConstants.ERROR_BASIC_DATA] = false;
            ViewData[BookMarkRecordConstants.ERROR_IMPORT_MESSAGE] = "";
            ViewData[BookMarkRecordConstants.ERROR_IMPORT] = true;
            ViewData[BookMarkRecordConstants.COMMENT] = false;

            //Nếu là môn GDCD
            if (idSubject.HasValue)
            {
                //Kiem tra mon co phai GDCD
                SubjectCat subject = SubjectCatBusiness.Find(idSubject);
                ViewData[BookMarkRecordConstants.COMMENT] = subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE;

                if (idSemester == null)
                    idSemester = 0;

                ViewData[BookMarkRecordConstants.checkHK1] = idSemester.Value != 2;
                ViewData[BookMarkRecordConstants.checkHK2] = idSemester.Value == 2;

                GlobalInfo Global = new GlobalInfo();

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = Global.AcademicYearID;
                SearchInfo["SchoolID"] = Global.SchoolID;
                SearchInfo["SubjectID"] = idSubject;
                SearchInfo["Semester"] = idSemester;
                SearchInfo["ClassID"] = idClassProfile;

                AcademicYear AcademicYear = AcademicYearBusiness.Find(Global.AcademicYearID.Value);
                ViewData[Constants.GlobalConstants.ACADEMIC_YEAR] = AcademicYear;

                IDictionary<string, object> dicM = new Dictionary<string, object>();
                dicM["AcademicYearID"] = Global.AcademicYearID;
                dicM["SchoolID"] = Global.SchoolID;
                dicM["Semester"] = idSemester;
                //dicM["Year"] = AcademicYear.Year;

                SemeterDeclaration SemeterDeclaration = SemesterDeclarationBusiness.Search(dicM).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
                if (SemeterDeclaration == null)
                    throw new BusinessException("Validate_School_NotMark");
                List<MarkRecordBO> listMarkRecord = this.MarkRecordBusiness.GetMardRecordOfClass(SearchInfo).ToList();
                ViewData[BookMarkRecordConstants.M] = SemeterDeclaration.InterviewMark;
                ViewData[BookMarkRecordConstants.CoefficientM] = SemeterDeclaration.CoefficientInterview;
                ViewData[BookMarkRecordConstants.P] = SemeterDeclaration.CoefficientWriting;
                ViewData[BookMarkRecordConstants.CoefficientP] = SemeterDeclaration.CoefficientTwice;
                ViewData[BookMarkRecordConstants.V] = SemeterDeclaration.TwiceCoeffiecientMark;
                ViewData[BookMarkRecordConstants.CoefficientV] = SemeterDeclaration.WritingMark;
                ViewData[BookMarkRecordConstants.HK] = listMarkRecord.Where(o => o.Title != null && o.Title.Contains("HK")).ToList();
                ViewData[BookMarkRecordConstants.CoefficientHK] = SemeterDeclaration.CoefficientSemester;

                List<int?> listSemester = new List<int?>() { idSemester };
                if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        listSemester.Add(SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                    }
                }
                List<BookMarkRecordViewModel> lstBookMarkRecordViewModel = listMarkRecord.Select(o => new BookMarkRecordViewModel
                {
                    MarkRecordID = o.MarkRecordID,
                    Semester = idSemester,
                    PupilCode = o.PupilCode,
                    FullName = o.FullName,
                    Mark = o.Mark,
                    PupilID = o.PupilID,
                    Title = o.Title,
                    Status = o.Status,
                    ClassID = o.ClassID,
                    SubjectID = idSubject.Value,
                    SchoolID = o.SchoolID,
                    AcademicYearID = o.AcademicYearID,
                    Name = o.Name,
                    TimeStatus = o.EndDate,
                    OrderInClass = o.OrderInClass
                }).ToList();

                List<SummedUpRecordBO> lstSummedUpRecord = SummedUpRecordBusiness.All.Where(o => o.SchoolID == Global.SchoolID.Value
                    && o.AcademicYearID == Global.AcademicYearID.Value
                    && listSemester.Contains(o.Semester) && o.ClassID == idClassProfile
                    && o.SubjectID == idSubject && !o.PeriodID.HasValue)
                    .Select(o => new SummedUpRecordBO()
                    {
                        PupilID = o.PupilID,
                        ClassID = o.ClassID,
                        SubjectID = o.SubjectID,
                        Semester = o.Semester,
                        SummedUpMark = o.SummedUpMark,
                        Comment = o.Comment
                    })
                    .ToList();
                List<BookMarkRecordViewModel> List_BookMarkRecordViewModel = new List<BookMarkRecordViewModel>();

                string strMarkTitle = MarkRecordBusiness.GetLockMarkTitle(new GlobalInfo().SchoolID.Value, new GlobalInfo().AcademicYearID.Value, idClassProfile.Value, (int)idSemester, (int)idSubject.Value);
                ViewData[BookMarkRecordConstants.LIST_LOCK_MARK] = strMarkTitle.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();

                List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.GetListExemptedSubject(idClassProfile.Value, idSemester.Value).ToList();

                foreach (var item in lstBookMarkRecordViewModel)
                {
                    int PupilID = item.PupilID.Value;
                    List<BookMarkRecordViewModel> lstPupil = lstBookMarkRecordViewModel.Where(o => o.PupilID == item.PupilID).OrderBy(o => o.Name).ToList();
                    item.DiemM = new decimal?[SemeterDeclaration.InterviewMark + 1];
                    item.DiemP = new decimal?[SemeterDeclaration.WritingMark + 1];
                    item.DiemV = new decimal?[SemeterDeclaration.TwiceCoeffiecientMark + 1];

                    foreach (BookMarkRecordViewModel item1 in lstPupil)
                    {
                        if (item1.Title != null)
                        {
                            for (int m = 1; m <= SemeterDeclaration.InterviewMark; m++)
                                if (item1.Title == ("M" + m.ToString()))
                                    item.DiemM[m] = item1.Mark;

                            for (int p = 1; p <= SemeterDeclaration.WritingMark; p++)
                                if (item1.Title == ("P" + p.ToString()))
                                    item.DiemP[p] = item1.Mark;

                            for (int v = 1; v <= SemeterDeclaration.TwiceCoeffiecientMark; v++)
                                if (item1.Title == ("V" + v.ToString()))
                                    item.DiemV[v] = item1.Mark;

                            if (item1.Title == "HK")
                                item.KTHK = item1.Mark;
                        }
                        else
                        {
                            continue;
                        }
                    }

                    List<SummedUpRecordBO> lstsurbo = lstSummedUpRecord.Where(o => o.PupilID == item.PupilID
                                                                                && o.ClassID == item.ClassID
                                                                                && o.SubjectID == item.SubjectID).ToList();

                    if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        SummedUpRecordBO surbo1 = lstsurbo.FirstOrDefault(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST);
                        if (surbo1 != null)
                        {
                            item.TBM1 = surbo1.SummedUpMark;
                            item.Comment = surbo1.Comment;
                        }
                    }

                    if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        SummedUpRecordBO surbo2 = lstsurbo.FirstOrDefault(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                        if (surbo2 != null)
                        {
                            item.TBM2 = surbo2.SummedUpMark;
                            item.Comment = surbo2.Comment;
                        }

                        SummedUpRecordBO surbo3 = lstsurbo.FirstOrDefault(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                        if (surbo3 != null)
                        {
                            item.TBMCN = surbo3.SummedUpMark;
                            item.Comment = surbo3.Comment;
                        }
                    }

                    item.show = item.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                || item.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                || (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && item.TimeStatus.HasValue && item.TimeStatus.Value > AcademicYear.FirstSemesterEndDate)
                                || (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && item.TimeStatus.HasValue && item.TimeStatus.Value > AcademicYear.SecondSemesterEndDate);

                    item.IsExempted = lstExempted.Any(u => u.PupilID == item.PupilID && u.SubjectID == item.SubjectID);

                    if (!List_BookMarkRecordViewModel.Any(o => o.PupilID == item.PupilID))
                    {
                        List_BookMarkRecordViewModel.Add(item);
                    }
                }

                List_BookMarkRecordViewModel = List_BookMarkRecordViewModel.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.FullName).ToList();

                if (List_BookMarkRecordViewModel.Count != 0)
                {
                    ViewData[BookMarkRecordConstants.LIST_MARKRECORD] = List_BookMarkRecordViewModel;
                }

                if (idSemester != 0)
                {
                    string Semester = "";

                    if (idSemester == 1)
                    {
                        Semester = SystemParamsInFile.SEMESTER_I;

                    }

                    if (idSemester == 2)
                    {
                        Semester = SystemParamsInFile.SEMESTER_II;
                    }

                    ClassProfile ClassProfile = ClassProfileBusiness.Find(idClassProfile.Value);
                    if (ClassProfile != null && subject != null)
                    {
                        ViewData[BookMarkRecordConstants.LABEL_MES] = Res.Get("BookMarkRecord_Label_Message").ToUpper() + " - " + subject.DisplayName.ToUpper() + "-" + ClassProfile.DisplayName.ToUpper() + "-" + Semester.ToUpper();
                    }
                }

                //Kiểm tra quyền của người dùng đăng nhập: btnImport, btnExport, btnSave, btnDelete, chkEntryMark
                bool permission = UtilsBusiness.HasSubjectTeacherPermission(Global.UserAccountID, idClassProfile.Value, idSubject.Value, idSemester.Value);
                if (Global.IsCurrentYear == false)
                {
                    ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "true";
                }
                else
                {
                    if (!permission)
                    {
                        ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "true";
                    }
                    else
                    {

                        if (Global.IsAdminSchoolRole == true)
                        {
                            ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "false";
                        }
                        else
                        {
                            bool timeSemester = idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                                  ? (AcademicYear != null && AcademicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= AcademicYear.FirstSemesterEndDate)
                                                                                  : (AcademicYear != null && AcademicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= AcademicYear.SecondSemesterEndDate);
                            if (timeSemester && !strMarkTitle.Contains("LHK"))
                            {
                                ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "false";
                            }
                            else
                            {
                                ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "true";
                            }
                        }
                    }
                }


                // end
                /* bỏ code
                if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    if (Global.HasSubjectTeacherPermission(idClassProfile.Value, idSubject.Value) == false || AcademicYearBusiness.Find(Global.AcademicYearID).FirstSemesterEndDate < DateTime.Now)
                    {
                        ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "true";
                    }
                    else
                    {
                        ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "false";
                    }
                }
                if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    if (Global.HasSubjectTeacherPermission(idClassProfile.Value, idSubject.Value) == false || AcademicYearBusiness.Find(Global.AcademicYearID).SecondSemesterEndDate < DateTime.Now)
                    {
                        ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "true";
                    }
                    else
                    {
                        ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "false";

                    }
                } */
            }

            return PartialView("_List");
        }

        [ValidateAntiForgeryToken]

        public PartialViewResult AjaxLoadGridDemo(int? idSubject, int? idSemester, int? idClassProfile)
        {
            ViewData[BookMarkRecordConstants.HAS_ERROR_DATA] = false;
            ViewData[BookMarkRecordConstants.LIST_IMPORTDATA] = null;
            ViewData[BookMarkRecordConstants.CLASS_12] = false;
            ViewData[BookMarkRecordConstants.SEMESTER_1] = true;
            ViewData[BookMarkRecordConstants.ERROR_BASIC_DATA] = false;
            ViewData[BookMarkRecordConstants.ERROR_IMPORT_MESSAGE] = "";
            ViewData[BookMarkRecordConstants.ERROR_IMPORT] = true;
            ViewData[BookMarkRecordConstants.COMMENT] = false;
            ClassProfile ClassProfile = null;
            SubjectCat subject = null;
            SchoolProfile objSP = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            int partitionId = UtilsBusiness.GetPartionId(objSP.SchoolProfileID);
            //Nếu là môn GDCD
            if (idSubject.HasValue)
            {
                //Kiem tra mon co phai GDCD
                subject = SubjectCatBusiness.Find(idSubject);
                ViewData[BookMarkRecordConstants.COMMENT] = subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE;
                if (idSemester == null)
                    idSemester = 0;

                ViewData[BookMarkRecordConstants.checkHK1] = idSemester.Value != 2;
                ViewData[BookMarkRecordConstants.checkHK2] = idSemester.Value == 2;
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
                SearchInfo["SchoolID"] = _globalInfo.SchoolID;
                SearchInfo["SubjectID"] = idSubject;
                SearchInfo["Semester"] = idSemester;
                SearchInfo["ClassID"] = idClassProfile;

                AcademicYear AcademicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
                ViewData[Constants.GlobalConstants.ACADEMIC_YEAR] = AcademicYear;

                IDictionary<string, object> dicM = new Dictionary<string, object>();
                dicM["AcademicYearID"] = _globalInfo.AcademicYearID;
                dicM["SchoolID"] = _globalInfo.SchoolID;
                dicM["Semester"] = idSemester;
                //dicM["Year"] = AcademicYear.Year;

                SemeterDeclaration SemeterDeclaration = SemesterDeclarationBusiness.Search(dicM).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
                if (SemeterDeclaration == null)
                    throw new BusinessException("Validate_School_NotMark");

                //lay danh sac hoc sinh theo cau hinh
                List<int> lstPupilID = new List<int>();
                IDictionary<string, object> dicSearchPupil = new Dictionary<string, object>()
                {
                    {"AcademicYear",_globalInfo.AcademicYearID},
                    {"SchoolID",_globalInfo.SchoolID},
                    {"ClassID",idClassProfile}
                };
                bool isNotShowPupil = AcademicYear.IsShowPupil.HasValue && AcademicYear.IsShowPupil.Value;
                List<PupilOfClass> lstPOC = PupilOfClassBusiness.Search(dicSearchPupil).AddPupilStatus(isNotShowPupil).ToList();
                lstPupilID = lstPOC.Select(p => p.PupilID).Distinct().ToList();

                var listMark = this.MarkRecordBusiness.GetMardRecordOfClassNew(SearchInfo);
                if (listMark != null)
                {
                    listMark = listMark.Where(p => lstPupilID.Contains(p.PUPIL_ID));
                }
                //lay ra danh logchange cua hoc sinh theo tung con diem
                List<BookRecordLogChangeBO> listLogChange = new List<BookRecordLogChangeBO>();

                if (UtilsBusiness.IsMoveHistory(AcademicYear))
                {
                    listLogChange = (from mr in MarkRecordHistoryBusiness.All
                                     where mr.AcademicYearID == _globalInfo.AcademicYearID
                                     && mr.SchoolID == _globalInfo.SchoolID
                                     && mr.ClassID == idClassProfile
                                     && mr.SubjectID == idSubject
                                     && mr.Semester == idSemester
                                     && mr.Last2digitNumberSchool == partitionId
                                     && lstPupilID.Contains(mr.PupilID)
                                     select new BookRecordLogChangeBO
                                     {
                                         PupilID = mr.PupilID,
                                         Title = mr.Title,
                                         LogChange = mr.LogChange,
                                         ModifiedDate = mr.ModifiedDate,
                                         CreateDate = mr.CreatedDate,
                                         UpdateDate = mr.ModifiedDate.HasValue ? mr.ModifiedDate : mr.CreatedDate
                                     }).Where(o => o.UpdateDate != null).ToList();
                }
                else
                {

                    listLogChange = (from mr in MarkRecordBusiness.All
                                     where mr.AcademicYearID == _globalInfo.AcademicYearID
                                     && mr.SchoolID == _globalInfo.SchoolID
                                     && mr.ClassID == idClassProfile
                                     && mr.SubjectID == idSubject
                                     && mr.Semester == idSemester
                                     && mr.Last2digitNumberSchool == partitionId
                                     && lstPupilID.Contains(mr.PupilID)
                                     select new BookRecordLogChangeBO
                                     {
                                         PupilID = mr.PupilID,
                                         Title = mr.Title,
                                         LogChange = mr.LogChange,
                                         ModifiedDate = mr.ModifiedDate,
                                         CreateDate = mr.CreatedDate,
                                         UpdateDate = mr.ModifiedDate.HasValue ? mr.ModifiedDate : mr.CreatedDate
                                     }).Where(o => o.UpdateDate != null).ToList();
                }
                List<int?> lstEmployeeID = listLogChange.Select(p => p.LogChange).Distinct().ToList();
                //viethd4: lay ra cac cap nhat gan nhat
                var lastUpdate = listLogChange.OrderByDescending(o => o.UpdateDate).FirstOrDefault();
                List<MarkRecordBO> lstLastMarkRecordID = new List<MarkRecordBO>();

                if (lastUpdate != null)
                {
                    string lastUpdateTime = lastUpdate.UpdateDate != null ? lastUpdate.UpdateDate.Value.ToString("MM/dd/yyyy hh:mm") : null;
                    lstLastMarkRecordID = listLogChange.Where(o => lastUpdateTime != null && o.UpdateDate != null ? o.UpdateDate.Value.ToString("MM/dd/yyyy hh:mm") == lastUpdateTime : false).Select(o =>
                        new MarkRecordBO
                        {
                            PupilID = o.PupilID,
                            Title = o.Title
                        }).ToList();

                }
                ViewData[BookMarkRecordConstants.LAST_UPDATE_MARK_RECORD] = lstLastMarkRecordID;

                List<Employee> lstEmployeeChange = new List<Employee>();
                if (lstEmployeeID.Count > 0)
                {
                    lstEmployeeChange = (from e in EmployeeBusiness.All
                                         where e.SchoolID == _globalInfo.SchoolID
                                         && lstEmployeeID.Contains(e.EmployeeID)
                                         select e).ToList();
                }
                Employee objEmployee = null;
                //viethd4: lay user va time cap nhat sau cung

                string tmpStr = String.Empty;
                string strLastUpdate = String.Empty;
                if (lastUpdate != null)
                {
                    string lastUserName = String.Empty;
                    if (lastUpdate.LogChange == 0)
                    {
                        lastUserName = "Quản trị trường";
                    }
                    else
                    {
                        Employee lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdate.LogChange).FirstOrDefault();
                        if (lastUser != null)
                        {
                            lastUserName = lastUser.FullName;
                        }
                    }

                    tmpStr = string.Format("<span style='color:#d56900 '>{0}h{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                        lastUpdate.UpdateDate.Value.Hour.ToString(),
                        lastUpdate.UpdateDate.Value.Minute.ToString("D2"),
                        lastUpdate.UpdateDate,
                        !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                        lastUserName});

                    strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);
                }

                ViewData[BookMarkRecordConstants.LAST_UPDATE_STRING] = strLastUpdate;

                //Kiem tra co mon tang cuong hay khong
                int subjectIdIncrease = 0;// id mon tang cuong
                List<ClassSubject> listClassSubjectObj = (from cs in ClassSubjectBusiness.All
                                                          join poc in PupilOfClassBusiness.All on cs.ClassID equals poc.ClassID
                                                          where cs.ClassID == idClassProfile
                                                          && poc.AcademicYearID == _globalInfo.AcademicYearID
                                                          && poc.ClassID == idClassProfile
                                                          && cs.Last2digitNumberSchool == partitionId
                                                          select cs).ToList();

                ClassSubject classSubjectObj = listClassSubjectObj.Where(p => p.SubjectID == idSubject).FirstOrDefault();
                if (classSubjectObj != null)
                {
                    subjectIdIncrease = classSubjectObj.SubjectIDIncrease.HasValue ? classSubjectObj.SubjectIDIncrease.Value : 0;
                }
                //lay diem trung binh mon tang cuong
                List<MarkRecordNewBO> listMarkIncrease = new List<MarkRecordNewBO>();
                ClassSubject ClassSubjectIncrese = null;
                if (subjectIdIncrease > 0)
                {
                    SearchInfo["SubjectID"] = subjectIdIncrease;
                    //kiem tra so tiet tren tuan cua hom hoc tang cuong
                    ClassSubjectIncrese = listClassSubjectObj.Where(p => p.SubjectID == subjectIdIncrease &&
                            (
                                (idSemester == Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST && p.SectionPerWeekFirstSemester > 0) ||
                                (idSemester == Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND && p.SectionPerWeekSecondSemester > 0)
                            )
                        ).FirstOrDefault();

                    if (ClassSubjectIncrese != null)
                    {
                        listMarkIncrease = this.MarkRecordBusiness.GetMardRecordOfClassNew(SearchInfo).ToList();
                    }
                }

                List<MarkRecordNewBO> listMarkRecord = listMark != null && listMark.Count() > 0 ? listMark.ToList() : new List<MarkRecordNewBO>();
                List<MarkRecordNewBO> listMarkRecordIncrease = listMarkIncrease != null && listMarkIncrease.Count() > 0 ? listMarkIncrease.ToList() : new List<MarkRecordNewBO>();

                //List<MarkRecordBO> listMarkRecord = this.MarkRecordBusiness.GetMardRecordOfClass(SearchInfo).ToList();
                ViewData[BookMarkRecordConstants.M] = SemeterDeclaration.InterviewMark;
                ViewData[BookMarkRecordConstants.P] = SemeterDeclaration.WritingMark;
                ViewData[BookMarkRecordConstants.V] = SemeterDeclaration.TwiceCoeffiecientMark;
                // AnhVD - He so cac con diem
                ViewData[BookMarkRecordConstants.CoefficientM] = SemeterDeclaration.CoefficientInterview;
                ViewData[BookMarkRecordConstants.CoefficientP] = SemeterDeclaration.CoefficientWriting;
                ViewData[BookMarkRecordConstants.CoefficientV] = SemeterDeclaration.CoefficientTwice;
                ViewData[BookMarkRecordConstants.CoefficientHK] = SemeterDeclaration.CoefficientSemester;
                // End 20131212
                //Bien check so con diem toi thieu
                bool isMinSemester = false;
                if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    isMinSemester = classSubjectObj != null && ((classSubjectObj.MMinFirstSemester.HasValue && classSubjectObj.MMinFirstSemester.Value > 0)
                                       || classSubjectObj.PMinFirstSemester.HasValue && classSubjectObj.PMinFirstSemester.Value > 0
                                       || classSubjectObj.VMinFirstSemester.HasValue && classSubjectObj.VMinFirstSemester.Value > 0);
                    ViewData[BookMarkRecordConstants.MMIN] = classSubjectObj != null && classSubjectObj.MMinFirstSemester.HasValue ? classSubjectObj.MMinFirstSemester.Value : 0;
                    ViewData[BookMarkRecordConstants.PMIN] = classSubjectObj != null && classSubjectObj.PMinFirstSemester.HasValue ? classSubjectObj.PMinFirstSemester.Value : 0;
                    ViewData[BookMarkRecordConstants.VMIN] = classSubjectObj != null && classSubjectObj.VMinFirstSemester.HasValue ? classSubjectObj.VMinFirstSemester.Value : 0;
                }
                else
                {
                    isMinSemester = classSubjectObj != null && ((classSubjectObj.MMinSecondSemester.HasValue && classSubjectObj.MMinSecondSemester.Value > 0)
                                       || classSubjectObj.PMinSecondSemester.HasValue && classSubjectObj.PMinSecondSemester.Value > 0
                                       || classSubjectObj.VMinSecondSemester.HasValue && classSubjectObj.VMinSecondSemester.Value > 0);
                    ViewData[BookMarkRecordConstants.MMIN] = classSubjectObj != null && classSubjectObj.MMinSecondSemester.HasValue ? classSubjectObj.MMinSecondSemester.Value : 0;
                    ViewData[BookMarkRecordConstants.PMIN] = classSubjectObj != null && classSubjectObj.PMinSecondSemester.HasValue ? classSubjectObj.PMinSecondSemester.Value : 0;
                    ViewData[BookMarkRecordConstants.VMIN] = classSubjectObj != null && classSubjectObj.VMinSecondSemester.HasValue ? classSubjectObj.VMinSecondSemester.Value : 0;
                }
                ViewData[BookMarkRecordConstants.ISMINSEMESTER] = isMinSemester;
                List<int?> listSemester = new List<int?>() { idSemester };
                if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        listSemester.Add(SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                    }
                }
                List<BookMarkRecordViewModel> lstBookMarkRecordViewModel = listMarkRecord.Select(o => new BookMarkRecordViewModel
                {
                    MarkRecordID = 0,
                    Semester = idSemester,
                    PupilCode = "",
                    FullName = o.FULL_NAME,
                    PupilID = o.PUPIL_ID,
                    Status = o.STATUS,
                    ClassID = idClassProfile,
                    SubjectID = idSubject.Value,
                    SchoolID = _globalInfo.SchoolID.Value,
                    AcademicYearID = _globalInfo.AcademicYearID.Value,
                    Name = o.S_NAME,
                    TimeStatus = o.END_DATE,
                    OrderInClass = o.ORDER_IN_CLASS,
                    Comment = o.S_COMMENT,
                    TBM1 = o.HK1,
                    TBM2 = o.HK2,
                    TBMCN = o.CN
                }).ToList();

                List<SummedUpRecordBO> lstSummedUpRecord = new List<SummedUpRecordBO>();
                foreach (int semester in listSemester)
                {
                    List<SummedUpRecordBO> lstSummedUpRecordOne = new List<SummedUpRecordBO>();
                    if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        lstSummedUpRecordOne = listMarkRecord.Where(u => u.HK1 != null).Select(o => new SummedUpRecordBO()
                        {
                            PupilID = o.PUPIL_ID,
                            ClassID = idClassProfile.Value,
                            SubjectID = idSubject,
                            Semester = idSemester,
                            SummedUpMark = o.HK1,
                            Comment = o.S_COMMENT,
                        }).ToList();
                    }
                    if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        lstSummedUpRecordOne = listMarkRecord.Where(u => u.HK2 != null).
                            Select(o => new SummedUpRecordBO()
                            {
                                PupilID = o.PUPIL_ID,
                                ClassID = idClassProfile.Value,
                                SubjectID = idSubject,
                                Semester = idSemester,
                                SummedUpMark = o.HK2,
                                Comment = o.S_COMMENT,
                            }).ToList();
                    }
                    if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                    {
                        lstSummedUpRecordOne = listMarkRecord.Where(u => u.CN != null).
                            Select(o => new SummedUpRecordBO()
                            {
                                PupilID = o.PUPIL_ID,
                                ClassID = idClassProfile.Value,
                                SubjectID = idSubject,
                                Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL,
                                SummedUpMark = o.CN,
                                Comment = o.S_COMMENT,
                            }).ToList();
                    }
                    lstSummedUpRecord.AddRange(lstSummedUpRecordOne);
                }

                List<SummedUpRecordBO> lstSummedUpRecordIncrease = new List<SummedUpRecordBO>();
                if (subjectIdIncrease > 0 && listMarkRecordIncrease != null && listMarkRecordIncrease.Count > 0)
                {
                    foreach (int semester in listSemester)
                    {
                        List<SummedUpRecordBO> lstSummedUpRecordOne = new List<SummedUpRecordBO>();
                        if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            lstSummedUpRecordOne = listMarkRecordIncrease.Where(u => u.HK1 != null).Select(o => new SummedUpRecordBO()
                            {
                                PupilID = o.PUPIL_ID,
                                ClassID = idClassProfile.Value,
                                SubjectID = subjectIdIncrease,
                                Semester = idSemester,
                                SummedUpMark = o.HK1,
                                Comment = o.S_COMMENT,
                            }).ToList();
                        }
                        if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            lstSummedUpRecordOne = listMarkRecordIncrease.Where(u => u.HK2 != null).
                                Select(o => new SummedUpRecordBO()
                                {
                                    PupilID = o.PUPIL_ID,
                                    ClassID = idClassProfile.Value,
                                    SubjectID = subjectIdIncrease,
                                    Semester = idSemester,
                                    SummedUpMark = o.HK2,
                                    Comment = o.S_COMMENT,
                                }).ToList();
                        }
                        if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                        {
                            lstSummedUpRecordOne = listMarkRecordIncrease.Where(u => u.CN != null).
                                Select(o => new SummedUpRecordBO()
                                {
                                    PupilID = o.PUPIL_ID,
                                    ClassID = idClassProfile.Value,
                                    SubjectID = subjectIdIncrease,
                                    Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL,
                                    SummedUpMark = o.CN,
                                    Comment = o.S_COMMENT,
                                }).ToList();
                        }
                        lstSummedUpRecordIncrease.AddRange(lstSummedUpRecordOne);
                    }
                }

                //List<SummedUpRecordBO> lstSummedUpRecord = SummedUpRecordBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID.Value
                //    && o.AcademicYearID == _globalInfo.AcademicYearID.Value
                //    && listSemester.Contains(o.Semester) && o.ClassID == idClassProfile
                //    && o.SubjectID == idSubject && !o.PeriodID.HasValue)
                //    .Select(o => new SummedUpRecordBO()
                //    {
                //        PupilID = o.PupilID,
                //        ClassID = o.ClassID,
                //        SubjectID = o.SubjectID,
                //        Semester = o.Semester,
                //        SummedUpMark = o.SummedUpMark,
                //        Comment = o.Comment
                //    })
                //    .ToList();
                List<BookMarkRecordViewModel> List_BookMarkRecordViewModel = new List<BookMarkRecordViewModel>();

                string strMarkTitle = MarkRecordBusiness.GetLockMarkTitle(new GlobalInfo().SchoolID.Value, new GlobalInfo().AcademicYearID.Value, idClassProfile.Value, (int)idSemester, (int)idSubject.Value);
                ViewData[BookMarkRecordConstants.LIST_LOCK_MARK] = strMarkTitle.Split(',').Where(u => !string.IsNullOrEmpty(u)).ToList();

                List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.GetListExemptedSubject(idClassProfile.Value, idSemester.Value).ToList();

                foreach (var item in lstBookMarkRecordViewModel)
                {
                    int PupilID = item.PupilID.Value;
                    var PupilData = listMarkRecord.Where(o => o.PUPIL_ID == item.PupilID).FirstOrDefault();
                    item.DiemM = new decimal?[SemeterDeclaration.InterviewMark + 1];
                    item.DiemP = new decimal?[SemeterDeclaration.WritingMark + 1];
                    item.DiemV = new decimal?[SemeterDeclaration.TwiceCoeffiecientMark + 1];
                    item.LogChangeM = new string[SemeterDeclaration.InterviewMark + 1];
                    item.LogChangeP = new string[SemeterDeclaration.WritingMark + 1];
                    item.LogChangeV = new string[SemeterDeclaration.TwiceCoeffiecientMark + 1];

                    //lay diem
                    for (int m = 1; m <= SemeterDeclaration.InterviewMark; m++)
                    {
                        objEmployee = null;
                        object Mark = Utils.Utils.GetValueByProperty(PupilData, "M" + m.ToString());
                        var objLogChange = listLogChange.Where(p => p.PupilID == PupilID && p.Title.Equals("M" + m.ToString())).FirstOrDefault();
                        if (objLogChange != null && objLogChange.LogChange > 0)
                        {
                            objEmployee = lstEmployeeChange.Where(p => p.EmployeeID == objLogChange.LogChange).FirstOrDefault();
                        }
                        item.LogChangeM[m] = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                        if (Mark != null)
                            item.DiemM[m] = decimal.Parse(Mark.ToString());
                    }
                    for (int p = 1; p <= SemeterDeclaration.WritingMark; p++)
                    {
                        objEmployee = null;
                        object Mark = Utils.Utils.GetValueByProperty(PupilData, "P" + p.ToString());
                        var objLogChange = listLogChange.Where(m => m.PupilID == PupilID && m.Title.Equals("P" + p.ToString())).FirstOrDefault();
                        if (objLogChange != null)
                        {
                            objEmployee = lstEmployeeChange.Where(e => e.EmployeeID == objLogChange.LogChange).FirstOrDefault();
                        }
                        if (Mark != null)
                            item.DiemP[p] = decimal.Parse(Mark.ToString());
                        item.LogChangeP[p] = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                    }

                    for (int v = 1; v <= SemeterDeclaration.TwiceCoeffiecientMark; v++)
                    {
                        objEmployee = null;
                        object Mark = Utils.Utils.GetValueByProperty(PupilData, "V" + v.ToString());
                        var objLogChange = listLogChange.Where(p => p.PupilID == PupilID && p.Title.Equals("V" + v.ToString())).FirstOrDefault();
                        if (objLogChange != null)
                        {
                            objEmployee = lstEmployeeChange.Where(p => p.EmployeeID == objLogChange.LogChange).FirstOrDefault();
                        }
                        if (Mark != null)
                            item.DiemV[v] = decimal.Parse(Mark.ToString());
                        item.LogChangeV[v] = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                    }
                    objEmployee = null;
                    object MarkHK = Utils.Utils.GetValueByProperty(PupilData, "HK");
                    var objLogChangeHK = listLogChange.Where(p => p.PupilID == PupilID && p.Title.Equals("HK")).FirstOrDefault();
                    if (objLogChangeHK != null)
                    {
                        objEmployee = lstEmployeeChange.Where(p => p.EmployeeID == objLogChangeHK.LogChange).FirstOrDefault();
                    }
                    if (MarkHK != null)
                        item.KTHK = decimal.Parse(MarkHK.ToString());
                    item.LogChangeHK = objLogChangeHK != null ? "Cập nhật " + (objLogChangeHK.ModifiedDate.HasValue ? objLogChangeHK.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChangeHK.CreateDate != null ? objLogChangeHK.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChangeHK.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";

                    List<SummedUpRecordBO> lstsurbo = lstSummedUpRecord.Where(o => o.PupilID == item.PupilID
                                                                                && o.ClassID == item.ClassID
                                                                                && o.SubjectID == item.SubjectID).ToList();

                    List<SummedUpRecordBO> lstsurboIncrease = lstSummedUpRecordIncrease.Where(o => o.PupilID == item.PupilID
                                                                                && o.ClassID == item.ClassID
                                                                                && o.SubjectID == subjectIdIncrease).ToList();

                    if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        SummedUpRecordBO surbo1 = lstsurbo.FirstOrDefault(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST);
                        SummedUpRecordBO surbo1Increase = lstsurboIncrease.FirstOrDefault(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST);
                        if (surbo1 != null)
                        {
                            item.TBM1 = surbo1.SummedUpMark;
                            item.Comment = surbo1.Comment;
                            if (surbo1.SummedUpMark.HasValue && subjectIdIncrease > 0)
                            {
                                item.TBM1 = surbo1.SummedUpMark.Value;
                            }
                        }
                        if (surbo1Increase != null && surbo1Increase.SummedUpMark.HasValue)
                        {
                            item.TBMTC = surbo1Increase.SummedUpMark.Value;
                            //if (surbo1 != null && surbo1.SummedUpMark.HasValue)
                            //{
                            //    item.TBM1 = this.getTBMTC(surbo1Increase.SummedUpMark.Value, surbo1.SummedUpMark.Value);
                            //}
                        }
                    }

                    if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        SummedUpRecordBO surbo2 = lstsurbo.FirstOrDefault(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                        SummedUpRecordBO surbo2Increase = lstsurboIncrease.FirstOrDefault(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                        if (surbo2 != null)
                        {
                            item.TBM2 = surbo2.SummedUpMark;
                            item.Comment = surbo2.Comment;
                            if (surbo2.SummedUpMark.HasValue && subjectIdIncrease > 0)
                            {
                                item.TBM2 = surbo2.SummedUpMark.Value;
                            }
                        }
                        if (surbo2Increase != null && surbo2Increase.SummedUpMark.HasValue)
                        {
                            item.TBMTC = surbo2Increase.SummedUpMark.Value;
                            //if (surbo2 != null && surbo2.SummedUpMark.HasValue)
                            //{
                            //    item.TBM2 = this.getTBMTC(surbo2Increase.SummedUpMark.Value, surbo2.SummedUpMark.Value);
                            //}
                        }

                        SummedUpRecordBO surbo3 = lstsurbo.FirstOrDefault(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                        if (surbo3 != null)
                        {
                            item.TBMCN = surbo3.SummedUpMark;
                            //item.Comment = surbo3.Comment;
                        }
                    }

                    //Cat Comment
                    if (item.Comment != null && item.Comment != "")
                    {
                        string Comment = item.Comment + "|";
                        string[] CmSemester = Comment.Split('|');
                        if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            item.Comment = CmSemester[0];
                        }
                        else if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            item.Comment = CmSemester[1];
                        }
                    }
                    DateTime? endSemester = item.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? AcademicYear.FirstSemesterEndDate : AcademicYear.SecondSemesterEndDate;
                    DateTime? endDate = item.TimeStatus;

                    bool pupilOtherStatus = item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS || (item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL) || (item.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF);

                    //item.show = item.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                    //            || item.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                    //            || (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && item.TimeStatus.HasValue && item.TimeStatus.Value > AcademicYear.FirstSemesterEndDate && pupilOtherStatus)
                    //            || (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && !pupilOtherStatus);

                    item.show = item.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                        || item.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                        || (item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                        || (item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value)
                                        || (item.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && endSemester.HasValue && endDate.HasValue && endDate.Value >= endSemester.Value);

                    item.IsExempted = lstExempted.Any(u => u.PupilID == item.PupilID && u.SubjectID == item.SubjectID);

                    if (!List_BookMarkRecordViewModel.Any(o => o.PupilID == item.PupilID))
                    {
                        List_BookMarkRecordViewModel.Add(item);
                    }
                }

                List_BookMarkRecordViewModel = List_BookMarkRecordViewModel.OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(u => u.FullName).ToList();

                if (List_BookMarkRecordViewModel.Count != 0)
                {
                    ViewData[BookMarkRecordConstants.LIST_MARKRECORD] = List_BookMarkRecordViewModel;
                }

                if (idSemester != 0)
                {
                    string Semester = "";

                    if (idSemester == 1)
                    {
                        Semester = SystemParamsInFile.SEMESTER_I;

                    }

                    if (idSemester == 2)
                    {
                        Semester = SystemParamsInFile.SEMESTER_II;
                    }

                    ClassProfile = ClassProfileBusiness.Find(idClassProfile.Value);
                    if (ClassProfile != null && subject != null)
                    {
                        ViewData[BookMarkRecordConstants.LABEL_MES] = Res.Get("BookMarkRecord_Label_Message").ToUpper() + " - " + subject.DisplayName.ToUpper() + "-" + ClassProfile.DisplayName.ToUpper() + "-" + Semester.ToUpper();
                    }
                }

                //Kiểm tra quyền của người dùng đăng nhập: btnImport, btnExport, btnSave, btnDelete, chkEntryMark
                bool permission = UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, idClassProfile.Value, idSubject.Value, idSemester.Value);
                if (_globalInfo.IsCurrentYear == false)
                {
                    ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "true";
                }
                else
                {
                    if (!permission)
                    {
                        ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "true";
                    }
                    else
                    {

                        if (_globalInfo.IsAdminSchoolRole == true)
                        {
                            ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "false";
                        }
                        else
                        {
                            bool timeSemester = idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                                  ? (AcademicYear != null && AcademicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= AcademicYear.FirstSemesterEndDate)
                                                                                  : (AcademicYear != null && AcademicYear.SecondSemesterStartDate <= DateTime.Now && DateTime.Now <= AcademicYear.SecondSemesterEndDate);
                            if (timeSemester && !strMarkTitle.Contains("LHK"))
                            {
                                ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "false";
                            }
                            else
                            {
                                ViewData[BookMarkRecordConstants.CHECK_ENTRY] = "true";
                            }
                        }
                    }
                }

                //Kiem tra mon hoc co tang cuong khogn
                // ClassSubject classSubjectObj = ClassSubjectBusiness.All.Where(p => p.SubjectID == subject.SubjectCatID && p.ClassID == idClassProfile.Value).FirstOrDefault();
                ViewData[BookMarkRecordConstants.SUBJECT_ID_INCREASE] = (classSubjectObj != null && listMarkIncrease != null && listMarkIncrease.Count > 0) ? classSubjectObj.SubjectIDIncrease : 0;

            }

            //Kiem tra khoa nhap lieu
            LockInputSupervisingDept objLockInput = this.GetLockTitleBySupervisingDept(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value);
            bool isLockInput = false;
            if (objLockInput != null && !string.IsNullOrEmpty(objLockInput.LockTitle))
            {
                isLockInput = objLockInput.LockTitle.Contains("HK" + idSemester);
            }
            ViewData[BookMarkRecordConstants.IS_LOCK_INPUT] = isLockInput;
            // Tạo dữ liệu ghi log
            SetViewDataActionAudit(String.Empty, String.Empty, idClassProfile.ToString(), "View mark_record class_id: " + idClassProfile + ", subject_id: " + idSubject + ", semester: " + idSemester, "class_id: " + idClassProfile + ", subject_id: " + idSubject + ", semester: " + idSemester, "Sổ điểm", GlobalConstants.ACTION_VIEW, "Xem sổ điểm lớp " + ClassProfile.DisplayName + " học kì " + idSemester + " môn học " + subject.SubjectName);
            ViewData[BookMarkRecordConstants.SUBJECT_ID] = idSubject;
            ViewData[BookMarkRecordConstants.TITLE_STRING] = String.Format("sổ điểm môn <span style='color:#d56900 '>{0}</span> học kỳ <span style='color:#d56900 '>{1}</span>, lớp <span style='color:#d56900 '>{2}</span>",
                subject != null ? subject.DisplayName : String.Empty, idSemester.ToString(), ClassProfile != null ? ClassProfile.DisplayName : String.Empty);
            if (objSP.TrainingTypeID == 3)//truong GDTX
            {
                return PartialView("_ListNewGDTX");
            }
            else
            {
                return PartialView("_ListNew");
            }
        }


        ///[HttpPost]
        [ActionAudit]
        public FileResult ExportFile()
        {
            int AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            int SchoolID = new GlobalInfo().SchoolID.Value;
            int ClassID = Business.Common.Utils.GetInt(Request["classid"]);
            int SubjectID = Business.Common.Utils.GetShort(Request["rptSubject"]);
            int SemesterID = Business.Common.Utils.GetByte(Request["rptSemester"]);
            int PeriodID = 0;
            StringBuilder descriptionObject = new StringBuilder();
            StringBuilder paramObject = new StringBuilder();
            StringBuilder userFunctions = new StringBuilder();
            StringBuilder userActions = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["HidClassProfileID"] = ClassID;
            dic["rptSubject"] = SubjectID;
            dic["rptSemester"] = SemesterID;

            //dua tat ca thong tin tu form vao dictionary
            String filename = "";

            // Tạo dữ liệu ghi log
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
            //SubjectCat subject = SubjectCatBusiness.Find(SubjectID);
            SubjectCatBO subjectCatBO = this.getClassSubject(SubjectID, AcademicYearID, ClassID, _globalInfo.AppliedLevel.Value, classProfile.EducationLevelID, SemesterID);
            Stream excel = null;
            if (subjectCatBO.IsCommenting == Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
            {
                excel = this.MarkRecordBusiness.ExportReportForSemester(SchoolID, AcademicYearID, SemesterID, _globalInfo.AppliedLevel.Value, classProfile.EducationLevelID, ClassID, PeriodID, SubjectID, out filename, new GlobalInfo().IsAdminSchoolRole);
            }
            else if (subjectCatBO.IsCommenting == Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT)
            {
                Dictionary<int, bool> dicDisplay = new Dictionary<int, bool>();
                excel = this.JudgeRecordBusiness.ExportReportForSemester(SchoolID, AcademicYearID, SemesterID, _globalInfo.AppliedLevel.Value, classProfile.EducationLevelID, ClassID, PeriodID, SubjectID, out filename, _globalInfo.IsAdminSchoolRole, dicDisplay);
            }

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = filename;
            result.FileDownloadName = ReportName;

            // Tạo dữ liệu ghi log

            descriptionObject.Append("Export excel mark record class_profile_id: " + classProfile.ClassProfileID.ToString());
            descriptionObject.Append(", SubjectID:" + SubjectID).Append(", SemesterID: " + SemesterID);
            userDescriptions.Append("Xuất excel bảng điểm lớp {");
            userDescriptions.Append(classProfile.DisplayName).Append("}, ");
            userDescriptions.Append("môn {").Append(subjectCatBO.DisplayName).Append("}, ");
            userDescriptions.Append("Học kỳ " + SemesterID).Append(", năm học ").Append(classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1));
            paramObject.Append("ClassID: " + ClassID + ", SubjectID: " + SubjectID + ", SemesterID: " + SemesterID);
            IDictionary<string, object> dicLog = new Dictionary<string, object>()
            {
                {Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,""},
                {Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,""},
                {Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,classProfile.ClassProfileID.ToString()},
                {Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionObject.ToString()},
                {Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramObject.ToString()},
                {Constants.GlobalConstants.ACTION_AUDIT_USERACTION,GlobalConstants.ACTION_EXPORT},
                {Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,"Sổ điểm"},
                {Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptions.ToString()}
            };

            SetViewDataActionAudit(dicLog);
            //SetViewDataActionAudit(String.Empty, String.Empty,
            //    classProfile.ClassProfileID.ToString(),
            //    "Export excel mark record class_profile_id:" + classProfile.ClassProfileID, "ClassID: " + ClassID + ", SubjectID: " + SubjectID + ", SemesterID: " + SemesterID, "Sổ điểm", SMAS.Business.Common.GlobalConstants.ACTION_EXPORT,
            //    "Xuất excel sổ điểm lớp " + classProfile.DisplayName + " HK " + SemesterID + " năm " + classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1) + " môn " + subjectCatBO.SubjectName);

            return result;
        }

        [ActionAudit]
        public FileResult ExportFileByListSubject()
        {
            int AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            int SchoolID = new GlobalInfo().SchoolID.Value;
            int ClassID = Business.Common.Utils.GetInt(Request["classid"]);
            string arrSubjectID = Business.Common.Utils.GetString(Request["arrSubject"]);
            int SemesterID = Business.Common.Utils.GetByte(Request["rptSemester"]);
            int radValue = Business.Common.Utils.GetInt(Request["radVal"]);
            StringBuilder descriptionObject = new StringBuilder();
            StringBuilder paramObject = new StringBuilder();
            StringBuilder userFunctions = new StringBuilder();
            StringBuilder userActions = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();

            List<string> listStrSubjectId = arrSubjectID.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            List<int> listSubjectId = new List<int>();
            if (listStrSubjectId != null && listStrSubjectId.Count > 0)
            {
                listSubjectId = listStrSubjectId.Select(p => int.Parse(p)).ToList();
            }

            List<SubjectCat> lstSC = (from sc in SubjectCatBusiness.All
                                      where sc.AppliedLevel == _globalInfo.AppliedLevel
                                      && sc.IsActive
                                      && listSubjectId.Contains(sc.SubjectCatID)
                                      select sc).ToList();

            //dua tat ca thong tin tu form vao dictionary
            String filename = "";

            // Tạo dữ liệu ghi log
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);
            Stream excel = this.MarkRecordBusiness.ExportReportForSemesterByListSubjectID(SchoolID, AcademicYearID, SemesterID, _globalInfo.AppliedLevel.Value, classProfile.EducationLevelID, ClassID, listSubjectId, out filename, _globalInfo.IsAdminSchoolRole);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = filename;
            result.FileDownloadName = ReportName;

            // Tạo dữ liệu ghi log
            descriptionObject.Append("Export excel mark record class_profile_id: " + classProfile.ClassProfileID.ToString());
            userDescriptions.Append("Xuất excel bảng điểm lớp {");
            userDescriptions.Append(classProfile.DisplayName).Append("}, ");
            userDescriptions.Append("môn {");
            paramObject.Append("ClassID: " + ClassID + ", SubjectID: {");
            SubjectCat objSC = null;
            for (int i = 0; i < lstSC.Count; i++)
            {
                objSC = lstSC[i];
                descriptionObject.Append(", SubjectID:{" + objSC.SubjectCatID);
                userDescriptions.Append(objSC.SubjectName);
                paramObject.Append(objSC.SubjectCatID);
                if (i < lstSC.Count - 1)
                {
                    descriptionObject.Append(",");
                    userDescriptions.Append(", ");
                    paramObject.Append(",");
                }
            }
            userDescriptions.Append("}, Học kỳ " + SemesterID).Append(", năm học ").Append(classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1)); ;
            descriptionObject.Append("}, SemesterID: " + SemesterID);
            paramObject.Append(", SemesterID: " + SemesterID);
            IDictionary<string, object> dicLog = new Dictionary<string, object>()
            {
                {Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,""},
                {Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,""},
                {Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,classProfile.ClassProfileID.ToString()},
                {Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionObject.ToString()},
                {Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramObject.ToString()},
                {Constants.GlobalConstants.ACTION_AUDIT_USERACTION,GlobalConstants.ACTION_EXPORT},
                {Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,"Sổ điểm"},
                {Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptions.ToString()}
            };
            SetViewDataActionAudit(dicLog);
            //SetViewDataActionAudit(String.Empty, String.Empty,
            //    classProfile.ClassProfileID.ToString(),
            //    "Export excel mark record class_profile_id:" + classProfile.ClassProfileID, "ClassID: " + ClassID + ", SubjectID: " + arrSubjectID + ", SemesterID: " + SemesterID, "Sổ điểm", SMAS.Business.Common.GlobalConstants.ACTION_EXPORT,
            //    "Xuất excel sổ điểm lớp " + classProfile.DisplayName + " HK " + SemesterID + " năm " + classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1));
            return result;
        }

        [ActionAudit]
        public FileResult ExportFileByListClassId()
        {
            int AcademicYearID = new GlobalInfo().AcademicYearID.Value;
            int SchoolID = new GlobalInfo().SchoolID.Value;
            string ArrClassID = Business.Common.Utils.GetString(Request["arrClassid"]);
            int SubjectID = Business.Common.Utils.GetInt(Request["subjectId"]);
            int SemesterID = Business.Common.Utils.GetByte(Request["rptSemester"]);
            int radValue = Business.Common.Utils.GetInt(Request["radVal"]);
            int EducationLevelId = Business.Common.Utils.GetInt(Request["educationLevelID"]); ;
            int subjectIDIncrease = Business.Common.Utils.GetInt(Request["subjectIDIncrease"]); ;
            int isCommenting = Business.Common.Utils.GetInt(Request["isCommenting"]);
            SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
            AcademicYear objaca = AcademicYearBusiness.Find(AcademicYearID);
            StringBuilder descriptionObject = new StringBuilder();
            StringBuilder paramObject = new StringBuilder();
            StringBuilder userFunctions = new StringBuilder();
            StringBuilder userActions = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();

            List<string> listStrClassId = ArrClassID.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            List<int> listClassId = new List<int>();
            if (listStrClassId != null && listStrClassId.Count > 0)
            {
                listClassId = listStrClassId.Select(p => int.Parse(p)).ToList();
            }

            List<ClassProfile> lstCP = (from cp in ClassProfileBusiness.All
                                        where cp.AcademicYearID == AcademicYearID
                                        && cp.SchoolID == SchoolID
                                        && listClassId.Contains(cp.ClassProfileID)
                                        && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                        select cp).ToList();

            //dua tat ca thong tin tu form vao dictionary
            String filename = "";

            Stream excel = this.MarkRecordBusiness.ExportReportForSemesterByListClassId(SchoolID, AcademicYearID, SemesterID, _globalInfo.AppliedLevel.Value, EducationLevelId, listClassId, SubjectID, subjectIDIncrease, isCommenting, out filename, _globalInfo.IsAdminSchoolRole);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            String ReportName = filename;
            result.FileDownloadName = ReportName;

            // Tạo dữ liệu ghi log
            descriptionObject.Append("Export excel mark record class_profile_id:{");
            userDescriptions.Append("Xuất excel bảng điểm lớp {");
            //userDescriptions.Append(classProfile.DisplayName).Append("} ");
            paramObject.Append("ClassID: {");
            ClassProfile objCP = null;
            for (int i = 0; i < lstCP.Count; i++)
            {
                objCP = lstCP[i];
                descriptionObject.Append(objCP.ClassProfileID);
                userDescriptions.Append(objCP.DisplayName);
                paramObject.Append(objCP.ClassProfileID);
                if (i < lstCP.Count - 1)
                {
                    descriptionObject.Append(",");
                    userDescriptions.Append(", ");
                    paramObject.Append(",");
                }
            }
            userDescriptions.Append("}, môn {" + objSC.SubjectName).Append("}, Học kỳ " + SemesterID).Append(", năm học ").Append(objaca.Year + "-" + (objaca.Year + 1));
            descriptionObject.Append("}, môn {" + objSC.SubjectName).Append("} SemesterID: " + SemesterID);
            paramObject.Append("}, SubjectID: " + SubjectID + "SemesterID: " + SemesterID);
            IDictionary<string, object> dicLog = new Dictionary<string, object>()
            {
                {Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,""},
                {Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,""},
                {Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objSC.SubjectCatID.ToString()},
                {Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionObject.ToString()},
                {Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramObject.ToString()},
                {Constants.GlobalConstants.ACTION_AUDIT_USERACTION,GlobalConstants.ACTION_EXPORT},
                {Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,"Sổ điểm"},
                {Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptions.ToString()}
            };
            SetViewDataActionAudit(dicLog);

            return result;
        }

        [ValidateAntiForgeryToken]
        public JsonResult UploadFileImport(IEnumerable<HttpPostedFileBase> attachmentsSemester, int? classId, int? semesterId, int? subjectId, int? educationLevelId)
        {
            if (attachmentsSemester == null || attachmentsSemester.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            var file = attachmentsSemester.FirstOrDefault();
            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extension = Path.GetExtension(file.FileName);
                fileName = fileName + "-" + _globalInfo.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                List<IVTWorksheet> lstSheets = oBook.GetSheets();
                IVTWorksheet sheet = null;
                List<int> listTypeReportExcel = new List<int>();
                string strValue = string.Empty;
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                }
                if (file.ContentLength / 1024 > 5120)
                {
                    return Json(new JsonMessage("Dung lượng file excel không được lớn hơn 5MB", "error"));
                }

                //danh sach ten cac mon hoc trong excel
                List<string> listSheetNameExcel = new List<string>();

                if (lstSheets != null && lstSheets.Count > 0)
                {
                    for (int i = 0; i < lstSheets.Count; i++)
                    {
                        sheet = oBook.GetSheet(i + 1);
                        strValue = (sheet.GetCellValue(BookMarkRecordConstants.CELL_CHECK_TYPE_FILE_IMPORT) != null ? sheet.GetCellValue(BookMarkRecordConstants.CELL_CHECK_TYPE_FILE_IMPORT).ToString() : string.Empty);

                        if (!String.IsNullOrEmpty(strValue))
                        {
                            if (strValue.Equals("1") || strValue.Equals("2") || strValue.Equals("3")) //chon option thu 2 hoac 3
                            {
                                listSheetNameExcel.Add(sheet.Name);
                                listTypeReportExcel.Add(int.Parse(strValue));
                            }
                        }
                        else
                        {
                            return Json(new JsonMessage("File excel không hợp lệ", "error"));
                        }
                    }
                }
                if (listTypeReportExcel != null && listTypeReportExcel.Count > 0)
                {
                    List<int> listType = listTypeReportExcel.Distinct().ToList();
                    if (listType.Count > 1)
                    {
                        return Json(new JsonMessage("File excel không hợp lệ", "error"));
                    }
                    else
                    {
                        int valueImport = listTypeReportExcel.FirstOrDefault();
                        Session[BookMarkRecordConstants.PHISICAL_PATH] = physicalPath;
                        StringBuilder strBuilder = new StringBuilder();
                        if (valueImport == 2) //chon option nhieu mon 1 lop
                        {
                            //Lay danh sach mon co quyen doi voi giao vien
                            //List<SubjectCatBO> listSubjectPremission = this.GetListSubject(classId.Value, semesterId.Value, true);
                            List<SubjectCatBO> listSubjectPremission = ClassSubjectBusiness.GetListSubjectbyTeaching(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semesterId.Value, classId.Value);
                            if (listSubjectPremission != null && listSubjectPremission.Count > 0)
                            {
                                string subjectName = string.Empty;
                                for (int i = 0; i < listSubjectPremission.Count; i++)
                                {
                                    subjectName = listSubjectPremission[i].DisplayName;
                                    if (listSheetNameExcel.Exists(p => p.Equals(Utils.Utils.StripVNSignAndSpace(subjectName))))
                                    {
                                        strBuilder.Append("<span class='ClassListSubjectImport editor-field'>");
                                        strBuilder.Append("<input class='classSubjectImport' type='checkbox' value='" + listSubjectPremission[i].SubjectCatID + "' name='OptionSubject_" + listSubjectPremission[i].SubjectCatID + "' iscommenting='" + listSubjectPremission[i].IsCommenting + "' onchange='changeSubjectImport()' subjectidincrease='" + listSubjectPremission[i].SubjectIdInCrease + "'>" + listSubjectPremission[i].DisplayName + "");
                                        strBuilder.Append("</span>");
                                    }
                                }
                            }
                        }
                        else if (valueImport == 3) //chon option thu 3
                        {
                            //Lay danh sach cac lop hien tai ma nguoi co quuyen thao tac
                            string strResult = string.Empty;
                            //Dictionary<string, object> dic = new Dictionary<string, object>();
                            //dic["SubjectID"] = subjectId;
                            int teacherId = 0;
                            if (!_globalInfo.IsAdmin)
                            {
                                teacherId = _globalInfo.EmployeeID.Value;
                            }

                            /*List<ClassProfileTempBO> listClassProfile = ClassSubjectBusiness.GetListClassBySubjectId(
                                _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, subjectId.Value, educationLevelId.Value,
                                teacherId, _globalInfo.IsAdmin, semesterId.Value, _globalInfo.AppliedLevel.Value, _globalInfo.IsAdminSchoolRole,
                                _globalInfo.IsViewAll, false, _globalInfo.UserAccountID, classId.Value, true);
                                
                             */
                            int academicYearId = _globalInfo.AcademicYearID.Value;
                            int schoolId = _globalInfo.SchoolID.Value;
                            bool isSchoolAdmin = _globalInfo.IsAdmin;

                            List<ClassProfileTempBO> listClassProfile = ClassSubjectBusiness.GetListClassByTeachingAndSubjectId(academicYearId, schoolId, subjectId.Value, teacherId, isSchoolAdmin, semesterId.Value, true);


                            if (listClassProfile != null && listClassProfile.Count > 0)
                            {
                                string className = string.Empty;
                                for (int i = 0; i < listClassProfile.Count; i++)
                                {
                                    className = listClassProfile[i].DisplayName;
                                    if (listSheetNameExcel.Exists(p => p.Equals(Business.Common.Utils.StripVNSignAndSpace(className))))
                                    {
                                        strBuilder.Append("<span class='ClassListClassOption editor-field'><input class='ClassExport' type='checkbox' value='" + listClassProfile[i].ClassProfileID + "' name='OptionClass_" + listClassProfile[i].ClassProfileID + "' onchange='changeClasExport()'>" + listClassProfile[i].DisplayName + "</span>");
                                    }
                                }
                            }
                        }

                        return Json(new { TypeImport = valueImport, strOption = strBuilder != null ? strBuilder.ToString() : string.Empty });
                    }
                }
                return Json(new JsonMessage("File excel không hợp lệ", "error"));
            }
            return Json(new JsonMessage("File excel không hợp lệ", "error"));
        }

        #region Ham ImportExcel

        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult ImportExcel(int? semesterid, int? ClassID, int? SubjectID,
            int typeImport, string arrSubjectid, string arrClassid, int? EducationLevelId, int isCommenting)
        {

            string error = "";
            if (Session[BookMarkRecordConstants.PHISICAL_PATH] == null) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            //IEnumerable<HttpPostedFileBase> attachments = (IEnumerable<HttpPostedFileBase>)Session[BookMarkRecordConstants.LINK_FILE_IMPORT_ATTACH];
            GlobalInfo global = new GlobalInfo();

            string physicalPath = (string)Session[BookMarkRecordConstants.PHISICAL_PATH];

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = new GlobalInfo().SchoolID.Value;
            dic["Semester"] = semesterid;
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
            SemeterDeclaration SemeterDeclaration = SemesterDeclarationBusiness.Search(dic).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();

            string strMarkTypePeriod = MarkRecordBusiness.GetMarkSemesterTitle(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, semesterid.Value);
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);

            IQueryable<ExemptedSubject> IQueryExempted = ExemptedSubjectBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID.Value
                                   && o.AcademicYearID == _globalInfo.AcademicYearID.Value
                                   && ((semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && o.FirstSemesterExemptType.HasValue && o.FirstSemesterExemptType > 0)
                                   || (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && o.SecondSemesterExemptType.HasValue && o.SecondSemesterExemptType > 0)));

            if (typeImport == 1)
            {
                #region Import theo 1 lop va 1 mon
                string strMarkTypeLock = MarkRecordBusiness.GetLockMarkTitle(new GlobalInfo().SchoolID.Value, new GlobalInfo().AcademicYearID.Value, ClassID.Value, (int)semesterid, (int)SubjectID);
                string Error = "";

                List<int> listSubjectId = new List<int>() { SubjectID.Value };
                List<SubjectCatBO> listsubjectCatBO = SubjectCatBusiness.GetListSubjectBO(listSubjectId, _globalInfo.AppliedLevel.Value, _globalInfo.AcademicYearID.Value, ClassID.Value, EducationLevelId.Value, semesterid.Value);
                SubjectCatBO subjectBO = null;
                if (listsubjectCatBO != null && listsubjectCatBO.Count > 0)
                {
                    subjectBO = listsubjectCatBO.FirstOrDefault();
                }
                List<PupilOfClassBO> listPupilOfClass = (from poc in PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> {
                            { "AcademicYearID", _globalInfo.AcademicYearID.Value }, { "ClassID", ClassID}, { "Check", "Check" }
                            })
                                                         join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                         where pp.IsActive && pp.CurrentSchoolID == academicYear.SchoolID
                                                         select new PupilOfClassBO
                                                         {
                                                             PupilID = poc.PupilID,
                                                             PupilFullName = pp.FullName,
                                                             PupilCode = pp.PupilCode,
                                                             Status = poc.Status
                                                         }).ToList();
                IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
                IVTWorksheet sheet = oBook.GetSheet(1);

                List<MarkRecordViewModel> list = getDataToFileImport(physicalPath, semesterid, ClassID, SubjectID, isCommenting,
                    SemeterDeclaration, strMarkTypePeriod, strMarkTypeLock, listPupilOfClass, subjectBO, academicYear, sheet, IQueryExempted, out Error);

                if (Error.Trim().Length == 0)
                {
                    if (list.Where(o => o.Pass == false).Count() > 0)
                    {
                        List<MarkRecordViewModel> listError = list.Where(o => o.Pass == false).ToList();
                        ViewData[BookMarkRecordConstants.LIST_IMPORTDATA] = listError;
                        return Json(new JsonMessage(RenderPartialViewToString("_ViewError", null), "grid"));
                    }
                    else
                    {
                        //luu thang vao csdl
                        if (isCommenting == Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                        {
                            error = SetDataInputToDb(list);
                        }
                        else
                        {
                            error = SetDataInputToDbJudgeReord(list);
                        }
                    }
                }
                else
                {
                    return Json(new JsonMessage(Error, "error"));
                }
                #endregion
            }
            else if (typeImport == 2)
            {
                #region Import mot lop nhieu mon
                Dictionary<int, string> dicMarkType = MarkRecordBusiness.GetLockMarkTitleBySubject(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, ClassID.Value, semesterid.Value);
                if (string.IsNullOrEmpty(arrSubjectid))
                {
                    return Json(new JsonMessage("File excel không hợp lệ.", "error"));
                }
                else
                {
                    #region Danh sach mon hoc cua lop
                    List<PupilOfClass> listPupilOfClass = (from poc in PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object> {
                            { "AcademicYearID", _globalInfo.AcademicYearID.Value }, { "ClassID", ClassID} })
                                                           join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                           select poc).ToList();

                    List<MarkRecordViewModel> list = new List<MarkRecordViewModel>();
                    MarkRecordViewObjModel markRecordOBj = getDataToFileImportByListSubjectID(arrSubjectid, ClassID.Value, EducationLevelId.Value
                        , semesterid.Value, physicalPath, SemeterDeclaration, strMarkTypePeriod, dicMarkType, listPupilOfClass, academicYear, IQueryExempted);

                    if (!string.IsNullOrEmpty(markRecordOBj.Error))
                    {
                        return Json(new JsonMessage(markRecordOBj.Error, "error"));
                    }
                    else
                    {
                        list = markRecordOBj.listMarkRecordViewModel;
                        if (list != null && list.Count > 0)
                        {
                            if (list.Where(o => o.Pass == false).Count() > 0)
                            {
                                List<MarkRecordViewModel> listError = list.Where(o => o.Pass == false).ToList();
                                ViewData[BookMarkRecordConstants.LIST_IMPORTDATA] = listError;
                                return Json(new JsonMessage(RenderPartialViewToString("_ViewError", null), "grid"));
                            }
                            else
                            {
                                List<MarkRecordViewModel> listMark = list.Where(p => p.IsCommenting == Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK).ToList();
                                List<MarkRecordViewModel> listJudge = list.Where(p => p.IsCommenting == Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT).ToList();
                                //luu thang vao csdl
                                if (listMark != null && listMark.Count > 0)
                                {
                                    error = SetDataInputToDbByListSubject(ClassID.Value, semesterid.Value, listPupilOfClass, listMark);
                                }

                                if (listJudge != null && listJudge.Count > 0)
                                {
                                    error = SetDataInputToDbJudgeReordByListSubject(ClassID.Value, semesterid.Value, listJudge);
                                }
                            }
                        }
                        else
                        {
                            return Json(new JsonMessage("File excel không hợp lệ.", "error"));
                        }
                    }


                    #endregion
                }
                #endregion
            }
            else if (typeImport == 3)
            {
                #region Import mot mon nhieu lop
                Dictionary<int, string> MarkTypeLock = MarkRecordBusiness.GetLockMarkTitleBySchoolID(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, semesterid.Value, 0, (int)SubjectID);
                List<MarkRecordViewModel> list = new List<MarkRecordViewModel>();
                MarkRecordViewObjModel resultObj = this.getDataToFileImportByListClassID(arrClassid, SubjectID.Value, EducationLevelId.Value, semesterid.Value,
                    physicalPath, SemeterDeclaration, strMarkTypePeriod, MarkTypeLock, academicYear, IQueryExempted);

                if (!string.IsNullOrEmpty(resultObj.Error) && resultObj.listMarkRecordViewModel != null && resultObj.listMarkRecordViewModel.Count <= 0)
                {
                    return Json(new JsonMessage(resultObj.Error, "error"));
                }
                else
                {
                    list = resultObj.listMarkRecordViewModel;
                    if (list != null && list.Count > 0)
                    {
                        if (list.Where(o => o.Pass == false).Count() > 0)
                        {
                            List<MarkRecordViewModel> listError = list.Where(o => o.Pass == false).ToList();
                            ViewData[BookMarkRecordConstants.LIST_IMPORTDATA] = listError;
                            return Json(new JsonMessage(RenderPartialViewToString("_ViewError", null), "grid"));
                        }
                        else
                        {
                            //luu thang vao csdl
                            if (isCommenting == Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                            {
                                List<MarkRecordViewModel> listMark = list.Where(p => p.IsCommenting == Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK).ToList();
                                error = SetDataInputToDbByListClassID(SubjectID.Value, semesterid.Value, listMark);
                            }
                            else
                            {
                                List<MarkRecordViewModel> listJudge = list.Where(p => p.IsCommenting == Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT).ToList();
                                error = SetDataInputToDbJudgeReordByListClass(SubjectID.Value, semesterid.Value, listJudge);
                            }
                        }
                    }
                    else
                    {
                        return Json(new JsonMessage("File excel không hợp lệ.", "error"));
                    }
                }
                #endregion
            }
            error = "Import điểm thành công.";
            return Json(new JsonMessage(error, "success"));
        }
        #endregion

        #region Ham chuyen PartialView thanh string
        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion

        public List<MarkRecordViewModel> getDataToFileImport(string filename, int? semesterid, int? classID, int? subjectID,
            int? isCommenting, SemeterDeclaration semeterDeclaration, string strMarkTypePeriod, string strMarkTypeLock,
            List<PupilOfClassBO> listPupilOfClass, SubjectCatBO subjectCat, AcademicYear academicYear, IVTWorksheet sheet, IQueryable<ExemptedSubject> IQueryExempted, out string Error)
        {
            GlobalInfo glo = new GlobalInfo();
            int SchoolID = glo.SchoolID.Value;
            int AcademicYearID = glo.AcademicYearID.Value;
            int AppliedLevel = glo.AppliedLevel.Value;
            string FilePath = filename;
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);

            List<MarkRecordViewModel> ListPupil = new List<MarkRecordViewModel>();
            #region Lay du lieu trong file excel
            Error = "";
            #region Kiem tra du lieu chon dau vao
            string ClassName = ClassProfileBusiness.Find(classID).DisplayName;
            string SubjectName = subjectCat.DisplayName;
            string SemesterName = ReportUtils.ConvertSemesterForReportName(semesterid.Value);
            string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
            string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
            string AcademicYearNameFromExcel = (string)sheet.GetCellValue("A5");
            string Title = "";
            var TitleObj = sheet.GetCellValue("A4");
            if (TitleObj != null)
                Title = TitleObj.ToString();

            if (Title == null || AcademicYearNameFromExcel == null)
            {
                Error = "- " + "File excel không đúng định dạng" + SubjectName;
                return null;
            }

            //viethd31: Kiem tra title, khong cho phep import file diem dot
            if ((string)sheet.GetCellValue("E6") != "Điểm hệ số 1")
            {
                Error = "File excel không phải là file điểm của HK" + semesterid;
                return null;
            }

            if (Title.Contains(SubjectName.ToUpper()) == false)
            {
                Error = "- " + Res.Get("MarkRecord_Label_SubjectError") + " " + SubjectName;
                return null;
            }
            if (Title.Contains(ClassName.ToUpper()) == false)
            {
                if (Error == "")
                    Error = "- " + Res.Get("MarkRecord_Label_ClassError") + " " + ClassName;
                else Error = Error + "</br>" + "- " + Res.Get("MarkRecord_Label_ClassError");
                return null;
            }
            //string[] tmp = rdol_Subject.SelectedValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] tmp = Title.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            bool isCurrentSemester = false;
            for (int i = tmp.Length - 1; i >= 0; i--)
            {
                if (tmp[i] == SemesterName)
                {
                    isCurrentSemester = true;
                    break;
                }
            }
            if (!isCurrentSemester)
            {
                if (Error == "")
                    Error = "- " + Res.Get("MarkRecord_Label_SemesterError") + " " + SemesterName;
                else Error = Error + "</br>" + "- " + Res.Get("MarkRecord_Label_SemesterError");
                return null;
            }
            if (AcademicYearNameFromExcel.Contains(AcademicYearName.ToUpper()) == false && AcademicYearNameFromExcel.Contains(_AcademicYearName.ToUpper()) == false)
            {
                if (Error == "")
                    Error = "- " + Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
                else Error = Error + "</br>" + "- " + Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
                return null;
            }


            #endregion

            //phan kiem tra du lieu chon ban dau va thong tin tren excel
            string error = "";

            //kiem tra cac cot trong bang csdl  xem da chuan chua
            #region Kiem tra so dau diem
            int startColMark = 5;
            int startRow = 8;

            string[] listMarkType = strMarkTypePeriod.Split(new Char[] { ',' }).Where(u => !string.IsNullOrEmpty(u) && !u.Equals("HK")).ToArray();
            int lengthMarkType = listMarkType.Length;
            #endregion

            //Lay danh sach hoc sinih co mien giam
            List<ExemptedSubject> listExempted = IQueryExempted.Where(p => p.SubjectID == subjectID.Value && p.ClassID == classID).ToList();
            bool Pass = true;
            // Kiem tra trung du lieu hay khong?
            List<string> lstPupilCode = new List<string>();
            #region for qua excel
            while (sheet.GetCellValue(startRow, 1) != null || sheet.GetCellValue(startRow, 2) != null || sheet.GetCellValue(startRow, 3) != null)
            {
                Pass = true;
                error = "";
                bool checkDuplicate = false;
                MarkRecordViewModel Pupil = new MarkRecordViewModel();
                string pupilcode = (string)sheet.GetCellValue(startRow, 3);

                var poc = listPupilOfClass.Where(o => o.PupilCode == pupilcode).FirstOrDefault();

                if (poc == null)
                {
                    error = "-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_PupilIDError");
                    Pass = false;
                }
                else
                {
                    //Kiem tra hoc sinh co mien giam thi khong them vao list
                    if (listExempted.Exists(p => p.PupilID == poc.PupilID))
                    {
                        startRow++;
                        continue;
                    }
                    Pupil.PupilID = poc.PupilID;
                }

                if (lstPupilCode.Count() == 0)
                {
                    // add vao list ma hoc sinh:
                    lstPupilCode.Add(pupilcode);
                }
                else
                {
                    if (lstPupilCode.Contains(pupilcode))
                    {
                        checkDuplicate = true;
                    }
                    else
                    {
                        lstPupilCode.Add(pupilcode);
                    }
                }

                Pupil.PupilCode = pupilcode;
                string pupilname = (string)sheet.GetCellValue(startRow, 4);
                if (poc != null && !poc.PupilFullName.Equals(pupilname, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (string.Empty.Equals(error))
                    {
                        error = "-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_NameError");
                    }
                    else
                    {
                        error += "</br>-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_NameError");
                    }
                    Pass = false;
                }
                // Check trùng dữ liệu:
                if (checkDuplicate)
                {
                    Pass = false;
                    if (string.Empty.Equals(error))
                    {
                        error = "-Sheet " + sheet.Name + ": Trùng mã học sinh.";
                    }
                    else
                    {
                        error += "</br>-Sheet " + sheet.Name + ": Trùng mã học sinh.";
                    }
                }
                Pupil.Fullname = pupilname;
                Pupil.ClassID = classID;
                Pupil.SubjectID = subjectID;

                int index = 0;
                Pupil.ListMark = new Dictionary<string, string>();
                Dictionary<string, string> MarkList = new Dictionary<string, string>();
                int col = 0;
                if (isCommenting == Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                {
                    #region Mon tinh diem
                    #region truong GDTX
                    if (objSP.TrainingTypeID == 3)//truong GDTX
                    {
                        for (col = startColMark; col < startColMark + lengthMarkType; col++)
                        {
                            var valuePeriodMark = sheet.GetCellValue(startRow, col);
                            // Neu la null hoac rong thi gan lai la null de xu ly o duoi
                            if (valuePeriodMark == null || valuePeriodMark.ToString().Trim().Equals(string.Empty))
                            {
                                valuePeriodMark = null;
                            }
                            string TypeMark = listMarkType[index];

                            if (TypeMark[0] == 'M')
                            {
                                int result = 0;
                                bool temp = int.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "0", out result);
                                temp = temp && result <= 10 && result >= 0;
                                if (valuePeriodMark == null)
                                {
                                    MarkList[TypeMark] = "";
                                }
                                else if (!temp)
                                {
                                    if (string.Empty.Equals(error))
                                    {
                                        error = "-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkM");
                                    }
                                    else
                                    {
                                        error += "</br>-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkM");
                                    }
                                    Pass = false;
                                    MarkList[TypeMark] = valuePeriodMark.ToString();
                                }
                                else
                                {
                                    MarkList[TypeMark] = valuePeriodMark.ToString();
                                }
                            }
                            else if (TypeMark[0] == 'P')
                            {
                                int result = 0;
                                bool temp = int.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "0", out result);
                                temp = temp && result <= 10 && result >= 0;
                                if (valuePeriodMark == null)
                                    MarkList[TypeMark] = "";
                                else if (!temp)
                                {
                                    if (string.Empty.Equals(error))
                                    {
                                        error = "-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkP");
                                    }
                                    else
                                    {
                                        error += "</br>-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkP");
                                    }
                                    Pass = false;
                                    MarkList[TypeMark] = valuePeriodMark.ToString();
                                }
                                else
                                    MarkList[TypeMark] = valuePeriodMark.ToString();
                            }
                            else if (TypeMark[0] == 'V')
                            {
                                int result = 0;
                                bool temp = int.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "0", out result);
                                temp = temp && result <= 10 && result >= 0;
                                if (valuePeriodMark == null)
                                    MarkList[TypeMark] = "";
                                else if (!temp)
                                {
                                    if (string.Empty.Equals(error))
                                    {
                                        error = "-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkV");
                                    }
                                    else
                                    {
                                        error += "</br>-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkV");
                                    }
                                    Pass = false;
                                    MarkList[TypeMark] = valuePeriodMark.ToString();
                                }
                                else
                                    MarkList[TypeMark] = valuePeriodMark.ToString();
                            }
                            index++;
                        }

                        var MarkKTHK = sheet.GetCellValue(startRow, col);
                        if (MarkKTHK == null || string.Empty.Equals(MarkKTHK.ToString().Trim()))
                        {
                            Pupil.MarkSemester = "";
                        }
                        else
                        {
                            float resultM = 0;
                            bool tempHK = float.TryParse(MarkKTHK != null ? MarkKTHK.ToString() : "0", out resultM);
                            tempHK = tempHK && resultM <= 10 && resultM >= 0;
                            if (!tempHK)
                            {
                                if (string.Empty.Equals(error))
                                {
                                    error = "-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkHK");
                                }
                                else
                                {
                                    error += "</br>-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkHK");
                                }
                                Pass = false;
                                Pupil.MarkSemester = MarkKTHK.ToString().Trim();
                            }
                            else
                            {
                                Pupil.MarkSemester = this.GetMarkHKOfGDTX(MarkKTHK.ToString().Trim());
                            }
                        }
                    }
                    #endregion
                    #region truong binh thuong
                    else
                    {
                        for (col = startColMark; col < startColMark + lengthMarkType; col++)
                        {
                            var valuePeriodMark = sheet.GetCellValue(startRow, col);
                            // Neu la null hoac rong thi gan lai la null de xu ly o duoi
                            if (valuePeriodMark == null || valuePeriodMark.ToString().Trim().Equals(string.Empty))
                            {
                                valuePeriodMark = null;
                            }
                            string TypeMark = listMarkType[index];

                            if (TypeMark[0] == 'M')
                            {
                                int result = 0;
                                bool temp = int.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "0", out result);
                                temp = temp && result <= 10 && result >= 0;
                                if (valuePeriodMark == null)
                                {
                                    MarkList[TypeMark] = "";
                                }
                                else if (!temp)
                                {
                                    if (string.Empty.Equals(error))
                                    {
                                        error = "-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkM");
                                    }
                                    else
                                    {
                                        error += "</br>-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkM");
                                    }
                                    Pass = false;
                                    MarkList[TypeMark] = valuePeriodMark.ToString();
                                }
                                else
                                {
                                    MarkList[TypeMark] = valuePeriodMark.ToString();
                                }
                            }
                            else if (TypeMark[0] == 'P')
                            {
                                float result = 0;
                                bool temp = float.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "0", out result);
                                temp = temp && result <= 10 && result >= 0;
                                if (valuePeriodMark == null)
                                    MarkList[TypeMark] = "";
                                else if (!temp)
                                {
                                    if (string.Empty.Equals(error))
                                    {
                                        error = "-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkP");
                                    }
                                    else
                                    {
                                        error += "</br>-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkP");
                                    }
                                    Pass = false;
                                    MarkList[TypeMark] = valuePeriodMark.ToString();
                                }
                                else
                                    MarkList[TypeMark] = valuePeriodMark.ToString();
                            }
                            else if (TypeMark[0] == 'V')
                            {
                                float result = 0;
                                bool temp = float.TryParse(valuePeriodMark != null ? valuePeriodMark.ToString() : "0", out result);
                                temp = temp && result <= 10 && result >= 0;
                                if (valuePeriodMark == null)
                                    MarkList[TypeMark] = "";
                                else if (!temp)
                                {
                                    if (string.Empty.Equals(error))
                                    {
                                        error = "-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkV");
                                    }
                                    else
                                    {
                                        error += "</br>-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkV");
                                    }
                                    Pass = false;
                                    MarkList[TypeMark] = valuePeriodMark.ToString();
                                }
                                else
                                    MarkList[TypeMark] = valuePeriodMark.ToString();
                            }
                            index++;
                        }

                        var MarkKTHK = sheet.GetCellValue(startRow, col);
                        if (MarkKTHK == null || string.Empty.Equals(MarkKTHK.ToString().Trim()))
                        {
                            Pupil.MarkSemester = "";
                        }
                        else
                        {
                            float resultM = 0;
                            bool tempHK = float.TryParse(MarkKTHK != null ? MarkKTHK.ToString() : "0", out resultM);
                            tempHK = tempHK && resultM <= 10 && resultM >= 0;
                            if (!tempHK)
                            {
                                if (string.Empty.Equals(error))
                                {
                                    error = "-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkHK");
                                }
                                else
                                {
                                    error += "</br>-Sheet " + sheet.Name + ": " + Res.Get("MarkRecord_Label_MarkHK");
                                }
                                Pass = false;
                                Pupil.MarkSemester = MarkKTHK.ToString().Trim();
                            }
                            else
                                Pupil.MarkSemester = MarkKTHK.ToString().Trim();
                        }
                    }
                    #endregion
                    ///tang cot len den cot TBM 
                    col++; //cot TBMTC
                    col++; //TBM TMP
                    col++; //TBM
                    var MarkTBM = sheet.GetCellValue(startRow, col);
                    float resultTBM = 0;
                    bool tempTBM = float.TryParse(MarkTBM != null ? MarkTBM.ToString() : "0", out resultTBM);
                    if (MarkTBM == null || MarkTBM.ToString().Trim().Equals(string.Empty))
                    {
                        Pupil.MarkTBM = "";
                    }
                    else if (tempTBM)
                    {
                        Pupil.MarkTBM = resultTBM.ToString();
                    }

                    col++;//Cot HK1 bi an
                    col++;//Cot ca nam
                    col++;//Cot nhan xet

                    object commentString = sheet.GetCellValue(startRow, col);
                    if (commentString != null)
                        Pupil.Comment = subjectCat.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE ? commentString.ToString() : null;
                    if (poc != null)
                    {
                        Pupil.PupilID = poc.PupilID;
                        Pupil.Status = poc.Status;
                    }
                    Pupil.ListMark = MarkList;
                    Pupil.Pass = Pass;
                    Pupil.SemesterID = semesterid;
                    Pupil.Note = error;
                    Pupil.SheetName = sheet.Name;
                    Pupil.IsCommenting = Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK;
                    ListPupil.Add(Pupil);
                    #endregion
                }
                else
                {
                    #region Mon nhan xet
                    for (col = startColMark; col < startColMark + lengthMarkType; col++)
                    {
                        var valuePeriodMark = sheet.GetCellValue(startRow, col);
                        // Neu la null hoac rong thi gan lai la null de xu ly o duoi
                        if (valuePeriodMark == null || valuePeriodMark.ToString().Trim().Equals(string.Empty))
                        {
                            valuePeriodMark = null;
                        }
                        string TypeMark = listMarkType[index];

                        if (TypeMark[0] == 'M')
                        {
                            if (valuePeriodMark == null)
                            {
                                MarkList[TypeMark] = "";
                            }
                            else
                            {
                                MarkList[TypeMark] = valuePeriodMark.ToString();
                                if (!valuePeriodMark.Equals("Đ") && !valuePeriodMark.Equals("CĐ"))
                                {
                                    Pass = false;
                                    error = "-Sheet " + sheet.Name + ":  Điểm miệng không hợp lệ </br>";
                                }
                            }
                        }
                        else if (TypeMark[0] == 'P')
                        {
                            if (valuePeriodMark == null)
                            {
                                MarkList[TypeMark] = "";
                            }
                            else
                            {
                                MarkList[TypeMark] = valuePeriodMark.ToString();
                                if (!valuePeriodMark.Equals("Đ") && !valuePeriodMark.Equals("CĐ"))
                                {
                                    Pass = false;
                                    error = "-Sheet " + sheet.Name + ": Điểm 15p không hợp lệ </br>";
                                }
                            }
                        }
                        else if (TypeMark[0] == 'V')
                        {
                            if (valuePeriodMark == null)
                            {
                                MarkList[TypeMark] = "";
                            }
                            else
                            {
                                MarkList[TypeMark] = valuePeriodMark.ToString();
                                if (!valuePeriodMark.Equals("Đ") && !valuePeriodMark.Equals("CĐ"))
                                {
                                    Pass = false;
                                    error = "-Sheet " + sheet.Name + ": Điểm 1 tiết không hợp lệ </br>";
                                }
                            }
                        }
                        index++;
                    }

                    var MarkKTHK = sheet.GetCellValue(startRow, col);
                    if (MarkKTHK == null || string.Empty.Equals(MarkKTHK.ToString().Trim()))
                    {
                        Pupil.MarkSemester = "";
                    }
                    else
                    {
                        Pupil.MarkSemester = MarkKTHK.ToString().Trim();
                        if (!MarkKTHK.Equals("Đ") && !MarkKTHK.Equals("CĐ"))
                        {
                            Pass = false;
                            error = "-Sheet " + sheet.Name + ": Điểm KTHK không hợp lệ </br>";
                        }
                    }

                    col++;
                    var MarkTBM = sheet.GetCellValue(startRow, col);
                    if (MarkTBM == null || string.Empty.Equals(MarkTBM.ToString().Trim()))
                    {
                        Pupil.MarkTBM = "";
                    }
                    else
                    {
                        Pupil.MarkTBM = MarkTBM.ToString().Trim();
                        if (!MarkTBM.Equals("Đ") && !MarkTBM.Equals("CĐ"))
                        {
                            Pass = false;
                            error = "-Sheet " + sheet.Name + ": Điểm TBM không hợp lệ </br>";
                        }
                    }

                    if (poc != null)
                    {
                        Pupil.PupilID = poc.PupilID;
                        Pupil.Status = poc.Status;
                    }
                    Pupil.ListMark = MarkList;
                    Pupil.Pass = Pass;
                    Pupil.SemesterID = semesterid;
                    Pupil.Note = error;
                    Pupil.SheetName = sheet.Name;
                    Pupil.IsCommenting = Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT;
                    ListPupil.Add(Pupil);
                    #endregion
                }
                startRow++;
            }
            #endregion

            return ListPupil;
            #endregion
        }

        public MarkRecordViewObjModel getDataToFileImportByListSubjectID(string arrSubjectid, int ClassID, int EducationLevelId,
            int Semesterid, string physicalPath, SemeterDeclaration semeterDeclaration, string strMarkTypePeriod, Dictionary<int, string> MarkTypeLock,
            List<PupilOfClass> listPupilOfClassID, AcademicYear academicYear, IQueryable<ExemptedSubject> IQueryExempted)
        {
            MarkRecordViewObjModel resultObj = new MarkRecordViewObjModel();
            List<int> listSubjectID = arrSubjectid.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            //Danh sach mon hoc cuar lop, bao gom co mon tang cuong
            List<SubjectCatBO> listSubjectCatBO = SubjectCatBusiness.GetListSubjectBO(listSubjectID, _globalInfo.AppliedLevel.Value, _globalInfo.AcademicYearID.Value, ClassID, EducationLevelId, Semesterid);
            SubjectCatBO subjectObj = null;
            // Danh sach list mon hoc trong file excel
            IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
            List<IVTWorksheet> lstSheets = oBook.GetSheets();
            IVTWorksheet sheetBySubjectName = null;
            string sheetNameDefault = string.Empty;
            string sheetName = string.Empty;
            List<MarkRecordViewModel> ListPupil = new List<MarkRecordViewModel>();
            List<MarkRecordViewModel> ListPupilBySheet = new List<MarkRecordViewModel>();
            MarkRecordViewModel pupil = null;
            MarkRecordViewModel pupilInExcel = null;
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);

            #region Validate
            for (int i = 0; i < listSubjectID.Count; i++)
            {
                subjectObj = listSubjectCatBO.Where(p => p.SubjectCatID == listSubjectID[i]).FirstOrDefault();
                sheetNameDefault = Utils.Utils.StripVNSignAndSpace(subjectObj.SubjectName);
                sheetBySubjectName = oBook.GetSheet(sheetNameDefault);

                #region Kiem tra du lieu chon dau vao
                string Error = string.Empty;
                string ClassName = classProfile.DisplayName;
                string SubjectName = subjectObj.SubjectName;
                string SemesterName = ReportUtils.ConvertSemesterForReportName(Semesterid);
                string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
                string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
                string AcademicYearNameFromExcel = (string)sheetBySubjectName.GetCellValue("A5");
                string Title = "";
                var TitleObj = sheetBySubjectName.GetCellValue("A4");

                if (TitleObj != null)
                    Title = TitleObj.ToString();

                if (Title == null || AcademicYearNameFromExcel == null)
                {
                    Error = "File excel không đúng định dạng" + SubjectName;
                }

                //viethd31: Kiem tra title, khong cho phep import file diem dot
                if ((string)sheetBySubjectName.GetCellValue("E6") != "Điểm hệ số 1")
                {
                    Error = "File excel không phải là file điểm của HK" + Semesterid;
                }

                if (Title.Contains(SubjectName.ToUpper()) == false)
                {
                    Error = Res.Get("MarkRecord_Label_SubjectError") + " " + SubjectName;
                }

                if (Title.Contains(ClassName.ToUpper()) == false)
                {
                    if (Error == "")
                        Error = Res.Get("MarkRecord_Label_ClassError") + " " + ClassName;
                    else Error = Error + "</br>" + "- " + Res.Get("MarkRecord_Label_ClassError");
                }

                //string[] tmp = rdol_Subject.SelectedValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                string[] tmp = Title.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                bool isCurrentSemester = false;
                for (int k = tmp.Length - 1; k >= 0; k--)
                {
                    if (tmp[k] == SemesterName)
                    {
                        isCurrentSemester = true;
                        break;
                    }
                }

                if (!isCurrentSemester)
                {
                    if (Error == "")
                        Error = Res.Get("MarkRecord_Label_SemesterError") + " " + SemesterName;
                    else Error = Error + "</br>" + "- " + Res.Get("MarkRecord_Label_SemesterError");
                }

                if (AcademicYearNameFromExcel.Contains(AcademicYearName.ToUpper()) == false && AcademicYearNameFromExcel.Contains(_AcademicYearName.ToUpper()) == false)
                {
                    if (Error == "")
                        Error = Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
                    else Error = Error + "</br>" + "- " + Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
                }

                if (!string.IsNullOrEmpty(Error))
                {
                    resultObj.Error = Error;
                    resultObj.listMarkRecordViewModel = new List<MarkRecordViewModel>();
                    return resultObj;
                }
                #endregion
            }

            #endregion

            #region Lay du lieu
            for (int i = 0; i < listSubjectID.Count; i++)
            {
                string Error = "";
                subjectObj = listSubjectCatBO.Where(p => p.SubjectCatID == listSubjectID[i]).FirstOrDefault();
                sheetNameDefault = Utils.Utils.StripVNSignAndSpace(subjectObj.SubjectName);
                //get sheet by sheetName
                sheetBySubjectName = oBook.GetSheet(sheetNameDefault);

                List<PupilOfClassBO> listPupilOfClass = (from poc in listPupilOfClassID
                                                         select new PupilOfClassBO
                                                         {
                                                             PupilID = poc.PupilID,
                                                             PupilFullName = poc.PupilProfile.FullName,
                                                             PupilCode = poc.PupilProfile.PupilCode,
                                                             Status = poc.Status
                                                         }).ToList();

                string strMarkTypeLock = MarkTypeLock.Where(p => p.Key == subjectObj.SubjectCatID).Select(p => p.Value).FirstOrDefault();
                ListPupilBySheet = this.getDataToFileImport(physicalPath, Semesterid, ClassID, subjectObj.SubjectCatID, subjectObj.IsCommenting, semeterDeclaration
                     , strMarkTypePeriod, strMarkTypeLock, listPupilOfClass, subjectObj, academicYear, sheetBySubjectName, IQueryExempted, out Error);

                if (ListPupilBySheet != null && ListPupilBySheet.Count > 0)
                {
                    for (int j = 0; j < ListPupilBySheet.Count; j++)
                    {
                        pupilInExcel = ListPupilBySheet[j];
                        pupil = new MarkRecordViewModel();
                        pupil.PupilCode = pupilInExcel.PupilCode;
                        pupil.PupilID = pupilInExcel.PupilID;
                        pupil.Fullname = pupilInExcel.Fullname;
                        pupil.SummedUpMark = pupilInExcel.SummedUpMark;
                        pupil.Status = pupilInExcel.Status;
                        pupil.InterviewMark = pupilInExcel.InterviewMark;
                        pupil.WritingMark = pupilInExcel.WritingMark;
                        pupil.TwiceCoeffiecientMark = pupilInExcel.TwiceCoeffiecientMark;
                        pupil.MarkTBM = pupilInExcel.MarkTBM;
                        pupil.Comment = pupilInExcel.Comment;
                        pupil.Disable = pupilInExcel.Disable;
                        pupil.Pass = pupilInExcel.Pass;
                        pupil.Note = pupilInExcel.Note;
                        pupil.SubjectID = pupilInExcel.SubjectID;
                        pupil.ClassID = pupilInExcel.ClassID;
                        pupil.PeriodID = pupilInExcel.PeriodID;
                        pupil.SemesterID = pupilInExcel.SemesterID;
                        pupil.ListMark = pupilInExcel.ListMark;
                        pupil.SheetName = pupilInExcel.SheetName;
                        pupil.MarkSemester = pupilInExcel.MarkSemester;
                        pupil.AcademicYearID = pupilInExcel.AcademicYearID;
                        pupil.SchoolID = pupilInExcel.SchoolID;
                        pupil.Year = pupilInExcel.Year;
                        pupil.IsCommenting = pupilInExcel.IsCommenting;
                        ListPupil.Add(pupil);
                    }
                }
            }
            resultObj.Error = string.Empty;
            resultObj.listMarkRecordViewModel = ListPupil;
            return resultObj;
            #endregion
        }

        public MarkRecordViewObjModel getDataToFileImportByListClassID(string arrClassId, int subjectID, int educationLevelId,
            int semesterid, string physicalPath, SemeterDeclaration semeterDeclaration, string strMarkTypePeriod, Dictionary<int, string> MarkTypeLock,
            AcademicYear academicYear, IQueryable<ExemptedSubject> IQueryExempted)
        {
            MarkRecordViewObjModel objResult = new MarkRecordViewObjModel();
            List<int> listClassID = arrClassId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            List<int> listSubjectId = new List<int>() { subjectID };
            //Danh sach mon hoc tat ca cac lop
            List<SubjectCatBO> listsubjectCatBO = new List<SubjectCatBO>();
            if (!_globalInfo.IsAdmin)
            {
                listsubjectCatBO = SubjectCatBusiness.GetListSubjectBO(listSubjectId, _globalInfo.AppliedLevel.Value, _globalInfo.AcademicYearID.Value, 0, 0, semesterid);
            }
            else
            {
                listsubjectCatBO = SubjectCatBusiness.GetListSubjectBO(listSubjectId, _globalInfo.AppliedLevel.Value, _globalInfo.AcademicYearID.Value, 0, educationLevelId, semesterid);
            }
            SubjectCatBO subjBOByClassId = null;
            //Danh sach lop cua arrClassId truyen vao
            List<ClassProfile> listClassProfile = (from cp in ClassProfileBusiness.All
                                                   where listClassID.Contains(cp.ClassProfileID)
                                                   && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                                   select cp).ToList();
            ClassProfile classProfileBO = null;
            // Danh sach list mon hoc trong file excel
            IVTWorkbook oBook = VTExport.OpenWorkbook(physicalPath);
            List<IVTWorksheet> lstSheets = oBook.GetSheets();
            IVTWorksheet sheetByClassID = null;
            string sheetNameDefault = string.Empty;
            string sheetName = string.Empty;
            string Error = string.Empty;
            int classId = 0;

            #region Validate
            for (int i = 0; i < listClassID.Count; i++)
            {
                classProfileBO = listClassProfile.Where(p => p.ClassProfileID == listClassID[i]).FirstOrDefault();
                classId = classProfileBO.ClassProfileID;
                subjBOByClassId = listsubjectCatBO.Where(p => p.ClassID == classId).FirstOrDefault();
                sheetByClassID = lstSheets.Where(p => p.Name.Equals(Utils.Utils.StripVNSignAndSpace(classProfileBO.DisplayName))).FirstOrDefault();
                if (sheetByClassID == null) continue;
                #region Kiem tra du lieu chon dau vao

                string ClassName = classProfileBO.DisplayName;
                string SubjectName = subjBOByClassId.DisplayName;
                string SemesterName = ReportUtils.ConvertSemesterForReportName(semesterid);
                string AcademicYearName = academicYear.Year + " - " + (academicYear.Year + 1);
                string _AcademicYearName = academicYear.Year + "-" + (academicYear.Year + 1);
                string AcademicYearNameFromExcel = (string)sheetByClassID.GetCellValue("A5");
                string Title = "";
                var TitleObj = sheetByClassID.GetCellValue("A4");
                if (TitleObj != null)
                    Title = TitleObj.ToString();

                if (Title == null || AcademicYearNameFromExcel == null)
                {
                    Error = "File excel không đúng định dạng" + SubjectName;
                }

                //viethd31: Kiem tra title, khong cho phep import file diem dot
                if ((string)sheetByClassID.GetCellValue("E6") != "Điểm hệ số 1")
                {
                    Error = "File excel không phải là file điểm của HK" + semesterid;
                }

                if (Title.Contains(SubjectName.ToUpper()) == false)
                {
                    Error = Res.Get("MarkRecord_Label_SubjectError") + " " + SubjectName;
                }

                if (Title.Contains(ClassName.ToUpper()) == false)
                {
                    if (Error == "")
                        Error = Res.Get("MarkRecord_Label_ClassError") + " " + ClassName;
                    else Error = Error + "</br>" + "- " + Res.Get("MarkRecord_Label_ClassError");
                }

                //string[] tmp = rdol_Subject.SelectedValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                string[] tmp = Title.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                bool isCurrentSemester = false;
                for (int k = tmp.Length - 1; k >= 0; k--)
                {
                    if (tmp[k] == SemesterName)
                    {
                        isCurrentSemester = true;
                        break;
                    }
                }

                if (!isCurrentSemester)
                {
                    if (Error == "")
                        Error = Res.Get("MarkRecord_Label_SemesterError") + " " + SemesterName;
                    else Error = Error + "</br>" + "- " + Res.Get("MarkRecord_Label_SemesterError");
                }

                if (AcademicYearNameFromExcel.Contains(AcademicYearName.ToUpper()) == false && AcademicYearNameFromExcel.Contains(_AcademicYearName.ToUpper()) == false)
                {
                    if (Error == "")
                        Error = Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
                    else Error = Error + "</br>" + "- " + Res.Get("MarkRecord_Label_YearError") + " " + AcademicYearName;
                }

                if (!string.IsNullOrEmpty(Error))
                {
                    objResult.Error = Error;
                    objResult.listMarkRecordViewModel = new List<MarkRecordViewModel>();
                    return objResult;
                }
                #endregion
            }

            #endregion

            #region Lay du lieu

            //Danh sach hoc sinh cua tât ca cac lop duoc chon
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            //dic["Check"] = "Check";
            dic["ListClassID"] = listClassID;
            List<PupilOfClassBO> listPupilOfClass = (from poc in PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic)
                                                     join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                     select new PupilOfClassBO
                                                     {
                                                         PupilID = poc.PupilID,
                                                         PupilFullName = pp.FullName,
                                                         PupilCode = pp.PupilCode,
                                                         Status = poc.Status,
                                                         ClassID = poc.ClassID
                                                     }).ToList();
            List<PupilOfClassBO> listPupilOfClassByClassID = new List<PupilOfClassBO>();

            List<MarkRecordViewModel> ListPupil = new List<MarkRecordViewModel>();
            List<MarkRecordViewModel> ListPupilByClassID = new List<MarkRecordViewModel>();
            List<MarkRecordViewModel> ListPupilBySheet = new List<MarkRecordViewModel>();
            MarkRecordViewModel pupil = null;
            MarkRecordViewModel pupilInExcel = null;

            for (int i = 0; i < listClassID.Count; i++)
            {

                classId = listClassID[i];
                classProfileBO = listClassProfile.Where(p => p.ClassProfileID == listClassID[i]).FirstOrDefault();
                subjBOByClassId = listsubjectCatBO.Where(p => p.ClassID == classId).FirstOrDefault();
                sheetByClassID = lstSheets.Where(p => p.Name.Equals(Business.Common.Utils.StripVNSignAndSpace(classProfileBO.DisplayName))).FirstOrDefault();
                listPupilOfClassByClassID = listPupilOfClass.Where(p => p.ClassID == classId).ToList();
                string strMarkTypeLock = MarkTypeLock.Where(p => p.Key == classId).Select(p => p.Value).FirstOrDefault();

                if (sheetByClassID != null)
                {
                    ListPupilByClassID = this.getDataToFileImport(physicalPath, semesterid, classId, subjectID, subjBOByClassId.IsCommenting, semeterDeclaration
                        , strMarkTypePeriod, strMarkTypeLock, listPupilOfClassByClassID, subjBOByClassId, academicYear, sheetByClassID, IQueryExempted, out Error);
                }

                if (ListPupilByClassID != null && ListPupilByClassID.Count > 0)
                {
                    for (int j = 0; j < ListPupilByClassID.Count; j++)
                    {
                        pupilInExcel = ListPupilByClassID[j];
                        pupil = new MarkRecordViewModel();
                        pupil.PupilCode = pupilInExcel.PupilCode;
                        pupil.PupilID = pupilInExcel.PupilID;
                        pupil.Fullname = pupilInExcel.Fullname;
                        pupil.SummedUpMark = pupilInExcel.SummedUpMark;
                        pupil.Status = pupilInExcel.Status;
                        pupil.InterviewMark = pupilInExcel.InterviewMark;
                        pupil.WritingMark = pupilInExcel.WritingMark;
                        pupil.TwiceCoeffiecientMark = pupilInExcel.TwiceCoeffiecientMark;
                        pupil.MarkTBM = pupilInExcel.MarkTBM;
                        pupil.Comment = pupilInExcel.Comment;
                        pupil.Disable = pupilInExcel.Disable;
                        pupil.Pass = pupilInExcel.Pass;
                        pupil.Note = pupilInExcel.Note;
                        pupil.SubjectID = pupilInExcel.SubjectID;
                        pupil.ClassID = pupilInExcel.ClassID;
                        pupil.PeriodID = pupilInExcel.PeriodID;
                        pupil.SemesterID = pupilInExcel.SemesterID;
                        pupil.ListMark = pupilInExcel.ListMark;
                        pupil.SheetName = pupilInExcel.SheetName;
                        pupil.MarkSemester = pupilInExcel.MarkSemester;
                        pupil.AcademicYearID = pupilInExcel.AcademicYearID;
                        pupil.SchoolID = pupilInExcel.SchoolID;
                        pupil.Year = pupilInExcel.Year;
                        pupil.IsCommenting = subjBOByClassId.IsCommenting.HasValue ? subjBOByClassId.IsCommenting.Value : -1;
                        ListPupil.Add(pupil);
                    }
                }
            }
            objResult.Error = string.Empty;
            objResult.listMarkRecordViewModel = ListPupil;
            return objResult;
            #endregion

        }

        public void ViewDataImportExcel()
        {
            List<MarkRecordViewModel> ListPupil = (List<MarkRecordViewModel>)Session["ListMarkRecord"];
            //Đưa thông tin lên MarkRecord_Label_Message

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = new GlobalInfo().SchoolID.Value;
            dic["Semester"] = ListPupil[0].SemesterID.Value;
            dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;

            SemeterDeclaration SemeterDeclaration = SemesterDeclarationBusiness.Search(dic).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();

            string Semester = "";

            if (SemeterDeclaration.Semester == 1)
            {
                Semester = SystemParamsInFile.SEMESTER_I;
            }
            if (SemeterDeclaration.Semester == 2)
            {
                Semester = SystemParamsInFile.SEMESTER_II;
            }

            ViewData[BookMarkRecordConstants.titleImport] = Res.Get("MarkRecord_Label_MarkRecordReportTitle") + " - " + SubjectCatBusiness.Find(ListPupil[0].SubjectID).DisplayName.ToUpper() + " - " + ClassProfileBusiness.Find(ListPupil[0].ClassID).DisplayName.ToUpper() + " - " + Semester.ToUpper();
            ViewData[BookMarkRecordConstants.listMarkRecordViewModel] = ListPupil;

            string MarkType = MarkRecordBusiness.GetMarkSemesterTitle(SemeterDeclaration.SemeterDeclarationID);
            string[] listMarkType = MarkType.Split(new Char[] { ',' });
            List<string> listMarkM = new List<string>();
            List<string> listMarkP = new List<string>();
            List<string> listMarkV = new List<string>();
            foreach (string item in listMarkType)
            {
                if (item.Length < 2)
                    continue;
                else
                {
                    if (item[0] == 'M')
                        listMarkM.Add(item);
                    if (item[0] == 'P')
                        listMarkP.Add(item);
                    if (item[0] == 'V')
                        listMarkV.Add(item);
                }
            }
            ViewData[BookMarkRecordConstants.ListTypeMarkMImport] = listMarkM;
            ViewData[BookMarkRecordConstants.ListTypeMarkPImport] = listMarkP;
            ViewData[BookMarkRecordConstants.ListTypeMarkVImport] = listMarkV;
        }

        public string SetDataInputToDb(List<MarkRecordViewModel> ListPupil = null)
        {

            GlobalInfo glo = new GlobalInfo();
            //Lay cac hoc sinh co trang thai dang hoc va hop le
            ListPupil = ListPupil.Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && u.Pass.HasValue && u.Pass.Value).ToList();
            AcademicYear acaYear = AcademicYearBusiness.Find(glo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
            int classid = 0;
            int semesterid = 0;
            int subjectid = 0;
            int Year = acaYear.Year;
            if (ListPupil != null && ListPupil.Count() > 0)
            {
                classid = ListPupil[0].ClassID.Value;
                semesterid = ListPupil[0].SemesterID.Value;
                classid = ListPupil[0].ClassID.Value;
                subjectid = ListPupil[0].SubjectID.Value;
            }
            else
                return Res.Get("MarkRecord_Label_NotImportDataSuccess");

            SubjectCat sc = SubjectCatBusiness.Find(subjectid);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = glo.AppliedLevel.Value;
            IEnumerable<MarkType> listMarkType = MarkTypeBusiness.Search(dic).ToList();

            List<MarkRecord> listMarkRecord = new List<MarkRecord>();
            List<SummedUpRecord> listSummedUpRecord = new List<SummedUpRecord>();

            // Tạo data ghi log
            List<OldMark> lstOldMark = new List<OldMark>();
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            StringBuilder isInsertLogstr = new StringBuilder();
            StringBuilder inforLog = null;
            StringBuilder oldobjtmp = null;

            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();
            StringBuilder isInsertLogstrtmp = new StringBuilder();
            int iLog = 0;
            bool isInsertLog = false;

            List<int> pupilIds = ListPupil.Select(s => s.PupilID).ToList();
            List<PupilOfClass> lstPOC = (from p in PupilOfClassBusiness.All
                                         where pupilIds.Contains(p.PupilID)
                                         //&& p.Last2digitNumberSchool == (glo.SchoolID % 100)
                                         && p.ClassID == classid
                                         && p.AcademicYearID == acaYear.AcademicYearID
                                         select p).ToList();
            List<MarkRecordHistory> listTempHistory = null;
            List<MarkRecord> listTemp = null;
            if (isMovedHistory)// du lieu lich su
            {
                Dictionary<string, object> Dics = new Dictionary<string, object>();
                Dics.Add("ClassID", classid);
                Dics.Add("AcademicYearID", acaYear.AcademicYearID);
                Dics.Add("SchoolID", acaYear.SchoolID);
                Dics.Add("SubjectID", subjectid);
                Dics.Add("Semester", semesterid);
                listTempHistory = MarkRecordHistoryBusiness.SearchMarkRecordPrimaryHistory(Dics).ToList();
            }
            else
            {
                int modSchoolID = _globalInfo.SchoolID.Value % 100;
                listTemp = (from m in MarkRecordBusiness.All
                            where
                            m.Last2digitNumberSchool == modSchoolID
                            && m.AcademicYearID == acaYear.AcademicYearID
                            && m.Semester == semesterid && m.SubjectID == subjectid
                            && m.ClassID == classid
                            select m).ToList();
            }
            string markString = string.Empty;
            decimal mark = 0;
            bool isChangeMark = false;
            bool ParseMark = false;
            OldMark objOM = null;
            foreach (MarkRecordViewModel model in ListPupil)
            {
                Dictionary<string, string> listMark = model.ListMark;
                isInsertLog = false;
                lstOldMark = new List<OldMark>();
                // Tạo dữ liệu ghi log
                int id = model.PupilID;
                PupilOfClass pop = lstPOC.FirstOrDefault(p => p.PupilID == id && p.ClassID == model.ClassID);
                inforLog = new StringBuilder();
                oldobjtmp = new StringBuilder();
                inforLog.Append("Import điểm HS " + pop.PupilProfile.FullName);
                inforLog.Append(", mã " + pop.PupilProfile.PupilCode);
                inforLog.Append(", Lớp " + pop.ClassProfile.DisplayName);
                inforLog.Append("/" + sc.SubjectName);
                inforLog.Append("/Học kỳ " + semesterid);
                inforLog.Append("/" + pop.Year.Value + "-" + (pop.Year.Value + 1));
                newObjectStrtmp = new StringBuilder();
                if (isMovedHistory)//Du lieu lich su
                {
                    if (listTempHistory != null)
                    {
                        List<MarkRecordHistory> listOldRecord = listTempHistory.Where(p => p.PupilID == id && p.ClassID == model.ClassID && p.SubjectID == subjectid && p.Semester == semesterid).ToList();
                        oldobjtmp.Append("(Giá trị trước khi sửa): {");
                        for (int j = 0; j < listOldRecord.Count; j++)
                        {
                            MarkRecordHistory record = listOldRecord[j];
                            OldMark objOldMark = new OldMark();
                            oldobjtmp.Append(record.Title + ":" + decimal.Parse(Business.Common.Utils.FormatMark(record.Mark)));
                            objOldMark.MarkRecordID = record.MarkRecordID;
                            objOldMark.Title = record.Title;
                            objOldMark.Mark = decimal.Parse(Business.Common.Utils.FormatMark(record.Mark));
                            lstOldMark.Add(objOldMark);
                            if (j < listOldRecord.Count - 1)
                            {
                                oldobjtmp.Append(",");
                            }
                        }
                        oldobjtmp.Append("}");
                    }
                }
                else
                {
                    if (listTemp != null && listTemp.Count > 0)
                    {
                        List<MarkRecord> listOldRecord = listTemp.Where(p => p.PupilID == id && p.ClassID == model.ClassID && p.SubjectID == subjectid && p.Semester == semesterid).ToList();
                        oldobjtmp.Append("(Giá trị trước khi sửa): {");
                        for (int j = 0; j < listOldRecord.Count; j++)
                        {
                            MarkRecord record = listOldRecord[j];
                            OldMark objOldMark = new OldMark();
                            oldobjtmp.Append(record.Title + ":" + decimal.Parse(Business.Common.Utils.FormatMark(record.Mark)));
                            objOldMark.MarkRecordID = record.MarkRecordID;
                            objOldMark.Title = record.Title;
                            objOldMark.Mark = decimal.Parse(Business.Common.Utils.FormatMark(record.Mark));
                            lstOldMark.Add(objOldMark);
                            if (j < listOldRecord.Count - 1)
                            {
                                oldobjtmp.Append(",");
                            }
                        }
                        oldobjtmp.Append("}");
                    }
                }
                descriptionStrtmp.Append("Update mark record pupil_id:" + id);
                paramsStrtmp.Append("pupil_id:" + id);
                userFuntionsStrtmp.Append("Sổ điểm");
                userActionsStrtmp.Append(GlobalConstants.ACTION_IMPORT);
                userDescriptionsStrtmp.Append(inforLog.ToString()).Append(": ");
                objectIDStrtmp.Append(id);
                oldObjectStrtmp.Append(oldobjtmp);
                newObjectStrtmp.Append("(Giá trị sau khi sửa): {");
                // end
                foreach (string key in listMark.Keys)
                {
                    isChangeMark = false;
                    markString = listMark[key].Replace(".", ",");
                    MarkRecord mr = new MarkRecord();
                    objOM = null;
                    mr.PupilID = model.PupilID;
                    mr.ClassID = classid;
                    mr.AcademicYearID = glo.AcademicYearID.Value;
                    mr.SchoolID = glo.SchoolID.Value;
                    mr.SubjectID = subjectid;
                    mr.MarkedDate = DateTime.Now.Date;
                    mr.CreatedAcademicYear = acaYear.Year;
                    mr.Semester = semesterid;
                    mr.Title = key;
                    mark = 0;
                    mr.OrderNumber = int.Parse((key.Substring(1).ToString()));
                    mr.MarkTypeID = listMarkType.Where(o => key.Contains(o.Title)).FirstOrDefault().MarkTypeID;
                    ParseMark = Decimal.TryParse(markString, out mark);
                    if (lstOldMark.Count > 0)
                    {
                        objOM = lstOldMark.Where(p => p.Title.Equals(mr.Title)).FirstOrDefault();
                        if (objOM != null)
                        {
                            if (objOM.Mark > -1 && objOM.Mark != mark)
                            {
                                isChangeMark = true;
                            }
                        }
                        else if (!string.IsNullOrEmpty(markString))
                        {
                            isChangeMark = true;
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(markString))
                        {
                            isChangeMark = true;
                        }
                    }


                    if (isChangeMark)
                    {
                        if (objOM != null)
                        {
                            userDescriptionsStrtmp.Append(mr.Title + "(" + objOM.Mark + ":");
                            mr.MarkRecordID = objOM.MarkRecordID;
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append(mr.Title + "(");
                        }

                        if (ParseMark)
                        {
                            mr.Mark = Business.Common.Utils.RoundMark(mark);
                            userDescriptionsStrtmp.Append(mr.Mark + "); ");
                            newObjectStrtmp.Append(mr.Title + ":" + mr.Mark);
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append("); ");
                            newObjectStrtmp.Append(mr.Title + ":");
                            mr.Mark = -1;
                        }
                        newObjectStrtmp.Append(", ");
                        isInsertLog = true;
                    }
                    else
                    {
                        mr.Mark = -1;
                    }
                    listMarkRecord.Add(mr);
                }

                //dau diem kthk        
                isChangeMark = false;
                string markSemsesterString = model.MarkSemester.Replace(".", ",");
                MarkRecord mar = new MarkRecord();
                objOM = null;
                mar.PupilID = model.PupilID;
                mar.ClassID = classid;
                mar.AcademicYearID = glo.AcademicYearID.Value;
                mar.SchoolID = glo.SchoolID.Value;
                mar.SubjectID = (int)subjectid;
                mar.MarkedDate = DateTime.Now.Date;
                mar.CreatedAcademicYear = acaYear.Year;
                mar.Semester = (int)semesterid;
                mar.Title = "HK";
                mar.MarkTypeID = listMarkType.Where(o => o.Title == "HK").FirstOrDefault().MarkTypeID;
                mar.OrderNumber = 1;
                //mar.LogChange = "Cập nhật " + DateTime.Now.ToString("hh:mm dd/MM/yyyy") + " bởi " + (_globalInfo.IsAdmin ? "Quản trị trường" : _globalInfo.EmployeeName);
                decimal markSemester = 0;
                ParseMark = Decimal.TryParse(markSemsesterString, out markSemester);
                if (lstOldMark.Count > 0)
                {
                    objOM = lstOldMark.Where(p => p.Title.Equals(mar.Title)).FirstOrDefault();
                    if (objOM != null)
                    {
                        if (objOM.Mark > -1 && objOM.Mark != markSemester)
                        {
                            isChangeMark = true;
                        }
                    }
                    else if (!string.IsNullOrEmpty(markSemsesterString))
                    {
                        isChangeMark = true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(markSemsesterString))
                    {
                        isChangeMark = true;
                    }
                }


                if (isChangeMark)
                {
                    if (objOM != null)
                    {
                        userDescriptionsStrtmp.Append(mar.Title + "(" + objOM.Mark + ":");
                        mar.MarkRecordID = objOM.MarkRecordID;
                    }
                    else
                    {
                        userDescriptionsStrtmp.Append(mar.Title + "(");
                    }

                    if (ParseMark)
                    {
                        mar.Mark = Business.Common.Utils.RoundMark(markSemester);
                        userDescriptionsStrtmp.Append(mar.Mark + "); ");
                        newObjectStrtmp.Append(mar.Title + ":" + mar.Mark);
                    }
                    else
                    {
                        userDescriptionsStrtmp.Append("); ");
                        newObjectStrtmp.Append(mar.Title + ":");
                        mar.Mark = -1;
                    }
                    newObjectStrtmp.Append(", ");
                    isInsertLog = true;
                }
                else
                {
                    mar.Mark = -1;
                }
                listMarkRecord.Add(mar);
                //-----------------Doi tuong sumrecord
                SummedUpRecord sur = new SummedUpRecord();
                sur.PupilID = model.PupilID;
                sur.ClassID = classid;
                sur.AcademicYearID = glo.AcademicYearID.Value;
                sur.SchoolID = glo.SchoolID.Value;
                sur.SubjectID = (int)subjectid;
                sur.CreatedAcademicYear = acaYear.Year;
                sur.Semester = (int)semesterid;
                sur.Comment = sc.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE ? model.Comment : null;

                string strMarkAvg = model.MarkTBM.Replace(".", ",");
                decimal markAvg = 0;
                if (Decimal.TryParse(strMarkAvg, out markAvg))
                {
                    sur.SummedUpMark = markAvg;
                }
                listSummedUpRecord.Add(sur);

                // Tạo dữ liệu ghi log
                if (isInsertLog)
                {
                    string newObj = string.Empty;
                    newObj = newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) + "}";
                    newObj += GlobalConstants.WILD_LOG;
                    string tmp = string.Empty;
                    objectIDStrtmp.Append(GlobalConstants.WILD_LOG);
                    descriptionStrtmp.Append(GlobalConstants.WILD_LOG);
                    paramsStrtmp.Append(GlobalConstants.WILD_LOG);
                    oldObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                    newObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                    userFuntionsStrtmp.Append(GlobalConstants.WILD_LOG);
                    userActionsStrtmp.Append(GlobalConstants.WILD_LOG);
                    tmp = userDescriptionsStrtmp != null ? userDescriptionsStrtmp.ToString().Substring(0, userDescriptionsStrtmp.Length - 2) : "";
                    tmp += GlobalConstants.WILD_LOG;
                    //userDescriptionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                    objectIDStr.Append(objectIDStrtmp);
                    descriptionStr.Append(descriptionStrtmp);
                    paramsStr.Append(paramsStrtmp);
                    oldObjectStr.Append(oldObjectStrtmp);
                    newObjectStr.Append(newObj);
                    userFuntionsStr.Append(userFuntionsStrtmp);
                    userActionsStr.Append(userActionsStrtmp);
                    userDescriptionsStr.Append(tmp);
                }

                objectIDStrtmp = new StringBuilder();
                descriptionStrtmp = new StringBuilder();
                paramsStrtmp = new StringBuilder();
                oldObjectStrtmp = new StringBuilder();
                newObjectStrtmp = new StringBuilder();
                userFuntionsStrtmp = new StringBuilder();
                userActionsStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
                isInsertLogstrtmp = new StringBuilder();
                iLog++;
            }

            //if (listMarkRecord.Count > 0)
            //{
            if (isMovedHistory)
            {
                List<MarkRecordHistory> lstMarkRecordHistory = new List<MarkRecordHistory>();
                List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                MarkRecordHistory objMarkRecordHistory = null;
                MarkRecord objMarkRecord = null;
                SummedUpRecord objSummedUpRecord = null;
                SummedUpRecordHistory objSummedUpRecordHistory = null;
                for (int i = 0; i < listMarkRecord.Count; i++)
                {
                    objMarkRecordHistory = new MarkRecordHistory();
                    objMarkRecord = new MarkRecord();
                    objMarkRecord = listMarkRecord[i];
                    objMarkRecordHistory.MarkRecordID = objMarkRecord.MarkRecordID;
                    objMarkRecordHistory.AcademicYearID = objMarkRecord.AcademicYearID;
                    objMarkRecordHistory.PupilID = objMarkRecord.PupilID;
                    objMarkRecordHistory.SchoolID = objMarkRecord.SchoolID;
                    objMarkRecordHistory.ClassID = objMarkRecord.ClassID;
                    objMarkRecordHistory.Mark = objMarkRecord.Mark;
                    objMarkRecordHistory.MarkTypeID = objMarkRecord.MarkTypeID;
                    objMarkRecordHistory.Semester = objMarkRecord.Semester;
                    objMarkRecordHistory.Title = objMarkRecord.Title;
                    objMarkRecordHistory.Last2digitNumberSchool = objMarkRecord.Last2digitNumberSchool;
                    objMarkRecordHistory.MarkedDate = objMarkRecord.MarkedDate;
                    objMarkRecordHistory.ModifiedDate = objMarkRecord.ModifiedDate;
                    objMarkRecordHistory.SubjectID = objMarkRecord.SubjectID;
                    objMarkRecordHistory.CreatedDate = objMarkRecord.CreatedDate;
                    objMarkRecordHistory.CreatedAcademicYear = objMarkRecord.CreatedAcademicYear;
                    objMarkRecordHistory.OrderNumber = objMarkRecord.OrderNumber;
                    lstMarkRecordHistory.Add(objMarkRecordHistory);
                }
                for (int i = 0; i < listSummedUpRecord.Count; i++)
                {
                    objSummedUpRecord = new SummedUpRecord();
                    objSummedUpRecordHistory = new SummedUpRecordHistory();
                    objSummedUpRecord = listSummedUpRecord[i];
                    objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                    objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                    objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                    objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                    objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                    objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                    objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                    objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                    objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                    objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                    objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                    objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                    objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                    objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                    objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                    objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                    objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                    lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                }
                MarkRecordHistoryBusiness.InsertMarkRecordHistory(glo.UserAccountID, lstMarkRecordHistory, lstSummedUpRecordHistory, semesterid, null, null, glo.SchoolID.Value, glo.AcademicYearID.Value, glo.AppliedLevel.Value, classid, subjectid, _globalInfo.EmployeeID, Year);

            }
            else
            {
                MarkRecordBusiness.InsertMarkRecord(glo.UserAccountID, listMarkRecord, listSummedUpRecord, semesterid, null, null, glo.SchoolID.Value, glo.AcademicYearID.Value, glo.AppliedLevel.Value, classid, subjectid, _globalInfo.EmployeeID, Year);

            }


            // Tạo dữ liệu ghi log
            IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
            SetViewDataActionAudit(dicLog);
            return Res.Get("MarkRecord_Label_ImportDataSuccess");
        }


        public string SetDataInputToDbByListSubject(int ClassID, int semesterId, List<PupilOfClass> lstPupilOfClass, List<MarkRecordViewModel> ListPupil = null)
        {
            if (ListPupil == null || ListPupil.Count == 0)
            {
                return Res.Get("MarkRecord_Label_NotImportDataSuccess");
            }
            GlobalInfo glo = new GlobalInfo();

            List<int> listSubjectId = ListPupil.Where(p => p.SubjectID.HasValue && p.SubjectID.Value > 0).Select(p => p.SubjectID.Value).Distinct().ToList();
            //Sap xep mon tang cuong truoc
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.GetListSubjectByClass(_globalInfo.AcademicYearID.Value, ClassID, listSubjectId);
            List<int> listSubjectIDIncrease = new List<int>();
            if (listClassSubject != null && listClassSubject.Count > 0)
            {
                listSubjectIDIncrease = listClassSubject.Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease.Value > 0).Select(p => p.SubjectIDIncrease.Value).ToList();
                listSubjectId = listSubjectIDIncrease.Union(listSubjectId).Distinct().ToList();
            }
            AcademicYear acaYear = AcademicYearBusiness.Find(glo.AcademicYearID.Value);
            //Lay cac hoc sinh co trang thai dang hoc va hop le
            ListPupil = ListPupil.Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && u.Pass.HasValue && u.Pass.Value).ToList();

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = glo.AppliedLevel.Value;
            IEnumerable<MarkType> listMarkType = MarkTypeBusiness.Search(dic).ToList();

            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
            List<MarkRecord> listMarkRecord = new List<MarkRecord>();
            List<SummedUpRecord> listSummedUpRecord = new List<SummedUpRecord>();

            List<SubjectCat> lstSubjectCat = (from sc in SubjectCatBusiness.All
                                              where sc.AppliedLevel == _globalInfo.AppliedLevel
                                              && listSubjectId.Contains(sc.SubjectCatID)
                                              select sc).ToList();
            SubjectCat objSC = null;

            List<MarkRecordHistory> listTempHistory = null;
            List<MarkRecord> listTemp = null;
            // Tạo data ghi log
            List<OldMark> lstOldMark = new List<OldMark>();
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            StringBuilder isInsertLogstr = new StringBuilder();
            StringBuilder inforLog = null;
            StringBuilder oldobjtmp = null;

            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();
            StringBuilder isInsertLogstrtmp = new StringBuilder();
            bool isInsertLog = false;
            int iLog = 0;

            if (isMovedHistory)
            {
                Dictionary<string, object> Dic = new Dictionary<string, object>();
                Dic.Add("ClassID", ClassID);
                Dic.Add("AcademicYearID", acaYear.AcademicYearID);
                Dic.Add("SchoolID", acaYear.SchoolID);
                Dic.Add("Semester", semesterId);
                listTempHistory = MarkRecordHistoryBusiness.SearchMarkRecordPrimaryHistory(Dic).Where(p => listSubjectId.Contains(p.SubjectID)).ToList();
            }
            else
            {
                int modSchoolID = _globalInfo.SchoolID.Value % 100;
                listTemp = (from m in MarkRecordBusiness.All
                            where
                            m.Last2digitNumberSchool == modSchoolID
                            && m.AcademicYearID == acaYear.AcademicYearID
                            && m.Semester == semesterId
                            && m.ClassID == ClassID
                            && listSubjectId.Contains(m.SubjectID)
                            select m).ToList();
            }
            bool isChangeMark = false;
            bool ParseMark = false;

            for (int i = 0; i < ListPupil.Count; i++)
            {
                isInsertLog = false;
                lstOldMark = new List<OldMark>();
                int classid = ListPupil[i].ClassID.Value;
                int semesterid = ListPupil[i].SemesterID.Value;
                int subjectid = ListPupil[i].SubjectID.Value;
                int Year = acaYear.Year;
                int pupilId = ListPupil[i].PupilID;
                string marktmp = string.Empty;
                Dictionary<string, string> listMark = ListPupil[i].ListMark;

                inforLog = new StringBuilder();
                oldobjtmp = new StringBuilder();
                PupilOfClass pop = lstPupilOfClass.Where(p => p.PupilID == pupilId).FirstOrDefault();
                objSC = lstSubjectCat.Where(p => p.SubjectCatID == ListPupil[i].SubjectID).FirstOrDefault();
                inforLog.Append("Import điểm HS " + pop.PupilProfile.FullName);
                inforLog.Append(", mã " + pop.PupilProfile.PupilCode);
                inforLog.Append(", Lớp " + pop.ClassProfile.DisplayName);
                inforLog.Append("/" + objSC.SubjectName);
                inforLog.Append("/Học kỳ " + semesterid);
                inforLog.Append("/" + pop.Year.Value + "-" + (pop.Year.Value + 1));
                newObjectStrtmp = new StringBuilder();
                if (isMovedHistory)
                {
                    if (listTempHistory != null)
                    {
                        List<MarkRecordHistory> listOldRecord = listTempHistory.Where(p => p.PupilID == pupilId && p.SubjectID == objSC.SubjectCatID && p.ClassID == classid && p.Semester == semesterid).ToList();
                        oldobjtmp.Append("(Giá trị trước khi sửa): {");
                        for (int j = 0; j < listOldRecord.Count; j++)
                        {
                            MarkRecordHistory record = listOldRecord[j];
                            OldMark objOldMark = new OldMark();
                            oldobjtmp.Append(record.Title + ":" + decimal.Parse(Business.Common.Utils.FormatMark(record.Mark)));
                            objOldMark.MarkRecordID = record.MarkRecordID;
                            objOldMark.Title = record.Title;
                            objOldMark.Mark = decimal.Parse(Business.Common.Utils.FormatMark(record.Mark));
                            lstOldMark.Add(objOldMark);
                            if (j < listOldRecord.Count - 1)
                            {
                                oldobjtmp.Append(",");
                            }
                        }
                        oldobjtmp.Append("}");
                    }
                }
                else
                {
                    if (listTemp != null && listTemp.Count > 0)
                    {
                        List<MarkRecord> listOldRecord = listTemp.Where(p => p.PupilID == pupilId && p.SubjectID == objSC.SubjectCatID && p.ClassID == classid && p.Semester == semesterid).ToList();
                        oldobjtmp.Append("(Giá trị trước khi sửa): {");
                        for (int j = 0; j < listOldRecord.Count; j++)
                        {
                            MarkRecord record = listOldRecord[j];
                            OldMark objOldMark = new OldMark();
                            oldobjtmp.Append(record.Title + ":" + decimal.Parse(Business.Common.Utils.FormatMark(record.Mark)));
                            objOldMark.MarkRecordID = record.MarkRecordID;
                            objOldMark.Title = record.Title;
                            objOldMark.Mark = decimal.Parse(Business.Common.Utils.FormatMark(record.Mark));
                            lstOldMark.Add(objOldMark);
                            if (j < listOldRecord.Count - 1)
                            {
                                oldobjtmp.Append(",");
                            }
                        }
                        oldobjtmp.Append("}");
                    }
                }
                descriptionStrtmp.Append("Update mark record pupil_id:" + pupilId);
                paramsStrtmp.Append("pupil_id:" + pupilId);
                userFuntionsStrtmp.Append("Sổ điểm");
                userActionsStrtmp.Append(GlobalConstants.ACTION_IMPORT);
                userDescriptionsStrtmp.Append(inforLog.ToString()).Append(": ");
                objectIDStrtmp.Append(pupilId);
                oldObjectStrtmp.Append(oldobjtmp);
                newObjectStrtmp.Append("(Giá trị sau khi sửa): {");
                // end
                OldMark objOM = null;
                foreach (string key in listMark.Keys)
                {
                    isChangeMark = false;
                    string markString = listMark[key].Replace(".", ",");
                    MarkRecord mr = new MarkRecord();
                    objOM = null;
                    mr.PupilID = pupilId;
                    mr.ClassID = classid;
                    mr.AcademicYearID = glo.AcademicYearID.Value;
                    mr.SchoolID = glo.SchoolID.Value;
                    mr.SubjectID = (int)subjectid;
                    mr.MarkedDate = DateTime.Now.Date;
                    mr.CreatedAcademicYear = acaYear.Year;
                    mr.Semester = (int)semesterid;
                    mr.OrderNumber = int.Parse((key.Substring(1).ToString()));
                    mr.MarkTypeID = listMarkType.Where(o => key.Contains(o.Title)).FirstOrDefault().MarkTypeID;
                    mr.Title = key;
                    decimal mark = 0;

                    ParseMark = Decimal.TryParse(markString, out mark);
                    if (lstOldMark.Count > 0)
                    {
                        objOM = lstOldMark.Where(p => p.Title.Equals(mr.Title)).FirstOrDefault();
                        if (objOM != null)
                        {
                            if (objOM.Mark > -1 && objOM.Mark != mark)
                            {
                                isChangeMark = true;
                            }
                        }
                        else if (!string.IsNullOrEmpty(markString))
                        {
                            isChangeMark = true;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(markString))
                        {
                            isChangeMark = true;
                        }
                    }


                    if (isChangeMark)
                    {
                        if (objOM != null)
                        {
                            userDescriptionsStrtmp.Append(mr.Title + "(" + objOM.Mark + ":");
                            mr.MarkRecordID = objOM.MarkRecordID;
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append(mr.Title + "(");
                        }

                        if (ParseMark)
                        {
                            mr.Mark = Business.Common.Utils.RoundMark(mark);
                            userDescriptionsStrtmp.Append(mr.Mark + "); ");
                            newObjectStrtmp.Append(mr.Title + ":" + mr.Mark);
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append("); ");
                            newObjectStrtmp.Append(mr.Title + ":");
                            mr.Mark = -1;
                        }
                        newObjectStrtmp.Append(", ");
                        isInsertLog = true;
                    }
                    else
                    {
                        mr.Mark = -1;
                    }
                    listMarkRecord.Add(mr);
                }

                //dau diem kthk
                isChangeMark = false;
                string markSemsesterString = ListPupil[i].MarkSemester.Replace(".", ",");
                MarkRecord mar = new MarkRecord();
                objOM = null;
                mar.PupilID = pupilId;
                mar.ClassID = classid;
                mar.AcademicYearID = glo.AcademicYearID.Value;
                mar.SchoolID = glo.SchoolID.Value;
                mar.SubjectID = (int)subjectid;
                mar.MarkedDate = DateTime.Now.Date;
                mar.CreatedAcademicYear = acaYear.Year;
                mar.Semester = (int)semesterid;
                mar.Title = "HK";
                mar.MarkTypeID = listMarkType.Where(o => o.Title == "HK").FirstOrDefault().MarkTypeID;
                mar.OrderNumber = 1;
                //mar.LogChange = "Cập nhật " + DateTime.Now.ToString("hh:mm dd/MM/yyyy") + " bởi " + (_globalInfo.IsAdmin ? "Quản trị trường" : _globalInfo.EmployeeName);
                decimal markSemester = 0;

                ParseMark = Decimal.TryParse(markSemsesterString, out markSemester);
                if (lstOldMark.Count > 0)
                {
                    objOM = lstOldMark.Where(p => p.Title.Equals(mar.Title)).FirstOrDefault();
                    if (objOM != null)
                    {
                        if (objOM.Mark > -1 && objOM.Mark != markSemester)
                        {
                            isChangeMark = true;
                        }
                    }
                    else if (!string.IsNullOrEmpty(markSemsesterString))
                    {
                        isChangeMark = true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(markSemsesterString))
                    {
                        isChangeMark = true;
                    }
                }


                if (isChangeMark)
                {
                    if (objOM != null)
                    {
                        userDescriptionsStrtmp.Append(mar.Title + "(" + objOM.Mark + ":");
                        mar.MarkRecordID = objOM.MarkRecordID;
                    }
                    else
                    {
                        userDescriptionsStrtmp.Append(mar.Title + "(");
                    }

                    if (ParseMark)
                    {
                        mar.Mark = Business.Common.Utils.RoundMark(markSemester);
                        userDescriptionsStrtmp.Append(mar.Mark + "); ");
                        newObjectStrtmp.Append(mar.Title + ":" + mar.Mark);
                    }
                    else
                    {
                        userDescriptionsStrtmp.Append("); ");
                        newObjectStrtmp.Append(mar.Title + ":");
                        mar.Mark = -1;
                    }
                    newObjectStrtmp.Append(", ");
                    isInsertLog = true;
                }
                else
                {
                    mar.Mark = -1;
                }
                listMarkRecord.Add(mar);
                //-----------------Doi tuong sumrecord
                SummedUpRecord sur = new SummedUpRecord();
                sur.PupilID = pupilId;
                sur.ClassID = classid;
                sur.AcademicYearID = glo.AcademicYearID.Value;
                sur.SchoolID = glo.SchoolID.Value;
                sur.SubjectID = (int)subjectid;
                sur.CreatedAcademicYear = acaYear.Year;
                sur.Semester = (int)semesterid;
                sur.Comment = ListPupil[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE ? ListPupil[i].Comment : null;

                string strMarkAvg = ListPupil[i].MarkTBM.Replace(".", ",");
                decimal markAvg = 0;
                if (Decimal.TryParse(strMarkAvg, out markAvg))
                {
                    sur.SummedUpMark = markAvg;
                }
                listSummedUpRecord.Add(sur);
                // Tạo dữ liệu ghi log
                if (isInsertLog)
                {
                    string newObj = string.Empty;
                    newObj = newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) + "}";
                    newObj += GlobalConstants.WILD_LOG;
                    string tmp = string.Empty;
                    objectIDStrtmp.Append(GlobalConstants.WILD_LOG);
                    descriptionStrtmp.Append(GlobalConstants.WILD_LOG);
                    paramsStrtmp.Append(GlobalConstants.WILD_LOG);
                    oldObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                    newObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                    userFuntionsStrtmp.Append(GlobalConstants.WILD_LOG);
                    userActionsStrtmp.Append(GlobalConstants.WILD_LOG);
                    tmp = userDescriptionsStrtmp != null ? userDescriptionsStrtmp.ToString().Substring(0, userDescriptionsStrtmp.Length - 2) : "";
                    tmp += GlobalConstants.WILD_LOG;
                    //userDescriptionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                    objectIDStr.Append(objectIDStrtmp);
                    descriptionStr.Append(descriptionStrtmp);
                    paramsStr.Append(paramsStrtmp);
                    oldObjectStr.Append(oldObjectStrtmp);
                    newObjectStr.Append(newObj);
                    userFuntionsStr.Append(userFuntionsStrtmp);
                    userActionsStr.Append(userActionsStrtmp);
                    userDescriptionsStr.Append(tmp);
                }

                objectIDStrtmp = new StringBuilder();
                descriptionStrtmp = new StringBuilder();
                paramsStrtmp = new StringBuilder();
                oldObjectStrtmp = new StringBuilder();
                newObjectStrtmp = new StringBuilder();
                userFuntionsStrtmp = new StringBuilder();
                userActionsStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
                isInsertLogstrtmp = new StringBuilder();
                iLog++;
            }

            if (isMovedHistory)
            {
                List<MarkRecordHistory> lstMarkRecordHistory = new List<MarkRecordHistory>();
                List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                MarkRecordHistory objMarkRecordHistory = null;
                MarkRecord objMarkRecord = null;
                SummedUpRecord objSummedUpRecord = null;
                SummedUpRecordHistory objSummedUpRecordHistory = null;
                for (int i = 0; i < listMarkRecord.Count; i++)
                {
                    objMarkRecordHistory = new MarkRecordHistory();
                    objMarkRecord = new MarkRecord();
                    objMarkRecord = listMarkRecord[i];
                    objMarkRecordHistory.MarkRecordID = objMarkRecord.MarkRecordID;
                    objMarkRecordHistory.AcademicYearID = objMarkRecord.AcademicYearID;
                    objMarkRecordHistory.PupilID = objMarkRecord.PupilID;
                    objMarkRecordHistory.SchoolID = objMarkRecord.SchoolID;
                    objMarkRecordHistory.ClassID = objMarkRecord.ClassID;
                    objMarkRecordHistory.Mark = objMarkRecord.Mark;
                    objMarkRecordHistory.MarkTypeID = objMarkRecord.MarkTypeID;
                    objMarkRecordHistory.Semester = objMarkRecord.Semester;
                    objMarkRecordHistory.Title = objMarkRecord.Title;
                    objMarkRecordHistory.Last2digitNumberSchool = objMarkRecord.Last2digitNumberSchool;
                    objMarkRecordHistory.MarkedDate = objMarkRecord.MarkedDate;
                    objMarkRecordHistory.ModifiedDate = objMarkRecord.ModifiedDate;
                    objMarkRecordHistory.SubjectID = objMarkRecord.SubjectID;
                    objMarkRecordHistory.CreatedDate = objMarkRecord.CreatedDate;
                    objMarkRecordHistory.CreatedAcademicYear = objMarkRecord.CreatedAcademicYear;
                    objMarkRecordHistory.OrderNumber = objMarkRecord.OrderNumber;
                    lstMarkRecordHistory.Add(objMarkRecordHistory);
                }
                for (int ik = 0; ik < listSummedUpRecord.Count; ik++)
                {
                    objSummedUpRecord = new SummedUpRecord();
                    objSummedUpRecordHistory = new SummedUpRecordHistory();
                    objSummedUpRecord = listSummedUpRecord[ik];
                    objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                    objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                    objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                    objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                    objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                    objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                    objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                    objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                    objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                    objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                    objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                    objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                    objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                    objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                    objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                    objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                    objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                    lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                }
                MarkRecordHistoryBusiness.InsertMarkRecordHistoryBySubjectID(glo.UserAccountID, lstMarkRecordHistory, lstSummedUpRecordHistory, semesterId, null, null, glo.SchoolID.Value, glo.AcademicYearID.Value, glo.AppliedLevel.Value, ClassID, listSubjectId, _globalInfo.EmployeeID, acaYear.Year);
            }
            else
            {
                MarkRecordBusiness.InsertMarkRecordByListSubject(glo.UserAccountID, listMarkRecord, listSummedUpRecord, semesterId, null, glo.SchoolID.Value, glo.AcademicYearID.Value, glo.AppliedLevel.Value, ClassID, listSubjectId, _globalInfo.EmployeeID, null, acaYear.Year);
            }
            IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
            SetViewDataActionAudit(dicLog);

            return Res.Get("MarkRecord_Label_ImportDataSuccess");


        }


        public string SetDataInputToDbByListClassID(int SubjectID, int semesterId, List<MarkRecordViewModel> ListPupil = null)
        {
            GlobalInfo glo = new GlobalInfo();
            if (ListPupil != null && ListPupil.Count > 0)
            {
                List<int> listClassId = ListPupil.Where(p => p.ClassID.HasValue && p.ClassID.Value > 0).Select(p => p.ClassID.Value).Distinct().ToList();
                AcademicYear acaYear = AcademicYearBusiness.Find(glo.AcademicYearID.Value);
                //Lay cac hoc sinh co trang thai dang hoc va hop le
                ListPupil = ListPupil.Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && u.Pass.HasValue && u.Pass.Value).ToList();
                List<ClassProfile> lstCP = (from cp in ClassProfileBusiness.All
                                            where cp.AcademicYearID == _globalInfo.AcademicYearID
                                            && cp.SchoolID == _globalInfo.SchoolID
                                            && listClassId.Contains(cp.ClassProfileID)
                                            && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                            select cp).ToList();
                ClassProfile objCP = null;

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AppliedLevel"] = glo.AppliedLevel.Value;
                IEnumerable<MarkType> listMarkType = MarkTypeBusiness.Search(dic).ToList();
                SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
                bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
                List<MarkRecordHistory> listTempMarkRecordHistory = new List<MarkRecordHistory>();
                List<MarkRecord> listTempMarkRecord = new List<MarkRecord>();
                if (isMovedHistory)
                {
                    Dictionary<string, object> Dics = new Dictionary<string, object>();
                    Dics.Add("AcademicYearID", acaYear.AcademicYearID);
                    Dics.Add("SchoolID", acaYear.SchoolID);
                    Dics.Add("Semester", semesterId);
                    Dics.Add("SubjectID", SubjectID);
                    listTempMarkRecordHistory = MarkRecordHistoryBusiness.SearchMarkRecordPrimaryHistory(Dics).Where(p => listClassId.Contains(p.ClassID)).ToList();
                }
                else
                {
                    int modSchoolID = _globalInfo.SchoolID.Value % 100;
                    listTempMarkRecord = (from m in MarkRecordBusiness.All
                                          where
                                          m.Last2digitNumberSchool == modSchoolID
                                          && m.AcademicYearID == acaYear.AcademicYearID
                                          && m.Semester == semesterId
                                          && m.SubjectID == SubjectID
                                          && listClassId.Contains(m.ClassID)
                                          select m).ToList();
                }
                // Tạo data ghi log
                List<OldMark> lstOldMark = new List<OldMark>();
                StringBuilder objectIDStr = new StringBuilder();
                StringBuilder descriptionStr = new StringBuilder();
                StringBuilder paramsStr = new StringBuilder();
                StringBuilder oldObjectStr = new StringBuilder();
                StringBuilder newObjectStr = new StringBuilder();
                StringBuilder userFuntionsStr = new StringBuilder();
                StringBuilder userActionsStr = new StringBuilder();
                StringBuilder userDescriptionsStr = new StringBuilder();
                StringBuilder isInsertLogstr = new StringBuilder();
                StringBuilder inforLog = null;
                StringBuilder oldobjtmp = null;

                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                StringBuilder isInsertLogstrtmp = new StringBuilder();
                int iLog = 0;
                bool isInsertLog = false;
                List<MarkRecord> listMarkRecord = new List<MarkRecord>();
                List<SummedUpRecord> listSummedUpRecord = new List<SummedUpRecord>();
                string marktmp = string.Empty;
                string markString = string.Empty;
                decimal mark = 0;
                bool isChangeMark = false;
                bool ParseMark = false;
                OldMark objOM = null;
                MarkRecordViewModel objVM = null;
                for (int i = 0; i < ListPupil.Count; i++)
                {
                    isInsertLog = false;
                    lstOldMark = new List<OldMark>();
                    objVM = ListPupil[i];
                    int classid = objVM.ClassID.Value;
                    int semesterid = objVM.SemesterID.Value;
                    int subjectid = objVM.SubjectID.Value;
                    int Year = acaYear.Year;
                    int pupilId = objVM.PupilID;
                    List<MarkRecordHistory> listTempHistory = null;
                    List<MarkRecord> listTemp = null;
                    if (isMovedHistory)
                    {
                        listTempHistory = listTempMarkRecordHistory.Where(p => p.ClassID == classid && p.PupilID == pupilId).ToList(); ;
                    }
                    else
                    {
                        listTemp = listTempMarkRecord.Where(p => p.ClassID == classid && p.PupilID == pupilId).ToList();
                    }

                    Dictionary<string, string> listMark = ListPupil[i].ListMark;
                    // Tạo dữ liệu ghi log                        
                    inforLog = new StringBuilder();
                    oldobjtmp = new StringBuilder();
                    objCP = lstCP.Where(p => p.ClassProfileID == classid).FirstOrDefault();
                    inforLog.Append("Import điểm HS " + objVM.Fullname);
                    inforLog.Append(", mã " + objVM.PupilCode);
                    inforLog.Append(", Lớp " + objCP.DisplayName);
                    inforLog.Append("/" + objSC.SubjectName);
                    inforLog.Append("/Học kỳ " + semesterid);
                    inforLog.Append("/" + acaYear.Year + "-" + (acaYear.Year + 1));
                    newObjectStrtmp = new StringBuilder();
                    if (isMovedHistory)
                    {
                        if (listTempHistory != null)
                        {
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listTempHistory.Count; j++)
                            {
                                MarkRecordHistory record = listTempHistory[j];
                                OldMark objOldMark = new OldMark();
                                oldobjtmp.Append(record.Title + ":" + decimal.Parse(Business.Common.Utils.FormatMark(record.Mark)));
                                objOldMark.MarkRecordID = record.MarkRecordID;
                                objOldMark.Title = record.Title;
                                objOldMark.Mark = decimal.Parse(Business.Common.Utils.FormatMark(record.Mark));
                                lstOldMark.Add(objOldMark);
                                if (j < listTempHistory.Count - 1)
                                {
                                    oldobjtmp.Append(",");
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                    }
                    else
                    {
                        if (listTemp != null && listTemp.Count > 0)
                        {
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listTemp.Count; j++)
                            {
                                MarkRecord record = listTemp[j];
                                OldMark objOldMark = new OldMark();
                                oldobjtmp.Append(record.Title + ":" + decimal.Parse(Business.Common.Utils.FormatMark(record.Mark)));
                                objOldMark.MarkRecordID = record.MarkRecordID;
                                objOldMark.Title = record.Title;
                                objOldMark.Mark = decimal.Parse(Business.Common.Utils.FormatMark(record.Mark));
                                lstOldMark.Add(objOldMark);
                                if (j < listTemp.Count - 1)
                                {
                                    oldobjtmp.Append(",");
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                    }
                    // end
                    descriptionStrtmp.Append("Update mark record pupil_id:" + pupilId);
                    paramsStrtmp.Append("pupil_id:" + pupilId);
                    userFuntionsStrtmp.Append("Sổ điểm");
                    userActionsStrtmp.Append(GlobalConstants.ACTION_IMPORT);
                    userDescriptionsStrtmp.Append(inforLog.ToString()).Append(": ");
                    objectIDStrtmp.Append(pupilId);
                    oldObjectStrtmp.Append(oldobjtmp);
                    newObjectStrtmp.Append("(Giá trị sau khi sửa): {");

                    foreach (string key in listMark.Keys)
                    {
                        isChangeMark = false;
                        markString = listMark[key].Replace(".", ",");
                        MarkRecord mr = new MarkRecord();
                        objOM = null;
                        mr.PupilID = pupilId;
                        mr.ClassID = classid;
                        mr.AcademicYearID = glo.AcademicYearID.Value;
                        mr.SchoolID = glo.SchoolID.Value;
                        mr.SubjectID = (int)subjectid;
                        mr.MarkedDate = DateTime.Now.Date;
                        mr.CreatedAcademicYear = acaYear.Year;
                        mr.Semester = (int)semesterid;
                        mr.Title = key;
                        mr.OrderNumber = int.Parse((key.Substring(1).ToString()));
                        mr.MarkTypeID = listMarkType.Where(o => key.Contains(o.Title)).FirstOrDefault().MarkTypeID;
                        ParseMark = Decimal.TryParse(markString, out mark);
                        if (lstOldMark.Count > 0)
                        {
                            objOM = lstOldMark.Where(p => p.Title.Equals(mr.Title)).FirstOrDefault();
                            if (objOM != null)
                            {
                                if (objOM.Mark > -1 && objOM.Mark != mark)
                                {
                                    isChangeMark = true;
                                }
                            }
                            else if (!string.IsNullOrEmpty(markString))
                            {
                                isChangeMark = true;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(markString))
                            {
                                isChangeMark = true;
                            }
                        }
                        if (isChangeMark)
                        {
                            objOM = lstOldMark.Where(p => p.Title.Equals(mr.Title)).FirstOrDefault();
                            if (objOM != null)
                            {
                                userDescriptionsStrtmp.Append(mr.Title + "(" + objOM.Mark + ":");
                                mr.MarkRecordID = objOM.MarkRecordID;
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(mr.Title + "(");
                            }

                            if (ParseMark)
                            {
                                mr.Mark = Business.Common.Utils.RoundMark(mark);
                                userDescriptionsStrtmp.Append(mr.Mark + "); ");
                                newObjectStrtmp.Append(mr.Title + ":" + mr.Mark);
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append("); ");
                                newObjectStrtmp.Append(mr.Title + ":");
                                mr.Mark = -1;
                            }
                            newObjectStrtmp.Append(", ");
                            isInsertLog = true;
                        }
                        else
                        {
                            mr.Mark = -1;
                        }
                        listMarkRecord.Add(mr);
                    }

                    //dau diem kthk
                    isChangeMark = false;
                    string markSemsesterString = ListPupil[i].MarkSemester.Replace(".", ",");
                    MarkRecord mar = new MarkRecord();
                    objOM = null;
                    mar.PupilID = pupilId;
                    mar.ClassID = classid;
                    mar.AcademicYearID = glo.AcademicYearID.Value;
                    mar.SchoolID = glo.SchoolID.Value;
                    mar.SubjectID = (int)subjectid;
                    mar.MarkedDate = DateTime.Now.Date;
                    mar.CreatedAcademicYear = acaYear.Year;
                    mar.Semester = (int)semesterid;
                    mar.Title = "HK";
                    mar.MarkTypeID = listMarkType.Where(o => o.Title == "HK").FirstOrDefault().MarkTypeID;
                    mar.OrderNumber = 1;
                    decimal markSemester = 0;

                    ParseMark = Decimal.TryParse(markSemsesterString, out markSemester);
                    if (lstOldMark.Count > 0)
                    {
                        objOM = lstOldMark.Where(p => p.Title.Equals(mar.Title)).FirstOrDefault();
                        if (objOM != null)
                        {
                            if (objOM.Mark > -1 && objOM.Mark != markSemester)
                            {
                                isChangeMark = true;
                            }
                        }
                        else if (!string.IsNullOrEmpty(markSemsesterString))
                        {
                            isChangeMark = true;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(markSemsesterString))
                        {
                            isChangeMark = true;
                        }
                    }


                    if (isChangeMark)
                    {
                        if (objOM != null)
                        {
                            userDescriptionsStrtmp.Append(mar.Title + "(" + objOM.Mark + ":");
                            mar.MarkRecordID = objOM.MarkRecordID;
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append(mar.Title + "(");
                        }

                        if (ParseMark)
                        {
                            mar.Mark = Business.Common.Utils.RoundMark(markSemester);
                            userDescriptionsStrtmp.Append(mar.Mark + "); ");
                            newObjectStrtmp.Append(mar.Title + ":" + mar.Mark);
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append("); ");
                            newObjectStrtmp.Append(mar.Title + ":");
                            mar.Mark = -1;
                        }
                        newObjectStrtmp.Append(", ");
                        isInsertLog = true;
                    }
                    else
                    {
                        mar.Mark = -1;
                    }
                    listMarkRecord.Add(mar);
                    //-----------------Doi tuong sumrecord
                    SummedUpRecord sur = new SummedUpRecord();
                    sur.PupilID = pupilId;
                    sur.ClassID = classid;
                    sur.AcademicYearID = glo.AcademicYearID.Value;
                    sur.SchoolID = glo.SchoolID.Value;
                    sur.SubjectID = (int)subjectid;
                    sur.CreatedAcademicYear = acaYear.Year;
                    sur.Semester = (int)semesterid;
                    sur.Comment = ListPupil[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE ? ListPupil[i].Comment : null;
                    string strMarkAvg = ListPupil[i].MarkTBM.Replace(".", ",");
                    decimal markAvg = 0;
                    if (Decimal.TryParse(strMarkAvg, out markAvg))
                    {
                        sur.SummedUpMark = markAvg;
                    }
                    listSummedUpRecord.Add(sur);

                    // Tạo dữ liệu ghi log
                    if (isInsertLog)
                    {
                        string newObj = string.Empty;
                        newObj = newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) + "}";
                        newObj += GlobalConstants.WILD_LOG;
                        string tmp = string.Empty;
                        objectIDStrtmp.Append(GlobalConstants.WILD_LOG);
                        descriptionStrtmp.Append(GlobalConstants.WILD_LOG);
                        paramsStrtmp.Append(GlobalConstants.WILD_LOG);
                        oldObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                        newObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                        userFuntionsStrtmp.Append(GlobalConstants.WILD_LOG);
                        userActionsStrtmp.Append(GlobalConstants.WILD_LOG);
                        tmp = userDescriptionsStrtmp != null ? userDescriptionsStrtmp.ToString().Substring(0, userDescriptionsStrtmp.Length - 2) : "";
                        tmp += GlobalConstants.WILD_LOG;
                        //userDescriptionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                        objectIDStr.Append(objectIDStrtmp);
                        descriptionStr.Append(descriptionStrtmp);
                        paramsStr.Append(paramsStrtmp);
                        oldObjectStr.Append(oldObjectStrtmp);
                        newObjectStr.Append(newObj);
                        userFuntionsStr.Append(userFuntionsStrtmp);
                        userActionsStr.Append(userActionsStrtmp);
                        userDescriptionsStr.Append(tmp);
                    }

                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                    isInsertLogstrtmp = new StringBuilder();
                    iLog++;
                }

                // if (listMarkRecord.Count > 0)
                // {
                if (isMovedHistory)
                {
                    List<MarkRecordHistory> lstMarkRecordHistory = new List<MarkRecordHistory>();
                    List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                    MarkRecordHistory objMarkRecordHistory = null;
                    MarkRecord objMarkRecord = null;
                    SummedUpRecord objSummedUpRecord = null;
                    SummedUpRecordHistory objSummedUpRecordHistory = null;
                    for (int i = 0; i < listMarkRecord.Count; i++)
                    {
                        objMarkRecordHistory = new MarkRecordHistory();
                        objMarkRecord = new MarkRecord();
                        objMarkRecord = listMarkRecord[i];
                        objMarkRecordHistory.MarkRecordID = objMarkRecord.MarkRecordID;
                        objMarkRecordHistory.AcademicYearID = objMarkRecord.AcademicYearID;
                        objMarkRecordHistory.PupilID = objMarkRecord.PupilID;
                        objMarkRecordHistory.SchoolID = objMarkRecord.SchoolID;
                        objMarkRecordHistory.ClassID = objMarkRecord.ClassID;
                        objMarkRecordHistory.Mark = objMarkRecord.Mark;
                        objMarkRecordHistory.MarkTypeID = objMarkRecord.MarkTypeID;
                        objMarkRecordHistory.Semester = objMarkRecord.Semester;
                        objMarkRecordHistory.Title = objMarkRecord.Title;
                        objMarkRecordHistory.Last2digitNumberSchool = objMarkRecord.Last2digitNumberSchool;
                        objMarkRecordHistory.MarkedDate = objMarkRecord.MarkedDate;
                        objMarkRecordHistory.ModifiedDate = objMarkRecord.ModifiedDate;
                        objMarkRecordHistory.SubjectID = objMarkRecord.SubjectID;
                        objMarkRecordHistory.CreatedDate = objMarkRecord.CreatedDate;
                        objMarkRecordHistory.CreatedAcademicYear = objMarkRecord.CreatedAcademicYear;
                        objMarkRecordHistory.OrderNumber = objMarkRecord.OrderNumber;
                        lstMarkRecordHistory.Add(objMarkRecordHistory);
                    }
                    for (int ik = 0; ik < listSummedUpRecord.Count; ik++)
                    {
                        objSummedUpRecord = new SummedUpRecord();
                        objSummedUpRecordHistory = new SummedUpRecordHistory();
                        objSummedUpRecord = listSummedUpRecord[ik];
                        objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                        objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                        objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                        objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                        objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                        objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                        objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                        objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                        objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                        objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                        objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                        objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                        objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                        objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                    }
                    MarkRecordHistoryBusiness.InsertMarkRecordHistoryByClassID(glo.UserAccountID, lstMarkRecordHistory, lstSummedUpRecordHistory, semesterId, null, null, glo.SchoolID.Value, glo.AcademicYearID.Value, glo.AppliedLevel.Value, listClassId, SubjectID, _globalInfo.EmployeeID, acaYear.Year);
                }
                else
                {
                    MarkRecordBusiness.InsertMarkRecordByListClass(glo.UserAccountID, listMarkRecord, listSummedUpRecord, semesterId, null, glo.SchoolID.Value, glo.AcademicYearID.Value, glo.AppliedLevel.Value, listClassId, SubjectID, _globalInfo.EmployeeID, null, acaYear.Year);
                }
                // Tạo dữ liệu ghi log
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
                SetViewDataActionAudit(dicLog);

                return Res.Get("MarkRecord_Label_ImportDataSuccess");
            }
            return Res.Get("MarkRecord_Label_NotImportDataSuccess");
        }


        public string SetDataInputToDbJudgeReord(List<MarkRecordViewModel> ListPupil = null)
        {
            GlobalInfo glo = new GlobalInfo();
            //Lay cac hoc sinh co trang thai dang hoc va hop le
            ListPupil = ListPupil.Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && u.Pass.HasValue && u.Pass.Value).ToList();
            AcademicYear acaYear = AcademicYearBusiness.Find(glo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
            int classid = 0;
            int semesterid = 0;
            int subjectid = 0;
            int Year = acaYear.Year;
            if (ListPupil != null && ListPupil.Count() > 0)
            {
                classid = ListPupil[0].ClassID.Value;
                semesterid = ListPupil[0].SemesterID.Value;
                classid = ListPupil[0].ClassID.Value;
                subjectid = ListPupil[0].SubjectID.Value;
            }
            else
            {
                return Res.Get("MarkRecord_Label_NotImportDataSuccess");
            }

            SubjectCat sc = SubjectCatBusiness.Find(subjectid);

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = glo.AppliedLevel.Value;
            IEnumerable<MarkType> listMarkType = MarkTypeBusiness.Search(dic).ToList();

            List<JudgeRecord> listMarkRecord = new List<JudgeRecord>();
            List<SummedUpRecord> listSummedUpRecord = new List<SummedUpRecord>();

            // Tạo data ghi log
            List<OldJudge> lstOldMark = new List<OldJudge>();
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            StringBuilder inforLog = null;
            StringBuilder oldobjtmp = null;

            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();
            StringBuilder isInsertLogstrtmp = new StringBuilder();
            int iLog = 0;
            bool isInsertLog = false;
            List<int> pupilIds = ListPupil.Select(s => s.PupilID).ToList();

            #region Danh sach hoc sinh
            List<PupilOfClass> lstPOC = (from p in PupilOfClassBusiness.All
                                         where pupilIds.Contains(p.PupilID)
                                         //&& p.Last2digitNumberSchool == (glo.SchoolID % 100)
                                         && p.ClassID == classid
                                         && p.AcademicYearID == acaYear.AcademicYearID
                                         && p.Year == Year
                                         select p).ToList();
            #endregion

            List<JudgeRecordHistory> listTempHistory = null;
            List<JudgeRecord> listTemp = null;
            if (isMovedHistory)
            {
                Dictionary<string, object> Dics = new Dictionary<string, object>();
                Dics.Add("ClassID", classid);
                Dics.Add("AcademicYearID", acaYear.AcademicYearID);
                Dics.Add("SchoolID", acaYear.SchoolID);
                Dics.Add("SubjectID", subjectid);
                Dics.Add("Semester", semesterid);
                listTempHistory = JudgeRecordHistoryBusiness.SearchJudgeRecordHistory(Dics).ToList();
            }
            else
            {
                int modSchoolID = _globalInfo.SchoolID.Value % 100;
                listTemp = (from m in JudgeRecordBusiness.All
                            where
                            //listPupilIds.Contains(m.PupilID) && 
                            m.Last2digitNumberSchool == modSchoolID
                            && m.AcademicYearID == acaYear.AcademicYearID
                            && m.Semester == semesterid && m.SubjectID == subjectid
                            && m.ClassID == classid
                            select m).ToList();
            }

            string judgetmp = string.Empty;
            string markString = string.Empty;
            string mark = string.Empty;
            bool isChangeMark = false;
            OldJudge objOM = null;
            foreach (MarkRecordViewModel model in ListPupil)
            {
                Dictionary<string, string> listMark = model.ListMark;
                // Tạo dữ liệu ghi log
                int id = model.PupilID;
                PupilOfClass pop = lstPOC.Where(p => p.PupilID == id).FirstOrDefault();
                lstOldMark = new List<OldJudge>();
                isInsertLog = false;
                lstOldMark = new List<OldJudge>();
                inforLog = new StringBuilder();
                oldobjtmp = new StringBuilder();
                inforLog.Append("Import điểm HS " + pop.PupilProfile.FullName);
                inforLog.Append(", mã " + pop.PupilProfile.PupilCode);
                inforLog.Append(", Lớp " + pop.ClassProfile.DisplayName);
                inforLog.Append("/" + sc.SubjectName);
                inforLog.Append("/Học kỳ " + semesterid);
                inforLog.Append("/" + pop.Year.Value + "-" + (pop.Year.Value + 1));
                newObjectStrtmp = new StringBuilder();

                if (isMovedHistory)
                {
                    if (listTempHistory != null)
                    {
                        List<JudgeRecordHistory> listOldRecord = listTempHistory.Where(p => p.PupilID == id && p.ClassID == classid && p.Semester == semesterid && p.SubjectID == subjectid).ToList();
                        oldobjtmp.Append("(Giá trị trước khi sửa): {");
                        for (int j = 0; j < listOldRecord.Count; j++)
                        {
                            JudgeRecordHistory record = listOldRecord[j];
                            OldJudge objOldMark = new OldJudge();
                            oldobjtmp.Append(record.Title + ":" + record.Judgement);
                            objOldMark.JudgeRecordID = record.JudgeRecordID;
                            objOldMark.Title = record.Title;
                            objOldMark.Judgement = record.Judgement;
                            lstOldMark.Add(objOldMark);
                            if (j < listOldRecord.Count - 1)
                            {
                                oldobjtmp.Append(",");
                            }
                        }
                        oldobjtmp.Append("}");
                    }
                }
                else
                {
                    if (listTemp != null && listTemp.Count > 0)
                    {
                        List<JudgeRecord> listOldRecord = listTemp.Where(p => p.PupilID == id && p.ClassID == classid && p.Semester == semesterid && p.SubjectID == subjectid).ToList();
                        oldobjtmp.Append("(Giá trị trước khi sửa): {");
                        for (int j = 0; j < listOldRecord.Count; j++)
                        {
                            JudgeRecord record = listOldRecord[j];
                            OldJudge objOldMark = new OldJudge();
                            oldobjtmp.Append(record.Title + ":" + record.Judgement);
                            objOldMark.JudgeRecordID = record.JudgeRecordID;
                            objOldMark.Title = record.Title;
                            objOldMark.Judgement = record.Judgement;
                            lstOldMark.Add(objOldMark);
                            if (j < listOldRecord.Count - 1)
                            {
                                oldobjtmp.Append(",");
                            }
                        }
                        oldobjtmp.Append("}");
                    }
                }
                // end
                descriptionStrtmp.Append("Update mark record pupil_id:" + id);
                paramsStrtmp.Append("pupil_id:" + id);
                userFuntionsStrtmp.Append("Sổ điểm");
                userActionsStrtmp.Append(GlobalConstants.ACTION_IMPORT);
                userDescriptionsStrtmp.Append(inforLog.ToString()).Append(": ");
                objectIDStrtmp.Append(id);
                oldObjectStrtmp.Append(oldobjtmp);
                newObjectStrtmp.Append("(Giá trị sau khi sửa): {");
                foreach (string key in listMark.Keys)
                {
                    isChangeMark = false;
                    markString = listMark[key];
                    JudgeRecord mr = new JudgeRecord();
                    objOM = null;
                    mr.PupilID = model.PupilID;
                    mr.ClassID = classid;
                    mr.AcademicYearID = glo.AcademicYearID.Value;
                    mr.SchoolID = glo.SchoolID.Value;
                    mr.SubjectID = (int)subjectid;
                    mr.MarkedDate = DateTime.Now.Date;
                    mr.CreatedAcademicYear = acaYear.Year;
                    mr.Semester = (int)semesterid;
                    mr.Title = key;
                    mr.OrderNumber = int.Parse((key.Substring(1).ToString()));
                    mr.MarkTypeID = listMarkType.Where(o => key.Contains(o.Title)).FirstOrDefault().MarkTypeID;
                    if (lstOldMark.Count > 0)
                    {
                        objOM = lstOldMark.Where(p => p.Title.Equals(mr.Title)).FirstOrDefault();
                        if (objOM != null)
                        {
                            if (!string.IsNullOrEmpty(objOM.Judgement) && objOM.Judgement != markString)
                            {
                                isChangeMark = true;
                            }
                        }
                        else if (!string.IsNullOrEmpty(markString))
                        {
                            isChangeMark = true;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(markString))
                        {
                            isChangeMark = true;
                        }
                    }
                    if (isChangeMark)
                    {
                        if (objOM != null)
                        {
                            userDescriptionsStrtmp.Append(mr.Title + "(" + objOM.Judgement + ":");
                            mr.JudgeRecordID = objOM.JudgeRecordID;
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append(mr.Title + "(");
                        }

                        if (!string.IsNullOrEmpty(markString))
                        {
                            mr.Judgement = markString;
                            userDescriptionsStrtmp.Append(mr.Judgement + "); ");
                            newObjectStrtmp.Append(mr.Title + ":" + mr.Judgement);
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append("); ");
                            newObjectStrtmp.Append(mr.Title + ":");
                            mr.Judgement = "-1";
                        }
                        newObjectStrtmp.Append(", ");
                        isInsertLog = true;
                    }
                    else
                    {
                        mr.Judgement = "-1";
                    }
                    listMarkRecord.Add(mr);
                }

                //dau diem kthk
                isChangeMark = false;
                string judgement = model.MarkSemester;
                JudgeRecord mar = new JudgeRecord();
                objOM = null;
                mar.PupilID = model.PupilID;
                mar.ClassID = classid;
                mar.AcademicYearID = glo.AcademicYearID.Value;
                mar.SchoolID = glo.SchoolID.Value;
                mar.SubjectID = (int)subjectid;
                mar.MarkedDate = DateTime.Now.Date;
                mar.CreatedAcademicYear = acaYear.Year;
                mar.Semester = (int)semesterid;
                mar.Title = "HK";
                mar.MarkTypeID = listMarkType.Where(o => o.Title == "HK").FirstOrDefault().MarkTypeID;
                mar.OrderNumber = 1;

                if (lstOldMark.Count > 0)
                {
                    objOM = lstOldMark.Where(p => p.Title.Equals(mar.Title)).FirstOrDefault();
                    if (objOM != null)
                    {
                        if (!string.IsNullOrEmpty(objOM.Judgement) && objOM.Judgement != judgement)
                        {
                            isChangeMark = true;
                        }
                    }
                    else if (!string.IsNullOrEmpty(judgement))
                    {
                        isChangeMark = true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(judgement))
                    {
                        isChangeMark = true;
                    }
                }
                if (isChangeMark)
                {
                    if (objOM != null)
                    {
                        userDescriptionsStrtmp.Append(mar.Title + "(" + objOM.Judgement + ":");
                        mar.JudgeRecordID = objOM.JudgeRecordID;
                    }
                    else
                    {
                        userDescriptionsStrtmp.Append(mar.Title + "(");
                    }

                    if (!string.IsNullOrEmpty(judgement))
                    {
                        mar.Judgement = judgement;
                        userDescriptionsStrtmp.Append(mar.Judgement + "); ");
                        newObjectStrtmp.Append(mar.Title + ":" + mar.Judgement);
                    }
                    else
                    {
                        userDescriptionsStrtmp.Append("); ");
                        newObjectStrtmp.Append(mar.Title + ":");
                        mar.Judgement = "-1";
                    }
                    newObjectStrtmp.Append(", ");
                    isInsertLog = true;
                }
                else
                {
                    mar.Judgement = "-1";
                }
                listMarkRecord.Add(mar);
                //-----------------Doi tuong sumrecord
                SummedUpRecord sur = new SummedUpRecord();
                sur.PupilID = model.PupilID;
                sur.ClassID = classid;
                sur.AcademicYearID = glo.AcademicYearID.Value;
                sur.SchoolID = glo.SchoolID.Value;
                sur.SubjectID = (int)subjectid;
                sur.CreatedAcademicYear = acaYear.Year;
                sur.Semester = (int)semesterid;
                sur.Comment = sc.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE ? model.Comment : null;
                sur.JudgementResult = model.MarkTBM;
                listSummedUpRecord.Add(sur);

                if (isInsertLog)
                {
                    string newObj = string.Empty;
                    newObj = newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) + "}";
                    newObj += GlobalConstants.WILD_LOG;
                    string tmp = string.Empty;
                    objectIDStrtmp.Append(GlobalConstants.WILD_LOG);
                    descriptionStrtmp.Append(GlobalConstants.WILD_LOG);
                    paramsStrtmp.Append(GlobalConstants.WILD_LOG);
                    oldObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                    newObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                    userFuntionsStrtmp.Append(GlobalConstants.WILD_LOG);
                    userActionsStrtmp.Append(GlobalConstants.WILD_LOG);
                    tmp = userDescriptionsStrtmp != null ? userDescriptionsStrtmp.ToString().Substring(0, userDescriptionsStrtmp.Length - 2) : "";
                    tmp += GlobalConstants.WILD_LOG;
                    //userDescriptionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                    objectIDStr.Append(objectIDStrtmp);
                    descriptionStr.Append(descriptionStrtmp);
                    paramsStr.Append(paramsStrtmp);
                    oldObjectStr.Append(oldObjectStrtmp);
                    newObjectStr.Append(newObj);
                    userFuntionsStr.Append(userFuntionsStrtmp);
                    userActionsStr.Append(userActionsStrtmp);
                    userDescriptionsStr.Append(tmp);
                }

                objectIDStrtmp = new StringBuilder();
                descriptionStrtmp = new StringBuilder();
                paramsStrtmp = new StringBuilder();
                oldObjectStrtmp = new StringBuilder();
                newObjectStrtmp = new StringBuilder();
                userFuntionsStrtmp = new StringBuilder();
                userActionsStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
                isInsertLogstrtmp = new StringBuilder();
                iLog++;
            }

            if (listMarkRecord.Count > 0)
            {
                if (isMovedHistory)
                {
                    List<JudgeRecordHistory> lstMarkRecordHistory = new List<JudgeRecordHistory>();
                    List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                    JudgeRecordHistory objMarkRecordHistory = null;
                    JudgeRecord objMarkRecord = null;
                    SummedUpRecord objSummedUpRecord = null;
                    SummedUpRecordHistory objSummedUpRecordHistory = null;
                    for (int i = 0; i < listMarkRecord.Count; i++)
                    {
                        objMarkRecordHistory = new JudgeRecordHistory();
                        objMarkRecord = new JudgeRecord();
                        objMarkRecord = listMarkRecord[i];
                        objMarkRecordHistory.JudgeRecordID = objMarkRecord.JudgeRecordID;
                        objMarkRecordHistory.AcademicYearID = objMarkRecord.AcademicYearID;
                        objMarkRecordHistory.PupilID = objMarkRecord.PupilID;
                        objMarkRecordHistory.SchoolID = objMarkRecord.SchoolID;
                        objMarkRecordHistory.ClassID = objMarkRecord.ClassID;
                        objMarkRecordHistory.Judgement = objMarkRecord.Judgement;
                        objMarkRecordHistory.MarkTypeID = objMarkRecord.MarkTypeID;
                        objMarkRecordHistory.Semester = objMarkRecord.Semester;
                        objMarkRecordHistory.Title = objMarkRecord.Title;
                        objMarkRecordHistory.Last2digitNumberSchool = objMarkRecord.Last2digitNumberSchool;
                        objMarkRecordHistory.MarkedDate = objMarkRecord.MarkedDate;
                        objMarkRecordHistory.ModifiedDate = objMarkRecord.ModifiedDate;
                        objMarkRecordHistory.SubjectID = objMarkRecord.SubjectID;
                        objMarkRecordHistory.CreatedDate = objMarkRecord.CreatedDate;
                        objMarkRecordHistory.CreatedAcademicYear = objMarkRecord.CreatedAcademicYear;
                        objMarkRecordHistory.OrderNumber = objMarkRecord.OrderNumber;
                        lstMarkRecordHistory.Add(objMarkRecordHistory);
                    }

                    for (int i = 0; i < listSummedUpRecord.Count; i++)
                    {
                        objSummedUpRecord = new SummedUpRecord();
                        objSummedUpRecordHistory = new SummedUpRecordHistory();
                        objSummedUpRecord = listSummedUpRecord[i];
                        objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                        objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                        objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                        objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                        objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                        objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                        objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                        objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                        objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                        objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                        objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                        objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                        objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                        objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                    }
                    JudgeRecordHistoryBusiness.InsertJudgeRecordHistory(_globalInfo.UserAccountID, lstMarkRecordHistory, lstSummedUpRecordHistory, semesterid, null, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classid, subjectid, _globalInfo.EmployeeID);
                    //JudgeRecordHistoryBusiness.Save();
                }
                else
                {
                    JudgeRecordBusiness.InsertJudgeRecord(_globalInfo.UserAccountID, listMarkRecord, listSummedUpRecord, semesterid, null, _globalInfo.EmployeeID, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classid, subjectid);
                    //JudgeRecordBusiness.Save();
                }

                // Tạo dữ liệu ghi log
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
                SetViewDataActionAudit(dicLog);
                return Res.Get("MarkRecord_Label_ImportDataSuccess");
            }

            return Res.Get("MarkRecord_Label_NotImportDataSuccess");
        }


        public string SetDataInputToDbJudgeReordByListSubject(int ClassID, int semesterId, List<MarkRecordViewModel> ListPupil = null)
        {
            GlobalInfo glo = new GlobalInfo();
            //Lay cac hoc sinh co trang thai dang hoc va hop le
            ListPupil = ListPupil.Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && u.Pass.HasValue && u.Pass.Value).ToList();

            AcademicYear acaYear = AcademicYearBusiness.Find(glo.AcademicYearID.Value);
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
            int classid = 0;
            int semesterid = 0;
            int subjectid = 0;
            int Year = acaYear.Year;
            int pupilId = 0;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = glo.AppliedLevel.Value;
            IEnumerable<MarkType> listMarkType = MarkTypeBusiness.Search(dic).ToList();

            List<JudgeRecord> listMarkRecord = new List<JudgeRecord>();
            List<SummedUpRecord> listSummedUpRecord = new List<SummedUpRecord>();

            List<OldJudge> lstOldMark = new List<OldJudge>();
            StringBuilder objectIDStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.ObjectID] != null ? ViewData[CommonKey.AuditActionKey.ObjectID].ToString() : "");
            StringBuilder descriptionStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.Description] != null ? ViewData[CommonKey.AuditActionKey.Description].ToString() : "");
            StringBuilder paramsStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.Parameter] != null ? ViewData[CommonKey.AuditActionKey.Parameter].ToString() : "");
            StringBuilder oldObjectStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.OldJsonObject] != null ? ViewData[CommonKey.AuditActionKey.OldJsonObject].ToString() : "");
            StringBuilder newObjectStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.NewJsonObject] != null ? ViewData[CommonKey.AuditActionKey.NewJsonObject].ToString() : "");
            StringBuilder userFuntionsStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.userFunction] != null ? ViewData[CommonKey.AuditActionKey.userFunction].ToString() : "");
            StringBuilder userActionsStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.userAction] != null ? ViewData[CommonKey.AuditActionKey.userAction].ToString() : "");
            StringBuilder userDescriptionsStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.userDescription] != null ? ViewData[CommonKey.AuditActionKey.userDescription].ToString() : "");
            StringBuilder inforLog = null;
            StringBuilder oldobjtmp = null;

            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();
            StringBuilder isInsertLogstrtmp = new StringBuilder();
            int iLog = 0;
            bool isInsertLog = false;
            List<JudgeRecordHistory> listTempJudgeHistory = new List<JudgeRecordHistory>();
            List<JudgeRecord> listJudgeTemp = new List<JudgeRecord>();
            List<int> listSubjectID = new List<int>();
            if (ListPupil != null && ListPupil.Count > 0)
            {
                listSubjectID = ListPupil.Where(p => p.SubjectID.HasValue && p.SubjectID.Value > 0).Select(p => p.SubjectID.Value).Distinct().ToList();
                if (isMovedHistory)
                {
                    Dictionary<string, object> Dics = new Dictionary<string, object>();
                    Dics.Add("ClassID", ClassID);
                    Dics.Add("AcademicYearID", acaYear.AcademicYearID);
                    Dics.Add("SchoolID", acaYear.SchoolID);
                    Dics.Add("Semester", semesterId);
                    listTempJudgeHistory = JudgeRecordHistoryBusiness.SearchJudgeRecordHistory(Dics).Where(p => listSubjectID.Contains(p.SubjectID)).ToList();
                }
                else
                {
                    int modSchoolID = _globalInfo.SchoolID.Value % 100;
                    listJudgeTemp = (from m in JudgeRecordBusiness.All
                                     where m.Last2digitNumberSchool == modSchoolID
                                     && m.AcademicYearID == acaYear.AcademicYearID
                                     && m.Semester == semesterId
                                     && m.ClassID == ClassID
                                     && listSubjectID.Contains(m.SubjectID)
                                     select m).ToList();
                }

                List<SubjectCat> lstSC = (from sc in SubjectCatBusiness.All
                                          where sc.AppliedLevel == _globalInfo.AppliedLevel
                                          && sc.IsActive
                                          && listSubjectID.Contains(sc.SubjectCatID)
                                          select sc).ToList();
                SubjectCat objSC = null;
                string judgetmp = string.Empty;
                string markString = string.Empty;
                string mark = string.Empty;
                bool isChangeMark = false;
                OldJudge objOM = null;
                MarkRecordViewModel objVM = null;
                for (int k = 0; k < ListPupil.Count; k++)
                {
                    objVM = ListPupil[k];
                    classid = objVM.ClassID.Value;
                    semesterid = objVM.SemesterID.Value;
                    classid = objVM.ClassID.Value;
                    subjectid = objVM.SubjectID.Value;
                    pupilId = objVM.PupilID;
                    if (ListPupil[k].Status != Constants.GlobalConstants.PUPIL_STATUS_STUDYING) continue;
                    objSC = lstSC.Where(p => p.SubjectCatID == objVM.SubjectID).FirstOrDefault();
                    lstOldMark = new List<OldJudge>();
                    isInsertLog = false;
                    lstOldMark = new List<OldJudge>();
                    inforLog = new StringBuilder();
                    oldobjtmp = new StringBuilder();
                    inforLog.Append("Import điểm HS " + objVM.Fullname);
                    inforLog.Append(", mã " + objVM.PupilCode);
                    inforLog.Append(", Lớp " + objCP.DisplayName);
                    inforLog.Append("/" + objSC.SubjectName);
                    inforLog.Append("/Học kỳ " + semesterid);
                    inforLog.Append("/" + acaYear.Year + "-" + (acaYear.Year + 1));
                    newObjectStrtmp = new StringBuilder();
                    List<JudgeRecordHistory> listTempHistory = null;
                    List<JudgeRecord> listTemp = null;
                    if (isMovedHistory)
                    {
                        listTempHistory = listTempJudgeHistory.Where(p => p.PupilID == pupilId && p.SubjectID == subjectid).ToList();
                    }
                    else
                    {
                        listTemp = listJudgeTemp.Where(p => p.PupilID == pupilId && p.SubjectID == subjectid).ToList();
                    }
                    Dictionary<string, string> listMark = ListPupil[k].ListMark;
                    if (isMovedHistory)
                    {
                        if (listTempHistory != null)
                        {
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listTempHistory.Count; j++)
                            {
                                JudgeRecordHistory record = listTempHistory[j];
                                OldJudge objOldMark = new OldJudge();
                                oldobjtmp.Append(record.Title + ":" + record.Judgement);
                                objOldMark.JudgeRecordID = record.JudgeRecordID;
                                objOldMark.Title = record.Title;
                                objOldMark.Judgement = record.Judgement;
                                lstOldMark.Add(objOldMark);
                                if (j < listTempHistory.Count - 1)
                                {
                                    oldobjtmp.Append(",");
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                    }
                    else
                    {
                        if (listTemp != null && listTemp.Count > 0)
                        {
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listTemp.Count; j++)
                            {
                                JudgeRecord record = listTemp[j];
                                OldJudge objOldMark = new OldJudge();
                                oldobjtmp.Append(record.Title + ":" + record.Judgement);
                                objOldMark.JudgeRecordID = record.JudgeRecordID;
                                objOldMark.Title = record.Title;
                                objOldMark.Judgement = record.Judgement;
                                lstOldMark.Add(objOldMark);
                                if (j < listTemp.Count - 1)
                                {
                                    oldobjtmp.Append(",");
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                    }
                    // end
                    descriptionStrtmp.Append("Update mark record pupil_id:" + pupilId);
                    paramsStrtmp.Append("pupil_id:" + pupilId);
                    userFuntionsStrtmp.Append("Sổ điểm");
                    userActionsStrtmp.Append(GlobalConstants.ACTION_IMPORT);
                    userDescriptionsStrtmp.Append(inforLog.ToString()).Append(": ");
                    objectIDStrtmp.Append(pupilId);
                    oldObjectStrtmp.Append(oldobjtmp);
                    newObjectStrtmp.Append("(Giá trị sau khi sửa): {");
                    foreach (string key in listMark.Keys)
                    {
                        isChangeMark = false;
                        markString = listMark[key];
                        JudgeRecord mr = new JudgeRecord();
                        objOM = null;
                        isChangeMark = false;
                        mr.PupilID = pupilId;
                        mr.ClassID = classid;
                        mr.AcademicYearID = glo.AcademicYearID.Value;
                        mr.SchoolID = glo.SchoolID.Value;
                        mr.SubjectID = (int)subjectid;
                        mr.MarkedDate = DateTime.Now.Date;
                        mr.CreatedAcademicYear = acaYear.Year;
                        mr.Semester = (int)semesterid;
                        mr.Title = key;
                        mr.OrderNumber = int.Parse((key.Substring(1).ToString()));
                        mr.MarkTypeID = listMarkType.Where(o => key.Contains(o.Title)).FirstOrDefault().MarkTypeID;
                        if (lstOldMark.Count > 0)
                        {
                            objOM = lstOldMark.Where(p => p.Title.Equals(mr.Title)).FirstOrDefault();
                            if (objOM != null)
                            {
                                if (!string.IsNullOrEmpty(objOM.Judgement) && objOM.Judgement != markString)
                                {
                                    isChangeMark = true;
                                }
                            }
                            else if (!string.IsNullOrEmpty(markString))
                            {
                                isChangeMark = true;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(markString))
                            {
                                isChangeMark = true;
                            }
                        }
                        if (isChangeMark)
                        {
                            if (objOM != null)
                            {
                                userDescriptionsStrtmp.Append(mr.Title + "(" + objOM.Judgement + ":");
                                mr.JudgeRecordID = objOM.JudgeRecordID;
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(mr.Title + "(");
                            }

                            if (!string.IsNullOrEmpty(markString))
                            {
                                mr.Judgement = markString;
                                userDescriptionsStrtmp.Append(mr.Judgement + "); ");
                                newObjectStrtmp.Append(mr.Title + ":" + mr.Judgement);
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append("); ");
                                newObjectStrtmp.Append(mr.Title + ":");
                                mr.Judgement = "-1";
                            }
                            newObjectStrtmp.Append(", ");
                            isInsertLog = true;
                        }
                        else
                        {
                            mr.Judgement = "-1";
                        }
                        listMarkRecord.Add(mr);
                    }

                    //dau diem kthk
                    isChangeMark = false;
                    string judgement = ListPupil[k].MarkSemester;
                    JudgeRecord mar = new JudgeRecord();
                    objOM = null;
                    mar.PupilID = pupilId;
                    mar.ClassID = classid;
                    mar.AcademicYearID = glo.AcademicYearID.Value;
                    mar.SchoolID = glo.SchoolID.Value;
                    mar.SubjectID = (int)subjectid;
                    mar.MarkedDate = DateTime.Now.Date;
                    mar.CreatedAcademicYear = acaYear.Year;
                    mar.Semester = (int)semesterid;
                    mar.Title = "HK";
                    mar.MarkTypeID = listMarkType.Where(o => o.Title == "HK").FirstOrDefault().MarkTypeID;
                    mar.OrderNumber = 1;
                    if (lstOldMark.Count > 0)
                    {
                        objOM = lstOldMark.Where(p => p.Title.Equals(mar.Title)).FirstOrDefault();
                        if (objOM != null)
                        {
                            if (!string.IsNullOrEmpty(objOM.Judgement) && objOM.Judgement != judgement)
                            {
                                isChangeMark = true;
                            }
                        }
                        else if (!string.IsNullOrEmpty(judgement))
                        {
                            isChangeMark = true;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(judgement))
                        {
                            isChangeMark = true;
                        }
                    }
                    if (isChangeMark)
                    {
                        if (objOM != null)
                        {
                            userDescriptionsStrtmp.Append(mar.Title + "(" + objOM.Judgement + ":");
                            mar.JudgeRecordID = objOM.JudgeRecordID;
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append(mar.Title + "(");
                        }

                        if (!string.IsNullOrEmpty(judgement))
                        {
                            mar.Judgement = judgement;
                            userDescriptionsStrtmp.Append(mar.Judgement + "); ");
                            newObjectStrtmp.Append(mar.Title + ":" + mar.Judgement);
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append("); ");
                            newObjectStrtmp.Append(mar.Title + ":");
                            mar.Judgement = "-1";
                        }
                        newObjectStrtmp.Append(", ");
                        isInsertLog = true;
                    }
                    else
                    {
                        mar.Judgement = "-1";
                    }
                    listMarkRecord.Add(mar);
                    //-----------------Doi tuong sumrecord
                    SummedUpRecord sur = new SummedUpRecord();
                    sur.PupilID = pupilId;
                    sur.ClassID = classid;
                    sur.AcademicYearID = glo.AcademicYearID.Value;
                    sur.SchoolID = glo.SchoolID.Value;
                    sur.SubjectID = (int)subjectid;
                    sur.CreatedAcademicYear = acaYear.Year;
                    sur.Semester = (int)semesterid;
                    sur.Comment = ListPupil[k].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE ? ListPupil[k].Comment : null;
                    sur.JudgementResult = ListPupil[k].MarkTBM;
                    listSummedUpRecord.Add(sur);

                    if (isInsertLog)
                    {
                        string newObj = string.Empty;
                        newObj = newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) + "}";
                        newObj += GlobalConstants.WILD_LOG;
                        string tmp = string.Empty;
                        objectIDStrtmp.Append(GlobalConstants.WILD_LOG);
                        descriptionStrtmp.Append(GlobalConstants.WILD_LOG);
                        paramsStrtmp.Append(GlobalConstants.WILD_LOG);
                        oldObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                        newObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                        userFuntionsStrtmp.Append(GlobalConstants.WILD_LOG);
                        userActionsStrtmp.Append(GlobalConstants.WILD_LOG);
                        tmp = userDescriptionsStrtmp != null ? userDescriptionsStrtmp.ToString().Substring(0, userDescriptionsStrtmp.Length - 2) : "";
                        tmp += GlobalConstants.WILD_LOG;
                        //userDescriptionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                        objectIDStr.Append(objectIDStrtmp);
                        descriptionStr.Append(descriptionStrtmp);
                        paramsStr.Append(paramsStrtmp);
                        oldObjectStr.Append(oldObjectStrtmp);
                        newObjectStr.Append(newObj);
                        userFuntionsStr.Append(userFuntionsStrtmp);
                        userActionsStr.Append(userActionsStrtmp);
                        userDescriptionsStr.Append(tmp);
                    }

                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                    isInsertLogstrtmp = new StringBuilder();
                    iLog++;
                }
            }
            else
            {
                return Res.Get("MarkRecord_Label_NotImportDataSuccess");
            }

            if (listMarkRecord.Count > 0)
            {
                if (isMovedHistory)
                {
                    List<JudgeRecordHistory> lstMarkRecordHistory = new List<JudgeRecordHistory>();
                    List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                    JudgeRecordHistory objMarkRecordHistory = null;
                    JudgeRecord objMarkRecord = null;
                    SummedUpRecord objSummedUpRecord = null;
                    SummedUpRecordHistory objSummedUpRecordHistory = null;
                    for (int i = 0; i < listMarkRecord.Count; i++)
                    {
                        objMarkRecordHistory = new JudgeRecordHistory();
                        objMarkRecord = new JudgeRecord();
                        objMarkRecord = listMarkRecord[i];
                        objMarkRecordHistory.JudgeRecordID = objMarkRecord.JudgeRecordID;
                        objMarkRecordHistory.AcademicYearID = objMarkRecord.AcademicYearID;
                        objMarkRecordHistory.PupilID = objMarkRecord.PupilID;
                        objMarkRecordHistory.SchoolID = objMarkRecord.SchoolID;
                        objMarkRecordHistory.ClassID = objMarkRecord.ClassID;
                        objMarkRecordHistory.Judgement = objMarkRecord.Judgement;
                        objMarkRecordHistory.MarkTypeID = objMarkRecord.MarkTypeID;
                        objMarkRecordHistory.Semester = objMarkRecord.Semester;
                        objMarkRecordHistory.Title = objMarkRecord.Title;
                        objMarkRecordHistory.Last2digitNumberSchool = objMarkRecord.Last2digitNumberSchool;
                        objMarkRecordHistory.MarkedDate = objMarkRecord.MarkedDate;
                        objMarkRecordHistory.ModifiedDate = objMarkRecord.ModifiedDate;
                        objMarkRecordHistory.SubjectID = objMarkRecord.SubjectID;
                        objMarkRecordHistory.CreatedDate = objMarkRecord.CreatedDate;
                        objMarkRecordHistory.CreatedAcademicYear = objMarkRecord.CreatedAcademicYear;
                        objMarkRecordHistory.OrderNumber = objMarkRecord.OrderNumber;
                        lstMarkRecordHistory.Add(objMarkRecordHistory);
                    }

                    for (int i = 0; i < listSummedUpRecord.Count; i++)
                    {
                        objSummedUpRecord = new SummedUpRecord();
                        objSummedUpRecordHistory = new SummedUpRecordHistory();
                        objSummedUpRecord = listSummedUpRecord[i];
                        objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                        objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                        objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                        objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                        objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                        objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                        objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                        objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                        objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                        objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                        objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                        objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                        objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                        objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                    }
                    JudgeRecordHistoryBusiness.InsertJudgeRecordHistoryByListSubject(_globalInfo.UserAccountID, lstMarkRecordHistory, lstSummedUpRecordHistory, semesterid, null, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classid, listSubjectID, _globalInfo.EmployeeID);
                }
                else
                {
                    JudgeRecordBusiness.InsertJudgeRecordByListSubject(_globalInfo.UserAccountID, listMarkRecord, listSummedUpRecord, semesterid, null, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classid, listSubjectID, _globalInfo.EmployeeID);
                }
                // Tạo dữ liệu ghi log
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
                SetViewDataActionAudit(dicLog);
                return Res.Get("MarkRecord_Label_ImportDataSuccess");
            }

            return Res.Get("MarkRecord_Label_NotImportDataSuccess");
        }


        public string SetDataInputToDbJudgeReordByListClass(int SubjectID, int semesterId, List<MarkRecordViewModel> ListPupil = null)
        {
            GlobalInfo glo = new GlobalInfo();
            //Lay cac hoc sinh co trang thai dang hoc va hop le
            ListPupil = ListPupil.Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && u.Pass.HasValue && u.Pass.Value).ToList();

            AcademicYear acaYear = AcademicYearBusiness.Find(glo.AcademicYearID.Value);
            SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
            int classid = 0;
            int semesterid = 0;
            int Year = acaYear.Year;
            int pupilId = 0;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = glo.AppliedLevel.Value;
            IEnumerable<MarkType> listMarkType = MarkTypeBusiness.Search(dic).ToList();

            List<JudgeRecord> listMarkRecord = new List<JudgeRecord>();
            List<SummedUpRecord> listSummedUpRecord = new List<SummedUpRecord>();

            // Tạo data ghi log
            List<OldJudge> lstOldMark = new List<OldJudge>();
            StringBuilder objectIDStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.ObjectID] != null ? ViewData[CommonKey.AuditActionKey.ObjectID].ToString() : "");
            StringBuilder descriptionStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.Description] != null ? ViewData[CommonKey.AuditActionKey.Description].ToString() : "");
            StringBuilder paramsStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.Parameter] != null ? ViewData[CommonKey.AuditActionKey.Parameter].ToString() : "");
            StringBuilder oldObjectStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.OldJsonObject] != null ? ViewData[CommonKey.AuditActionKey.OldJsonObject].ToString() : "");
            StringBuilder newObjectStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.NewJsonObject] != null ? ViewData[CommonKey.AuditActionKey.NewJsonObject].ToString() : "");
            StringBuilder userFuntionsStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.userFunction] != null ? ViewData[CommonKey.AuditActionKey.userFunction].ToString() : "");
            StringBuilder userActionsStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.userAction] != null ? ViewData[CommonKey.AuditActionKey.userAction].ToString() : "");
            StringBuilder userDescriptionsStr = new StringBuilder(ViewData[CommonKey.AuditActionKey.userDescription] != null ? ViewData[CommonKey.AuditActionKey.userDescription].ToString() : "");
            StringBuilder inforLog = null;
            StringBuilder oldobjtmp = null;

            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();
            StringBuilder isInsertLogstrtmp = new StringBuilder();
            int iLog = 0;
            bool isInsertLog = false;
            List<JudgeRecord> listJudgeTemp = new List<JudgeRecord>();
            List<JudgeRecordHistory> listTempJudgeHistory = new List<JudgeRecordHistory>();
            List<int> listClassID = new List<int>();
            string mark = string.Empty;
            bool isChangeMark = false;
            OldJudge objOM = null;
            if (ListPupil != null && ListPupil.Count > 0)
            {
                listClassID = ListPupil.Where(p => p.ClassID.HasValue && p.ClassID.Value > 0).Select(p => p.ClassID.Value).Distinct().ToList();

                if (isMovedHistory)
                {
                    Dictionary<string, object> Dics = new Dictionary<string, object>();
                    //Dics.Add("ClassID", ClassID);
                    Dics.Add("AcademicYearID", acaYear.AcademicYearID);
                    Dics.Add("SchoolID", acaYear.SchoolID);
                    Dics.Add("SubjectID", SubjectID);
                    Dics.Add("Semester", semesterId);
                    listTempJudgeHistory = JudgeRecordHistoryBusiness.SearchJudgeRecordHistory(Dics).Where(p => listClassID.Contains(p.ClassID)).ToList();
                }
                else
                {
                    int modSchoolID = _globalInfo.SchoolID.Value % 100;
                    listJudgeTemp = (from m in JudgeRecordBusiness.All
                                     where m.Last2digitNumberSchool == modSchoolID
                                     && m.AcademicYearID == acaYear.AcademicYearID
                                     && m.Semester == semesterId
                                     && m.SubjectID == SubjectID
                                     && listClassID.Contains(m.ClassID)
                                     select m).ToList();
                }

                List<ClassProfile> lstCP = (from cp in ClassProfileBusiness.All
                                            where cp.AcademicYearID == _globalInfo.AcademicYearID.Value
                                            && cp.SchoolID == _globalInfo.SchoolID.Value
                                            && listClassID.Contains(cp.ClassProfileID)
                                            && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                            select cp).ToList();
                ClassProfile objCP = null;
                MarkRecordViewModel objVM = null;
                for (int k = 0; k < ListPupil.Count; k++)
                {
                    objVM = ListPupil[k];
                    semesterid = objVM.SemesterID.Value;
                    classid = objVM.ClassID.Value;
                    pupilId = objVM.PupilID;

                    List<JudgeRecordHistory> listTempHistory = null;
                    List<JudgeRecord> listTemp = null;
                    if (isMovedHistory)
                    {
                        listTempHistory = listTempJudgeHistory.Where(p => p.PupilID == pupilId && p.ClassID == classid).ToList();
                    }
                    else
                    {
                        listTemp = listJudgeTemp.Where(p => p.PupilID == pupilId && p.ClassID == classid).ToList();
                    }
                    objCP = lstCP.Where(p => p.ClassProfileID == objVM.ClassID).FirstOrDefault();
                    Dictionary<string, string> listMark = ListPupil[k].ListMark;
                    lstOldMark = new List<OldJudge>();
                    isInsertLog = false;
                    inforLog = new StringBuilder();
                    oldobjtmp = new StringBuilder();
                    inforLog.Append("Import điểm HS " + objVM.Fullname);
                    inforLog.Append(", mã " + objVM.PupilCode);
                    inforLog.Append(", Lớp " + objCP.DisplayName);
                    inforLog.Append("/" + objSC.SubjectName);
                    inforLog.Append("/Học kỳ " + semesterid);
                    inforLog.Append("/" + acaYear.Year + "-" + (acaYear.Year + 1));
                    newObjectStrtmp = new StringBuilder();

                    if (isMovedHistory)
                    {
                        if (listTempHistory != null)
                        {
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listTempHistory.Count; j++)
                            {
                                JudgeRecordHistory record = listTempHistory[j];
                                OldJudge objOldMark = new OldJudge();
                                oldobjtmp.Append(record.Title + ":" + record.Judgement);
                                objOldMark.JudgeRecordID = record.JudgeRecordID;
                                objOldMark.Title = record.Title;
                                objOldMark.Judgement = record.Judgement;
                                lstOldMark.Add(objOldMark);
                                if (j < listTempHistory.Count - 1)
                                {
                                    oldobjtmp.Append(",");
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                    }
                    else
                    {
                        if (listTemp != null && listTemp.Count > 0)
                        {
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listTemp.Count; j++)
                            {
                                JudgeRecord record = listTemp[j];
                                OldJudge objOldMark = new OldJudge();
                                oldobjtmp.Append(record.Title + ":" + record.Judgement);
                                objOldMark.JudgeRecordID = record.JudgeRecordID;
                                objOldMark.Title = record.Title;
                                objOldMark.Judgement = record.Judgement;
                                lstOldMark.Add(objOldMark);
                                if (j < listTemp.Count - 1)
                                {
                                    oldobjtmp.Append(",");
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                    }
                    // end
                    descriptionStrtmp.Append("Update mark record pupil_id:" + pupilId);
                    paramsStrtmp.Append("pupil_id:" + pupilId);
                    userFuntionsStrtmp.Append("Sổ điểm");
                    userActionsStrtmp.Append(GlobalConstants.ACTION_IMPORT);
                    userDescriptionsStrtmp.Append(inforLog.ToString()).Append(": ");
                    objectIDStrtmp.Append(pupilId);
                    oldObjectStrtmp.Append(oldobjtmp);
                    newObjectStrtmp.Append("(Giá trị sau khi sửa): {");
                    foreach (string key in listMark.Keys)
                    {
                        isChangeMark = false;
                        string judgement = listMark[key];
                        JudgeRecord mr = new JudgeRecord();
                        objOM = null;
                        mr.PupilID = pupilId;
                        mr.ClassID = classid;
                        mr.AcademicYearID = glo.AcademicYearID.Value;
                        mr.SchoolID = glo.SchoolID.Value;
                        mr.SubjectID = SubjectID;
                        mr.MarkedDate = DateTime.Now.Date;
                        mr.CreatedAcademicYear = acaYear.Year;
                        mr.Semester = (int)semesterid;
                        mr.Title = key;
                        mr.OrderNumber = int.Parse((key.Substring(1).ToString()));
                        mr.MarkTypeID = listMarkType.Where(o => key.Contains(o.Title)).FirstOrDefault().MarkTypeID;
                        if (lstOldMark.Count > 0)
                        {
                            objOM = lstOldMark.Where(p => p.Title.Equals(mr.Title)).FirstOrDefault();
                            if (objOM != null)
                            {
                                if (!string.IsNullOrEmpty(objOM.Judgement) && objOM.Judgement != judgement)
                                {
                                    isChangeMark = true;
                                }
                            }
                            else if (!string.IsNullOrEmpty(judgement))
                            {
                                isChangeMark = true;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(judgement))
                            {
                                isChangeMark = true;
                            }
                        }
                        if (isChangeMark)
                        {
                            if (objOM != null)
                            {
                                userDescriptionsStrtmp.Append(mr.Title + "(" + objOM.Judgement + ":");
                                mr.JudgeRecordID = objOM.JudgeRecordID;
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(mr.Title + "(");
                            }

                            if (!string.IsNullOrEmpty(judgement))
                            {
                                mr.Judgement = judgement;
                                userDescriptionsStrtmp.Append(mr.Judgement + "); ");
                                newObjectStrtmp.Append(mr.Title + ":" + mr.Judgement);
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append("); ");
                                newObjectStrtmp.Append(mr.Title + ":");
                                mr.Judgement = "-1";
                            }
                            newObjectStrtmp.Append(", ");
                            isInsertLog = true;
                        }
                        else
                        {
                            mr.Judgement = "-1";
                        }
                        listMarkRecord.Add(mr);
                    }

                    //dau diem kthk
                    isChangeMark = false;
                    string judgementResult = ListPupil[k].MarkSemester;
                    JudgeRecord mar = new JudgeRecord();
                    objOM = null;
                    mar.PupilID = pupilId;
                    mar.ClassID = classid;
                    mar.AcademicYearID = glo.AcademicYearID.Value;
                    mar.SchoolID = glo.SchoolID.Value;
                    mar.SubjectID = SubjectID;
                    mar.MarkedDate = DateTime.Now.Date;
                    mar.CreatedAcademicYear = acaYear.Year;
                    mar.Semester = (int)semesterid;
                    mar.Title = "HK";
                    mar.MarkTypeID = listMarkType.Where(o => o.Title == "HK").FirstOrDefault().MarkTypeID;
                    mar.OrderNumber = 1;
                    if (lstOldMark.Count > 0)
                    {
                        objOM = lstOldMark.Where(p => p.Title.Equals(mar.Title)).FirstOrDefault();
                        if (objOM != null)
                        {
                            if (!string.IsNullOrEmpty(objOM.Judgement) && objOM.Judgement != judgementResult)
                            {
                                isChangeMark = true;
                            }
                        }
                        else if (!string.IsNullOrEmpty(judgementResult))
                        {
                            isChangeMark = true;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(judgementResult))
                        {
                            isChangeMark = true;
                        }
                    }
                    if (isChangeMark)
                    {
                        if (objOM != null)
                        {
                            userDescriptionsStrtmp.Append(mar.Title + "(" + objOM.Judgement + ":");
                            mar.JudgeRecordID = objOM.JudgeRecordID;
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append(mar.Title + "(");
                        }

                        if (!string.IsNullOrEmpty(judgementResult))
                        {
                            mar.Judgement = judgementResult;
                            userDescriptionsStrtmp.Append(mar.Judgement + "); ");
                            newObjectStrtmp.Append(mar.Title + ":" + mar.Judgement);
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append("); ");
                            newObjectStrtmp.Append(mar.Title + ":");
                            mar.Judgement = "-1";
                        }
                        newObjectStrtmp.Append(", ");
                        isInsertLog = true;
                    }
                    else
                    {
                        mar.Judgement = "-1";
                    }
                    listMarkRecord.Add(mar);
                    //-----------------Doi tuong sumrecord
                    SummedUpRecord sur = new SummedUpRecord();
                    sur.PupilID = pupilId;
                    sur.ClassID = classid;
                    sur.AcademicYearID = glo.AcademicYearID.Value;
                    sur.SchoolID = glo.SchoolID.Value;
                    sur.SubjectID = SubjectID;
                    sur.CreatedAcademicYear = acaYear.Year;
                    sur.Semester = (int)semesterid;
                    sur.Comment = ListPupil[k].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE ? ListPupil[k].Comment : null;
                    sur.JudgementResult = ListPupil[k].MarkTBM;
                    listSummedUpRecord.Add(sur);

                    if (isInsertLog)
                    {
                        string newObj = string.Empty;
                        newObj = newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) + "}";
                        newObj += GlobalConstants.WILD_LOG;
                        string tmp = string.Empty;
                        objectIDStrtmp.Append(GlobalConstants.WILD_LOG);
                        descriptionStrtmp.Append(GlobalConstants.WILD_LOG);
                        paramsStrtmp.Append(GlobalConstants.WILD_LOG);
                        oldObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                        newObjectStrtmp.Append(GlobalConstants.WILD_LOG);
                        userFuntionsStrtmp.Append(GlobalConstants.WILD_LOG);
                        userActionsStrtmp.Append(GlobalConstants.WILD_LOG);
                        tmp = userDescriptionsStrtmp != null ? userDescriptionsStrtmp.ToString().Substring(0, userDescriptionsStrtmp.Length - 2) : "";
                        tmp += GlobalConstants.WILD_LOG;
                        //userDescriptionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                        objectIDStr.Append(objectIDStrtmp);
                        descriptionStr.Append(descriptionStrtmp);
                        paramsStr.Append(paramsStrtmp);
                        oldObjectStr.Append(oldObjectStrtmp);
                        newObjectStr.Append(newObj);
                        userFuntionsStr.Append(userFuntionsStrtmp);
                        userActionsStr.Append(userActionsStrtmp);
                        userDescriptionsStr.Append(tmp);
                    }

                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                    isInsertLogstrtmp = new StringBuilder();
                    iLog++;
                }
            }
            else
            {
                return Res.Get("MarkRecord_Label_NotImportDataSuccess");
            }

            if (listMarkRecord.Count > 0)
            {
                if (isMovedHistory)
                {
                    List<JudgeRecordHistory> lstMarkRecordHistory = new List<JudgeRecordHistory>();
                    List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                    JudgeRecordHistory objMarkRecordHistory = null;
                    JudgeRecord objMarkRecord = null;
                    SummedUpRecord objSummedUpRecord = null;
                    SummedUpRecordHistory objSummedUpRecordHistory = null;
                    for (int i = 0; i < listMarkRecord.Count; i++)
                    {
                        objMarkRecordHistory = new JudgeRecordHistory();
                        objMarkRecord = new JudgeRecord();
                        objMarkRecord = listMarkRecord[i];
                        objMarkRecordHistory.JudgeRecordID = objMarkRecord.JudgeRecordID;
                        objMarkRecordHistory.AcademicYearID = objMarkRecord.AcademicYearID;
                        objMarkRecordHistory.PupilID = objMarkRecord.PupilID;
                        objMarkRecordHistory.SchoolID = objMarkRecord.SchoolID;
                        objMarkRecordHistory.ClassID = objMarkRecord.ClassID;
                        objMarkRecordHistory.Judgement = objMarkRecord.Judgement;
                        objMarkRecordHistory.MarkTypeID = objMarkRecord.MarkTypeID;
                        objMarkRecordHistory.Semester = objMarkRecord.Semester;
                        objMarkRecordHistory.Title = objMarkRecord.Title;
                        objMarkRecordHistory.Last2digitNumberSchool = objMarkRecord.Last2digitNumberSchool;
                        objMarkRecordHistory.MarkedDate = objMarkRecord.MarkedDate;
                        objMarkRecordHistory.ModifiedDate = objMarkRecord.ModifiedDate;
                        objMarkRecordHistory.SubjectID = objMarkRecord.SubjectID;
                        objMarkRecordHistory.CreatedDate = objMarkRecord.CreatedDate;
                        objMarkRecordHistory.CreatedAcademicYear = objMarkRecord.CreatedAcademicYear;
                        objMarkRecordHistory.OrderNumber = objMarkRecord.OrderNumber;
                        lstMarkRecordHistory.Add(objMarkRecordHistory);
                    }

                    for (int i = 0; i < listSummedUpRecord.Count; i++)
                    {
                        objSummedUpRecord = new SummedUpRecord();
                        objSummedUpRecordHistory = new SummedUpRecordHistory();
                        objSummedUpRecord = listSummedUpRecord[i];
                        objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                        objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                        objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                        objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                        objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                        objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                        objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                        objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                        objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                        objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                        objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                        objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                        objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                        objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                    }
                    JudgeRecordHistoryBusiness.InsertJudgeRecordHistoryByListClass(_globalInfo.UserAccountID, lstMarkRecordHistory, lstSummedUpRecordHistory, semesterid, null, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, listClassID, SubjectID, _globalInfo.EmployeeID);
                    // JudgeRecordHistoryBusiness.Save();
                }
                else
                {
                    JudgeRecordBusiness.InsertJudgeRecordByListClass(_globalInfo.UserAccountID, listMarkRecord, listSummedUpRecord, semesterid, null, null, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, listClassID, SubjectID, _globalInfo.EmployeeID);
                    // JudgeRecordBusiness.Save();
                }

                // Tạo dữ liệu ghi log
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
                SetViewDataActionAudit(dicLog);
                return Res.Get("MarkRecord_Label_ImportDataSuccess");
            }

            return Res.Get("MarkRecord_Label_NotImportDataSuccess");
        }


        private DateTime? MaxDate(DateTime? a, DateTime? b, DateTime? c)
        {
            DateTime? result = null;

            List<DateTime> lst = new List<DateTime>();

            if (a != null)
            {
                lst.Add(a.Value);
            }
            if (b != null)
            {
                lst.Add(b.Value);
            }
            if (c != null)
            {
                lst.Add(c.Value);
            }
            result = lst.Max(o => o);



            return result;
        }

        private decimal getTBMTC(decimal MarkIncrease, decimal Mark)
        {
            decimal TBMTC = 0m;
            decimal maxMark = 8.0m;
            decimal minK = 6.5m;
            decimal minTB = 5.0m;
            if (Mark == 10)
            {
                TBMTC = Mark;
            }
            else
                if (MarkIncrease >= 8)
            {
                TBMTC = Mark + 0.3m;
            }
            else if (maxMark > MarkIncrease && MarkIncrease >= minK)
            {
                TBMTC = Mark + 0.2m;
            }
            else if (minK > MarkIncrease && MarkIncrease >= minTB)
            {
                TBMTC = Mark + 0.1m;
            }
            return TBMTC;
        }

        private SubjectCatBO getClassSubject(int subjectID, int academicYearID, int classID, int appliedLevel, int EducationLevelID, int semester)
        {
            SubjectCatBO subjectCatBOObj = (from cs in ClassSubjectBusiness.All
                                            join sc in SubjectCatBusiness.All on cs.SubjectID equals sc.SubjectCatID
                                            join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                            where cs.SubjectID == subjectID
                                            && sc.AppliedLevel == appliedLevel
                                            && cp.AcademicYearID == academicYearID
                                            && cp.EducationLevel.Grade == appliedLevel
                                            && cs.ClassID == classID
                                            && cp.EducationLevelID == EducationLevelID
                                            && sc.IsActive == true
                                            && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                            && ((semester == Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST && cs.SectionPerWeekFirstSemester > 0)
                                                || (semester == Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND && cs.SectionPerWeekSecondSemester > 0))
                                            select new SubjectCatBO
                                            {
                                                SubjectCatID = cs.SubjectID,
                                                SubjectName = sc.DisplayName,
                                                IsCommenting = cs.IsCommenting,
                                                DisplayName = sc.DisplayName,
                                                SubjectIdInCrease = cs.SubjectIDIncrease
                                            }).FirstOrDefault();
            return subjectCatBOObj;
        }

        private string GetSubjectNameBySubjectIncrease(string sheetname)
        {
            if (sheetname.Contains("_TC"))
            {
                sheetname = sheetname.Replace("_TC", "");
            }
            return sheetname;
        }

        private List<SubjectCatBO> GetListSubject(int classId, int semesterId, bool isImport = false)
        {
            bool ViewAll = false;//lay cac mon GV co quyen, khong lay quyen xem toan truong
            //Bo sung dieu kien partition cho bang TeachingAssignmentBusiness
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
            if (classId > 0 && semesterId > 0)
            {
                lsClassSubject = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, semesterId, classId, ViewAll)
                                  join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                  where s.IsActive == true
                                  && s.IsApprenticeshipSubject == false
                                  select new SubjectCatBO()
                                  {
                                      SubjectCatID = s.SubjectCatID,
                                      DisplayName = s.DisplayName,
                                      IsCommenting = cs.IsCommenting,
                                      OrderInSubject = s.OrderInSubject,
                                      SubjectIdInCrease = cs.SubjectIDIncrease
                                  }
                                  ).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }
            int UserAccountID = _globalInfo.UserAccountID;
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classId);
            // Neu la admin truong thi hien thi het
            if (_globalInfo.IsAdminSchoolRole || (ViewAll && !isImport) || (isGVCN && !isImport))
            {
                return lsClassSubject;
            }
            else
            {
                int? TeacherID = UserAccountBusiness.Find(UserAccountID).EmployeeID;
                List<int> listPermitSubjectID = null; ;
                if (TeacherID.HasValue)
                {
                    // Phan cong giao vu la giao vien bo mon
                    if (ClassSupervisorAssignmentBusiness.All.Where(
                                                            o => o.TeacherID == TeacherID &&
                                                            o.ClassID == classId &&
                                                            o.AcademicYearID == _globalInfo.AcademicYearID &&
                                                            o.SchoolID == _globalInfo.SchoolID &&
                                                            o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER).Count() > 0
                                                            )
                    {
                        listPermitSubjectID = lsClassSubject.Select(o => o.SubjectCatID).ToList();
                    }
                    else
                    {
                        // Khong phai lai giao vien phan cong giao vu thi kiem tra la giao vien bo mon
                        IDictionary<string, object> searchInfo = new Dictionary<string, object>();
                        searchInfo["ClassID"] = classId;
                        searchInfo["TeacherID"] = TeacherID;
                        searchInfo["Semester"] = semesterId;
                        searchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                        searchInfo["IsActive"] = true;
                        listPermitSubjectID = TeachingAssignmentBusiness.SearchBySchool(schoolId, searchInfo).Select(o => o.SubjectID).ToList();
                    }
                }
                else
                {
                    listPermitSubjectID = new List<int>();
                }
                lsClassSubject = lsClassSubject.Where(p => listPermitSubjectID.Contains(p.SubjectCatID)).ToList();
            }
            return lsClassSubject;
        }
        private string GetMarkHKOfGDTX(string mark)
        {
            string result = string.Empty;
            if (mark.Length == 1 || "0".Equals(mark) || "10".Equals(mark))
            {
                result = mark;
            }
            else
            {
                mark = mark.Replace(".", ",");
                List<string> lstOfMark = mark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                int tmp0 = int.Parse(lstOfMark[0]);
                int tmp = int.Parse(lstOfMark[1]);
                if (tmp < 3)
                {
                    result = lstOfMark[0] + "," + "0";
                }
                else if (tmp > 2 && tmp < 8)
                {
                    result = lstOfMark[0] + "," + "5";
                }
                else
                {
                    result = (tmp0 + 1).ToString() + "," + "0";
                }
            }
            return result;
        }
        private class OldMark
        {
            public long MarkRecordID { get; set; }
            public string Title { get; set; }
            public decimal Mark { get; set; }
        }

        private class OldJudge
        {
            public long JudgeRecordID { get; set; }
            public string Title { get; set; }
            public string Judgement { get; set; }
        }
    }
}





