﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EmployeeProfileManagementArea.Models
{
    public class RowExcelModel
    {
        public int RowId { get; set; }
        public string RowName { get; set; }
    }
}