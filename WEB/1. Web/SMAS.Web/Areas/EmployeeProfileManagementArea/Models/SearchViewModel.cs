/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SMAS.Web.Models.Attributes;
using Resources;

namespace SMAS.Web.Areas.EmployeeProfileManagementArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Employee_Label_SchoolFaculty")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", EmployeeProfileManagementConstant.LISTSCHOOLFACULTY)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? SchoolFaculty { get; set; }

        [ResourceDisplayName("Employee_Label_WorkType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", EmployeeProfileManagementConstant.LISTWORKTYPE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? WorkType { get; set; }

        [ResourceDisplayName("Employee_Label_EmploymentStatus")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", EmployeeProfileManagementConstant.LISTEMPLOYMENTSTATUS)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? EmploymentStatus { get; set; }

        [ResourceDisplayName("Employee_Label_EmployeeCode")]
        [UIHint("Textbox")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string EmployeeCode { get; set; }

        [ResourceDisplayName("Employee_Label_FullName")]
        [UIHint("Textbox")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string Fullname { get; set; }

        [ResourceDisplayName("Employee_Label_BirthDate")]
        public DateTime? BirthDate { get; set; }

        [ResourceDisplayName("Employee_Label_Sex")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", EmployeeProfileManagementConstant.LISTSEX)]
        [AdditionalMetadata("Placeholder", "All")]
        public bool? Sex { get; set; }

        [ResourceDisplayName("Employee_Label_GraduationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", EmployeeProfileManagementConstant.LISTGRADUATIONLEVEL)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? TrainingLevel { get; set; }

        [ResourceDisplayName("Employee_Label_Contract")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", EmployeeProfileManagementConstant.LISTCONTRACTTYPE)]
        [AdditionalMetadata("Placeholder", "All")]
        public int? ContractType { get; set; }
    }
}