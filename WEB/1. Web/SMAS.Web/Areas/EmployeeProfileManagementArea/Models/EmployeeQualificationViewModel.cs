/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Resources;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.EmployeeProfileManagementArea.Models
{
    public class EmployeeQualificationViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 EmployeeQualificationID { get; set; }

        [ScaffoldColumn(false)]
        public System.Int32 EmployeeID { get; set; }

        [ResourceDisplayName("EmployeeProfileManagement_Column_FullName")]
        [UIHint("Display")]
        public string EmployeeName { get; set; }

        [ResourceDisplayName("Qualification_Control_QualifiedAt")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String QualifiedAt { get; set; }

        [ResourceDisplayName("Qualification_Control_Speciality")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_SPECIALITYCAT)]
        public System.Int32 SpecialityID { get; set; }

        [ResourceDisplayName("Qualification_Control_QualificationType")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_QUALIFICATIONTYPE)]
        public System.Nullable<System.Int32> QualificationTypeID { get; set; }

       

        [ResourceDisplayName("Qualification_Control_GraduationLevel")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_GRADUATIONLEVEL)]
        public System.Nullable<System.Int32> TrainingLevelID { get; set; }

        [ScaffoldColumn(false)]
        public System.Nullable<System.Int32> SchoolID { get; set; }

        [ScaffoldColumn(false)]
        public System.Nullable<System.Int32> SupervisingDeptID { get; set; }

        // Name
        [ScaffoldColumn(false)]
        [ResourceDisplayName("Qualification_Control_Speciality")]
        public string SpecialityName { get; set; }

        // Name
        [ScaffoldColumn(false)]
        [ResourceDisplayName("Qualification_Control_QualificationType")]
        public string QualificationTypeName { get; set; }

        //[ResDisplayName("Qualification_Control_QualifiedBy")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        //[StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        //public System.String QualifiedBy { get; set; }

        

        [ResourceDisplayName("Qualification_Control_Fromdate")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Nullable<System.DateTime> FromDate { get; set; }

        [ResourceDisplayName("Qualification_Control_QualifiedDate")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        public System.Nullable<System.DateTime> QualifiedDate { get; set; }

        [ScaffoldColumn(false)]
        public System.Nullable<System.Boolean> IsPrimaryQualification { get; set; }

        [ResourceDisplayName("Qualification_Control_IsRequalified")]
        [ScaffoldColumn(false)]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_ISREQUALIFIED)]
        public Nullable<bool> IsRequalified { get; set; }

        // Name
        [ScaffoldColumn(false)]
        [ResourceDisplayName("Qualification_Control_IsRequalified")]
        public string IsRequalifiedName { get; set; }

        //[ResDisplayName("Qualification_Control_QualificationGrade")]
        //[UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        //[AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_QUALIFICATIONGRADE)]
        //public System.Nullable<System.Int32> QualificationGradeID { get; set; }
        [ResourceDisplayName("Qualification_Control_Result")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [ScaffoldColumn(false)]
        public System.String Result { get; set; }

        // Name
        [ScaffoldColumn(false)]
        [ResourceDisplayName("Qualification_Control_QualificationGrade")]
        public string QualificationGradeName { get; set; }

        [ResourceDisplayName("Qualification_Control_Description")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Description { get; set; }
    }

    public class EmployeeQualificationListViewModel
    {
        public System.Int32 EmployeeQualificationID { get; set; }

        public System.Int32 EmployeeID { get; set; }

        [ResourceDisplayName("EmployeeProfileManagement_Column_FullName")]
        [UIHint("Display")]
        public string EmployeeName { get; set; }

        public System.Nullable<System.Int32> SchoolID { get; set; }

        public System.Nullable<System.Int32> SupervisingDeptID { get; set; }

        [ResourceDisplayName("Qualification_Control_GraduationLevel")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_GRADUATIONLEVEL)]
        public System.Nullable<System.Int32> TrainingLevelID { get; set; }

        // Name
        [ResourceDisplayName("Qualification_Control_GraduationLevel")]
        public string TrainingLevelName { get; set; }


        [ResourceDisplayName("Qualification_Control_Speciality")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_SPECIALITYCAT)]
        public System.Int32 SpecialityID { get; set; }

        // Name
        [ResourceDisplayName("Qualification_Control_Speciality")]
        public string SpecialityName { get; set; }

        [ResourceDisplayName("Qualification_Control_QualificationType")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_QUALIFICATIONTYPE)]
        public System.Nullable<System.Int32> QualificationTypeID { get; set; }

        // Name
        [ResourceDisplayName("Qualification_Control_QualificationType")]
        public string QualificationTypeName { get; set; }

        //[ResDisplayName("Qualification_Control_QualifiedBy")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        //[StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        //public System.String QualifiedBy { get; set; }

        [ResourceDisplayName("Qualification_Control_QualifiedAt")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String QualifiedAt { get; set; }

        [ResourceDisplayName("Qualification_Control_Fromdate")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        public System.Nullable<System.DateTime> FromDate { get; set; }

        [ResourceDisplayName("Qualification_Control_QualifiedDate")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        public System.Nullable<System.DateTime> QualifiedDate { get; set; }

        public System.Nullable<System.Boolean> IsPrimaryQualification { get; set; }

        [ResourceDisplayName("Qualification_Control_IsRequalified")]
        [UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_ISREQUALIFIED)]
        public Nullable<bool> IsRequalified { get; set; }

        // Name
        [ResourceDisplayName("Qualification_Control_IsRequalified")]
        public string IsRequalifiedName { get; set; }

        //[ResDisplayName("Qualification_Control_QualificationGrade")]
        //[UIHint("Combobox"), Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        //[AdditionalMetadata("ViewDataKey", EmployeeQualificationConstants.LIST_QUALIFICATIONGRADE)]
        //public System.Nullable<System.Int32> QualificationGradeID { get; set; }
        [ResourceDisplayName("Qualification_Control_Result")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Result { get; set; }

        // Name
        [ResourceDisplayName("Qualification_Control_QualificationGrade")]
        public string QualificationGradeName { get; set; }

        [ResourceDisplayName("Qualification_Control_Description")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public System.String Description { get; set; }
    }
}
