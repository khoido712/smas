﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EmployeeProfileManagementArea
{
    public class EmployeeProfileManagementAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EmployeeProfileManagementArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EmployeeProfileManagementArea_default",
                "EmployeeProfileManagementArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}