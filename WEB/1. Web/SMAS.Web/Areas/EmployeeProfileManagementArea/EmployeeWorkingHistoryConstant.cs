/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.EmployeeProfileManagementArea
{
    public class EmployeeWorkingHistoryConstants
    {
        public const string LIST_EMPLOYEEWORKINGHISTORY = "listEmployeeWorkingHistory";
        public const string IS_CURRENT_YEAR = "IS_CURRENT_YEAR";
        public const string IS_PERMISS = "IS_PERMISS";
        public const string HAS_PERMISSION = "HAS_PERMISSION";
    }
}