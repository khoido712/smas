﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EmployeeProfileManagementArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System.Linq;
using SMAS.Business.Common;
using System;

namespace SMAS.Web.Areas.EmployeeProfileManagementArea.Controllers
{
    public class EmployeeQualificationController : BaseController
    {
        private readonly IEmployeeQualificationBusiness EmployeeQualificationBusiness;
        private readonly IQualificationGradeBusiness QualificationGradeBusiness;
        private readonly IGraduationLevelBusiness GraduationLevelBusiness;
        private readonly IQualificationTypeBusiness QualificationTypeBusiness;
        private readonly ISpecialityCatBusiness SpecialityCatBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;

        public EmployeeQualificationController(IEmployeeQualificationBusiness EmployeeQualificationBusiness,
            IQualificationGradeBusiness QualificationGradeBusiness,
            IGraduationLevelBusiness GraduationLevelBusiness,
            ISpecialityCatBusiness SpecialityCatBusiness,
            IQualificationTypeBusiness QualificationTypeBusiness,
            IEmployeeBusiness EmployeeBusiness)
        {
            this.EmployeeQualificationBusiness = EmployeeQualificationBusiness;
            this.QualificationGradeBusiness = QualificationGradeBusiness;
            this.GraduationLevelBusiness = GraduationLevelBusiness;
            this.QualificationTypeBusiness = QualificationTypeBusiness;
            this.SpecialityCatBusiness = SpecialityCatBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }

        //
        // GET: /EmployeeQualification/Search


        public PartialViewResult Search(int EmployeeID, int hdfIsEmployee)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = EmployeeID;

            //add search info
            //
            GlobalInfo global = new GlobalInfo();
            var permission = -1;
            if (hdfIsEmployee == 1)
            {
                permission = GetMenupermission("EmployeeProfile_ManagementForTeacher", global.UserAccountID, global.IsAdmin);
            }
            else
            {
                permission = GetMenupermission("EmployeeProfileManagement", global.UserAccountID, global.IsAdmin);
            }
            ViewData[EmployeeQualificationConstants.HAS_PERMISSION] = permission;
            IEnumerable<EmployeeQualificationListViewModel> lst = new EmployeeInfo().SearchEmployeeQualification(EmployeeQualificationBusiness, SearchInfo);
            lst = lst.OrderByDescending(o => o.FromDate);
            ViewData[EmployeeQualificationConstants.LIST_EMPLOYEEQUALIFICATION] = lst;
            Employee employee = EmployeeBusiness.Find(EmployeeID);
            if (employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementConstant.EMPLOYEE_WORKING] = "false";
            }
            //Get view data here
            ViewData["checkReadonly"] = false;
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(int hdfIsEmployeeQualificationCreate)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            var permission = 0;
            if (hdfIsEmployeeQualificationCreate == 1)
            {
                permission = this.GetMenupermission("EmployeeProfile_ManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            else
            {
                permission = this.GetMenupermission("EmployeeProfileManagement", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            if (permission < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeQualification employeequalification = new EmployeeQualification();
            TryUpdateModel(employeequalification);
            Utils.Utils.TrimObject(employeequalification);
            employeequalification.SchoolID = new GlobalInfo().SchoolID.Value;

            this.EmployeeQualificationBusiness.Insert(employeequalification);
            this.EmployeeQualificationBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage"), JsonMessage.SUCCESS));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        //[ValidateAntiForgeryToken]

        public JsonResult Edit(int EmployeeQualificationID, int hdfIsEmployeeQualificationEdit)
        {
            
            GlobalInfo globalInfo = new GlobalInfo();
            CheckPermissionForAction(EmployeeQualificationID, "EmployeeQualification");
            var permission = 0;
            if (hdfIsEmployeeQualificationEdit == 1)
            {
                permission = this.GetMenupermission("EmployeeProfile_ManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            else
            {
                permission = this.GetMenupermission("EmployeeProfileManagement", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            if (permission < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeQualification employeequalification = this.EmployeeQualificationBusiness.Find(EmployeeQualificationID);
            TryUpdateModel(employeequalification);
            Utils.Utils.TrimObject(employeequalification);
            this.EmployeeQualificationBusiness.Update(employeequalification);
            this.EmployeeQualificationBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"),JsonMessage.SUCCESS));
            
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id, int hdfisEmployee)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            CheckPermissionForAction(id, "EmployeeQualification");
            var permission = 0;
            if (hdfisEmployee == 1)
            {
                permission = this.GetMenupermission("EmployeeProfile_ManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            else
            {
                permission = this.GetMenupermission("EmployeeProfileManagement", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }

            if (permission < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.EmployeeQualificationBusiness.Delete(id);
            this.EmployeeQualificationBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), JsonMessage.SUCCESS));
        }
    }
}
