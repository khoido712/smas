﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EmployeeProfileManagementArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using System.Globalization;

namespace SMAS.Web.Areas.EmployeeProfileManagementArea.Controllers
{
    public class EmployeeSalaryController : BaseController
    {
        private readonly IEmployeeSalaryBusiness EmployeeSalaryBusiness;
        private readonly ISalaryLevelBusiness SalaryLevelBusiness;
        private readonly IEmployeeScaleBusiness EmployeeScaleBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;

        public EmployeeSalaryController(IEmployeeSalaryBusiness employeesalaryBusiness,
            ISalaryLevelBusiness SalaryLevelBusiness,
            IEmployeeScaleBusiness EmployeeScaleBusiness,
            IEmployeeBusiness EmployeeBusiness)
        {
            this.EmployeeSalaryBusiness = employeesalaryBusiness;
            this.SalaryLevelBusiness = SalaryLevelBusiness;
            this.EmployeeScaleBusiness = EmployeeScaleBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }

        /// <summary>
        /// Lay he so
        /// </summary>
        /// <param name="salaryLevelID"></param>
        /// <returns></returns>

        [ValidateAntiForgeryToken]
        public JsonResult GetCoefficientByLevel(int EmployeeScaleID, int SubLevel)
        {
            IDictionary<string, object> dicParam = new Dictionary<string, object>();
            dicParam["ScaleID"] = EmployeeScaleID;
            dicParam["SubLevel"] = SubLevel;
            List<SalaryLevel> ListSalaryLevel = SalaryLevelBusiness.Search(dicParam).ToList();
            EmployeeSalaryViewModel EmployeeSalaryViewModel = new EmployeeSalaryViewModel();
            if (ListSalaryLevel.Count > 0)
            {
                SalaryLevel SalaryLevel = ListSalaryLevel[0];
                EmployeeSalaryViewModel.Coefficient = SalaryLevel.Coefficient;
                EmployeeSalaryViewModel.SalaryLevelID = SalaryLevel.SalaryLevelID;
            }
            return Json(EmployeeSalaryViewModel);
        }

        public string GetEmployeeScaleCode(int EmployeeScaleID)
        {
            string strResult = string.Empty;
            EmployeeScale objES = EmployeeScaleBusiness.Find(EmployeeScaleID);
            strResult = objES != null ? objES.EmployeeScaleCode : "";
            return strResult;
        }

        //
        // GET: /EmployeeSalary/Search


        public PartialViewResult Search(int EmployeeID, int hdfIsEmployee)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = EmployeeID;
            GlobalInfo global = new GlobalInfo();
            var permission = -1;
            if (hdfIsEmployee == 1)
            {
                permission = GetMenupermission("EmployeeProfile_ManagementForTeacher", global.UserAccountID, global.IsAdmin);
            }
            else
            {
                permission = GetMenupermission("EmployeeProfileManagement", global.UserAccountID, global.IsAdmin);
            }
            ViewData[EmployeeProfileManagementConstant.HAS_PERMISSION] = permission;
            IEnumerable<EmployeeSalaryViewModel> lst = new EmployeeInfo().SearchEmployeeSalary(EmployeeSalaryBusiness,EmployeeScaleBusiness,SalaryLevelBusiness, SearchInfo);
            ViewData[EmployeeSalaryConstants.LIST_EMPLOYEESALARY] = lst;
            Employee em = EmployeeBusiness.Find(EmployeeID);
            if (em.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementConstant.EMPLOYEE_WORKING] = "false";
            }
            //Get view data here
            ViewData["checkReadonly"] = false;
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(FormCollection frm)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            var permission = 0;
            int hdfIsEmployeeSalaryCreate = frm["hdfIsEmployeeSalaryCreate"] != null ? int.Parse(frm["hdfIsEmployeeSalaryCreate"]) : 0;
            if (hdfIsEmployeeSalaryCreate == 1)
            {
                permission = this.GetMenupermission("EmployeeProfile_ManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            else
            {
                permission = this.GetMenupermission("EmployeeProfileManagement", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            if (permission < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeSalary employeesalary = new EmployeeSalary();
            TryUpdateModel(employeesalary);
            if (!employeesalary.AppliedDate.HasValue)
            {
                return Json(new JsonMessage(Res.Get("Ngày hưởng không được để trống"), "error"));
            }
            if (!employeesalary.EmployeeScaleID.HasValue)
            {
                return Json(new JsonMessage(Res.Get("Ngạch/hạng không được để trống"), "error"));
            }
            if (!employeesalary.SalaryLevelID.HasValue)
            {
                return Json(new JsonMessage(Res.Get("Bậc lương không được để trống"), "error"));
            }
            if (string.IsNullOrEmpty(frm["CoefficientName"]))
            {
                return Json(new JsonMessage(Res.Get("Hệ số lương không được để trống"), "error"));
            }

            if (!string.IsNullOrEmpty(frm["CoefficientName"]))
            {
                decimal val = 0;
                if (decimal.TryParse(frm["CoefficientName"].Replace(".",","), out val))
                {
                    if (val > 100)
                    {
                        return Json(new JsonMessage(Res.Get("Hệ số lương phải nẳm trong khoảng [0 - 99.99]"), "error"));
                    }
                    else
                    {
                        employeesalary.Coefficient = val;
                    }
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("Hệ số lương phải nẳm trong khoảng [0 - 99.99]"), "error"));
                }
            }
            Utils.Utils.TrimObject(employeesalary);
            employeesalary.SchoolID = globalInfo.SchoolID;
            employeesalary.SupervisingDeptID = globalInfo.SupervisingDeptID;

            this.EmployeeSalaryBusiness.Insert(employeesalary);
            this.EmployeeSalaryBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int EmployeeSalaryID,FormCollection frm)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            CheckPermissionForAction(EmployeeSalaryID, "EmployeeSalary");
            var permission = 0;
            int hdfIsEmployeeSalaryEdit = frm["hdfIsEmployeeSalaryEdit"] != null ? int.Parse(frm["hdfIsEmployeeSalaryEdit"]) : 0;
            if (hdfIsEmployeeSalaryEdit == 1)
            {
                permission = this.GetMenupermission("EmployeeProfile_ManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            else
            {
                permission = this.GetMenupermission("EmployeeProfileManagement", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            if (permission < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeSalary employeesalary = this.EmployeeSalaryBusiness.Find(EmployeeSalaryID);
            TryUpdateModel(employeesalary);
            if (!employeesalary.AppliedDate.HasValue)
            {
                return Json(new JsonMessage(Res.Get("Ngày hưởng không được để trống"), "error"));
            }
            if (!employeesalary.EmployeeScaleID.HasValue)
            {
                return Json(new JsonMessage(Res.Get("Ngạch/hạng không được để trống"), "error"));
            }
            if (!employeesalary.SalaryLevelID.HasValue)
            {
                return Json(new JsonMessage(Res.Get("Bậc lương không được để trống"), "error"));
            }
            if (string.IsNullOrEmpty(frm["CoefficientName"]))
            {
                return Json(new JsonMessage(Res.Get("Hệ số lương không được để trống"), "error"));
            }
            if (!string.IsNullOrEmpty(frm["CoefficientName"]))
            {
                decimal val = 0;
                if (decimal.TryParse(frm["CoefficientName"].Replace(".",","),out val))
                {
                    if (val > 100)
                    {
                        return Json(new JsonMessage(Res.Get("Hệ số lương phải nẳm trong khoảng [0 - 99.99]"), "error"));
                    }
                    else
                    {
                        employeesalary.Coefficient = val;
                    }
                }
                else
                {
                    return Json(new JsonMessage(Res.Get("Hệ số lương phải nẳm trong khoảng [0 - 99.99]"), "error"));
                }
            }
            
            Utils.Utils.TrimObject(employeesalary);
            employeesalary.SchoolID = globalInfo.SchoolID;
            employeesalary.SupervisingDeptID = globalInfo.SupervisingDeptID;
            this.EmployeeSalaryBusiness.Update(employeesalary);
            this.EmployeeSalaryBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id, int isEmployeeSalary)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            var permission = 0;
            if (isEmployeeSalary == 1)
            {
                permission = this.GetMenupermission("EmployeeProfile_ManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            else
            {
                permission = this.GetMenupermission("EmployeeProfileManagement", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            if (permission < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            CheckPermissionForAction(id, "EmployeeSalary");
            this.EmployeeSalaryBusiness.Delete(id);
            this.EmployeeSalaryBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
    }
}
