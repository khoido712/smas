using System.Collections.Generic;
using System.Linq;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EmployeeProfileManagementArea.Models;
using SMAS.Web.Utils;

namespace SMAS.Web.Areas.EmployeeProfileManagementArea.Controllers
{
    public class EmployeeInfo
    {
        private string GRADE = Res.Get("Common_Label_Grade");

        #region Tim kiem thong tin dao tao cua can bo

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeQualificationListViewModel> SearchEmployeeQualification(IEmployeeQualificationBusiness EmployeeQualificationBusiness,
            IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<EmployeeQualification> query = EmployeeQualificationBusiness.All;
            if (global.SchoolID != null)
            {
                query = EmployeeQualificationBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo);
            }
            else
            {
                query = EmployeeQualificationBusiness.All.Where(o => o.EmployeeID == global.EmployeeID);
            }
            IQueryable<EmployeeQualificationListViewModel> lst = query.Select(o => new EmployeeQualificationListViewModel
            {
                EmployeeQualificationID = o.EmployeeQualificationID,
                EmployeeID = o.EmployeeID,
                EmployeeName = o.Employee.FullName,
                SchoolID = o.SchoolID,
                SupervisingDeptID = o.SupervisingDeptID,
                SpecialityID = o.SpecialityID,
                SpecialityName = o.SpecialityCat.Resolution,
                QualificationTypeID = o.QualificationTypeID,
                QualificationTypeName = o.QualificationType.Resolution,
                //TrainingLevelID = (int?)o.Employee.TrainingLevelID,
                //TrainingLevelName = o.Employee.TrainingLevel.Resolution,
                TrainingLevelID = (int?)o.TrainingLevelID,
                TrainingLevelName = o.TrainingLevel.Resolution,
                //QualificationGradeID = o.QualificationGradeID,
                QualificationGradeName = o.QualificationGrade.Resolution,
                Result = o.Result,
                QualifiedAt = o.QualifiedAt,
                FromDate = o.FromDate,
                QualifiedDate = o.QualifiedDate,
                IsPrimaryQualification = o.IsPrimaryQualification,
                IsRequalified = o.IsRequalified,
                Description = o.Description
            });

            IEnumerable<EmployeeQualificationListViewModel> listRes = lst.ToList();
            foreach (EmployeeQualificationListViewModel item in listRes)
            {
                item.IsRequalifiedName = (item.IsRequalified.HasValue) ?
                    (item.IsRequalified.Value ?
                    Res.Get("EmployeeQualification_Label_ReTraining") : Res.Get("EmployeeQualification_Label_FirstTraining"))
                    : string.Empty;
            }
            return listRes;
        }

        #endregion Tim kiem thong tin dao tao cua can bo

        #region Tim kiem thong tin luong cua can bo

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeSalaryViewModel> SearchEmployeeSalary(IEmployeeSalaryBusiness EmployeeSalaryBusiness,IEmployeeScaleBusiness EmployeeScaleBusiness,
            ISalaryLevelBusiness SalaryLevelBusiness,IDictionary<string, object> SearchInfo)
        {
            IQueryable<EmployeeSalary> query = EmployeeSalaryBusiness.Search(SearchInfo);
            IQueryable<EmployeeSalaryViewModel> lst = (from q in query
                                                       join ec in EmployeeScaleBusiness.All on q.EmployeeScaleID equals ec.EmployeeScaleID
                                                       join sl in SalaryLevelBusiness.All on q.SalaryLevelID equals sl.SalaryLevelID
                                                       select new EmployeeSalaryViewModel
                                                         {
                                                             EmployeeSalaryID = q.EmployeeSalaryID,
                                                             EmployeeID = q.EmployeeID,
                                                             EmployeeName = q.Employee != null ? q.Employee.FullName : string.Empty,
                                                             EmployeeScaleID = q.EmployeeScaleID,
                                                             EmployeeScaleCode = ec.EmployeeScaleCode,
                                                             EmployeeScaleName = ec.Resolution,
                                                             SalaryLevelID = q.SalaryLevelID,
                                                             SalaryLevelName = sl.Resolution,
                                                             Coefficient = q.Coefficient,
                                                             SchoolID = q.SchoolID,
                                                             SupervisingDeptID = q.SupervisingDeptID,
                                                             AppliedDate = q.AppliedDate,
                                                             SalaryResolution = q.SalaryResolution,
                                                             SalaryAmount = q.SalaryAmount,
                                                             Description = q.Description
                                                         }).OrderByDescending(p => p.AppliedDate);
            return lst.ToList();
        }
        #endregion Tim kiem thong tin luong cua can bo
    }
}
