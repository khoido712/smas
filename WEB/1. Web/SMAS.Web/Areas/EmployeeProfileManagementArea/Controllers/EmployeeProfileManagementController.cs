﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EmployeeProfileManagementArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using Newtonsoft.Json;
using System.Text;
using Telerik.Web.Mvc;
using SMAS.Web.Areas.EmployeeBreatherArea;
using SMAS.VTUtils.Log;
using System.Globalization;

namespace SMAS.Web.Areas.EmployeeProfileManagementArea.Controllers
{
    public class EmployeeProfileManagementController : BaseController
    {
        #region variable
        private readonly ITrainingLevelBusiness TrainingLevelBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IWorkTypeBusiness WorkTypeBusiness;
        private readonly IGraduationLevelBusiness GraduationLevelBusiness;
        private readonly IContractTypeBusiness ContractTypeBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IGroupCatBusiness GroupCatBusiness;
        private readonly IEthnicBusiness EthnicBusiness;
        private readonly IReligionBusiness ReligionBusiness;
        private readonly IFamilyTypeBusiness FamilyTypeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEmployeeBreatherBusiness EmployeeBreatherBusiness;

        private readonly IQualificationTypeBusiness QualificationTypeBusiness;
        private readonly ISpecialityCatBusiness SpecialityCatBusiness;
        private readonly IQualificationLevelBusiness QualificationLevelBusiness;

        private readonly IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness;
        private readonly IITQualificationLevelBusiness ITQualificationLevelBusiness;

        private readonly IStateManagementGradeBusiness StateManagementGradeBusiness;
        private readonly IPoliticalGradeBusiness PoliticalGradeBusiness;

        private readonly IEducationalManagementGradeBusiness EducationalManagementGradeBusiness;

        private readonly IStaffPositionBusiness StaffPositionBusiness;
        private readonly IContractBusiness ContractBusiness;
        private readonly IWorkGroupTypeBusiness WorkGroupTypeBusiness;
        private readonly IEmployeeWorkingHistoryBusiness EmployeeWorkingHistoryBusiness;
        private ISubjectCatBusiness SubjectCatBusiness;
        private IQualificationGradeBusiness QualificationGradeBusiness;
        private IEmployeeQualificationBusiness EmployeeQualificationBusiness;
        private IEmployeeSalaryBusiness EmployeeSalaryBusiness;
        private IEmployeeScaleBusiness EmployeeScaleBusiness;
        private readonly IEmployeeEvaluationBusiness EmployeeEvaluationBusiness;
        private string GRADE = Res.Get("Common_Label_Grade");
        private readonly ICodeConfigBusiness CodeConfigBusiness;
        private string TEMPLATE_EMPLOYEE = "GV_DanhSachCanBo.xls";
        private string TEMPLATE_EMPLOYEE_NAME = "GV_DanhSachCanBo_{0}.xls";
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ICommuneBusiness CommuneBusiness;
        private readonly IEmployeeWorkMovementBusiness EmployeeWorkMovementBusiness;

        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IVacationReasonBusiness VacationReasonBusiness;

        private readonly IProfileTemplateBusiness ProfileTemplateBusiness;
        private readonly IColumnDescriptionBusiness ColumnDescriptionBusiness;
        private readonly IForeignLanguageCatBusiness ForeignLanguageCatBusiness;
        private readonly IConcurrentWorkTypeBusiness ConcurrentWorkTypeBusiness;
        private readonly IConcurrentWorkAssignmentBusiness ConcurrentWorkAssignmentBusiness;
        private readonly ISalaryLevelBusiness SalaryLevelBusiness;
        #endregion variable

        #region contructor

        public EmployeeProfileManagementController(IEmployeeBusiness employeeBusiness
            , ISchoolFacultyBusiness SchoolFacultyBusiness
            , IWorkTypeBusiness WorkTypeBusiness
            , IGraduationLevelBusiness GraduationLevelBusiness
            , IContractTypeBusiness ContractTypeBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness,
            IGroupCatBusiness GroupBusiness, IEthnicBusiness EthnicBusiness, IReligionBusiness ReligionBusiness
            , IFamilyTypeBusiness FamilyTypeBusiness, IQualificationTypeBusiness QualificationTypeBusiness,
            ISpecialityCatBusiness SpecialityCatBusiness, IQualificationLevelBusiness QualificationLevelBusiness,
            IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness, IITQualificationLevelBusiness ITQualificationLevelBusiness,
            IStateManagementGradeBusiness StateManagementGradeBusiness,
            IPoliticalGradeBusiness PoliticalGradeBusiness,
            IEducationalManagementGradeBusiness EducationalManagementGradeBusiness,
            IStaffPositionBusiness StaffPositionBusiness,
            IContractBusiness ContractBusiness
            , IWorkGroupTypeBusiness WorkGroupTypeBusiness
            , ISubjectCatBusiness SubjectCatBusiness
            , IEmployeeWorkingHistoryBusiness EmployeeWorkingHistoryBusiness
            , IQualificationGradeBusiness QualificationGradeBusiness
            , IEmployeeQualificationBusiness EmployeeQualificationBusiness
            , IEmployeeSalaryBusiness EmployeeSalaryBusiness
            , IEmployeeScaleBusiness EmployeeScaleBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , ICodeConfigBusiness CodeConfigBusiness
            , IProvinceBusiness ProvinceBusiness
            , IDistrictBusiness DistrictBusiness
            , ICommuneBusiness CommuneBusiness
            , ITrainingLevelBusiness TrainingLevelBusiness
            , IEmployeeWorkMovementBusiness employeeWorkMovementBusiness
            , IEmployeeBreatherBusiness employeeBreatherBusiness
            , IUserAccountBusiness UserAccountBusiness
            , IVacationReasonBusiness VacationReasonBusiness,
             IDistrictBusiness districtBusiness,
            ICommuneBusiness communeBusiness,
            IProfileTemplateBusiness ProfileTemplateBusiness,
            IColumnDescriptionBusiness ColumnDescriptionBusiness,
            IForeignLanguageCatBusiness ForeignLanguageCatBusiness,
            IEmployeeEvaluationBusiness EmployeeEvaluationBusiness,
            IConcurrentWorkTypeBusiness ConcurrentWorkTypeBusiness,
            IConcurrentWorkAssignmentBusiness ConcurrentWorkAssignmentBusiness,
            ISalaryLevelBusiness SalaryLevelBusiness
            )
        {
            this.TrainingLevelBusiness = TrainingLevelBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.CommuneBusiness = CommuneBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.WorkTypeBusiness = WorkTypeBusiness;
            this.GraduationLevelBusiness = GraduationLevelBusiness;
            this.ContractTypeBusiness = ContractTypeBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.GroupCatBusiness = GroupBusiness;
            this.EthnicBusiness = EthnicBusiness;
            this.ReligionBusiness = ReligionBusiness;
            this.FamilyTypeBusiness = FamilyTypeBusiness;
            this.QualificationTypeBusiness = QualificationTypeBusiness;
            this.SpecialityCatBusiness = SpecialityCatBusiness;
            this.QualificationLevelBusiness = QualificationLevelBusiness;
            this.ForeignLanguageGradeBusiness = ForeignLanguageGradeBusiness;
            this.ITQualificationLevelBusiness = ITQualificationLevelBusiness;
            this.StateManagementGradeBusiness = StateManagementGradeBusiness;
            this.PoliticalGradeBusiness = PoliticalGradeBusiness;

            this.EducationalManagementGradeBusiness = EducationalManagementGradeBusiness;
            this.StaffPositionBusiness = StaffPositionBusiness;
            this.ContractBusiness = ContractBusiness;
            this.WorkGroupTypeBusiness = WorkGroupTypeBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.EmployeeWorkingHistoryBusiness = EmployeeWorkingHistoryBusiness;
            this.QualificationGradeBusiness = QualificationGradeBusiness;
            this.EmployeeQualificationBusiness = EmployeeQualificationBusiness;
            this.EmployeeSalaryBusiness = EmployeeSalaryBusiness;
            this.EmployeeScaleBusiness = EmployeeScaleBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.CodeConfigBusiness = CodeConfigBusiness;
            this.EmployeeWorkMovementBusiness = employeeWorkMovementBusiness;
            this.EmployeeBreatherBusiness = employeeBreatherBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.VacationReasonBusiness = VacationReasonBusiness;
            this.DistrictBusiness = districtBusiness;
            this.CommuneBusiness = communeBusiness;
            this.ProfileTemplateBusiness = ProfileTemplateBusiness;
            this.ColumnDescriptionBusiness = ColumnDescriptionBusiness;
            this.ForeignLanguageCatBusiness = ForeignLanguageCatBusiness;
            this.EmployeeEvaluationBusiness = EmployeeEvaluationBusiness;
            this.ConcurrentWorkTypeBusiness = ConcurrentWorkTypeBusiness;
            this.ConcurrentWorkAssignmentBusiness = ConcurrentWorkAssignmentBusiness;
            this.SalaryLevelBusiness = SalaryLevelBusiness;
        }

        #endregion contructor

        #region index

        //
        // GET: /EmployeeProfileManagement/

        public ActionResult Index(int? LoadSS)
        {
            CheckActionPermissionMinView();
            SetViewData_SearchForm();
            SetProfileTemplate(0);
            SetRowExcel();
            //ViewData[EmployeeProfileManagementConstant.LIST_COLUMN_TEMPLATE] = GetColumnDescription(0);
            GlobalInfo global = new GlobalInfo();
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["AcademicYearID"] = global.AcademicYearID;

            ViewData[EmployeeProfileManagementConstant.LIST_EMPLOYEE] = new List<EmployeeProfileManagementViewModel>();
            if (LoadSS == 1)
            {
                // Lay lai gia tri cac tham so tu session
                Dictionary<string, object> dic = (Dictionary<string, object>)Session["forBack"];
                // Truyen cac tham so cho view
                ViewData[EmployeeProfileManagementConstant.FORBACK] = dic;
                //IEnumerable<EmployeeProfileManagementViewModel> paging = this._Search(dic);
                //ViewData[EmployeeProfileManagementConstant.LIST_EMPLOYEE] = paging;
            }
            ViewData[EmployeeBreatherConstant.LIST_REASON] = new SelectList(VacationReasonBusiness.GetAll(), "VacationReasonID", "VacationName");


            return View();
        }

        #endregion index

        #region create

        #region create or edit

        public ActionResult CreateOrEdit()
        {
            SetViewData();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo.Clear();
            IQueryable<EmployeeScale> IQSalaryScale = EmployeeScaleBusiness.Search(SearchInfo);
            SelectList listSalaryLevel = new SelectList((IEnumerable<EmployeeScale>)IQSalaryScale.ToList(), "EmployeeScaleID", "Resolution");
            ViewData[EmployeeSalaryConstants.LIST_SALARYSCALE] = listSalaryLevel;

            List<ComboObject> listComboObj = new List<ComboObject>();
            List<SalaryLevel> lstSalaryLevel = SalaryLevelBusiness.All.Where(p => p.IsActive).ToList();
            SelectList lstSelectList = new SelectList((IEnumerable<SalaryLevel>)lstSalaryLevel, "SalaryLevelID", "Resolution");
            ViewData[EmployeeSalaryConstants.LIST_SALARYLEVEL] = lstSelectList;
            ViewData[EmployeeProfileManagementConstant.IS_EDIT] = false;
            return PartialView("_Create");
        }

        #endregion create or edit

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            var Permission2 = GetMenupermission("EmployeeProfileManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            if (Permission2 < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Employee employee = new Employee();
            EmployeeProfileManagementViewModel EPMVM = new EmployeeProfileManagementViewModel();
            TryUpdateModel(EPMVM);
            Utils.Utils.BindTo(EPMVM, employee);

            if (EPMVM.IsSyndicate)
            {
                if (EPMVM.SyndicateDate.HasValue && EPMVM.SyndicateDate.Value.Date > DateTime.Now.Date)
                {
                    throw new SMAS.Business.Common.BusinessException("Message_Syndicate_Error");
                }
            }

            //VALIDATE nam sinh,doi du lieu tu nam sinh object sang nam sinh cua entity
            if (EPMVM.iFatherBirthDate.HasValue)
            {
                if (EPMVM.iFatherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_Father_Error");
                }
                if (EPMVM.iFatherBirthDate >= 2000)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Max_BirthYear_Father_Error");
                }
                employee.FatherBirthDate = new DateTime(EPMVM.iFatherBirthDate.Value, 1, 1);
            }
            if (EPMVM.iMotherBirthDate.HasValue)
            {
                if (EPMVM.iMotherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_Mother_Error");
                }
                if (EPMVM.iMotherBirthDate >= 2000)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Max_BirthYear_Mother_Error");
                }
                employee.MotherBirthDate = new DateTime(EPMVM.iMotherBirthDate.Value, 1, 1);
            }
            if (EPMVM.iSpouseBirthDate.HasValue)
            {
                if (EPMVM.iSpouseBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_SPouse_Error");
                }
                if (EPMVM.iSpouseBirthDate >= 2000)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Max_BirthYear_SPouse_Error");
                }
                employee.SpouseBirthDate = new DateTime(EPMVM.iSpouseBirthDate.Value, 1, 1);
            }
            //if (EPMVM.ContractTypeID != 0 && EPMVM.ContractTypeID != null)
            //{
            //    ContractType contractType = ContractTypeBusiness.Find(EPMVM.ContractTypeID);
            //    if (contractType.Resolution.ToLower() == SystemParamsInFile.CONTRACT_TYPE_STAFF_NAME)
            //    {
            //        if (EPMVM.JoinedDate == null)
            //        {
            //            throw new SMAS.Business.Common.BusinessException("Employee_ContractType_Label_JoinedDate");
            //        }
            //    }
            //}

            if ((employee.WorkTypeID == 10 || employee.WorkTypeID == 12) && (!employee.AppliedLevel.HasValue || employee.AppliedLevel == 0))
            {
                throw new SMAS.Business.Common.BusinessException("Thầy/cô chưa chọn cấp dạy chính");
            }


            //
            Utils.Utils.TrimObject(employee);

            //truong Name lay tu FullName
            int space = employee.FullName.LastIndexOf(" ");
            if (space == -1)
            {
                employee.Name = employee.FullName;
            }
            else
            {
                employee.Name = employee.FullName.Substring(space + 1);
            }

            //dua image vao employee
            object imagePath = Session["ImagePath"];
            if (imagePath != null)
            {
                byte[] imageBytes = FileToByteArray((string)imagePath);
                employee.Image = imageBytes;
            }
            GlobalInfo global = new GlobalInfo();
            employee.SchoolID = global.SchoolID;

            //Neu truong chon tu dong generate code
            if (CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
            {
                //neu truong khong duoc sua thi lay ma tu dong
                if (!CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
                {
                    employee.EmployeeCode = CodeConfigBusiness.CreateTeacherCode(global.SchoolID.Value);
                }
                else
                {
                    //Neu truong duoc sua va truong chon tu dong gen
                    if (EPMVM.AutoGenCode)
                    {
                        employee.EmployeeCode = CodeConfigBusiness.CreateTeacherCode(global.SchoolID.Value);
                    }
                }
            }

            IDictionary<string, object> dicInsertSalary = new Dictionary<string, object>();
            EmployeeSalary objES = new EmployeeSalary();
            bool isInsertSalary = false;
            if (EPMVM.EmployeeScaleID.HasValue || EPMVM.SubLevel.HasValue || EPMVM.AppliedDateID.HasValue || !string.IsNullOrEmpty(EPMVM.Coefficient))
            {
                if (EPMVM.EmployeeScaleID.HasValue && EPMVM.SubLevel.HasValue && EPMVM.AppliedDateID.HasValue && !string.IsNullOrEmpty(EPMVM.Coefficient))
                {
                    if (!string.IsNullOrEmpty(EPMVM.Coefficient))
                    {
                        decimal val = 0;
                        if (decimal.TryParse(EPMVM.Coefficient.Replace(".", ","), out val))
                        {
                            if (val > 100)
                            {
                                return Json(new JsonMessage(Res.Get("Hệ số lương phải nẳm trong khoảng [0 - 99.99]"), "error"));
                            }
                            else
                            {
                                objES.Coefficient = val;
                                isInsertSalary = true;
                            }
                        }
                        else
                        {
                            return Json(new JsonMessage(Res.Get("Hệ số lương phải nẳm trong khoảng [0 - 99.99]"), "error"));
                        }
                    }
                }
                else
                {
                    return Json(new JsonMessage("Để nhập thông tin lương cần phải nhập đủ các trường thông tin: Ngày hưởng, Ngạch/hạng, Bậc lương, Hệ số", "error"));
                }
            }
            employee.VocationalAllowance = EPMVM.VOCATIONAL_ALLOWANCE;
            employee.SeniorityAllowance = EPMVM.SENIORITY_ALLOWANCE;
            employee.PreferentialAllowance = EPMVM.PREFERENTIAL_ALLOWANCE;
            //Danh hieu phong tang
            employee.HigiestTitle = EPMVM.AppellationAward;
            this.EmployeeBusiness.InsertTeacher(employee);

            if (EPMVM.ConcurrentWorkId.HasValue)
            {
                ConcurrentWorkAssignment newObj = new ConcurrentWorkAssignment();
                newObj.AcademicYearID = _globalInfo.AcademicYearID.Value;
                newObj.ConcurrentWorkTypeID = EPMVM.ConcurrentWorkId.Value;
                newObj.FacultyID = EPMVM.SchoolFacultyID;
                newObj.SchoolID = _globalInfo.SchoolID.Value;
                newObj.Employee = employee;
                newObj.IsActive = true;
                ConcurrentWorkAssignmentBusiness.Insert(newObj);
            }

            this.EmployeeBusiness.Save();

            if (isInsertSalary)
            {
                //Tao doi tuong luu bang luong
                objES.EmployeeID = employee.EmployeeID;
                objES.EmployeeScaleID = EPMVM.EmployeeScaleID;
                objES.SalaryLevelID = EPMVM.SubLevel;
                objES.SchoolID = _globalInfo.SchoolID;
                objES.SupervisingDeptID = _globalInfo.SupervisingDeptID;
                objES.AppliedDate = EPMVM.AppliedDateID;
                objES.SalaryAmount = EPMVM.SalaryAmount;
                dicInsertSalary = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AppliedDate", EPMVM.AppliedDateID},
                    {"EmployeeID",employee.EmployeeID}
                };
                //luu luong cua can bo
                EmployeeSalaryBusiness.InsertOrUpdateSalary(dicInsertSalary, objES);
                EmployeeSalaryBusiness.Save();
            }

            

            // Tạo giá trị lưu log action_audit
            string newObject = JsonConvert.SerializeObject(SMAS.Business.Common.Utils.BindTo<EmployeeBO>(employee), new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects }); ;
            SetViewDataActionAudit(String.Empty, newObject, employee.EmployeeID.ToString(), "Add employee_id:" + employee.EmployeeID.ToString(), employee.EmployeeID.ToString(), "Hồ sơ cán bộ/nhân viên", GlobalConstants.ACTION_ADD, "Thêm mới " + employee.FullName + " mã " + employee.EmployeeCode);

            ModelState.Clear();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        #endregion create

        #region edit,update

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionAudit]
        public JsonResult Edit(EmployeeProfileManagementViewModel EmployeeProfileManagementViewModel)
        {
            CheckPermissionForAction(EmployeeProfileManagementViewModel.EmployeeID, "Employee");
            GlobalInfo global = new GlobalInfo();
            var hasPermision = -1;
            if (EmployeeProfileManagementViewModel.hdfisEmployee == 1)
            {
                hasPermision = GetMenupermission("EmployeeProfile_ManagementForTeacher", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            }
            else
            {
                hasPermision = GetMenupermission("EmployeeProfileManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            }
            if (hasPermision < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Employee employee = EmployeeBusiness.Find(EmployeeProfileManagementViewModel.EmployeeID);
            //string oldObject = JsonConvert.SerializeObject(SMAS.Business.Common.Utils.BindTo<EmployeeBO>(employee), new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects });// convert to json string for old object
            string oldObject = "{EmployeeID:" + employee.EmployeeID + ", EmployeeCode:\"" + employee.EmployeeCode + "\", FullName:\"" + employee.FullName + "\", BirthDate:\"" + employee.BirthDate.ToString() + "\",EthnicID:" + employee.EthnicID + ",SchoolFacultyID:" + employee.SchoolFacultyID + ",IntoSchoolDate:\"" + employee.IntoSchoolDate.ToString() + "\",ContractTypeID:" + employee.ContractTypeID + ",WorkGroupTypeID:" + employee.WorkGroupTypeID + ",WorkTypeID:" + employee.WorkTypeID + ", AppliedLevel:" + employee.AppliedLevel + "}";
            if (!CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
            {
                EmployeeProfileManagementViewModel.EmployeeCode = employee.EmployeeCode;
            }
            EmployeeProfileManagementViewModel.Image = employee.Image;
            Utils.Utils.BindTo(EmployeeProfileManagementViewModel, employee);
            if (EmployeeProfileManagementViewModel.IsSyndicate)
            {
                if (EmployeeProfileManagementViewModel.SyndicateDate.HasValue && EmployeeProfileManagementViewModel.SyndicateDate.Value.Date > DateTime.Now.Date)
                {
                    throw new SMAS.Business.Common.BusinessException("Message_Syndicate_Error");
                }
            }
            if (EmployeeProfileManagementViewModel.iFatherBirthDate.HasValue)
            {
                if (EmployeeProfileManagementViewModel.iFatherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_Father_Error");
                }
                if (EmployeeProfileManagementViewModel.iFatherBirthDate >= 2000)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Max_BirthYear_Father_Error");
                }
                employee.FatherBirthDate = new DateTime(EmployeeProfileManagementViewModel.iFatherBirthDate.Value, 1, 1);
            }
            if (EmployeeProfileManagementViewModel.iMotherBirthDate.HasValue)
            {
                if (EmployeeProfileManagementViewModel.iMotherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_Mother_Error");
                }
                if (EmployeeProfileManagementViewModel.iMotherBirthDate >= 2000)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Max_BirthYear_Mother_Error");
                }
                employee.MotherBirthDate = new DateTime(EmployeeProfileManagementViewModel.iMotherBirthDate.Value, 1, 1);
            }
            if (EmployeeProfileManagementViewModel.iSpouseBirthDate.HasValue)
            {
                if (EmployeeProfileManagementViewModel.iSpouseBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYear_SPouse_Error");
                }
                if (EmployeeProfileManagementViewModel.iSpouseBirthDate >= 2000)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Max_BirthYear_SPouse_Error");
                }
                employee.SpouseBirthDate = new DateTime(EmployeeProfileManagementViewModel.iSpouseBirthDate.Value, 1, 1);
            }

            //if (EmployeeProfileManagementViewModel.ContractTypeID != 0 && EmployeeProfileManagementViewModel.ContractTypeID != null)
            //{
            //    ContractType contractType = ContractTypeBusiness.Find(EmployeeProfileManagementViewModel.ContractTypeID);
            //    if (contractType.Resolution.ToLower() == SystemParamsInFile.CONTRACT_TYPE_STAFF_NAME)
            //    {
            //        if (EmployeeProfileManagementViewModel.JoinedDate == null)
            //        {
            //            throw new SMAS.Business.Common.BusinessException("Employee_ContractType_Label_JoinedDate");
            //        }
            //    }
            //}

            if ((employee.WorkTypeID == 10 || employee.WorkTypeID == 12) && (!employee.AppliedLevel.HasValue || employee.AppliedLevel == 0))
            {
                throw new SMAS.Business.Common.BusinessException("Thầy/cô chưa chọn cấp dạy chính");
            }
            //truong Name lay tu FullName
            int space = employee.FullName.LastIndexOf(" ");
            if (space == -1)
            {
                employee.Name = employee.FullName;
            }
            else
            {
                employee.Name = employee.FullName.Substring(space + 1);
            }

            //dua image vao employee
            object imagePath = Session["ImagePath"];
            if (imagePath != null)
            {
                byte[] imageBytes = FileToByteArray((string)imagePath);
                employee.Image = imageBytes;
            }

            employee.SchoolID = global.SchoolID;
            employee.IsActive = true;

            employee.HigiestTitle = EmployeeProfileManagementViewModel.AppellationAward;//Danh hieu phong tang


            if (CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER)     //Neu truong duoc tu dong generate code
                && CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER) //Truong duoc sua
                && EmployeeProfileManagementViewModel.AutoGenCode)                                                                       //Nguoi dung chon sua
            {
                employee.EmployeeCode = CodeConfigBusiness.CreateTeacherCode(global.SchoolID.Value);
            }

            //Tao doi tuong luu luong
            if (EmployeeProfileManagementViewModel.EmployeeScaleID.HasValue || EmployeeProfileManagementViewModel.SubLevel.HasValue
                || EmployeeProfileManagementViewModel.AppliedDateID.HasValue || !string.IsNullOrEmpty(EmployeeProfileManagementViewModel.Coefficient))
            {
                if (EmployeeProfileManagementViewModel.EmployeeScaleID.HasValue && EmployeeProfileManagementViewModel.SubLevel.HasValue
                && EmployeeProfileManagementViewModel.AppliedDateID.HasValue && !string.IsNullOrEmpty(EmployeeProfileManagementViewModel.Coefficient))
                {
                    EmployeeSalary objES = new EmployeeSalary();
                    objES.EmployeeID = EmployeeProfileManagementViewModel.EmployeeID;
                    objES.EmployeeScaleID = EmployeeProfileManagementViewModel.EmployeeScaleID;
                    objES.SalaryLevelID = EmployeeProfileManagementViewModel.SubLevel.Value;
                    objES.SchoolID = _globalInfo.SchoolID;
                    objES.SupervisingDeptID = _globalInfo.SupervisingDeptID;
                    objES.AppliedDate = EmployeeProfileManagementViewModel.AppliedDateID;
                    objES.SalaryAmount = EmployeeProfileManagementViewModel.SalaryAmount;
                    if (!string.IsNullOrEmpty(EmployeeProfileManagementViewModel.Coefficient))
	                {
                        decimal val = 0;
                        if (decimal.TryParse(EmployeeProfileManagementViewModel.Coefficient.Replace(".",","), out val))
                        {
                            if (val > 100)
                            {
                                return Json(new JsonMessage(Res.Get("Hệ số lương phải nẳm trong khoảng [0 - 99.99]"), "error"));
                            }
                            else
                            {
                                objES.Coefficient = val;
                            }
                        }
                        else
                        {
                            return Json(new JsonMessage(Res.Get("Hệ số lương phải nẳm trong khoảng [0 - 99.99]"), "error"));
                        }
	                }
                    IDictionary<string, object> dicInsertSalary = new Dictionary<string, object>()
                    {
                        {"SchoolID",_globalInfo.SchoolID},
                        {"AppliedDate", EmployeeProfileManagementViewModel.AppliedDateID},
                        {"EmployeeID",EmployeeProfileManagementViewModel.EmployeeID}
                    };

                    EmployeeSalaryBusiness.InsertOrUpdateSalary(dicInsertSalary, objES);
                }
                else
                {
                    throw new SMAS.Business.Common.BusinessException("Để nhập thông tin lương cần phải nhập đủ các trường thông tin: Ngày hưởng, Ngạch/hạng, Bậc lương, Hệ số");
                }
                
            }

            employee.VocationalAllowance = EmployeeProfileManagementViewModel.VOCATIONAL_ALLOWANCE;
            employee.SeniorityAllowance = EmployeeProfileManagementViewModel.SENIORITY_ALLOWANCE;
            employee.PreferentialAllowance = EmployeeProfileManagementViewModel.PREFERENTIAL_ALLOWANCE;
            //Danh hieu phong tang
            employee.HigiestTitle = EmployeeProfileManagementViewModel.AppellationAward;
            employee.EmploymentStatus = SMAS.Business.Common.GlobalConstants.EMPLOYMENT_STATUS_WORKING;
            this.EmployeeBusiness.UpdateTeacher(employee);

            if (EmployeeProfileManagementViewModel.ConcurrentWorkId.HasValue)
            {
                ConcurrentWorkAssignment lstcc = ConcurrentWorkAssignmentBusiness.All.Where(o =>  o.TeacherID == employee.EmployeeID && o.AcademicYearID == _globalInfo.AcademicYearID).OrderByDescending(o=>o.ConcurrentWorkAssignmentID).FirstOrDefault();

                if (lstcc != null)
                {
                    ConcurrentWorkAssignmentBusiness.Delete(lstcc.ConcurrentWorkAssignmentID);
                }
                ConcurrentWorkAssignment newObj = new ConcurrentWorkAssignment();
                newObj.AcademicYearID = _globalInfo.AcademicYearID.Value;
                newObj.ConcurrentWorkTypeID = EmployeeProfileManagementViewModel.ConcurrentWorkId.Value;
                newObj.FacultyID = EmployeeProfileManagementViewModel.SchoolFacultyID;
                newObj.SchoolID = _globalInfo.SchoolID.Value;
                newObj.TeacherID = employee.EmployeeID;
                newObj.IsActive = true;
                ConcurrentWorkAssignmentBusiness.Insert(newObj);
            }
            else
            {
                List<ConcurrentWorkAssignment> lstcc = ConcurrentWorkAssignmentBusiness.All.Where(o => o.TeacherID == employee.EmployeeID && o.AcademicYearID == _globalInfo.AcademicYearID).ToList();

                ConcurrentWorkAssignmentBusiness.DeleteAll(lstcc);
            }

            this.EmployeeBusiness.Save();

            // Tạo giá trị ghi log
            //string newObject = JsonConvert.SerializeObject(SMAS.Business.Common.Utils.BindTo<EmployeeBO>(employee), new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
            string newObject = "{EmployeeID:" + employee.EmployeeID + ", EmployeeCode:\"" + employee.EmployeeCode + "\", FullName:\"" + employee.FullName + "\", BirthDate:\"" + employee.BirthDate.ToString() + "\",EthnicID:" + employee.EthnicID + ",SchoolFacultyID:" + employee.SchoolFacultyID + ",IntoSchoolDate:\"" + employee.IntoSchoolDate.ToString() + "\",ContractTypeID:" + employee.ContractTypeID + ",WorkGroupTypeID:" + employee.WorkGroupTypeID + ",WorkTypeID:" + employee.WorkTypeID + ", AppliedLevel:" + employee.AppliedLevel + "}";
            SetViewDataActionAudit(oldObject, newObject, employee.EmployeeID.ToString(), "Update employee_id:" + employee.EmployeeID.ToString(), employee.EmployeeID.ToString(), "Hồ sơ cán bộ/nhân viên", GlobalConstants.ACTION_UPDATE, "Cập nhật hồ sơ " + employee.FullName + " mã " + employee.EmployeeCode);

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        public ActionResult GetEditEmployee(int EmployeeId,bool? isEmployee = false)
        {

            CheckPermissionForAction(EmployeeId, "Employee");
            GlobalInfo global = new GlobalInfo();
            Employee employee = this.EmployeeBusiness.Find(EmployeeId);
            if (employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementConstant.EMPLOYEE_WORKING] = "false";
            }
            EmployeeProfileManagementViewModel EmployeeProfileManagementViewModel = new EmployeeProfileManagementViewModel();

            SetViewData(isEmployee);
            #region load du lieu cho tab thu nhat
            Utils.Utils.BindTo(employee, EmployeeProfileManagementViewModel);

            EmployeeProfileManagementViewModel.AutoGenCode = CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER);

            if (employee.FatherBirthDate.HasValue) EmployeeProfileManagementViewModel.iFatherBirthDate = employee.FatherBirthDate.Value.Year;
            if (employee.MotherBirthDate.HasValue) EmployeeProfileManagementViewModel.iMotherBirthDate = employee.MotherBirthDate.Value.Year;
            if (employee.SpouseBirthDate.HasValue) EmployeeProfileManagementViewModel.iSpouseBirthDate = employee.SpouseBirthDate.Value.Year;

            //lay danh gia xep loai cua giao vien
            List<EmployeeEvaluationBO> lstEE = new List<EmployeeEvaluationBO>();
            EmployeeEvaluationBO objEE = null;
            IDictionary<string, object> dicEE = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"EmployeeID",employee.EmployeeID}
            };
            lstEE = EmployeeEvaluationBusiness.GetListEmployeeEvaluation(dicEE);
            if (lstEE.Count > 0)
            {
                objEE = lstEE.Where(p => p.EvaluationFieldID == GlobalConstants.EMPLOYEE_EVALUATION_FIELD1).FirstOrDefault();
                EmployeeProfileManagementViewModel.EvaluationField1 = objEE != null ? (objEE.FieldType == 1 ? objEE.EvaluationLevelName : objEE.EvaluationText) : "";
                objEE = lstEE.Where(p => p.EvaluationFieldID == GlobalConstants.EMPLOYEE_EVALUATION_FIELD2).FirstOrDefault();
                EmployeeProfileManagementViewModel.EvaluationField2 = objEE != null ? (objEE.FieldType == 1 ? objEE.EvaluationLevelName : objEE.EvaluationText) : "";
                objEE = lstEE.Where(p => p.EvaluationFieldID == GlobalConstants.EMPLOYEE_EVALUATION_FIELD3).FirstOrDefault();
                EmployeeProfileManagementViewModel.EvaluationField3 = objEE != null ? (objEE.FieldType == 1 ? objEE.EvaluationLevelName : objEE.EvaluationText) : "";
                objEE = lstEE.Where(p => p.EvaluationFieldID == GlobalConstants.EMPLOYEE_EVALUATION_FIELD4).FirstOrDefault();
                EmployeeProfileManagementViewModel.EvaluationField4 = objEE != null ? (objEE.FieldType == 1 ? objEE.EvaluationLevelName : objEE.EvaluationText) : "";
            }

            EmployeeProfileManagementViewModel.VOCATIONAL_ALLOWANCE = employee.VocationalAllowance;
            EmployeeProfileManagementViewModel.SENIORITY_ALLOWANCE = employee.SeniorityAllowance;
            EmployeeProfileManagementViewModel.PREFERENTIAL_ALLOWANCE = employee.PreferentialAllowance;
            EmployeeProfileManagementViewModel.AppellationAward = employee.HigiestTitle;
            //EmployeeProfileManagementViewModel.WorkGroupTypeID = employee.WorkTypeID.HasValue ? employee.WorkType.WorkGroupTypeID : new Nullable<int>();

            //vi employee chua co workgrouptypeid nen phai them vao dua vao worktypeid
            if (EmployeeProfileManagementViewModel.WorkGroupTypeID.HasValue)
            {
                //them vao viewdata worktype
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["WorkGroupTypeID"] = EmployeeProfileManagementViewModel.WorkGroupTypeID.Value;
                IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(dic);
                ViewData[EmployeeProfileManagementConstant.LS_WORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution");

                IQueryable<SubjectCat> lsSubjectCat = SubjectCatBusiness.Search(new Dictionary<string, object>() {
                { "AppliedLevel", employee.AppliedLevel },
                { "IsActive", true } }).OrderBy(o => o.DisplayName);
                ViewData[EmployeeProfileManagementConstant.LS_PRIMARILYASSIGNEDSUBJECT] = new SelectList(lsSubjectCat, "SubjectCatID", "DisplayName");
            }
            ViewData[EmployeeProfileManagementConstant.CHOSENEMPLOYEE] = EmployeeProfileManagementViewModel;

            //Lay thong tin luong
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            IEnumerable<EmployeeGeneralInfoSalaryViewModel> lst = (from o in new EmployeeInfo().SearchEmployeeSalary(EmployeeSalaryBusiness,EmployeeScaleBusiness
                                                                       ,SalaryLevelBusiness, new Dictionary<string, object> { { "EmployeeID", employee.EmployeeID } })
                                                                   select new EmployeeGeneralInfoSalaryViewModel
                                                                   {
                                                                       EmployeeSalaryID = o.EmployeeSalaryID,
                                                                       EmployeeID = o.EmployeeID,
                                                                       EmployeeName = o.EmployeeName,
                                                                       EmployeeScaleID = o.EmployeeScaleID,
                                                                       EmployeeScaleName = o.EmployeeScaleName,
                                                                       EmployeeScaleCode = o.EmployeeScaleCode,
                                                                       SalaryLevelID = o.SalaryLevelID,
                                                                       SalaryLevelName = o.SalaryLevelName,
                                                                       Coefficient = o.Coefficient.HasValue ? o.Coefficient.Value.ToString("0.00").Replace(",", ".") : string.Empty,
                                                                       SchoolID = o.SchoolID,
                                                                       SupervisingDeptID = o.SupervisingDeptID,
                                                                       AppliedDate = o.AppliedDate,
                                                                       SalaryResolution = o.SalaryResolution,
                                                                       SalaryAmount = o.SalaryAmount,
                                                                       Description = o.Description
                                                                   }).OrderByDescending(p => p.AppliedDate);
            ViewData[EmployeeSalaryConstants.LIST_EMPLOYEESALARY] = lst;

            EmployeeGeneralInfoSalaryViewModel objEGIS = lst.FirstOrDefault();

            if (objEGIS != null)
            {
                EmployeeProfileManagementViewModel.AppliedDateID = objEGIS.AppliedDate;
                EmployeeProfileManagementViewModel.SalaryAmount = objEGIS.SalaryAmount;
                EmployeeProfileManagementViewModel.Coefficient = objEGIS.Coefficient;
            }
            

            SearchInfo.Clear();
            IQueryable<EmployeeScale> IQSalaryScale = EmployeeScaleBusiness.Search(SearchInfo);
            SelectList listSalaryScale = new SelectList((IEnumerable<EmployeeScale>)IQSalaryScale.ToList(), "EmployeeScaleID", "Resolution", objEGIS != null ? objEGIS.EmployeeScaleID : 0);
            ViewData[EmployeeSalaryConstants.LIST_SALARYSCALE] = listSalaryScale;

            // SalaryLevel
            List<ComboObject> listComboObj = new List<ComboObject>();
            List<SalaryLevel> lstSalaryLevel = SalaryLevelBusiness.All.Where(p => p.IsActive).ToList();
            SelectList lstSelectList = new SelectList((IEnumerable<SalaryLevel>)lstSalaryLevel, "SalaryLevelID", "Resolution", objEGIS != null ? objEGIS.SalaryLevelID : 0);
            ViewData[EmployeeSalaryConstants.LIST_SALARYLEVEL] = lstSelectList;

            //Lay danh sach quan huyen, xa phuong
            if (employee.ProvinceId.HasValue)
            {
                List<District> lstDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == employee.ProvinceId && o.IsActive == true).ToList();
                ViewData[EmployeeProfileManagementConstant.LIST_DISTRICT] = new SelectList(lstDistrict, "DistrictID", "DistrictName");
            }

            if (employee.DistrictId.HasValue)
            {
                List<Commune> lstCom = CommuneBusiness.All.Where(o => o.DistrictID == employee.DistrictId && o.IsActive == true).ToList();
                ViewData[EmployeeProfileManagementConstant.LIST_COMMUNE] = new SelectList(lstCom, "CommuneID", "CommuneName");
            }

            //Danh sach qua trinh dao tao
            IEnumerable<EmployeeQualificationListViewModel> IQEmployeeQualification = new EmployeeInfo().SearchEmployeeQualification(EmployeeQualificationBusiness, new Dictionary<string, object> { { "EmployeeID", employee.EmployeeID } }); ;
            ViewData[EmployeeQualificationConstants.LIST_EMPLOYEEQUALIFICATION] = IQEmployeeQualification.OrderByDescending(o => o.FromDate).ToList();

            //Lay ra viec kiem nhiem moi nhat
            ConcurrentWorkAssignment cc = ConcurrentWorkAssignmentBusiness.All.Where(o => o.AcademicYearID == _globalInfo.AcademicYearID.Value
                && o.IsActive && o.TeacherID == employee.EmployeeID).OrderByDescending(o => o.ConcurrentWorkAssignmentID).FirstOrDefault();
            EmployeeProfileManagementViewModel.ConcurrentWorkId = cc != null ? (int?)cc.ConcurrentWorkTypeID : null;

            ViewData[EmployeeProfileManagementConstant.IS_EDIT] = true;
            ViewData[EmployeeProfileManagementConstant.IS_EMPLOYEE] = isEmployee;
            #endregion load du lieu cho tab thu nhat

            //#region load du lieu cho tab thu 2
            ////Nếu UserInfo.IsSchoolRole = TRUE: kiểm tra nếu Employee(EmployeeID).SchoolID <> UserInfo.SchoolID =>
            ////Chuyển sang trang thông báo lỗi: “Bạn không có quyền truy cập vào dữ liệu này -Common_Label_HasHeadTeacherPermissionError
            //bool isPermiss = true;
            //if (global.IsSchoolRole)
            //{
            //    if (employee.SchoolID.HasValue)
            //        isPermiss &= employee.SchoolID.Value == global.SchoolID.Value;
            //}
            //else if (global.IsSuperVisingDeptRole)
            //{
            //    if (employee.SupervisingDeptID.HasValue)
            //        isPermiss &= employee.SupervisingDeptID.Value == global.SupervisingDeptID.Value;
            //}
            //else
            //{
            //    isPermiss &= false;
            //}


            //ViewData[EmployeeWorkingHistoryConstants.IS_PERMISS] = isPermiss;
            //ViewData[EmployeeWorkingHistoryConstants.HAS_PERMISSION] = GetMenupermission("EmployeeProfileManagement", global.UserAccountID, global.IsAdmin);

            //IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //IEnumerable<EmployeeWorkingHistoryViewModel> lst = employee.EmployeeWorkingHistories
            //                                                            .Select(o => new EmployeeWorkingHistoryViewModel
            //                                                            {
            //                                                                EmployeeWorkingHistoryID = o.EmployeeWorkingHistoryID,
            //                                                                EmployeeID = o.EmployeeID,
            //                                                                EmployeeName = o.Employee.FullName,
            //                                                                SupervisingDeptID = o.SupervisingDeptID,
            //                                                                SchoolID = o.SchoolID,
            //                                                                Organization = o.Organization,
            //                                                                Department = o.Department,
            //                                                                Position = o.Position,
            //                                                                Resolution = o.Resolution,
            //                                                                FromDate = o.FromDate,
            //                                                                ToDate = o.ToDate
            //                                                            })
            //                                                            .OrderByDescending(o => o.FromDate)
            //                                                            .ThenBy(u => u.EmployeeName)
            //                                                            .ToList();
            //ViewData[EmployeeWorkingHistoryConstants.LIST_EMPLOYEEWORKINGHISTORY] = lst;
            //ViewData[EmployeeProfileManagementConstant.CHOSENEMPLOYEE] = EmployeeProfileManagementViewModel;
            //#endregion load du lieu cho tab thu 2

            //#region Load du lieu cho tab 3 - Thong tin dao tao

            //this.LoadEmployeeQualification(employee);

            //#endregion Load du lieu cho tab 3 - Thong tin dao tao

            //#region Load du lieu tab 4 - Thong tin luong

            //this.LoadEmployeeSalary(employee);

            //#endregion Load du lieu tab 4 - Thong tin luong
            ViewData["checkReadonly"] = false;
            return View("_Edit");
        }
        #endregion edit,update

        #region Load du lieu cho tab thong tin dao tao cua can bo
        /// <summary>
        /// Load du lieu
        /// </summary>
        /// <param name="EmployeeID"></param>
        private void LoadEmployeeQualification(Employee employee)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            //Get view data here

            IEnumerable<EmployeeQualificationListViewModel> IQEmployeeQualification = new EmployeeInfo().SearchEmployeeQualification(EmployeeQualificationBusiness, new Dictionary<string, object> { { "EmployeeID", employee.EmployeeID } }); ;
            ViewData[EmployeeQualificationConstants.LIST_EMPLOYEEQUALIFICATION] = IQEmployeeQualification.OrderByDescending(o => o.FromDate).ToList();

            // Lay du lieu combobox

            IQueryable<TrainingLevel> IQGraduationLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } });
            SelectList listIQGraduationLevel = new SelectList((IEnumerable<TrainingLevel>)IQGraduationLevel.ToList(), "TrainingLevelID", "Resolution");
            ViewData[EmployeeQualificationConstants.LIST_GRADUATIONLEVEL] = listIQGraduationLevel;

            // QualificationType
            IQueryable<QualificationType> IQQualificationType = QualificationTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });
            SelectList listQualificationType = new SelectList(IQQualificationType.ToList(), "QualificationTypeID", "Resolution");
            ViewData[EmployeeQualificationConstants.LIST_QUALIFICATIONTYPE] = listQualificationType;

            // QualificationGrade
            IQueryable<QualificationGrade> IQQualificationGrade = QualificationGradeBusiness.All.Where(o => o.IsActive == true);
            SelectList listQualificationGrade = new SelectList((IEnumerable<QualificationGrade>)IQQualificationGrade.ToList(), "QualificationGradeID", "Resolution");
            ViewData[EmployeeQualificationConstants.LIST_QUALIFICATIONGRADE] = listQualificationGrade;

            // SpecialityCat
            IQueryable<SpecialityCat> IQSpecialityCat = SpecialityCatBusiness.Search(new Dictionary<string, object> { { "IsActive", true } });
            SelectList listSpecialityCat = new SelectList((IEnumerable<SpecialityCat>)IQSpecialityCat.ToList(), "SpecialityCatID", "Resolution");
            ViewData[EmployeeQualificationConstants.LIST_SPECIALITYCAT] = listSpecialityCat;

            // Ket qua
            List<ComboObject> listCobObj = new List<ComboObject>();
            listCobObj.Add(new ComboObject(EmployeeQualificationConstants.FIRST_TRAINING_VAL, Res.Get("EmployeeQualification_Label_FirstTraining")));
            listCobObj.Add(new ComboObject(EmployeeQualificationConstants.RE_TRAINING_VAL, Res.Get("EmployeeQualification_Label_ReTraining")));
            SelectList listResult = new SelectList(listCobObj, "key", "value");
            ViewData[EmployeeQualificationConstants.LIST_ISREQUALIFIED] = listResult;
        }
        #endregion Load du lieu cho tab thong tin dao tao cua can bo

        #region Load du lieu cho tab thong tin luong cua can bo
        private void LoadEmployeeSalary(Employee employee)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo globalInfo = new GlobalInfo();

            IEnumerable<EmployeeSalaryViewModel> lst = new EmployeeInfo().SearchEmployeeSalary(EmployeeSalaryBusiness,EmployeeScaleBusiness,SalaryLevelBusiness, new Dictionary<string, object> { { "EmployeeID", employee.EmployeeID } });
            ViewData[EmployeeSalaryConstants.LIST_EMPLOYEESALARY] = lst;

            // Lay du lieu combobox
            SearchInfo.Clear();
            IQueryable<EmployeeScale> IQSalaryScale = EmployeeScaleBusiness.Search(SearchInfo);
            SelectList listSalaryScale = new SelectList((IEnumerable<EmployeeScale>)IQSalaryScale.ToList(), "EmployeeScaleID", "Resolution");
            ViewData[EmployeeSalaryConstants.LIST_SALARYSCALE] = listSalaryScale;

            // SalaryLevel
            List<ComboObject> listComboObj = new List<ComboObject>();
            List<SalaryLevel> lstSalaryLevel = SalaryLevelBusiness.All.Where(p => p.IsActive).ToList();
            SelectList lstSelectList = new SelectList((IEnumerable<SalaryLevel>)lstSalaryLevel, "SalaryLevelID", "Resolution");
            ViewData[EmployeeSalaryConstants.LIST_SALARYLEVEL] = lstSelectList;
        }
        #endregion Load du lieu cho tab thong tin luong cua can bo

        #region delete

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo global = new GlobalInfo();
            var Permission2 = GetMenupermission("EmployeeProfileManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            if (Permission2 < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.EmployeeBusiness.DeleteTeacher(id, _globalInfo.SchoolID.Value);
            this.EmployeeBusiness.Save();
            // Tạo giá trị lưu log action_audit
            Employee employee = EmployeeBusiness.Find(id);
            SetViewDataActionAudit("{\"EmployeeID\":\"" + id + "\",\"EmployeeCode\":\"" + employee.EmployeeCode + "\"}", String.Empty, id.ToString(), "Delete employee_id:" + id, id.ToString(), "Hồ sơ cán bộ/nhân viên", GlobalConstants.ACTION_DELETE, "Xóa " + employee.FullName + " mã " + employee.EmployeeCode);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        [ValidateAntiForgeryToken]
        [ActionAudit]
        public ActionResult DeleteCheckEmployee(int[] checkedDelete)
        {
            GlobalInfo global = new GlobalInfo();
            var Permission2 = GetMenupermission("EmployeeProfileManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            if (Permission2 < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            // Tạo giá trị ghi log
            List<int> listID = checkedDelete.ToList();
            List<Employee> LstEmployee = EmployeeBusiness.All.Where(o => listID.Contains(o.EmployeeID)).ToList();
            StringBuilder oldObject = new StringBuilder();
            StringBuilder objectID = new StringBuilder();
            StringBuilder descriptionObject = new StringBuilder();
            StringBuilder paramObject = new StringBuilder();
            StringBuilder userFunctions = new StringBuilder();
            StringBuilder userActions = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();
            Employee employee = null;
            for (int i = listID.Count - 1; i >= 0; i--)
            {
                employee = LstEmployee.FirstOrDefault(e => e.EmployeeID == listID[i]);
                // AnhVD9 - Note 20140310 - Đang thực hiện xóa theo từng phần tử - CHẬM
                EmployeeBusiness.DeleteTeacher(listID[i], _globalInfo.SchoolID.Value);
                if (employee != null)
                {
                    oldObject.Append("{\"EmployeeID\":\"" + employee.EmployeeID.ToString() + "\",\"EmployeeCode\":\"" + employee.EmployeeCode + "\"}");
                    objectID.Append(employee.EmployeeID.ToString());
                    descriptionObject.Append("Delete employee_id:" + employee.EmployeeID.ToString());
                    paramObject.Append(employee.EmployeeID.ToString());
                    userFunctions.Append("Hồ sơ cán bộ/nhân viên");
                    userActions.Append(GlobalConstants.ACTION_DELETE);
                    userDescriptions.Append("Xóa " + employee.FullName + " mã " + employee.EmployeeCode);
                    if (i > 0)
                    {
                        oldObject.Append(GlobalConstants.WILD_LOG);
                        descriptionObject.Append(GlobalConstants.WILD_LOG);
                        paramObject.Append(GlobalConstants.WILD_LOG);
                        objectID.Append(GlobalConstants.WILD_LOG);
                        userFunctions.Append(GlobalConstants.WILD_LOG);
                        userActions.Append(GlobalConstants.WILD_LOG);
                        userDescriptions.Append(GlobalConstants.WILD_LOG);
                    }
                }
            }
            EmployeeBusiness.Save();

            // Tạo giá trị ghi log
            SetViewDataActionAudit(oldObject.ToString(), String.Empty, objectID.ToString(), descriptionObject.ToString(), paramObject.ToString(), userFunctions.ToString(), userActions.ToString(), userDescriptions.ToString());

            SetViewData();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = _globalInfo.SchoolID;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            IEnumerable<EmployeeProfileManagementViewModel> lst = this._Search(SearchInfo);
            ViewData[EmployeeProfileManagementConstant.LIST_EMPLOYEE] = lst;
            ViewData[EmployeeProfileManagementConstant.EMPLOYEE_TOTAL] = lst.Count();
            ViewData[EmployeeProfileManagementConstant.PAGE_NUMBER] = 1;
            return View("_List");
        }

        #endregion delete

        #region excel

        public FileResult ExportExcel(SearchViewModel frm)
        {
            IVTWorkbook oBook;
            exportToImportTemplate(out oBook, frm);
            Stream excel = oBook.ToStream();

            // Fix chrome file type
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            result.FileDownloadName = ReportUtils.StripVNSign(string.Format(TEMPLATE_EMPLOYEE_NAME, "_" + sp.SchoolName));

            // Tạo dữ liệu ghi log
            String param = "{\"SchoolFaculty\":\"" + frm.SchoolFaculty + "\",\"WorkType\":\"" + frm.WorkType + "\", \"Sex\":\"" + frm.Sex + "\",\"EmployeeCode\":\"" + frm.EmployeeCode + "\",\"Fullname\":\"" + frm.Fullname + "\",\"EmploymentStatus\":\"" + frm.EmploymentStatus + "\",\"BirthDate\":\"" + frm.BirthDate + "\",\"TrainingLevel\":\"" + frm.TrainingLevel + "\",\"ContractType\":\"" + frm.ContractType + "\"}";
            SetViewDataActionAudit(String.Empty, String.Empty, String.Empty, "Export excel employee", param, "Hồ sơ cán bộ/nhân viên", GlobalConstants.ACTION_EXPORT, "Xuất danh sách cán bộ/nhân viên");

            return result;
        }

        public PartialViewResult OpenDialogExcel(int? id)
        {

            int schoolId = _globalInfo.SchoolID ?? 0;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ReportType", SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_EMPLOYEE_PROFILE);
            dic.Add("SchoolID", schoolId);
            dic.Add("TemplateID", id);
            List<ProfileTemplate> lstProfileTemplate = ProfileTemplateBusiness.Search(dic).ToList();
            lstProfileTemplate.Insert(0, new ProfileTemplate { ProfileTemplateName = "[Thêm mới]", ProfileTemplateId = 0 });
            ViewData[EmployeeProfileManagementConstant.LIST_PROFILE_TEMPLATE] = new SelectList(lstProfileTemplate, "ProfileTemplateId", "ProfileTemplateName", id);
            return PartialView("_ListExcel");

        }

        #endregion

        #region Load combobox
        public JsonResult AjaxLoadDistrict(int? provinceId)
        {
            List<District> lstDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == provinceId && o.IsActive == true).ToList();

            return Json(new SelectList(lstDistrict, "DistrictID", "DistrictName"));
        }

        public JsonResult AjaxLoadCommune(int? provinceId, int? districtId)
        {
            List<Commune> lstCommune = CommuneBusiness.All.Where(o => o.DistrictID == districtId && o.IsActive == true).ToList();

            return Json(new SelectList(lstCommune, "CommuneID", "CommuneName"));
        }
        #endregion
        private void export(out IVTWorkbook oBook, SearchViewModel frm)
        {

            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //add search info
            SearchInfo["FacultyID"] = frm.SchoolFaculty;
            SearchInfo["WorkTypeID"] = frm.WorkType;
            SearchInfo["EmploymentStatus"] = frm.EmploymentStatus;
            SearchInfo["EmployeeCode"] = frm.EmployeeCode;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["BirthDate"] = frm.BirthDate;
            SearchInfo["Genre"] = frm.Sex;
            SearchInfo["TrainingLevelID"] = frm.TrainingLevel;
            SearchInfo["ContractTypeID"] = frm.ContractType;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            List<EmployeeProfileManagementViewModel> lst = this._Search(SearchInfo).ToList();

            string templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "GV", TEMPLATE_EMPLOYEE);
            oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            #region new
            int firstRow = 8;
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "G" + firstRow);

            //tuy theo tung lop ma fill excel theo cac tab

            IVTRange bottomRow = firstSheet.GetRange("A9", "G9");
            IVTRange reportRow = firstSheet.GetRange("F12", "G12");
            Dictionary<string, object> KingDic = new Dictionary<string, object>();
            List<object> QueenDic = new List<object>();


            int currentRow = 9;
            for (int i = 0; i < lst.Count(); i++)
            {
                sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                currentRow++;
                EmployeeProfileManagementViewModel thisModel = lst[i];
                Dictionary<string, object> princeDic = new Dictionary<string, object>();
                princeDic["STT"] = i + 1;
                princeDic["FullName"] = thisModel.FullName;
                princeDic["BirthDate"] = string.Format("{0:dd/MM/yyyy}", thisModel.BirthDate);
                princeDic["Genre"] = thisModel.Sex;
                princeDic["SchoolFaculty"] = thisModel.SchoolFacultyName;
                princeDic["PermanentResidentalAddress"] = thisModel.PermanentResidentalAddress;
                QueenDic.Add(princeDic);
            }

            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            int SupervisingDeptID = SchoolProfileBusiness.Find(_globalInfo.SchoolID).SupervisingDeptID.Value;
            SupervisingDept sd = SupervisingDeptBusiness.Find(SupervisingDeptID);
            KingDic.Add("Rows", QueenDic);
            KingDic.Add("SchoolName", sp.SchoolName);
            KingDic.Add("SupervisingDeptName", sd.SupervisingDeptName);

            Province p = ProvinceBusiness.Find(_globalInfo.ProvinceID);
            string ProvinceAndDateTime = p.ProvinceName + "," + string.Format("Ngày {0} tháng {1} năm {2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            KingDic.Add("ProvinceDateAndTime", ProvinceAndDateTime);
            sheet.FillVariableValue(KingDic);
            sheet.Name = "DSCB";
            sheet.CopyPasteSameSize(reportRow, currentRow + 2, 6);
            sheet.SetCellValue(currentRow + 2, 6, "Người lập báo cáo");

            firstSheet.Delete();
            #endregion
        }

        private void exportToImportTemplate(out IVTWorkbook oBook, SearchViewModel frm)
        {

            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //add search info
            SearchInfo["FacultyID"] = frm.SchoolFaculty;
            SearchInfo["WorkTypeID"] = frm.WorkType;
            SearchInfo["EmploymentStatus"] = frm.EmploymentStatus;
            SearchInfo["EmployeeCode"] = frm.EmployeeCode;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["BirthDate"] = frm.BirthDate;
            SearchInfo["Genre"] = frm.Sex;
            SearchInfo["TrainingLevelID"] = frm.TrainingLevel;
            SearchInfo["ContractTypeID"] = frm.ContractType;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            List<EmployeeProfileManagementViewModel> lst = this._Search(SearchInfo).ToList();
            SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID.Value);

            #region Chuan bi file excel
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "Import" + "/" + "Mau_Import_Canbo.xls";
            oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet 
            IVTWorksheet employeeSheet = oBook.GetSheet(1);
            IVTWorksheet refSheet = oBook.GetSheet(2);
            IVTWorksheet provinceSheet = oBook.GetSheet(3);
            IVTWorksheet districtSheet = oBook.GetSheet(4);
            IVTWorksheet communeSheet = oBook.GetSheet(5);

            //Lay du lieu
            List<SchoolFaculty> LstSchoolFaculty = SchoolFacultyBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.FacultyName).ToList();
            List<QualificationLevel> LstQualificationLevel = QualificationLevelBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<WorkType> LstWorkType = WorkTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<ContractType> LstContractType = ContractTypeBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<Ethnic> LstEthnicTemp = EthnicBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.EthnicName).ToList();
            List<Ethnic> LstEthnic = new List<Ethnic>();
            Ethnic ethnic = LstEthnicTemp.Where(u => u.EthnicName.ToLower().Equals("kinh")).FirstOrDefault();
            if (ethnic != null)
            {
                LstEthnic.Add(ethnic);
                LstEthnic.AddRange(LstEthnicTemp.Where(u => u.EthnicID != ethnic.EthnicID));
            }
            List<Religion> LstReligionTemp = ReligionBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<Religion> LstReligion = new List<Religion>();
            Religion religion = LstReligionTemp.Where(u => u.Resolution.ToLower().Equals("không")).FirstOrDefault();
            if (religion != null)
            {
                LstReligion.Add(religion);
                LstReligion.AddRange(LstReligionTemp.Where(u => u.ReligionID != religion.ReligionID));
            }
            List<TrainingLevel> LstTrainingLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<SpecialityCat> LstSpecialityCat = SpecialityCatBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<ForeignLanguageGrade> LstForeignLanguageGrade = ForeignLanguageGradeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<ITQualificationLevel> LstITQualificationLevel = ITQualificationLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<PoliticalGrade> LstPoliticalGrade = PoliticalGradeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<StateManagementGrade> LstStateManagementGrade = StateManagementGradeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<EducationalManagementGrade> LstEducationalManagementGrade = EducationalManagementGradeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();
            List<FamilyType> LstFamilyType = FamilyTypeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(u => u.Resolution).ToList();




            //Cấp học - Cột B
            List<string> ListGrade = SMAS.Business.Common.Utils.GetListGrade(school.EducationGrade);
            for (int i = 0; i < ListGrade.Count; i++)
            {
                refSheet.SetCellValue("B" + (i + 2), ListGrade[i]);
            }
            //Tổ bộ môn - Cột C
            for (int i = 0; i < LstSchoolFaculty.Count(); i++)
            {
                refSheet.SetCellValue("C" + (i + 2), LstSchoolFaculty[i].FacultyName);
            }
            //Trình độ văn hóa - Cột D
            for (int i = 0; i < LstQualificationLevel.Count(); i++)
            {
                refSheet.SetCellValue("D" + (i + 2), LstQualificationLevel[i].Resolution);
            }
            //Loại công việc - Cột E
            for (int i = 0; i < LstWorkType.Count(); i++)
            {
                refSheet.SetCellValue("E" + (i + 2), LstWorkType[i].Resolution);
            }
            //Loại hợp đồng - Cột F
            for (int i = 0; i < LstContractType.Count(); i++)
            {
                refSheet.SetCellValue("F" + (i + 2), LstContractType[i].Resolution);
            }
            //Dân tộc - Cột G
            for (int i = 0; i < LstEthnic.Count(); i++)
            {
                refSheet.SetCellValue("G" + (i + 2), LstEthnic[i].EthnicName);
            }
            //Tôn giáo - Cột H
            for (int i = 0; i < LstReligion.Count(); i++)
            {
                refSheet.SetCellValue("H" + (i + 2), LstReligion[i].Resolution);
            }
            //Trinh do dao tao - Cột J
            for (int i = 0; i < LstTrainingLevel.Count(); i++)
                refSheet.SetCellValue("J" + (i + 2), LstTrainingLevel[i].Resolution);
            //Chuyen nganh dao tao - Cột K
            for (int i = 0; i < LstSpecialityCat.Count(); i++)
                refSheet.SetCellValue("K" + (i + 2), LstSpecialityCat[i].Resolution);
            //Trinh do ngoai ngu - Cột L
            for (int i = 0; i < LstForeignLanguageGrade.Count(); i++)
                refSheet.SetCellValue("L" + (i + 2), LstForeignLanguageGrade[i].Resolution);
            //Trinh do tin hoc - Cột M
            for (int i = 0; i < LstITQualificationLevel.Count(); i++)
                refSheet.SetCellValue("M" + (i + 2), LstITQualificationLevel[i].Resolution);
            //Trinh do Lyluan chinh tri - Cột N
            for (int i = 0; i < LstPoliticalGrade.Count(); i++)
                refSheet.SetCellValue("N" + (i + 2), LstPoliticalGrade[i].Resolution);
            //Trinh do Quan ly nha nuoc - Cột O
            for (int i = 0; i < LstStateManagementGrade.Count(); i++)
                refSheet.SetCellValue("O" + (i + 2), LstStateManagementGrade[i].Resolution);
            //Trinh do Quan ly Giao duc - Cột P
            for (int i = 0; i < LstEducationalManagementGrade.Count(); i++)
                refSheet.SetCellValue("P" + (i + 2), LstEducationalManagementGrade[i].Resolution);
            //Thanh phan gia dinh - Cột Q
            for (int i = 0; i < LstFamilyType.Count(); i++)
                refSheet.SetCellValue("Q" + (i + 2), LstFamilyType[i].Resolution);

            #endregion

            #region Xuat du lieu ra excel
            // Xuat ten truong
            IVTRange SchoolNameRange = employeeSheet.GetRange("A2", "D2");
            SchoolNameRange.MergeLeft();
            SchoolNameRange.Value = school.SchoolName;
            // Xuat du lieu hoc sinh
            int firstRow = 5;
            IVTRange firstRange = employeeSheet.GetRange("A5", "AZ5");
            int currentRow = firstRow;
            int stt = 0;
            foreach (var currentEmployeeProfile in lst)
            {
                if (currentRow > firstRow)
                {
                    employeeSheet.CopyPasteSameRowHeigh(firstRange, currentRow, 1);
                }
                // STT
                ++stt;
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.STT_1, stt);

                // To bo mon
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.TO_BO_MON_2, currentEmployeeProfile.SchoolFacultyName);

                // Ma giao vien

                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.MA_CAN_BO_3, currentEmployeeProfile.EmployeeCode);
                // Ho va ten


                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.HO_VA_TEN_4, currentEmployeeProfile.FullName);
                // Gioi tinh

                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.GIOI_TINH_5, currentEmployeeProfile.Genre ? "" : "x");
                // Ngay sinh

                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NGAY_SINH_6, string.Format("{0:dd/MM/yyyy}", currentEmployeeProfile.BirthDate));
                // Loai cong viec

                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.LOAI_CONG_VIEC_7, currentEmployeeProfile.WorkTypeName);
                // Loai hop dong
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.HINH_THUC_HD_8, currentEmployeeProfile.ContractTypeName);

                // Dan toc
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.DAN_TOC_9, currentEmployeeProfile.EthnicName);

                // Ton giao
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.TON_GIAO_10, currentEmployeeProfile.ReligionName);

                // Cap day chinh
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.CAP_DAY_CHINH_11, currentEmployeeProfile.AppliedLevelName);

                // Ngay vao truong
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NGAY_VAO_TRUONG_12, string.Format("{0:dd/MM/yyyy}", currentEmployeeProfile.IntoSchoolDate));
                // So CMND
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.SO_CMND_13, currentEmployeeProfile.IdentityNumber);
                // Ngay cap
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NGAY_CAP_CMND_14, string.Format("{0:dd/MM/yyyy}", currentEmployeeProfile.IdentityIssuedDate));

                // Noi cap
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NOI_CAP_CMND_15, currentEmployeeProfile.IdentityIssuedPlace);
                // So dien thoai
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.SO_DIEN_THOAI_16, currentEmployeeProfile.Telephone);

                // SDT di dong
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.SO_DIEN_THOAI_DI_DONG_17, currentEmployeeProfile.Mobile);

                // Email
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.EMAIL_18, currentEmployeeProfile.Email);
                //So so bao hiem
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.SO_BHXH_19, currentEmployeeProfile.InsuranceNumber);
                // Que quan
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.QUE_QUAN_20, currentEmployeeProfile.HomeTown);
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.TINH_THANH_21, currentEmployeeProfile.ProvinceName);
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.QUAN_HUYEN_22, currentEmployeeProfile.DistrictName);
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.XA_PHUONG_23, currentEmployeeProfile.CommuneName);
                // Dia chi thuong tru
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.DIA_CHI_THUONG_TRU_24, currentEmployeeProfile.PermanentResidentalAddress);

                // Suc khoe
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.SUC_KHOE_25, currentEmployeeProfile.HealthStatus);

                // Doan vien
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.DOAN_VIEN_26, currentEmployeeProfile.IsYouthLeageMember ? "x" : "");

                // Dang vien
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.DANG_VIEN_27, currentEmployeeProfile.IsCommunistPartyMember ? "x" : "");

                // Cong Doan vien
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.CONG_DOAN_VIEN_28, currentEmployeeProfile.IsSyndicate ? "x" : "");

                // Thanh phan gia dinh
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.THANH_PHAN_GD_29, currentEmployeeProfile.FamilyTypeName);

                // Ho ten bo
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.HO_TEN_BO_30, currentEmployeeProfile.FatherFullName);

                // Nam sinh bo
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NAM_SINH_BO_31, string.Format("{0:yyyy}", currentEmployeeProfile.FatherBirthDate));

                // Nghe nghiep bo
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NGHE_NGHIEP_BO_32, currentEmployeeProfile.FatherJob);

                // Noi lam viec bo
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NOI_LAM_VIEC_BO_33, currentEmployeeProfile.FatherWorkingPlace);

                // Ho ten me
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.HO_TEN_ME_34, currentEmployeeProfile.MotherFullName);

                // Nam sinh me
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NAM_SINH_ME_35, string.Format("{0:yyyy}", currentEmployeeProfile.MotherBirthDate));

                // Nghe nghiep me
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NGHE_NGHIEP_ME_36, currentEmployeeProfile.MotherJob);

                // Noi lam viec me
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NOI_LAM_VIEC_ME_37, currentEmployeeProfile.MotherWorkingPlace);

                // Ho ten vo/chong
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.HO_VA_TEN_VC_38, currentEmployeeProfile.SpouseFullName);

                // Nam sinh vo/chong
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NAM_SINH_VC_39, string.Format("{0:yyyy}", currentEmployeeProfile.SpouseBirthDate));

                // Nghe nghiep vo/chong
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NGHE_NGHIEP_VC_40, currentEmployeeProfile.SpouseJob);

                // Noi lam viec vo/chong
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NOI_LAM_VIEC_VC_41, currentEmployeeProfile.SpouseWorkingPlace);


                // Trinh do dao tao
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.TRINH_DO_CMNV_CAONHAT_42, currentEmployeeProfile.TrainingLevelName);

                // Chuyen nganh dao tao
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.CHUYEN_NGANH_DAO_TAO_43, currentEmployeeProfile.SpecialityCatName);

                // Trinh do van hoa
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.TRINH_DO_VAN_HOA_44, currentEmployeeProfile.QualificationLevelName);

                // Trinh do ngoai ngu
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.TRINH_DO_NGOAI_NGU_45, currentEmployeeProfile.ForeignLanguageGradeName);

                // Trinh do tin hoc
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.TRINH_DO_TIN_HOC_46, currentEmployeeProfile.ITQualificationLevelName);
                // Trinh do ly luan chinh tri
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.TRINH_DO_LY_LUAN_CT_47, currentEmployeeProfile.PoliticalGradeName);

                // Trinh do quan ly nha nuoc
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.TRINH_DO_QLNN_48, currentEmployeeProfile.StateManagementGradeName);

                // Trinh do quan ly giao duc
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.TRINH_DO_QLGD_49, currentEmployeeProfile.EducationalManagementGradeName);

                // Chuyen trach doan doi
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.CHUYEN_TRACH_DD_50, currentEmployeeProfile.DedicatedForYoungLeague ? "x" : "");

                // Boi duong thuong xuyen
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.BOI_DUONG_TX_51, currentEmployeeProfile.RegularRefresher ? "x" : "");

                // Ngay vao nghe
                employeeSheet.SetCellValue(currentRow, ExportEmployeeConstants.NGAY_VAO_NGHE_52, string.Format("{0:dd/MM/yyyy}", currentEmployeeProfile.StartingDate));

                // Tao border
                employeeSheet.GetRange(currentRow, ExportEmployeeConstants.STT_1, currentRow, ExportEmployeeConstants.NGAY_VAO_NGHE_52).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                ++currentRow;
            }
            // Xoa cac hang sau do
            for (int i = 1; i < 1000; i++)
            {
                employeeSheet.DeleteRow(currentRow);
            }
            employeeSheet.HideColumn(ExportEmployeeConstants.MO_TA_LOI_53);
            #endregion

            #region Tinh/thanh, quan/huyen, xa phuong

            List<Province> lstProvince = ProvinceBusiness.All.Where(p => p.IsActive == true && p.ProvinceID != GlobalConstants.ProvinceID_NA).OrderBy(p => p.ProvinceName).ToList();
            List<District> lstDistrict = DistrictBusiness.All.Where(d => d.IsActive == true).ToList();
            List<Commune> lstCommune = CommuneBusiness.All.Where(s => s.IsActive == true).ToList();

            //Fill sheet Province;
            int provinceRow = 4;
            int districtRow = 5;
            int districtOder = 0;
            IVTRange rangeDistrict = districtSheet.GetRange("A4", "D4");

            //row xa phuong
            IVTRange rgCommuneProvince = communeSheet.GetRange("A4", "E4");
            IVTRange rgcommenu = communeSheet.GetRange("A5", "E5");
            int communeRow = 6;
            //int commnueProvinceRow = 4;
            int communeOrder = 1;
            for (int i = 0; i < lstProvince.Count; i++)
            {
                int provinceOrder = i + 1;
                Province province = lstProvince[i];
                provinceSheet.SetCellValue(provinceRow, 1, provinceOrder);
                provinceSheet.SetCellValue(provinceRow, 2, province.ProvinceCode);
                provinceSheet.SetCellValue(provinceRow, 3, province.ProvinceName);
                provinceRow++;

                //Dien du lieu quan huyen
                List<District> lstDistrictbyProvince = lstDistrict.Where(d => d.ProvinceID == province.ProvinceID).OrderBy(d => d.DistrictName).ToList();
                if (lstDistrictbyProvince == null)
                {
                    continue;
                }
                int firstDistrictRow = districtRow; //(i > 0) ? districtRow + 1 : districtRow;
                for (int k = 0; k < lstDistrictbyProvince.Count; k++)
                {
                    District district = lstDistrictbyProvince[k];
                    districtOder++;
                    if (k > 0)
                    {
                        districtSheet.CopyPasteSameSize(rangeDistrict, districtRow, 1);
                    }
                    districtSheet.SetCellValue(districtRow, 1, districtOder);
                    districtSheet.SetCellValue(districtRow, 2, province.ProvinceName);
                    districtSheet.SetCellValue(districtRow, 3, district.DistrictCode);
                    districtSheet.SetCellValue(districtRow, 4, district.DistrictName);
                    districtRow++;
                }

                IVTRange range = districtSheet.GetRange(firstDistrictRow, 2, districtRow - 1, 2);
                range.Merge();

                //Dien du lieu xa phuong
                List<Commune> lstCommuneDistrict = (from c in lstCommune
                                                    join d in lstDistrictbyProvince on c.DistrictID equals d.DistrictID
                                                    orderby d.DistrictName, c.CommuneName
                                                    select c).ToList();
                if (lstCommuneDistrict == null || lstCommuneDistrict.Count == 0)
                {
                    continue;
                }


                communeSheet.CopyPasteSameSize(rgCommuneProvince, communeRow, 1);
                communeSheet.SetCellValue(communeRow, 1, i + 1);
                communeSheet.SetCellValue(communeRow, 2, province.ProvinceName);
                communeRow++;

                for (int j = 0; j < lstCommuneDistrict.Count; j++)
                {
                    Commune objCommune = lstCommuneDistrict[j];
                    communeSheet.CopyPasteSameSize(rgcommenu, communeRow, 1);
                    communeSheet.SetCellValue(communeRow, 1, communeOrder);
                    communeSheet.SetCellValue(communeRow, 2, objCommune.District.DistrictCode);
                    communeSheet.SetCellValue(communeRow, 3, objCommune.District.DistrictName);
                    communeSheet.SetCellValue(communeRow, 4, objCommune.CommuneCode);
                    communeSheet.SetCellValue(communeRow, 5, objCommune.CommuneName);
                    communeRow++;
                    communeOrder++;
                }
                //commnueProvinceRow += 1;
                //commnueProvinceRow += lstCommuneDistrict.Count;

            }
            #endregion
            districtSheet.DeleteRow(4);
            communeSheet.DeleteRow(4);
            communeSheet.DeleteRow(4);
        }

        #region detail

        /// <summary>
        /// Lay chi tiet ho so can bo.
        /// Ham nay duoc them vao de fix bug ghi log nhieu lan (bug 14831)
        /// Neu goi truc tiep ham GetDetailEmployee thi se bi ghi log nhieu lan do ham nay tra ve view _Detail co chua nhieu tab 
        /// (tuong ung moi tab se co mot request nguoc len server nen ghi log nhieu lan)
        /// </summary>
        /// <param name="EmployeeId"></param>
        /// <returns></returns>

        public ActionResult GetDetailOfEmployee(int EmployeeId)
        {
            CheckPermissionForAction(EmployeeId, "Employee");
            GlobalInfo global = new GlobalInfo();
            if ((global.IsSubSuperVisingDeptRole || global.IsSuperVisingDeptRole) && global.EmployeeID != null)
            {
                List<Menu> menus = Session[GlobalConstants.MENUS] as List<Menu>;
                if (!menus.Any(u => u.URL.ToLower().Contains("employeeprofilemanagement")) && global.EmployeeID != EmployeeId)
                {
                    return RedirectToAction("/Home/RoleError");
                }
            }
            Employee employee = this.EmployeeBusiness.Find(EmployeeId);
            if (employee == null)
            {
                return RedirectToAction("/Home/RoleError");
            }
            SetViewDataActionAudit(String.Empty, String.Empty, EmployeeId.ToString(), "View employee_id:" + EmployeeId, EmployeeId.ToString(), "Hồ sơ cán bộ/nhân viên", GlobalConstants.ACTION_VIEW, "Xem hồ sơ " + employee.FullName + " mã " + employee.EmployeeCode);
            return RedirectToAction("GetDetailEmployee", new { EmployeeId = EmployeeId });
        }

        /// <summary>
        /// Lay chi tiet ho so can bo
        /// </summary>
        /// <param name="EmployeeId"></param>
        /// <returns></returns>
        public ActionResult GetDetailEmployee(int EmployeeId)
        {

            CheckPermissionForAction(EmployeeId, "Employee");
            GlobalInfo global = new GlobalInfo();
            if ((global.IsSubSuperVisingDeptRole || global.IsSuperVisingDeptRole) && global.EmployeeID != null)
            {
                List<Menu> menus = Session[GlobalConstants.MENUS] as List<Menu>;
                if (!menus.Any(u => u.URL.ToLower().Contains("employeeprofilemanagement")) && global.EmployeeID != EmployeeId)
                {
                    return RedirectToAction("/Home/RoleError");
                }
            }
            Employee employee = this.EmployeeBusiness.Find(EmployeeId);
            if (employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementConstant.EMPLOYEE_WORKING] = "false";
            }
            EmployeeProfileManagementViewModel EmployeeProfileManagementViewModel = new EmployeeProfileManagementViewModel();
            SetViewData();
            bool isSupervisingDept = global.IsSuperVisingDeptRole || global.IsSubSuperVisingDeptRole;
            ViewData[EmployeeProfileManagementConstant.IS_SUPERVISINGDEPT] = isSupervisingDept;
            #region load du lieu tab 1

            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");

            Utils.Utils.BindTo(employee, EmployeeProfileManagementViewModel);

            if (employee.FatherBirthDate.HasValue) EmployeeProfileManagementViewModel.iFatherBirthDate = employee.FatherBirthDate.Value.Year;
            if (employee.MotherBirthDate.HasValue) EmployeeProfileManagementViewModel.iMotherBirthDate = employee.MotherBirthDate.Value.Year;
            if (employee.SpouseBirthDate.HasValue) EmployeeProfileManagementViewModel.iSpouseBirthDate = employee.SpouseBirthDate.Value.Year;

            //lay danh gia xep loai cua giao vien
            List<EmployeeEvaluationBO> lstEE = new List<EmployeeEvaluationBO>();
            EmployeeEvaluationBO objEE = null;
            IDictionary<string, object> dicEE = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"EmployeeID",employee.EmployeeID}
            };
            lstEE = EmployeeEvaluationBusiness.GetListEmployeeEvaluation(dicEE);
            if (lstEE.Count > 0)
            {
                objEE = lstEE.Where(p => p.EvaluationFieldID == GlobalConstants.EMPLOYEE_EVALUATION_FIELD1).FirstOrDefault();
                EmployeeProfileManagementViewModel.EvaluationField1 = objEE != null ? objEE.EvaluationLevelName : "";
                objEE = lstEE.Where(p => p.EvaluationFieldID == GlobalConstants.EMPLOYEE_EVALUATION_FIELD2).FirstOrDefault();
                EmployeeProfileManagementViewModel.EvaluationField2 = objEE != null ? objEE.EvaluationLevelName : "";
                objEE = lstEE.Where(p => p.EvaluationFieldID == GlobalConstants.EMPLOYEE_EVALUATION_FIELD3).FirstOrDefault();
                EmployeeProfileManagementViewModel.EvaluationField3 = objEE != null ? objEE.EvaluationLevelName : "";
                objEE = lstEE.Where(p => p.EvaluationFieldID == GlobalConstants.EMPLOYEE_EVALUATION_FIELD4).FirstOrDefault();
                EmployeeProfileManagementViewModel.EvaluationField4 = objEE != null ? objEE.EvaluationLevelName : "";
            }
            EmployeeProfileManagementViewModel.AppellationAward = employee.HigiestTitle;
            //EmployeeProfileManagementViewModel.WorkGroupTypeID = employee.WorkTypeID.HasValue ? employee.WorkType.WorkGroupTypeID : new Nullable<int>();

            //vi employee chua co workgrouptypeid nen phai them vao dua vao worktypeid
            if (EmployeeProfileManagementViewModel.WorkGroupTypeID.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["WorkGroupTypeID"] = EmployeeProfileManagementViewModel.WorkGroupTypeID.Value;
                dic["IsActive"] = true;
                IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(dic);
                ViewData[EmployeeProfileManagementConstant.LS_WORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution");
            }
            EmployeeProfileManagementViewModel.Sex = EmployeeProfileManagementViewModel.Genre ? male : female;
            EmployeeProfileManagementViewModel.TrainingLevelName = employee.TrainingLevel != null ? employee.TrainingLevel.Resolution : null;
            EmployeeProfileManagementViewModel.SpecialityCatName = employee.SpecialityCat != null ? employee.SpecialityCat.Resolution : null;
            EmployeeProfileManagementViewModel.QualificationLevelName = employee.QualificationLevel != null ? employee.QualificationLevel.Resolution : null;
            EmployeeProfileManagementViewModel.ForeignLanguageGradeName = employee.ForeignLanguageGrade != null ? employee.ForeignLanguageGrade.Resolution : null;
            EmployeeProfileManagementViewModel.ITQualificationLevelName = employee.ITQualificationLevel != null ? employee.ITQualificationLevel.Resolution : null;
            EmployeeProfileManagementViewModel.PoliticalGradeName = employee.PoliticalGrade != null ? employee.PoliticalGrade.Resolution : null;
            EmployeeProfileManagementViewModel.StateManagementGradeName = employee.StateManagementGrade != null ? employee.StateManagementGrade.Resolution : null;
            EmployeeProfileManagementViewModel.EducationalManagementGradeName = employee.EducationalManagementGrade != null ? employee.EducationalManagementGrade.Resolution : null;
            EmployeeProfileManagementViewModel.ContractTypeName = employee.ContractType != null ? employee.ContractType.Resolution : null;
            EmployeeProfileManagementViewModel.WorkGroupTypeName = employee.WorkGroupType != null ? employee.WorkGroupType.Resolution : null;
            //EmployeeProfileManagementViewModel.WorkGroupTypeName = employee.WorkType != null && employee.WorkType.WorkGroupType != null ? employee.WorkType.WorkGroupType.Resolution : null;
            EmployeeProfileManagementViewModel.WorkTypeName = employee.WorkType != null ? employee.WorkType.Resolution : null;
            EmployeeProfileManagementViewModel.PrimarilyAssignedSubjectName = employee.SubjectCat != null ? employee.SubjectCat.DisplayName : null;
            EmployeeProfileManagementViewModel.ReligionName = employee.Religion != null ? employee.Religion.Resolution : null;
            EmployeeProfileManagementViewModel.EthnicName = employee.Ethnic != null ? employee.Ethnic.EthnicName : null;
            EmployeeProfileManagementViewModel.FamilyTypeName = employee.FamilyType != null ? employee.FamilyType.Resolution : null;
            EmployeeProfileManagementViewModel.QualificationTypeName = employee.QualificationType != null ? employee.QualificationType.Resolution : null;
            EmployeeProfileManagementViewModel.SchoolFacultyName = employee.SchoolFaculty != null ? employee.SchoolFaculty.FacultyName : null;
            ViewData[EmployeeProfileManagementConstant.CHOSENEMPLOYEE] = EmployeeProfileManagementViewModel;
            #endregion load du lieu tab 1
            // Set viewdata cho việc readonly cho các tab trong menu xem hồ sơ cán bộ
            ViewData["checkReadonly"] = false;

            // Tạo giá trị lưu log action_audit
            //SetViewDataActionAudit(String.Empty, String.Empty, EmployeeId.ToString(), "View employee_id:" + EmployeeId, EmployeeId.ToString(), "Hồ sơ cán bộ/nhân viên", GlobalConstants.ACTION_VIEW, "Xem hồ sơ " + employee.FullName +" mã " + employee.EmployeeCode);
            ViewData[EmployeeProfileManagementConstant.ENABLE_BACKUP] = "false";
            var Permission2 = GetMenupermission("EmployeeProfileManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            if (Permission2 >= GlobalConstants.PERMISSION_LEVEL_VIEW)
            {
                ViewData[EmployeeProfileManagementConstant.ENABLE_BACKUP] = "true";
            }
            return PartialView("_Detail");
        }

        #endregion detail

        #region AjaxLoadTab
        public PartialViewResult AjaxLoadTab(int Tab, int EmployeeID,int hdfIsEmployee)
        {
            CheckPermissionForAction(EmployeeID, "Employee");
            Employee employee = this.EmployeeBusiness.Find(EmployeeID);
            if (employee.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementConstant.EMPLOYEE_WORKING] = "false";
            }
            ViewData["FullName"] = employee.FullName;
            EmployeeProfileManagementViewModel EmployeeProfileManagementViewModel = new EmployeeProfileManagementViewModel();
            SetViewData(hdfIsEmployee == 1 ? true : false);
            bool isSupervisingDept = _globalInfo.IsSuperVisingDeptRole || _globalInfo.IsSubSuperVisingDeptRole;
            ViewData[EmployeeProfileManagementConstant.IS_SUPERVISINGDEPT] = isSupervisingDept;
            Utils.Utils.BindTo(employee, EmployeeProfileManagementViewModel);
            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            if (EmployeeProfileManagementViewModel.WorkGroupTypeID.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["WorkGroupTypeID"] = EmployeeProfileManagementViewModel.WorkGroupTypeID.Value;
                dic["IsActive"] = true;
                IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(dic);
                ViewData[EmployeeProfileManagementConstant.LS_WORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution");
            }
            EmployeeProfileManagementViewModel.Sex = EmployeeProfileManagementViewModel.Genre ? male : female;
            EmployeeProfileManagementViewModel.TrainingLevelName = employee.TrainingLevel != null ? employee.TrainingLevel.Resolution : null;
            EmployeeProfileManagementViewModel.SpecialityCatName = employee.SpecialityCat != null ? employee.SpecialityCat.Resolution : null;
            EmployeeProfileManagementViewModel.QualificationLevelName = employee.QualificationLevel != null ? employee.QualificationLevel.Resolution : null;
            EmployeeProfileManagementViewModel.ForeignLanguageGradeName = employee.ForeignLanguageGrade != null ? employee.ForeignLanguageGrade.Resolution : null;
            EmployeeProfileManagementViewModel.ITQualificationLevelName = employee.ITQualificationLevel != null ? employee.ITQualificationLevel.Resolution : null;
            EmployeeProfileManagementViewModel.PoliticalGradeName = employee.PoliticalGrade != null ? employee.PoliticalGrade.Resolution : null;
            EmployeeProfileManagementViewModel.StateManagementGradeName = employee.StateManagementGrade != null ? employee.StateManagementGrade.Resolution : null;
            EmployeeProfileManagementViewModel.EducationalManagementGradeName = employee.EducationalManagementGrade != null ? employee.EducationalManagementGrade.Resolution : null;
            EmployeeProfileManagementViewModel.ContractTypeName = employee.ContractType != null ? employee.ContractType.Resolution : null;
            EmployeeProfileManagementViewModel.WorkGroupTypeName = employee.WorkGroupType != null ? employee.WorkGroupType.Resolution : null;
            EmployeeProfileManagementViewModel.WorkTypeName = employee.WorkType != null ? employee.WorkType.Resolution : null;
            EmployeeProfileManagementViewModel.PrimarilyAssignedSubjectName = employee.SubjectCat != null ? employee.SubjectCat.DisplayName : null;
            EmployeeProfileManagementViewModel.ReligionName = employee.Religion != null ? employee.Religion.Resolution : null;
            EmployeeProfileManagementViewModel.EthnicName = employee.Ethnic != null ? employee.Ethnic.EthnicName : null;
            EmployeeProfileManagementViewModel.FamilyTypeName = employee.FamilyType != null ? employee.FamilyType.Resolution : null;
            EmployeeProfileManagementViewModel.QualificationTypeName = employee.QualificationType != null ? employee.QualificationType.Resolution : null;
            EmployeeProfileManagementViewModel.SchoolFacultyName = employee.SchoolFaculty != null ? employee.SchoolFaculty.FacultyName : null;
            ViewData[EmployeeProfileManagementConstant.CHOSENEMPLOYEE] = EmployeeProfileManagementViewModel;
            ViewData["checkReadonly"] = false;
            ViewData[EmployeeProfileManagementConstant.ENABLE_BACKUP] = "false";
            ViewData[EmployeeProfileManagementConstant.IS_EMPLOYEE] = hdfIsEmployee;
            var permission = -1;
            if (hdfIsEmployee == 1)
            {
                permission = GetMenupermission("EmployeeProfile_ManagementForTeacher", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            }
            else
            {
                permission = GetMenupermission("EmployeeProfileManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            }
            if (permission >= GlobalConstants.PERMISSION_LEVEL_VIEW)
            {
                ViewData[EmployeeProfileManagementConstant.ENABLE_BACKUP] = "true";
            }
            bool isPermiss = true;
            string PartitalViewStr = string.Empty;
            if (Tab == 2)
            {
                if (_globalInfo.IsSchoolRole)
                {
                    if (employee.SchoolID.HasValue)
                        isPermiss &= employee.SchoolID.Value == _globalInfo.SchoolID.Value;
                }
                else if (_globalInfo.IsSuperVisingDeptRole)
                {
                    if (employee.SupervisingDeptID.HasValue)
                        isPermiss &= employee.SupervisingDeptID.Value == _globalInfo.SupervisingDeptID.Value;
                }
                else
                {
                    isPermiss &= false;
                }
                ViewData[EmployeeWorkingHistoryConstants.IS_PERMISS] = isPermiss;

                IEnumerable<EmployeeWorkingHistoryViewModel> lst = employee.EmployeeWorkingHistories
                                                                            .Select(o => new EmployeeWorkingHistoryViewModel
                                                                            {
                                                                                EmployeeWorkingHistoryID = o.EmployeeWorkingHistoryID,
                                                                                EmployeeID = o.EmployeeID,
                                                                                EmployeeName = o.Employee.FullName,
                                                                                SupervisingDeptID = o.SupervisingDeptID,
                                                                                SchoolID = o.SchoolID,
                                                                                Organization = o.Organization,
                                                                                Department = o.Department,
                                                                                Position = o.Position,
                                                                                Resolution = o.Resolution,
                                                                                FromDate = o.FromDate,
                                                                                ToDate = o.ToDate
                                                                            })
                                                                            .OrderByDescending(o => o.FromDate)
                                                                            .ThenBy(u => u.EmployeeName)
                                                                            .ToList();
                ViewData[EmployeeWorkingHistoryConstants.LIST_EMPLOYEEWORKINGHISTORY] = lst;
                PartitalViewStr = "../EmployeeWorkingHistory/IndexEmployeeWorkingHistory";
            }
            else if (Tab == 3)
            {
                this.LoadEmployeeQualification(employee);
                PartitalViewStr = "../EmployeeQualification/Index";
            }
            else if (Tab == 4)
            {
                this.LoadEmployeeSalary(employee);
                PartitalViewStr = "../EmployeeSalary/Index";
            }
            return PartialView(PartitalViewStr);
        }

        #endregion

        #region search

        //
        // GET: /EmployeeProfileManagement/Search


        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();
            if (global.IsCurrentYear)
            {
                ViewData[EmployeeProfileManagementConstant.IS_CURRENT_YEAR] = true;
            }
            else
            {
                ViewData[EmployeeProfileManagementConstant.IS_CURRENT_YEAR] = false;
            }
            //add search info
            SearchInfo["FacultyID"] = frm.SchoolFaculty;
            SearchInfo["WorkTypeID"] = frm.WorkType;
            SearchInfo["EmploymentStatus"] = frm.EmploymentStatus;
            SearchInfo["EmployeeCode"] = frm.EmployeeCode;
            SearchInfo["FullName"] = frm.Fullname;
            SearchInfo["BirthDate"] = frm.BirthDate;
            SearchInfo["Genre"] = frm.Sex;
            SearchInfo["TrainingLevelID"] = frm.TrainingLevel;
            SearchInfo["ContractTypeID"] = frm.ContractType;
            SearchInfo["AcademicYearID"] = global.AcademicYearID;

            // DungVA -12/08 - Chuẩn bị dữ liệu cho việc quay lại trạng thái màn hình tìm kiếm
            Dictionary<string, object> dicForBack = new Dictionary<string, object>();

            dicForBack["FacultyID"] = frm.SchoolFaculty;
            dicForBack["WorkTypeID"] = frm.WorkType;
            dicForBack["EmploymentStatus"] = frm.EmploymentStatus;
            dicForBack["EmployeeCode"] = frm.EmployeeCode;
            dicForBack["FullName"] = frm.Fullname;

            dicForBack["BirthDate"] = frm.BirthDate;
            if (frm.Sex.HasValue)
            {
                dicForBack["Genre"] = CommonConvert.Genrebool(frm.Sex.Value);
            }
            dicForBack["TrainingLevelID"] = frm.TrainingLevel;
            dicForBack["ContractTypeID"] = frm.ContractType;
            dicForBack["AcademicYearID"] = global.AcademicYearID;
            Session["forBack"] = dicForBack;
            ViewData[EmployeeProfileManagementConstant.FORBACK] = dicForBack;

            // Page 1
            List<EmployeeProfileManagementViewModel> lst = this._Search(SearchInfo).ToList();
            Session[EmployeeProfileManagementConstant.LIST_EMPLOYEE] = lst;
            List<EmployeeProfileManagementViewModel> lstPage1 = lst.Skip(0).Take(Config.PageSize).ToList();
            ViewData[EmployeeProfileManagementConstant.EMPLOYEE_TOTAL] = lst.Count;
            ViewData[EmployeeProfileManagementConstant.PAGE_NUMBER] = 1;
            ViewData[EmployeeProfileManagementConstant.LIST_EMPLOYEE] = lstPage1;


            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<EmployeeProfileManagementViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }

            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");
            if (SearchInfo.ContainsKey("Genre") && SearchInfo["Genre"] != null)
            {
                if (SearchInfo["Genre"].ToString().ToLower() == male.Trim().ToLower())
                {
                    SearchInfo["Genre"] = true;
                }
                if (SearchInfo["Genre"].ToString().ToLower() == female.Trim().ToLower())
                {
                    SearchInfo["Genre"] = false;
                }
            }
            GlobalInfo global = new GlobalInfo();

            List<EmployeeBO> query = this.EmployeeWorkingHistoryBusiness.SearchTeacher(global.SchoolID.Value, SearchInfo).ToList();
            //IQueryable<Employee> query = this.EmployeeBusiness.SearchTeacher(global.SchoolID.Value, SearchInfo);
            var Permission2 = GetMenupermission("EmployeeProfileManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            ViewData[EmployeeProfileManagementConstant.HAS_PERMISSION] = Permission2;
            ViewData[EmployeeProfileManagementConstant.Has_Permission_TeachingAssignment] = GetMenupermission("TeachingAssignment", global.UserAccountID, global.IsAdmin);
            List<EmployeeProfileManagementViewModel> lst = query.Select(o => new EmployeeProfileManagementViewModel
            {
                EmployeeID = o.EmployeeID,
                EmployeeCode = o.EmployeeCode,
                EmployeeType = o.EmployeeType,
                EmploymentStatus = o.EmployeeStatus,
                SupervisingDeptID = o.SupervisingDeptID,
                SchoolID = o.SchoolID,
                Name = o.Name,
                FullName = o.FullName,
                BirthDate = o.BirthDate,
                IdentityNumber = o.IdentifyNumber,
                IdentityIssuedDate = o.IdentityIssuedDate,
                IdentityIssuedPlace = o.IdentityIssuedPlace,
                Mobile = o.Mobile,
                Telephone = o.Telephone,
                HomeTown = o.HomeTown,
                Email = o.Email,
                Genre = o.Genre,
                JoinedDate = o.JoinedDate,
                StartingDate = o.StartingDate,
                HealthStatus = o.HealthStatus,
                RegularRefresher = o.RegularRefresher.HasValue ? o.RegularRefresher.Value : false,
                GraduationLevelID = o.GraduationLevelID,
                ContractID = o.ContractID,
                QualificationTypeID = o.QualificationTypeID,
                SpecialityCatID = o.SpecialityCatID,
                QualificationLevelID = o.QualificationLevelID,
                QualificationLevelName = o.QualificationLevelName,
                ITQualificationLevelID = o.ITQualificationLevelID,
                ITQualificationLevelName = o.ITQualificationLevelResolution,
                WorkTypeID = o.WorkTypeID,
                SchoolFacultyID = o.SchoolFacultyID,
                Description = o.Description,
                Sex = o.Genre ? male : female,
                SchoolFacultyName = o.SchoolFacultyName,
                WorkTypeName = o.WorkTypeName,
                ContractName = o.ContractName,
                ContractTypeName = o.ContractTypeName,
                ContractTypeID = o.ContractTypeID,
                WorkGroupTypeID = o.WorkGroupTypeID,
                TrainingLevelName = o.TrainingLevelResolution,
                PermanentResidentalAddress = o.PermanentResidentalAddress,
                IsUsed = false,
                IntoSchoolDate = o.IntoSchoolDate,
                EthnicName = o.EthnicName,
                ReligionName = o.ReligionName,
                AppliedLevel = o.AppliedLevel ?? -1,
                AppliedLevelName = o.AppliedLevel == 1 ? "Cấp 1" :
                                    o.AppliedLevel == 2 ? "Cấp 2" :
                                    o.AppliedLevel == 3 ? "Cấp 3" :
                                    o.AppliedLevel == 4 ? "Nhà trẻ" :
                                    o.AppliedLevel == 5 ? "Mẫu giáo" : null,
                FatherFullName = o.FatherFullName,
                FatherBirthDate = o.FatherBirthDate,
                FatherJob = o.FatherJob,
                FatherWorkingPlace = o.FatherWorkingPlace,
                MotherFullName = o.MotherFullName,
                MotherBirthDate = o.MotherBirthDate,
                MotherJob = o.MotherJob,
                MotherWorkingPlace = o.MotherWorkingPlace,
                SpouseFullName = o.SpouseFullName,
                SpouseBirthDate = o.SpouseBirthDate,
                SpouseJob = o.SpouseJob,
                SpouseWorkingPlace = o.SpouseWorkingPlace,
                ForeignLanguageGradeName = o.ForeignLanguageGradeName,
                PoliticalGradeName = o.PoliticalGradeName,
                StateManagementGradeName = o.StateManagementGradeName,
                EducationalManagementGradeName = o.EducationalManagementGradeName,
                DedicatedForYoungLeague = o.DedicatedForYoungLeague.HasValue ? o.DedicatedForYoungLeague.Value : false,
                IsCommunistPartyMember = o.IsCommunistPartyMember.HasValue ? o.IsCommunistPartyMember.Value : false,
                IsYouthLeageMember = o.IsYouthLeageMember.HasValue ? o.IsYouthLeageMember.Value : false,
                IsSyndicate = o.IsSyndicate.HasValue ? o.IsSyndicate.Value : false,
                SpecialityCatName = o.SpecialityCatResolution,
                FamilyTypeName = o.FamilyTypeName,
                InsuranceNumber = o.InsuranceNumber,
                ProvinceName = o.ProvinceName,
                DistrictName = o.DistrictName,
                CommuneName = o.CommuneName
            }).ToList();
            List<EmployeeProfileManagementViewModel> lsEmployeeProfileManagementViewModel = lst;

            List<EmployeeWorkMovement> lstEmpoyeeMovementOfSchool = EmployeeWorkMovementBusiness.Search(new Dictionary<string, object> { { "FromSchoolID", global.SchoolID.Value } }).ToList();

            IDictionary<string, object> dicEmBreather = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID}
            };
            List<EmployeeBreather> lstEmployeeBreather = EmployeeBreatherBusiness.GetListEmployeeBreather(dicEmBreather).ToList();
            EmployeeWorkMovement objemployMovement = null;
            EmployeeBreather objEmployeeBreather = null;

            foreach (EmployeeProfileManagementViewModel item in lsEmployeeProfileManagementViewModel)
            {
                if (item.EmploymentStatus.HasValue)
                {
                    if (item.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
                    {
                        item.EmploymentStatusName = Res.Get("Employee_Label_EmployeeStatusWorking");
                    }
                    else if (item.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_MOVED)
                    {
                        item.EmploymentStatusName = Res.Get("Employee_Label_EmployeeStatusWorkingChange");
                    }
                    else if (item.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_RETIRED)
                    {
                        item.EmploymentStatusName = Res.Get("Employee_Label_EmployeeStatusRetired");
                    }
                    else if (item.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_SEVERANCE)
                    {
                        item.EmploymentStatusName = Res.Get("Employee_Label_EmployeeStatusSeverance");
                    }
                    else if (item.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_BREATHER)
                    {
                        item.EmploymentStatusName = Res.Get("Employee_Label_EmployeeStatusBreather");
                    }

                    objemployMovement = lstEmpoyeeMovementOfSchool.Where(u => u.TeacherID == item.EmployeeID).OrderByDescending(u => u.MovedDate).FirstOrDefault();
                    if (objemployMovement != null)
                    {
                        item.EmployeeWorkMovementID = objemployMovement.EmployeeWorkMovementID;
                        item.FromSchoolID = objemployMovement.FromSchoolID;
                        item.ResolutionDocument = objemployMovement.ResolutionDocument;
                        item.ToSchoolID = objemployMovement.ToSchoolID;
                        item.ToSupervisingDeptID = objemployMovement.ToSupervisingDeptID;
                        item.ToProvinceID = objemployMovement.ToProvinceID;
                        item.ToDistrictID = objemployMovement.ToDistrictID;
                        item.ToCommuneID = objemployMovement.ToCommuneID;
                        item.MovementType = objemployMovement.MovementType;
                        item.MoveTo = objemployMovement.MoveTo;
                        item.DescriptionMovement = objemployMovement.Description;
                        item.MovedDate = objemployMovement.MovedDate;
                        item.IsAccepted = objemployMovement.IsAccepted;
                        item.FacultyID = objemployMovement.FacultyID;
                    }

                    objEmployeeBreather = lstEmployeeBreather.Where(p => p.TeacherID == item.EmployeeID).FirstOrDefault();
                    if (objEmployeeBreather != null)
                    {
                        item.EmployeeBreatherID = objEmployeeBreather.EmployeeBreatherID;
                        item.DateBreather = objEmployeeBreather.DateBreather;
                        item.VacationReasonID = objEmployeeBreather.VacationReasonID;
                        item.DateStartWork = objEmployeeBreather.DateStartWork;
                        item.Note = objEmployeeBreather.Note;
                        item.DateStartWork = objEmployeeBreather.DateStartWork;
                        item.IsComeBack = objEmployeeBreather.IsComeBack;
                    }
                }
            }

            lsEmployeeProfileManagementViewModel = lsEmployeeProfileManagementViewModel.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            return lsEmployeeProfileManagementViewModel;
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<EmployeeWorkingHistoryViewModel> _SearchEmployeeWorkingHistory(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<EmployeeWorkingHistory> query = EmployeeWorkingHistoryBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo);
            IQueryable<EmployeeWorkingHistoryViewModel> lst = query.Select(o => new EmployeeWorkingHistoryViewModel
            {
                EmployeeWorkingHistoryID = o.EmployeeWorkingHistoryID,
                EmployeeID = o.EmployeeID,
                EmployeeName = o.Employee.FullName,
                SupervisingDeptID = o.SupervisingDeptID,
                SchoolID = o.SchoolID,
                Organization = o.Organization,
                Department = o.Department,
                Position = o.Position,
                Resolution = o.Resolution,
                FromDate = o.FromDate,
                ToDate = o.ToDate
            });

            List<EmployeeWorkingHistoryViewModel> lsEmployeeWorkingHistoryViewModel = lst.ToList();
            return lsEmployeeWorkingHistoryViewModel.OrderByDescending(o => o.FromDate).OrderBy(o => o.EmployeeName).ToList();
        }


        /// <summary>
        /// Tìm kiếm có phân trang
        /// </summary>
        /// 
        [GridAction(EnableCustomBinding = true)]
        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(GridCommand command)
        {
            int Page = 1;
            if (command != null) Page = command.Page;

            string Member = command.SortDescriptors.Count > 0 ? command.SortDescriptors[0].Member : "";
            string SortDirection = command.SortDescriptors.Count > 0 ? command.SortDescriptors[0].SortDirection.ToString() : "Descending";
            IEnumerable<EmployeeProfileManagementViewModel> lst;
            if (Session[EmployeeProfileManagementConstant.LIST_EMPLOYEE] != null)
            {
                lst = (IEnumerable<EmployeeProfileManagementViewModel>)Session[EmployeeProfileManagementConstant.LIST_EMPLOYEE];
            }
            else
            {
                IDictionary<string, object> SearchInfo = (IDictionary<string, object>)Session["forBack"];
                lst = this._Search(SearchInfo);
            }

            if (command.SortDescriptors.Count > 0 && command.SortDescriptors != null)
            {
                lst = this.SortGrid(lst, Member, SortDirection);
            }

            List<EmployeeProfileManagementViewModel> lstAtCurrentPage = lst.Skip((Page - 1) * Config.PageSize).Take(Config.PageSize).ToList();
            ViewData[EmployeeProfileManagementConstant.LIST_EMPLOYEE] = lstAtCurrentPage;
            GridModel<EmployeeProfileManagementViewModel> gm = new GridModel<EmployeeProfileManagementViewModel>(lstAtCurrentPage);
            gm.Total = lst.Count();
            return View(gm);
        }

        #endregion search

        #region sort

        private IEnumerable<EmployeeProfileManagementViewModel> SortGrid(IEnumerable<EmployeeProfileManagementViewModel> lst, string Member, string SortDirection)
        {
            if (Member.Equals("EmployeeCode"))
            {
                if (SortDirection.Equals("Descending"))
                {
                    lst = lst.OrderByDescending(p => p.Name).ToList();
                }
                else
                {
                    lst = lst.OrderBy(p => p.Name).ToList();
                }
            }
            else if (Member.Equals("BirthDate"))
            {
                if (SortDirection.Equals("Descending"))
                {
                    lst = lst.OrderByDescending(p => p.BirthDate).ToList();
                }
                else
                {
                    lst = lst.OrderBy(p => p.BirthDate).ToList();
                }
            }
            else if (Member.Equals("Sex"))
            {
                if (SortDirection.Equals("Descending"))
                {
                    lst = lst.OrderByDescending(p => p.Sex).ToList();
                }
                else
                {
                    lst = lst.OrderBy(p => p.Sex).ToList();
                }

            }
            else if (Member.Equals("SchoolFacultyName"))
            {
                if (SortDirection.Equals("Descending"))
                {
                    lst = lst.OrderByDescending(p => p.SchoolFacultyName).ToList();
                }
                else
                {
                    lst = lst.OrderBy(p => p.SchoolFacultyName).ToList();
                }
            }
            else if (Member.Equals("WorkTypeName"))
            {
                if (SortDirection.Equals("Descending"))
                {
                    lst = lst.OrderByDescending(p => p.WorkTypeName).ToList();
                }
                else
                {
                    lst = lst.OrderBy(p => p.WorkTypeName).ToList();
                }
            }
            else if (Member.Equals("ContractTypeName"))
            {
                if (SortDirection.Equals("Descending"))
                {
                    lst = lst.OrderByDescending(p => p.ContractTypeName).ToList();
                }
                else
                {
                    lst = lst.OrderBy(p => p.ContractTypeName).ToList();
                }
            }
            else if (Member.Equals("TrainingLevelName"))
            {
                if (SortDirection.Equals("Descending"))
                {
                    lst = lst.OrderByDescending(p => p.TrainingLevelName).ToList();
                }
                else
                {
                    lst = lst.OrderBy(p => p.TrainingLevelName).ToList();
                }
            }
            else if (Member.Equals("EmploymentStatusName"))
            {
                if (SortDirection.Equals("Descending"))
                {
                    lst = lst.OrderByDescending(p => p.EmploymentStatusName).ToList();
                }
                else
                {
                    lst = lst.OrderBy(p => p.EmploymentStatusName).ToList();
                }
            }
            return lst;
        }

        #endregion

        #region setViewData

        public void SetViewData(bool? isEmployee = false)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = GlobalInfo.getInstance();
            var hasPermision = -1;
            if (isEmployee.HasValue && isEmployee.Value)
            {
                hasPermision = GetMenupermission("EmployeeProfile_ManagementForTeacher", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            }
            else
            {
                hasPermision = GetMenupermission("EmployeeProfileManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            }
            ViewData[EmployeeWorkingHistoryConstants.HAS_PERMISSION] = hasPermision;
            ViewData[EmployeeQualificationConstants.HAS_PERMISSION] = hasPermision;
            ViewData[EmployeeSalaryConstants.HAS_PERMISSION] = hasPermision;
            ViewData[EmployeeProfileManagementConstant.Has_Permission_TeachingAssignment] = hasPermision;
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            IQueryable<SchoolFaculty> lsSchoolFaculty = SchoolFacultyBusiness.Search(SearchInfo).OrderBy(o => o.FacultyName);
            ViewData[EmployeeProfileManagementConstant.LISTSCHOOLFACULTY] = new SelectList(lsSchoolFaculty, "SchoolFacultyID", "FacultyName");
            List<WorkType> lsWorkType = this.getListOfWorkType();
            ViewData[EmployeeProfileManagementConstant.LISTWORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution");

            //cboEmploymentStatus:
            //+ EMPLOYMENT_STATUS_WORKING : Đang làm việc
            //+ EMPLOYMENT_STATUS_WORK_CHANGED : Chuyển công tác
            //+ EMPLOYMENT_STATUS_RETIRED : Nghỉ hưu
            //+ EMPLOYMENT_STATUS_BREATHER: Tạm nghỉ
            List<ComboObject> listEmploymentStatus = new List<ComboObject>();
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementConstant.EMPLOYMENT_STATUS_WORKING.ToString(), Res.Get("Employee_Label_EmployeeStatusWorking")));
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementConstant.EMPLOYMENT_STATUS_WORK_CHANGED.ToString(), Res.Get("Employee_Label_EmployeeStatusWorkingChange")));
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementConstant.EMPLOYMENT_STATUS_RETIRED.ToString(), Res.Get("Employee_Label_EmployeeStatusRetired")));
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementConstant.EMPLOYMENT_STATUS_SEVERANCE.ToString(), Res.Get("Employee_Label_EmployeeStatusSeverance")));
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementConstant.EMPLOYMENT_STATUS_BREATHER.ToString(), Res.Get("Common_Label_EmployeeStatusLeavedOff")));
            ViewData[EmployeeProfileManagementConstant.LISTEMPLOYMENTSTATUS] = new SelectList(listEmploymentStatus, "key", "value");

            //   cboSex:
            //+ 1 : Nam
            //+ 2 : Nữ
            List<ComboObject> listSex = new List<ComboObject>();
            listSex.Add(new ComboObject("true", Res.Get("Common_Label_Male")));
            listSex.Add(new ComboObject("false", Res.Get("Common_Label_Female")));
            ViewData[EmployeeProfileManagementConstant.LISTSEX] = new SelectList(listSex, "key", "value");

            List<TrainingLevel> lsTrainingLevel = this.getListOfTrainingLevel();
            ViewData[EmployeeProfileManagementConstant.LISTGRADUATIONLEVEL] = new SelectList(lsTrainingLevel, "TrainingLevelID", "Resolution");

            //cboGraduationLevel: Lấy danh sách từ GraduationLevelBussiness.Search(Dictionary) với Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel ->cho form create,edit
            List<TrainingLevel> lsGraduationLevelUpdateOrEdit = this.getListOfTrainingLevel();
            ViewData[EmployeeProfileManagementConstant.LS_GRADUATIONLEVEL] = new SelectList(lsGraduationLevelUpdateOrEdit, "TrainingLevelID", "Resolution");

            //cboContractType: Lấy danh sách từ ContractTypeBusiness.Search()
            List<ContractType> lsContractType = this.getListOfContractType();
            ViewData[EmployeeProfileManagementConstant.LISTCONTRACTTYPE] = new SelectList(lsContractType, "ContractTypeID", "Resolution", 6);

            // Danh sach cac dan toc
            List<Ethnic> listEthnic = this.getListOfEthnic();
            ViewData[EmployeeProfileManagementConstant.LS_ETHNIC] = new SelectList(listEthnic, "EthnicID", "EthnicName", SMAS.Business.Common.SystemParamsInFile.DEFAULT_ETHNIC);

            List<Religion> listReligion = this.getListOfReligion();
            ViewData[EmployeeProfileManagementConstant.LS_RELIGION] = new SelectList(listReligion, "ReligionID", "Resolution", SMAS.Business.Common.SystemParamsInFile.DEFAULT_RELIGION);

            List<FamilyType> listFamilyType = this.getListOfFamilyType();
            ViewData[EmployeeProfileManagementConstant.LS_FAMILYTYPE] = new SelectList(listFamilyType, "FamilyTypeID", "Resolution");

            List<QualificationType> listQualificationType = this.getListOfQualificationType();
            ViewData[EmployeeProfileManagementConstant.LS_QUALIFICATIONTYPE] = new SelectList(listQualificationType, "QualificationTypeID", "Resolution");

            List<SpecialityCat> listSpecialityCat = this.getListOfSpecialityCat();
            ViewData[EmployeeProfileManagementConstant.LS_SPECIALITYCAT] = new SelectList(listSpecialityCat, "SpecialityCatID", "Resolution");

            List<QualificationLevel> listQualificationLevel = this.getListOfQualificationLevel();
            ViewData[EmployeeProfileManagementConstant.LS_QUALIFICATIONLEVEL] = new SelectList(listQualificationLevel, "QualificationLevelID", "Resolution");

            List<ForeignLanguageGrade> listForeignLanguageGrade = this.getListOfForeignLanguageGrade();
            ViewData[EmployeeProfileManagementConstant.LS_FOREIGNLANGUAGEGRADE] = new SelectList(listForeignLanguageGrade, "ForeignLanguageGradeID", "Resolution");

            List<ITQualificationLevel> listITQualificationLevel = this.getListOfITQualificationLevel();
            ViewData[EmployeeProfileManagementConstant.LS_ITQUALIFICATIONLEVEL] = new SelectList(listITQualificationLevel, "ITQualificationLevelID", "Resolution");

            List<StateManagementGrade> listStateManagementGrade = this.getListOfStateManagementGrade();
            ViewData[EmployeeProfileManagementConstant.LS_STATEMANAGEMENTGRADE] = new SelectList(listStateManagementGrade, "StateManagementGradeID", "Resolution");

            List<PoliticalGrade> listPoliticalGrade = this.getListOfPoliticalGrade();
            ViewData[EmployeeProfileManagementConstant.LS_POLITICALGRADE] = new SelectList(listPoliticalGrade, "PoliticalGradeID", "Resolution");

            List<EducationalManagementGrade> listEducationalManagementGrade = this.getListOfEducationalManagementGrade();
            ViewData[EmployeeProfileManagementConstant.LS_EDUCATIONALMANAGEMENTGRADE] = new SelectList(listEducationalManagementGrade, "EducationalManagementGradeID", "Resolution");

            List<Contract> listContract = this.getListOfContract();
            ViewData[EmployeeProfileManagementConstant.LS_CONTRACT] = new SelectList(listContract, "ContractID", "ContractID");

            List<StaffPosition> listStaffPosition = this.getListOfStaffPosition();
            ViewData[EmployeeProfileManagementConstant.LS_STAFFPOSITION] = new SelectList(listStaffPosition, "StaffPositionID", "Resolution");

            List<SupervisingDept> listSupervisingDept = this.getListOfSupervisingDept();
            ViewData[EmployeeProfileManagementConstant.LS_SUPERVISINGDEPT] = new SelectList(listSupervisingDept, "SupervisingDeptID", "SupervisingDeptName");

            //cboWorkGroupType: Lấy danh sách từ WorkGroupTypeBussiness.Search()
            List<WorkGroupType> lsWorkGroupType = this.getListOfWorkGroupType();
            ViewData[EmployeeProfileManagementConstant.LS_WORKGROUPTYPE] = new SelectList(lsWorkGroupType, "WorkGroupTypeID", "Resolution", 2);

            //cboWorkType: Không có dữ liệu
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["WorkGroupTypeID"] = 2;
            lsWorkType = WorkTypeBusiness.Search(dic).ToList();
            ViewData[EmployeeProfileManagementConstant.LS_WORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution", 10);


            //cboPrimarilyAssignedSubject: Lấy danh sách từ SubjectCat.Search(Dictionary)
            //với Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
            IQueryable<SubjectCat> lsSubjectCat = SubjectCatBusiness.Search(new Dictionary<string, object>() { { "AppliedLevel", global.AppliedLevel.Value }, { "IsActive", true } }).OrderBy(o => o.DisplayName);
            ViewData[EmployeeProfileManagementConstant.LS_PRIMARILYASSIGNEDSUBJECT] = new SelectList(new string[] { });

            ViewData[EmployeeProfileManagementConstant.LS_APPLIEDLEVEL] = new SelectList(new List<ComboObject>(), "key", "value", _globalInfo.AppliedLevel);
            ViewData[EmployeeProfileManagementConstant.AUTO_GEN_CODE_CHECK] = false;
            ViewData[EmployeeProfileManagementConstant.IS_SCHOOL_MODIFIED] = false;
            ViewData[EmployeeProfileManagementConstant.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = false;
            ViewData[EmployeeProfileManagementConstant.AUTO_GEN_CODE_CHECK_AND_DISABLE] = true;
            if (global.SchoolID != null)
            {
                List<int> lsAL = SchoolProfileBusiness.GetListAppliedLevel(global.SchoolID.Value);
                lsAL.Sort();
                List<ComboObject> listAppliedLevel = new List<ComboObject>();

                foreach (var item in lsAL)
                {
                    listAppliedLevel.Add(new ComboObject(item.ToString(), CommonConvert.ConvertToResolutionApplied(item)));
                }
                ViewData[EmployeeProfileManagementConstant.LS_APPLIEDLEVEL] = new SelectList(listAppliedLevel, "key", "value", _globalInfo.AppliedLevel);

                IQueryable<SubjectCat> lsSubjectCats = SubjectCatBusiness.Search(new Dictionary<string, object>() {
                { "AppliedLevel", _globalInfo.AppliedLevel },
                { "IsActive", true } }).OrderBy(o => o.DisplayName);
                ViewData[EmployeeProfileManagementConstant.LS_PRIMARILYASSIGNEDSUBJECT] = new SelectList(lsSubjectCats, "SubjectCatID", "DisplayName");

                //Autogen code
                //Nếu  CodeConfigBusiness.IsAutoGenCode(UserInfo.SchoolID, CODE_CONFIG_TYPE_PUPIL) = TRUE thì chkAutoCode được chọn
                if (CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
                {
                    ViewData[EmployeeProfileManagementConstant.AUTO_GEN_CODE_CHECK] = true;
                }
                else
                {
                    ViewData[EmployeeProfileManagementConstant.AUTO_GEN_CODE_CHECK] = false;
                }

                //-	Nếu CodeConfigBusiness.IsSchoolModify(UserInfo.SchoolID, CODE_CONFIG_TYPE_PUPIL) = TRUE
                if (CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
                {
                    ViewData[EmployeeProfileManagementConstant.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = true;
                    ViewData[EmployeeProfileManagementConstant.AUTO_GEN_CODE_CHECK_AND_DISABLE] = false;
                    ViewData[EmployeeProfileManagementConstant.IS_SCHOOL_MODIFIED] = true;
                }
                else
                {
                    ViewData[EmployeeProfileManagementConstant.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = false;
                    ViewData[EmployeeProfileManagementConstant.AUTO_GEN_CODE_CHECK_AND_DISABLE] = true;
                    ViewData[EmployeeProfileManagementConstant.IS_SCHOOL_MODIFIED] = false;
                }
            }
            //var lstProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.ProvinceID, u.ProvinceName }).OrderBy(o => o.ProvinceName).ToList();
            var lstProvince = this.getListOfProvince();
            ViewData[EmployeeProfileManagementConstant.LIST_PROVINCE] = new SelectList(lstProvince, "ProvinceID", "ProvinceName");
            if (global.IsCurrentYear)
            {
                ViewData[EmployeeProfileManagementConstant.IS_CURRENT_YEAR] = true;
            }
            else
            {
                ViewData[EmployeeProfileManagementConstant.IS_CURRENT_YEAR] = false;
            }

            ViewData[EmployeeProfileManagementConstant.LIST_COLUMN_DESCRIPTION] = UtilsBusiness.GetListColumnDescription();

            List<ForeignLanguageCat> lstFl = ForeignLanguageCatBusiness.All.Where(o => o.IsActive).ToList();
            ViewData[EmployeeProfileManagementConstant.LS_FOREIGNLANGUAGECAT] = new SelectList(lstFl, "ForeignLanguageCatId", "ForeignLanguageName");

            List<ConcurrentWorkType> lstCc = ConcurrentWorkTypeBusiness.All.Where(o => o.SchoolID == _globalInfo.SchoolID && o.IsActive).OrderByDescending(o => o.CreatedDate).ToList();
            ViewData[EmployeeProfileManagementConstant.LIST_CONCURRENT_WORK_TYPE] = new SelectList(lstCc, "ConcurrentWorkTypeID", "Resolution");

        }

        #endregion setViewData

        #region setViewData dung cho search form

        //Bo mot so thanh phan ko can thiet de load cho nhanh
        public void SetViewData_SearchForm()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = GlobalInfo.getInstance();
            var Permission2 = GetMenupermission("EmployeeProfileManagement", global.UserAccountID, global.IsAdmin);
            ViewData[EmployeeWorkingHistoryConstants.HAS_PERMISSION] = Permission2;
            ViewData[EmployeeQualificationConstants.HAS_PERMISSION] = Permission2;
            ViewData[EmployeeSalaryConstants.HAS_PERMISSION] = Permission2;
            ViewData[EmployeeProfileManagementConstant.Has_Permission_TeachingAssignment] = Permission2;
            // SchoolFacultyBussiness.SearchBySchool(IDictionary)
            //với IDictionary[“SchoolID”] = UserInfo.SchoolID
            SearchInfo["SchoolID"] = global.SchoolID;
            SearchInfo["AcademicYearID"] = global.AcademicYearID;
            IQueryable<SchoolFaculty> lsSchoolFaculty = SchoolFacultyBusiness.Search(SearchInfo).OrderBy(o => o.FacultyName);
            ViewData[EmployeeProfileManagementConstant.LISTSCHOOLFACULTY] = new SelectList(lsSchoolFaculty, "SchoolFacultyID", "FacultyName");

            //cboWorkType: Lấy danh sách từ WorkTypeBusiness.Search()
            //IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(new Dictionary<string, object>() { 
            ////{ "SchoolID", global.SchoolID.Value }, 
            //{ "IsActive", true } }).OrderBy(o => o.Resolution);
            List<WorkType> lsWorkType = this.getListOfWorkType();
            ViewData[EmployeeProfileManagementConstant.LISTWORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution");

            //cboEmploymentStatus:
            //+ EMPLOYMENT_STATUS_WORKING : Đang làm việc
            //+ EMPLOYMENT_STATUS_WORK_CHANGED : Chuyển công tác
            //+ EMPLOYMENT_STATUS_RETIRED : Nghỉ hưu
            List<ComboObject> listEmploymentStatus = new List<ComboObject>();
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementConstant.EMPLOYMENT_STATUS_WORKING.ToString(), Res.Get("Employee_Label_EmployeeStatusWorking")));
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementConstant.EMPLOYMENT_STATUS_WORK_CHANGED.ToString(), Res.Get("Employee_Label_EmployeeStatusWorkingChange")));
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementConstant.EMPLOYMENT_STATUS_RETIRED.ToString(), Res.Get("Employee_Label_EmployeeStatusRetired")));
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementConstant.EMPLOYMENT_STATUS_SEVERANCE.ToString(), Res.Get("Employee_Label_EmployeeStatusSeverance")));
            listEmploymentStatus.Add(new ComboObject(EmployeeProfileManagementConstant.EMPLOYMENT_STATUS_BREATHER.ToString(), Res.Get("Employee_Label_EmployeeStatusBreather")));
            ViewData[EmployeeProfileManagementConstant.LISTEMPLOYMENTSTATUS] = new SelectList(listEmploymentStatus, "key", "value");

            //   cboSex:
            //+ 1 : Nam
            //+ 2 : Nữ
            List<ComboObject> listSex = new List<ComboObject>();
            listSex.Add(new ComboObject("true", Res.Get("Common_Label_Male")));
            listSex.Add(new ComboObject("false", Res.Get("Common_Label_Female")));
            ViewData[EmployeeProfileManagementConstant.LISTSEX] = new SelectList(listSex, "key", "value");

            //cboTrainingLevel: TrainingLevelBussiness.Search()    
            //IQueryable<TrainingLevel> lsTrainingLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } });
            List<TrainingLevel> lsTrainingLevel = this.getListOfTrainingLevel();
            ViewData[EmployeeProfileManagementConstant.LISTGRADUATIONLEVEL] = new SelectList(lsTrainingLevel, "TrainingLevelID", "Resolution");

            //cboGraduationLevel: Lấy danh sách từ GraduationLevelBussiness.Search(Dictionary) với Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel ->cho form create,edit
            //IQueryable<TrainingLevel> lsGraduationLevelUpdateOrEdit = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.Resolution);
            List<TrainingLevel> lsGraduationLevelUpdateOrEdit = this.getListOfTrainingLevel();
            ViewData[EmployeeProfileManagementConstant.LS_GRADUATIONLEVEL] = new SelectList(lsGraduationLevelUpdateOrEdit, "TrainingLevelID", "Resolution");

            //cboContractType: Lấy danh sách từ ContractTypeBusiness.Search()
            //IQueryable<ContractType> lsContractType = ContractTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution);
            List<ContractType> lsContractType = this.getListOfContractType();
            ViewData[EmployeeProfileManagementConstant.LISTCONTRACTTYPE] = new SelectList(lsContractType, "ContractTypeID", "Resolution");

            //cboWorkGroupType: Lấy danh sách từ WorkGroupTypeBussiness.Search()
            //IQueryable<WorkGroupType> lsWorkGroupType = WorkGroupTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution);
            List<WorkGroupType> lsWorkGroupType = this.getListOfWorkGroupType();
            ViewData[EmployeeProfileManagementConstant.LS_WORKGROUPTYPE] = new SelectList(lsWorkGroupType, "WorkGroupTypeID", "Resolution");

            //cboWorkType: Không có dữ liệu
            ViewData[EmployeeProfileManagementConstant.LS_WORKTYPE] = new SelectList(new string[] { });
            //var lstProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).Select(u => new { u.ProvinceID, u.ProvinceName }).OrderBy(o => o.ProvinceName).ToList();
            var lstProvince = this.getListOfProvince();
            ViewData[EmployeeProfileManagementConstant.LIST_PROVINCE] = new SelectList(lstProvince, "ProvinceID", "ProvinceName");

            if (global.IsCurrentYear)
            {
                ViewData[EmployeeProfileManagementConstant.IS_CURRENT_YEAR] = true;
            }
            else
            {
                ViewData[EmployeeProfileManagementConstant.IS_CURRENT_YEAR] = false;
            }
        }

        #endregion setViewData

        #region test image


        [ValidateAntiForgeryToken]
        public JsonResult SaveImage(IEnumerable<HttpPostedFileBase> ImageUploader)
        {
            GlobalInfo global = new GlobalInfo();

            if (ImageUploader == null || ImageUploader.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = ImageUploader.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> imageExtension = new List<string>();
                imageExtension.Add(".JPG");
                imageExtension.Add(".JPEG");
                imageExtension.Add(".PNG");
                imageExtension.Add(".BMP");
                imageExtension.Add(".ICO");
                imageExtension.Add(".GIF");
                var extension = Path.GetExtension(file.FileName);
                if (!imageExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ImageExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Photos"), fileName);
                file.SaveAs(physicalPath);
                Session["ImagePath"] = physicalPath;

                string relativePath = "/Uploads/Photos/" + fileName;
                JsonResult res = Json(new JsonMessage(fileName));
                res.ContentType = "text/plain";
                return res;
            }

            // Return an empty string to signify success
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }


        [ValidateAntiForgeryToken]
        public ActionResult RemoveImage(string[] fileNames)
        {
            //// The parameter of the Remove action must be called "fileNames"
            //foreach (var fullName in fileNames)
            //{
            //    var fileName = Path.GetFileName(fullName);
            //    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

            //    // TODO: Verify user permissions
            //    if (System.IO.File.Exists(physicalPath))
            //    {
            //        // The files are not actually removed in this demo
            //        System.IO.File.Delete(physicalPath);
            //    }
            //}

            // Return an empty string to signify success
            return Content("");
        }

        /// <summary>
        /// Function to get int array from a file
        /// </summary>
        /// <param name="_FileName">File name to get int array</param>
        /// <returns>Int32 Array</returns>
        public byte[] FileToByteArray(string _FileName)
        {
            return SMAS.Business.Common.UtilsBusiness.FileToByteArray(_FileName);
        }

        #endregion test image

        #region _GetcboWorkType

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetcboWorkType(int? GroupTypeId)
        {
            if (GroupTypeId.HasValue)
            {
                //Nếu cboWorkGroupType.Value <> 0 -> lấy danh sách từ WorkType Search(Dictionary)
                //với Dictionary[“WorkGroupTypeID”] = cboWorkGroupType.Value

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["WorkGroupTypeID"] = GroupTypeId.Value;
                IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(dic).OrderBy(o => o.Resolution);

                return Json(new SelectList(lsWorkType, "WorkTypeID", "Resolution"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }

        #endregion _GetcboWorkType

        #region _GetPrimarilyAssignedSubject

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetPrimarilyAssignedSubject(int? AppliedLevel)
        {
            if (AppliedLevel.HasValue)
            {
                IQueryable<SubjectCat> lsSubjectCat = SubjectCatBusiness.Search(new Dictionary<string, object>() { { "AppliedLevel", AppliedLevel }, { "IsActive", true } }).OrderBy(o => o.SubjectName);
                return Json(new SelectList(lsSubjectCat, "SubjectCatID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region EmployeeWorkingHistory

        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult CreateEmployeeWorkingHistory(int EmployeeProfileManagementViewModelID, int IsEmployeeWorkingHistory)
        {
            CheckPermissionForAction(EmployeeProfileManagementViewModelID, "Employee");
            GlobalInfo globalInfo = new GlobalInfo();
            var Permission2 = 0;
            if (IsEmployeeWorkingHistory == 1)
            {
                Permission2 = GetMenupermission("EmployeeProfile_ManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            else
            {
                Permission2 = GetMenupermission("EmployeeProfileManagement", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            if (Permission2 < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeWorkingHistory employeeworkinghistory = new EmployeeWorkingHistory();
            TryUpdateModel(employeeworkinghistory);
            Utils.Utils.TrimObject(employeeworkinghistory);

            //fix cứng
            employeeworkinghistory.EmployeeID = EmployeeProfileManagementViewModelID;
            //tu employeeid lay supervisingdeptid va schoolid tong vao object
            Employee temp = EmployeeBusiness.All.Where(o => (o.EmployeeID == employeeworkinghistory.EmployeeID)).FirstOrDefault();

            employeeworkinghistory.SchoolID = temp.SchoolID;
            employeeworkinghistory.SupervisingDeptID = temp.SupervisingDeptID;

            this.EmployeeWorkingHistoryBusiness.Insert(employeeworkinghistory);
            this.EmployeeWorkingHistoryBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }


        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult EditEmployeeWorkingHistory(int EmployeeProfileManagementViewModelID, int EmployeeWorkingHistoryID, int IsEmployeeWorkingHistory)
        {
            CheckPermissionForAction(EmployeeProfileManagementViewModelID, "Employee");
            GlobalInfo globalInfo = new GlobalInfo();
            var Permission = 0;
            if (IsEmployeeWorkingHistory == 1)
            {
                Permission = GetMenupermission("EmployeeProfile_ManagementForTeacher", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }
            else
            {
                Permission = GetMenupermission("EmployeeProfileManagement", globalInfo.UserAccountID, globalInfo.IsAdmin);
            }

            if (Permission < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            EmployeeWorkingHistory employeeworkinghistory = this.EmployeeWorkingHistoryBusiness.Find(EmployeeWorkingHistoryID);
            TryUpdateModel(employeeworkinghistory);
            Utils.Utils.TrimObject(employeeworkinghistory);

            //fix cứng
            employeeworkinghistory.EmployeeID = EmployeeProfileManagementViewModelID;

            //tu employeeid lay supervisingdeptid va schoolid tong vao object
            Employee temp = EmployeeBusiness.All.Where(o => (o.EmployeeID == employeeworkinghistory.EmployeeID)).FirstOrDefault();

            employeeworkinghistory.SchoolID = temp.SchoolID;
            employeeworkinghistory.SupervisingDeptID = temp.SupervisingDeptID;

            this.EmployeeWorkingHistoryBusiness.Update(employeeworkinghistory);
            this.EmployeeWorkingHistoryBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        public PartialViewResult SearchEmployeeWorkingHistory(int EmployeeID, int hdfIsEmployeeWorking, FormCollection fc)
        {
            GlobalInfo global = new GlobalInfo();
            //Nếu UserInfo.IsSchoolRole = TRUE: kiểm tra nếu Employee(EmployeeID).SchoolID <> UserInfo.SchoolID =>
            //Chuyển sang trang thông báo lỗi: “Bạn không có quyền truy cập vào dữ liệu này -Common_Label_HasHeadTeacherPermissionError
            Employee em = EmployeeBusiness.Find(EmployeeID);
            if (em.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                ViewData[EmployeeProfileManagementConstant.EMPLOYEE_WORKING] = "true";
            }
            else
            {
                ViewData[EmployeeProfileManagementConstant.EMPLOYEE_WORKING] = "false";
            }
            bool isPermiss = true;
            if (global.IsSchoolRole)
            {
                if (em.SchoolID.HasValue)
                {
                    if (em.SchoolID.Value != global.SchoolID.Value)
                    {
                        //throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
                        isPermiss &= false;
                    }
                    else
                    {
                        //throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
                        isPermiss &= true;
                    }
                }
            }
            else if (global.IsSuperVisingDeptRole)
            {
                if (em.SupervisingDeptID.HasValue)
                {
                    if (em.SupervisingDeptID.Value != global.SupervisingDeptID.Value)
                    {
                        //throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
                        isPermiss &= false;
                    }
                    else
                    {
                        //throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
                        isPermiss &= true;
                    }
                }
            }
            else
            {
                isPermiss &= false;
            }

            var Permission2 = -1;
            if (hdfIsEmployeeWorking == 1)
            {
                Permission2 = GetMenupermission("EmployeeProfile_ManagementForTeacher", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            }
            else
            {
                Permission2 = GetMenupermission("EmployeeProfileManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            }
            ViewData[EmployeeWorkingHistoryConstants.HAS_PERMISSION] = Permission2;

            ViewData[EmployeeWorkingHistoryConstants.IS_PERMISS] = isPermiss;
            ViewData[EmployeeProfileManagementConstant.IS_CURRENT_YEAR] = global.IsCurrentYear;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            //
            SearchInfo["EmployeeID"] = EmployeeID; //tam thoi cho fix cung id
            SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;

            IEnumerable<EmployeeWorkingHistoryViewModel> lst = this._SearchEmployeeWorkingHistory(SearchInfo);
            ViewData[EmployeeWorkingHistoryConstants.LIST_EMPLOYEEWORKINGHISTORY] = lst;

            //Get view data here
            ViewData["checkReadonly"] = false;
            return PartialView("../EmployeeWorkingHistory/_ListEmployeeWorkingHistory", lst);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult DeleteEmployeeWorkingHistory(int id, int hdfIsEmployee)
        {
            CheckPermissionForAction(id, "EmployeeWorkingHistory");
            GlobalInfo global = new GlobalInfo();
            var Permission2 = -1;
            if (hdfIsEmployee == 1)
            {
                Permission2 = GetMenupermission("EmployeeProfile_ManagementForTeacher", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            }
            else
            {
                Permission2 = GetMenupermission("EmployeeProfileManagement", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            }
            if (Permission2 < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            //Nếu UserInfo.IsSchoolRole = TRUE:
            //EmployeeWorkingHistoryBusiness.Delete(UserInfo.SchoolID, EmployeeWorkingHistoryID)
            if (global.IsSchoolRole)
            {
                this.EmployeeWorkingHistoryBusiness.Delete(global.SchoolID.Value, id);
                this.EmployeeWorkingHistoryBusiness.Save();
            }
            if (global.IsSuperVisingDeptRole)
            {
                this.EmployeeWorkingHistoryBusiness.Delete(global.SupervisingDeptID.Value, id);
                this.EmployeeWorkingHistoryBusiness.Save();
            }
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion EmployeeWorkingHistory

        #region LAY CAC DANH SACH CAN THIET TU CO SO DU LIEU VA LUU CACHE
        // TanND11, 20140828

        int CachingTime = 2; // Thoi gian cache du lieu

        /// <summary>
        /// Lay danh sach loai cong viec
        /// </summary>
        /// <returns></returns>
        public List<WorkType> getListOfWorkType()
        {
            string key = "CK_LISTWORKTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<WorkType> lsWorkType = this.WorkTypeBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsWorkType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsWorkType;
            }
            else
            {
                return value as List<WorkType>;
            }
        }
        public List<TrainingLevel> getListOfTrainingLevel()
        {
            string key = "CK_LISTGRADUATIONLEVEL";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<TrainingLevel> lsTrainingLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsTrainingLevel, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsTrainingLevel;
            }
            else
            {
                return value as List<TrainingLevel>;
            }
        }
        /// <summary>
        /// Danh sach loai hop dong
        /// </summary>
        /// <returns></returns>
        public List<ContractType> getListOfContractType()
        {
            string key = "CK_LISTCONTRACTTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<ContractType> lsContractType = ContractTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsContractType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsContractType;
            }
            else
            {
                return value as List<ContractType>;
            }
        }
        /// <summary>
        /// Lay danh sach nhom cong viec
        /// </summary>
        /// <returns></returns>
        public List<WorkGroupType> getListOfWorkGroupType()
        {
            string key = "CK_LS_WORKGROUPTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<WorkGroupType> lsWorkGroupType = WorkGroupTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsWorkGroupType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsWorkGroupType;
            }
            else
            {
                return value as List<WorkGroupType>;
            }
        }
        /// <summary>
        /// Lay danh sach cac tinh thanh
        /// </summary>
        /// <returns></returns>
        public List<Province> getListOfProvince()
        {
            string key = "CK_LIST_PROVINCE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<Province> lsProvince = ProvinceBusiness.Search(new Dictionary<string, object> { { "IsActive", true } }).OrderBy(o => o.ProvinceName).ToList();
                System.Web.HttpRuntime.Cache.Add(key, lsProvince, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return lsProvince;
            }
            else
            {
                return value as List<Province>;
            }
        }
        /// <summary>
        /// Lay danh sach cac dan toc
        /// </summary>
        /// <returns></returns>
        public List<Ethnic> getListOfEthnic()
        {
            string key = "CK_LS_ETHNIC";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<Ethnic> listEthnic = EthnicBusiness.Search(paras).OrderBy(o => o.EthnicName).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listEthnic, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listEthnic;
            }
            else
            {
                return value as List<Ethnic>;
            }
        }
        /// <summary>
        /// Lay danh sach cac ton giao
        /// </summary>
        /// <returns></returns>
        public List<Religion> getListOfReligion()
        {
            string key = "CK_LS_RELIGION";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<Religion> listReligion = ReligionBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listReligion, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listReligion;
            }
            else
            {
                return value as List<Religion>;
            }
        }
        public List<FamilyType> getListOfFamilyType()
        {
            string key = "CK_LS_FAMILYTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<FamilyType> listFamilyType = FamilyTypeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listFamilyType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listFamilyType;
            }
            else
            {
                return value as List<FamilyType>;
            }
        }

        public List<QualificationType> getListOfQualificationType()
        {
            string key = "CK_LS_QUALIFICATIONTYPE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<QualificationType> listQualificationType = QualificationTypeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listQualificationType, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listQualificationType;
            }
            else
            {
                return value as List<QualificationType>;
            }
        }
        public List<SpecialityCat> getListOfSpecialityCat()
        {
            string key = "CK_LS_SPECIALITYCAT";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<SpecialityCat> listSpecialityCat = SpecialityCatBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listSpecialityCat, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listSpecialityCat;
            }
            else
            {
                return value as List<SpecialityCat>;
            }
        }
        public List<QualificationLevel> getListOfQualificationLevel()
        {
            string key = "CK_LS_QUALIFICATIONLEVEL";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<QualificationLevel> listQualificationLevel = QualificationLevelBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listQualificationLevel, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listQualificationLevel;
            }
            else
            {
                return value as List<QualificationLevel>;
            }
        }
        public List<ForeignLanguageGrade> getListOfForeignLanguageGrade()
        {
            string key = "CK_LS_FOREIGNLANGUAGEGRADE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<ForeignLanguageGrade> listForeignLanguageGrade = ForeignLanguageGradeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listForeignLanguageGrade, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listForeignLanguageGrade;
            }
            else
            {
                return value as List<ForeignLanguageGrade>;
            }
        }
        public List<ITQualificationLevel> getListOfITQualificationLevel()
        {
            string key = "CK_LS_ITQUALIFICATIONLEVEL";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<ITQualificationLevel> listITQualificationLevel = ITQualificationLevelBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listITQualificationLevel, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listITQualificationLevel;
            }
            else
            {
                return value as List<ITQualificationLevel>;
            }
        }
        public List<StateManagementGrade> getListOfStateManagementGrade()
        {
            string key = "CK_LS_STATEMANAGEMENTGRADE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<StateManagementGrade> listStateManagementGrade = StateManagementGradeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listStateManagementGrade, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listStateManagementGrade;
            }
            else
            {
                return value as List<StateManagementGrade>;
            }
        }
        public List<PoliticalGrade> getListOfPoliticalGrade()
        {
            string key = "CK_LS_POLITICALGRADE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<PoliticalGrade> listPoliticalGrade = PoliticalGradeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listPoliticalGrade, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listPoliticalGrade;
            }
            else
            {
                return value as List<PoliticalGrade>;
            }
        }
        public List<EducationalManagementGrade> getListOfEducationalManagementGrade()
        {
            string key = "CK_LS_EDUCATIONALMANAGEMENTGRADE";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<EducationalManagementGrade> listEducationalManagementGrade = EducationalManagementGradeBusiness.Search(paras).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listEducationalManagementGrade, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listEducationalManagementGrade;
            }
            else
            {
                return value as List<EducationalManagementGrade>;
            }
        }

        public List<Contract> getListOfContract()
        {
            string key = "CK_LS_CONTRACT";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<Contract> listContract = ContractBusiness.All.Where(em => (em.IsActive == true)).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listContract, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listContract;
            }
            else
            {
                return value as List<Contract>;
            }
        }
        public List<StaffPosition> getListOfStaffPosition()
        {
            string key = "CK_LS_STAFFPOSITION";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                List<StaffPosition> listStaffPosition = StaffPositionBusiness.All.Where(em => (em.IsActive == true)).OrderBy(o => o.Resolution).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listStaffPosition, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listStaffPosition;
            }
            else
            {
                return value as List<StaffPosition>;
            }
        }
        public List<SupervisingDept> getListOfSupervisingDept()
        {
            string key = "CK_LS_SUPERVISINGDEPT";
            object value = System.Web.HttpRuntime.Cache.Get(key);
            if (value == null)
            {
                IDictionary<string, object> paras = new Dictionary<string, object> { { "IsActive", true } };
                List<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.Search(paras).OrderBy(o => o.SupervisingDeptName).ToList();
                System.Web.HttpRuntime.Cache.Add(key, listSupervisingDept, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, CachingTime, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                return listSupervisingDept;
            }
            else
            {
                return value as List<SupervisingDept>;
            }
        }

        private List<ColumnDescriptionModel> GetColumnDescription(int profileTemplateId)
        {
            ProfileTemplate profileTemplate = ProfileTemplateBusiness.Find(profileTemplateId);
            List<EmployeeColumnTemplateBO> lstTemplate = new List<EmployeeColumnTemplateBO>();
            if (profileTemplate != null && !string.IsNullOrEmpty(profileTemplate.ColDescriptionIds))
            {
                lstTemplate = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeColumnTemplateBO>>(profileTemplate.ColDescriptionIds);
            }
            List<ColumnDescription> lstColumnDescription = ColumnDescriptionBusiness.All
                                        .Where(x=> x.ReportType == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_EMPLOYEE_PROFILE)
                                        .OrderBy(c => c.ColumnIndex).ToList();
            List<ColumnDescriptionModel> lstColumnModel = new List<ColumnDescriptionModel>();
            VTExport export = new VTExport();
            for (int i = 0; i < lstColumnDescription.Count; i++)
            {

                ColumnDescription column = lstColumnDescription[i];
                ColumnDescriptionModel objColumnModel = new ColumnDescriptionModel
                {
                    ColumnDescriptionId = column.ColumnDescriptionId,
                    ColumnName = column.ColumnName,
                    //ColumnOrder = column.ColumnOrder,
                    RequiredColumn = column.RequiredColumn,
                    Resolution = column.Resolution,
                };
                EmployeeColumnTemplateBO employeeColumnTemplateBO = lstTemplate.FirstOrDefault(s => s.ColDesId == column.ColumnDescriptionId);
                if (employeeColumnTemplateBO != null)
                {
                    VTVector vTVector = new VTVector(employeeColumnTemplateBO.ColExcel + "1");
                    objColumnModel.ExcelName = employeeColumnTemplateBO.ExcelName;
                    objColumnModel.ColExcel = employeeColumnTemplateBO.ColExcel;
                    objColumnModel.CheckValue = true;
                    objColumnModel.ColumnIndex = vTVector.Y;
                }
                else
                {
                    string columnExcel = VTVector.ColumnIntToString(column.ColumnIndex);
                    objColumnModel.ColExcel = columnExcel;
                    objColumnModel.ExcelName = column.Resolution;
                    objColumnModel.CheckValue = false;
                    objColumnModel.ColumnIndex = column.ColumnIndex;

                }
                lstColumnModel.Add(objColumnModel);
            }
            lstColumnModel = lstColumnModel.OrderBy(s => s.ColumnIndex).ToList();
            return lstColumnModel;

        }
        #endregion

        #region Template excel

        private void SetProfileTemplate(int profileTemplateId)
        {
            int schoolId = _globalInfo.SchoolID ?? 0;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("TemplateID", profileTemplateId);
            dic.Add("SchoolID", schoolId);
            dic.Add("ReportType", SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_EMPLOYEE_PROFILE);   

            List<ProfileTemplate> lstProfileTemplate = ProfileTemplateBusiness.Search(dic).ToList();
            if (profileTemplateId == 0)
            {
                if (lstProfileTemplate == null)
                {
                    lstProfileTemplate = new List<ProfileTemplate>();
                }
                lstProfileTemplate.Insert(0, new ProfileTemplate { ProfileTemplateName = "[Thêm mới]", ProfileTemplateId = 0 });
            }
            int defaultId = 0;
            ProfileTemplate objProfileTempOfSchool = lstProfileTemplate.FirstOrDefault(s => s.SchoolId == schoolId);
            if (objProfileTempOfSchool != null)
            {
                defaultId = objProfileTempOfSchool.ProfileTemplateId;
            }
            else
            {
                ProfileTemplate objProfileTemp = lstProfileTemplate.FirstOrDefault(p => p.ProfileTemplateId > 0);
                if (objProfileTemp != null)
                {
                    defaultId = objProfileTemp.ProfileTemplateId;
                }
            }
            ViewData[EmployeeProfileManagementConstant.LIST_PROFILE_TEMPLATE] = new SelectList(lstProfileTemplate, "ProfileTemplateId", "ProfileTemplateName", defaultId);
        }

        private void SetRowExcel()
        {
            List<RowExcelModel> lstRow = new List<RowExcelModel>();
            for (int i = 1; i <= 20; i++)
            {
                lstRow.Add(new RowExcelModel { RowId = i, RowName = i.ToString() });
            }

            ViewData[EmployeeProfileManagementConstant.LIST_ROW_EXCEL] = new SelectList(lstRow, "RowId", "RowName", 1);
        }
        [HttpPost]
        public PartialViewResult LoadTemplate(int id)
        {
            SetProfileTemplate(id);
            SetRowExcel();
            List<ColumnDescriptionModel> lstVal = GetColumnDescription(id);
            ViewData[EmployeeProfileManagementConstant.LIST_COLUMN_TEMPLATE] = lstVal;
            return PartialView("_ListTemplate");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveExcelTemplate(FormCollection frm)
        {
            int templapteId = Convert.ToInt32(frm["ProfileTemplateId"]);
            string templateName = frm["ProfileTemplateName"];
            if (string.IsNullOrEmpty(templateName))
            {
                return Json(new JsonMessage("Thầy/cô chưa nhập tên mẫu", JsonMessage.ERROR));
            }

            ProfileTemplate profileTemplate = ProfileTemplateBusiness.Find(templapteId);

            bool existstName = ProfileTemplateBusiness.All.Any(s => s.ProfileTemplateName.ToLower().Contains(templateName.ToLower()) && s.ReportType == 1);
            if (existstName && templapteId==0)
            {
                return Json(new JsonMessage("Tên mẫu đã được khai báo", JsonMessage.ERROR));
            }

            if(profileTemplate!=null && profileTemplate.SchoolId == 0)
            {
                return Json(new JsonMessage("Thầy/cô không được sửa mẫu của hệ thống", JsonMessage.ERROR));
            }

            List<ColumnDescription> lstColumnDescription = ColumnDescriptionBusiness.All
                                                            .Where(x => x.ReportType == SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_EMPLOYEE_PROFILE)
                                                            .OrderBy(s => s.ColumnIndex).ToList();
            List<string> lstError = new List<string>();
            List<EmployeeColumnTemplateBO> listTemplateBO = new List<EmployeeColumnTemplateBO>();
            for (int i = 0; i < lstColumnDescription.Count; i++)
            {
                ColumnDescription columnDescription = lstColumnDescription[i];
                string chk = frm["chk_" + columnDescription.ColumnDescriptionId];
                if ((chk == null || chk != "on") && !columnDescription.RequiredColumn)
                {
                    continue;
                }
                string excelName = frm["ExcelName_" + columnDescription.ColumnDescriptionId];
                string colExcel = frm["ColExcel_" + columnDescription.ColumnDescriptionId];

                EmployeeColumnTemplateBO objTemplateBO = new EmployeeColumnTemplateBO
                {
                    ColDesId = columnDescription.ColumnDescriptionId,
                    ColExcel = colExcel,
                    ExcelName = excelName
                };

                if (string.IsNullOrEmpty(colExcel))
                {
                    lstError.Add("Chưa nhập cột excel");
                    continue;
                }

                if (string.IsNullOrEmpty(excelName))
                {
                    lstError.Add("Chưa nhập tên cột excel");
                    continue;
                }

                VTVector vTVector = new VTVector(objTemplateBO.ColExcel + "1");
                if (vTVector.X == 0)
                {
                    lstError.Add(String.Format("Tên cột excel {0} không hợp lệ", objTemplateBO.ColExcel));
                    continue;
                }
                if (listTemplateBO.Any(s => s.ColExcel == objTemplateBO.ColExcel))
                {
                    lstError.Add(String.Format("Tên cột excel {0} đã được sử dụng", objTemplateBO.ColExcel));
                    continue;
                }

                listTemplateBO.Add(objTemplateBO);
            }

            //Validate cot excel;
            if (lstError.Count > 0)
            {
                string msg = string.Join(",", lstError);
                return Json(new JsonMessage(msg, JsonMessage.ERROR));
            }
            if (templapteId > 0)
            {
                profileTemplate.ProfileTemplateName = templateName;
                profileTemplate.ColDescriptionIds = Newtonsoft.Json.JsonConvert.SerializeObject(listTemplateBO);
                profileTemplate.ModifiedDate = DateTime.Now;
                profileTemplate.SchoolId = _globalInfo.SchoolID.Value;
                profileTemplate.ReportType = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_EMPLOYEE_PROFILE;
                ProfileTemplateBusiness.Update(profileTemplate);
                ProfileTemplateBusiness.Save();
                //Common_Label_AddNewMessage
                return Json(new JsonMessage(Res.Get("Cập nhật thành công")));
            }
            else
            {
                ProfileTemplate profileTemplateInsert = new ProfileTemplate
                {
                    ProfileTemplateName = templateName,
                    SchoolId = _globalInfo.SchoolID.Value,
                    ColDescriptionIds = Newtonsoft.Json.JsonConvert.SerializeObject(listTemplateBO),
                    ReportType = SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_EMPLOYEE_PROFILE,
                    CreatedDate = DateTime.Now,
                };
                
                int length = profileTemplateInsert.ColDescriptionIds.Length;
                ProfileTemplateBusiness.Insert(profileTemplateInsert);
                ProfileTemplateBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
        }

        [HttpPost]
        public JsonResult DeleteTemplate(int profileTemplateId)
        {
            int schoolId = _globalInfo.SchoolID ?? 0;
            ProfileTemplate profile = ProfileTemplateBusiness.Find(profileTemplateId);
            if (profile == null)
            {
                return Json(new JsonMessage("Mẫu excel không hợp lệ", JsonMessage.ERROR));
            }
            if (profile.SchoolId == 0)
            {
                return Json(new JsonMessage("Thầy/cô không xóa được mẫu excel của hệ thống", JsonMessage.ERROR));
            }
            ProfileTemplateBusiness.Delete(profileTemplateId);
            ProfileTemplateBusiness.Save();
            return Json(new JsonMessage(Res.Get("Xóa mẫu excel thành công")));
        }

        public JsonResult ReLoadTemplate()
        {
            int schoolId = _globalInfo.SchoolID ?? 0;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("SchoolID", schoolId);
            dic.Add("ReportType", SMAS.Business.Common.GlobalConstants.REPORT_TYPE_OF_EMPLOYEE_PROFILE);          
            List<ProfileTemplate> lstProfileTemplate = ProfileTemplateBusiness.Search(dic).ToList();

            if (lstProfileTemplate == null)
            {
                lstProfileTemplate = new List<ProfileTemplate>();
            }
            lstProfileTemplate.Insert(0, new ProfileTemplate { ProfileTemplateName = "[Thêm mới]", ProfileTemplateId = 0 });


            int defaultId = 0;
            ProfileTemplate objProfileTempOfSchool = lstProfileTemplate.FirstOrDefault(s => s.SchoolId == schoolId);
            if (objProfileTempOfSchool != null)
            {
                defaultId = objProfileTempOfSchool.ProfileTemplateId;
            }
            else
            {
                ProfileTemplate objProfileTemp = lstProfileTemplate.FirstOrDefault(p => p.ProfileTemplateId > 0);
                if (objProfileTemp != null)
                {
                    defaultId = objProfileTemp.ProfileTemplateId;
                }
            }

            return Json(new SelectList(lstProfileTemplate, "ProfileTemplateId", "ProfileTemplateName", defaultId));
        }

        public FileResult ExportExcelBGD(int profileTemplateId, int rowId)
        {

            Stream excel = ProfileTemplateBusiness.ExportEmployeeInfo(_globalInfo.SchoolID.Value,_globalInfo.AcademicYearID.Value, profileTemplateId, rowId);
            // Fix chrome file type
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            SchoolProfile sp = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
            result.FileDownloadName = ReportUtils.StripVNSign(string.Format(TEMPLATE_EMPLOYEE_NAME, "_" + sp.SchoolName));


            return result;
        }
        #endregion
    }
}
