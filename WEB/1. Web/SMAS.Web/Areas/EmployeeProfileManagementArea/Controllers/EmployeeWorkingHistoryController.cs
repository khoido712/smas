﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.EmployeeProfileManagementArea.Controllers
{
    public class EmployeeWorkingHistoryController : BaseController
    {
        private readonly IEmployeeWorkingHistoryBusiness EmployeeWorkingHistoryBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;

        public EmployeeWorkingHistoryController(IEmployeeWorkingHistoryBusiness employeeworkinghistoryBusiness
            , IEmployeeBusiness EmployeeBusiness)
        {
            this.EmployeeWorkingHistoryBusiness = employeeworkinghistoryBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }

        //
        // GET: /EmployeeWorkingHistory/

        //public ActionResult Index(FormCollection fc)
        //{
        //    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

        //    //Hệ thống gọi hàm Search trong nghiệp vụ EmployeeWorkHistory và
        //    //truyền vào các tham số của hệ thống bao gồm SchoolID, EmployeeID
        //    SearchInfo["EmployeeID"] = fc["EmployeeID"]; //tam thoi cho fix cung id
        //    SearchInfo["SchoolID"] = new GlobalInfo().SchoolID;
        //    //
        //    IEnumerable<EmployeeWorkingHistoryViewModel> lst = this._Search(SearchInfo);
        //    ViewData[EmployeeWorkingHistoryConstants.LIST_EMPLOYEEWORKINGHISTORY] = lst;
        //    return View("IndexEmployeeWorkingHistory");
        //}

        //
        // GET: /EmployeeWorkingHistory/Search

        #region da chuyen sang controller EmployeeProfileManagement

        ///// <summary>
        ///// Create
        ///// </summary>
        ///// <returns></returns>
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public JsonResult CreateEmployeeWorkingHistory(int EmployeeProfileManagementViewModelID)
        //{
        //    EmployeeWorkingHistory employeeworkinghistory = new EmployeeWorkingHistory();
        //    TryUpdateModel(employeeworkinghistory);
        //    Utils.Utils.TrimObject(employeeworkinghistory);

        //    //fix cứng
        //    employeeworkinghistory.EmployeeID = EmployeeProfileManagementViewModelID;
        //    //tu employeeid lay supervisingdeptid va schoolid tong vao object
        //    Employee temp = EmployeeBusiness.All.Where(o => (o.EmployeeID == employeeworkinghistory.EmployeeID)).FirstOrDefault();
        //    employeeworkinghistory.SchoolID = temp.SchoolID;
        //    employeeworkinghistory.SupervisingDeptID = temp.SupervisingDeptID;

        //    this.EmployeeWorkingHistoryBusiness.Insert(employeeworkinghistory);
        //    this.EmployeeWorkingHistoryBusiness.Save();

        //    return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        //}

        ///// <summary>
        ///// Update
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public JsonResult EditEmployeeWorkingHistory(int EmployeeProfileManagementViewModelID, int EmployeeWorkingHistoryID)
        //{
        //    EmployeeWorkingHistory employeeworkinghistory = this.EmployeeWorkingHistoryBusiness.Find(EmployeeWorkingHistoryID);
        //    TryUpdateModel(employeeworkinghistory);
        //    Utils.Utils.TrimObject(employeeworkinghistory);
        //    //fix cứng
        //    employeeworkinghistory.EmployeeID = EmployeeProfileManagementViewModelID;
        //    //tu employeeid lay supervisingdeptid va schoolid tong vao object
        //    Employee temp = EmployeeBusiness.All.Where(o => (o.EmployeeID == employeeworkinghistory.EmployeeID)).FirstOrDefault();
        //    employeeworkinghistory.SchoolID = temp.SchoolID;
        //    employeeworkinghistory.SupervisingDeptID = temp.SupervisingDeptID;

        //    this.EmployeeWorkingHistoryBusiness.Update(employeeworkinghistory);
        //    this.EmployeeWorkingHistoryBusiness.Save();

        //    return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        //}

        ///// <summary>
        ///// Delete
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public JsonResult DeleteEmployeeWorkingHistory(int id)
        //{
        //    GlobalInfo global = new GlobalInfo();
        //    //Nếu UserInfo.IsSchoolRole = TRUE:
        //    //EmployeeWorkingHistoryBusiness.Delete(UserInfo.SchoolID, EmployeeWorkingHistoryID)
        //    if (global.IsSchoolRole)
        //    {
        //        this.EmployeeWorkingHistoryBusiness.Delete(global.SchoolID.Value, id);
        //        this.EmployeeWorkingHistoryBusiness.Save();
        //    }
        //    if (global.IsSuperVisingDeptRole)
        //    {
        //        this.EmployeeWorkingHistoryBusiness.Delete(global.SupervisingDeptID.Value, id);
        //        this.EmployeeWorkingHistoryBusiness.Save();
        //    }
        //    return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        //}

        ///// <summary>
        ///// Search
        ///// </summary>
        ///// <param name="SearchInfo"></param>
        ///// <returns></returns>
        //private IEnumerable<EmployeeWorkingHistoryViewModel> _SearchEmployeeWorkingHistory(IDictionary<string, object> SearchInfo)
        //{
        //    IQueryable<EmployeeWorkingHistory> query = this.EmployeeWorkingHistoryBusiness.Search(SearchInfo);
        //    IQueryable<EmployeeWorkingHistoryViewModel> lst = query.Select(o => new EmployeeWorkingHistoryViewModel
        //    {
        //        EmployeeWorkingHistoryID = o.EmployeeWorkingHistoryID,
        //        EmployeeID = o.EmployeeID,
        //        SupervisingDeptID = o.SupervisingDeptID,
        //        SchoolID = o.SchoolID,
        //        Organization = o.Organization,
        //        Department = o.Department,
        //        Position = o.Position,
        //        Resolution = o.Resolution,
        //        FromDate = o.FromDate,
        //        ToDate = o.ToDate

        //    });

        //    List<EmployeeWorkingHistoryViewModel> lsEmployeeWorkingHistoryViewModel = lst.ToList();
        //    //foreach (EmployeeWorkingHistoryViewModel item in lsEmployeeWorkingHistoryViewModel)
        //    //{
        //    //     Employee temp = EmployeeBusiness.All.Where(o=>(o.EmployeeID==item.EmployeeID)).FirstOrDefault();
        //    //     item.EmployeeName = temp.FullName;
        //    //}

        //    return lsEmployeeWorkingHistoryViewModel;
        //}

        #endregion da chuyen sang controller EmployeeProfileManagement
    }
}