/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.EmployeeProfileManagementArea
{
    public class EmployeeProfileManagementConstant
    {
        public const string LIST_EMPLOYEE = "listEmployee";
		public const string LIST_PROVINCE = "LIST_PROVINCE";
        public const string LIST_DISTRICT = "LIST_DISTRICT";
        public const string LIST_COMMUNE = "LIST_COMMUNE";
        public const string LISTSCHOOLFACULTY = "LISTSCHOOLFACULTY";
        public const string LISTWORKTYPE = "LISTWORKTYPE";
        public const string LISTEMPLOYMENTSTATUS = "LISTEMPLOYMENTSTATUS";
        public const string LISTSEX = "LISTSEX";
        public const string LISTGRADUATIONLEVEL = "LISTGRADUATIONLEVEL";
        public const string LISTCONTRACTTYPE = "LISTCONTRACTTYPE";
        public const string LIST_CONCURRENT_WORK_TYPE = "LIST_CONCURRENT_WORK_TYPE";

        public const string LS_RELIGION = "LISTRELIGION";
        public const string LS_ETHNIC = "LISTETHNIC";
        public const string LS_FAMILYTYPE = "LISTFAMILYTYPE";
        public const string LS_QUALIFICATIONTYPE = "LISTQUALIFICATIONTYPE";

        public const string LS_SPECIALITYCAT = "LISTSPECIALITYCAT";
        public const string LS_QUALIFICATIONLEVEL = "LISTQUALIFICATIONLEVEL";
        public const string LS_FOREIGNLANGUAGEGRADE = "LISTFOREIGNLANGUAGEGRADE";
        public const string LS_FOREIGNLANGUAGECAT = "LS_FOREIGNLANGUAGECAT";

        public const string LS_ITQUALIFICATIONLEVEL = "LISTITQUALIFICATIONLEVEL";
        public const string LS_STATEMANAGEMENTGRADE = "LISTSTATEMANAGEMENTGRADE";

        public const string LS_POLITICALGRADE = "LISTPOLITICALGRADE";

        public const string LS_EDUCATIONALMANAGEMENTGRADE = "LISTEDUCATIONALMANAGEMENTGRADE";

        public const string LS_CONTRACT = "LISTCONTRACT";

        public const string LS_STAFFPOSITION = "LISTSTAFFPOSITION";

        public const string LS_SUPERVISINGDEPT = "LISTSUPERVISINGDEPT";

        public const string LS_GROUPCAT = "LISTGROUPCAT";

        public const string LS_GRADUATIONLEVEL = "LS_GRADUATIONLEVEL";

        public const string LS_WORKGROUPTYPE = "LS_WORKGROUPTYPE";

        public const string LS_WORKTYPE = "LS_WORKTYPE";

        public const string LS_PRIMARILYASSIGNEDSUBJECT = "LS_PRIMARILYASSIGNEDSUBJECT";

        public const int EMPLOYMENT_STATUS_WORKING = 1;
        public const int EMPLOYMENT_STATUS_WORK_CHANGED = 2;
        public const int EMPLOYMENT_STATUS_RETIRED = 3;
        public const int EMPLOYMENT_STATUS_SEVERANCE = 5;
        public const int EMPLOYMENT_STATUS_BREATHER = 6;

        public const string CHOSENEMPLOYEE = "CHOSENEMPLOYEE";

        public const string LS_APPLIEDLEVEL = "LS_APPLIEDLEVEL";

        public const string AUTO_GEN_CODE_CHECK = "AUTO_GEN_CODE_CHECK";
        public const string AUTO_GEN_CODE_CHECK_AND_NO_DISABLE = "AUTO_GEN_CODE_CHECK_AND_NO_DISABLE";
        public const string AUTO_GEN_CODE_CHECK_AND_DISABLE = "AUTO_GEN_CODE_CHECK_AND_DISABLE";
        public const string IS_SCHOOL_MODIFIED = "IS_SCHOOL_MODIFIED";
        public const string IS_CURRENT_YEAR = "IS_CURRENT_YEAR";
        public const string HAS_PERMISSION = "HAS_PERMISSION";
        public const string EMPLOYEE_WORKING = "EMPLOYEE_WORKING";
        public const string Has_Permission_TeachingAssignment = "Has_Permission_TeachingAssignment";
        public const string FORBACK = "FORBACK";

        public const string EMPLOYEE_TOTAL = "total employee";
        public const string PAGE_NUMBER = "position of page";
        public const string ENABLE_BACKUP = "ENABLE_BACKUP";
        public const string IS_SUPERVISINGDEPT = "IS_SUPERVISINGDEPT";

        public const string LIST_COLUMN_DESCRIPTION = "LIST_COLUMN_DESCRIPTION";
        public const string IS_EDIT = "isEdit";

        public const string LIST_PROFILE_TEMPLATE = "LIST_PROFILE_TEMPLATE";
        public const string LIST_COLUMN_TEMPLATE = "LIST_COLUMN_TEMPLATE";
        public const string LIST_ROW_EXCEL = "LIST_ROW_EXCEL";

        public const string IS_EMPLOYEE = "isEmployee";

    }
}
