﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.EmployeeProfileManagementArea
{
    public class EmployeeQualificationConstants
    {
        public const string LIST_EMPLOYEEQUALIFICATION = "listEmployeeQualification";
        public const string LIST_GRADUATIONLEVEL = "listGraduationLevel";
        public const string LIST_SPECIALITYCAT = "listSpecialityCat";
        public const string LIST_QUALIFICATIONTYPE = "listQualificationType";
        public const string LIST_ISREQUALIFIED = "listIsRequalified";
        public const string LIST_QUALIFICATIONGRADE = "listQualificationGrade";
        public const string FIRST_TRAINING_VAL = "false";
        public const string RE_TRAINING_VAL = "true";
        public const string HAS_PERMISSION = "HAS_PERMISSION";
    }
}