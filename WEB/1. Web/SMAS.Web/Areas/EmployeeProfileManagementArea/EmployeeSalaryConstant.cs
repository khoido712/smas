/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.EmployeeProfileManagementArea
{
    public class EmployeeSalaryConstants
    {
        public const string LIST_EMPLOYEESALARY = "listEmployeeSalary";
        public const string LIST_SALARYSCALE = "listSalaryScale";
        public const string LIST_SALARYLEVEL = "listSalaryLevel";
        public const string LIST_COFFICIENT = "listCoefficient";
        public const int MAX_GRADE = 12;
        public const string HAS_PERMISSION = "HAS_PERMISSION";
    }
}