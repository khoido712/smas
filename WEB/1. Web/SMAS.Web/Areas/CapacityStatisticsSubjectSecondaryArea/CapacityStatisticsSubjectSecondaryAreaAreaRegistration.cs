﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.CapacityStatisticsSubjectSecondaryArea
{
    public class CapacityStatisticsSubjectSecondaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CapacityStatisticsSubjectSecondaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CapacityStatisticsSubjectSecondaryArea_default",
                "CapacityStatisticsSubjectSecondaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
