﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.CapacityStatisticsSubjectSecondaryArea
{
    public class CapacityStatisticsSubjectSecondaryConstants
    {
        public const string LIST_MARKSTATISTICS = "LIST_MARKSTATISTICS";
        public const string LIST_DISTRICT = "LIST_DISTRICT";
        public const string LIST_AcademicYear = "LIST_AcademicYear";
        public const string LIST_EducationLevel = "LIST_EducationLevel";
        public const string LIST_TrainingType = "LIST_TrainingType";
        public const string LIST_Semester = "LIST_Semmester";
        public const string LIST_Subject = "LIST_Subject";
        public const string DisableDistrict = "DisableDistrict";
        public const string GRID_REPORT_ID = "grid_report_id";
        public const bool Show_List = false;
    }
}