﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.CapacityStatisticsSubjectSecondaryArea.Models
{
    public class CapacityStatisticsSubjectViewModel
    {
        [ResourceDisplayName("MarkStatistics_Label_SchoolName")]
        public string SchoolName { get; set; }
        [ResourceDisplayName("MarkStatistics_Label_SentDate")]
        public DateTime? SentDate { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_TotalExcellent")]
        public int? TotalExcellent { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_TotalGood")]
        public int? TotalGood { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_TotalNormal")]
        public int? TotalNormal { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_TotalWeak")]
        public int? TotalWeak { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_TotalPoor")]
        public int? TotalPoor { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_TotalPass")]
        public int? TotalPass { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_TotalFail")]
        public int? TotalFail { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_PercentExcellent")]
        public double? PercentExcellent { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_PercentGood")]
        public double? PercentGood { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_PercentNormal")]
        public double? PercentNormal { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_PercentWeak")]
        public double? PercentWeak { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_PercentPoor")]
        public double? PercentPoor { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_PercentPass")]
        public double? PercentPass { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_PercentFail")]
        public double? PercentFail { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_TotalPupil")]
        public int? TotalPupil { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_TotalAboveAverage")]
        public int? TotalAboveAverage { get; set; }
        [ResourceDisplayName("CapacityStatistics_Label_PercentAboveAverage")]
        public double? PercentAboveAverage { get; set; }
        public int? SchoolID { get; set; }
    }
}