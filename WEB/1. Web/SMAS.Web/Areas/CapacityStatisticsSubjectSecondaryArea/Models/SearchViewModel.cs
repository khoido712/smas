﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Areas.CapacityStatisticsSubjectSecondaryArea;
using Resources;

namespace SMAS.Web.Areas.CapacityStatisticsSubjectSecondaryArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("MarkStatistics_Label_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectSecondaryConstants.LIST_DISTRICT)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int? DistrictID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_AcademicYear")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectSecondaryConstants.LIST_AcademicYear)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int? AcademicYearID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectSecondaryConstants.LIST_EducationLevel)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_TrainingType")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectSecondaryConstants.LIST_TrainingType)]
        [AdditionalMetadata("Placeholder", "All")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int? TrainingTypeID { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_Semmeter")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectSecondaryConstants.LIST_Semester)]
        [AdditionalMetadata("Placeholder", "null")]
        [AdditionalMetadata("OnChange", "AjaxLoadSubject(this)")]
        public int? Semester { get; set; }

        [ResourceDisplayName("MarkStatistics_Label_Subject")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", CapacityStatisticsSubjectSecondaryConstants.LIST_Subject)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")] 
        public int SubjectID { get; set; }
    }
}