﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;
namespace SMAS.Web.Areas.PupilRankingNewArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class PupilRankingNewController : BaseController
    {
        private readonly IConductLevelBusiness ConductLevelBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public PupilRankingNewController(IConductLevelBusiness conductLevelBusiness,
                                    IPeriodDeclarationBusiness PeriodDeclarationBusiness,
                                    IClassProfileBusiness ClassProfileBusiness,
                                    IAcademicYearBusiness AcademicYearBusiness)
        {
            
            this.ConductLevelBusiness = conductLevelBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }

        //
        // GET: /PupilRankingNewArea/PupilRankingNew/

        public ActionResult Index(int? ClassID)
        {
            return View(ClassID);
        }

        public ActionResult _index(int? ClassID)
        {
            SetViewData(ClassID);
            return View();
        }

        /// <summary>
        /// Ajax load class
        /// </summary>
        /// <returns></returns>
         [HttpPost]
        public PartialViewResult AjaxLoadClass()
        {
            var r = Request;
            int Semester = string.IsNullOrWhiteSpace(r["semesterid"]) ? 0 : int.Parse(r["semesterid"]);
            int AppliedLevel = string.IsNullOrWhiteSpace(r["appliedlevelid"]) ? 0 : int.Parse(r["appliedlevelid"]);
            int classid = string.IsNullOrWhiteSpace(r["classid"]) ? 0 : int.Parse(r["classid"]);
            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();

            int i = 0;
            foreach (EducationLevel item in _globalInfo.EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[PupilRankingNewConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["Semester"] = Semester;
            IEnumerable<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();
            List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
            if (lstPeriod != null && lstPeriod.Count() > 0)
            {
                foreach (var item in lstPeriod)
                {
                    PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                    periodDeclaration = item;
                    if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                        periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + periodDeclaration.FromDate.Value.ToShortDateString() + " - " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";

                    listPeriodDeclaration.Add(periodDeclaration);
                }
            }
            int period = 0;
            if (listPeriodDeclaration != null && listPeriodDeclaration.Count() != 0)
            {
                period = listPeriodDeclaration.FirstOrDefault().PeriodDeclarationID;
            }
            foreach (PeriodDeclaration pd in listPeriodDeclaration)
            {
                if (DateTime.Now >= pd.FromDate && DateTime.Now <= pd.EndDate)
                {
                    period = pd.PeriodDeclarationID;
                }
            }
            ViewData[PupilRankingNewConstants.LIST_PERIOD] = new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution", period);
            ViewData[PupilRankingNewConstants.LIST_APPLIEDLEVEL] = new GlobalInfo().AppliedLevel;

            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

                Dictionary<string, object> dictionary = new Dictionary<string, object> {
                {"AcademicYearID", _globalInfo.AcademicYearID},
                {"AppliedLevel", AppliedLevel}
                // {"Semester", Semester} // Phân quyền ko phân biệt kỳ - Bug 15008      
            };
                AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.GetValueOrDefault());
                if (!academicYear.IsTeacherCanViewAll && !_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                {
                    dictionary["UserAccountID"] = _globalInfo.UserAccountID;
                    dictionary["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                List<ClassProfile> listCP = ClassProfileBusiness.SearchBySchool(
                    _globalInfo.SchoolID.GetValueOrDefault(), dictionary)
                    .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[PupilRankingNewConstants.LIST_CLASS] = listCP;

                // Lớp đc chọn đầu tiên
                ClassProfile cp = null;
                if (listCP != null && listCP.Count > 0)
                {
                    var lcp = listCP.Where(o => o.ClassProfileID == classid);
                    //Tính ra lớp cần được chọn đầu tiên
                    if (lcp != null && lcp.Count() > 0)
                    {
                        cp = ClassProfileBusiness.Find(classid);
                    }
                    else
                    {
                        cp = listCP.First();

                        if (!_globalInfo.IsAdminSchoolRole)
                        {

                            dictionary["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                            List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(
                                _globalInfo.SchoolID.GetValueOrDefault(), dictionary)
                                .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listHead != null && listHead.Count > 0)
                            {
                                cp = listHead.First();
                            }
                            else
                            {
                                dictionary["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                                List<ClassProfile> listOverSee = ClassProfileBusiness.SearchBySchool(
                                    _globalInfo.SchoolID.GetValueOrDefault(), dictionary)
                                    .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listOverSee != null && listOverSee.Count > 0)
                                {
                                    cp = listOverSee.First();
                                }
                            }
                        }
                    }
                }
                ViewData["ClassProfile"] = cp;
            }
            return PartialView("_ListClass");
        }

        #region SetViewData
        private void SetViewData(int? ClassID)
        {
            bool semester1 = _globalInfo.Semester.Value == 1 ? true : false;
            bool semester2 = _globalInfo.Semester.Value == 2 ? true : false;
            bool semester3 = false;
            if (!semester1 && !semester2)
                semester3 = true;
          
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("ConductRanking_Label_SemesterFirst"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("ConductRanking_Label_SemesterSecond"), 2, semester2, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("ConductRanking_Label_SemesterAllOfYear"), 3, semester3, false));
            ViewData[PupilRankingNewConstants.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();
            ClassProfile cp = null;
            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[PupilRankingNewConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["Semester"] = _globalInfo.Semester.Value;
            IEnumerable<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.FromDate).ToList();
            List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
            if (lstPeriod != null && lstPeriod.Count() > 0)
            {
                foreach (var item in lstPeriod)
                {
                    PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                    periodDeclaration = item;
                    if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                        periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + periodDeclaration.FromDate.Value.ToShortDateString() + " - " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";

                    listPeriodDeclaration.Add(periodDeclaration);
                }
            }
            int period = 0;
            if (listPeriodDeclaration != null && listPeriodDeclaration.Count() != 0)
            {
                period = listPeriodDeclaration.FirstOrDefault().PeriodDeclarationID;
            }
            foreach (PeriodDeclaration pd in listPeriodDeclaration)
            {
                if (DateTime.Now >= pd.FromDate && DateTime.Now <= pd.EndDate)
                {
                    period = pd.PeriodDeclarationID;
                }
            }
            ViewData[PupilRankingNewConstants.LIST_PERIOD] = new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution", period);
            ViewData[PupilRankingNewConstants.LIST_APPLIEDLEVEL] = new GlobalInfo().AppliedLevel;

            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
              
                Dictionary<string, object> dictionary = new Dictionary<string, object> {
                        {"AcademicYearID", _globalInfo.AcademicYearID},
                        {"AppliedLevel", _globalInfo.AppliedLevel},
                        {"IsVNEN",true}
            };

                AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.GetValueOrDefault());
                if (!academicYear.IsTeacherCanViewAll && !_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                {
                    dictionary["UserAccountID"] = _globalInfo.UserAccountID;
                    dictionary["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }

                List<ClassProfile> listCP = ClassProfileBusiness.SearchBySchool(
                    _globalInfo.SchoolID.GetValueOrDefault(), dictionary)
                    .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[PupilRankingNewConstants.LIST_CLASS] = listCP;

                //ClassProfile cp = null;
                if (listCP != null && listCP.Count > 0)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                    else
                    {
                        if (!_globalInfo.IsAdminSchoolRole)
                        {
                          
                            dictionary["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                            List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(
                                _globalInfo.SchoolID.GetValueOrDefault(), dictionary)
                                .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                            if (listHead != null && listHead.Count > 0)
                            {
                                cp = listHead.First();
                            }
                            else
                            {
                                dictionary["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                                List<ClassProfile> listOverSee = ClassProfileBusiness.SearchBySchool(
                                    _globalInfo.SchoolID.GetValueOrDefault(), dictionary)
                                    .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listOverSee != null && listOverSee.Count > 0)
                                {
                                    cp = listOverSee.First();
                                }
                            }
                        }
                    }
                }
                ViewData["ClassProfile"] = cp;
            }

            #region Check quyền
                        //Tamhm1
                        ViewData[PupilRankingNewConstants.HAS_PERMISSION] = "false";
            if (cp != null && cp.ClassProfileID != 0)
                        {
                                ViewData[PupilRankingNewConstants.HAS_PERMISSION] = "true";
                        }
            #endregion
            ViewData[PupilRankingNewConstants.LIST_SUBJECT] = null;

            ViewData[PupilRankingNewConstants.ShowList] = false;

            ViewData[PupilRankingNewConstants.HAS_ERROR_DATA] = false;
            ViewData[PupilRankingNewConstants.LIST_IMPORTDATA] = null;

            ViewData[PupilRankingNewConstants.CLASS_12] = false;
            ViewData[PupilRankingNewConstants.SEMESTER_1] = true;
            ViewData[PupilRankingNewConstants.ERROR_BASIC_DATA] = false;
            ViewData[PupilRankingNewConstants.ERROR_IMPORT_MESSAGE] = "";
            ViewData[PupilRankingNewConstants.ERROR_IMPORT] = true;
            ViewData[PupilRankingNewConstants.ENABLE_ENTRYMARK] = "true";
        }
        #endregion
    }
}
