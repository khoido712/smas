﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PupilRankingNewArea
{
    public class PupilRankingNewAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PupilRankingNewArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PupilRankingNewArea_default",
                "PupilRankingNewArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
