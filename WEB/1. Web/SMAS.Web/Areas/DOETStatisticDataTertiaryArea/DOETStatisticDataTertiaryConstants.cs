﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DOETStatisticDataTertiaryArea
{
    public class DOETStatisticDataTertiaryConstants
    {
        public const string LIST_MARKSTATISTICS = "LIST_MARKSTATISTICS";
        public const string LIST_DISTRICT = "LIST_DISTRICT";
        public const string LIST_AcademicYear = "LIST_AcademicYear";
        public const string LIST_EducationLevel = "LIST_EducationLevel";      
        public const string LIST_Semester = "LIST_Semmester";
        public const string LIST_TYPESTATICS = "LIST_TYPESTATICS";
        public const string CURRENT_SEMESTER = "CURRENT_SEMESTER";
    
    }
}