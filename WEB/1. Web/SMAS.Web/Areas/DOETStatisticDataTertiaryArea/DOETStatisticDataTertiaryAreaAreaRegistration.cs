﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETStatisticDataTertiaryArea
{
    public class DOETStatisticDataTertiaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DOETStatisticDataTertiaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DOETStatisticDataTertiaryArea_default",
                "DOETStatisticDataTertiaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
