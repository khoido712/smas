﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author trangdd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.MarkRecordOfPrimaryArea.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.Text;
using System.Configuration;


namespace SMAS.Web.Areas.MarkRecordOfPrimaryArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class MarkRecordOfPrimaryController : BaseController
    {
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IVMarkRecordBusiness VMarkRecordBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly IVSummedUpRecordBusiness VSummedUpRecordBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IClassMovementBusiness ClassMovementBusiness;
        private readonly IPupilLeavingOffBusiness PupilLeavingOffBusiness;
        private readonly ILockMarkPrimaryBusiness LockMarkPrimaryBusiness;
        private readonly IMarkRecordHistoryBusiness MarkRecordHistoryBusiness;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;

        public MarkRecordOfPrimaryController(IPupilLeavingOffBusiness PupilLeavingOffBusiness, IClassMovementBusiness ClassMovementBusiness, ISchoolMovementBusiness SchoolMovementBusiness, IUserAccountBusiness UserAccountBusiness, IMarkRecordBusiness markrecordBusiness, IPeriodDeclarationBusiness PeriodDeclarationBusiness, IClassProfileBusiness ClassProfileBusiness, IClassSubjectBusiness ClassSubjectBusiness,
            IEducationLevelBusiness educationLevelBusiness, ITeachingAssignmentBusiness teachingAssignmentBusiness, ISubjectCatBusiness subjectCatBusiness,
            IPupilOfClassBusiness pupilOfClassBusiness, IMarkTypeBusiness markTypeBusiness, IAcademicYearBusiness academicYearBusiness,
            ISummedUpRecordBusiness summedUpRecordBusiness, IExemptedSubjectBusiness exemptedSubjectBusiness, ISchoolProfileBusiness schoolProfileBusiness,
            IPupilProfileBusiness pupilProfileBusiness, IVMarkRecordBusiness VMarkRecordBusiness, IVSummedUpRecordBusiness VSummedUpRecordBusiness, ILockMarkPrimaryBusiness LockMarkPrimaryBusiness, IMarkRecordHistoryBusiness MarkRecordHistoryBusiness, ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness
            )
        {
            this.PupilLeavingOffBusiness = PupilLeavingOffBusiness;
            this.ClassMovementBusiness = ClassMovementBusiness;
            this.SchoolMovementBusiness = SchoolMovementBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.MarkRecordBusiness = markrecordBusiness;
            this.PeriodDeclarationBusiness = PeriodDeclarationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.TeachingAssignmentBusiness = teachingAssignmentBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.MarkTypeBusiness = markTypeBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.SummedUpRecordBusiness = summedUpRecordBusiness;
            this.ExemptedSubjectBusiness = exemptedSubjectBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.VSummedUpRecordBusiness = VSummedUpRecordBusiness;
            this.VMarkRecordBusiness = VMarkRecordBusiness;
            this.LockMarkPrimaryBusiness = LockMarkPrimaryBusiness;
            this.MarkRecordHistoryBusiness = MarkRecordHistoryBusiness;
            this.SummedUpRecordHistoryBusiness = SummedUpRecordHistoryBusiness;
        }
        private GlobalInfo globalInfo = new GlobalInfo();

        const int month1 = 9;
        const int month2 = 10;
        const int month3 = 11;
        const int month4 = 12;
        const int month5 = 1;
        const int month6 = 2;
        const int month7 = 3;
        const int month8 = 4;
        const int month9 = 5;
        const string DGK1 = "DGK1";
        const string VGK1 = "VGK1";
        const string GK1 = "GK1";
        const string DCK1 = "DCK1";
        const string VCK1 = "VCK1";
        const string CK1 = "CK1";
        const string DGK2 = "DGK2";
        const string VGK2 = "VGK2";
        const string GK2 = "GK2";
        const string DCN = "DCN";
        const string VCN = "VCN";
        const string CN = "CN";
        const string HLHK1 = "HLHK1";
        const string HLCN = "HLCN";

        #region Index
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        /// <author>hath</author>
        /// <date>4/10/2013</date>
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }
        #endregion

        #region SetViewData
        /// <summary>
        /// SetViewData
        /// </summary>
        /// <author>hath</author>
        /// <date>4/10/2013</date>
        private void SetViewData()
        {
            //Radio hocky
            List<ViettelCheckboxList> lrSemester = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio = new ViettelCheckboxList();
            bool semester1 = false;
            bool semester2 = false;
            if (globalInfo.Semester.Value == 1)
            { semester1 = true; }
            else
            { semester2 = true; }
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = SystemParamsInFile.SEMESTER_I;
            viettelradio.cchecked = semester1;
            viettelradio.disabled = false;
            viettelradio.Value = 1;
            lrSemester.Add(viettelradio);
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = SystemParamsInFile.SEMESTER_II;
            viettelradio.cchecked = semester2;
            viettelradio.disabled = false;
            viettelradio.Value = 2;
            lrSemester.Add(viettelradio);
            ViewData[MarkRecordOfPrimaryConstants.LIST_SEMESTER] = lrSemester;
            //Radio Khoi hoc
            List<ViettelCheckboxList> lrEducationLevel = new List<ViettelCheckboxList>();
            int i = 0;
            foreach (EducationLevel item in globalInfo.EducationLevels)
            {
                i++;
                bool checkEdu = false;
                if (i == 1) { checkEdu = true; }
                viettelradio = new ViettelCheckboxList();
                viettelradio.Label = item.Resolution;
                viettelradio.cchecked = checkEdu;
                viettelradio.disabled = false;
                viettelradio.Value = item.EducationLevelID;
                lrEducationLevel.Add(viettelradio);

            }
            ViewData[MarkRecordOfPrimaryConstants.LIST_EDUCATIONLEVEL] = lrEducationLevel;
            //List Button Lop
            ViewData[MarkRecordOfPrimaryConstants.LIST_CLASS] = new List<ClassProfile>();
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = globalInfo.EducationLevels.FirstOrDefault().EducationLevelID;
            dicClass["AcademicYearID"] = globalInfo.AcademicYearID;
            if (!globalInfo.IsAdminSchoolRole && !globalInfo.IsViewAll)
            {
                dicClass["UserAccountID"] = globalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(globalInfo.SchoolID.Value, dicClass).OrderBy(o => o.DisplayName);
            if (lsClass.Count() != 0)
            {

                ViewData[MarkRecordOfPrimaryConstants.LIST_CLASS] = lsClass;
            }
            //Radio Mon hoc
            ViewData[MarkRecordOfPrimaryConstants.LIST_SUBJECT] = new List<ViettelCheckboxList>();
            //List
            ViewData[MarkRecordOfPrimaryConstants.LIST_MARKRECORD] = new List<MarkRecordOfPrimaryViewModel>();
            //Mac dinh
            ViewData[MarkRecordOfPrimaryConstants.BOOL_TOAN] = false;
            ViewData[MarkRecordOfPrimaryConstants.BOOL_MONKHAC] = false;
            if (globalInfo.Semester == 1)
            {
                ViewData[MarkRecordOfPrimaryConstants.BOOL_HKI] = true;
                ViewData[MarkRecordOfPrimaryConstants.BOOL_HKII] = false;
            }
            if (globalInfo.Semester == 2)
            {
                ViewData[MarkRecordOfPrimaryConstants.BOOL_HKI] = false;
                ViewData[MarkRecordOfPrimaryConstants.BOOL_HKII] = true;
            }

            ViewData[MarkRecordOfPrimaryConstants.HAS_ERROR_DATA] = false;
            ViewData[MarkRecordOfPrimaryConstants.LIST_IMPORTDATA] = null;

            ViewData[MarkRecordOfPrimaryConstants.ERROR_BASIC_DATA] = false;
            ViewData[MarkRecordOfPrimaryConstants.ERROR_IMPORT_MESSAGE] = "";
            ViewData[MarkRecordOfPrimaryConstants.DISABLED_BUTTON] = false;
        }
        #endregion

        /// <summary>
        /// SearchTV
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>4/10/2013</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchTV(FormCollection fc)
        {
            int idSemester = int.Parse(fc["rptSemester"]);
            int idClass = int.Parse(fc["HidClassProfileID"]);
            int idSubject = int.Parse(fc["rptSubject"]);
            AjaxLoadGrid(idSemester, idSubject, idClass);
            return PartialView("_ListTV");
        }
        /// <summary>
        /// SearchToan
        /// </summary>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>4/10/2013</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchToan(FormCollection fc)
        {
            int idSemester = int.Parse(fc["rptSemester"]);
            int idClass = int.Parse(fc["HidClassProfileID"]);
            int idSubject = int.Parse(fc["rptSubject"]);
            AjaxLoadGrid(idSemester, idSubject, idClass);
            return PartialView("_ListToan");
        }
        /// <summary>
        /// SearchMK
        /// </summary>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>4/10/2013</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult SearchMK(FormCollection fc)
        {
            int idSemester = int.Parse(fc["rptSemester"]);
            int idClass = int.Parse(fc["HidClassProfileID"]);
            int idSubject = int.Parse(fc["rptSubject"]);
            AjaxLoadGrid(idSemester, idSubject, idClass);
            return PartialView("_ListMK");
        }
        /// <summary>
        /// AjaxLoadClass
        /// </summary>
        /// <param name="idEducationLevel">The id education level.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>4/10/2013</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadClass(int? idEducationLevel)
        {
            if (globalInfo.Semester == 1)
            {
                ViewData[MarkRecordOfPrimaryConstants.BOOL_HKI] = true;
                ViewData[MarkRecordOfPrimaryConstants.BOOL_HKII] = false;
            }
            if (globalInfo.Semester == 2)
            {
                ViewData[MarkRecordOfPrimaryConstants.BOOL_HKI] = false;
                ViewData[MarkRecordOfPrimaryConstants.BOOL_HKII] = true;
            }
            Nullable<Int32> nullableInt = null;
            if (idEducationLevel != nullableInt)
            {
                Session["idEducationLevel"] = idEducationLevel;
            }
            else
            {
                idEducationLevel = Convert.ToInt32(Session["idEducationLevel"] ?? 0);
            }
            SearchViewModel svm = new SearchViewModel();
            ViewData[MarkRecordOfPrimaryConstants.LIST_CLASS] = new List<ClassProfile>();
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["EducationLevelID"] = Session["idEducationLevel"];
            dicClass["AcademicYearID"] = GlobalInfo.AcademicYearID;

            if (GlobalInfo.IsAdminSchoolRole == false && !GlobalInfo.IsViewAll)
            {
                dicClass["UserAccountID"] = GlobalInfo.UserAccountID;
                dicClass["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, dicClass);
            if (lsClass.Count() != 0)
            {


                ViewData[MarkRecordOfPrimaryConstants.LIST_CLASS] = lsClass.OrderBy(o => o.DisplayName).ToList();

                svm.EducationLevelID = Convert.ToInt32(Session["idEducationLevel"]);
                svm.ClassProfileID = lsClass.ToList().FirstOrDefault().ClassProfileID;
            }

            return PartialView("_ClassPanel", svm);
        }
        /// <summary>
        /// AjaxLoadSubject
        /// </summary>
        /// <param name="idClassProfile">The id class profile.</param>
        /// <param name="idSemester">The id semester.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>4/10/2013</date>

        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxLoadSubject(int? idClassProfile, int? idSemester)
        {
            if (globalInfo.Semester == 1)
            {
                ViewData[MarkRecordOfPrimaryConstants.BOOL_HKI] = true;
                ViewData[MarkRecordOfPrimaryConstants.BOOL_HKII] = false;
            }
            if (globalInfo.Semester == 2)
            {
                ViewData[MarkRecordOfPrimaryConstants.BOOL_HKI] = false;
                ViewData[MarkRecordOfPrimaryConstants.BOOL_HKII] = true;
            }
            if (idClassProfile == null) idClassProfile = 0;
            if (idSemester == null) idSemester = 0;

            SearchViewModel svm = new SearchViewModel();
            svm.ClassProfileID = idClassProfile;
            svm.SemesterID = idSemester;

            GlobalInfo GlobalInfo = new GlobalInfo();
            List<ViettelCheckboxList> lrSubject = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio = new ViettelCheckboxList();


            List<ClassSubject> lsClassSubject = new List<ClassSubject>();
            if (GlobalInfo.AcademicYearID.HasValue && GlobalInfo.SchoolID.HasValue && idClassProfile.HasValue && idSemester.HasValue)
            {
                lsClassSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(GlobalInfo.UserAccountID,
                    GlobalInfo.AcademicYearID.Value, GlobalInfo.SchoolID.Value, idSemester.Value,
                    idClassProfile.Value, GlobalInfo.IsViewAll)
                .Where(o => o.SubjectCat.IsActive == true)
                .Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.SubjectName)
                .ToList();
            }
            foreach (ClassSubject cs in lsClassSubject)
            {
                viettelradio = new ViettelCheckboxList();
                viettelradio.Label = cs.SubjectCat.DisplayName;
                viettelradio.cchecked = false;
                viettelradio.disabled = false;
                viettelradio.Value = cs.SubjectID;
                lrSubject.Add(viettelradio);
            }


            ViewData[MarkRecordOfPrimaryConstants.LIST_SUBJECT] = lrSubject;
            if (lrSubject.Count != 0 && idSemester != 0)
            {
                string Semester = "";
                if (idSemester == 1)
                    Semester = SystemParamsInFile.SEMESTER_I;
                if (idSemester == 2)
                    Semester = SystemParamsInFile.SEMESTER_II;
                if (ClassProfileBusiness.Find(idClassProfile.Value) != null && SubjectCatBusiness.Find(lrSubject.FirstOrDefault().Value).DisplayName.ToUpper() != null)
                    ViewData[MarkRecordOfPrimaryConstants.LABEL_MES] = Res.Get("BookMarkRecord_Label_Message").ToUpper() + " - " + SubjectCatBusiness.Find(lrSubject.FirstOrDefault().Value).DisplayName.ToUpper() + "-" + ClassProfileBusiness.Find(idClassProfile.Value).DisplayName.ToUpper() + "-" + Semester.ToUpper();
            }

            return PartialView("_SubjectPanel", svm);
        }
        /// <summary>
        /// AjaxLoadGrid
        /// </summary>
        /// <param name="idSubject">The id subject.</param>
        /// <param name="idSemester">The id semester.</param>
        /// <param name="idClassProfile">The id class profile.</param>
        /// <returns>
        /// PartialViewResult
        /// </returns>
        /// <author>hath</author>
        /// <date>4/10/2013</date>

        [ValidateAntiForgeryToken]
        
        public PartialViewResult AjaxLoadGrid(int? idSubject, int? idSemester, int? idClassProfile)
        {
            SetViewData();

            AcademicYear ac = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);
            ViewData[MarkRecordOfPrimaryConstants.DISABLED_BUTTON] = true;
            ViewData[MarkRecordOfPrimaryConstants.LIST_MARKRECORD] = new List<MarkRecordOfPrimaryViewModel>();
            List<MarkRecordOfPrimaryViewModel> lstmr = new List<MarkRecordOfPrimaryViewModel>();
            if (idSemester != null)
            {
                #region check Permission
                if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    ViewData[MarkRecordOfPrimaryConstants.BOOL_HKI] = true;
                    ViewData[MarkRecordOfPrimaryConstants.BOOL_HKII] = false;
                    if (UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, idClassProfile.Value, idSubject.Value, idSemester.Value))
                    {
                        if (globalInfo.IsAdminSchoolRole)
                        {
                            ViewData[MarkRecordOfPrimaryConstants.DISABLED_BUTTON] = false;
                        }
                        else
                        {
                            if (ac.FirstSemesterEndDate >= DateTime.Now && ac.FirstSemesterStartDate <= DateTime.Now)
                            {
                                ViewData[MarkRecordOfPrimaryConstants.DISABLED_BUTTON] = false;
                            }
                        }
                    }
                }
                if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    ViewData[MarkRecordOfPrimaryConstants.BOOL_HKI] = false;
                    ViewData[MarkRecordOfPrimaryConstants.BOOL_HKII] = true;
                    if (UtilsBusiness.HasSubjectTeacherPermission(_globalInfo.UserAccountID, idClassProfile.Value, idSubject.Value, idSemester.Value))
                    {
                        if (globalInfo.IsAdminSchoolRole)
                        {
                            ViewData[MarkRecordOfPrimaryConstants.DISABLED_BUTTON] = false;
                        }
                        else
                        {
                            if (ac.SecondSemesterEndDate >= DateTime.Now && ac.SecondSemesterStartDate <= DateTime.Now)
                            {
                                ViewData[MarkRecordOfPrimaryConstants.DISABLED_BUTTON] = false;
                            }
                        }
                    }
                }
                // Neu khong la nam hoc hien tai thi khong cho phep thao tac voi con diem (ke ca QTHT)
                if (!globalInfo.IsCurrentYear)
                {
                    ViewData[MarkRecordOfPrimaryConstants.DISABLED_BUTTON] = true;
                }

                #endregion

                #region Fill Data Grid
                IDictionary<string, object> SearchPupil = new Dictionary<string, object>();
                SearchPupil["SchoolID"] = globalInfo.SchoolID;
                SearchPupil["AcademicYearID"] = globalInfo.AcademicYearID;
                SearchPupil["ClassID"] = idClassProfile;
                SearchPupil["SubjectID"] = idSubject;
                SearchPupil["Semester"] = idSemester;

                List<MarkRecordBO> lstMarkRecordOfClass = MarkRecordBusiness
                    .GetMardRecordOfClass(SearchPupil).ToList();
                List<MarkRecordBO> lstMarkRecordOfClassByTX = lstMarkRecordOfClass.Where(o => o.MarkedDate != null).ToList();

                //Get markLockTitle NAMTA
                string LockMarkTitle = MarkRecordBusiness.GetLockMarkTitlePrimary(globalInfo.SchoolID.Value, globalInfo.AcademicYearID.Value, idClassProfile.Value, idSubject.Value, idSemester.Value).Replace("_", "");

                ViewData[MarkRecordOfPrimaryConstants.LOCK_MARK_TITLE] = !string.IsNullOrWhiteSpace(LockMarkTitle) ? LockMarkTitle.Substring(0, LockMarkTitle.Length - 1) : string.Empty;

                List<ExemptedSubject> lstExempteds = ExemptedSubjectBusiness.GetListExemptedSubject(idClassProfile.Value, idSemester.Value).Where(u => u.SubjectID == idSubject.Value).ToList();

                List<MarkType> lstMarkType = MarkTypeBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", globalInfo.AppliedLevel.Value } }).ToList();
                MarkType markTypeDGK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.DGK1);
                MarkType markTypeVGK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.VGK1);
                MarkType markTypeGK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.GK1);
                MarkType markTypeDCK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.DCK1);
                MarkType markTypeVCK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.VCK1);
                MarkType markTypeCK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.CK1);
                MarkType markTypeDGK2 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.DGK2);
                MarkType markTypeVGK2 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.VGK2);
                MarkType markTypeGK2 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.GK2);
                MarkType markTypeDCN = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.DCN);
                MarkType markTypeVCN = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.VCN);
                MarkType markTypeCN = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.CN);
                MarkType markTypeDTX = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.TX);

                List<SumUpRecordPrimaryBO> listSummedUpRecord = SummedUpRecordBusiness.GetSummedUpWithRestestOfPrimary(globalInfo.SchoolID.Value, globalInfo.AcademicYearID.Value, idSemester.Value, idClassProfile.Value, idSubject.Value).ToList();
                //listSummedUpRecord = listSummedUpRecord.Where(u => u.Semester == listSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester)).ToList();
                //lay max semester mon cap 1
                List<SumUpRecordPrimaryBO> listSummedUpRecordSpec = listSummedUpRecord.Where(u => u.Semester == listSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.SubjectID == u.SubjectID).Max(v => v.Semester)).ToList();

                List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchByClass(idClassProfile.Value, new Dictionary<string, object>() { { "SubjectID", idSubject }, { "SchoolID", ac.SchoolID } }).ToList();
                if (listClassSubject != null && listClassSubject.Count() > 0)
                {

                }
                //Duyệt list và add dữ liệu của hs vào list model
                //foreach (MarkRecordBO mrbo in lstMarkRecordOfClass)
                for (int i = 0; i < lstMarkRecordOfClass.Count; i++)
                {
                    MarkRecordBO mrbo = lstMarkRecordOfClass[i];
                    MarkRecordOfPrimaryViewModel mpv = lstmr.Where(o => o.PupilID == mrbo.PupilID).FirstOrDefault();

                    if (mpv == null)
                    {
                        mpv = new MarkRecordOfPrimaryViewModel();
                        mpv.PupilID = mrbo.PupilID;
                        mpv.PupilCode = mrbo.PupilCode;
                        mpv.PupilName = mrbo.FullName;
                        mpv.Name = mrbo.Name;
                        mpv.OrderInClass = mrbo.OrderInClass;
                        lstmr.Add(mpv);
                    }
                    bool statusPupilOther = false;
                    #region Kiểm tra trạng thái của học sinh chuyển trường,chuyển lớp,thôi học
                    if (mrbo.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL
                        || mrbo.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                        || mrbo.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF)
                    {
                        statusPupilOther = true;
                        mpv.TimeStatus = mrbo.EndDate;
                        mpv.Red = true;
                    }

                    #endregion

                    //Kiểm tra học sinh miễn giảm
                    mpv.checkbox = lstExempteds.Any(u => u.PupilID == mrbo.PupilID);

                    #region Diem thuong xuyen
                    if (mrbo.MarkedDate != null)
                    {
                        if (mrbo.Semester == 1)
                        {
                            if (mrbo.MarkedDate.Value.Month == 9)
                            {
                                mpv.Month1 += mrbo.Mark.ToString().Replace(",00", "") + " ";
                            }
                            if (mrbo.MarkedDate.Value.Month == 10)
                            {
                                mpv.Month2 += mrbo.Mark.ToString().Replace(",00", "") + " ";
                            }
                            if (mrbo.MarkedDate.Value.Month == 11)
                            {
                                mpv.Month3 += mrbo.Mark.ToString().Replace(",00", "") + " ";
                            }
                            if (mrbo.MarkedDate.Value.Month == 12)
                            {
                                mpv.Month4 += mrbo.Mark.ToString().Replace(",00", "") + " ";
                            }
                            if (mrbo.MarkedDate.Value.Month == 1)
                            {
                                mpv.Month5 += mrbo.Mark.ToString().Replace(",00", "") + " ";
                            }
                        }
                        if (mrbo.Semester == 2)
                        {
                            if (mrbo.MarkedDate.Value.Month == 2)
                            {
                                mpv.Month6 += mrbo.Mark.ToString().Replace(",00", "") + " ";
                            }
                            if (mrbo.MarkedDate.Value.Month == 3)
                            {
                                mpv.Month7 += mrbo.Mark.ToString().Replace(",00", "") + " ";
                            }
                            if (mrbo.MarkedDate.Value.Month == 4)
                            {
                                mpv.Month8 += mrbo.Mark.ToString().Replace(",00", "") + " ";
                            }
                            if (mrbo.MarkedDate.Value.Month == 5)
                            {
                                mpv.Month9 += mrbo.Mark.ToString().Replace(",00", "") + " ";
                            }
                        }
                    }
                    #endregion

                    #region Hoc Ky 1
                    if (mrbo.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        if (mpv.TimeStatus.HasValue && mpv.TimeStatus < ac.FirstSemesterEndDate)
                        {
                            mpv.Show = false;
                        }

                        if (mrbo.MarkedDate == null)
                        {
                            //Diem doc
                            if (mrbo.MarkTypeID == markTypeDGK1.MarkTypeID)
                            {
                                mpv.MarkDK[mpv.PupilID.ToString() + "GKI_D"] = mrbo.Mark;
                            }
                            //Diem viet
                            if (mrbo.MarkTypeID == markTypeVGK1.MarkTypeID)
                            {
                                mpv.MarkDK[mpv.PupilID.ToString() + "GKI_V"] = mrbo.Mark;
                            }
                            //Diem GK
                            if (mrbo.MarkTypeID == markTypeGK1.MarkTypeID)
                            {
                                mpv.MarkDK[mpv.PupilID.ToString() + "GKI_G"] = mrbo.Mark;
                                mpv.KTDKGKI = Math.Round(mrbo.Mark.Value);
                            }
                            //Diem doc
                            if (mrbo.MarkTypeID == markTypeDCK1.MarkTypeID)
                            {
                                mpv.MarkDK[mpv.PupilID.ToString() + "CKI_D"] = mrbo.Mark;
                            }
                            //Diem viet
                            if (mrbo.MarkTypeID == markTypeVCK1.MarkTypeID)
                            {
                                mpv.MarkDK[mpv.PupilID.ToString() + "CKI_V"] = mrbo.Mark;
                            }
                            //Diem CK
                            if (mrbo.MarkTypeID == markTypeCK1.MarkTypeID)
                            {
                                mpv.MarkDK[mpv.PupilID.ToString() + "CKI_C"] = mrbo.Mark;
                                mpv.KTDKCKI = Math.Round(mrbo.Mark.Value);
                            }
                        }
                    }
                    #endregion

                    #region Hoc Ky 2
                    if (mrbo.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        if (mpv.TimeStatus.HasValue && statusPupilOther)
                        {
                            mpv.Show = false;
                        }
                        if (mrbo.MarkedDate == null)
                        {
                            //Diem doc
                            if (mrbo.MarkTypeID == markTypeDGK2.MarkTypeID)
                            {
                                mpv.MarkDK[mpv.PupilID.ToString() + "GKII_D"] = mrbo.Mark;
                            }
                            //Diem viet
                            if (mrbo.MarkTypeID == markTypeVGK2.MarkTypeID)
                            {
                                mpv.MarkDK[mpv.PupilID.ToString() + "GKII_V"] = mrbo.Mark;
                            }
                            //Diem GK
                            if (mrbo.MarkTypeID == markTypeGK2.MarkTypeID)
                            {
                                mpv.MarkDK[mpv.PupilID.ToString() + "GKII_G"] = Math.Round(mrbo.Mark.Value);
                                mpv.KTDKGKII = Math.Round(mrbo.Mark.Value);
                            }
                            //Diem doc
                            if (mrbo.MarkTypeID == markTypeDCN.MarkTypeID)
                            {
                                mpv.MarkDK[mpv.PupilID.ToString() + "CKII_D"] = mrbo.Mark;
                            }
                            //Diem viet
                            if (mrbo.MarkTypeID == markTypeVCN.MarkTypeID)
                            {
                                mpv.MarkDK[mpv.PupilID.ToString() + "CKII_V"] = mrbo.Mark;
                            }
                            //Diem CN
                            if (mrbo.MarkTypeID == markTypeCN.MarkTypeID)
                            {
                                mpv.MarkDK[mpv.PupilID.ToString() + "CKII_C"] = Math.Round(mrbo.Mark.Value);
                                mpv.KTDKCKII = Math.Round(mrbo.Mark.Value);
                            }
                        }
                    }
                    #endregion
                }

                #region lay diem trung binh mon va tinh xep loai
                //foreach (MarkRecordOfPrimaryViewModel mpv in lstmr)
                for (int j = 0; j < lstmr.Count; j++)
                {
                    MarkRecordOfPrimaryViewModel mpv = lstmr[j];
                    if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        SumUpRecordPrimaryBO surBO = new SumUpRecordPrimaryBO();
                        var surList = listSummedUpRecord.Where(u => u.PupilID == mpv.PupilID);
                        if (surList.Any(u => u.Semester == 5) && !surList.Any(u => u.Semester == 2))
                        {
                            surBO = surList.Where(u => u.Semester == 5).FirstOrDefault();
                        }
                        else
                        {
                            surBO = surList.Where(u => u.Semester == 1).FirstOrDefault();
                        }
                        if (surBO != null)
                        {
                            mpv.HLMKI = surBO.ReTestMark.HasValue ? surBO.ReTestMark.Value : surBO.SummedUpMark;

                            if (mpv.HLMKI >= 9)
                                mpv.XLHLMKI = SystemParamsInFile.XL_GIOI;

                            if (mpv.HLMKI >= 7 && mpv.HLMKI < 9)
                                mpv.XLHLMKI = SystemParamsInFile.XL_KHA;

                            if (mpv.HLMKI >= 5 && mpv.HLMKI < 7)
                                mpv.XLHLMKI = SystemParamsInFile.XL_TRUNGBINH;

                            if (mpv.HLMKI < 5)
                                mpv.XLHLMKI = SystemParamsInFile.XL_YEU;
                        }
                    }

                    if (idSemester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        SumUpRecordPrimaryBO surBO = new SumUpRecordPrimaryBO();
                        var surList = listSummedUpRecord.Where(u => u.PupilID == mpv.PupilID);
                        if (surList.Any(u => u.Semester == 5))
                        {
                            surBO = surList.Where(u => u.Semester == 5).FirstOrDefault();
                        }
                        else
                        {
                            surBO = surList.Where(u => u.Semester == 2).FirstOrDefault();
                        }
                        if (surBO != null)
                        {
                            mpv.HLMKII = surBO.ReTestMark.HasValue ? surBO.ReTestMark.Value : surBO.SummedUpMark;

                            if (mpv.HLMKII >= 9)
                                mpv.XLHLMKII = SystemParamsInFile.XL_GIOI;

                            if (mpv.HLMKII >= 7 && mpv.HLMKII < 9)
                                mpv.XLHLMKII = SystemParamsInFile.XL_KHA;

                            if (mpv.HLMKII >= 5 && mpv.HLMKII < 7)
                                mpv.XLHLMKII = SystemParamsInFile.XL_TRUNGBINH;

                            if (mpv.HLMKII < 5)
                                mpv.XLHLMKII = SystemParamsInFile.XL_YEU;
                            // Fill du lieu ca nam
                            mpv.HLMN = mpv.HLMKII;
                            mpv.XLHLMN = mpv.XLHLMKII;
                        }
                    }
                }
                #endregion

                // AnhVD 20140814 - Order tren danh sach sau cung tra ve
                ViewData[MarkRecordOfPrimaryConstants.LIST_MARKRECORD] = lstmr.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.PupilName).ToList();

                List<int> ListToanID = SubjectCatBusiness.All.Where(o => o.DisplayName.Contains(SystemParamsInFile.MON_TOAN) &&
                    o.AppliedLevel == globalInfo.AppliedLevel && o.IsCoreSubject).Select(o => o.SubjectCatID).ToList();
                List<int> ListTiengVietID = SubjectCatBusiness.All.Where(o => o.DisplayName.Contains(SystemParamsInFile.MON_TIENGVIET) &&
                   o.AppliedLevel == globalInfo.AppliedLevel && o.IsCoreSubject).Select(o => o.SubjectCatID).ToList();

                // Tạo dữ liệu ghi log
                ClassProfile ClassProfile = ClassProfileBusiness.Find(idClassProfile);
                SubjectCat subject = SubjectCatBusiness.Find(idSubject);
                SetViewDataActionAudit(String.Empty, String.Empty, idClassProfile.ToString(), "View mark_record class_id: " + idClassProfile + ", subject_id: " + idSubject + ", semester: " + idSemester, "class_id: " + idClassProfile + ", subject_id: " + idSubject + ", semester: " + idSemester, "Sổ điểm", SMAS.Business.Common.GlobalConstants.ACTION_VIEW, "Xem sổ điểm lớp " + ClassProfile.DisplayName + " học kì " + idSemester + " môn học " + subject.SubjectName);

                //Nếu là môn toán
                if (ListToanID.Contains(idSubject.Value))
                {
                    return PartialView("_ListToan");
                }
                else
                {
                    //Nếu là môn tiếng việt
                    if (ListTiengVietID.Contains(idSubject.Value))
                    {
                        return PartialView("_ListTV");
                    }
                    //Nếu là môn khác
                    else
                    {
                        return PartialView("_ListMK");
                    }
                }
                #endregion
            }

            return PartialView("_List");
        }

        /// <summary>
        /// Creates the specified fc.
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <returns></returns>
        /// <author>hath</author>
        /// <date>4/10/2013</date>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Create(FormCollection fc)
        {
            int idSemester = int.Parse(fc["Semester"]);
            int idClass = int.Parse(fc["ClassID"]);
            int idSubject = int.Parse(fc["SubjectID"]);

            string lockTitle = fc["LockTitle"];
            // Kiem tra du lieu khoa tu client truyen len co khop voi server khong
            LockMarkPrimaryBusiness.CheckCurrentLockMark(lockTitle, idClass, SystemParamsInFile.ISCOMMENTING_TYPE_MARK);

            AcademicYear AcademicYear = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);         
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(AcademicYear);
            List<MarkRecord> ListMarkRecord = new List<MarkRecord>();
            List<MarkRecord> ListMarkRecordDK = new List<MarkRecord>();
            List<SummedUpRecord> lstSummedUpRecord = new List<SummedUpRecord>();

            if (fc["chkSatus"] == "" || fc["chkSatus"] == null)
            {
                throw new BusinessException("MarkRecord_Label_NoPupil");
            }

            List<MarkType> lstMarkType = MarkTypeBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", globalInfo.AppliedLevel.Value } }).ToList();
            MarkType markTypeDGK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.DGK1);
            MarkType markTypeVGK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.VGK1);
            MarkType markTypeGK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.GK1);
            MarkType markTypeDCK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.DCK1);
            MarkType markTypeVCK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.VCK1);
            MarkType markTypeCK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.CK1);
            MarkType markTypeDGK2 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.DGK2);
            MarkType markTypeVGK2 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.VGK2);
            MarkType markTypeGK2 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.GK2);
            MarkType markTypeDCN = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.DCN);
            MarkType markTypeVCN = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.VCN);
            MarkType markTypeCN = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.CN);
            MarkType markTypeDTX = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.TX);

            string[] lsPupilID = fc["chkSatus"] != null ? fc["chkSatus"].Split(new Char[] { ',' }) : new string[] { };

            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(idClass, new Dictionary<string, object> { { "SubjectID", idSubject }, { "SchoolID", AcademicYear.SchoolID } }).SingleOrDefault();

            //check mon hoc
            bool Toan = classSubject.SubjectCat.DisplayName.Equals(SystemParamsInFile.MON_TOAN, StringComparison.InvariantCultureIgnoreCase);
            bool TV = classSubject.SubjectCat.DisplayName.Equals(SystemParamsInFile.MON_TIENGVIET, StringComparison.InvariantCultureIgnoreCase);
            bool MK = !Toan && !TV;

            //Validate -1 in mark
            string srtMonth = "M";
            string strGK = "GK";
            string strCK = "CK";
            foreach (string key in fc.AllKeys)
            {
                if (key.Contains(srtMonth) || key.Contains(strGK) || key.Contains(strCK))
                {
                    if (fc.GetValues(key).Contains("-1"))
                    {
                        throw new BusinessException();
                    }
                }
            }
            //End check validate

            // Tạo data ghi log
            List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", globalInfo.AcademicYearID }, { "ClassID", idClass }, { "Check", "Check" } }).ToList();
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            StringBuilder inforLog = null;
            int iLog = 0;
            foreach (string item in lsPupilID)
            {
                int id = int.Parse(item);

                // Tạo dữ liệu ghi log
                PupilOfClass pop = lstPOC.Where(p => p.PupilID == id).FirstOrDefault();
                objectIDStr.Append(id);
                descriptionStr.Append("Update mark record pupil_id:" + id);
                paramsStr.Append("pupil_id:" + id);
                userFuntionsStr.Append("Sổ điểm");
                userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                inforLog = new StringBuilder();
                inforLog.Append("Cập nhật điểm cho " + pop.PupilProfile.FullName);
                inforLog.Append(" mã " + pop.PupilProfile.PupilCode);
                inforLog.Append(" lớp " + pop.ClassProfile.DisplayName);
                inforLog.Append(" môn " + classSubject.SubjectCat.SubjectName);
                inforLog.Append(" học kì " + idSemester + " ");
                inforLog.Append(" năm " + pop.Year.Value + "-" + (pop.Year.Value + 1) + " ");
                userDescriptionsStr.Append(inforLog.ToString());
                //List<MarkRecord> listOldRecord = pop.PupilProfile.MarkRecords.ToList();// chỗ này ko tốn performance vì bản thân lstPOC đã có thông tin điểm
                //oldObjectStr.Append("{");
                //oldObjectStr.Append("pupil_profile_id:" + id + ",");
                //for (int j = 0; j < listOldRecord.Count; j++)
                //{
                //    MarkRecord record = listOldRecord[j];
                //    oldObjectStr.Append("{MarkRecordID:" + record.MarkRecordID + "," + record.Title + ":" + record.Mark + "}");
                //    if (j < listOldRecord.Count - 1)
                //    {
                //        oldObjectStr.Append(",");
                //    }
                //}
                //oldObjectStr.Append("}");
                // end

                #region Diem TX

                string strMonth1 = item + "M1";
                string strMonth2 = item + "M2";
                string strMonth3 = item + "M3";
                string strMonth4 = item + "M4";
                string strMonth5 = item + "M5";
                string strMonth6 = item + "M6";
                string strMonth7 = item + "M7";
                string strMonth8 = item + "M8";
                string strMonth9 = item + "M9";
                string MarkMonth1 = "";
                string MarkMonth2 = "";
                string MarkMonth3 = "";
                string MarkMonth4 = "";
                string MarkMonth5 = "";
                string MarkMonth6 = "";
                string MarkMonth7 = "";
                string MarkMonth8 = "";
                string MarkMonth9 = "";

                //Check vallidate Mark -1



                #region ky 1
                if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    int i = 0;
                    MarkMonth1 = fc[strMonth1];
                    if (MarkMonth1 == "")
                    {
                        MarkMonth1 = "-1";
                    }
                    List<decimal> lstMarkMonth1 = this.ConvertMark(MarkMonth1);
                    // Tạo dữ liệu ghi log
                    userDescriptionsStr.Append("Tháng thứ 1: ");
                    foreach (decimal m1 in lstMarkMonth1)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = id;
                        mark.Semester = idSemester;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = idClass;
                        mark.SubjectID = idSubject;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.Mark = m1;
                        mark.Title = SystemParamsInFile.TX;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 9, 1);
                        ListMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m1 + " ");
                    }

                    i = 0;
                    MarkMonth2 = fc[strMonth2];
                    if (MarkMonth2 == "")
                    {
                        MarkMonth2 = "-1";
                    }
                    List<decimal> lstMarkMonth2 = this.ConvertMark(MarkMonth2);
                    // Tạo dữ liệu ghi log
                    userDescriptionsStr.Append("Tháng thứ 2: ");
                    foreach (decimal m2 in lstMarkMonth2)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = id;
                        mark.Semester = idSemester;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = idClass;
                        mark.SubjectID = idSubject;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.Mark = m2;
                        mark.Title = SystemParamsInFile.TX;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 10, 1);
                        ListMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m2 + " ");
                    }

                    i = 0;
                    MarkMonth3 = fc[strMonth3];
                    if (MarkMonth3 == "")
                    {
                        MarkMonth3 = "-1";
                    }
                    List<decimal> lstMarkMonth3 = this.ConvertMark(MarkMonth3);
                    userDescriptionsStr.Append("Tháng thứ 3: ");
                    foreach (decimal m3 in lstMarkMonth3)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = id;
                        mark.Semester = idSemester;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = idClass;
                        mark.SubjectID = idSubject;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.Mark = m3;
                        mark.Title = SystemParamsInFile.TX;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 11, 1);
                        ListMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m3 + " ");
                    }

                    i = 0;
                    MarkMonth4 = fc[strMonth4];
                    if (MarkMonth4 == "")
                    {
                        MarkMonth4 = "-1";
                    }
                    List<decimal> lstMarkMonth4 = this.ConvertMark(MarkMonth4);
                    userDescriptionsStr.Append("Tháng thứ 4: ");
                    foreach (decimal m4 in lstMarkMonth4)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = id;
                        mark.Semester = idSemester;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = idClass;
                        mark.SubjectID = idSubject;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.Mark = m4;
                        mark.Title = SystemParamsInFile.TX;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 12, 1);
                        ListMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m4 + " ");
                    }

                    i = 0;
                    MarkMonth5 = fc[strMonth5];
                    if (MarkMonth5 == "")
                    {
                        MarkMonth5 = "-1";
                    }
                    List<decimal> lstMarkMonth5 = this.ConvertMark(MarkMonth5);
                    userDescriptionsStr.Append("Tháng thứ 5: ");
                    foreach (decimal m5 in lstMarkMonth5)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = id;
                        mark.Semester = idSemester;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = idClass;
                        mark.SubjectID = idSubject;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.Mark = m5;
                        mark.Title = SystemParamsInFile.TX;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 1, 1);
                        ListMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m5 + " ");
                    }
                }
                #endregion

                #region ky 2
                if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    int i = 0;
                    MarkMonth6 = fc[strMonth6];
                    if (MarkMonth6 == "")
                    {
                        MarkMonth6 = "-1";
                    }
                    List<decimal> lstMarkMonth6 = this.ConvertMark(MarkMonth6);
                    userDescriptionsStr.Append("Tháng thứ 6: ");
                    foreach (decimal m6 in lstMarkMonth6)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = id;
                        mark.Semester = idSemester;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = idClass;
                        mark.SubjectID = idSubject;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.Mark = m6;
                        mark.Title = SystemParamsInFile.TX;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 2, 1);
                        ListMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m6 + " ");
                    }

                    i = 0;
                    MarkMonth7 = fc[strMonth7];
                    if (MarkMonth7 == "")
                    {
                        MarkMonth7 = "-1";
                    }
                    List<decimal> lstMarkMonth7 = this.ConvertMark(MarkMonth7);
                    userDescriptionsStr.Append("Tháng thứ 7: ");
                    foreach (decimal m7 in lstMarkMonth7)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = id;
                        mark.Semester = idSemester;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = idClass;
                        mark.SubjectID = idSubject;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.Mark = m7;
                        mark.Title = SystemParamsInFile.TX;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 3, 1);
                        ListMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m7 + " ");
                    }

                    i = 0;
                    MarkMonth8 = fc[strMonth8];
                    if (MarkMonth8 == "")
                    {
                        MarkMonth8 = "-1";
                    }
                    List<decimal> lstMarkMonth8 = this.ConvertMark(MarkMonth8);
                    userDescriptionsStr.Append("Tháng thứ 8: ");
                    foreach (decimal m8 in lstMarkMonth8)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = id;
                        mark.Semester = idSemester;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = idClass;
                        mark.SubjectID = idSubject;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.Mark = m8;
                        mark.Title = SystemParamsInFile.TX;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 4, 1);
                        ListMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m8 + " ");
                    }

                    i = 0;
                    MarkMonth9 = fc[strMonth9];
                    if (MarkMonth9 == "")
                    {
                        MarkMonth9 = "-1";
                    }
                    List<decimal> lstMarkMonth9 = this.ConvertMark(MarkMonth9);
                    userDescriptionsStr.Append("Tháng thứ 9: ");
                    foreach (decimal m9 in lstMarkMonth9)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = id;
                        mark.Semester = idSemester;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = idClass;
                        mark.SubjectID = idSubject;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.Mark = m9;
                        mark.Title = SystemParamsInFile.TX;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 5, 1);
                        ListMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m9 + " ");
                    }
                }
                #endregion

                #endregion

                #region Diem KTDK

                #region ki 1
                if (idSemester == 1)
                {
                    //KTDK GKI
                    //Neu la mon toan
                    if (Toan == true)
                    {
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeGK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.GK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        string strMark = id.ToString() + "GKI";
                        userDescriptionsStr.Append("GKI: ");
                        if (fc[strMark] == null || fc[strMark] == "")
                        {
                            fc[strMark] = "-1";
                        }
                        if (fc[strMark] != null && fc[strMark] != "" && fc[strMark] != ",,")
                        {
                            mrd.Mark = decimal.Parse(fc[strMark]);
                            mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                            ListMarkRecordDK.Add(mrd);
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }
                    }

                    //Neu la mon TV
                    if (TV == true)
                    {
                        //Diem doc
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeDGK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.DGK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        string strMark = id.ToString() + "GKI_D";
                        userDescriptionsStr.Append("GKI_D: ");
                        if (fc[strMark] == null || fc[strMark] == "")
                        {
                            fc[strMark] = "-1";
                        }
                        if (fc[strMark] != null && fc[strMark] != "" && fc[strMark] != ",,")
                        {
                            mrd.Mark = decimal.Parse(fc[strMark]);
                            mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                            ListMarkRecordDK.Add(mrd);
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }

                        //Diem Viet
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeVGK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.VGK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        strMark = id.ToString() + "GKI_V";
                        userDescriptionsStr.Append("GKI_V: ");
                        if (fc[strMark] == null || fc[strMark] == "")
                        {
                            fc[strMark] = "-1";
                        }
                        if (fc[strMark] != null && fc[strMark] != "" && fc[strMark] != ",,")
                        {
                            mrd.Mark = decimal.Parse(fc[strMark]);
                            mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                            ListMarkRecordDK.Add(mrd);
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }

                        // Diem GK
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeGK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.GK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        string strMark1 = id.ToString() + "GKI_D";
                        string strMark2 = id.ToString() + "GKI_V";
                        userDescriptionsStr.Append("GKI: ");
                        if (fc[strMark1] == null || fc[strMark1] == "")
                        {
                            fc[strMark1] = "-1";
                        }
                        if (fc[strMark2] == null || fc[strMark2] == "")
                        {
                            fc[strMark2] = "-1";
                        }
                        if (fc[strMark1] != null && fc[strMark2] != null && fc[strMark2] != "" && fc[strMark2] != ",," && fc[strMark1] == "-1")
                        {
                            decimal val2 = Math.Round(decimal.Parse(fc[strMark2]), MidpointRounding.AwayFromZero);
                            mrd.Mark = val2;
                            ListMarkRecordDK.Add(mrd);
                        }
                        if (fc[strMark1] != null && fc[strMark2] != null && fc[strMark1] != "" && fc[strMark1] != ",," && fc[strMark2] == "-1")
                        {
                            decimal val1 = Math.Round(decimal.Parse(fc[strMark1]), MidpointRounding.AwayFromZero);
                            mrd.Mark = val1;
                            ListMarkRecordDK.Add(mrd);
                        }

                        if (fc[strMark1] != null && fc[strMark2] != null && fc[strMark1] != "" && fc[strMark1] != ",," && fc[strMark1] != "-1"
                            && fc[strMark2] != "" && fc[strMark2] != ",," && fc[strMark2] != "-1")
                        {
                            decimal val1 = decimal.Parse(fc[strMark1]);
                            decimal val2 = decimal.Parse(fc[strMark2]);
                            mrd.Mark = Math.Round(decimal.Parse(((val1 + val2) / 2).ToString()), MidpointRounding.AwayFromZero);
                            ListMarkRecordDK.Add(mrd);
                        }
                        userDescriptionsStr.Append(mrd.Mark + " ");
                    }

                    //KTDK CKI
                    //Neu la mon toan va mon khac
                    if (Toan == true || MK == true)
                    {
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeCK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.CK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        string strMark = id.ToString() + "CKI";
                        userDescriptionsStr.Append("CKI: ");
                        if (fc[strMark] == null || fc[strMark] == "")
                        {
                            fc[strMark] = "-1";
                        }
                        if (fc[strMark] != null && fc[strMark] != "" && fc[strMark] != ",,")
                        {
                            mrd.Mark = decimal.Parse(fc[strMark]);
                            mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                            ListMarkRecordDK.Add(mrd);
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }
                    }

                    //Neu la mon TV
                    if (TV == true)
                    {
                        //Diem doc
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeDCK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.DCK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        string strMark = id.ToString() + "CKI_D";
                        userDescriptionsStr.Append("CKI_D: ");
                        if (fc[strMark] == null || fc[strMark] == "")
                        {
                            fc[strMark] = "-1";
                        }
                        if (fc[strMark] != null && fc[strMark] != "" && fc[strMark] != ",,")
                        {
                            mrd.Mark = decimal.Parse(fc[strMark]);
                            mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                            ListMarkRecordDK.Add(mrd);
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }

                        //Diem Viet
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeVCK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.VCK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        strMark = id.ToString() + "CKI_V";
                        userDescriptionsStr.Append("CKI_V: ");
                        if (fc[strMark] == null || fc[strMark] == "")
                        {
                            fc[strMark] = "-1";
                        }
                        if (fc[strMark] != null && fc[strMark] != "" && fc[strMark] != ",,")
                        {
                            mrd.Mark = decimal.Parse(fc[strMark]);
                            mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                            ListMarkRecordDK.Add(mrd);
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }

                        // Diem CK
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeCK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.CK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        string strMark1 = id.ToString() + "CKI_D";
                        string strMark2 = id.ToString() + "CKI_V";
                        userDescriptionsStr.Append("CKI: ");
                        if (fc[strMark1] == null || fc[strMark1] == "")
                        {
                            fc[strMark1] = "-1";
                        }
                        if (fc[strMark2] == null || fc[strMark2] == "")
                        {
                            fc[strMark2] = "-1";
                        }
                        if (fc[strMark1] != null && fc[strMark2] != null && fc[strMark2] != "" && fc[strMark2] != ",," && fc[strMark1] == "-1")
                        {
                            decimal val2 = Math.Round(decimal.Parse(fc[strMark2]), MidpointRounding.AwayFromZero);
                            mrd.Mark = val2;
                            ListMarkRecordDK.Add(mrd);
                        }
                        if (fc[strMark1] != null && fc[strMark2] != null && fc[strMark1] != "" && fc[strMark1] != ",," && fc[strMark2] == "-1")
                        {
                            decimal val1 = Math.Round(decimal.Parse(fc[strMark1]), MidpointRounding.AwayFromZero);
                            mrd.Mark = val1;
                            ListMarkRecordDK.Add(mrd);
                        }

                        if (fc[strMark1] != null && fc[strMark2] != null && fc[strMark1] != "" && fc[strMark1] != ",," && fc[strMark1] != "-1"
                            && fc[strMark2] != "" && fc[strMark2] != ",," && fc[strMark2] != "-1")
                        {
                            decimal val1 = decimal.Parse(fc[strMark1]);
                            decimal val2 = decimal.Parse(fc[strMark2]);
                            mrd.Mark = Math.Round(decimal.Parse(((val1 + val2) / 2).ToString()), MidpointRounding.AwayFromZero);
                            ListMarkRecordDK.Add(mrd);
                        }
                        userDescriptionsStr.Append(mrd.Mark + " ");
                    }
                }
                #endregion

                #region ki 2
                if (idSemester == 2)
                {
                    //KTDK GKII
                    //Neu la mon toan
                    if (Toan == true)
                    {
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeGK2.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.GK2;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        string strMark = id.ToString() + "GKII";
                        userDescriptionsStr.Append("GKII: ");
                        if (fc[strMark] == null || fc[strMark] == "")
                        {
                            fc[strMark] = "-1";
                        }
                        if (fc[strMark] != null && fc[strMark] != ",," && fc[strMark] != "")
                        {
                            mrd.Mark = decimal.Parse(fc[strMark]);
                            mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                            ListMarkRecordDK.Add(mrd);
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }

                    }
                    //Neu la mon TV
                    if (TV == true)
                    {
                        //Diem doc
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeDGK2.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.DGK2;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        string strMark = id.ToString() + "GKII_D";
                        userDescriptionsStr.Append("GKII_D: ");
                        if (fc[strMark] == null || fc[strMark] == "")
                        {
                            fc[strMark] = "-1";
                        }
                        if (fc[strMark] != null && fc[strMark] != "" && fc[strMark] != ",,")
                        {
                            mrd.Mark = decimal.Parse(fc[strMark]);
                            mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                            ListMarkRecordDK.Add(mrd);
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }

                        //Diem Viet
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeVGK2.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.VGK2;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        strMark = id.ToString() + "GKII_V";
                        userDescriptionsStr.Append("GKII_V: ");
                        if (fc[strMark] == null || fc[strMark] == "")
                        {
                            fc[strMark] = "-1";
                        }
                        if (fc[strMark] != null && fc[strMark] != "" && fc[strMark] != ",,")
                        {
                            mrd.Mark = decimal.Parse(fc[strMark]);
                            mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                            ListMarkRecordDK.Add(mrd);
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }

                        // Diem GK
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeGK2.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.GK2;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        string strMark1 = id.ToString() + "GKII_D";
                        string strMark2 = id.ToString() + "GKII_V";
                        userDescriptionsStr.Append("GKII: ");
                        if (fc[strMark1] == null || fc[strMark1] == "")
                        {
                            fc[strMark1] = "-1";
                        }
                        if (fc[strMark2] == null || fc[strMark2] == "")
                        {
                            fc[strMark2] = "-1";
                        }
                        if (fc[strMark1] != null && fc[strMark2] != null && fc[strMark2] != "" && fc[strMark2] != ",," && fc[strMark1] == "-1")
                        {
                            decimal val2 = Math.Round(decimal.Parse(fc[strMark2]), MidpointRounding.AwayFromZero);
                            mrd.Mark = val2;
                            ListMarkRecordDK.Add(mrd);
                        }
                        if (fc[strMark1] != null && fc[strMark2] != null && fc[strMark1] != "" && fc[strMark1] != ",," && fc[strMark2] == "-1")
                        {
                            decimal val1 = Math.Round(decimal.Parse(fc[strMark1]), MidpointRounding.AwayFromZero);
                            mrd.Mark = val1;
                            ListMarkRecordDK.Add(mrd);
                        }

                        if (fc[strMark1] != null && fc[strMark2] != null && fc[strMark1] != "" && fc[strMark1] != ",," && fc[strMark1] != "-1"
                            && fc[strMark2] != "" && fc[strMark2] != ",," && fc[strMark2] != "-1")
                        {
                            decimal val1 = decimal.Parse(fc[strMark1]);
                            decimal val2 = decimal.Parse(fc[strMark2]);
                            mrd.Mark = Math.Round(decimal.Parse(((val1 + val2) / 2).ToString()), MidpointRounding.AwayFromZero);
                            ListMarkRecordDK.Add(mrd);
                        }
                        userDescriptionsStr.Append(mrd.Mark + " ");
                    }
                    //KTDK CKII
                    //Neu la mon toan
                    if (Toan == true || MK == true)
                    {
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeCN.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.CN;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        string strMark = id.ToString() + "CKII";
                        userDescriptionsStr.Append("CKII: ");
                        if (fc[strMark] == null || fc[strMark] == "")
                        {
                            fc[strMark] = "-1";
                        }
                        if (fc[strMark] != null && fc[strMark] != "" && fc[strMark] != ",,")
                        {
                            mrd.Mark = decimal.Parse(fc[strMark]);
                            mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                            ListMarkRecordDK.Add(mrd);
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }

                    }
                    //Neu la mon TV
                    if (TV == true)
                    {
                        //Diem doc
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeDCN.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.DCN;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        string strMark = id.ToString() + "CKII_D";
                        userDescriptionsStr.Append("CKII: ");
                        if (fc[strMark] == null || fc[strMark] == "")
                        {
                            fc[strMark] = "-1";
                        }
                        if (fc[strMark] != null && fc[strMark] != "" && fc[strMark] != ",,")
                        {
                            mrd.Mark = decimal.Parse(fc[strMark]);
                            mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                            ListMarkRecordDK.Add(mrd);
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }

                        //Diem Viet
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeVCN.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.VCN;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        strMark = id.ToString() + "CKII_V";
                        userDescriptionsStr.Append("CKII_V: ");
                        if (fc[strMark] == null || fc[strMark] == "")
                        {
                            fc[strMark] = "-1";
                        }
                        if (fc[strMark] != null && fc[strMark] != "" && fc[strMark] != ",,")
                        {
                            mrd.Mark = decimal.Parse(fc[strMark]);
                            mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                            ListMarkRecordDK.Add(mrd);
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }

                        // Diem CK
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = idClass;
                        mrd.SubjectID = idSubject;
                        mrd.PupilID = id;
                        mrd.MarkTypeID = markTypeCN.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.CN;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = idSemester;
                        string strMark1 = id.ToString() + "CKII_D";
                        string strMark2 = id.ToString() + "CKII_V";
                        userDescriptionsStr.Append("CKII: ");
                        if (fc[strMark1] == null || fc[strMark1] == "")
                        {
                            fc[strMark1] = "-1";
                        }
                        if (fc[strMark2] == null || fc[strMark2] == "")
                        {
                            fc[strMark2] = "-1";
                        }
                        if (fc[strMark1] != null && fc[strMark2] != null && fc[strMark2] != "" && fc[strMark2] != ",," && fc[strMark1] == "-1")
                        {
                            decimal val2 = Math.Round(decimal.Parse(fc[strMark2]), MidpointRounding.AwayFromZero);
                            mrd.Mark = val2;
                            ListMarkRecordDK.Add(mrd);
                        }
                        if (fc[strMark1] != null && fc[strMark2] != null && fc[strMark1] != "" && fc[strMark1] != ",," && fc[strMark2] == "-1")
                        {
                            decimal val1 = Math.Round(decimal.Parse(fc[strMark1]), MidpointRounding.AwayFromZero);
                            mrd.Mark = val1;
                            ListMarkRecordDK.Add(mrd);
                        }

                        if (fc[strMark1] != null && fc[strMark2] != null && fc[strMark1] != "" && fc[strMark1] != ",," && fc[strMark1] != "-1"
                            && fc[strMark2] != "" && fc[strMark2] != ",," && fc[strMark2] != "-1")
                        {
                            decimal val1 = decimal.Parse(fc[strMark1]);
                            decimal val2 = decimal.Parse(fc[strMark2]);
                            mrd.Mark = Math.Round(decimal.Parse(((val1 + val2) / 2).ToString()), MidpointRounding.AwayFromZero);
                            ListMarkRecordDK.Add(mrd);
                        }
                        userDescriptionsStr.Append(mrd.Mark + " ");
                    }
                }
                #endregion

                #endregion

                SummedUpRecord sur = new SummedUpRecord();
                sur.PupilID = id;
                sur.ClassID = idClass;
                sur.AcademicYearID = AcademicYear.AcademicYearID;
                sur.SchoolID = globalInfo.SchoolID.Value;
                sur.SubjectID = idSubject;
                sur.IsCommenting = classSubject.IsCommenting.Value;
                sur.CreatedAcademicYear = AcademicYear.Year;
                sur.Semester = idSemester;
                lstSummedUpRecord.Add(sur);

                // Tạo dữ liệu ghi log
                if (iLog < lsPupilID.Length - 1)
                {
                    objectIDStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    oldObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    newObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userDescriptionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);                    
                }
                iLog++;
            }

            using (TransactionScope scope = new TransactionScope())
            {
                if (!isMovedHistory)
                {
                    MarkRecordBusiness.InsertMarkRecordTXOfPrimary(ListMarkRecord);
                    MarkRecordBusiness.InsertMarkRecordDKOfPrimary(globalInfo.UserAccountID, ListMarkRecordDK, lstSummedUpRecord, idSemester);
                    MarkRecordBusiness.Save();
                    scope.Complete();
                }
                else
                {
                    List<MarkRecordHistory> ListMarkRecordHistory = new List<MarkRecordHistory>();
                    MarkRecord objMarkRecord = new MarkRecord();
                    MarkRecordHistory objMarkRecordHistory = null;
                    for (int i = 0; i < ListMarkRecord.Count; i++)
                    {
                        objMarkRecordHistory = new MarkRecordHistory();
                        objMarkRecord = ListMarkRecord[i];
                        //objMarkRecordHistory.AcademicYear = objMarkRecord.AcademicYear;
                        objMarkRecordHistory.AcademicYearID = objMarkRecord.AcademicYearID;
                        objMarkRecordHistory.ClassID = objMarkRecord.ClassID;
                        //objMarkRecordHistory.ClassProfile = objMarkRecord.ClassProfile;
                        objMarkRecordHistory.CreatedAcademicYear = objMarkRecord.CreatedAcademicYear;
                        objMarkRecordHistory.CreatedDate = objMarkRecord.CreatedDate;
                        objMarkRecordHistory.IsOldData = objMarkRecord.IsOldData;
                        objMarkRecordHistory.IsSMS = objMarkRecord.IsSMS;
                        objMarkRecordHistory.Last2digitNumberSchool = objMarkRecord.Last2digitNumberSchool;
                        objMarkRecordHistory.M_IsByC = objMarkRecord.M_IsByC;
                        objMarkRecordHistory.M_MarkRecord = objMarkRecord.M_MarkRecord;
                        objMarkRecordHistory.M_OldID = objMarkRecord.M_OldID;
                        objMarkRecordHistory.M_ProvinceID = objMarkRecord.M_ProvinceID;
                        objMarkRecordHistory.Mark = objMarkRecord.Mark;
                        objMarkRecordHistory.MarkedDate = objMarkRecord.MarkedDate;
                        objMarkRecordHistory.MarkRecordID = objMarkRecord.MarkRecordID;
                        //objMarkRecordHistory.MarkType = objMarkRecord.MarkType;
                        objMarkRecordHistory.MarkTypeID = objMarkRecord.MarkTypeID;
                        objMarkRecordHistory.ModifiedDate = objMarkRecord.ModifiedDate;
                        objMarkRecordHistory.MSourcedb = objMarkRecord.MSourcedb;
                        objMarkRecordHistory.OldMark = objMarkRecord.OldMark;
                        objMarkRecordHistory.OrderNumber = objMarkRecord.OrderNumber;
                        objMarkRecordHistory.PupilID = objMarkRecord.PupilID;
                        //objMarkRecordHistory.PupilProfile = objMarkRecord.PupilProfile;
                        objMarkRecordHistory.SchoolID = objMarkRecord.SchoolID;
                        //objMarkRecordHistory.SchoolProfile = objMarkRecord.SchoolProfile;
                        objMarkRecordHistory.Semester = objMarkRecord.Semester;
                        //objMarkRecordHistory.SubjectCat = objMarkRecord.SubjectCat;
                        objMarkRecordHistory.SubjectID = objMarkRecord.SubjectID;
                        objMarkRecordHistory.SynchronizeID = objMarkRecord.SynchronizeID;
                        objMarkRecordHistory.Title = objMarkRecord.Title;
                        ListMarkRecordHistory.Add(objMarkRecordHistory);
                    }
                    List<MarkRecordHistory> ListMarkRecordDKHistory = new List<MarkRecordHistory>();
                    MarkRecord objMarkRecordDK = new MarkRecord();
                    MarkRecordHistory objMarkRecordDKHistory = null;
                    for (int j = 0; j < ListMarkRecordDK.Count; j++)
                    {
                        objMarkRecordDKHistory = new MarkRecordHistory();
                        objMarkRecordDK = ListMarkRecordDK[j];
                        //objMarkRecordDKHistory.AcademicYear = objMarkRecordDK.AcademicYear;
                        objMarkRecordDKHistory.AcademicYearID = objMarkRecordDK.AcademicYearID;
                        objMarkRecordDKHistory.ClassID = objMarkRecordDK.ClassID;
                        //objMarkRecordDKHistory.ClassProfile = objMarkRecordDK.ClassProfile;
                        objMarkRecordDKHistory.CreatedAcademicYear = objMarkRecordDK.CreatedAcademicYear;
                        objMarkRecordDKHistory.CreatedDate = objMarkRecordDK.CreatedDate;
                        objMarkRecordDKHistory.IsOldData = objMarkRecordDK.IsOldData;
                        objMarkRecordDKHistory.IsSMS = objMarkRecordDK.IsSMS;
                        objMarkRecordDKHistory.Last2digitNumberSchool = objMarkRecordDK.Last2digitNumberSchool;
                        objMarkRecordDKHistory.M_IsByC = objMarkRecordDK.M_IsByC;
                        objMarkRecordDKHistory.M_MarkRecord = objMarkRecordDK.M_MarkRecord;
                        objMarkRecordDKHistory.M_OldID = objMarkRecordDK.M_OldID;
                        objMarkRecordDKHistory.M_ProvinceID = objMarkRecordDK.M_ProvinceID;
                        objMarkRecordDKHistory.Mark = objMarkRecordDK.Mark;
                        objMarkRecordDKHistory.MarkedDate = objMarkRecordDK.MarkedDate;
                        objMarkRecordDKHistory.MarkRecordID = objMarkRecordDK.MarkRecordID;
                        //objMarkRecordDKHistory.MarkType = objMarkRecordDK.MarkType;
                        objMarkRecordDKHistory.MarkTypeID = objMarkRecordDK.MarkTypeID;
                        objMarkRecordDKHistory.ModifiedDate = objMarkRecordDK.ModifiedDate;
                        objMarkRecordDKHistory.MSourcedb = objMarkRecordDK.MSourcedb;
                        objMarkRecordDKHistory.OldMark = objMarkRecordDK.OldMark;
                        objMarkRecordDKHistory.OrderNumber = objMarkRecordDK.OrderNumber;
                        objMarkRecordDKHistory.PupilID = objMarkRecordDK.PupilID;
                        //objMarkRecordDKHistory.PupilProfile = objMarkRecordDK.PupilProfile;
                        objMarkRecordDKHistory.SchoolID = objMarkRecordDK.SchoolID;
                        //objMarkRecordDKHistory.SchoolProfile = objMarkRecordDK.SchoolProfile;
                        objMarkRecordDKHistory.Semester = objMarkRecordDK.Semester;
                        //objMarkRecordDKHistory.SubjectCat = objMarkRecordDK.SubjectCat;
                        objMarkRecordDKHistory.SubjectID = objMarkRecordDK.SubjectID;
                        objMarkRecordDKHistory.SynchronizeID = objMarkRecordDK.SynchronizeID;
                        objMarkRecordDKHistory.Title = objMarkRecordDK.Title;
                        ListMarkRecordDKHistory.Add(objMarkRecordDKHistory);
                    }

                    List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                    SummedUpRecord objSumUpRecord = new SummedUpRecord();
                    SummedUpRecordHistory objSumUpRecordHistory = null;
                    for (int k = 0; k < lstSummedUpRecord.Count; k++)
                    {
                        objSumUpRecord = lstSummedUpRecord[k];
                        objSumUpRecordHistory = new SummedUpRecordHistory();
                        //objSumUpRecordHistory.AcademicYear = objSumUpRecord.AcademicYear;
                        objSumUpRecordHistory.AcademicYearID = objSumUpRecord.AcademicYearID;
                        objSumUpRecordHistory.ClassID = objSumUpRecord.ClassID;
                        //objSumUpRecordHistory.ClassProfile = objSumUpRecord.ClassProfile;
                        objSumUpRecordHistory.Comment = objSumUpRecord.Comment;
                        objSumUpRecordHistory.CreatedAcademicYear = objSumUpRecord.CreatedAcademicYear;
                        objSumUpRecordHistory.IsCommenting = objSumUpRecord.IsCommenting;
                        objSumUpRecordHistory.IsOldData = objSumUpRecord.IsOldData;
                        objSumUpRecordHistory.JudgementResult = objSumUpRecord.JudgementResult;
                        objSumUpRecordHistory.Last2digitNumberSchool = objSumUpRecord.Last2digitNumberSchool;
                        objSumUpRecordHistory.M_OldID = objSumUpRecord.M_OldID;
                        objSumUpRecordHistory.M_ProvinceID = objSumUpRecord.M_ProvinceID;
                        objSumUpRecordHistory.MSourcedb = objSumUpRecord.MSourcedb;
                        //objSumUpRecordHistory.PeriodDeclaration = objSumUpRecord.PeriodDeclaration;
                        objSumUpRecordHistory.PeriodID = objSumUpRecord.PeriodID;
                        objSumUpRecordHistory.PupilID = objSumUpRecord.PupilID;
                        //objSumUpRecordHistory.PupilProfile = objSumUpRecord.PupilProfile;
                        objSumUpRecordHistory.ReTestJudgement = objSumUpRecord.ReTestJudgement;
                        objSumUpRecordHistory.ReTestMark = objSumUpRecord.ReTestMark;
                        objSumUpRecordHistory.SchoolID = objSumUpRecord.SchoolID;
                        //objSumUpRecordHistory.SchoolProfile = objSumUpRecord.SchoolProfile;
                        objSumUpRecordHistory.Semester = objSumUpRecord.Semester;
                        //objSumUpRecordHistory.SubjectCat = objSumUpRecord.SubjectCat;
                        objSumUpRecordHistory.SubjectID = objSumUpRecord.SubjectID;
                        objSumUpRecordHistory.SummedUpDate = objSumUpRecord.SummedUpDate;
                        objSumUpRecordHistory.SummedUpMark = objSumUpRecord.SummedUpMark;
                        objSumUpRecordHistory.SummedUpRecordID = objSumUpRecord.SummedUpRecordID;
                        objSumUpRecordHistory.SynchronizeID = objSumUpRecord.SynchronizeID;
                        lstSummedUpRecordHistory.Add(objSumUpRecordHistory);
                    }
                    MarkRecordHistoryBusiness.InsertMarkRecordTXOfPrimaryHistory(ListMarkRecordHistory);
                    MarkRecordHistoryBusiness.InsertMarkRecordDKOfPrimaryHistory(globalInfo.UserAccountID, ListMarkRecordDKHistory, lstSummedUpRecordHistory, idSemester);
                    MarkRecordHistoryBusiness.Save();
                    scope.Complete();
                }

                // Tạo dữ liệu ghi log
                userDescriptionsStr.Replace("-1", " ");
                SetViewDataActionAudit(oldObjectStr.ToString(), newObjectStr.ToString(), objectIDStr.ToString(), descriptionStr.ToString(), paramsStr.ToString(), userFuntionsStr.ToString(), userActionsStr.ToString(), userDescriptionsStr.ToString());
            }

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateMarkSuccess")));
        }

        /// <summary>
        /// Deletes the specified fc.
        /// </summary>
        /// <param name="fc">The fc.</param>
        /// <returns></returns>
        /// <author>hath</author>
        /// <date>4/10/2013</date>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(FormCollection fc)
        {

            GlobalInfo Global = new GlobalInfo();
            int idSemester = int.Parse(fc["Semester"]);
            int idClass = int.Parse(fc["ClassID"]);
            int idSubject = int.Parse(fc["SubjectID"]);
            var liststringid = fc["chkSatus"];
            if (fc["chkSatus"] == "" || fc["chkSatus"] == null)
            {
                throw new BusinessException("MarkRecord_Label_NoPupil");
            }
            List<int> lstPupilID = liststringid.Split(',').Where(u => !string.IsNullOrEmpty(u)).Select(u => int.Parse(u)).ToList();
            if (lstPupilID.Count() > 0)
            {
                // Tạo dữ liệu ghi log
                List<PupilProfile> listPupilProfile = PupilProfileBusiness.All.Where(o => lstPupilID.Contains(o.PupilProfileID)).ToList();
                ClassProfile classProfile = ClassProfileBusiness.Find(idClass);
                SubjectCat subject = SubjectCatBusiness.Find(idSubject);
                StringBuilder oldObject = new StringBuilder();
                StringBuilder objectID = new StringBuilder();
                StringBuilder descriptionObject = new StringBuilder();
                StringBuilder paramObject = new StringBuilder();
                StringBuilder userFunctions = new StringBuilder();
                StringBuilder userActions = new StringBuilder();
                StringBuilder userDescriptions = new StringBuilder();
                StringBuilder markStr = new StringBuilder();
                int modSchoolID = Global.SchoolID.Value % 100;
                //var query = from m in MarkRecordBusiness.All
                //            where lstPupilID.Contains(m.PupilID) && m.Last2digitNumberSchool == modSchoolID && m.AcademicYearID == AcademicYearID
                //            select m;
                //List<MarkRecord> allMarks = query.ToList();
                for (int i = 0, size = listPupilProfile.Count; i < size; i++)
                {
                    //oldObject.Append("{\"PupilProfileID\":\"" + listPupilProfile[i].PupilProfileID.ToString() + "\",\"PupilCode\":\"" + listPupilProfile[i].PupilCode + ",");
                    //MarkRecord mark = null;
                    //markStr = new StringBuilder();
                    //List<MarkRecord> marks = allMarks.Where(a => a.PupilID == listPupilProfile[i].PupilProfileID).ToList();
                    //for (int j = 0; j < marks.Count; j++)
                    //{
                    //    mark = marks[j];
                    //    oldObject.Append("{\"MarkRecordID\":\"" + mark.MarkRecordID + "\",\"" + mark.Title + "\":\"" + mark.Mark + "\"}");
                    //    markStr.Append(mark.Title + ":" + mark.Mark + " ");
                    //    if (j < marks.Count - 1)
                    //    {
                    //        oldObject.Append(",");
                    //    }
                    //}
                    //oldObject.Append("\"}");

                    objectID.Append(listPupilProfile[i].PupilProfileID.ToString());
                    descriptionObject.Append("Delete mark_record:" + listPupilProfile[i].PupilProfileID.ToString());
                    paramObject.Append(listPupilProfile[i].PupilProfileID.ToString());
                    userFunctions.Append("Sổ điểm");
                    userActions.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                    userDescriptions.Append("Xóa toàn bộ điểm học sinh " + listPupilProfile[i].FullName
                        + " mã " + listPupilProfile[i].PupilCode
                        + " lớp " + classProfile.DisplayName
                        + " môn " + subject.SubjectName
                        + " năm " + classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1)
                        + " HK " + idSemester + " "
                        + markStr.ToString());
                    if (i < size - 1)
                    {
                        oldObject.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionObject.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramObject.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        objectID.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFunctions.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActions.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userDescriptions.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    }
                }
                // end
                AcademicYear AcademicYear = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);           
                bool isMovedHistory = UtilsBusiness.IsMoveHistory(AcademicYear);
                using (TransactionScope scope = new TransactionScope())
                {
                    if (!isMovedHistory)
                    {
                        MarkRecordBusiness.DeleteMarkRecord(Global.UserAccountID, Global.SchoolID.Value, Global.AcademicYearID.Value, idClass, idSemester, null, idSubject, lstPupilID);
                        // Kiem tra neu nhu con diem hoc ky cua lop bi khoa thi khong can phai xoa diem tong ket nua
                        string LockTitleS = MarkRecordBusiness.GetLockMarkTitlePrimary(Global.SchoolID.Value, Global.AcademicYearID.Value, idClass, idSubject, idSemester);
                        bool isDelSum = true;
                        if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            if (LockTitleS.Contains(SystemParamsInFile.CKI + ","))
                            {
                                isDelSum = false;
                            }
                        }
                        else
                        {
                            if (LockTitleS.Contains(SystemParamsInFile.CKII + ","))
                            {
                                isDelSum = false;
                            }
                        }
                        if (isDelSum)
                        {
                            SummedUpRecordBusiness.DeleteSummedUpRecordPrimary(Global.UserAccountID, Global.SchoolID.Value, Global.AcademicYearID.Value, idClass, idSemester, null, idSubject, lstPupilID);
                        }
                        MarkRecordBusiness.Save();
                        scope.Complete();
                    }
                    else
                    {
                        MarkRecordHistoryBusiness.DeleteMarkRecordPrimaryHistory(Global.UserAccountID, Global.SchoolID.Value, Global.AcademicYearID.Value, idClass, idSemester, null, idSubject, lstPupilID);
                        // Kiem tra neu nhu con diem hoc ky cua lop bi khoa thi khong can phai xoa diem tong ket nua
                        string LockTitleS = MarkRecordBusiness.GetLockMarkTitlePrimary(Global.SchoolID.Value, Global.AcademicYearID.Value, idClass, idSubject, idSemester);
                        bool isDelSum = true;
                        if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            if (LockTitleS.Contains(SystemParamsInFile.CKI + ","))
                            {
                                isDelSum = false;
                            }
                        }
                        else
                        {
                            if (LockTitleS.Contains(SystemParamsInFile.CKII + ","))
                            {
                                isDelSum = false;
                            }
                        }
                        if (isDelSum)
                        {
                            SummedUpRecordHistoryBusiness.DeleteSummedUpRecordPrimaryHistory(Global.UserAccountID, Global.SchoolID.Value, Global.AcademicYearID.Value, idClass, idSemester, null, idSubject, lstPupilID);
                        }
                        MarkRecordHistoryBusiness.Save();
                        scope.Complete();
                    }


                    // Tạo dữ liệu ghi log            
                    SetViewDataActionAudit(oldObject.ToString(), String.Empty, objectID.ToString(), descriptionObject.ToString(), paramObject.ToString(), userFunctions.ToString(), userActions.ToString(), userDescriptions.ToString());
                }
            }
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// ConvertMark
        /// </summary>
        /// <param name="Mark">The mark.</param>
        /// <returns>
        /// Decimal}
        /// </returns>
        /// <author>hath</author>
        /// <date>4/10/2013</date>
        public List<decimal> ConvertMark(string Mark)
        {
            if (Mark == null)
            {
                Mark = "";
            }
            string[] lsMark = Mark.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            List<decimal> lstMark = new List<decimal>();
            if (lsMark.Length == 0)
            {
                lstMark.Add(-1);
                return lstMark;
            }
            foreach (string ma in lsMark)
            {
                decimal output = 0;
                if (decimal.TryParse(ma, out output))
                {
                    lstMark.Add(decimal.Parse(ma));
                }
            }
            return lstMark;
        }

        
        public FileResult ExportExcel(int? subjectid, int? classid, int? idSemester)
        {
            GlobalInfo global = new GlobalInfo();
            //idSemester = 2;
            SubjectCat SubjectCat = SubjectCatBusiness.Find(subjectid);
            ClassProfile ClassProfile = ClassProfileBusiness.Find(classid);
            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID);
            string templatePath = "";
            DateTime? endDate = null;
            if (idSemester == 1)
            {
                endDate = academicYear.FirstSemesterEndDate;
            }
            if (idSemester == 2)
            {
                endDate = academicYear.SecondSemesterEndDate;
            }
            //check mon hoc
            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(classid.Value, new Dictionary<string, object> { { "SubjectID", subjectid.Value }, { "SchoolID", academicYear.SchoolID } }).SingleOrDefault();

            bool Toan = classSubject.SubjectCat.DisplayName.Equals(SystemParamsInFile.MON_TOAN, StringComparison.InvariantCultureIgnoreCase);
            bool TV = classSubject.SubjectCat.DisplayName.Equals(SystemParamsInFile.MON_TIENGVIET, StringComparison.InvariantCultureIgnoreCase);
            bool MK = !Toan && !TV;

            if (TV == true)
            {
                if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HS" + "/" + "BangDiemMonTiengVietHK1.xls";
                }
                else
                {
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HS" + "/" + "BangDiemMonTiengVietHK2.xls";
                }
            }
            if (Toan == true)
            {
                if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HS" + "/" + "BangDiemMonToanHK1.xls";
                }
                else
                {
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HS" + "/" + "BangDiemMonToanHK2.xls";
                }
            }
            if (MK == true)
            {
                if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HS" + "/" + "BangDiemMonHK1.xls";
                }
                else
                {
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HS" + "/" + "BangDiemMonHK2.xls";
                }
            }

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet 
            IVTWorksheet sheet = oBook.GetSheet(1);
            SchoolProfile school = SchoolProfileBusiness.Find(new GlobalInfo().SchoolID);

            //Fill dữ liệu chung
            string schoolName = school.SchoolName.ToUpper();
            string SemesterName;
            if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SemesterName = "HỌC KỲ 1";
            }
            else
            { SemesterName = "HỌC KỲ 2"; }
            sheet.SetCellValue("A2", schoolName);
            sheet.SetCellValue("A3", "BẢNG ĐÁNH GIÁ HỌC LỰC MÔN " + SubjectCat.SubjectName.ToUpper() + " " + SemesterName.ToUpper() + " " + ClassProfile.DisplayName.ToUpper());
            sheet.SetCellValue("A4", "NĂM HỌC " + academicYear.Year + " - " + (academicYear.Year + 1));
            sheet.GetRange(2, 1, 4, 1);

            IVTRange range = sheet.GetRange("A92", "R92");
            IVTRange range5 = sheet.GetRange("A94", "R94");
            IVTRange rangeLeavingOff = sheet.GetRange("A96", "R96");
            IVTRange rangeLeavingOff5 = sheet.GetRange("A98", "R98");
            int firstRow = 8;
            int index = 0;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = global.AcademicYearID;
            dic["SchoolID"] = global.SchoolID;
            dic["SubjectID"] = subjectid;
            dic["Semester"] = idSemester;
            dic["ClassID"] = classid;

            //IQueryable<MarkRecordBO> lstMarkRecordOfClassFromDB = MarkRecordBusiness.GetMardRecordOfClass(dic).OrderBy(x => x.OrderInClass).ThenBy(x => x.Name);
            List<MarkRecordBO> lstMarkRecordOfClass = MarkRecordBusiness.GetMardRecordOfClass(dic).OrderBy(x => x.OrderInClass).ThenBy(x => x.Name).ToList();
            List<MarkRecorkTXDK> ListMarkRecorkTXDK = new List<MarkRecorkTXDK>();

            List<SchoolMovement> lstSchoolMovement = SchoolMovementBusiness.All.Where(o => o.SchoolID == globalInfo.SchoolID &&
                                                                                o.AcademicYearID == globalInfo.AcademicYearID &&
                                                                                o.ClassID == classid).ToList();
            List<ClassMovement> lstClassMovement = ClassMovementBusiness.All.Where(o => o.SchoolID == globalInfo.SchoolID &&
                                                                            o.AcademicYearID == globalInfo.AcademicYearID &&
                                                                            o.FromClassID == classid).ToList();
            List<PupilLeavingOff> lstPupilLeavingOff = PupilLeavingOffBusiness.All.Where(o => o.SchoolID == globalInfo.SchoolID &&
                                                                                o.AcademicYearID == globalInfo.AcademicYearID &&
                                                                                o.ClassID == classid).ToList();
            if (lstMarkRecordOfClass.Count > 0)
            {
                #region Duyet list diem
                // NamTa Sua lai
                foreach (var item in lstMarkRecordOfClass)
                {
                    //Kiem tra trong list da chua hoc sinh day chua

                    if (!ListMarkRecorkTXDK.Where(o => o.PupilID == item.PupilID && o.ClassID == item.ClassID).Any())
                    {
                        var listMark = lstMarkRecordOfClass.Where(o => o.PupilID == item.PupilID && o.ClassID == item.ClassID);
                        #region tao doi tuong
                        //Thêm học sinh vào list
                        MarkRecorkTXDK MarkRecorkTXDK = new MarkRecorkTXDK();
                        MarkRecorkTXDK.ClassID = item.ClassID.Value;
                        MarkRecorkTXDK.PupilID = item.PupilID.Value;
                        MarkRecorkTXDK.PupilCode = item.PupilCode;
                        MarkRecorkTXDK.FullName = item.FullName;
                        MarkRecorkTXDK.BirthDate = item.BrithDate.Value;
                        //Disabled voi hoc sinh duoc mien giam
                        bool MienGiam = ExemptedSubjectBusiness.IsExemptedSubject(item.PupilID.Value, item.ClassID.Value, item.AcademicYearID.Value, subjectid.Value, idSemester.Value);
                        //Disable ô nhập điểm: với trạng thái học sinh chuyển trường, chuyển lớp, thôi học
                        if (item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL || item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS || item.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF || MienGiam == true)
                        {
                            MarkRecorkTXDK.Disable = true;
                        }
                        else
                        {
                            MarkRecorkTXDK.Disable = false;
                        }

                        if (item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL || item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS || item.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF || item.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                        {
                            MarkRecorkTXDK.IsShow = true;
                        }
                        else
                        {
                            MarkRecorkTXDK.IsShow = false;
                        }

                        #region Kiểm tra trạng thái của học sinh chuyển trường,chuyển lớp,thôi học

                        DateTime TimeStatus = new DateTime();
                        if (item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL)
                        {
                            SchoolMovement sm = lstSchoolMovement.Where(u => u.PupilID == item.PupilID).OrderByDescending(u => u.MovedDate).FirstOrDefault();
                            if (sm != null)
                            {
                                TimeStatus = sm.MovedDate;
                            }
                        }
                        if (item.Status == SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                        {
                            ClassMovement cm = lstClassMovement.Where(u => u.PupilID == item.PupilID).OrderByDescending(u => u.MovedDate).FirstOrDefault();
                            if (cm != null)
                            {
                                TimeStatus = cm.MovedDate;
                            }
                        }
                        if (item.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF)
                        {
                            PupilLeavingOff lo = lstPupilLeavingOff.Where(u => u.PupilID == item.PupilID).OrderByDescending(u => u.LeavingDate).FirstOrDefault();
                            if (lo != null)
                            {
                                TimeStatus = lo.LeavingDate;
                            }
                        }
                        if (item.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                        {
                            MarkRecorkTXDK.IsShow = MarkRecorkTXDK.IsShow & TimeStatus >= endDate;
                        }
                        foreach (var Mark in listMark)
                        {
                            #region Do diem vao cho doi tuong
                            if ((MarkRecorkTXDK.IsShow == true))
                            {
                                if (Mark.MarkedDate.HasValue)
                                {
                                    if (Mark.MarkedDate.Value.Month == month1)
                                    {
                                        MarkRecorkTXDK.Month1 = MarkRecorkTXDK.Month1 + " " + Math.Round(Mark.Mark.Value, 0).ToString();
                                    }
                                    if (Mark.MarkedDate.Value.Month == month2)
                                    {
                                        MarkRecorkTXDK.Month2 = MarkRecorkTXDK.Month2 + " " + Math.Round(Mark.Mark.Value, 0).ToString();
                                    }
                                    if (Mark.MarkedDate.Value.Month == month3)
                                    {
                                        MarkRecorkTXDK.Month3 = MarkRecorkTXDK.Month3 + " " + Math.Round(Mark.Mark.Value, 0).ToString();
                                    }
                                    if (Mark.MarkedDate.Value.Month == month4)
                                    {
                                        MarkRecorkTXDK.Month4 = MarkRecorkTXDK.Month4 + " " + Math.Round(Mark.Mark.Value, 0).ToString();
                                    }
                                    if (Mark.MarkedDate.Value.Month == month5)
                                    {
                                        MarkRecorkTXDK.Month5 = MarkRecorkTXDK.Month5 + " " + Math.Round(Mark.Mark.Value, 0).ToString();
                                    }
                                    if (Mark.MarkedDate.Value.Month == month6)
                                    {
                                        MarkRecorkTXDK.Month6 = MarkRecorkTXDK.Month6 + " " + Math.Round(Mark.Mark.Value, 0).ToString();
                                    }
                                    if (Mark.MarkedDate.Value.Month == month7)
                                    {
                                        MarkRecorkTXDK.Month7 = MarkRecorkTXDK.Month7 + " " + Math.Round(Mark.Mark.Value, 0).ToString();
                                    }
                                    if (Mark.MarkedDate.Value.Month == month8)
                                    {
                                        MarkRecorkTXDK.Month8 = MarkRecorkTXDK.Month8 + " " + Math.Round(Mark.Mark.Value, 0).ToString();
                                    }
                                    if (Mark.MarkedDate.Value.Month == month9)
                                    {
                                        MarkRecorkTXDK.Month9 = MarkRecorkTXDK.Month9 + " " + Math.Round(Mark.Mark.Value, 0).ToString();
                                    }
                                }
                                if (!Mark.MarkedDate.HasValue)
                                {
                                    //Thêm điểm học sinh vào các con điểm tương ứng
                                    if (Mark.Title == DGK1)
                                    {
                                        MarkRecorkTXDK.DGK1 = Mark.Mark.Value;
                                    }
                                    if (Mark.Title == VGK1)
                                    {
                                        MarkRecorkTXDK.VGK1 = Mark.Mark.Value;
                                    }
                                    if (Mark.Title == GK1)
                                    {
                                        MarkRecorkTXDK.GK1 = Math.Round(Mark.Mark.Value, 0);
                                    }
                                    if (Mark.Title == DCK1)
                                    {
                                        MarkRecorkTXDK.DCK1 = Mark.Mark.Value;
                                    }
                                    if (Mark.Title == VCK1)
                                    {
                                        MarkRecorkTXDK.VCK1 = Mark.Mark.Value;
                                    }
                                    if (Mark.Title == CK1)
                                    {
                                        MarkRecorkTXDK.CK1 = Math.Round(Mark.Mark.Value, 0);
                                    }
                                    if (Mark.Title == DGK2)
                                    {
                                        MarkRecorkTXDK.DGK2 = Mark.Mark.Value;
                                    }
                                    if (Mark.Title == VGK2)
                                    {
                                        MarkRecorkTXDK.VGK2 = Mark.Mark.Value;
                                    }
                                    if (Mark.Title == GK2)
                                    {
                                        MarkRecorkTXDK.GK2 = Math.Round(Mark.Mark.Value, 0);
                                    }
                                    if (Mark.Title == DCN)
                                    {
                                        MarkRecorkTXDK.DCK2 = Mark.Mark.Value;
                                    }
                                    if (Mark.Title == VCN)
                                    {
                                        MarkRecorkTXDK.VCK2 = Mark.Mark.Value;
                                    }
                                    if (Mark.Title == CN)
                                    {
                                        MarkRecorkTXDK.CK2 = Math.Round(Mark.Mark.Value, 0);
                                    }

                                }

                            }
                            #endregion
                        }
                        #endregion
                        #endregion
                        ListMarkRecorkTXDK.Add(MarkRecorkTXDK);
                    }
                }
                #endregion
            }
            #region Fill Excel
            // HaTH8 fixed 13/6/2013
            for (int i = 0; i < ListMarkRecorkTXDK.Count; i++)
            {
                int currentRow = firstRow + index;
                //Copy row style
                if (currentRow + i >= 8)
                {
                    if ((i + 1) % 5 == 0 || i == ListMarkRecorkTXDK.Count - 1)
                    {
                        if (ListMarkRecorkTXDK[i].Disable == true)
                        {
                            sheet.CopyPasteSameRowHeigh(rangeLeavingOff5, currentRow);
                            sheet.GetRange(currentRow, 1, currentRow, 18).IsLock = true;
                        }
                        else
                        { sheet.CopyPasteSameRowHeigh(range5, currentRow); }
                    }
                    else
                    {
                        if (ListMarkRecorkTXDK[i].Disable == true)
                        {
                            sheet.CopyPasteSameRowHeigh(rangeLeavingOff, currentRow);
                            sheet.GetRange(currentRow, 1, currentRow, 18).IsLock = true;
                        }
                        else
                        {
                            sheet.CopyPasteSameRowHeigh(range, currentRow);
                        }
                    }
                }
                index++;
                //Fill dữ liệu
                sheet.SetCellValue("A" + currentRow, index);
                sheet.SetCellValue("B" + currentRow, ListMarkRecorkTXDK[i].PupilCode);
                sheet.SetCellValue("C" + currentRow, ListMarkRecorkTXDK[i].FullName);
                sheet.SetCellValue("D" + currentRow, String.Format("{0:dd/MM/yyyy}", ListMarkRecorkTXDK[i].BirthDate));
                // Lock tieu de
                sheet.GetRange(2, 1, 4, 1).IsLock = true;

                string titleLockMark = MarkRecordBusiness.GetLockMarkTitlePrimary(global.SchoolID.Value, global.AcademicYearID.Value, classid.Value, subjectid.Value, idSemester.Value);

                int WidthCol = 11;
                if (TV == true)
                {
                    if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        sheet.SetCellValue("E" + currentRow, ListMarkRecorkTXDK[i].Month1);
                        sheet.SetCellValue("F" + currentRow, ListMarkRecorkTXDK[i].Month2);
                        sheet.SetCellValue("G" + currentRow, ListMarkRecorkTXDK[i].Month3);
                        sheet.SetCellValue("H" + currentRow, ListMarkRecorkTXDK[i].DGK1);
                        sheet.SetCellValue("I" + currentRow, ListMarkRecorkTXDK[i].VGK1);
                        sheet.SetCellValue("K" + currentRow, ListMarkRecorkTXDK[i].Month4);
                        sheet.SetCellValue("L" + currentRow, ListMarkRecorkTXDK[i].Month5);
                        sheet.SetCellValue("M" + currentRow, ListMarkRecorkTXDK[i].DCK1);
                        sheet.SetCellValue("N" + currentRow, ListMarkRecorkTXDK[i].VCK1);
                        sheet.GetRange(firstRow, 1, firstRow + ListMarkRecorkTXDK.Count - 1, 4).IsLock = true;
                        sheet.GetRange(firstRow, 10, firstRow + ListMarkRecorkTXDK.Count - 1, 10).IsLock = true;
                        sheet.GetRange(firstRow, 15, firstRow + ListMarkRecorkTXDK.Count - 1, 15).IsLock = true;
                        sheet.GetRange(firstRow, 15, firstRow + ListMarkRecorkTXDK.Count - 1, 17).IsLock = true;
                        //fix do rong cua cot
                        sheet.SetColumnWidth('Q', WidthCol);

                        //check lock mark to lock row

                        if (titleLockMark.Contains("T1"))
                        {
                            sheet.GetRange(firstRow, 5, firstRow + ListMarkRecorkTXDK.Count - 1, 5).IsLock = true;
                        }
                        if (titleLockMark.Contains("T2"))
                        {
                            sheet.GetRange(firstRow, 6, firstRow + ListMarkRecorkTXDK.Count - 1, 6).IsLock = true;
                        }
                        if (titleLockMark.Contains("T3"))
                        {
                            sheet.GetRange(firstRow, 7, firstRow + ListMarkRecorkTXDK.Count - 1, 7).IsLock = true;
                        }
                        if (titleLockMark.Contains("T4"))
                        {
                            sheet.GetRange(firstRow, 11, firstRow + ListMarkRecorkTXDK.Count - 1, 11).IsLock = true;
                        }
                        if (titleLockMark.Contains("T5"))
                        {
                            sheet.GetRange(firstRow, 12, firstRow + ListMarkRecorkTXDK.Count - 1, 12).IsLock = true;
                        }
                        if (titleLockMark.Contains("GK1"))
                        {
                            sheet.GetRange(firstRow, 8, firstRow + ListMarkRecorkTXDK.Count - 1, 8).IsLock = true;
                            sheet.GetRange(firstRow, 9, firstRow + ListMarkRecorkTXDK.Count - 1, 9).IsLock = true;
                        }
                        if (titleLockMark.Contains("CK1"))
                        {
                            sheet.GetRange(firstRow, 13, firstRow + ListMarkRecorkTXDK.Count - 1, 13).IsLock = true;
                            sheet.GetRange(firstRow, 14, firstRow + ListMarkRecorkTXDK.Count - 1, 14).IsLock = true;
                        }
                    }
                    else
                    {
                        sheet.SetCellValue("E" + currentRow, ListMarkRecorkTXDK[i].Month6);
                        sheet.SetCellValue("F" + currentRow, ListMarkRecorkTXDK[i].Month7);
                        sheet.SetCellValue("G" + currentRow, ListMarkRecorkTXDK[i].DGK2);
                        sheet.SetCellValue("H" + currentRow, ListMarkRecorkTXDK[i].VGK2);
                        sheet.SetCellValue("J" + currentRow, ListMarkRecorkTXDK[i].Month8);
                        sheet.SetCellValue("K" + currentRow, ListMarkRecorkTXDK[i].Month9);
                        sheet.SetCellValue("L" + currentRow, ListMarkRecorkTXDK[i].DCK2);
                        sheet.SetCellValue("M" + currentRow, ListMarkRecorkTXDK[i].VCK2);
                        sheet.GetRange(firstRow, 1, firstRow + ListMarkRecorkTXDK.Count - 1, 4).IsLock = true;
                        sheet.GetRange(firstRow, 9, firstRow + ListMarkRecorkTXDK.Count - 1, 9).IsLock = true;
                        sheet.GetRange(firstRow, 14, firstRow + ListMarkRecorkTXDK.Count - 1, 14).IsLock = true;
                        sheet.GetRange(firstRow, 16, firstRow + ListMarkRecorkTXDK.Count - 1, 16).IsLock = true;
                        sheet.GetRange(firstRow, 18, firstRow + ListMarkRecorkTXDK.Count - 1, 18).IsLock = true;
                        //fix do rong cua cot
                        sheet.SetColumnWidth('R', WidthCol);
                        sheet.SetColumnWidth('P', 15);

                        //check lock mark to lock row

                        if (titleLockMark.Contains("T6"))
                        {
                            sheet.GetRange(firstRow, 5, firstRow + ListMarkRecorkTXDK.Count - 1, 5).IsLock = true;
                        }
                        if (titleLockMark.Contains("T7"))
                        {
                            sheet.GetRange(firstRow, 6, firstRow + ListMarkRecorkTXDK.Count - 1, 6).IsLock = true;
                        }
                        if (titleLockMark.Contains("T8"))
                        {
                            sheet.GetRange(firstRow, 7, firstRow + ListMarkRecorkTXDK.Count - 1, 7).IsLock = true;
                        }
                        if (titleLockMark.Contains("T9"))
                        {
                            sheet.GetRange(firstRow, 11, firstRow + ListMarkRecorkTXDK.Count - 1, 11).IsLock = true;
                        }
                        if (titleLockMark.Contains("T10"))
                        {
                            sheet.GetRange(firstRow, 12, firstRow + ListMarkRecorkTXDK.Count - 1, 12).IsLock = true;
                        }
                        if (titleLockMark.Contains("GK2"))
                        {
                            sheet.GetRange(firstRow, 7, firstRow + ListMarkRecorkTXDK.Count - 1, 7).IsLock = true;
                            sheet.GetRange(firstRow, 8, firstRow + ListMarkRecorkTXDK.Count - 1, 8).IsLock = true;
                        }
                        if (titleLockMark.Contains("CN"))
                        {
                            sheet.GetRange(firstRow, 12, firstRow + ListMarkRecorkTXDK.Count - 1, 12).IsLock = true;
                            sheet.GetRange(firstRow, 13, firstRow + ListMarkRecorkTXDK.Count - 1, 13).IsLock = true;
                        }
                    }


                }
                if (Toan == true)
                {
                    if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        sheet.SetCellValue("E" + currentRow, ListMarkRecorkTXDK[i].Month1);
                        sheet.SetCellValue("F" + currentRow, ListMarkRecorkTXDK[i].Month2);
                        sheet.SetCellValue("G" + currentRow, ListMarkRecorkTXDK[i].Month3);
                        sheet.SetCellValue("H" + currentRow, ListMarkRecorkTXDK[i].GK1);
                        sheet.SetCellValue("I" + currentRow, ListMarkRecorkTXDK[i].Month4);
                        sheet.SetCellValue("J" + currentRow, ListMarkRecorkTXDK[i].Month5);
                        sheet.SetCellValue("K" + currentRow, ListMarkRecorkTXDK[i].CK1);
                        sheet.GetRange(firstRow, 1, firstRow + ListMarkRecorkTXDK.Count - 1, 4).IsLock = true;
                        sheet.GetRange(firstRow, 12, firstRow + ListMarkRecorkTXDK.Count - 1, 13).IsLock = true;
                        //fix do rong cua cot
                        sheet.SetColumnWidth('M', WidthCol);
                        //check lock mark to lock row

                        if (titleLockMark.Contains("T1"))
                        {
                            sheet.GetRange(firstRow, 5, firstRow + ListMarkRecorkTXDK.Count - 1, 5).IsLock = true;
                        }
                        if (titleLockMark.Contains("T2"))
                        {
                            sheet.GetRange(firstRow, 6, firstRow + ListMarkRecorkTXDK.Count - 1, 6).IsLock = true;
                        }
                        if (titleLockMark.Contains("T3"))
                        {
                            sheet.GetRange(firstRow, 7, firstRow + ListMarkRecorkTXDK.Count - 1, 7).IsLock = true;
                        }
                        if (titleLockMark.Contains("T4"))
                        {
                            sheet.GetRange(firstRow, 9, firstRow + ListMarkRecorkTXDK.Count - 1, 9).IsLock = true;
                        }
                        if (titleLockMark.Contains("T5"))
                        {
                            sheet.GetRange(firstRow, 10, firstRow + ListMarkRecorkTXDK.Count - 1, 10).IsLock = true;
                        }
                        if (titleLockMark.Contains("GK1"))
                        {
                            sheet.GetRange(firstRow, 8, firstRow + ListMarkRecorkTXDK.Count - 1, 8).IsLock = true;
                        }
                        if (titleLockMark.Contains("CK1"))
                        {
                            sheet.GetRange(firstRow, 11, firstRow + ListMarkRecorkTXDK.Count - 1, 11).IsLock = true;
                        }
                    }
                    else
                    {
                        sheet.SetCellValue("E" + currentRow, ListMarkRecorkTXDK[i].Month6);
                        sheet.SetCellValue("F" + currentRow, ListMarkRecorkTXDK[i].Month7);
                        sheet.SetCellValue("G" + currentRow, ListMarkRecorkTXDK[i].GK2);
                        sheet.SetCellValue("H" + currentRow, ListMarkRecorkTXDK[i].Month8);
                        sheet.SetCellValue("I" + currentRow, ListMarkRecorkTXDK[i].Month9);
                        sheet.SetCellValue("J" + currentRow, ListMarkRecorkTXDK[i].CK2);
                        sheet.GetRange(firstRow, 1, firstRow + ListMarkRecorkTXDK.Count - 1, 4).IsLock = true;
                        sheet.GetRange(firstRow, 11, firstRow + ListMarkRecorkTXDK.Count - 1, 14).IsLock = true;
                        //fix do rong cua cot
                        sheet.SetColumnWidth('L', WidthCol);
                        sheet.SetColumnWidth('N', WidthCol);

                        //check lock mark to lock row

                        if (titleLockMark.Contains("T6"))
                        {
                            sheet.GetRange(firstRow, 5, firstRow + ListMarkRecorkTXDK.Count - 1, 5).IsLock = true;
                        }
                        if (titleLockMark.Contains("T7"))
                        {
                            sheet.GetRange(firstRow, 6, firstRow + ListMarkRecorkTXDK.Count - 1, 6).IsLock = true;
                        }
                        if (titleLockMark.Contains("T8"))
                        {
                            sheet.GetRange(firstRow, 7, firstRow + ListMarkRecorkTXDK.Count - 1, 7).IsLock = true;
                        }
                        if (titleLockMark.Contains("T9"))
                        {
                            sheet.GetRange(firstRow, 9, firstRow + ListMarkRecorkTXDK.Count - 1, 9).IsLock = true;
                        }
                        if (titleLockMark.Contains("T10"))
                        {
                            sheet.GetRange(firstRow, 10, firstRow + ListMarkRecorkTXDK.Count - 1, 10).IsLock = true;
                        }
                        if (titleLockMark.Contains("GK2"))
                        {
                            sheet.GetRange(firstRow, 7, firstRow + ListMarkRecorkTXDK.Count - 1, 7).IsLock = true;
                        }
                        if (titleLockMark.Contains("CN"))
                        {
                            sheet.GetRange(firstRow, 10, firstRow + ListMarkRecorkTXDK.Count - 1, 10).IsLock = true;
                        }
                    }
                }
                if (MK == true)
                {
                    if (idSemester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        sheet.SetCellValue("E" + currentRow, ListMarkRecorkTXDK[i].Month1);
                        sheet.SetCellValue("F" + currentRow, ListMarkRecorkTXDK[i].Month2);
                        sheet.SetCellValue("G" + currentRow, ListMarkRecorkTXDK[i].Month3);
                        sheet.SetCellValue("H" + currentRow, ListMarkRecorkTXDK[i].Month4);
                        sheet.SetCellValue("I" + currentRow, ListMarkRecorkTXDK[i].Month5);
                        sheet.SetCellValue("J" + currentRow, ListMarkRecorkTXDK[i].CK1);
                        sheet.GetRange(firstRow, 1, firstRow + ListMarkRecorkTXDK.Count - 1, 4).IsLock = true;
                        sheet.GetRange(firstRow, 11, firstRow + ListMarkRecorkTXDK.Count - 1, 12).IsLock = true;
                        //fix do rong cua cot
                        sheet.SetColumnWidth('L', WidthCol);

                        //check lock mark to lock row

                        if (titleLockMark.Contains("T1"))
                        {
                            sheet.GetRange(firstRow, 5, firstRow + ListMarkRecorkTXDK.Count - 1, 5).IsLock = true;
                        }
                        if (titleLockMark.Contains("T2"))
                        {
                            sheet.GetRange(firstRow, 6, firstRow + ListMarkRecorkTXDK.Count - 1, 6).IsLock = true;
                        }
                        if (titleLockMark.Contains("T3"))
                        {
                            sheet.GetRange(firstRow, 7, firstRow + ListMarkRecorkTXDK.Count - 1, 7).IsLock = true;
                        }
                        if (titleLockMark.Contains("T4"))
                        {
                            sheet.GetRange(firstRow, 8, firstRow + ListMarkRecorkTXDK.Count - 1, 8).IsLock = true;
                        }
                        if (titleLockMark.Contains("T5"))
                        {
                            sheet.GetRange(firstRow, 9, firstRow + ListMarkRecorkTXDK.Count - 1, 9).IsLock = true;
                        }

                        if (titleLockMark.Contains("CK1"))
                        {
                            sheet.GetRange(firstRow, 10, firstRow + ListMarkRecorkTXDK.Count - 1, 10).IsLock = true;
                        }
                    }
                    else
                    {
                        sheet.SetCellValue("E" + currentRow, ListMarkRecorkTXDK[i].Month6);
                        sheet.SetCellValue("F" + currentRow, ListMarkRecorkTXDK[i].Month7);
                        sheet.SetCellValue("G" + currentRow, ListMarkRecorkTXDK[i].Month8);
                        sheet.SetCellValue("H" + currentRow, ListMarkRecorkTXDK[i].Month9);
                        sheet.SetCellValue("I" + currentRow, ListMarkRecorkTXDK[i].CK2);
                        sheet.GetRange(firstRow, 1, firstRow + ListMarkRecorkTXDK.Count - 1, 4).IsLock = true;
                        sheet.GetRange(firstRow, 10, firstRow + ListMarkRecorkTXDK.Count - 1, 13).IsLock = true;
                        //fix do rong cua cot
                        sheet.SetColumnWidth('K', WidthCol);
                        sheet.SetColumnWidth('M', WidthCol);

                        //check lock mark to lock row

                        if (titleLockMark.Contains("T6"))
                        {
                            sheet.GetRange(firstRow, 5, firstRow + ListMarkRecorkTXDK.Count - 1, 5).IsLock = true;
                        }
                        if (titleLockMark.Contains("T7"))
                        {
                            sheet.GetRange(firstRow, 6, firstRow + ListMarkRecorkTXDK.Count - 1, 6).IsLock = true;
                        }
                        if (titleLockMark.Contains("T8"))
                        {
                            sheet.GetRange(firstRow, 7, firstRow + ListMarkRecorkTXDK.Count - 1, 7).IsLock = true;
                        }
                        if (titleLockMark.Contains("T9"))
                        {
                            sheet.GetRange(firstRow, 8, firstRow + ListMarkRecorkTXDK.Count - 1, 8).IsLock = true;
                        }
                        if (titleLockMark.Contains("CN"))
                        {
                            sheet.GetRange(firstRow, 9, firstRow + ListMarkRecorkTXDK.Count - 1, 9).IsLock = true;
                        }
                    }
                }
            }


            sheet.DeleteRow(98);
            sheet.DeleteRow(96);
            sheet.DeleteRow(94);
            sheet.DeleteRow(92);
            sheet.ProtectSheet();
            #endregion

            Stream excel = oBook.ToStream();


            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            string ReportName = "BangDiemMon" + ReportUtils.StripVNSign(SubjectCat.SubjectName) + "Lop" + ReportUtils.StripVNSign(ClassProfile.DisplayName) + "HocKy" + idSemester + ".xls";
            result.FileDownloadName = ReportName.Replace(" ", "").Replace("&", "");

            // Tạo dữ liệu ghi log
            ClassProfile classProfile = classSubject.ClassProfile;
            SetViewDataActionAudit(String.Empty, String.Empty,
                classProfile.ClassProfileID.ToString(),
                "Export excel mark record class_profile_id:" + classProfile.ClassProfileID, "ClassID: " + classid + ", SubjectID: " + subjectid + ", SemesterID: " + idSemester, "Sổ điểm", SMAS.Business.Common.GlobalConstants.ACTION_EXPORT,
                "Xuất excel sổ điểm lớp " + classProfile.DisplayName + " HK " + idSemester + " năm " + classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1) + " môn " + classSubject.SubjectCat.SubjectName);

            return result;
        }

        #region Import

        [ValidateAntiForgeryToken]
        public JsonResult SaveFile(IEnumerable<HttpPostedFileBase> attachments)
        {
            GlobalInfo global = new GlobalInfo();

            if (attachments == null || attachments.Count() <= 0)
            {
                JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
                res.ContentType = "text/plain";
                return res;
            }
            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);
                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    JsonResult res = Json(new JsonMessage(Res.Get("Common_Label_ExcelExtensionError"), "error"));
                    res.ContentType = "text/plain";
                    return res;
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                Session["FilePath"] = physicalPath;
                JsonResult res1 = Json(new JsonMessage(""));
                res1.ContentType = "text/plain";
                return res1;
            }
            JsonResult res2 = Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            res2.ContentType = "text/plain";
            return res2;
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(int? ClassID, int? SubjectID, int? SemesterID, int? EducationLevelID)
        {
            int? ClassIDImport = ClassID;
            int? SubjectIDImport = SubjectID;
            int? SemesterIDImport = SemesterID;
            int? EducationLevelIDImport = EducationLevelID;

            if (!SubjectIDImport.HasValue || !SemesterIDImport.HasValue || !EducationLevelIDImport.HasValue || !ClassIDImport.HasValue)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            try
            {
                GlobalInfo global = new GlobalInfo();
                string FilePath = (string)Session["FilePath"];
                //check cac loi co ban nhu ten truong,mon,hoc ky,lop,nam hoc
                string basicError = checkBasicErrorDataFromImportFile(FilePath, ClassIDImport, SubjectIDImport, SemesterIDImport, EducationLevelIDImport);

                if (basicError.Trim().Length == 0) //khong loi co ban
                {
                    List<MarkRecordOfPrimaryViewModel> lsTemp = getDataFromImportFile(FilePath, ClassIDImport, SubjectIDImport, SemesterIDImport, EducationLevelIDImport);
                    Session["ImportData"] = lsTemp;
                    ViewData[MarkRecordOfPrimaryConstants.LIST_IMPORTDATA] = lsTemp;
                    ViewData[MarkRecordOfPrimaryConstants.HAS_ERROR_DATA] = lsTemp.Where(o => o.PupilID != 0).Where(o => o.ErrorDescription != null).Any();

                    if (lsTemp.Where(o => o.PupilID >= 0).Where(o => o.isLegal == false).Any())
                    {
                        ViewData[MarkRecordOfPrimaryConstants.ERROR_BASIC_DATA] = false;
                        ViewData[MarkRecordOfPrimaryConstants.ERROR_IMPORT_MESSAGE] = "Có lỗi trong file excel";
                        return Json(new JsonMessage(RenderPartialViewToString("_ChooseAction", null), "grid"));
                    }
                    else
                    {
                        return SaveLegalDataImport(lsTemp, SemesterIDImport, (int)SubjectIDImport.Value, ClassIDImport);
                    }
                }
                else   //co loi co ban,tra ve man hinh co loi va disable nut xem truoc,nut import data hop le
                {
                    ViewData[MarkRecordOfPrimaryConstants.LIST_IMPORTDATA] = new List<MarkRecordOfPrimaryViewModel>();
                    ViewData[MarkRecordOfPrimaryConstants.ERROR_BASIC_DATA] = true;
                    ViewData[MarkRecordOfPrimaryConstants.ERROR_IMPORT_MESSAGE] = basicError;
                    return Json(new JsonMessage(RenderPartialViewToString("_ChooseAction", null), "grid"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        [ValidateAntiForgeryToken]
        public JsonResult ImportLegalData(int? ClassIDImport, int? SubjectIDImport, int? SemesterIDImport, int? EducationLevelIDImport)
        {
            List<MarkRecordOfPrimaryViewModel> lsTemp = (List<MarkRecordOfPrimaryViewModel>)Session["ImportData"];
            lsTemp = lsTemp.Where(o => o.isLegal).ToList();
            return SaveLegalDataImport(lsTemp, SemesterIDImport, (int)SubjectIDImport.Value, ClassIDImport);
        }

        public string checkBasicErrorDataFromImportFile(string filePath, int? ClassIDImport, int? SubjectIDImport, int? SemesterIDImport, int? EducationLevelIDImport)
        {
            try
            {
                GlobalInfo global = new GlobalInfo();
                AcademicYear ay = AcademicYearBusiness.Find(global.AcademicYearID.Value);
                
                IVTWorkbook oBook = VTExport.OpenWorkbook(filePath);
                IVTWorksheet sheet = oBook.GetSheet(1);
                string School = (string)sheet.GetCellValue(2, VTVector.dic['A']);
                string subjectAndSemesterAndClass = (string)sheet.GetCellValue(3, VTVector.dic['A']);
                string YearTitle = (string)sheet.GetCellValue(4, VTVector.dic['A']);
                subjectAndSemesterAndClass = subjectAndSemesterAndClass.ToUpper();
                YearTitle = YearTitle != null ? YearTitle.ToUpper() : "";

                SchoolProfile sp = SchoolProfileBusiness.Find(global.SchoolID.Value);
                SubjectCat sc = SubjectCatBusiness.Find(SubjectIDImport.Value);
                ClassProfile cp = ClassProfileBusiness.Find(ClassIDImport.Value);
                string compareSemester = string.Format("{0} {1}", "học kỳ", SemesterIDImport);

                bool isSchoolError = !School.ToUpper().Contains(sp.SchoolName.ToUpper());
                bool isSubjectError = !subjectAndSemesterAndClass.ToUpper().Contains(sc.DisplayName.ToUpper());
                bool isSemesterError = !subjectAndSemesterAndClass.ToUpper().Contains(compareSemester.ToUpper());
                bool isClassError = !subjectAndSemesterAndClass.ToUpper().Contains(cp.DisplayName.ToUpper());
                string strYear = ay.Year.ToString() + "-" + (ay.Year + 1).ToString();
                bool isYearError = !YearTitle.Replace(" ", "").Contains(strYear.Trim());
                StringBuilder error = new StringBuilder();
                if (isSchoolError && isSubjectError && isClassError && isYearError)
                {
                    error.Append("-Có lỗi trong file excel").Append("<br>");
                }
                if (isSchoolError)
                {
                    error.Append("-File excel không phải là file điểm của trường ").Append(sp.SchoolName).Append("<br>");
                }
                if (isSemesterError)
                {
                    error.Append("-File excel không phải là file điểm của học kỳ ").Append(SemesterIDImport).Append("<br>");
                }
                if (isSubjectError)
                {
                    error.Append("-File excel không phải là file điểm của môn ").Append(sc.DisplayName).Append("<br>");
                }
                if (isClassError)
                {
                    error.Append("-File excel không phải là file điểm của lớp ").Append(cp.DisplayName).Append("<br>");
                }
                if (isYearError)
                {
                    error.Append("-File excel không phải là file điểm của năm ").Append(ay.Year).Append("<br>");
                }

                return error.ToString();
            }
            catch (Exception ex)
            {
                return "File excel không đúng định dạng" + "<br>";
            }
        }
        

        [ValidateAntiForgeryToken]
        public JsonResult SaveLegalDataImport(List<MarkRecordOfPrimaryViewModel> LegalData, int? SemesterIDImport, int SubjectIDImport, int? ClassIDImport)
        {
            AcademicYear AcademicYear = AcademicYearBusiness.Find(globalInfo.AcademicYearID.Value);
            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(ClassIDImport.Value, new Dictionary<string, object> { { "SubjectID", SubjectIDImport }, { "SchoolID", AcademicYear.SchoolID }}).SingleOrDefault();

            bool Toan = classSubject.SubjectCat.DisplayName.Equals(SystemParamsInFile.MON_TOAN, StringComparison.InvariantCultureIgnoreCase);
            bool TV = classSubject.SubjectCat.DisplayName.Equals(SystemParamsInFile.MON_TIENGVIET, StringComparison.InvariantCultureIgnoreCase);
            bool MK = !Toan && !TV;

            List<MarkType> lstMarkType = MarkTypeBusiness.Search(new Dictionary<string, object> { { "AppliedLevel", globalInfo.AppliedLevel.Value } }).ToList();
            MarkType markTypeDGK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.DGK1);
            MarkType markTypeVGK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.VGK1);
            MarkType markTypeGK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.GK1);
            MarkType markTypeDCK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.DCK1);
            MarkType markTypeVCK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.VCK1);
            MarkType markTypeCK1 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.CK1);
            MarkType markTypeDGK2 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.DGK2);
            MarkType markTypeVGK2 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.VGK2);
            MarkType markTypeGK2 = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.GK2);
            MarkType markTypeDCN = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.DCN);
            MarkType markTypeVCN = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.VCN);
            MarkType markTypeCN = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.CN);
            MarkType markTypeDTX = lstMarkType.SingleOrDefault(u => u.Title == SystemParamsInFile.TX);

            List<MarkRecord> lstMarkRecord = new List<MarkRecord>();
            List<MarkRecord> ListMarkRecordDK = new List<MarkRecord>();
            List<SummedUpRecord> lstSummedUpRecord = new List<SummedUpRecord>();

            // Tạo data ghi log
            List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", globalInfo.AcademicYearID }, { "ClassID", classSubject.ClassID }, { "Check", "Check" } }).ToList();
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder oldObjectStr = new StringBuilder();
            StringBuilder newObjectStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();
            StringBuilder inforLog = null;
            int iLog = 0;

            foreach (MarkRecordOfPrimaryViewModel mpv in LegalData)
            {
                //if pupil is not studying to break
                if (mpv.Red.HasValue && mpv.Red.Value)
                    continue;
                int id = mpv.PupilID.Value;
                // Tạo dữ liệu ghi log
                PupilOfClass pop = lstPOC.Where(p => p.PupilID == id).FirstOrDefault();
                objectIDStr.Append(id);
                descriptionStr.Append("Import mark record pupil_id:" + id);
                paramsStr.Append("pupil_id:" + id);
                userFuntionsStr.Append("Sổ điểm");
                userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_IMPORT);
                inforLog = new StringBuilder();
                inforLog.Append("Import điểm cho " + pop.PupilProfile.FullName);
                inforLog.Append(" mã " + pop.PupilProfile.PupilCode);
                inforLog.Append(" lớp " + pop.ClassProfile.DisplayName);
                inforLog.Append(" môn " + classSubject.SubjectCat.SubjectName);
                inforLog.Append(" học kì " + SemesterIDImport + " ");
                inforLog.Append(" năm " + pop.Year.Value + "-" + (pop.Year.Value + 1) + " ");
                userDescriptionsStr.Append(inforLog.ToString());
                //List<MarkRecord> listOldRecord = pop.PupilProfile.MarkRecords.ToList();// chỗ này ko tốn performance vì bản thân lstPOC đã có thông tin điểm
                //oldObjectStr.Append("{");
                //oldObjectStr.Append("pupil_profile_id:" + id + ",");
                //for (int j = 0; j < listOldRecord.Count; j++)
                //{
                //    MarkRecord record = listOldRecord[j];
                //    oldObjectStr.Append("{MarkRecordID:" + record.MarkRecordID + "," + record.Title + ":" + record.Mark + "}");
                //    if (j < listOldRecord.Count - 1)
                //    {
                //        oldObjectStr.Append(",");
                //    }
                //}
                //oldObjectStr.Append("}");
                // end

                #region Diem TX
                #region ki 1
                if (SemesterIDImport == 1)
                {
                    int i = 0;
                    List<decimal> lstMarkMonth1 = this.ConvertMark(mpv.Month1);
                    // Tạo dữ liệu ghi log
                    userDescriptionsStr.Append("Tháng thứ 1: ");
                    foreach (decimal m1 in lstMarkMonth1)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = mpv.PupilID.Value;
                        mark.Semester = (int)SemesterIDImport;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = ClassIDImport.Value;
                        mark.SubjectID = SubjectIDImport;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.Title = SystemParamsInFile.TX;
                        mark.Mark = m1;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 9, 1);
                        lstMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m1 + " ");
                    }
                    i = 0;
                    List<decimal> lstMarkMonth2 = this.ConvertMark(mpv.Month2);
                    userDescriptionsStr.Append("Tháng thứ 2: ");
                    foreach (decimal m2 in lstMarkMonth2)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = mpv.PupilID.Value;
                        mark.Semester = (int)SemesterIDImport;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = ClassIDImport.Value;
                        mark.SubjectID = SubjectIDImport;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.Title = SystemParamsInFile.TX;
                        mark.Mark = m2;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 10, 1);
                        lstMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m2 + " ");
                    }
                    i = 0;
                    List<decimal> lstMarkMonth3 = this.ConvertMark(mpv.Month3);
                    userDescriptionsStr.Append("Tháng thứ 3: ");
                    foreach (decimal m2 in lstMarkMonth3)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = mpv.PupilID.Value;
                        mark.Semester = (int)SemesterIDImport;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = ClassIDImport.Value;
                        mark.SubjectID = SubjectIDImport;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.Title = SystemParamsInFile.TX;
                        mark.Mark = m2;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 11, 1);
                        lstMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m2 + " ");
                    }
                    i = 0;
                    List<decimal> lstMarkMonth4 = this.ConvertMark(mpv.Month4);
                    userDescriptionsStr.Append("Tháng thứ 4: ");
                    foreach (decimal m2 in lstMarkMonth4)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = mpv.PupilID.Value;
                        mark.Semester = (int)SemesterIDImport;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = ClassIDImport.Value;
                        mark.SubjectID = SubjectIDImport;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.Title = SystemParamsInFile.TX;
                        mark.Mark = m2;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 12, 1);
                        lstMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m2 + " ");
                    }
                    i = 0;
                    List<decimal> lstMarkMonth5 = this.ConvertMark(mpv.Month5);
                    userDescriptionsStr.Append("Tháng thứ 5: ");
                    foreach (decimal m2 in lstMarkMonth5)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = mpv.PupilID.Value;
                        mark.Semester = (int)SemesterIDImport;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = ClassIDImport.Value;
                        mark.SubjectID = SubjectIDImport;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.Title = SystemParamsInFile.TX;
                        mark.Mark = m2;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 1, 1);
                        lstMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m2 + " ");
                    }

                }
                #endregion

                #region ki 2
                if (SemesterIDImport == 2)
                {
                    int i = 0;
                    List<decimal> lstMarkMonth6 = this.ConvertMark(mpv.Month6);
                    userDescriptionsStr.Append("Tháng thứ 6: ");
                    foreach (decimal m1 in lstMarkMonth6)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = mpv.PupilID.Value;
                        mark.Semester = (int)SemesterIDImport;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = ClassIDImport.Value;
                        mark.SubjectID = SubjectIDImport;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.Title = SystemParamsInFile.TX;
                        mark.Mark = m1;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 2, 1);
                        lstMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m1 + " ");
                    }
                    i = 0;
                    List<decimal> lstMarkMonth7 = this.ConvertMark(mpv.Month7);
                    userDescriptionsStr.Append("Tháng thứ 7: ");
                    foreach (decimal m1 in lstMarkMonth7)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = mpv.PupilID.Value;
                        mark.Semester = (int)SemesterIDImport;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = ClassIDImport.Value;
                        mark.SubjectID = SubjectIDImport;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.Title = SystemParamsInFile.TX;
                        mark.Mark = m1;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 3, 1);
                        lstMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m1 + " ");
                    }
                    i = 0;
                    List<decimal> lstMarkMonth8 = this.ConvertMark(mpv.Month8);
                    userDescriptionsStr.Append("Tháng thứ 8: ");
                    foreach (decimal m1 in lstMarkMonth8)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = mpv.PupilID.Value;
                        mark.Semester = (int)SemesterIDImport;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = ClassIDImport.Value;
                        mark.SubjectID = SubjectIDImport;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.Title = SystemParamsInFile.TX;
                        mark.Mark = m1;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 4, 1);
                        lstMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m1 + " ");
                    }
                    i = 0;
                    List<decimal> lstMarkMonth9 = this.ConvertMark(mpv.Month9);
                    userDescriptionsStr.Append("Tháng thứ 9: ");
                    foreach (decimal m1 in lstMarkMonth9)
                    {
                        MarkRecord mark = new MarkRecord();
                        mark.PupilID = mpv.PupilID.Value;
                        mark.Semester = (int)SemesterIDImport;
                        mark.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mark.SchoolID = globalInfo.SchoolID.Value;
                        mark.ClassID = ClassIDImport.Value;
                        mark.SubjectID = SubjectIDImport;
                        mark.CreatedAcademicYear = AcademicYear.Year;
                        mark.MarkTypeID = markTypeDTX.MarkTypeID;
                        mark.Title = SystemParamsInFile.TX;
                        mark.Mark = m1;
                        mark.OrderNumber = (int)(i++);
                        mark.MarkedDate = new DateTime(AcademicYear.Year, 5, 1);
                        lstMarkRecord.Add(mark);
                        userDescriptionsStr.Append(m1 + " ");
                    }
                }
                #endregion
                #endregion

                #region DiemDK
                #region ki 1
                if (SemesterIDImport == 1)
                {
                    //KTDK GKI
                    //Neu la mon toan
                    if (Toan == true)
                    {
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeGK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.GK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        userDescriptionsStr.Append("GKI: ");
                        if (mpv.KTDKGKI.HasValue)
                        {
                            mrd.Mark = (decimal)mpv.KTDKGKI;
                        }
                        else
                        {
                            mrd.Mark = -1;
                        }
                        ListMarkRecordDK.Add(mrd);
                        userDescriptionsStr.Append(mrd.Mark + " ");
                    }
                    //Neu la mon TV
                    if (TV == true)
                    {
                        //Diem doc
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeDGK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.DGK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        userDescriptionsStr.Append("GKI_D: ");
                        if (mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "GKI_D"))
                        {
                            var temp = mpv.MarkDK[mpv.PupilID.ToString() + "GKI_D"];
                            decimal result = 0;
                            if (decimal.TryParse(temp.ToString(), out result))
                            {
                                mrd.Mark = result;
                                mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                                ListMarkRecordDK.Add(mrd);
                            }
                            else
                            {
                                mrd.Mark = -1;
                                ListMarkRecordDK.Add(mrd);
                            }
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }

                        //Diem Viet
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeVGK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.VGK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        userDescriptionsStr.Append("GKI_V: ");
                        if (mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "GKI_V"))
                        {
                            var temp = mpv.MarkDK[mpv.PupilID.ToString() + "GKI_V"];
                            decimal result = 0;
                            if (decimal.TryParse(temp.ToString(), out result))
                            {
                                mrd.Mark = result;
                                mrd.Mark = decimal.Parse(SMAS.Business.Common.Utils.FormatMark(mrd.Mark));
                                ListMarkRecordDK.Add(mrd);
                            }
                            else
                            {
                                mrd.Mark = -1;
                                ListMarkRecordDK.Add(mrd);
                            }
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }

                        // Diem GK
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeGK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.GK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        string strMark1 = mpv.PupilID.ToString() + "GKI_D";
                        string strMark2 = mpv.PupilID.ToString() + "GKI_V";
                        userDescriptionsStr.Append("GKI: ");
                        if (mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "GKI_D") && mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "GKI_V"))
                        {
                            var temp1 = ConvertToString(mpv.MarkDK[strMark1]);
                            var temp2 = ConvertToString(mpv.MarkDK[strMark2]);
                            bool temp1IsNull = string.IsNullOrEmpty(temp1);
                            bool temp2IsNull = string.IsNullOrEmpty(temp2);
                            if (!temp1IsNull && !temp2IsNull)
                            {
                                decimal val1 = decimal.Parse(temp1);
                                decimal val2 = decimal.Parse(temp2);
                                mrd.Mark = Math.Round(decimal.Parse(((val1 + val2) / 2).ToString()), MidpointRounding.AwayFromZero);
                                ListMarkRecordDK.Add(mrd);
                            }
                            else if (!(temp1IsNull && temp2IsNull))
                            {
                                decimal val1 = Math.Round(decimal.Parse(temp1IsNull ? temp2 : temp1), MidpointRounding.AwayFromZero);
                                mrd.Mark = val1;
                                ListMarkRecordDK.Add(mrd);
                            }
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }
                    }

                    //Diem KTDK CKI
                    //Neu la mon toan
                    if (Toan == true || MK == true)
                    {
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeCK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.CK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        userDescriptionsStr.Append("CKI: ");
                        if (mpv.KTDKCKI.HasValue)
                        {
                            mrd.Mark = (decimal)mpv.KTDKCKI;
                            ListMarkRecordDK.Add(mrd);
                        }
                        else
                        {
                            mrd.Mark = -1;
                            ListMarkRecordDK.Add(mrd);
                        }
                        userDescriptionsStr.Append(mrd.Mark + " ");
                    }
                    //Neu la mon TV
                    if (TV == true)
                    {
                        //Diem doc
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeDCK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.DCK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        userDescriptionsStr.Append("CKI_D: ");
                        if (mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "CKI_D"))
                        {
                            var temp1 = ConvertToString(mpv.MarkDK[mpv.PupilID.ToString() + "CKI_D"]);
                            decimal temp2 = 0;
                            if (!string.IsNullOrEmpty(temp1) && decimal.TryParse(temp1, out temp2))
                            {
                                mrd.Mark = temp2;
                                ListMarkRecordDK.Add(mrd);
                            }
                            else
                            {
                                mrd.Mark = -1;
                                ListMarkRecordDK.Add(mrd);
                            }
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }
                        //Diem Viet
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeVCK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.VCK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        userDescriptionsStr.Append("CKI_V: ");
                        if (mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "CKI_V"))
                        {
                            var temp1 = ConvertToString(mpv.MarkDK[mpv.PupilID.ToString() + "CKI_V"]);
                            decimal temp2 = 0;
                            if (!string.IsNullOrEmpty(temp1) && decimal.TryParse(temp1, out temp2))
                            {
                                mrd.Mark = temp2;
                                ListMarkRecordDK.Add(mrd);
                            }
                            else
                            {
                                mrd.Mark = -1;
                                ListMarkRecordDK.Add(mrd);
                            }
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }
                        // Diem GK
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeCK1.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.CK1;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        string strMark1 = mpv.PupilID.ToString() + "CKI_D";
                        string strMark2 = mpv.PupilID.ToString() + "CKI_V";
                        userDescriptionsStr.Append("CKI: ");
                        if (mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "CKI_D") && mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "CKI_V"))
                        {
                            var temp1 = ConvertToString(mpv.MarkDK[strMark1]);
                            var temp2 = ConvertToString(mpv.MarkDK[strMark2]);
                            bool temp1IsNull = string.IsNullOrEmpty(temp1);
                            bool temp2IsNull = string.IsNullOrEmpty(temp2);
                            if (!temp1IsNull && !temp2IsNull)
                            {
                                decimal val1 = decimal.Parse(temp1);
                                decimal val2 = decimal.Parse(temp2);
                                mrd.Mark = Math.Round(decimal.Parse(((val1 + val2) / 2).ToString()), MidpointRounding.AwayFromZero);
                                ListMarkRecordDK.Add(mrd);
                            }
                            else if (!(temp1IsNull && temp2IsNull))
                            {
                                decimal val1 = Math.Round(decimal.Parse(temp1IsNull ? temp2 : temp1), MidpointRounding.AwayFromZero);
                                mrd.Mark = val1;
                                ListMarkRecordDK.Add(mrd);
                            }
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }
                    }
                }
                #endregion

                #region ki 2
                if (SemesterIDImport == 2)
                {
                    //KTDK GKII
                    //Neu la mon toan
                    if (Toan == true)
                    {
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeGK2.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.GK2;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        userDescriptionsStr.Append("GKII: ");
                        if (mpv.KTDKGKII.HasValue)
                        {
                            mrd.Mark = (decimal)mpv.KTDKGKII;
                            ListMarkRecordDK.Add(mrd);
                        }
                        else
                        {
                            mrd.Mark = -1;
                            ListMarkRecordDK.Add(mrd);
                        }
                        userDescriptionsStr.Append(mrd.Mark + " ");
                    }
                    //Neu la mon TV
                    if (TV == true)
                    {
                        //Diem doc
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeDGK2.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.DGK2;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        userDescriptionsStr.Append("GKII_D: ");
                        if (mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "GKII_D"))
                        {
                            var temp1 = ConvertToString(mpv.MarkDK[mpv.PupilID.ToString() + "GKII_D"]);
                            decimal temp2 = 0;
                            if (!string.IsNullOrEmpty(temp1) && decimal.TryParse(temp1, out temp2))
                            {
                                mrd.Mark = temp2;
                                ListMarkRecordDK.Add(mrd);
                            }
                            else
                            {
                                mrd.Mark = -1;
                                ListMarkRecordDK.Add(mrd);
                            }
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }
                        //Diem Viet
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeVGK2.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.VGK2;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        userDescriptionsStr.Append("GKII_V: ");
                        if (mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "GKII_V"))
                        {
                            var temp1 = ConvertToString(mpv.MarkDK[mpv.PupilID.ToString() + "GKII_V"]);
                            decimal temp2 = 0;
                            if (!string.IsNullOrEmpty(temp1) && decimal.TryParse(temp1, out temp2))
                            {
                                mrd.Mark = temp2;
                                ListMarkRecordDK.Add(mrd);
                            }
                            else
                            {
                                mrd.Mark = -1;
                                ListMarkRecordDK.Add(mrd);
                            }
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }
                        // Diem GK
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeGK2.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.GK2;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        string strMark1 = mpv.PupilID.ToString() + "GKII_D";
                        string strMark2 = mpv.PupilID.ToString() + "GKII_V";
                        userDescriptionsStr.Append("GKII: ");
                        if (mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "GKII_D") && mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "GKII_V"))
                        {
                            //
                            var temp1 = ConvertToString(mpv.MarkDK[strMark1]);
                            var temp2 = ConvertToString(mpv.MarkDK[strMark2]);
                            bool temp1IsNull = string.IsNullOrEmpty(temp1);
                            bool temp2IsNull = string.IsNullOrEmpty(temp2);
                            if (!temp1IsNull && !temp2IsNull)
                            {
                                decimal val1 = decimal.Parse(temp1);
                                decimal val2 = decimal.Parse(temp2);
                                mrd.Mark = Math.Round(decimal.Parse(((val1 + val2) / 2).ToString()), MidpointRounding.AwayFromZero);
                                ListMarkRecordDK.Add(mrd);
                            }
                            else if (!(temp1IsNull && temp2IsNull))
                            {
                                decimal val1 = Math.Round(decimal.Parse(temp1IsNull ? temp2 : temp1), MidpointRounding.AwayFromZero);
                                mrd.Mark = val1;
                                ListMarkRecordDK.Add(mrd);
                            }
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }
                    }

                    //Diem KTDK CKII
                    //Neu la mon toan
                    if (Toan == true || MK == true)
                    {
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeCN.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.CN;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        userDescriptionsStr.Append("CKII: ");
                        if (mpv.KTDKCKII.HasValue)
                        {
                            mrd.Mark = (decimal)mpv.KTDKCKII;
                            ListMarkRecordDK.Add(mrd);
                        }
                        else
                        {
                            mrd.Mark = -1;
                            ListMarkRecordDK.Add(mrd);
                        }
                        userDescriptionsStr.Append(mrd.Mark + " ");
                    }
                    //Neu la mon TV
                    if (TV == true)
                    {
                        //Diem doc
                        MarkRecord mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeDCN.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.DCN;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        userDescriptionsStr.Append("CKII_D: ");
                        if (mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "CKII_D"))
                        {
                            var temp1 = ConvertToString(mpv.MarkDK[mpv.PupilID.ToString() + "CKII_D"]);
                            decimal temp2 = 0;
                            if (!string.IsNullOrEmpty(temp1) && decimal.TryParse(temp1, out temp2))
                            {
                                mrd.Mark = temp2;
                                ListMarkRecordDK.Add(mrd);
                            }
                            else
                            {
                                mrd.Mark = -1;
                                ListMarkRecordDK.Add(mrd);
                            }
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }
                        //Diem Viet
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeVCN.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.VCN;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        userDescriptionsStr.Append("CKII_V: ");
                        if (mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "CKII_V"))
                        {
                            var temp1 = ConvertToString(mpv.MarkDK[mpv.PupilID.ToString() + "CKII_V"]);
                            decimal temp2 = 0;
                            if (!string.IsNullOrEmpty(temp1) && decimal.TryParse(temp1, out temp2))
                            {
                                mrd.Mark = temp2;
                                ListMarkRecordDK.Add(mrd);
                            }
                            else
                            {
                                mrd.Mark = -1;
                                ListMarkRecordDK.Add(mrd);
                            }
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }
                        // Diem GK
                        mrd = new MarkRecord();
                        mrd.AcademicYearID = globalInfo.AcademicYearID.Value;
                        mrd.SchoolID = globalInfo.SchoolID.Value;
                        mrd.ClassID = ClassIDImport.Value;
                        mrd.SubjectID = SubjectIDImport;
                        mrd.PupilID = mpv.PupilID.Value;
                        mrd.MarkTypeID = markTypeCN.MarkTypeID;
                        mrd.OrderNumber = 1;
                        mrd.Title = SystemParamsInFile.CN;
                        mrd.CreatedAcademicYear = AcademicYear.Year;
                        mrd.Semester = (int)SemesterIDImport;
                        string strMark1 = mpv.PupilID.ToString() + "CKII_D";
                        string strMark2 = mpv.PupilID.ToString() + "CKII_V";
                        userDescriptionsStr.Append("CKII: ");
                        if (mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "CKII_D") && mpv.MarkDK.ContainsKey(mpv.PupilID.ToString() + "GKII_V"))
                        {
                            var temp1 = ConvertToString(mpv.MarkDK[strMark1]);
                            var temp2 = ConvertToString(mpv.MarkDK[strMark2]);
                            bool temp1IsNull = string.IsNullOrEmpty(temp1);
                            bool temp2IsNull = string.IsNullOrEmpty(temp2);
                            if (!temp1IsNull && !temp2IsNull)
                            {
                                decimal val1 = decimal.Parse(temp1);
                                decimal val2 = decimal.Parse(temp2);
                                mrd.Mark = Math.Round(decimal.Parse(((val1 + val2) / 2).ToString()), MidpointRounding.AwayFromZero);
                                ListMarkRecordDK.Add(mrd);
                            }
                            else if (!(temp1IsNull && temp2IsNull))
                            {
                                decimal val1 = Math.Round(decimal.Parse(temp1IsNull ? temp2 : temp1), MidpointRounding.AwayFromZero);
                                mrd.Mark = val1;
                                ListMarkRecordDK.Add(mrd);
                            }
                            userDescriptionsStr.Append(mrd.Mark + " ");
                        }
                    }
                }
                #endregion
                #endregion

                SummedUpRecord sur = new SummedUpRecord();
                sur.PupilID = mpv.PupilID.Value;
                sur.ClassID = ClassIDImport.Value;
                sur.AcademicYearID = AcademicYear.AcademicYearID;
                sur.SchoolID = globalInfo.SchoolID.Value;
                sur.SubjectID = SubjectIDImport;
                sur.IsCommenting = classSubject.IsCommenting.Value;
                sur.CreatedAcademicYear = AcademicYear.Year;
                sur.Semester = (int)SemesterIDImport.Value;
                lstSummedUpRecord.Add(sur);

                // Tạo dữ liệu ghi log
                userDescriptionsStr.Replace("-1", " ");
                if (iLog < LegalData.Count - 1)
                {
                    objectIDStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    oldObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    newObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userDescriptionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                }
            }     
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(AcademicYear);
            if (!isMovedHistory)
            {
                MarkRecordBusiness.InsertMarkRecordTXOfPrimary(lstMarkRecord);
                MarkRecordBusiness.InsertMarkRecordDKOfPrimary(globalInfo.UserAccountID, ListMarkRecordDK, lstSummedUpRecord, SemesterIDImport.Value);
                MarkRecordBusiness.Save();
            }
            else
            {
                List<MarkRecordHistory> ListMarkRecordHistory = new List<MarkRecordHistory>();
                MarkRecord objMarkRecord = new MarkRecord();
                MarkRecordHistory objMarkRecordHistory = null;
                for (int i = 0; i < lstMarkRecord.Count; i++)
                {
                    objMarkRecordHistory = new MarkRecordHistory();
                    objMarkRecord = lstMarkRecord[i];
                    //objMarkRecordHistory.AcademicYear = objMarkRecord.AcademicYear;
                    objMarkRecordHistory.AcademicYearID = objMarkRecord.AcademicYearID;
                    objMarkRecordHistory.ClassID = objMarkRecord.ClassID;
                    //objMarkRecordHistory.ClassProfile = objMarkRecord.ClassProfile;
                    objMarkRecordHistory.CreatedAcademicYear = objMarkRecord.CreatedAcademicYear;
                    objMarkRecordHistory.CreatedDate = objMarkRecord.CreatedDate;
                    objMarkRecordHistory.IsOldData = objMarkRecord.IsOldData;
                    objMarkRecordHistory.IsSMS = objMarkRecord.IsSMS;
                    objMarkRecordHistory.Last2digitNumberSchool = objMarkRecord.Last2digitNumberSchool;
                    objMarkRecordHistory.M_IsByC = objMarkRecord.M_IsByC;
                    objMarkRecordHistory.M_MarkRecord = objMarkRecord.M_MarkRecord;
                    objMarkRecordHistory.M_OldID = objMarkRecord.M_OldID;
                    objMarkRecordHistory.M_ProvinceID = objMarkRecord.M_ProvinceID;
                    objMarkRecordHistory.Mark = objMarkRecord.Mark;
                    objMarkRecordHistory.MarkedDate = objMarkRecord.MarkedDate;
                    objMarkRecordHistory.MarkRecordID = objMarkRecord.MarkRecordID;
                    //objMarkRecordHistory.MarkType = objMarkRecord.MarkType;
                    objMarkRecordHistory.MarkTypeID = objMarkRecord.MarkTypeID;
                    objMarkRecordHistory.ModifiedDate = objMarkRecord.ModifiedDate;
                    objMarkRecordHistory.MSourcedb = objMarkRecord.MSourcedb;
                    objMarkRecordHistory.OldMark = objMarkRecord.OldMark;
                    objMarkRecordHistory.OrderNumber = objMarkRecord.OrderNumber;
                    objMarkRecordHistory.PupilID = objMarkRecord.PupilID;
                    //objMarkRecordHistory.PupilProfile = objMarkRecord.PupilProfile;
                    objMarkRecordHistory.SchoolID = objMarkRecord.SchoolID;
                    //objMarkRecordHistory.SchoolProfile = objMarkRecord.SchoolProfile;
                    objMarkRecordHistory.Semester = objMarkRecord.Semester;
                    //objMarkRecordHistory.SubjectCat = objMarkRecord.SubjectCat;
                    objMarkRecordHistory.SubjectID = objMarkRecord.SubjectID;
                    objMarkRecordHistory.SynchronizeID = objMarkRecord.SynchronizeID;
                    objMarkRecordHistory.Title = objMarkRecord.Title;
                    ListMarkRecordHistory.Add(objMarkRecordHistory);
                }
                List<MarkRecordHistory> ListMarkRecordDKHistory = new List<MarkRecordHistory>();
                MarkRecord objMarkRecordDK = new MarkRecord();
                MarkRecordHistory objMarkRecordDKHistory = null;
                for (int j = 0; j < ListMarkRecordDK.Count; j++)
                {
                    objMarkRecordDKHistory = new MarkRecordHistory();
                    objMarkRecordDK = ListMarkRecordDK[j];
                    //objMarkRecordDKHistory.AcademicYear = objMarkRecordDK.AcademicYear;
                    objMarkRecordDKHistory.AcademicYearID = objMarkRecordDK.AcademicYearID;
                    objMarkRecordDKHistory.ClassID = objMarkRecordDK.ClassID;
                    //objMarkRecordDKHistory.ClassProfile = objMarkRecordDK.ClassProfile;
                    objMarkRecordDKHistory.CreatedAcademicYear = objMarkRecordDK.CreatedAcademicYear;
                    objMarkRecordDKHistory.CreatedDate = objMarkRecordDK.CreatedDate;
                    objMarkRecordDKHistory.IsOldData = objMarkRecordDK.IsOldData;
                    objMarkRecordDKHistory.IsSMS = objMarkRecordDK.IsSMS;
                    objMarkRecordDKHistory.Last2digitNumberSchool = objMarkRecordDK.Last2digitNumberSchool;
                    objMarkRecordDKHistory.M_IsByC = objMarkRecordDK.M_IsByC;
                    objMarkRecordDKHistory.M_MarkRecord = objMarkRecordDK.M_MarkRecord;
                    objMarkRecordDKHistory.M_OldID = objMarkRecordDK.M_OldID;
                    objMarkRecordDKHistory.M_ProvinceID = objMarkRecordDK.M_ProvinceID;
                    objMarkRecordDKHistory.Mark = objMarkRecordDK.Mark;
                    objMarkRecordDKHistory.MarkedDate = objMarkRecordDK.MarkedDate;
                    objMarkRecordDKHistory.MarkRecordID = objMarkRecordDK.MarkRecordID;
                    //objMarkRecordDKHistory.MarkType = objMarkRecordDK.MarkType;
                    objMarkRecordDKHistory.MarkTypeID = objMarkRecordDK.MarkTypeID;
                    objMarkRecordDKHistory.ModifiedDate = objMarkRecordDK.ModifiedDate;
                    objMarkRecordDKHistory.MSourcedb = objMarkRecordDK.MSourcedb;
                    objMarkRecordDKHistory.OldMark = objMarkRecordDK.OldMark;
                    objMarkRecordDKHistory.OrderNumber = objMarkRecordDK.OrderNumber;
                    objMarkRecordDKHistory.PupilID = objMarkRecordDK.PupilID;
                    //objMarkRecordDKHistory.PupilProfile = objMarkRecordDK.PupilProfile;
                    objMarkRecordDKHistory.SchoolID = objMarkRecordDK.SchoolID;
                    //objMarkRecordDKHistory.SchoolProfile = objMarkRecordDK.SchoolProfile;
                    objMarkRecordDKHistory.Semester = objMarkRecordDK.Semester;
                    //objMarkRecordDKHistory.SubjectCat = objMarkRecordDK.SubjectCat;
                    objMarkRecordDKHistory.SubjectID = objMarkRecordDK.SubjectID;
                    objMarkRecordDKHistory.SynchronizeID = objMarkRecordDK.SynchronizeID;
                    objMarkRecordDKHistory.Title = objMarkRecordDK.Title;
                    ListMarkRecordDKHistory.Add(objMarkRecordDKHistory);
                }

                List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                SummedUpRecord objSumUpRecord = new SummedUpRecord();
                SummedUpRecordHistory objSumUpRecordHistory = null;
                for (int k = 0; k < lstSummedUpRecord.Count; k++)
                {
                    objSumUpRecord = lstSummedUpRecord[k];
                    objSumUpRecordHistory = new SummedUpRecordHistory();
                    //objSumUpRecordHistory.AcademicYear = objSumUpRecord.AcademicYear;
                    objSumUpRecordHistory.AcademicYearID = objSumUpRecord.AcademicYearID;
                    objSumUpRecordHistory.ClassID = objSumUpRecord.ClassID;
                    //objSumUpRecordHistory.ClassProfile = objSumUpRecord.ClassProfile;
                    objSumUpRecordHistory.Comment = objSumUpRecord.Comment;
                    objSumUpRecordHistory.CreatedAcademicYear = objSumUpRecord.CreatedAcademicYear;
                    objSumUpRecordHistory.IsCommenting = objSumUpRecord.IsCommenting;
                    objSumUpRecordHistory.IsOldData = objSumUpRecord.IsOldData;
                    objSumUpRecordHistory.JudgementResult = objSumUpRecord.JudgementResult;
                    objSumUpRecordHistory.Last2digitNumberSchool = objSumUpRecord.Last2digitNumberSchool;
                    objSumUpRecordHistory.M_OldID = objSumUpRecord.M_OldID;
                    objSumUpRecordHistory.M_ProvinceID = objSumUpRecord.M_ProvinceID;
                    objSumUpRecordHistory.MSourcedb = objSumUpRecord.MSourcedb;
                    //objSumUpRecordHistory.PeriodDeclaration = objSumUpRecord.PeriodDeclaration;
                    objSumUpRecordHistory.PeriodID = objSumUpRecord.PeriodID;
                    objSumUpRecordHistory.PupilID = objSumUpRecord.PupilID;
                    //objSumUpRecordHistory.PupilProfile = objSumUpRecord.PupilProfile;
                    objSumUpRecordHistory.ReTestJudgement = objSumUpRecord.ReTestJudgement;
                    objSumUpRecordHistory.ReTestMark = objSumUpRecord.ReTestMark;
                    objSumUpRecordHistory.SchoolID = objSumUpRecord.SchoolID;
                    //objSumUpRecordHistory.SchoolProfile = objSumUpRecord.SchoolProfile;
                    objSumUpRecordHistory.Semester = objSumUpRecord.Semester;
                    //objSumUpRecordHistory.SubjectCat = objSumUpRecord.SubjectCat;
                    objSumUpRecordHistory.SubjectID = objSumUpRecord.SubjectID;
                    objSumUpRecordHistory.SummedUpDate = objSumUpRecord.SummedUpDate;
                    objSumUpRecordHistory.SummedUpMark = objSumUpRecord.SummedUpMark;
                    objSumUpRecordHistory.SummedUpRecordID = objSumUpRecord.SummedUpRecordID;
                    objSumUpRecordHistory.SynchronizeID = objSumUpRecord.SynchronizeID;
                    lstSummedUpRecordHistory.Add(objSumUpRecordHistory);
                }
                MarkRecordHistoryBusiness.InsertMarkRecordTXOfPrimaryHistory(ListMarkRecordHistory);
                MarkRecordHistoryBusiness.InsertMarkRecordDKOfPrimaryHistory(globalInfo.UserAccountID, ListMarkRecordDKHistory, lstSummedUpRecordHistory, SemesterIDImport.Value);
                MarkRecordHistoryBusiness.Save();
            }
            // Tạo dữ liệu ghi log
            SetViewDataActionAudit(oldObjectStr.ToString(), newObjectStr.ToString(), objectIDStr.ToString(), descriptionStr.ToString(), paramsStr.ToString(), userFuntionsStr.ToString(), userActionsStr.ToString(), userDescriptionsStr.ToString());

            return Json(new JsonMessage(Res.Get("Common_Label_ImportSuccessMessage")));
        }

        private List<MarkRecordOfPrimaryViewModel> getDataFromImportFile(string filePath, int? ClassIDImport, int? SubjectIDImport, int? SemesterIDImport, int? EducationLevelIDImport)
        {
            List<MarkRecordOfPrimaryViewModel> lsJRVM = new List<MarkRecordOfPrimaryViewModel>();
            IVTWorkbook oBook = VTExport.OpenWorkbook(filePath);
            GlobalInfo global = new GlobalInfo();

            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);

            List<PupilOfClass> listPOC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object> { { "ClassID", ClassIDImport.Value }, { "Check", "Check" } }).ToList();

            ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(ClassIDImport.Value, new Dictionary<string, object> { { "SubjectID", SubjectIDImport.Value }, { "SchoolID", _globalInfo.SchoolID.Value } }).SingleOrDefault();
            bool isMonToan = classSubject.SubjectCat.DisplayName.Equals(SystemParamsInFile.MON_TOAN, StringComparison.InvariantCultureIgnoreCase);
            bool isMonTiengViet = classSubject.SubjectCat.DisplayName.Equals(SystemParamsInFile.MON_TIENGVIET, StringComparison.InvariantCultureIgnoreCase);
            bool isMonKhac = !isMonToan && !isMonTiengViet;

            for (int i = 0; i < 1000; i++)
            {
                bool isLegalBot = true;
                bool PupilNeedCheck = true;
                MarkRecordOfPrimaryViewModel jrvm = new MarkRecordOfPrimaryViewModel();

                #region Ten, ma, ngay sinh
                string PupilCode = "", FullName = "", BirthDay = "";

                PupilCode = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['B']));
                FullName = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['C']));
                var BirthDate = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['D']));

                if ((PupilCode == null || PupilCode.Trim().Length == 0) && (FullName == null || FullName.Trim().Length == 0) && BirthDay.Trim().Length == 0)
                {
                    break;
                }

                jrvm.PupilCode = PupilCode;
                jrvm.PupilName = FullName;

                PupilOfClass poc = listPOC.Where(u => u.PupilProfile.PupilCode.Equals(PupilCode, StringComparison.InvariantCultureIgnoreCase)).OrderByDescending(u => u.AssignedDate)
                                            .OrderByDescending(u => u.PupilOfClassID).FirstOrDefault();


                if (poc == null)
                {
                    jrvm.PupilID = 0;
                    isLegalBot = false;
                    jrvm.ErrorDescription += "-Học sinh không tồn tại trong trường <br>";
                    PupilNeedCheck = false;
                }
                else
                {
                    jrvm.PupilID = poc.PupilID;

                    if (lsJRVM.Any(u => u.PupilID == jrvm.PupilID))
                    {
                        isLegalBot = false;
                        jrvm.ErrorDescription += "-Học sinh bị trùng lặp <br>";
                    }

                    if (!poc.PupilProfile.FullName.Equals(FullName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        isLegalBot = false;
                        jrvm.ErrorDescription += "-Tên học sinh không phù hợp <br>";
                    }

                    if (poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        //isLegalBot = false;
                        jrvm.Red = true;
                        PupilNeedCheck = false;
                    }
                }
                #endregion

                #region bat dau tim diem
                string Month1 = "", Month2 = "", Month3 = "", Month4 = "", Month5 = ""
                    , Month6 = "", Month7 = "", Month8 = "", Month9 = "", KTDKGKI = ""
                    , KTDKGKII = "", KTDKCKI = "", KTDKCKII = "", HLMKI = "", HLMKII = "", XLHLMKI = "", XLHLMKII = "", HLMKCN = "", XLHLMKCN = "";

                Dictionary<string, object> MarkDK = new Dictionary<string, object>();

                if (PupilNeedCheck)
                {
                    #region Nếu là môn toán
                    //Nếu là môn toán
                    if (isMonToan)
                    {
                        #region hoc ky 1: quan tam den cac diem:month1,month2,KTDKGKI,month3,month4,month5,KTDKCKI,HLMKI,XLHLMKI
                        if (SemesterIDImport.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            Month1 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['E']));
                            Month2 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['F']));
                            Month3 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['G']));

                            KTDKGKI = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['H']));

                            Month4 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['I']));
                            Month5 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['J']));

                            KTDKCKI = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['K']));
                            HLMKI = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['L']));
                            XLHLMKI = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['M']));

                            jrvm.Month1 = Month1;
                            jrvm.Month2 = Month2;
                            jrvm.Month3 = Month3;
                            jrvm.Month4 = Month4;
                            jrvm.Month5 = Month5;

                            jrvm.strKTDKCKI = KTDKCKI;
                            jrvm.strHLMKI = HLMKI;

                            jrvm.strKTDKGKI = KTDKGKI;
                            jrvm.XLHLMKI = XLHLMKI;

                            //neu hs ko nhap diem thi bo qua luon
                            List<string> lsMark = new List<string>() { Month1, Month2, Month3, Month4, Month5, KTDKCKI, HLMKI, XLHLMKI };
                            if (!CheckIsPupilHasMark(lsMark))
                            {
                                continue;
                            }

                            //validate
                            decimal result = 0;
                            if (!"0".Equals(KTDKGKI.Trim()) && KTDKGKI.Trim().Length != 0)
                            {
                                if (KTDKGKI.Contains(",") || KTDKGKI.Contains("."))
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTGKError");
                                }
                                else if (decimal.TryParse(KTDKGKI, out result))
                                {
                                    if (result <= 10 || result == 0)
                                    {
                                        jrvm.KTDKGKI = result;
                                    }
                                    else
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTGKError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTGKError");
                                }
                            }

                            if (!"0".Equals(KTDKCKI.Trim()) && KTDKCKI.Trim().Length != 0)
                            {
                                if (KTDKCKI.Contains(",") || KTDKCKI.Contains("."))
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                }
                                else if (decimal.TryParse(KTDKCKI, out result))
                                {
                                    if (result <= 10 || result == 0)
                                    {
                                        jrvm.KTDKCKI = result;
                                    }
                                    else
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                }
                            }

                            if (HLMKI.Trim().Length != 0)
                            {
                                if (decimal.TryParse(HLMKI, out result))
                                {
                                    if (result <= 10)
                                    {
                                        jrvm.HLMKI = result;
                                    }
                                    else
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMError");
                                }
                            }
                            if (!IsValidHK(XLHLMKI))
                            {
                                isLegalBot = false;
                                jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HKError");
                            }

                            //diem thang
                            List<int> lsMarkError = new List<int>();

                            if (!IsValidMark(Month1))
                            {
                                lsMarkError.Add(1);
                            }
                            if (!IsValidMark(Month2))
                            {
                                lsMarkError.Add(2);
                            }
                            if (!IsValidMark(Month3))
                            {
                                lsMarkError.Add(3);
                            }
                            if (!IsValidMark(Month4))
                            {
                                lsMarkError.Add(4);
                            }
                            if (!IsValidMark(Month5))
                            {
                                lsMarkError.Add(5);
                            }
                            if (lsMarkError.Count() > 0)
                            {
                                string monthError = string.Join(", ", lsMarkError);
                                isLegalBot = false;
                                string error = string.Format(Res.Get("MarkRecordOfPrimary_Label_MonthMarkError"), monthError);
                                jrvm.ErrorDescription += error;
                            }
                        }
                        #endregion

                        #region hoc ky 2: quan tam den cac diem:month6,month7,KTDKGKII,month8,month9,KTDKCKII,HLMKII,XLHLMKII,HLMKCN,XLHLMKCN
                        if (SemesterIDImport.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            Month6 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['E']));
                            Month7 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['F']));
                            KTDKGKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['G']));

                            Month8 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['H']));
                            Month9 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['I']));

                            KTDKCKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['J']));
                            HLMKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['K']));
                            XLHLMKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['L']));

                            HLMKCN = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['M']));
                            XLHLMKCN = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['N']));

                            jrvm.Month6 = Month6;
                            jrvm.Month7 = Month7;
                            jrvm.Month8 = Month8;
                            jrvm.Month9 = Month9;

                            jrvm.strKTDKCKII = KTDKCKII;
                            jrvm.strHLMKII = HLMKII;

                            jrvm.strKTDKGKII = KTDKGKII;
                            jrvm.XLHLMKII = XLHLMKII;

                            //neu hs ko nhap diem thi bo qua luon
                            List<string> lsMark = new List<string>() { Month6, Month7, Month8, Month9, KTDKGKII, KTDKCKII, HLMKII, XLHLMKII, HLMKCN, XLHLMKCN };
                            if (!CheckIsPupilHasMark(lsMark))
                            {
                                continue;
                            }

                            //validate
                            decimal result = 0;
                            if (!"0".Equals(KTDKGKII.Trim()) && KTDKGKII.Trim().Length != 0)
                            {
                                if (KTDKGKII.Contains(",") || KTDKGKII.Contains("."))
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTGKError");
                                }
                                else 
                                if (decimal.TryParse(KTDKGKII, out result))
                                {
                                    if (result <= 10)
                                    {
                                        jrvm.KTDKGKII = result;
                                    }
                                    else
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTGKError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTGKError");
                                }
                            }

                            if (!"0".Equals(KTDKCKII.Trim()) && KTDKCKII.Trim().Length != 0)
                            {
                                if (KTDKCKII.Contains(",") || KTDKCKII.Contains("."))
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                }
                                else 
                                if (decimal.TryParse(KTDKCKII, out result))
                                {
                                    if (result <= 10)
                                    {
                                        jrvm.KTDKCKII = result;
                                    }
                                    else
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                }
                            }

                            if (HLMKII.Trim().Length != 0)
                            {
                                if (decimal.TryParse(HLMKII, out result))
                                {
                                    if (result <= 10)
                                    {
                                        jrvm.HLMKII = result;
                                    }
                                    else
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMError");
                                }
                            }

                            if (!IsValidHK(XLHLMKII))
                            {
                                isLegalBot = false;
                                jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HKError");
                            }

                            if (HLMKII.Trim().Length != 0)
                            {
                                if (!decimal.TryParse(HLMKCN, out result))
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMCNError");
                                }
                                else
                                {
                                    if (result > 10)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMCNError");
                                    }
                                }
                            }
                            if (!IsValidHK(XLHLMKCN))
                            {
                                isLegalBot = false;
                                jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HKCNError");
                            }

                            //diem thang
                            List<int> lsMarkError = new List<int>();

                            if (!IsValidMark(Month6))
                            {
                                lsMarkError.Add(6);
                            }
                            if (!IsValidMark(Month7))
                            {
                                lsMarkError.Add(7);
                            }
                            if (!IsValidMark(Month8))
                            {
                                lsMarkError.Add(8);
                            }
                            if (!IsValidMark(Month9))
                            {
                                lsMarkError.Add(9);
                            }
                            if (lsMarkError.Count() > 0)
                            {
                                string monthError = string.Join(", ", lsMarkError);
                                isLegalBot = false;
                                string error = string.Format(Res.Get("MarkRecordOfPrimary_Label_MonthMarkError"), monthError);
                                jrvm.ErrorDescription += error;
                            }
                        }
                        #endregion
                    }
                    #endregion

                    #region Nếu là môn tiếng việt
                    //Nếu là môn tiếng việt
                    if (isMonTiengViet)
                    {
                        #region hoc ky 1 :Month1,Month2,KTDKGKI(D,V,GKI),Month3,Month4,Month5,KTDKCKI(D,V,CKI), HLMKI,XLHLMKI
                        if (SemesterIDImport.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            Month1 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['E']));
                            Month2 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['F']));
                            Month3 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['G']));

                            string KTDKGKI_D = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['H']));
                            string KTDKGKI_V = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['I']));
                            string KTDKGKI_GKI = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['J']));
                            MarkDK[jrvm.PupilID.ToString() + "GKI_V"] = KTDKGKI_V;
                            MarkDK[jrvm.PupilID.ToString() + "GKI_D"] = KTDKGKI_D;

                            jrvm.strKTDKGKI = KTDKGKI_GKI;

                            Month4 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['K']));
                            Month5 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['L']));

                            string KTDKCKI_D = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['M']));
                            string KTDKCKI_V = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['N']));
                            string KTDKCKI_CKI = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['O']));

                            MarkDK[jrvm.PupilID.ToString() + "CKI_V"] = KTDKCKI_V;
                            MarkDK[jrvm.PupilID.ToString() + "CKI_D"] = KTDKCKI_D;
                            jrvm.strKTDKCKI = KTDKCKI_CKI;

                            HLMKI = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['P']));
                            XLHLMKI = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['Q']));

                            jrvm.Month1 = Month1;
                            jrvm.Month2 = Month2;
                            jrvm.Month3 = Month3;
                            jrvm.Month4 = Month4;
                            jrvm.Month5 = Month5;

                            jrvm.strHLMKI = HLMKI;
                            jrvm.XLHLMKI = XLHLMKI;

                            //neu hs ko nhap diem thi bo qua
                            List<string> lsMark = new List<string>() { Month1, Month2, Month3, Month4, Month5, KTDKCKI_D, KTDKCKI_V, KTDKCKI_CKI, HLMKI, XLHLMKI };

                            //validate
                            decimal result = 0;

                            if (KTDKGKI_D.Trim().Length != 0)
                            {
                                if (decimal.TryParse(KTDKGKI_D, out result))
                                {
                                    MarkDK[jrvm.PupilID.ToString() + "GKI_D"] = result;
                                    if (result > 10 || result <= 0 )
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_D");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_D");
                                }
                            }

                            if (KTDKCKI_D.Trim().Length != 0)
                            {
                                if (decimal.TryParse(KTDKCKI_D, out result))
                                {
                                    MarkDK[jrvm.PupilID.ToString() + "CKI_D"] = result;
                                    if (result > 10 || result <= 0)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_D");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_D");
                                }
                            }

                            if (KTDKGKI_V.Trim().Length != 0)
                            {
                                if (decimal.TryParse(KTDKGKI_V, out result))
                                {

                                    MarkDK[jrvm.PupilID.ToString() + "GKI_V"] = result;
                                    if (result > 10  || result <= 0)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_V");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_V");
                                }
                            }

                            if (KTDKCKI_V.Trim().Length != 0)
                            {
                                if (decimal.TryParse(KTDKCKI_V, out result))
                                {
                                    MarkDK[jrvm.PupilID.ToString() + "CKI_V"] = result;
                                    if (result > 10 || result <= 0)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_V");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_V");
                                }
                            }

                            if (!"0".Equals(KTDKGKI_GKI.Trim()) && KTDKGKI_GKI.Trim().Length != 0)
                            {
                                if (KTDKGKI_GKI.Contains(",") || KTDKGKI_GKI.Contains("."))
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTGKError");
                                }
                                else 
                                if (decimal.TryParse(KTDKGKI_GKI, out result))
                                {
                                    if (result <= 10 || result == 0)
                                    {
                                        jrvm.KTDKGKI = result;
                                    }
                                    else
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTGKError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTGKError");
                                }
                            }

                            if (!"0".Equals(KTDKCKI_CKI.Trim()) && KTDKCKI_CKI.Trim().Length != 0)
                            {
                                if (KTDKCKI_CKI.Contains(",") || KTDKCKI_CKI.Contains("."))
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                }
                                else 
                                if (decimal.TryParse(KTDKCKI_CKI, out result))
                                {
                                    if (result <= 10 || result == 0)
                                    {
                                        jrvm.KTDKCKI = result;
                                    }
                                    else
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                }
                            }

                            if (HLMKI.Trim().Length != 0)
                            {
                                if (decimal.TryParse(HLMKI, out result))
                                {
                                    jrvm.HLMKI = result;
                                    if (result > 10)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMError");
                                }
                            }

                            if (!IsValidHK(XLHLMKI))
                            {
                                isLegalBot = false;
                                jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HKError");
                            }

                            //diem thang
                            List<int> lsMarkError = new List<int>();

                            if (!IsValidMark(Month1))
                            {
                                lsMarkError.Add(1);
                            }
                            if (!IsValidMark(Month2))
                            {
                                lsMarkError.Add(2);
                            }
                            if (!IsValidMark(Month3))
                            {
                                lsMarkError.Add(3);
                            }
                            if (!IsValidMark(Month4))
                            {
                                lsMarkError.Add(4);
                            }
                            if (!IsValidMark(Month5))
                            {
                                lsMarkError.Add(5);
                            }
                            if (lsMarkError.Count() > 0)
                            {
                                string monthError = string.Join(", ", lsMarkError);
                                isLegalBot = false;
                                string error = string.Format(Res.Get("MarkRecordOfPrimary_Label_MonthMarkError"), monthError);
                                jrvm.ErrorDescription += error;
                            }
                            if (!PupilNeedCheck)
                            {
                                string monthError = string.Join(", ", lsMarkError);
                                isLegalBot = false;
                                string error = "";
                                jrvm.ErrorDescription += error;
                            }
                            jrvm.MarkDK = MarkDK;

                        }
                        #endregion

                        #region hoc ky 2 :Month6,Month7,KTDKGKII(D,V,GKII),Month8,Month9,KTDKCKII(D,V,CKII), HLMKII,XLHLMKII,HLMKCN,XLHLMKCN
                        if (SemesterIDImport.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            Month6 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['E']));
                            Month7 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['F']));
                            string KTDKGKII_D = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['G']));
                            string KTDKGKII_V = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['H']));
                            string KTDKGKI_GKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['I']));
                            MarkDK[jrvm.PupilID.ToString() + "GKII_V"] = KTDKGKII_V;
                            MarkDK[jrvm.PupilID.ToString() + "GKII_D"] = KTDKGKII_D;

                            jrvm.strKTDKGKII = KTDKGKI_GKII;

                            Month8 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['J']));
                            Month9 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['K']));

                            string KTDKCKII_D = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['L']));
                            string KTDKCKII_V = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['M']));
                            string KTDKCKII_CKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['N']));

                            MarkDK[jrvm.PupilID.ToString() + "CKII_V"] = KTDKCKII_V;
                            MarkDK[jrvm.PupilID.ToString() + "CKII_D"] = KTDKCKII_D;
                            jrvm.strKTDKCKII = KTDKCKII_CKII;

                            HLMKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['O']));
                            XLHLMKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['P']));

                            HLMKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['Q']));
                            XLHLMKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['R']));

                            jrvm.Month6 = Month6;
                            jrvm.Month7 = Month7;
                            jrvm.Month8 = Month8;
                            jrvm.Month9 = Month9;

                            jrvm.strHLMKII = HLMKII;
                            jrvm.XLHLMKII = XLHLMKII;

                            decimal result = 0;
                            decimal result1 = 0;

                            if (KTDKGKII_D.Trim().Length != 0)
                            {
                                if (decimal.TryParse(KTDKGKII_D, out result))
                                {
                                    MarkDK[jrvm.PupilID.ToString() + "GKII_D"] = result;
                                    if (result > 10 || result <= 0 )
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_D");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_D");
                                }
                            }

                            if (KTDKCKII_D.Trim().Length != 0)
                            {
                                if (decimal.TryParse(KTDKCKII_D, out result))
                                {
                                    MarkDK[jrvm.PupilID.ToString() + "CKII_D"] = result;
                                    if (result > 10 || result <= 0)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_D");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_D");
                                }
                            }

                            if (KTDKGKII_V.Trim().Length != 0)
                            {
                                if (decimal.TryParse(KTDKGKII_V, out result))
                                {
                                    MarkDK[jrvm.PupilID.ToString() + "GKII_V"] = result;
                                    if (result > 10 || result <= 0)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_V");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_V");
                                }
                            }

                            if (KTDKCKII_V.Trim().Length != 0)
                            {
                                if (decimal.TryParse(KTDKCKII_V, out result))
                                {
                                    if (decimal.TryParse(KTDKCKII_V, out result1))
                                    {
                                        MarkDK[jrvm.PupilID.ToString() + "CKII_V"] = result;
                                    }
                                    if (result > 10 || result <= 0 )
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_V");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTDKCK_V");
                                }
                            }

                            if (!"0".Equals(KTDKGKI_GKII.Trim()) && KTDKGKI_GKII.Trim().Length != 0)
                            {
                                if (decimal.TryParse(KTDKGKI_GKII, out result))
                                {
                                    jrvm.KTDKGKII = result;
                                    if (result > 10)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTGKError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTGKError");
                                }
                            }

                            if (!"0".Equals(KTDKCKII_CKII.Trim()) && KTDKCKII_CKII.Trim().Length != 0)
                            {
                                if (decimal.TryParse(KTDKCKII_CKII, out result))
                                {
                                    jrvm.KTDKCKII = result;
                                    if (result > 10)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                }
                            }

                            if (HLMKII.Trim().Length != 0)
                            {
                                if (decimal.TryParse(HLMKII, out result))
                                {
                                    jrvm.HLMKII = result;
                                    if (result > 10)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMError");
                                }
                            }

                            if (!IsValidHK(XLHLMKII))
                            {
                                isLegalBot = false;
                                jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HKError");
                            }

                            //diem thang
                            List<int> lsMarkError = new List<int>();

                            if (!IsValidMark(Month6))
                            {
                                lsMarkError.Add(6);
                            }
                            if (!IsValidMark(Month7))
                            {
                                lsMarkError.Add(7);
                            }
                            if (!IsValidMark(Month8))
                            {
                                lsMarkError.Add(8);
                            }
                            if (!IsValidMark(Month9))
                            {
                                lsMarkError.Add(9);
                            }
                            if (lsMarkError.Count() > 0)
                            {
                                string monthError = string.Join(", ", lsMarkError);
                                isLegalBot = false;
                                string error = string.Format(Res.Get("MarkRecordOfPrimary_Label_MonthMarkError"), monthError);
                                jrvm.ErrorDescription += error;
                            }
                            if (!PupilNeedCheck)
                            {
                                string monthError = string.Join(", ", lsMarkError);
                                isLegalBot = false;
                                string error = "";
                                jrvm.ErrorDescription += error;
                            }
                            jrvm.MarkDK = MarkDK;
                        }
                        #endregion
                    }
                    #endregion

                    #region Môn khác
                    //Nếu là môn khác
                    if (isMonKhac)
                    {
                        #region hoc ky 1: quan tam den cac diem:month1,month2,month3,month4,month5,KTDKCKI,HLMKI,XLHLMKI
                        if (SemesterIDImport.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            Month1 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['E']));
                            Month2 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['F']));

                            Month3 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['G']));
                            Month4 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['H']));
                            Month5 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['I']));

                            KTDKCKI = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['J']));
                            HLMKI = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['K']));
                            XLHLMKI = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['L']));

                            jrvm.Month1 = Month1;
                            jrvm.Month2 = Month2;
                            jrvm.Month3 = Month3;
                            jrvm.Month4 = Month4;
                            jrvm.Month5 = Month5;

                            jrvm.strKTDKCKI = KTDKCKI;
                            jrvm.strHLMKI = HLMKI;
                            jrvm.XLHLMKI = XLHLMKI;

                            //neu hs ko nhap diem thi bo qua
                            List<string> lsMark = new List<string>() { Month1, Month2, Month3, Month4, Month5, KTDKCKI, HLMKI, XLHLMKI };
                            if (!CheckIsPupilHasMark(lsMark))
                            {
                                //continue;
                            }

                            decimal result = 0;

                            if (!"0".Equals(KTDKCKI.Trim()) && KTDKCKI.Trim().Length != 0)
                            {
                                if (KTDKCKI.Contains(",") || KTDKCKI.Contains("."))
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                }
                                else 
                                if (decimal.TryParse(KTDKCKI, out result))
                                {
                                    jrvm.KTDKCKI = result;
                                    if (result > 10 || result == 0)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                }
                            }

                            if (HLMKI.Trim().Length != 0)
                            {
                                if (decimal.TryParse(HLMKI, out result))
                                {
                                    jrvm.HLMKI = result;
                                    if (result > 10)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMError");
                                }
                            }

                            if (!IsValidHK(XLHLMKI))
                            {
                                isLegalBot = false;
                                jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HKError");
                            }

                            //diem thang
                            List<int> lsMarkError = new List<int>();

                            if (!IsValidMark(Month1))
                            {
                                lsMarkError.Add(1);
                            }
                            if (!IsValidMark(Month2))
                            {
                                lsMarkError.Add(2);
                            }
                            if (!IsValidMark(Month3))
                            {
                                lsMarkError.Add(3);
                            }
                            if (!IsValidMark(Month4))
                            {
                                lsMarkError.Add(4);
                            }
                            if (!IsValidMark(Month5))
                            {
                                lsMarkError.Add(5);
                            }
                            if (lsMarkError.Count() > 0)
                            {
                                string monthError = string.Join(", ", lsMarkError);
                                isLegalBot = false;
                                string error = string.Format(Res.Get("MarkRecordOfPrimary_Label_MonthMarkError"), monthError);
                                jrvm.ErrorDescription += error;
                            }
                            if (!PupilNeedCheck)
                            {
                                string monthError = string.Join(", ", lsMarkError);
                                isLegalBot = false;
                                string error = "";
                                jrvm.ErrorDescription += error;
                            }
                        }
                        #endregion

                        #region hoc ky 2: quan tam den cac diem:month6,month7,month8,month9,KTDKCKII,HLMKII,XLHLMKII,HLMKCN,XLHLMKCN
                        if (SemesterIDImport.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            Month6 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['E']));
                            Month7 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['F']));

                            Month8 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['G']));
                            Month9 = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['H']));

                            KTDKCKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['I']));
                            HLMKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['J']));
                            XLHLMKII = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['K']));

                            HLMKCN = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['L']));
                            XLHLMKCN = ConvertToString(sheet.GetCellValue(i + 8, VTVector.dic['M']));

                            jrvm.Month6 = Month6;
                            jrvm.Month7 = Month7;
                            jrvm.Month8 = Month8;
                            jrvm.Month9 = Month9;

                            jrvm.strKTDKCKII = KTDKCKII;
                            jrvm.strHLMKII = HLMKII;
                            jrvm.XLHLMKII = XLHLMKII;

                            //neu hs ko nhap diem thi bo qua
                            List<string> lsMark = new List<string>() { Month6, Month7, Month8, Month9, Month5, KTDKCKII, HLMKII, XLHLMKII, HLMKCN, XLHLMKCN };
                            if (!CheckIsPupilHasMark(lsMark))
                            {
                                //continue;
                            }
                            //validate
                            decimal result = 0;
                            if (!"0".Equals(KTDKCKII.Trim()) && KTDKCKII.Trim().Length != 0)
                            {
                                if (KTDKCKII.Contains(",") || KTDKCKII.Contains("."))
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                }
                                else
                                if (decimal.TryParse(KTDKCKII, out result))
                                {
                                    jrvm.KTDKCKII = result;
                                    if (result > 10 || result == 0)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_KTCKError");
                                }
                            }

                            if (HLMKII.Trim().Length != 0)
                            {
                                if (decimal.TryParse(HLMKII, out result))
                                {
                                    jrvm.HLMKII = result;
                                    if (result > 10)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMError");
                                    }
                                }
                                else
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMError");
                                }
                            }
                            if (!IsValidHK(XLHLMKII))
                            {
                                isLegalBot = false;
                                jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HKError");
                            }

                            if (HLMKCN.Trim().Length != 0)
                            {
                                if (!decimal.TryParse(HLMKCN, out result))
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMCNError");
                                }
                                else
                                {
                                    if (result > 10)
                                    {
                                        isLegalBot = false;
                                        jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HLMCNError");
                                    }
                                }
                            }
                            if (!IsValidHK(XLHLMKCN))
                            {
                                isLegalBot = false;
                                jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HKCNError");
                            }
                            else
                            {
                                if (result > 10)
                                {
                                    isLegalBot = false;
                                    jrvm.ErrorDescription += Res.Get("MarkRecordOfPrimary_Label_HKCNError");
                                }
                            }

                            //diem thang
                            List<int> lsMarkError = new List<int>();

                            if (!IsValidMark(Month6))
                            {
                                lsMarkError.Add(6);
                            }
                            if (!IsValidMark(Month7))
                            {
                                lsMarkError.Add(7);
                            }
                            if (!IsValidMark(Month8))
                            {
                                lsMarkError.Add(8);
                            }
                            if (!IsValidMark(Month9))
                            {
                                lsMarkError.Add(9);
                            }
                            if (lsMarkError.Count() > 0)
                            {
                                string monthError = string.Join(", ", lsMarkError);
                                isLegalBot = false;
                                string error = string.Format(Res.Get("MarkRecordOfPrimary_Label_MonthMarkError"), monthError);
                                jrvm.ErrorDescription += error;
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
                #endregion

                jrvm.isLegal = isLegalBot;
                lsJRVM.Add(jrvm);
            }
            return lsJRVM;
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        private bool IsValidMark(string Mark)
        {
            if (Mark == null)
            {
                Mark = "";
            }
            if (Mark.Trim().Length == 0)
            {
                return true;
            }
            var lsMark = Mark.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var item in lsMark)
            {
                if (item.Equals("0") || item.Contains(",") || item.Contains(".")) return false;
                decimal output = 0;
                if (!decimal.TryParse(item, out output))
                {
                    return false;
                }
                if (output > 10)
                {
                    return false;
                }
            }
            return true;
        }

        private bool IsValidHK(string HK)
        {
            HK = HK.Trim();
            if (HK.Length == 0)
            {
                return true;
            }
            if (HK.ToLower() == "trung bình")
            {
                HK = "TB";
            }
            List<string> lsHanhKiem = new List<string>() { 
                    SystemParamsInFile.REPORT_CAPACITYBYMARK_EXCELLENT, 
                    SystemParamsInFile.REPORT_CAPACITYBYMARK_GOOD,
                    SystemParamsInFile.REPORT_CAPACITYBYMARK_NORMAL,
                    SystemParamsInFile.REPORT_CAPACITYBYMARK_BAD
                };
            return lsHanhKiem.Contains(HK);
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult ReturnImportData(int? ClassIDImport, int? SubjectIDImport, int? SemesterIDImport, int? EducationLevelIDImport)
        {
            ViewData[MarkRecordOfPrimaryConstants.LIST_MARKRECORD] = new List<MarkRecordOfPrimaryViewModel>();
            if (SemesterIDImport != null)
            {
                if (SemesterIDImport.Value == 1)
                {
                    ViewData[MarkRecordOfPrimaryConstants.BOOL_HKI] = true;
                    ViewData[MarkRecordOfPrimaryConstants.BOOL_HKII] = false;
                }
                if (SemesterIDImport.Value == 2)
                {
                    ViewData[MarkRecordOfPrimaryConstants.BOOL_HKI] = false;
                    ViewData[MarkRecordOfPrimaryConstants.BOOL_HKII] = true;
                }
                List<MarkRecordOfPrimaryViewModel> lstmr = (List<MarkRecordOfPrimaryViewModel>)Session["ImportData"];
                ViewData[MarkRecordOfPrimaryConstants.LIST_MARKRECORD] = lstmr;
                ViewData[MarkRecordOfPrimaryConstants.ERROR_IMPORT_MESSAGE] = "Có lỗi trong file excel";
                ViewData[MarkRecordOfPrimaryConstants.HAS_ERROR_DATA] = lstmr.Where(o => o.ErrorDescription != null).Any();
                ViewData[MarkRecordOfPrimaryConstants.ERROR_BASIC_DATA] = false;

                List<int> ListToanID = ClassSubjectBusiness.All.Where(o => o.SubjectCat.DisplayName.Contains(SystemParamsInFile.MON_TOAN) &&
                    o.SubjectCat.AppliedLevel == globalInfo.AppliedLevel && o.SubjectCat.IsCoreSubject).Select(o => o.SubjectID).ToList();
                List<int> ListTiengVietID = ClassSubjectBusiness.All.Where(o => o.SubjectCat.DisplayName.Contains(SystemParamsInFile.MON_TIENGVIET) &&
                   o.SubjectCat.AppliedLevel == globalInfo.AppliedLevel && o.SubjectCat.IsCoreSubject).Select(o => o.SubjectID).ToList();


                //Nếu là môn toán
                if (ListToanID.Contains(SubjectIDImport.Value))
                {
                    return PartialView("_ListToanForImport");
                }
                else
                {
                    //Nếu là môn tiếng việt
                    if (ListTiengVietID.Contains(SubjectIDImport.Value))
                    {
                        return PartialView("_ListTVForImport");
                    }
                    //Nếu là môn khác
                    else
                    {
                        return PartialView("_ListMKForImport");
                    }
                }

            }
            return PartialView("_ListForImport");
        }

        private string ConvertToString(object o)
        {
            if (o == null)
            {
                return "";
            }
            return o.ToString().Trim();
        }

        private bool CheckIsPupilHasMark(List<string> lsMark)
        {
            return lsMark.Any(o => o.Trim().Length > 0);
        }
        #endregion

        private string GetCellString(IVTWorksheet sheet, string cellName)
        {
            object o = sheet.GetRange(cellName, cellName).Value;
            return o == null ? string.Empty : o.ToString().Trim();
        }
    }
}





