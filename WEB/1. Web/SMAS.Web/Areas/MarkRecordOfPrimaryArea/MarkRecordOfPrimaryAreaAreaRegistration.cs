﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MarkRecordOfPrimaryArea
{
    public class MarkRecordOfPrimaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MarkRecordOfPrimaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MarkRecordOfPrimaryArea_default",
                "MarkRecordOfPrimaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
