/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.MarkRecordOfPrimaryArea.Models
{
    public class MarkRecordOfPrimaryViewModel
    {
        public int MarkRecordID { get; set; }
        public int? PupilID { get; set; }

        [ResourceDisplayName("MarkRecord_Column_PupilCode")]
        public string PupilCode { get; set; }
        [ResourceDisplayName("MarkRecord_Column_FullName")]
        public string PupilName { get; set; }

        public string Name { get; set; }

        public int? OrderInClass { get; set; }
        public Dictionary<string, object> MarkDK { get; set; }

        [ResourceDisplayName("MarkRecord_Column_Month1")]
        public string Month1 { get; set; }
        [ResourceDisplayName("MarkRecord_Column_Month2")]
        public string Month2 { get; set; }
        [ResourceDisplayName("MarkRecord_Column_Month3")]
        public string Month3 { get; set; }
        [ResourceDisplayName("MarkRecord_Column_Month4")]
        public string Month4 { get; set; }
        [ResourceDisplayName("MarkRecord_Column_Month5")]
        public string Month5 { get; set; }
        [ResourceDisplayName("MarkRecord_Column_Month6")]
        public string Month6 { get; set; }
        [ResourceDisplayName("MarkRecord_Column_Month7")]
        public string Month7 { get; set; }
        [ResourceDisplayName("MarkRecord_Column_Month8")]
        public string Month8 { get; set; }
        [ResourceDisplayName("MarkRecord_Column_Month9")]
        public string Month9 { get; set; }

        [ResourceDisplayName("MarkRecord_Column_KTDKGKI")]
        public decimal? KTDKGKI { get; set; }
        [ResourceDisplayName("MarkRecord_Column_KTDKGKII")]
        public decimal? KTDKGKII { get; set; }
        [ResourceDisplayName("MarkRecord_Column_KTDKCKI")]
        public decimal? KTDKCKI { get; set; }
        [ResourceDisplayName("MarkRecord_Column_KTDKCKII")]
        public decimal? KTDKCKII { get; set; }
        [ResourceDisplayName("MarkRecord_Column_HLMKI")]
        public decimal? HLMKI { get; set; }
        [ResourceDisplayName("MarkRecord_Column_HLMKII")]
        public decimal? HLMKII { get; set; }
        [ResourceDisplayName("MarkRecord_Column_XLHLMHKI")]
        public string XLHLMKI { get; set; }
        [ResourceDisplayName("MarkRecord_Column_XLHLMHKII")]
        public string XLHLMKII { get; set; }
        [ResourceDisplayName("MarkRecord_Column_HLMN")]
        public decimal? HLMN { get; set; }
        [ResourceDisplayName("MarkRecord_Column_XLHLMN")]
        public string XLHLMN { get; set; }


        public DateTime? TimeStatus { get; set; }
        public bool? Show { get; set; }
        public bool? Red { get; set; }
        public bool? checkbox { get; set; }
        public string LockTitle { get; set; }

        public MarkRecordOfPrimaryViewModel()
        {
            MarkDK = new Dictionary<string, object>();
        }

        #region import additional

        public bool isLegal { get; set; }

        public string ErrorDescription { get; set; }

        [ResourceDisplayName("MarkRecord_Column_KTDKGKI")]
        public string strKTDKGKI { get; set; }
        [ResourceDisplayName("MarkRecord_Column_KTDKGKII")]
        public string strKTDKGKII { get; set; }
        [ResourceDisplayName("MarkRecord_Column_KTDKCKI")]
        public string strKTDKCKI { get; set; }
        [ResourceDisplayName("MarkRecord_Column_KTDKCKII")]
        public string strKTDKCKII { get; set; }
        [ResourceDisplayName("MarkRecord_Column_HLMKI")]
        public string strHLMKI { get; set; }
        [ResourceDisplayName("MarkRecord_Column_HLMKII")]
        public string strHLMKII { get; set; }

        #endregion import additional
    }
}


