/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.MarkRecordOfPrimaryArea
{
    public class MarkRecordOfPrimaryConstants
    {
        public const string LIST_MARKRECORD = "listMarkRecord";
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "ListEducationLevel";
        public const string LIST_CLASS = "ListClass";
        public const string LIST_SUBJECT = "ListSubject";
        public const string LABEL_MES = "LabelMessage";
        public const string BOOL_TOAN = "BoolToan";
        public const string BOOL_MONKHAC = "BoolMonKhac";
        public const string BOOL_TIENGVIET = "BoolTiengViet";
        public const string BOOL_HKI = "BoolSemesterI";
        public const string BOOL_HKII = "BoolSemesterII";


        public const string LIST_IMPORTDATA = "LIST_IMPORTDATA";
        public const string HAS_ERROR_DATA = "HAS_ERROR_DATA";
        public const string ERROR_BASIC_DATA = "ERROR_BASIC_DATA";
        public const string ERROR_IMPORT_MESSAGE = "ERROR_IMPORT_MESSAGE";
        public const string DISABLED_BUTTON = "DISABLED_BUTTON";
        public const string LOCK_MARK_TITLE = "LOCK_MARK_TITLE";
    }
}