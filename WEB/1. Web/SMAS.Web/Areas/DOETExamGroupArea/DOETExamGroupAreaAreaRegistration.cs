﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamGroupArea
{
    public class DOETExamGroupAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DOETExamGroupArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DOETExamGroupArea_default",
                "DOETExamGroupArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
