﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DOETExamGroupArea
{
    public class DOETExamGroupConstants
    {
        public const string CBO_EXAMINATIONS = "cbo_examinations";
        public const string LIST_EXAM_GROUP = "list_exam_group";
        public const string PER_CHECK_CREATE = "per_check_create";
        public const string LIST_YEAR = "listyear";
        public const string DISABLE_ADD_NEWW = "DisableAddNew";
        public const string EXAMINATION_ID = "EXAMINATION_ID";
    }
}