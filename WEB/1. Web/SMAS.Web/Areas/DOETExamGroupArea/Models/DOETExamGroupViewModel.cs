﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SMAS.Web.Models.Attributes;
using System.Web.Mvc;
using SMAS.Web.Areas.DOETExamGroupArea;
using Resources;
namespace SMAS.Web.Areas.DOETExamGroupArea.Models
{
    public class DOETExamGroupViewModel
    {
        [ScaffoldColumn(false)]
        public int ExamGroupID { get; set; }

        [ResourceDisplayName("EmployeeHistoryStatus_Label_AppliedLevel")]        
        public int AppliedLevel { get; set; }

        public int ExaminationsID { get; set; }

        [ResourceDisplayName("ExamGroup_Label_ExamGroupCode")]        
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamGroup_Validate_Required_ExamGroupCode")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamGroup_Validate_Regex_ExamGroupCode")]
        [StringLength(5, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ExamGroupCode { get; set; }

        [ResourceDisplayName("ExamGroup_Label_ExamGroupName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ExamGroup_Validate_Required_ExamGroupName")]
        [StringLength(64, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string ExamGroupName { get; set; }

        [ResourceDisplayName("ExamGroup_Label_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamGroupConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public long? tempExaminationsID { get; set; }

        public bool checkEdit { get; set; }

        public bool checkDelete { get; set; }

        public bool disableGrade { get; set; }
    }
}