﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Web.Filter;
using SMAS.Web.Areas.ExamGroupArea.Models;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Web.Areas.DOETExamGroupArea.Models;
using SMAS.Business.BusinessObject;
using System.Text;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.DOETExamGroupArea.Controllers
{
    public class DOETExamGroupController : BaseController
    {
        #region Properties
        private readonly IDOETExamGroupBusiness DOETExamGroupBusiness;
        private readonly IDOETExaminationsBusiness DOETExaminationsBusiness;
        private readonly IDOETExamPupilBusiness DOETExamPupilBusiness;
        private readonly IDOETExamSubjectBusiness DOETExamSubjectBusiness;
        private List<DOETExaminations> listExaminations;
        #endregion

        #region Constructor
        public DOETExamGroupController(IDOETExamGroupBusiness examGroupBusiness, IDOETExaminationsBusiness examinationsBusiness,
            IDOETExamPupilBusiness DOETExamPupilBusiness, IDOETExamSubjectBusiness DOETExamSubjectBusiness)
        {
            this.DOETExamGroupBusiness = examGroupBusiness;
            this.DOETExaminationsBusiness = examinationsBusiness;
            this.DOETExamPupilBusiness = DOETExamPupilBusiness;
            this.DOETExamSubjectBusiness = DOETExamSubjectBusiness;
            listExaminations = new List<DOETExaminations>();
        }
        #endregion

        //#region Actions
        ////
        //// GET: /DOETExamGroupArea/DOETExamGroup/

        public ActionResult Index()
        {
            SetViewDataPermission("DOETExamGroup", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            int unitId = _globalInfo.SupervisingDeptID.HasValue ? _globalInfo.SupervisingDeptID.Value : 0;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("UnitID", unitId);
            //Lấy danh sách kỳ thi
            listExaminations = DOETExaminationsBusiness.Search(dic).ToList();
            if (listExaminations != null && listExaminations.Count > 0)
            {
                //list nam hoc
                // List<int> listYear = listExaminations.Select(p => p.Year).Distinct().OrderByDescending(p => p).ToList();
                //List<YearBO> listStrYear = UtilsBusiness.GetListYearDOET(listYear);
                List<ComboObject> listYear = GetListYear();
                List<YearBO> listStrYear = listYear.Select(p => new YearBO
                {
                    YearID = int.Parse(p.key),
                    DisplayYear = p.value
                }).ToList();
                int Year = listYear.Select(p => int.Parse(p.key)).FirstOrDefault();
                listExaminations = listExaminations.Where(p => p.Year == Year).OrderByDescending(o => o.CreateTime).ToList();
                ViewData[DOETExamGroupConstants.CBO_EXAMINATIONS] = new SelectList(listExaminations, "ExaminationsID", "ExaminationsName");
                ViewData[DOETExamGroupConstants.LIST_YEAR] = new SelectList(listStrYear, "YearID", "DisplayYear");


                //Search du lieu
                int examIdFirst = listExaminations.FirstOrDefault() != null ? listExaminations.FirstOrDefault().ExaminationsID : 0;
                List<DOETExamGroupViewModel> listResult = _Search(examIdFirst).ToList();
                ViewData[DOETExamGroupConstants.LIST_EXAM_GROUP] = listResult;
                ViewData[DOETExamGroupConstants.DISABLE_ADD_NEWW] = this.CheckDisableAddNew(examIdFirst);
                ViewData[DOETExamGroupConstants.EXAMINATION_ID] = examIdFirst;
                //Kiem tra button
                CheckCommandPermision(listExaminations.FirstOrDefault(), listResult);
            }
            else
            {
                ViewData[DOETExamGroupConstants.EXAMINATION_ID] = 0;
            }
            return View();
        }

        //
        // GET: /ExamGroup/Search

        public PartialViewResult Search(int? examinationsID)
        {
            SetViewDataPermission("DOETExamGroup", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            List<DOETExamGroupViewModel> listResult = new List<DOETExamGroupViewModel>();
            if (examinationsID.HasValue)
            {
                listResult = _Search(examinationsID.GetValueOrDefault()).ToList();
                //Kiem tra button
                DOETExaminations examinationOBj = DOETExaminationsBusiness.Find(examinationsID);
                ViewData[DOETExamGroupConstants.DISABLE_ADD_NEWW] = this.CheckDisableAddNew(examinationsID.HasValue ? examinationsID.Value : 0);
                ViewData[DOETExamGroupConstants.EXAMINATION_ID] = examinationsID.Value;
                CheckCommandPermision(examinationOBj, listResult);
            }
            else
            {
                ViewData[DOETExamGroupConstants.EXAMINATION_ID] = 0;
            }
            return PartialView("_List", listResult);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Create(FormCollection fr)
        {
            try
            {

                int examinationId = Int32.Parse(fr["ExaminationsID"]);
                string groupName = fr["ExamGroupName"];
                string groupCode = fr["ExamGroupCode"];
                int eduationGrade = Int32.Parse(fr["HfEducationGrade"]);
                if (this.CheckDisableAddNew(examinationId))
                {
                    return Json(new JsonMessage("Kỳ thi đã hết hạn", "success"));
                }
                DOETExamGroup groupObj = new DOETExamGroup();
                groupObj.ExamGroupName = groupName;
                groupObj.ExamGroupCode = groupCode;
                groupObj.ExaminationsID = examinationId;
                groupObj.AppliedLevel = eduationGrade;
                groupObj.CreateTime = DateTime.Now;

                #region Check Validate
                if (groupName.Equals(string.Empty))
                {
                    return Json(new JsonMessage("Tên nhóm thi không được bỏ trống.", "error"));
                }
                if (groupCode.Equals(string.Empty))
                {
                    return Json(new JsonMessage("Mã nhóm thi không được bỏ trống.", "error"));
                }
                if (eduationGrade == 0)
                {
                    return Json(new JsonMessage("Thầy cô cần chọn cấp học của nhóm thi.", "error"));
                }
                if (DOETExamGroupBusiness.CheckGroupName(examinationId, groupName))
                {
                    return Json(new JsonMessage("Tên nhóm đã tồn tại trong kỳ thi", "error"));
                }
                if (DOETExamGroupBusiness.CheckGroupCode(examinationId, groupCode))
                {
                    return Json(new JsonMessage("Mã nhóm đã tồn tại trong kỳ thi", "error"));
                }
                #endregion
                //Luu
                DOETExamGroupBusiness.InsertOrUpdateGroup(groupObj, true);
                //ghi log
                SetViewDataActionAudit(String.Empty, String.Empty
                , String.Empty
                , Res.Get("DOETExamGroup_Log_Create")
                , String.Empty
                , Res.Get("DOETExamGroup_Log_FunctionName")
                , SMAS.Business.Common.GlobalConstants.ACTION_ADD
                , Res.Get("DOETExamGroup_Log_Create"));

                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage"), "success"));
            }
            catch (Exception ex)
            {
                string para = "FormCollection fr";
                LogExtensions.ErrorExt(logger, DateTime.Now, "Create",para,ex);
                return Json(new JsonMessage("Lỗi trong quá trình xử lý, xin thử lại", "error"));
            }
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(FormCollection fr)
        {
            int examinationId = Int32.Parse(fr["ExaminationsID"]);
            string groupName = fr["ExamGroupName"];
            string groupCode = fr["ExamGroupCode"];
            int eduationGrade = Int32.Parse(fr["HfEducationGradeEdit"]);
            int examGroupId = Int32.Parse(fr["ExamGroupID"]);

            DOETExamGroup groupObj = new DOETExamGroup();
            groupObj.ExamGroupID = examGroupId;
            groupObj.ExamGroupName = groupName;
            groupObj.ExamGroupCode = groupCode;
            groupObj.ExaminationsID = examinationId;
            groupObj.AppliedLevel = eduationGrade;
            groupObj.UpdateTime = DateTime.Now;
            if (this.CheckDisableAddNew(examinationId))
            {
                return Json(new JsonMessage("Kỳ thi đã hết hạn", "success"));
            }
            #region Check Validate
            if (groupName.Equals(string.Empty))
            {
                return Json(new JsonMessage("Tên nhóm thi không được bỏ trống.", "error"));
            }
            if (groupCode.Equals(string.Empty))
            {
                return Json(new JsonMessage("Mã nhóm thi không được bỏ trống.", "error"));
            }
            if (eduationGrade == 0)
            {
                return Json(new JsonMessage("Thầy cô cần chọn cấp học của nhóm thi.", "error"));
            }
            if (DOETExamGroupBusiness.CheckGroupNameUpdate(examinationId, examGroupId, groupName))
            {
                return Json(new JsonMessage("Tên nhóm đã tồn tại trong kỳ thi", "error"));
            }
            if (DOETExamGroupBusiness.CheckGroupCodeUpdate(examinationId, examGroupId, groupCode))
            {
                return Json(new JsonMessage("Mã nhóm đã tồn tại trong kỳ thi", "error"));
            }
            #endregion
            //Luu
            DOETExamGroupBusiness.InsertOrUpdateGroup(groupObj, false);

            //ghi log
            SetViewDataActionAudit(String.Empty, String.Empty
            , groupObj.ExamGroupID.ToString()
            , Res.Get("ExamGroup_Log_Edit")
            , "ExamGroupID: " + groupObj.ExamGroupID
            , Res.Get("ExamGroup_Log_FunctionName")
            , SMAS.Business.Common.GlobalConstants.ACTION_UPDATE
            , Res.Get("ExamGroup_Log_Edit"));

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        //#endregion

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Delete(int examGroupId, int examinationsId)
        {
            if (this.CheckDisableAddNew(examinationsId))
            {
                return Json(new JsonMessage("Kỳ thi đã hết hạn", "error"));
            }
            //Xoa mon hoc lien quan den nhom
            List<DOETExamSubject> listsubjectObj = DOETExamSubjectBusiness.All.Where(p => p.ExamGroupID == examGroupId).ToList();
            if (listsubjectObj != null && listsubjectObj.Count > 0)
            {
                for (int i = 0; i < listsubjectObj.Count; i++)
                {
                    DOETExamSubjectBusiness.Delete(listsubjectObj[i].ExamSubjectID);
                }
                DOETExamSubjectBusiness.Save();
            }
            //Xoa nhom
            this.DOETExamGroupBusiness.Delete(examGroupId);
            this.DOETExamGroupBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        //#region Private methods


        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private List<DOETExamGroupViewModel> _Search(long examinationID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ExaminationsID", examinationID);
            List<DOETExamGroupViewModel> ret = DOETExamGroupBusiness.Search(dic).ToList()
                                            .Select(o => new DOETExamGroupViewModel
                                            {
                                                ExamGroupID = o.ExamGroupID,
                                                ExamGroupCode = o.ExamGroupCode,
                                                ExamGroupName = o.ExamGroupName,
                                                AppliedLevel = o.AppliedLevel
                                            }).ToList();
            return ret;
        }

        /// <summary>
        /// Kiem tra disable/enable, an/hien cac button
        /// </summary>
        /// <param name="listResult"></param>
        private void CheckCommandPermision(DOETExaminations examinations, List<DOETExamGroupViewModel> listResult)
        {
            //SetViewDataPermission("DOETExamGroup", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            //Kiem tra quyen them, sua, xoa
            bool checkCreate = false;

            //Kiem tra quyen them
            if (examinations != null)
            {
                if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_CREATE])
                {
                    checkCreate = true;
                }
            }
            ViewData[DOETExamGroupConstants.PER_CHECK_CREATE] = checkCreate;

            //Kiem tra quyen sua, xoa
            //Lay danh sach du lieu rang buoc
            List<int> lstExamGroupID = listResult.Select(o => o.ExamGroupID).ToList<int>();
            Dictionary<string, object> searchObj = new Dictionary<string, object>();
            searchObj.Add("ExaminationsID", examinations.ExaminationsID);
            searchObj.Add("ListGroupID", lstExamGroupID);
            searchObj.Add("UnitID", _globalInfo.SupervisingDeptID);

            List<int> lstInUsedExamGroupID = DOETExamPupilBusiness.Search(searchObj).Select(p => p.ExamGroupID).Distinct().ToList();
            List<int> lstInUsedExamSubject = DOETExamSubjectBusiness.Search(searchObj).Select(p => p.ExamGroupID).Distinct().ToList();
            for (int i = 0; i < listResult.Count; i++)
            {
                DOETExamGroupViewModel ex = listResult[i];
                ex.checkEdit = false;
                ex.checkDelete = false;

                if (examinations != null)
                {
                    bool isExamGroupUsed = lstInUsedExamGroupID.Contains(ex.ExamGroupID);
                    bool isExamSubjectUsed = lstInUsedExamSubject.Contains(ex.ExamGroupID);
                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_UPDATE]
                        && !isExamSubjectUsed)
                    {
                        ex.checkEdit = true;
                    }

                    if ((bool)ViewData[SMAS.Business.Common.SystemParamsInFile.PERMISSION_DELETE]
                     && !isExamGroupUsed)
                    {
                        ex.checkDelete = true;
                    }

                    if (isExamGroupUsed)
                    {
                        ex.disableGrade = true;
                    }
                    else
                    {
                        ex.disableGrade = false;
                    }
                }
            }
        }

        public JsonResult OnchangeYear(int year)
        {
            int unitId = _globalInfo.SupervisingDeptID.HasValue ? _globalInfo.SupervisingDeptID.Value : 0;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("UnitID", unitId);
            dic.Add("Year", year);
            //Lấy danh sách kỳ thi
            listExaminations = DOETExaminationsBusiness.Search(dic).ToList();
            StringBuilder strAppend = null;
            if (listExaminations != null && listExaminations.Count > 0)
            {
                strAppend = new StringBuilder();
                for (int i = 0; i < listExaminations.Count; i++)
                {
                    strAppend.Append("<option value=" + listExaminations[i].ExaminationsID + ">" + listExaminations[i].ExaminationsName + "</option>");
                }
            }
            if (strAppend != null)
            {
                return Json(new JsonMessage(strAppend.ToString(), "success"));
            }
            return Json(new JsonMessage(string.Empty, "success")); ;
        }

        public bool CheckDisableAddNew(int examinationsId)
        {
            DOETExaminations examObj = DOETExaminationsBusiness.All.Where(p => p.ExaminationsID == examinationsId).FirstOrDefault();
            if (examObj != null)
            {
                DateTime dateDeadline = examObj.DeadLine;
                DateTime dateNow = DateTime.Now;
                //Neu thoi gian dang ky nho thoi gian hien tai thi het han dang ky
                if (dateDeadline.Date < dateNow.Date)
                {
                    return true;
                }
                return false;
            }
            return false;
        }
    }
}
