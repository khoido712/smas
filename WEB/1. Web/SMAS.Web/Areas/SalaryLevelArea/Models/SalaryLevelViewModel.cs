/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  namdv
* @version $Revision: $
*/

using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.SalaryLevelArea.Models
{
    public class SalaryLevelViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 SalaryLevelID { get; set; }

        [ScaffoldColumn(false)]
        public System.String Resolution { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("SalaryLevel_Label_Scale")]
        public string ScaleName { get; set; }

        [ResourceDisplayName("SalaryLevel_Label_Scale")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", SalaryLevelConstants.SCALE)]
        public System.Nullable<System.Int32> ScaleID { get; set; }

        [ResourceDisplayName("SalaryLevel_Label_SalaryLevel")]
        [UIHint("Combobox")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("ViewDataKey", SalaryLevelConstants.SALARYLEVEL)]
        public System.Nullable<int> SubLevel { get; set; }

        [ResourceDisplayName("SalaryLevel_Label_Coefficient")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [AdditionalMetadata("class", "decimal")]
        public System.Nullable<System.Decimal> Coefficient { get; set; }

        [ResourceDisplayName("SalaryLevel_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public System.String Description { get; set; }

        [ScaffoldColumn(false)]
        [ResourceDisplayName("SalaryLevel_Label_SalaryLevel")]
        public string SubLevelName { get; set; }
    }
}
