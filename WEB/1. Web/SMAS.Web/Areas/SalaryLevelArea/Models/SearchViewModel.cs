/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  namdv
* @version $Revision: $
*/

using System;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace SMAS.Web.Areas.SalaryLevelArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("SalaryLevel_Label_Scale")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SalaryLevelConstants.SCALE)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public Nullable<int> ScaleID { get; set; }

        [ResourceDisplayName("SalaryLevel_Label_SalaryLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", SalaryLevelConstants.SALARYLEVEL)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public Nullable<int> SubLevel { get; set; }
    }
}