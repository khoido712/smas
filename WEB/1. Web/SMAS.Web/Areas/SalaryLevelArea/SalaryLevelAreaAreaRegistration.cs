﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SalaryLevelArea
{
    public class SalaryLevelAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SalaryLevelArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SalaryLevelArea_default",
                "SalaryLevelArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
