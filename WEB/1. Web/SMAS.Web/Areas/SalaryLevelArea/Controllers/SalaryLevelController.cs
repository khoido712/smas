﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  namdv
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.SalaryLevelArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.SalaryLevelArea.Controllers
{
    public class SalaryLevelController : BaseController
    {
        private readonly ISalaryLevelBusiness SalaryLevelBusiness;
        private readonly IEmployeeScaleBusiness EmployeeScaleBusiness;

        public SalaryLevelController(ISalaryLevelBusiness salarylevelBusiness, IEmployeeScaleBusiness employeeScaleBusiness)
        {
            this.SalaryLevelBusiness = salarylevelBusiness;
            this.EmployeeScaleBusiness = employeeScaleBusiness;
        }

        //
        // GET: /SalaryLevel/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            //Get view data here
            var listScale = EmployeeScaleBusiness.Search(new Dictionary<string, object>()).OrderBy(o=>o.Resolution).ToList();
            var listLevel = GetLevels();
            ViewData[SalaryLevelConstants.SCALE] = new SelectList(listScale, "EmployeeScaleID", "Resolution");
            ViewData[SalaryLevelConstants.SALARYLEVEL] = new SelectList(listLevel, "Key", "Value");
            IEnumerable<SalaryLevelViewModel> lst = this._Search(SearchInfo);
            ViewData[SalaryLevelConstants.LIST_SALARYLEVEL] = lst;
            return View();
        }

        //
        // GET: /SalaryLevel/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            SearchInfo["ScaleID"] = frm.ScaleID;
            SearchInfo["SubLevel"] = frm.SubLevel;
            IEnumerable<SalaryLevelViewModel> lst = this._Search(SearchInfo);
            ViewData[SalaryLevelConstants.LIST_SALARYLEVEL] = lst;
            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            SalaryLevel salarylevel = new SalaryLevel();
            TryUpdateModel(salarylevel);

            salarylevel.IsActive = true;
            salarylevel.CreatedDate = DateTime.Now;

            Utils.Utils.TrimObject(salarylevel);

            this.SalaryLevelBusiness.Insert(salarylevel);
            this.SalaryLevelBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int SalaryLevelID, FormCollection col)
        {
            SalaryLevel salarylevel = this.SalaryLevelBusiness.Find(SalaryLevelID);
            TryUpdateModel(salarylevel);
            Utils.Utils.TrimObject(salarylevel);
            salarylevel.Coefficient = Convert.ToDecimal(col["Coefficient"]);
            this.SalaryLevelBusiness.Update(salarylevel);
            this.SalaryLevelBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.SalaryLevelBusiness.Delete(id);
            this.SalaryLevelBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<SalaryLevelViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<SalaryLevel> query = this.SalaryLevelBusiness.Search(SearchInfo);
            List<SalaryLevelViewModel> lst = query.ToList().Select(o => new SalaryLevelViewModel
            {
                SalaryLevelID = o.SalaryLevelID,
                Resolution = o.Resolution,
                ScaleName = o.EmployeeScale.Resolution,
                ScaleID = o.ScaleID,
                SubLevel = o.SubLevel,
                Coefficient = o.Coefficient,
                Description = o.Description,
                SubLevelName = Res.Get("SalaryLevel_Label_LevelList") + o.SubLevel.Value.ToString()
            }).ToList();

            return lst.OrderBy(o=>o.ScaleName).ThenBy(o => o.SubLevel);
        }
        private int[] levels = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        private Dictionary<int, string> GetLevels()
        {
            Dictionary<int, string> Levels = new Dictionary<int, string>();
            foreach (int i in levels)
            {
                Levels.Add(i, i.ToString());
            }
            return Levels;
        }
    }
}





