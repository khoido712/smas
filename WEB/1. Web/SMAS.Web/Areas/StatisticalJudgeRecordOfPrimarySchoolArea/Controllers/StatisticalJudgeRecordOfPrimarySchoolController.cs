﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.StatisticalJudgeRecordOfPrimarySchoolArea.Models;
using SMAS.Web.Utils;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.StatisticalJudgeRecordOfPrimarySchoolArea.Controllers
{
    public class StatisticalJudgeRecordOfPrimarySchoolController : BaseController
    {
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ITrainingTypeBusiness TrainingTypeBusiness;
        private readonly ICapacityStatisticBusiness CapacityStatisticBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IMarkStatisticBusiness MarkStatisticBusiness;

        //
        // GET: /StatisticalJudgeRecordOfPrimarySchoolArea/StatisticalJudgeRecordOfPrimarySchool/
        public StatisticalJudgeRecordOfPrimarySchoolController(IAcademicYearBusiness AcademicYearBusiness
            , ITrainingTypeBusiness TrainingTypeBusiness
            , ICapacityStatisticBusiness CapacityStatisticBusiness
            , IProcessedReportBusiness ProcessedReportBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness
            , IMarkStatisticBusiness MarkStatisticBusiness)
        {
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.TrainingTypeBusiness = TrainingTypeBusiness;
            this.CapacityStatisticBusiness = CapacityStatisticBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.MarkStatisticBusiness = MarkStatisticBusiness;
        }

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        #region set view data

        public void SetViewData()
        {
            GlobalInfo global = new GlobalInfo();

            //[AdditionalMetadata("ViewDataKey", StatisticalJudgeRecordOfPrimarySchoolConstant.LISTACADEMICYEAR)]
            List<int> lsYear = AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value);
            List<ComboObject> lscbYear = new List<ComboObject>();
            foreach (var year in lsYear)
            {
                string value = year.ToString() + "-" + (year + 1).ToString();
                ComboObject cb = new ComboObject(year.ToString(), value);
                lscbYear.Add(cb);
            }
            ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.LISTACADEMICYEAR] = new SelectList(lscbYear, "key", "value");

            // [AdditionalMetadata("ViewDataKey", StatisticalJudgeRecordOfPrimarySchoolConstant.LISTSEMESTER)]
            ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.LISTSEMESTER] = new SelectList(CommonList.Semester(), "key", "value");

            //[AdditionalMetadata("ViewDataKey", StatisticalJudgeRecordOfPrimarySchoolConstant.LISTEDUCATIONLEVEL)]
            List<EducationLevel> lsEducationLevel = global.EducationLevels;

            //-	cboEducationLevel: UserInfo.EducationLevels
            if (lsEducationLevel != null)
            {
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.LISTEDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.LISTEDUCATIONLEVEL] = new SelectList(new string[] { });
            }

            //[AdditionalMetadata("ViewDataKey", StatisticalJudgeRecordOfPrimarySchoolConstant.LISTTRAININGTYPE)]
            IQueryable<TrainingType> lsTT = TrainingTypeBusiness.Search(new Dictionary<string, object>() { });
            if (lsTT.Count() > 0)
            {
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.LISTTRAININGTYPE] = new SelectList(lsTT, "TrainingTypeID", "Resolution");
            }
            else
            {
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.LISTTRAININGTYPE] = new SelectList(new string[] { });
            }

            // [AdditionalMetadata("ViewDataKey", StatisticalJudgeRecordOfPrimarySchoolConstant.LISTSUBJECT)]
            ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.LISTSUBJECT] = new SelectList(new string[] { });
            if (global.IsSuperVisingDeptRole)
            {
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTSO] = true;
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTPHONG] = false;
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTSO] = false;
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTPHONG] = true;
            }
            else
            {
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTSO] = false;
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTPHONG] = false;
            }

            ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.SHOW_EXCEL] = false;
        }

        #endregion set view data

        #region load combo box

        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? Year, int? EducationLevelID, int? SemesterID, int? TrainingTypeID)
        {
            GlobalInfo global = new GlobalInfo();
            int EducationLevel = EducationLevelID.HasValue ? EducationLevelID.Value : 0;
            int TrainingType = TrainingTypeID.HasValue ? TrainingTypeID.Value : 0;
            int? ProvinceID = null;
            int? SupervisingDeptID = null;
            if (global.IsSuperVisingDeptRole)
            {
                ProvinceID = global.ProvinceID;
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                SupervisingDeptID = global.SupervisingDeptID;
            }
            int iYear = Year.HasValue ? Year.Value : 0;
            if (iYear == 0)
            {
                return Json(new SelectList(new string[] { }));
            }

            IDictionary<string, object> newDic = new Dictionary<string, object>()
                {
                {"ReportCode",SystemParamsInFile.THONGKEHOCLUCMONNHANXETCAP1},
                {"Year",iYear},
                {"Semester",SemesterID},
                {"SentToSupervisor",true},
                {"EducationLevelID ",EducationLevel},
                {"SubCommitteeID ",0},
                {"TrainingTypeID",TrainingType},
                {"SupervisingDeptID",SupervisingDeptID},
                {"ProvinceID",global.ProvinceID}
                };

            List<SubjectCatBO> lsSC = CapacityStatisticBusiness.SearchSubjectHasReport(newDic).ToList();
            if (lsSC != null)
            {
                return Json(new SelectList(lsSC.ToList(), "SubjectCatID", "SubjectName"));
            }
            else
            {
                return Json(new SelectList(new string[] { }));
            }
        }

        #endregion load combo box

        #region search

        public JsonResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();

            //true hoặc hàm SupervisingDeptBusiness.GetHierachyLevel(UserInfo.EmployeeID) = EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE (5)
            if (global.IsSuperVisingDeptRole)
            {
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTSO] = true;
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTPHONG] = false;
                CapacityConductReport mrbo = new CapacityConductReport();
                mrbo.DistrictID = 0;
                mrbo.EducationLevelID = frm.EducationLevel.HasValue ? frm.EducationLevel.Value : 0;
                mrbo.ProvinceID = global.ProvinceID.Value;
                mrbo.Semester = frm.Semester.HasValue ? frm.Semester.Value : 0;
                mrbo.SubcommitteeID = 0;
                mrbo.SubjectID = frm.Subject.Value;
                mrbo.TrainingTypeID = frm.TrainingType.HasValue ? frm.TrainingType.Value : 0;
                mrbo.Year = frm.AcademicYear.HasValue ? frm.AcademicYear.Value : 0;
                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(mrbo);
                string ReportCode = "SGD_TH_TongHopHocLucMonNhanXet";

                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    List<CapacityStatisticsBO> lsmsbd = CapacityStatisticBusiness.CreateSGDCapacityJudgeSubjectStatisticsPrimary(new Dictionary<string, object>()
                        {
                            {"Year",mrbo.Year},
                            {"Semester",frm.Semester.HasValue ? frm.Semester.Value : 0},
                            {"EducationLevelID",mrbo.EducationLevelID},
                            {"TrainingTypeID",mrbo.TrainingTypeID},
                            {"SubjectID",mrbo.SubjectID},
                            {"ProvinceID",global.ProvinceID},
                            {"ReportCode",SystemParamsInFile.THONGKEHOCLUCMONNHANXETCAP1},
                            {"SentToSupervisor",true},
                              {"SupervisingDeptID",global.SupervisingDeptID.Value}
                        }, InputParameterHashKey);
                    if (lsmsbd != null)
                    {
                        List<GridViewModel> lsGVM = new List<GridViewModel>();
                        foreach (var item in lsmsbd)
                        {
                            GridViewModel gvm = new GridViewModel();
                            gvm.DistrictID = item.DistrictID.HasValue ? item.DistrictID.Value : 0;
                            gvm.DistrictName = item.DistrictName;
                            gvm.TotalSchool = item.TotalSchool.HasValue ? item.TotalSchool.Value : 0;
                            gvm.TotalPupil = item.TotalPupil.HasValue ? item.TotalPupil.Value : 0;

                            gvm.TotalAPlus = item.TotalExcellent.HasValue ? item.TotalExcellent.Value : 0;
                            gvm.PercentAPlus = string.Format("{0:P2}", item.PercentExcellent.HasValue ? item.PercentExcellent.Value : 0);

                            gvm.TotalA = item.TotalNormal.HasValue ? item.TotalNormal.Value : 0;
                            gvm.PercentA = string.Format("{0:P2}", item.PercentNormal.HasValue ? item.PercentNormal.Value : 0);

                            gvm.TotalB = item.TotalWeak.HasValue ? item.TotalWeak.Value : 0;
                            gvm.PercentB = string.Format("{0:P2}", item.PercentWeak.HasValue ? item.PercentWeak.Value : 0);

                            lsGVM.Add(gvm);
                        }
                        ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.SHOW_EXCEL] = true;
                        ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.LIST_GRIDVIEWMODEL] = lsGVM;
                    }
                    else
                    {
                        ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.SHOW_EXCEL] = false;
                    }
                    return Json(new JsonMessage(RenderPartialViewToString("_List", null), "grid"));
                }
                else
                {
                    string type = JsonReportMessage.OLD;
                    return Json(new JsonReportMessage(entity, type));
                }
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTSO] = false;
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTPHONG] = true;
                CapacityConductReport mrbo = new CapacityConductReport();
                mrbo.EducationLevelID = frm.EducationLevel.HasValue ? frm.EducationLevel.Value : 0;
                mrbo.ProvinceID = global.ProvinceID.Value;
                mrbo.Semester = frm.Semester.HasValue ? frm.Semester.Value : 0;
                mrbo.SubcommitteeID = 0;
                mrbo.SubjectID = frm.Subject.Value;
                mrbo.TrainingTypeID = frm.TrainingType.HasValue ? frm.TrainingType.Value : 0;
                mrbo.Year = frm.AcademicYear.Value;
                mrbo.DistrictID = global.DistrictID.Value;

                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(mrbo);
                string ReportCode = "PGD_TH_TongHopHocLucMonNhanXet";

                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    List<CapacityStatisticsBO> lsmsosbo = CapacityStatisticBusiness.CreatePGDCapacityJudgeSubjectStatisticsPrimary(new Dictionary<string, object>()
                        {
                            {"Year",mrbo.Year},
                            {"Semester",mrbo.Semester},
                            {"EducationLevelID",mrbo.EducationLevelID},
                            {"TrainingTypeID",mrbo.TrainingTypeID},
                            {"SubjectID",mrbo.SubjectID},
                            {"ProvinceID",global.ProvinceID},
                            {"DistrictID",global.DistrictID},
                            {"ReportCode",SystemParamsInFile.THONGKEHOCLUCMONNHANXETCAP1},
                            {"SentToSupervisor",true},
                               {"SupervisingDeptID",global.SupervisingDeptID.Value}
                        }, InputParameterHashKey);
                    if (lsmsosbo != null)
                    {
                        List<GridViewModel> lsGVM = new List<GridViewModel>();
                        foreach (var item in lsmsosbo)
                        {
                            GridViewModel gvm = new GridViewModel();
                            gvm.SchoolID = item.SchoolID.HasValue ? item.SchoolID.Value : 0;
                            gvm.SchoolName = item.SchoolName;
                            gvm.TotalPupil = item.TotalPupil.HasValue ? item.TotalPupil.Value : 0;

                            gvm.TotalAPlus = item.TotalExcellent.HasValue ? item.TotalExcellent.Value : 0;
                            gvm.PercentAPlus = string.Format("{0:P2}", item.PercentExcellent.HasValue ? item.PercentExcellent.Value : 0);

                            gvm.TotalA = item.TotalNormal.HasValue ? item.TotalNormal.Value : 0;
                            gvm.PercentA = string.Format("{0:P2}", item.PercentNormal.HasValue ? item.PercentNormal.Value : 0);

                            gvm.TotalB = item.TotalWeak.HasValue ? item.TotalWeak.Value : 0;
                            gvm.PercentB = string.Format("{0:P2}", item.PercentWeak.HasValue ? item.PercentWeak.Value : 0);

                            lsGVM.Add(gvm);
                        }
                        ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.SHOW_EXCEL] = true;
                        ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.LIST_GRIDVIEWMODEL] = lsGVM;
                    }
                    else
                    {
                        ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.SHOW_EXCEL] = false;
                    }
                    return Json(new JsonMessage(RenderPartialViewToString("_List", null), "grid"));
                }
                else
                {
                    string type = JsonReportMessage.OLD;
                    return Json(new JsonReportMessage(entity, type));
                }
            }
            return Json(new JsonMessage(RenderPartialViewToString("_List", null), "grid"));
        }

        #endregion search

        #region report

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                 {"SupervisingDeptID",GlobalInfo.SupervisingDeptID}
            };
            List<string> listRC = new List<string> {
               "PGD_TH_TongHopHocLucMonNhanXet","SGD_TH_TongHopHocLucMonNhanXet"
            };

            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReport(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();

            //true hoặc hàm SupervisingDeptBusiness.GetHierachyLevel(UserInfo.EmployeeID) = EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE (5)
            if (global.IsSuperVisingDeptRole)
            {
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTSO] = true;
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTPHONG] = false;
                CapacityConductReport mrbo = new CapacityConductReport();
                mrbo.DistrictID = 0;
                mrbo.EducationLevelID = frm.EducationLevel.HasValue ? frm.EducationLevel.Value : 0;
                mrbo.ProvinceID = global.ProvinceID.Value;
                mrbo.Semester = frm.Semester.HasValue ? frm.Semester.Value : 0;
                mrbo.SubcommitteeID = 0;
                mrbo.SubjectID = frm.Subject.Value;
                mrbo.TrainingTypeID = frm.TrainingType.HasValue ? frm.TrainingType.Value : 0;
                mrbo.Year = frm.AcademicYear.HasValue ? frm.AcademicYear.Value : 0;
                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(mrbo);

                //lam nhu voi entity = null
                List<CapacityStatisticsBO> lsmsbd = CapacityStatisticBusiness.CreateSGDCapacityJudgeSubjectStatisticsPrimary(new Dictionary<string, object>()
                        {
                            {"Year",mrbo.Year},
                            {"Semester",frm.Semester.HasValue ? frm.Semester.Value : 0},
                            {"EducationLevelID",mrbo.EducationLevelID},
                            {"TrainingTypeID",mrbo.TrainingTypeID},
                            {"SubjectID",mrbo.SubjectID},
                            {"ProvinceID",global.ProvinceID},
                            {"ReportCode",SystemParamsInFile.THONGKEHOCLUCMONNHANXETCAP1},
                            {"SentToSupervisor",true},
                              {"SupervisingDeptID",global.SupervisingDeptID.Value}
                        }, InputParameterHashKey);
                if (lsmsbd != null)
                {
                    List<GridViewModel> lsGVM = new List<GridViewModel>();
                    foreach (var item in lsmsbd)
                    {
                        GridViewModel gvm = new GridViewModel();
                        gvm.DistrictID = item.DistrictID.HasValue ? item.DistrictID.Value : 0;
                        gvm.DistrictName = item.DistrictName;
                        gvm.TotalSchool = item.TotalSchool.HasValue ? item.TotalSchool.Value : 0;
                        gvm.TotalPupil = item.TotalPupil.HasValue ? item.TotalPupil.Value : 0;

                        gvm.TotalAPlus = item.TotalExcellent.HasValue ? item.TotalExcellent.Value : 0;
                        gvm.PercentAPlus = string.Format("{0:P2}", item.PercentExcellent.HasValue ? item.PercentExcellent.Value : 0);

                        gvm.TotalA = item.TotalNormal.HasValue ? item.TotalNormal.Value : 0;
                        gvm.PercentA = string.Format("{0:P2}", item.PercentNormal.HasValue ? item.PercentNormal.Value : 0);

                        gvm.TotalB = item.TotalWeak.HasValue ? item.TotalWeak.Value : 0;
                        gvm.PercentB = string.Format("{0:P2}", item.PercentWeak.HasValue ? item.PercentWeak.Value : 0);

                        lsGVM.Add(gvm);
                    }
                    ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.SHOW_EXCEL] = true;
                    ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.LIST_GRIDVIEWMODEL] = lsGVM;
                }
                else
                {
                    ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.SHOW_EXCEL] = false;
                }
                return Json(new JsonMessage(RenderPartialViewToString("_List", null), "grid"));
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTSO] = false;
                ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.ACCOUNTPHONG] = true;
                CapacityConductReport mrbo = new CapacityConductReport();
                mrbo.EducationLevelID = frm.EducationLevel.HasValue ? frm.EducationLevel.Value : 0;
                mrbo.ProvinceID = global.ProvinceID.Value;
                mrbo.Semester = frm.Semester.HasValue ? frm.Semester.Value : 0;
                mrbo.SubcommitteeID = 0;
                mrbo.SubjectID = frm.Subject.Value;
                mrbo.TrainingTypeID = frm.TrainingType.HasValue ? frm.TrainingType.Value : 0;
                mrbo.Year = frm.AcademicYear.Value;
                mrbo.DistrictID = global.DistrictID.Value;

                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(mrbo);
                List<CapacityStatisticsBO> lsmsosbo = CapacityStatisticBusiness.CreatePGDCapacitySubjectStatisticsPrimary(new Dictionary<string, object>()
                        {
                            {"Year",mrbo.Year},
                            {"Semester",mrbo.Semester},
                            {"EducationLevelID",mrbo.EducationLevelID},
                            {"TrainingTypeID",mrbo.TrainingTypeID},
                            {"SubjectID",mrbo.SubjectID},
                            {"ProvinceID",global.ProvinceID},
                            {"DistrictID",global.DistrictID},
                            {"ReportCode",SystemParamsInFile.THONGKEHOCLUCMONNHANXETCAP1},
                            {"SentToSupervisor",true},
                               {"SupervisingDeptID",global.SupervisingDeptID.Value}
                        }, InputParameterHashKey);
                if (lsmsosbo != null)
                {
                    List<GridViewModel> lsGVM = new List<GridViewModel>();
                    foreach (var item in lsmsosbo)
                    {
                        GridViewModel gvm = new GridViewModel();
                        gvm.SchoolID = item.SchoolID.HasValue ? item.SchoolID.Value : 0;
                        gvm.SchoolName = item.SchoolName;
                        gvm.TotalPupil = item.TotalPupil.HasValue ? item.TotalPupil.Value : 0;

                        gvm.TotalAPlus = item.TotalExcellent.HasValue ? item.TotalExcellent.Value : 0;
                        gvm.PercentAPlus = string.Format("{0:P2}", item.PercentExcellent.HasValue ? item.PercentExcellent.Value : 0);

                        gvm.TotalA = item.TotalNormal.HasValue ? item.TotalNormal.Value : 0;
                        gvm.PercentA = string.Format("{0:P2}", item.PercentNormal.HasValue ? item.PercentNormal.Value : 0);

                        gvm.TotalB = item.TotalWeak.HasValue ? item.TotalWeak.Value : 0;
                        gvm.PercentB = string.Format("{0:P2}", item.PercentWeak.HasValue ? item.PercentWeak.Value : 0);

                        lsGVM.Add(gvm);
                    }
                    ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.SHOW_EXCEL] = true;
                    ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.LIST_GRIDVIEWMODEL] = lsGVM;
                }
                else
                {
                    ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.SHOW_EXCEL] = false;
                }
                return Json(new JsonMessage(RenderPartialViewToString("_List", null), "grid"));
            }
            ViewData[StatisticalJudgeRecordOfPrimarySchoolConstant.SHOW_EXCEL] = false;
            return Json(new JsonMessage(RenderPartialViewToString("_List", null), "grid"));
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }


        public JsonResult ExportExcel(SearchViewModel frm)
        {
            GlobalInfo global = new GlobalInfo();

            //true hoặc hàm SupervisingDeptBusiness.GetHierachyLevel(UserInfo.EmployeeID) = EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE (5)
            if (global.IsSuperVisingDeptRole)
            {
                CapacityConductReport mrbo = new CapacityConductReport();
                mrbo.DistrictID = 0;
                mrbo.EducationLevelID = frm.EducationLevel.HasValue ? frm.EducationLevel.Value : 0;
                mrbo.ProvinceID = global.ProvinceID.Value;
                mrbo.Semester = frm.Semester.HasValue ? frm.Semester.Value : 0;
                mrbo.SubcommitteeID = 0;
                mrbo.SubjectID = frm.Subject.Value;
                mrbo.TrainingTypeID = frm.TrainingType.HasValue ? frm.TrainingType.Value : 0;
                mrbo.Year = frm.AcademicYear.HasValue ? frm.AcademicYear.Value : 0;
                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(mrbo);
                string ReportCode = "SGD_TH_TongHopHocLucMonNhanXet";

                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                return Json(new JsonMessage(entity.ProcessedReportID.ToString()));
            }
            else if (global.IsSubSuperVisingDeptRole)
            {
                CapacityConductReport mrbo = new CapacityConductReport();
                mrbo.EducationLevelID = frm.EducationLevel.HasValue ? frm.EducationLevel.Value : 0;
                mrbo.ProvinceID = global.ProvinceID.Value;
                mrbo.Semester = frm.Semester.HasValue ? frm.Semester.Value : 0;
                mrbo.SubcommitteeID = 0;
                mrbo.SubjectID = frm.Subject.Value;
                mrbo.TrainingTypeID = frm.TrainingType.HasValue ? frm.TrainingType.Value : 0;
                mrbo.Year = frm.AcademicYear.Value;
                mrbo.DistrictID = global.DistrictID.Value;

                string InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(mrbo);
                string ReportCode = "PGD_TH_TongHopHocLucMonNhanXet";

                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                return Json(new JsonMessage(entity.ProcessedReportID.ToString()));
            }
            throw new BusinessException("Common_Error_InternalError");
        }

        #endregion report
    }
}
