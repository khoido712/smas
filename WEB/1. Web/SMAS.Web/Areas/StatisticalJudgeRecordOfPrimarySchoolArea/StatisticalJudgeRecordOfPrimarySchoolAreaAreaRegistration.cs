﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticalJudgeRecordOfPrimarySchoolArea
{
    public class StatisticalJudgeRecordOfPrimarySchoolAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticalJudgeRecordOfPrimarySchoolArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticalJudgeRecordOfPrimarySchoolArea_default",
                "StatisticalJudgeRecordOfPrimarySchoolArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}