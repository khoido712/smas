﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Areas.LearningOutcomePrimaryReportArea.Models;
using SMAS.Business.Business;
using SMAS.Web.Filter;
using SMAS.Web.Controllers;

namespace SMAS.Web.Areas.LearningOutcomePrimaryReportArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class LearningOutcomePrimaryReportController : BaseController
    {
        //
        // GET: /LearningOutcomePrimaryReportArea/LearningOutcomePrimaryReport/

        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IPupilRewardReportBusiness PupilRewardReportBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;
        private readonly IReportPupilRetestRegistrationBusiness ReportPupilRetestRegistrationBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IReportMarkRecordBusiness ReportMarkRecordBusiness;
        private readonly IReportJudgeRecordBusiness ReportJudgeRecordBusiness;
        private readonly IReportSummedUpRecordBusiness ReportSummedUpRecordBusiness;
        private readonly IReportPupilRankingBusiness ReportPupilRankingBusiness;
        private readonly IReportPupilEmulationBusiness ReportPupilEmulationBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private int selectedSemester;

        public LearningOutcomePrimaryReportController(IClassProfileBusiness classProfileBusiness,
            IEducationLevelBusiness educationLevelBusiness,
            IPeriodDeclarationBusiness periodDeclarationBusiness,
            IPupilRewardReportBusiness pupilRewardReportBusiness,
            IReportDefinitionBusiness reportDefinitionBusiness,
            IProcessedReportBusiness processedReportBusiness,
            IReportPupilRetestRegistrationBusiness reportPupilRetestRegistrationBusiness,
            IClassSubjectBusiness classSubjectBusiness,
            ISchoolSubjectBusiness schoolSubjectBusiness,
            IReportMarkRecordBusiness reportMarkRecordBusiness,
            IReportJudgeRecordBusiness reportJudgeRecordBusiness,
            IReportSummedUpRecordBusiness reportSummedUpRecordBusiness,
            IReportPupilRankingBusiness reportPupilRankingBusiness,
            IReportPupilEmulationBusiness reportPupilEmulationBusiness,
            IAcademicYearBusiness AcademicYearBusiness)
        {
            this.EducationLevelBusiness = educationLevelBusiness;
            this.ClassProfileBusiness = classProfileBusiness;
            this.PeriodDeclarationBusiness = periodDeclarationBusiness;
            this.PupilRewardReportBusiness = pupilRewardReportBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
            this.ReportPupilRetestRegistrationBusiness = reportPupilRetestRegistrationBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.SchoolSubjectBusiness = schoolSubjectBusiness;
            this.ReportMarkRecordBusiness = reportMarkRecordBusiness;
            this.ReportJudgeRecordBusiness = reportJudgeRecordBusiness;
            this.ReportSummedUpRecordBusiness = reportSummedUpRecordBusiness;
            this.ReportPupilRankingBusiness = reportPupilRankingBusiness;
            this.ReportPupilEmulationBusiness = reportPupilEmulationBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult _index()
        {
            GlobalInfo Global = new GlobalInfo();

            //Danh sách khối học
            ViewData[LearningOutcomePrimaryReportConstants.LS_EDUCATION_LEVEL] = Global.EducationLevels;

            //Danh sách môn học
            List<SubjectCatBO> lstSubject = SchoolSubjectBusiness.SearchBySchool(Global.SchoolID.Value, new Dictionary<string, object>() { 
                {"AcademicYearID", Global.AcademicYearID},
                {"AppliedLevel", Global.AppliedLevel},            
            })
                .Select(o => new SubjectCatBO
                {
                    SubjectCatID = o.SubjectID,
                    DisplayName = o.SubjectCat.DisplayName,
                    OrderInSubject = o.SubjectCat.OrderInSubject
                })
                .Distinct()
                .OrderBy(x => x.OrderInSubject)
                .ThenBy(x => x.DisplayName)
                .ToList();
            ViewData[LearningOutcomePrimaryReportConstants.LS_SUBJECT] = lstSubject.ToList();
            //Danh sách học kỳ
            ViewData[LearningOutcomePrimaryReportConstants.LS_SEMESTER] = CommonList.SemesterPrimary();

            var academicYear = this.AcademicYearBusiness.Find(Global.AcademicYearID);

            if (DateTime.Now >= academicYear.FirstSemesterStartDate && DateTime.Now <= academicYear.FirstSemesterEndDate)
            {
                selectedSemester = 1;
            }
            if (DateTime.Now >= academicYear.SecondSemesterStartDate && DateTime.Now <= academicYear.SecondSemesterEndDate)
            {
                selectedSemester = 2;
            }

            ViewData[LearningOutcomePrimaryReportConstants.DF_SEMESTER] = selectedSemester;
            return PartialView();
        }

        //Danh sách khen thưởng

        [ValidateAntiForgeryToken]
        public JsonResult GetPupilRewardReport(LearningOutcomePrimaryViewModel view)
        {
            PupilRewardReportBO bo = new PupilRewardReportBO();
            GlobalInfo GlobalInfo = new GlobalInfo();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            bo.EducationLevelID = view.EducationLevelID;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_CAP1);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PupilRewardReportBusiness.GetPupilRewardReportForPrimary(bo);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = PupilRewardReportBusiness.CreatePupilRewardPrimaryReport(bo);
                processedReport = PupilRewardReportBusiness.InsertPupilRewardPrimaryReport(bo, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }
        [ValidateAntiForgeryToken]
        public JsonResult GetPupilRewardReportTT30(LearningOutcomePrimaryViewModel view)
        {
            PupilRewardReportBO bo = new PupilRewardReportBO();
            GlobalInfo GlobalInfo = new GlobalInfo();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            bo.EducationLevelID = view.EducationLevelID;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_TT30_CAP1);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = PupilRewardReportBusiness.GetPupilRewardReportForPrimaryTT30(bo);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = PupilRewardReportBusiness.CreatePupilRewardPrimaryReportTT30(bo);
                processedReport = PupilRewardReportBusiness.InsertPupilRewardPrimaryReportTT30(bo, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewPupilRewardReport(LearningOutcomePrimaryViewModel view)
        {
            PupilRewardReportBO bo = new PupilRewardReportBO();
            GlobalInfo GlobalInfo = new GlobalInfo();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            bo.EducationLevelID = view.EducationLevelID;
            Stream excel = PupilRewardReportBusiness.CreatePupilRewardPrimaryReport(bo);

            ProcessedReport processedReport = PupilRewardReportBusiness.InsertPupilRewardPrimaryReport(bo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }
        [ValidateAntiForgeryToken]
        public JsonResult GetNewPupilRewardReportTT30(LearningOutcomePrimaryViewModel view)
        {
            PupilRewardReportBO bo = new PupilRewardReportBO();
            GlobalInfo GlobalInfo = new GlobalInfo();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            bo.EducationLevelID = view.EducationLevelID;
            Stream excel = PupilRewardReportBusiness.CreatePupilRewardPrimaryReportTT30(bo);

            ProcessedReport processedReport = PupilRewardReportBusiness.InsertPupilRewardPrimaryReportTT30(bo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        //Danh sách học sinh thi lại

        [ValidateAntiForgeryToken]
        public JsonResult GetReportPupilRetest(LearningOutcomePrimaryViewModel view)
        {
            ReportPupilRetestRegistrationSearchBO bo = new ReportPupilRetestRegistrationSearchBO();
            GlobalInfo GlobalInfo = new GlobalInfo();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_HS_THI_LAI_CAP1);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ReportPupilRetestRegistrationBusiness.GetReportPupilRetestPrimary(bo);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportPupilRetestRegistrationBusiness.CreateReportPupilRetestPrimary(bo);
                processedReport = ReportPupilRetestRegistrationBusiness.InsertReportPupilRetestPrimary(bo, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportPupilRetest(LearningOutcomePrimaryViewModel view)
        {
            ReportPupilRetestRegistrationSearchBO bo = new ReportPupilRetestRegistrationSearchBO();
            GlobalInfo GlobalInfo = new GlobalInfo();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            Stream excel = ReportPupilRetestRegistrationBusiness.CreateReportPupilRetestPrimary(bo);
            ProcessedReport processedReport = ReportPupilRetestRegistrationBusiness.InsertReportPupilRetestPrimary(bo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        //Danh sách học sinh đăng ký thi lại

        [ValidateAntiForgeryToken]
        public JsonResult GetReportPupilRetestRegistration(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.Value,
                new Dictionary<string, object>(){
                    {"AcademicYearID",GlobalInfo.AcademicYearID},
                    {"SubjectID",view.SubjectID},
                }).FirstOrDefault();
            if (schoolSubject == null)
            {
                throw new BusinessException("SchoolSubject_Label_AcademicYearID_Err");
            }
            ReportPupilRetestRegistrationSearchBO bo = new ReportPupilRetestRegistrationSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            bo.SubjectID = view.SubjectID;
            bo.IsCommenting = schoolSubject.IsCommenting.GetValueOrDefault();
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_HS_DANG_KY_THI_LAI_CAP1);
            string type = JsonReportMessage.NEW;
            ProcessedReport processedReport = null;
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ReportPupilRetestRegistrationBusiness.GetReportPupilRetestRegistrationPrimary(bo);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportPupilRetestRegistrationBusiness.CreateReportPupilRetestRegistrationPrimary(bo);
                processedReport = ReportPupilRetestRegistrationBusiness.InsertReportPupilRetestRegistrationPrimary(bo, excel);
                excel.Close();

            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportPupilRetestRegistration(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.Value,
                new Dictionary<string, object>(){
                    {"AcademicYearID",GlobalInfo.AcademicYearID},
                    {"SubjectID",view.SubjectID},
                }).FirstOrDefault();
            if (schoolSubject == null)
            {
                throw new BusinessException("SchoolSubject_Label_AcademicYearID_Err");
            }
            ReportPupilRetestRegistrationSearchBO bo = new ReportPupilRetestRegistrationSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            bo.IsCommenting = schoolSubject.IsCommenting.GetValueOrDefault();
            bo.SubjectID = view.SubjectID;
            Stream excel = ReportPupilRetestRegistrationBusiness.CreateReportPupilRetestRegistrationPrimary(bo);
            ProcessedReport processedReport = ReportPupilRetestRegistrationBusiness.InsertReportPupilRetestRegistrationPrimary(bo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        //Thống kê điểm thi học kỳ theo môn

        [ValidateAntiForgeryToken]
        public JsonResult GetReportMarkSemester(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.Value,
                new Dictionary<string, object>(){
                    {"AcademicYearID",GlobalInfo.AcademicYearID},
                    {"SubjectID",view.SubjectID},
                }).FirstOrDefault();
            if (schoolSubject == null)
            {
                throw new BusinessException("SchoolSubject_Label_AcademicYearID_Err");
            }
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            if (schoolSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
            {
                ReportMarkRecordSearchBO bo = new ReportMarkRecordSearchBO();
                bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
                bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                bo.Semester = view.Semester;
                bo.SubjectID = schoolSubject.SubjectID;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_TINH_DIEM_CAP1);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = ReportMarkRecordBusiness.GetReportMarkRecordPrimary(bo);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ReportMarkRecordBusiness.CreateReportMarkRecordPrimary(bo);
                    processedReport = ReportMarkRecordBusiness.InsertReportMarkRecordPrimary(bo, excel);
                    excel.Close();

                }
            }
            else
            {
                //ReportJudgeRecordSearchBO bo = new ReportJudgeRecordSearchBO();
                //bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
                //bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                //bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                //bo.Semester = view.Semester;
                //bo.SubjectID = schoolSubject.SubjectID;
                //ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_NHAN_XET_CAP1);
                //if (reportDef.IsPreprocessed == true)
                //{
                //    processedReport = ReportJudgeRecordBusiness.GetReportJudgeRecordPrimary(bo);
                //    if (processedReport != null)
                //    {
                //        type = JsonReportMessage.OLD;
                //    }
                //}
                //if (type == JsonReportMessage.NEW)
                //{
                //    Stream excel = ReportJudgeRecordBusiness.CreateReportJudgeRecordPrimary(bo);
                //    processedReport = ReportJudgeRecordBusiness.InsertReportJudgeRecordPrimary(bo, excel);
                //    excel.Close();

                //}
                throw new BusinessException("Subject_Label_Err");
            }
            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportMarkSemester(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.Value,
                new Dictionary<string, object>(){
                    {"AcademicYearID",GlobalInfo.AcademicYearID},
                    {"SubjectID",view.SubjectID},
                }).FirstOrDefault();
            if (schoolSubject == null)
            {
                throw new BusinessException("SchoolSubject_Label_AcademicYearID_Err");
            }
            ProcessedReport processedReport = null;
            if (schoolSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
            {
                ReportMarkRecordSearchBO bo = new ReportMarkRecordSearchBO();
                bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
                bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                bo.Semester = view.Semester;
                bo.SubjectID = schoolSubject.SubjectID;
                Stream excel = ReportMarkRecordBusiness.CreateReportMarkRecordPrimary(bo);
                processedReport = ReportMarkRecordBusiness.InsertReportMarkRecordPrimary(bo, excel);
                excel.Close();
            }
            else
            {
                throw new BusinessException("Subject_Label_Err");
            }
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetReportMarkSemesterTT30(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            AcademicYear academicYearObj = AcademicYearBusiness.Find(GlobalInfo.AcademicYearID.Value);
            bool isMoveHistory = UtilsBusiness.IsMoveHistory(academicYearObj);
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.Value,
                new Dictionary<string, object>(){
                    {"AcademicYearID",GlobalInfo.AcademicYearID},
                    {"SubjectID",view.SubjectID},
                }).FirstOrDefault();
            if (schoolSubject == null)
            {
                throw new BusinessException("SchoolSubject_Label_AcademicYearID_Err");
            }
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;
            if (schoolSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
            {
                ReportMarkRecordSearchBO bo = new ReportMarkRecordSearchBO();
                bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
                bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                bo.Semester = view.Semester;
                bo.SubjectID = schoolSubject.SubjectID;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_TINH_DIEM_CAP1);
                if (reportDef.IsPreprocessed == true)
                {
                    processedReport = ReportMarkRecordBusiness.GetReportMarkRecordPrimary(bo);
                    if (processedReport != null)
                    {
                        type = JsonReportMessage.OLD;
                    }
                }
                if (type == JsonReportMessage.NEW)
                {
                    Stream excel = ReportMarkRecordBusiness.CreateReportMarkRecordPrimaryTT30(bo, isMoveHistory);
                    processedReport = ReportMarkRecordBusiness.InsertReportMarkRecordPrimary(bo, excel);
                    excel.Close();
                }
            }
            else
            {
                //ReportJudgeRecordSearchBO bo = new ReportJudgeRecordSearchBO();
                //bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
                //bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                //bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                //bo.Semester = view.Semester;
                //bo.SubjectID = schoolSubject.SubjectID;
                //ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_NHAN_XET_CAP1);
                //if (reportDef.IsPreprocessed == true)
                //{
                //    processedReport = ReportJudgeRecordBusiness.GetReportJudgeRecordPrimary(bo);
                //    if (processedReport != null)
                //    {
                //        type = JsonReportMessage.OLD;
                //    }
                //}
                //if (type == JsonReportMessage.NEW)
                //{
                //    Stream excel = ReportJudgeRecordBusiness.CreateReportJudgeRecordPrimary(bo);
                //    processedReport = ReportJudgeRecordBusiness.InsertReportJudgeRecordPrimary(bo, excel);
                //    excel.Close();

                //}
                throw new BusinessException("Subject_Label_Err");
            }
            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportMarkSemesterTT30(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            AcademicYear academicYearObj = AcademicYearBusiness.Find(GlobalInfo.AcademicYearID.Value);
            bool isMoveHistory = UtilsBusiness.IsMoveHistory(academicYearObj);
            SchoolSubject schoolSubject = SchoolSubjectBusiness.SearchBySchool(GlobalInfo.SchoolID.Value,
                new Dictionary<string, object>(){
                    {"AcademicYearID",GlobalInfo.AcademicYearID},
                    {"SubjectID",view.SubjectID},
                }).FirstOrDefault();
            if (schoolSubject == null)
            {
                throw new BusinessException("SchoolSubject_Label_AcademicYearID_Err");
            }
            ProcessedReport processedReport = null;
            if (schoolSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
            {
                ReportMarkRecordSearchBO bo = new ReportMarkRecordSearchBO();
                bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
                bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
                bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                bo.Semester = view.Semester;
                bo.SubjectID = schoolSubject.SubjectID;
                Stream excel = ReportMarkRecordBusiness.CreateReportMarkRecordPrimaryTT30(bo, isMoveHistory);
                processedReport = ReportMarkRecordBusiness.InsertReportMarkRecordPrimary(bo, excel);
                excel.Close();
            }
            else
            {
                throw new BusinessException("Subject_Label_Err");
            }
            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        //Thống kê xếp loại chi tiết theo môn

        [ValidateAntiForgeryToken]
        public JsonResult GetReportMarkSummedUpGroupByClass(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;

            ReportSummedUpRecordSearchBO bo = new ReportSummedUpRecordSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            bo.EducationLevelID = view.EducationLevelID;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_XEP_LOAI_CHI_TIET_CAP1);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ReportSummedUpRecordBusiness.GetReportSummedUpRecordGroupByClassPrimary(bo);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportSummedUpRecordBusiness.CreateReportSummedUpRecordGroupByClassPrimary(bo);
                processedReport = ReportSummedUpRecordBusiness.InsertReportSummedUpRecordGroupByClassPrimary(bo, excel);
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportMarkSummedUpGroupByClass(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            ReportSummedUpRecordSearchBO bo = new ReportSummedUpRecordSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            bo.EducationLevelID = view.EducationLevelID;
            Stream excel = ReportSummedUpRecordBusiness.CreateReportSummedUpRecordGroupByClassPrimary(bo);
            processedReport = ReportSummedUpRecordBusiness.InsertReportSummedUpRecordGroupByClassPrimary(bo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        //Thống kê xếp loại giáo dục

        [ValidateAntiForgeryToken]
        public JsonResult GetReportCapacityLevel(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;

            ReportPupilRankingSearchBO bo = new ReportPupilRankingSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_GIAO_DUC_CAP1);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ReportPupilRankingBusiness.GetReportCapacityPrimary(bo);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportPupilRankingBusiness.CreateReportCapacityPrimary(bo);
                processedReport = ReportPupilRankingBusiness.InsertReportCapacityPrimary(bo, excel);
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportCapacityLevel(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            ReportPupilRankingSearchBO bo = new ReportPupilRankingSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            Stream excel = ReportPupilRankingBusiness.CreateReportCapacityPrimary(bo);
            processedReport = ReportPupilRankingBusiness.InsertReportCapacityPrimary(bo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        //thống kê xếp loại hạnh kiểm

        [ValidateAntiForgeryToken]
        public JsonResult GetReportConductLevel(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;

            ReportPupilRankingSearchBO bo = new ReportPupilRankingSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HANH_KIEM_CAP1);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ReportPupilRankingBusiness.GetReportConductPrimary(bo);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportPupilRankingBusiness.CreateReportConductPrimary(bo);
                processedReport = ReportPupilRankingBusiness.InsertReportConductPrimary(bo, excel);
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportConductLevel(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            ReportPupilRankingSearchBO bo = new ReportPupilRankingSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            Stream excel = ReportPupilRankingBusiness.CreateReportConductPrimary(bo);
            processedReport = ReportPupilRankingBusiness.InsertReportConductPrimary(bo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        //Thống kê xếp loại học lực môn 

        [ValidateAntiForgeryToken]
        public JsonResult GetReportMarkSummedUpGroupBySubject(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;

            ReportSummedUpRecordSearchBO bo = new ReportSummedUpRecordSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HOC_LUC_MON_CAP1);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ReportSummedUpRecordBusiness.GetReportSummedUpRecordGroupBySubjectPrimary(bo);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportSummedUpRecordBusiness.CreateReportSummedUpRecordGroupBySubjectPrimary(bo);
                processedReport = ReportSummedUpRecordBusiness.InsertReportSummedUpRecordGroupBySubjectPrimary(bo, excel);
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportMarkSummedUpGroupBySubject(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            ReportSummedUpRecordSearchBO bo = new ReportSummedUpRecordSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            Stream excel = ReportSummedUpRecordBusiness.CreateReportSummedUpRecordGroupBySubjectPrimary(bo);
            processedReport = ReportSummedUpRecordBusiness.InsertReportSummedUpRecordGroupBySubjectPrimary(bo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        // Thống kê tổng hợp các môn theo khối 

        [ValidateAntiForgeryToken]
        public JsonResult GetReportMarkSummedUpGroupByEducationLevel(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;

            ReportSummedUpRecordSearchBO bo = new ReportSummedUpRecordSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HOC_LUC_MON_THEO_KHOI_CAP1);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ReportSummedUpRecordBusiness.GetReportSummedUpRecordGroupByEducationLevelPrimary(bo);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportSummedUpRecordBusiness.CreateReportSummedUpRecordGroupByEducationLevelPrimary(bo);
                processedReport = ReportSummedUpRecordBusiness.InsertReportSummedUpRecordGroupByEducationLevelPrimary(bo, excel);
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportMarkSummedUpGroupByEducationLevel(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            ReportSummedUpRecordSearchBO bo = new ReportSummedUpRecordSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            Stream excel = ReportSummedUpRecordBusiness.CreateReportSummedUpRecordGroupByEducationLevelPrimary(bo);
            processedReport = ReportSummedUpRecordBusiness.InsertReportSummedUpRecordGroupByEducationLevelPrimary(bo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        // Thống kê danh hiệu thi đua 

        [ValidateAntiForgeryToken]
        public JsonResult GetReportPupilEmulation(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;

            ReportPupilEmulationSearchBO bo = new ReportPupilEmulationSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_THONG_KE_XEP_DANH_HIEU_THI_DUA_CAP1);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ReportPupilEmulationBusiness.GetReportPupilEmulationPrimary(bo);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportPupilEmulationBusiness.CreateReportPupilEmulationPrimary(bo);
                processedReport = ReportPupilEmulationBusiness.InsertReportPupilEmulationPrimary(bo, excel);
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, type));
        }


        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportPupilEmulation(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            ReportPupilEmulationSearchBO bo = new ReportPupilEmulationSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            Stream excel = ReportPupilEmulationBusiness.CreateReportPupilEmulationPrimary(bo);
            processedReport = ReportPupilEmulationBusiness.InsertReportPupilEmulationPrimary(bo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        // Thống kê kết quả cuối năm 
        [ValidateAntiForgeryToken]
        public JsonResult GetReportFinalResult(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;

            ReportPupilRankingSearchBO bo = new ReportPupilRankingSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ReportPupilRankingBusiness.GetReportFinalResultPrimary(bo);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportPupilRankingBusiness.CreateReportFinalResultPrimary(bo);
                processedReport = ReportPupilRankingBusiness.InsertReportFinalResultPrimary(bo, excel);
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportFinalResult(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            ReportPupilRankingSearchBO bo = new ReportPupilRankingSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            Stream excel = ReportPupilRankingBusiness.CreateReportFinalResultPrimary(bo);
            processedReport = ReportPupilRankingBusiness.InsertReportFinalResultPrimary(bo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        //Thống kê kết quả cuối năm Thông tư 30
        [ValidateAntiForgeryToken]
        public JsonResult GetReportFinalResultTT30(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            string type = JsonReportMessage.NEW;

            ReportPupilRankingSearchBO bo = new ReportPupilRankingSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1_TT30);
            if (reportDef.IsPreprocessed == true)
            {
                processedReport = ReportPupilRankingBusiness.GetReportFinalResultPrimaryTT30(bo);
                if (processedReport != null)
                {
                    type = JsonReportMessage.OLD;
                }
            }
            if (type == JsonReportMessage.NEW)
            {
                Stream excel = ReportPupilRankingBusiness.CreateReportFinalResultPrimaryTT30(bo);
                processedReport = ReportPupilRankingBusiness.InsertReportFinalResultPrimaryTT30(bo, excel);
                excel.Close();

            }

            return Json(new JsonReportMessage(processedReport, type));
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetNewReportFinalResultTT30(LearningOutcomePrimaryViewModel view)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            ProcessedReport processedReport = null;
            ReportPupilRankingSearchBO bo = new ReportPupilRankingSearchBO();
            bo.AcademicYearID = GlobalInfo.AcademicYearID.GetValueOrDefault();
            bo.SchoolID = GlobalInfo.SchoolID.GetValueOrDefault();
            bo.AppliedLevel = SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
            bo.Semester = view.Semester;
            Stream excel = ReportPupilRankingBusiness.CreateReportFinalResultPrimaryTT30(bo);
            processedReport = ReportPupilRankingBusiness.InsertReportFinalResultPrimaryTT30(bo, excel);
            excel.Close();

            return Json(new JsonReportMessage(processedReport, JsonReportMessage.NEW));
        }

        public FileResult DownloadReport(int idProcessedReport)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", GlobalInfo.SchoolID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_CAP1,
                SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_TT30_CAP1,
                SystemParamsInFile.REPORT_DS_HS_DANG_KY_THI_LAI_CAP1,
                SystemParamsInFile.REPORT_DS_HS_THI_LAI_CAP1,
                SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_NHAN_XET_CAP1,
                SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_TINH_DIEM_CAP1,
                SystemParamsInFile.REPORT_DS_XEP_LOAI_CHI_TIET_CAP1,
                SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_GIAO_DUC_CAP1,
                SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HANH_KIEM_CAP1,
                SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HOC_LUC_MON_CAP1,
                SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HOC_LUC_MON_THEO_KHOI_CAP1,
                SystemParamsInFile.REPORT_DS_THONG_KE_XEP_DANH_HIEU_THI_DUA_CAP1,
                SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1,
                SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1_TT30,
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(idProcessedReport, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }
    }
}
