﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LearningOutcomePrimaryReportArea.Models
{
    public class LearningOutcomePrimaryViewModel
    {
        public int EducationLevelID { get; set; }
        public int Semester { get; set; }
        public int SubjectID { get; set; }        
    }
}