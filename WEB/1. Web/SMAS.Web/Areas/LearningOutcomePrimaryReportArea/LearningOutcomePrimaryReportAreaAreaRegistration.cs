﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.LearningOutcomePrimaryReportArea
{
    public class LearningOutcomePrimaryReportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "LearningOutcomePrimaryReportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LearningOutcomePrimaryReportArea_default",
                "LearningOutcomePrimaryReportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
