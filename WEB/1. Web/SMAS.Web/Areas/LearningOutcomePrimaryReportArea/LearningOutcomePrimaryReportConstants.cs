﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.LearningOutcomePrimaryReportArea
{
    public class LearningOutcomePrimaryReportConstants
    {
        public const string LS_EDUCATION_LEVEL = "listEducationLevel";
        public const string LS_SUBJECT = "listSubject";
        public const string LS_SEMESTER = "listSemester";
        public const string DF_SEMESTER = "defaultSemester";
        public const string LS_IS_COMMENTING = "listIsCommenting";
        public const string DF_IS_COMMENTING = "defaultIsCommenting";
    }
}