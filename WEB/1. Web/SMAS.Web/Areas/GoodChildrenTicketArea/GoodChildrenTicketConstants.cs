/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.GoodChildrenTicketArea
{
    public class GoodChildrenTicketConstants
    {
        public const string LIST_GOODCHILDRENTICKET = "listGoodChildrenTicket";
        public const string LST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LST_MONTH = "listMonth";
        public const string LS_CBOLDCLASS = "listOldClass";
        public const string ENABLE_SAVE = "ENABLE_SAVE";

        public const string LIST_EDUCATIONLEVEL = "LIST_EDUCATIONLEVEL";
        public const string LIST_CLASS = "LIST_CLASS";
        public const string NUMBER_WEEK_OF_MONTH = "NUMBER_WEEK_OF_MONTH";
        public const string LIST_RESULT_GOODCHILDREN = "LIST_RESULT_GOODCHILDREN";
        public const string LIST_RESULT_COMMENT = "LIST_RESULT_COMMENT";
        public const string LAST_UPDATE_STRING = "LAST_UPDATE_STRING";
        public const string LIST_WEEK_OF_MONTH = "LIST_WEEK_OF_MONTH";
    }
}