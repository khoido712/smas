/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.GoodChildrenTicketArea.Models
{
    public class GoodChildrenTicketViewModel
    {
		public System.Int32 GoodChildrenTicketID { get; set; }								
		public System.Boolean IsGood { get; set; }								
		public System.Int32 WeekInMonth { get; set; }								
		public System.DateTime Month { get; set; }								
		public System.String Note { get; set; }								
		public System.Int32 Year { get; set; }								
		public System.Int32 PupilID { get; set; }								
		public System.Int32 ClassID { get; set; }								
		public System.Int32 SchoolID { get; set; }								
		public System.Int32 AcademicYearID { get; set; }
        [ResourceDisplayName("GoodChildrenTicket_Label_PupilCode")]
        public string PupilCode { get; set; }
        [ResourceDisplayName("GoodChildrenTicket_Label_PupilName")]
        public string PupilName { get; set; }
        public string[] selected { get; set; }
        public string[] notes { get; set; }

        public int Status { get; set; }
    }

    public class CommentWeek
    {
        public int PupilID { get; set; }
        public int ValuaWeek { get; set; }
        public string Comment { get; set; }
        public int ValGooodChildren { get; set; }
    }
}


