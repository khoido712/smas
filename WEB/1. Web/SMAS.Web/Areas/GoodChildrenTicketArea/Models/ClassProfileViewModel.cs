﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.GoodChildrenTicketArea.Models
{
    public class ClassProfileViewModel
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int EducationLevelID { get; set; }
        public bool Selected { get; set; }
    }
}