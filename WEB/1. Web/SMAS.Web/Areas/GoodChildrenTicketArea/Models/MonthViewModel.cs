﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.GoodChildrenTicketArea.Models
{
    public class MonthViewModel
    {
        public string Display { get; set; }
        public string MonthID { get; set; }
        public bool Selected { get; set; }
    }

    public class NumberWeek
    {
        public int NumberWeekID { get; set; }
        public string Display { get; set; }
    }
}