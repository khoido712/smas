﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.GoodChildrenTicketArea
{
    public class GoodChildrenTicketAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "GoodChildrenTicketArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "GoodChildrenTicketArea_default",
                "GoodChildrenTicketArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
