﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.GoodChildrenTicketArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.HtmlHelpers;

namespace SMAS.Web.Areas.GoodChildrenTicketArea.Controllers
{
    public class GoodChildrenTicketController : BaseController
    {
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IGoodChildrenTicketBusiness GoodChildrenTicketBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private int WEEK_OF_MONTH = 5;
        private GlobalInfo gl = new GlobalInfo();

        public GoodChildrenTicketController(IClassProfileBusiness cp, 
            IAcademicYearBusiness ab, 
            IPupilOfClassBusiness PupilOfClassBusiness, 
            IGoodChildrenTicketBusiness goodchildrenticketBusiness,
            IEmployeeBusiness EmployeeBusiness)
        {
            this.GoodChildrenTicketBusiness = goodchildrenticketBusiness;
            this.ClassProfileBusiness = cp;
            this.AcademicYearBusiness = ab;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
        }

        //incomp: set default date
        public ActionResult Index(int? ClassID)
        {
            Session["INDEX"] = 1;
            this.SetViewData(ClassID);
            return View();
        }

        #region SetViewData
        private void SetViewData(int? ClassID)
        {
            bool isHeadTeacher = false;
            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();
            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels.ToList())
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }

            ViewData[GoodChildrenTicketConstants.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            ClassProfile cp = new ClassProfile();
            dic = new Dictionary<string, object>();
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    isHeadTeacher = true;
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_REPOSIBILITY;
                }

                List<ClassProfile> listClass = ClassProfileBusiness.SearchTeacherTeachingBySchool(_globalInfo.SchoolID.Value, dic)
                                                                            .OrderBy(x => x.EducationLevelID)
                                                                            .ThenBy(x => x.DisplayName).ToList();
              
                var listCP = listClass.ToList();

                if (listCP != null && listCP.Count() > 0 && isHeadTeacher)
                {
                    //Tính ra lớp cần được chọn đầu tiên
                    cp = listCP.First();
                    if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                    {
                        cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                    }
                }

                int ExpandEducationLevel = 0;

                List<ClassProfileViewModel> lstClassVM = new List<ClassProfileViewModel>();
                for (int j = 0; j < listClass.Count(); j++)
                {
                    bool selected = isHeadTeacher ? (listClass[j].ClassProfileID == cp.ClassProfileID ? true: false) : false;
                    if (selected)
                    {
                        ExpandEducationLevel = listClass[j].EducationLevelID;

                    }

                    lstClassVM.Add(new ClassProfileViewModel() {
                        ClassID = listClass[j].ClassProfileID,
                        ClassName = listClass[j].DisplayName,
                        EducationLevelID = listClass[j].EducationLevelID,
                        Selected = selected
                    });
                }
                ViewData["ClassIDSelected"] = isHeadTeacher ? cp.ClassProfileID : 0;
                ViewData["ExpandEducationLevel"] = ExpandEducationLevel;
                ViewData[GoodChildrenTicketConstants.LIST_CLASS] = lstClassVM;
            }

            // Danh sach thang
            //this.LoadMonth();
            // Danh sach so tuan trong thang
            List<ComboObject> lstNumberWeek = new List<ComboObject>();
            for (int j = 1; j <= WEEK_OF_MONTH; j++)
            {
                lstNumberWeek.Add(new ComboObject(j + " Tuần", j.ToString()));
            }
            ViewData[GoodChildrenTicketConstants.NUMBER_WEEK_OF_MONTH] = lstNumberWeek;
        }
        #endregion

        #region // Load Grid
        public PartialViewResult AjaxLoadGrid(int ClassID, string MonthID, int chooseWeekSelected, int numberWeek = 4)
        {
            List<string> monthSelected = new List<string>();
            if (!string.IsNullOrEmpty(MonthID))
            {
                monthSelected = MonthID.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
                if(monthSelected.Count == 0 || monthSelected.Count != 2)
                {
                    throw new BusinessException("Thầy/cô chọn tháng không hợp lệ!");
                }
            }

            if(ClassID == 0)
            {
                throw new BusinessException("Lớp học không tồn tại! Thầy cô vui lòng kiểm tra lại.");
            }

            if (numberWeek > 5 || numberWeek <= 0)
            {
                throw new BusinessException("Số tuần của tháng không hợp lệ!");
            }

            // danh sach cac danh gia be ngoan
            DateTime monthDate = Convert.ToDateTime(MonthID);
            ClassProfile objClass = ClassProfileBusiness.Find(ClassID);
            AcademicYear academicYear = AcademicYearBusiness.Find(gl.AcademicYearID);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = gl.AcademicYearID;
            SearchInfo["ClassID"] = ClassID;
            SearchInfo["Month"] = monthDate;
            SearchInfo["AppliedLevel"] = gl.AppliedLevel;
            SearchInfo["EducationLevelID"] = objClass.EducationLevelID;

            int _numberDefault = numberWeek;
            ViewData[GoodChildrenTicketConstants.NUMBER_WEEK_OF_MONTH] = numberWeek;

            List<GoodChildrenTicket> lstGoodChildren = GoodChildrenTicketBusiness.SearchBySchool(gl.SchoolID.Value, SearchInfo).ToList();
            var checkObjGoodChildren = lstGoodChildren.Where(x => x.WeekInMonth == 5 && (x.IsGood == true || !string.IsNullOrEmpty(x.Note))).FirstOrDefault();
            if (checkObjGoodChildren != null)
            {              
                numberWeek = 5;
            }

            if (chooseWeekSelected == 1) // Chọn tuần
            {
                ViewData[GoodChildrenTicketConstants.NUMBER_WEEK_OF_MONTH] = _numberDefault;
            }
            else
            {
                if (numberWeek > _numberDefault)
                {
                    ViewData[GoodChildrenTicketConstants.NUMBER_WEEK_OF_MONTH] = numberWeek;
                }
            }

            List<int> lstWeekOfMonth = new List<int>();
            for (int i = 1; i <= numberWeek; i++)
            {
                lstWeekOfMonth.Add(i);
            }
            lstWeekOfMonth.Add(6);
            lstGoodChildren = lstGoodChildren.Where(x => lstWeekOfMonth.Contains(x.WeekInMonth)).ToList();
            List<GoodChildrenTicketViewModel> lstResult = this.GetListGoodChildren(ClassID, MonthID, numberWeek, lstGoodChildren, objClass);

            #region nguoi cap nhat sau cung
            List<int> lstEmployeeID = new List<int>();
            GoodChildrenTicket lastUpdateBO = new GoodChildrenTicket();
            Employee lastUser = null;
            string tmpStr = String.Empty;
            string strLastUpdate = String.Empty;

            List<Employee> lstEmployeeChange = new List<Employee>();        
            lastUpdateBO = lstGoodChildren.OrderByDescending(x => x.ModifiedDate).FirstOrDefault();
            if (lastUpdateBO != null)
            {
                string lastUserName = String.Empty;

                if (lastUpdateBO.LogID == 0 || !lastUpdateBO.LogID.HasValue)
                {
                    lastUserName = "Quản trị trường";
                }
                else
                {
                    lstEmployeeID = lstGoodChildren.Where(p => p.LogID.HasValue && p.LogID > 0).Select(p => p.LogID ?? 0).Distinct().ToList();
                    if (lstEmployeeID.Count > 0)
                    {
                        lstEmployeeChange = EmployeeBusiness.All.Where(x => x.SchoolID == _globalInfo.SchoolID && lstEmployeeID.Contains(x.EmployeeID)).ToList();
                    }

                    lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdateBO.LogID).FirstOrDefault();
                    if (lastUser != null)
                    {
                        lastUserName = lastUser.FullName;
                    }
                }

                if (lastUpdateBO.ModifiedDate.HasValue)
                {
                    tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateBO.ModifiedDate.Value.Hour.ToString(),
                             lastUpdateBO.ModifiedDate.Value.Minute.ToString("D2"),
                             lastUpdateBO.ModifiedDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                }
                else
                {
                    if (lastUpdateBO.CreateDate.HasValue)
                    {
                        tmpStr = string.Format("<span style='color:#d56900 '>{0}:{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                             lastUpdateBO.CreateDate.Value.Hour.ToString(),
                             lastUpdateBO.CreateDate.Value.Minute.ToString("D2"),
                             lastUpdateBO.CreateDate,
                             !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                             lastUserName});
                    }
                   
                }
                strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);
            }
            #endregion

            ViewData[GoodChildrenTicketConstants.LAST_UPDATE_STRING] = strLastUpdate;
            ViewData[GoodChildrenTicketConstants.LIST_RESULT_GOODCHILDREN] = lstResult;
           
            return PartialView("_GridGoodChildren");
        }       
        private List<GoodChildrenTicketViewModel> GetListGoodChildren(int classID, string MonthID, int numberWeek,
            List<GoodChildrenTicket> lstGoodChildren, ClassProfile objClass)
        { 
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = gl.AcademicYearID;
            dic["ClassID"] = classID;
            dic["EducationLevelID"] = objClass.EducationLevelID;
            dic["AppliedLevel"] = gl.AppliedLevel;
            dic["Check"] = "check";
            List<PupilOfClassBO> lstPupil = PupilOfClassBusiness.SearchBySchool(gl.SchoolID.Value, dic)
                .Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).Select(o => new PupilOfClassBO
            {
                PupilID = o.PupilID,
                ClassID = o.ClassID,
                PupilFullName = o.PupilProfile.FullName,
                Name = o.PupilProfile.Name,
                OrderInClass = o.OrderInClass,
                AcademicYearID = o.AcademicYearID,
                SchoolID = o.SchoolID,
                PupilCode = o.PupilProfile.PupilCode,
                Status = o.Status
            }).ToList();

            lstPupil = lstPupil.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ToList();       

            GoodChildrenTicketViewModel g = new GoodChildrenTicketViewModel();
            List<GoodChildrenTicketViewModel> list = new List<GoodChildrenTicketViewModel>();
            List<CommentWeek> lstCommentWeek = new List<CommentWeek>();          

            for (int i = 0; i < lstPupil.Count; i++)
            {
                g = new GoodChildrenTicketViewModel();
                g.PupilID = lstPupil[i].PupilID;
                g.PupilName = lstPupil[i].PupilFullName;
                //g.AcademicYearID = lstPupil[i].AcademicYearID;
                //g.ClassID = lstPupil[i].ClassID;
                //g.PupilCode = lstPupil[i].PupilCode;
                g.Status = lstPupil[i].Status;         
                CommentWeek objComment = null;
                int index = 0;
                // Add tuan
                for (int j = 1; j <= numberWeek; j++)
                {
                    objComment = new CommentWeek();
                    objComment.PupilID = lstPupil[i].PupilID;
                    objComment.ValuaWeek = j; // Tuần
                    objComment.Comment = "";
                    objComment.ValGooodChildren = 0;

                    GoodChildrenTicket gct = lstGoodChildren.Where(o => o.PupilID == lstPupil[i].PupilID && 
                                                                        o.ClassID == lstPupil[i].ClassID && 
                                                                        o.WeekInMonth == j).FirstOrDefault();
                    if (gct != null)
                    {                       
                        objComment.Comment = gct.Note ;
                        objComment.ValGooodChildren = gct.IsGood ? 1 : 0;

                       /* if(gct.IsGood)
                        {
                            index++;
                        }*/
                    }
                    
                    lstCommentWeek.Add(objComment);
                }

                //Add thang
                objComment = new CommentWeek();
                objComment.PupilID = lstPupil[i].PupilID;
                objComment.ValuaWeek = 6; // Tháng               
                objComment.Comment = "";
                objComment.ValGooodChildren = 0;
                //if (index == numberWeek)
                {
                    GoodChildrenTicket gct = lstGoodChildren.Where(o => o.PupilID == lstPupil[i].PupilID && 
                                                                        o.ClassID == lstPupil[i].ClassID && 
                                                                        o.WeekInMonth == 6).FirstOrDefault();
                    objComment.Comment = gct != null ? gct.Note : "";
                    objComment.ValGooodChildren = gct != null ? (gct.IsGood ? 1 : 0) : 0;
                }
                lstCommentWeek.Add(objComment);
                list.Add(g);
            }

            ViewData[GoodChildrenTicketConstants.LIST_RESULT_COMMENT] = lstCommentWeek;
            return list;
        }
        #endregion

        #region SaveGoodChildrenTicket
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveGoodChildrenTicket(FormCollection frm)
        {
            int ClassID = !string.IsNullOrEmpty(frm["ClassID"]) ? int.Parse(frm["ClassID"]) : 0;
            if(ClassID == 0)
                return Json(new JsonMessage(Res.Get("Lớp không tồn tại. Vui lòng kiểm tra lại!"), "error"));

            int NumberWeek = !string.IsNullOrEmpty(frm["NumberWeek"]) ? int.Parse(frm["NumberWeek"]) : 0;
            if (NumberWeek == 0 || NumberWeek > 5)
                return Json(new JsonMessage(Res.Get("Số tuần không hợp lệ. Vui lòng kiểm tra lại!"), "error"));

            string MonthID = !string.IsNullOrEmpty(frm["MonthID"]) ? frm["MonthID"] : "";
            if(string.IsNullOrEmpty(MonthID))
            {
                 return Json(new JsonMessage(Res.Get("Thầy/cô chọn tháng không hợp lệ!"), "error"));
            }
            else
            {
                List<string> monthSelected = new List<string>();         
                monthSelected = MonthID.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
                if(monthSelected.Count != 2)
                {
                     return Json(new JsonMessage(Res.Get("Thầy/cô chọn tháng không hợp lệ!"), "error"));
                }           
            }

            //lay danh sach hoc sinh trong lop
            List<PupilOfClassBO> lstPOCDB = PupilOfClassBusiness.GetPupilInClass(ClassID).Distinct().ToList();
            List<PupilOfClassBO> lstPOC = lstPOCDB.Count() > 0 ? lstPOCDB.OrderBy(x => x.PupilID).ToList() : new List<PupilOfClassBO>();
            PupilOfClassBO objPOC = null;

            List<GoodChildrenTicket> lstGoodChildren = new List<GoodChildrenTicket>();
            GoodChildrenTicket objGoodChildren = null;

            //string UserNameComment = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeName : Res.Get("Message_Label_GroupUserSchoolAdmin");
            AcademicYear objYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime monthDate = Convert.ToDateTime(MonthID);
            int LogChangeID = _globalInfo.EmployeeID > 0 ? _globalInfo.EmployeeID.Value : 0;
            int PupilID = 0;

            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(ClassID);
            
            List<int> lstPupilID = new List<int>();
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                PupilID = objPOC.PupilID;
                if (!"on".Equals(frm["chk_" + PupilID]) || objPOC.Status != SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_STUDYING)
                {                 
                    continue;
                }
                lstPupilID.Add(PupilID);

                // Duyet tuan
                for (int j = 1; j <= NumberWeek; j++)
                {
                    objGoodChildren = new GoodChildrenTicket();
                    objGoodChildren.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objGoodChildren.ClassID = ClassID;
                    objGoodChildren.PupilID = PupilID;
                    objGoodChildren.SchoolID = _globalInfo.SchoolID.Value;
                    objGoodChildren.Month = monthDate;
                    objGoodChildren.WeekInMonth = j;
                    objGoodChildren.LogID = LogChangeID;
                    objGoodChildren.CreateDate = DateTime.Now;
                    objGoodChildren.ModifiedDate = DateTime.Now;
                    objGoodChildren.Year = objYear.Year;

                    string isGood = "week_" + j + "_" + PupilID;
                    string frmValueIsGood = frm[isGood];

                    string noteWeek = "comment_week" + j + "_" + PupilID;
                    string frmValueNote = frm[noteWeek].Trim();

                    objGoodChildren.IsGood = frmValueIsGood == "1" ? true : false;
                    objGoodChildren.Note = frmValueNote;

                    //if (!objGoodChildren.IsGood && !string.IsNullOrEmpty(objGoodChildren.Note))
                    //{
                    //    continue;
                    //}

                    lstGoodChildren.Add(objGoodChildren);
                }

                //Thang
                objGoodChildren = new GoodChildrenTicket();
                objGoodChildren.AcademicYearID = _globalInfo.AcademicYearID.Value;
                objGoodChildren.ClassID = ClassID;
                objGoodChildren.PupilID = PupilID;
                objGoodChildren.SchoolID = _globalInfo.SchoolID.Value;
                objGoodChildren.Month = monthDate;
                objGoodChildren.WeekInMonth = 6; // Tháng
                objGoodChildren.LogID = LogChangeID;
                objGoodChildren.CreateDate = DateTime.Now;
                objGoodChildren.ModifiedDate = DateTime.Now;
                objGoodChildren.Year = objYear.Year;

                string isGoodMonth = "month_" + PupilID;
                string frmValueIsGoodMonth = frm[isGoodMonth];

                string noteMonth = "comment_month_"  + PupilID;
                string frmValueNoteMonth = frm[noteMonth].Trim();

                objGoodChildren.IsGood = frmValueIsGoodMonth == "1" ? true : false;
                objGoodChildren.Note = frmValueNoteMonth;
                lstGoodChildren.Add(objGoodChildren);
            }

            Dictionary<string, object> dicInsert = new Dictionary<string, object>()
            {
                    {"SchoolID",_globalInfo.SchoolID.Value},
                    {"AcademicYearID",_globalInfo.AcademicYearID.Value},
                    {"EducationLevelID", EducationLevelID},
                    {"ClassID",ClassID},
                    {"Month", monthDate},
                    {"Year", objYear.Year},
                    {"lstPupilID", lstPupilID}
           };

            GoodChildrenTicketBusiness.InsertOrUpdateGoodChildren(lstGoodChildren, dicInsert);

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage"), "success"));
        }
        #endregion

        #region DeleteGoodChildrenTicket
        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteGoodChildrenTicket(int ClassID, string MonthID, string arrPupilID)
        {
            List<int> lstPupilID = arrPupilID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
         
            if (string.IsNullOrEmpty(MonthID))
            {
                return Json(new JsonMessage(Res.Get("Thầy/cô chọn tháng không hợp lệ!"), "error"));
            }
            else
            {
                List<string> monthSelected = new List<string>();
                monthSelected = MonthID.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList();
                if (monthSelected.Count != 2)
                {
                    return Json(new JsonMessage(Res.Get("Thầy/cô chọn tháng không hợp lệ!"), "error"));
                }
            }
            DateTime monthDate = Convert.ToDateTime(MonthID);

            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"Month", monthDate},
                {"lstPupilID",lstPupilID}
            };

            GoodChildrenTicketBusiness.DeleteGoodChildren(dic);
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage"), "success"));
        }
        #endregion

        public PartialViewResult AjaxLoadMonth()
        {
            LoadMonth();
            return PartialView("_ViewMonth");
        }

        #region LoadMonth
        private void LoadMonth()
        {
            List<MonthViewModel> lstMonth = new List<MonthViewModel>();
            if (gl.AcademicYearID.HasValue)
            {
                AcademicYear ay = AcademicYearBusiness.Find(gl.AcademicYearID.Value);
                string start = "";
                string end = "";
                int startYear = ay.FirstSemesterStartDate.Value.Year;
                int endYear = ay.SecondSemesterEndDate.Value.Year;
                int startMonth = ay.FirstSemesterStartDate.Value.Month;
                int endMonth = ay.SecondSemesterEndDate.Value.Month;

                int yearNow = DateTime.Now.Year;
                int MonthNow = DateTime.Now.Month;
                string monthDefault = MonthNow + "/" + yearNow;
                if (MonthNow < 10)
                {
                    monthDefault = "0" + MonthNow + "/" + yearNow;
                }

                MonthViewModel objMonth = null;
                for (int i = startMonth; i <= 12; i++)
                {
                    if (i < 10)
                    {
                        start = Convert.ToString("0" + i + "/" + startYear);
                    }
                    else
                    {
                        start = Convert.ToString(i + "/" + startYear);
                    }                  

                    objMonth = new MonthViewModel();
                    objMonth.Display = "Tháng " + i;
                    objMonth.MonthID = start;
                    objMonth.Selected = (start == monthDefault ? true: false);
                    lstMonth.Add(objMonth);
                }

                if (endYear > startYear)
                {
                    for (int j = 1; j <= endMonth; j++)
                    {
                        if (j < 10)
                        {
                            end = Convert.ToString("0" + j + "/" + endYear);
                        }
                        else
                        {
                            end = Convert.ToString(j + "/" + endYear);
                        }

                        objMonth = new MonthViewModel();
                        objMonth.Display = "Tháng " + j;
                        objMonth.MonthID = end;
                        objMonth.Selected = (end == monthDefault ? true : false);
                        lstMonth.Add(objMonth);
                    }
                }
                var check = lstMonth.Where(x=>x.Selected).FirstOrDefault();           
                if (check == null)
                {
                    lstMonth[0].Selected = true;
                }           
            }

            ViewData[GoodChildrenTicketConstants.LST_MONTH] = lstMonth;
        }
        #endregion
    }
}





