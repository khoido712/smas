﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.TrainingLevelArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.TrainingLevelArea.Controllers
{
    public class TrainingLevelController : BaseController
    {        
        private readonly ITrainingLevelBusiness TrainingLevelBusiness;
		
		public TrainingLevelController (ITrainingLevelBusiness traininglevelBusiness)
		{
			this.TrainingLevelBusiness = traininglevelBusiness;
		}
		
		//
        // GET: /TrainingLevel/

        public ActionResult Index()
        {
            SetViewDataPermission("TrainningLevel", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here
            SearchInfo["IsActive"] = true;
            IEnumerable<TrainingLevelViewModel> lst = this._Search(SearchInfo);
            ViewData[TrainingLevelConstants.LIST_TRAININGLEVEL] = lst;
            return View();
        }

		//
        // GET: /TrainingLevel/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
			//
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;

            IEnumerable<TrainingLevelViewModel> lst = this._Search(SearchInfo);
            ViewData[TrainingLevelConstants.LIST_TRAININGLEVEL] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("TrainningLevel", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            TrainingLevel traininglevel = new TrainingLevel();
            traininglevel.IsActive = true;
            TryUpdateModel(traininglevel); 
            Utils.Utils.TrimObject(traininglevel);

            this.TrainingLevelBusiness.Insert(traininglevel);
            this.TrainingLevelBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int TrainingLevelID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("TrainningLevel", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            TrainingLevel traininglevel = this.TrainingLevelBusiness.Find(TrainingLevelID);
            TryUpdateModel(traininglevel);
            Utils.Utils.TrimObject(traininglevel);
            this.TrainingLevelBusiness.Update(traininglevel);
            this.TrainingLevelBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("TrainningLevel", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.TrainingLevelBusiness.Delete(id);
            this.TrainingLevelBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<TrainingLevelViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("TrainningLevel", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<TrainingLevel> query = this.TrainingLevelBusiness.Search(SearchInfo);
            IQueryable<TrainingLevelViewModel> lst = query.Select(o => new TrainingLevelViewModel {               
						TrainingLevelID = o.TrainingLevelID,								
						Resolution = o.Resolution,								
						Description = o.Description								
						
				
            });

            return lst.ToList();
        }        
    }
}





