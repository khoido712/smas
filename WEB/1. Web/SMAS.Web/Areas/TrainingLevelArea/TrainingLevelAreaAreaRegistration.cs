﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.TrainingLevelArea
{
    public class TrainingLevelAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TrainingLevelArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TrainingLevelArea_default",
                "TrainingLevelArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
