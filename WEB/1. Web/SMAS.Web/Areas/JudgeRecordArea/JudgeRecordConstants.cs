/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.JudgeRecordArea
{
    public class JudgeRecordConstants
    {
        public const string LIST_JUDGERECORD = "listJudgeRecord";
        public const string LIST_SEMESTER = "listSemester";
        public const string SEMESTER = "semester";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_PERIOD = "listPeriod";
        public const string LIST_CLASS = "listClass";
        public const string LIST_SUBJECT = "listSubject";
        public const string LIST_SUMMEDUPRECORDOFCLASS = "listSummedUpRecordOfClass";
        public const string LIST_SUMMEDUPRECORDOFCLASSIMPORT = "listSummedUpRecordOfClassImport";
        public const string LIST_LOCKEDMARKDETAIL = "listLockedMarkDetail";
        public const string LIST_JUDGERECORDOFCLASS = "listJudgeRecordOfClass";
        public const string LIST_PERIODDECLARATION = "listPeriodDeclaration";
        public const string LIST_MARKTYPE = "listMarkType";
        public const string SEARCHTITLE = "SearchTitle";
        public const string JUDGERECORDMARK = "JudgeRecordMark";
        public const string JUDGERECORDMARKTBM = "JudgeRecordMarkTBM";
        public const string M_COEFFICIENT = "m_coefficient";
        public const string P_COEFFICIENT = "p_coefficient";
        public const string V_COEFFICIENT = "v_coefficient";
        public const string M_TYPE = "m_type";
        public const string P_TYPE = "p_type";
        public const string V_TYPE = "v_type";
        public const string HASPERMISION = "haspermision";

        public const string STR_INTERVIEW_MARK = "strInterviewMark";
        public const string STR_WRITINGVIEW_MARK = "strWritingViewMark";
        public const string STR_TWICEVIEW_MARK = "strTwiceViewMark";

        public const string LIST_IMPORTDATA = "LIST_IMPORTDATA";
        public const string HAS_ERROR_DATA = "HAS_ERROR_DATA";
        public const string ERROR_BASIC_DATA = "ERROR_BASIC_DATA";
        public const string ERROR_IMPORT_MESSAGE = "ERROR_IMPORT_MESSAGE";

        public const string ISPERMISSIONSUPERVISING = "isPermissionSupervising";
        public const string LOCKACTION = "LockAction";
        public const string SEMESTERMARK = "SEMESTERMARK";
        public const string CHECKTIMEOUTOFPERIOD = "CHECKTIMEOUTOFPERIOD";

        public const string CHECK_PEMISSION_GVBM = "CHECK_PERMISSION_GVBM";
        public const string ISCLASSIFICATION = "IsClassification";

        public const string LAST_UPDATE_MARK_RECORD = "LAST_UPDATE_MARK_RECORD";
        public const string LAST_UPDATE_STRING = "LAST_UPDATE_STRING";
        public const string LIST_LOCK_TITLE = "LIST_LOCK_TITLE";
        public const string TITLE_STRING = "TITLE_STRING";
        public const string LIST_CHANGE_MARK = "LIST_CHANGE_MARK";
        public const string IS_LOCK_INPUT = "IsLockInput";
        public const string LOCK_USER_NAME = "LockUserName";
    }
}