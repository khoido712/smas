﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  namdv3
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Business.Business;
using SMAS.Web.Areas.JudgeRecordArea.Models;

using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Text;
using SMAS.Web.Filter;
using SMAS.Models.Models.CustomModels;
using System.Configuration;
using System.Text.RegularExpressions;
using SMAS.VTUtils.Log;
using Newtonsoft.Json;

namespace SMAS.Web.Areas.JudgeRecordArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class JudgeRecordController : BaseController
    {
        private readonly IJudgeRecordBusiness JudgeRecordBusiness;
        private readonly IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness;
        private readonly ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness;
        private readonly IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IMarkTypeBusiness MarkTypeBusiness;
        private readonly ISemeterDeclarationBusiness SemeterDeclarationBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISummedUpRecordBusiness SummedUpRecordBusiness;
        private readonly ILockedMarkDetailBusiness LockedMarkDetailBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IRestoreDataBusiness RestoreDataBusiness;
        private readonly IRestoreDataDetailBusiness RestoreDataDetailBusiness;
        public JudgeRecordController(IJudgeRecordBusiness judgerecordBusiness, IJudgeRecordHistoryBusiness judgeRecordHistoryBusiness,
            ISummedUpRecordHistoryBusiness summedUpRecordHistoryBusiness, IPeriodDeclarationBusiness perioddeclarationbusiness,
            IClassProfileBusiness classprofilebusiness, IClassSubjectBusiness classsubjectbusiness, ITeachingAssignmentBusiness teachingassignmentbusiness,
            IMarkTypeBusiness marktypebusiness, ISemeterDeclarationBusiness semeterdeclarationbusiness, IAcademicYearBusiness academicyearbusiness,
            ISummedUpRecordBusiness summeduprecordbusiness, ILockedMarkDetailBusiness lockedmarkdetailbusiness, IMarkRecordBusiness markrecordbusiness,
            ISubjectCatBusiness subjectcatbusiness, IExemptedSubjectBusiness exemptedsubjectbusiness, IPupilProfileBusiness pupilprofilebusiness,
            IPupilOfClassBusiness pupilofclassbusiness, IUserAccountBusiness UserAccountBusiness, IEmployeeBusiness employeeBusiness,
            IRestoreDataBusiness RestoreDataBusiness,
            IRestoreDataDetailBusiness RestoreDataDetailBusiness
            )
        {
            this.JudgeRecordBusiness = judgerecordBusiness;
            this.JudgeRecordHistoryBusiness = judgeRecordHistoryBusiness;
            this.SummedUpRecordHistoryBusiness = summedUpRecordHistoryBusiness;
            this.PeriodDeclarationBusiness = perioddeclarationbusiness;
            this.ClassProfileBusiness = classprofilebusiness;
            this.ClassSubjectBusiness = classsubjectbusiness;
            this.TeachingAssignmentBusiness = teachingassignmentbusiness;
            this.MarkTypeBusiness = marktypebusiness;
            this.SemeterDeclarationBusiness = semeterdeclarationbusiness;
            this.AcademicYearBusiness = academicyearbusiness;
            this.SummedUpRecordBusiness = summeduprecordbusiness;
            this.LockedMarkDetailBusiness = lockedmarkdetailbusiness;
            this.MarkRecordBusiness = markrecordbusiness;
            this.SubjectCatBusiness = subjectcatbusiness;
            this.ExemptedSubjectBusiness = exemptedsubjectbusiness;
            this.PupilProfileBusiness = pupilprofilebusiness;
            this.PupilOfClassBusiness = pupilofclassbusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.RestoreDataBusiness = RestoreDataBusiness;
            this.RestoreDataDetailBusiness = RestoreDataDetailBusiness;
        }

        //
        // GET: /JudgeRecord/

        public ActionResult Index()
        {
            if (_globalInfo.HasSubjectTeacherPermission(0, 0))
            {
                //rptSemester: CommonList.Semester, mặc định: UserInfo.Semester
                var lstSemester = CommonList.Semester();
                List<ViettelCheckboxList> lstViettelCheckboxListSemester = new List<ViettelCheckboxList>();
                lstViettelCheckboxListSemester.Add(new ViettelCheckboxList(lstSemester[0].value, lstSemester[0].key, int.Parse(lstSemester[0].key) == _globalInfo.Semester ? true : false, false));
                lstViettelCheckboxListSemester.Add(new ViettelCheckboxList(lstSemester[1].value, lstSemester[1].key, int.Parse(lstSemester[1].key) == _globalInfo.Semester ? true : false, false));
                ViewData[JudgeRecordConstants.LIST_SEMESTER] = lstViettelCheckboxListSemester;

                //rptEducationLevel: lấy thông tin UserInfo.EducationLevels (mặc định tích chọn giá trị đầu tiên trả về)
                var lstEducationlevel = _globalInfo.EducationLevels;
                List<ViettelCheckboxList> lstViettelCheckboxListEducationlevel = new List<ViettelCheckboxList>();
                lstViettelCheckboxListEducationlevel.Add(new ViettelCheckboxList(lstEducationlevel[0].Resolution, lstEducationlevel[0].EducationLevelID, true, false));
                foreach (var item in lstEducationlevel.Skip(1))
                {
                    lstViettelCheckboxListEducationlevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, false, false));
                }
                ViewData[JudgeRecordConstants.LIST_EDUCATIONLEVEL] = lstViettelCheckboxListEducationlevel;

                // cboPeriod: Lấy định nghĩa giai đoạn (sắp xếp theo thời gian thực hiện theo thứ tự tăng dần) 
                // và cấu hình số con điểm bằng cách gọi hàm PeriodDeclarationBusiness.SearchBySchool(UserInfo.SchoolID, IDictionary),
                // giá trị các tham số bắt buộc truyền vào: 
                //  + AcademicYearID = UserInfo.AcademicYearID
                //  + Semester = rptSemester.Value
                //  (mặc định giá trị đầu tiên trả về)
                IDictionary<string, object> PeriodSearchInfo = new Dictionary<string, object>();
                PeriodSearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
                PeriodSearchInfo["Semester"] = _globalInfo.Semester;
                var lstPeriod = PeriodDeclarationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, PeriodSearchInfo);
                List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
                int PeriodDeclarationIDSelected = 0;
                if (lstPeriod != null && lstPeriod.Count() > 0)
                {
                    DateTime startDate = new DateTime();
                    DateTime endDate = new DateTime();
                    DateTime nowDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    PeriodDeclarationIDSelected = lstPeriod.FirstOrDefault().PeriodDeclarationID;
                    foreach (var item in lstPeriod)
                    {
                        PeriodDeclaration periodDeclaration = new PeriodDeclaration();
                        periodDeclaration = item;
                        if (periodDeclaration.FromDate.HasValue && periodDeclaration.EndDate.HasValue)
                        {
                            periodDeclaration.Resolution = periodDeclaration.Resolution + " (" + Res.Get("Common_Label_From") + " " + periodDeclaration.FromDate.Value.ToShortDateString() + " " + Res.Get("Common_Label_To") + " " + periodDeclaration.EndDate.Value.ToShortDateString() + ")";
                            startDate = new DateTime(periodDeclaration.FromDate.Value.Year, periodDeclaration.FromDate.Value.Month, periodDeclaration.FromDate.Value.Day, 0, 0, 0);
                            endDate = new DateTime(periodDeclaration.EndDate.Value.Year, periodDeclaration.EndDate.Value.Month, periodDeclaration.EndDate.Value.Day, 0, 0, 0);
                            if (startDate <= nowDate && nowDate <= endDate)
                            {
                                PeriodDeclarationIDSelected = periodDeclaration.PeriodDeclarationID;
                            }
                        }
                        listPeriodDeclaration.Add(periodDeclaration);
                    }
                }
                ViewData[JudgeRecordConstants.LIST_PERIOD] = new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution", PeriodDeclarationIDSelected);

                //- rptClass: ClassProfileBussiness.SearchBySchool(UserInfo.SchoolID, Dictionnary) với
                //  + Dictionnary[“EducationLevelID”] = cboEducationLevel.Value
                //  + Dictionnary[“AcademicYearID”] = UserInfo.AcademicYearID
                //Đối với UserInfo.IsAdminSchoolRole() = False thêm điều kiện:
                //  + Dictionnary[“UserAccountID”] = UserInfo.UserAccountID  
                //  + Dictionary[“Type”] = TEACHER_ROLE_SUBJECTTEACHER (GVBM). 
                IDictionary<string, object> ClassSearchInfo = new Dictionary<string, object>();
                ClassSearchInfo["EducationLevelID"] = lstEducationlevel.FirstOrDefault().EducationLevelID;
                ClassSearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
                if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsViewAll)
                {
                    ClassSearchInfo["UserAccountID"] = _globalInfo.UserAccountID;
                    ClassSearchInfo["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                var lstClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, ClassSearchInfo).ToList();
                ViewData[JudgeRecordConstants.LIST_CLASS] = lstClass;
                ViewData[JudgeRecordConstants.ERROR_BASIC_DATA] = false;
                ViewData[JudgeRecordConstants.LIST_PERIODDECLARATION] = new PeriodDeclaration();
                ViewData[JudgeRecordConstants.LIST_JUDGERECORDOFCLASS] = new List<JudgeRecordBO>();
                ViewData[JudgeRecordConstants.SEMESTER] = _globalInfo.Semester;
                return View();
            }
            return RedirectToAction("LogOn", "Home");
        }

        [HttpPost]
        public ActionResult GetListPeriod()
        {
            int SemesterID = string.IsNullOrWhiteSpace(Request["SemesterID"]) ? 0 : int.Parse(Request["SemesterID"]);
            if (SemesterID > 0)
            {
                //Load combobox lop hoc theo khoi hoc
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = new GlobalInfo().AcademicYearID.Value;
                dic["Semester"] = SemesterID;
                IEnumerable<PeriodDeclaration> lstPeriod = PeriodDeclarationBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).OrderBy(o => o.FromDate);
                List<PeriodDeclaration> listPeriodDeclaration = new List<PeriodDeclaration>();
                if (lstPeriod != null && lstPeriod.Count() > 0)
                {
                    // Hungnd8 18/07/2013 Convert Foreach to linq
                    listPeriodDeclaration = (from p in lstPeriod
                                             select new PeriodDeclaration
                                             {
                                                 Resolution = ((p.FromDate.HasValue && p.EndDate.HasValue) ? (p.Resolution + " (từ " + p.FromDate.Value.ToShortDateString() + " đến " + p.EndDate.Value.ToShortDateString() + ")") : p.Resolution)
                                             }).ToList();
                }
                ViewData[JudgeRecordConstants.LIST_PERIOD] = new SelectList(listPeriodDeclaration, "PeriodDeclarationID", "Resolution");
            }
            return PartialView("_ListPeriod");
        }

        [HttpPost]

        [ValidateAntiForgeryToken]
        public PartialViewResult GetListClass(int? id)
        {
            if (id.HasValue)
            {
                var global = new GlobalInfo();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = global.AcademicYearID.Value;
                dic["EducationLevelID"] = id.Value;
                if (!global.IsAdminSchoolRole && !global.IsViewAll)
                {
                    dic["UserAccountID"] = global.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, dic).ToList();
                ViewData[JudgeRecordConstants.LIST_CLASS] = listClass;
            }
            return PartialView("_ListClass");
        }

        [HttpPost]
        public PartialViewResult GetListSubject()
        {
            int classid = string.IsNullOrWhiteSpace(Request["id"]) ? 0 : int.Parse(Request["id"]);
            int semesterid = string.IsNullOrWhiteSpace(Request["semesterid"]) ? 0 : int.Parse(Request["semesterid"]);
            if (classid > 0 && semesterid > 0)
            {
                var global = new GlobalInfo();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                //dic["IsCommenting"] = SMAS.Web.Constants.GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT;
                dic["ClassID"] = classid;
                dic["Semester"] = semesterid;
                List<ViettelCheckboxList> listSJ = new List<ViettelCheckboxList>();
                //Lấy danh sách môn học dựa vào quyền của giáo viên và có  IsCommenting = ISCOMMENTING_TYPE_JUDGE (Môn nhận xét)

                List<SubjectCat> listSubject = new List<SubjectCat>();
                if (global.AcademicYearID.HasValue && global.SchoolID.HasValue)
                {
                    listSubject = ClassSubjectBusiness.GetListSubjectBySubjectTeacher(global.UserAccountID, global.AcademicYearID.Value,
                        global.SchoolID.Value, semesterid, classid, global.IsViewAll)
                                 .Where(o => o.SubjectCat.IsActive == true)
                                 .Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                 .Select(o => o.SubjectCat)
                                 .OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName)
                                 .ToList();
                }

                foreach (var item in listSubject)
                {
                    listSJ.Add(new ViettelCheckboxList(item.DisplayName, item.SubjectCatID, false, false));
                }

                ViewData[JudgeRecordConstants.LIST_SUBJECT] = listSJ;
                var subject = (listSubject != null && listSubject.Count > 0) ? listSubject.FirstOrDefault() : null;
                ViewData[JudgeRecordConstants.HASPERMISION] = subject != null ? new GlobalInfo().HasSubjectTeacherPermission(classid, subject.SubjectCatID) : false;
                //ViewData[JudgeRecordConstants.CHECKTIMEOUTOFPERIOD] = true;
            }

            return PartialView("_ListSubject");
        }

        [HttpPost]

        public PartialViewResult Search()
        {
            GlobalInfo global = new GlobalInfo();
            var r = Request;
            int classid = string.IsNullOrWhiteSpace(r["Classid"]) ? 0 : int.Parse(r["Classid"]);
            int semesterid = string.IsNullOrWhiteSpace(r["semesterid"]) ? 0 : int.Parse(r["semesterid"]);
            int subjectid = string.IsNullOrWhiteSpace(r["subjectid"]) ? 0 : int.Parse(r["subjectid"]);
            int educationlevelid = string.IsNullOrWhiteSpace(r["educationlevelid"]) ? 0 : int.Parse(r["educationlevelid"]);
            int periodid = string.IsNullOrWhiteSpace(r["periodid"]) ? 0 : int.Parse(r["periodid"]);

            if (classid <= 0 || semesterid <= 0 || periodid <= 0)
                throw new BusinessException("Common_Validate_DataInvalid");

            ViewData[JudgeRecordConstants.ERROR_BASIC_DATA] = false;
            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            DateTime? EndDateSemester = DateTime.Now;
            if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                EndDateSemester = academicYear.FirstSemesterEndDate;
            }
            else if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                EndDateSemester = academicYear.SecondSemesterEndDate;
            }

            //Load tiêu đề hiển thị con điểm:
            var lstMarkType = MarkRecordBusiness.GetMarkTitle(periodid);
            ViewData[JudgeRecordConstants.SEMESTERMARK] = lstMarkType.Contains("HK");

            //Load thông tin số các con điểm M, P, V 
            PeriodDeclaration periodDeclaration = PeriodDeclarationBusiness.Find(periodid);
            if (periodDeclaration == null)
                throw new BusinessException("Validate_School_NotMarkPeriod");
            ClassProfile classProfile = ClassProfileBusiness.Find(classid);
            SubjectCat subjectCat = SubjectCatBusiness.Find(subjectid);

            IDictionary<string, object> SearchInfoJudgeRecord = new Dictionary<string, object>();
            SearchInfoJudgeRecord["SchoolID"] = global.SchoolID;
            SearchInfoJudgeRecord["AcademicYearID"] = global.AcademicYearID;
            SearchInfoJudgeRecord["Semester"] = ViewData["semesterid"] = semesterid;
            SearchInfoJudgeRecord["PeriodID"] = ViewData["periodid"] = periodid;
            SearchInfoJudgeRecord["ClassID"] = ViewData["classid"] = classid;
            SearchInfoJudgeRecord["SubjectID"] = ViewData["subjectid"] = subjectid;
            string titleMarkPeriod = MarkRecordBusiness.GetMarkTitle(periodid);
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = aca.IsShowPupil.HasValue && aca.IsShowPupil.Value;
            Dictionary<string, object> dicSearchPOC = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"ClassID",classid}
            };
            IQueryable<PupilOfClass> iqueryPOC = PupilOfClassBusiness.Search(dicSearchPOC).AddPupilStatus(isNotShowPupil);
            List<int> lstPupilID = iqueryPOC.Select(p => p.PupilID).Distinct().ToList();

            var listAfterGet = JudgeRecordBusiness.GetJudgeRecordOfClassNew(SearchInfoJudgeRecord);
            if (listAfterGet != null)
            {
                listAfterGet = listAfterGet.Where(p => lstPupilID.Contains(p.PUPIL_ID)).ToList();
            }
            //lay ra danh logchange cua hoc sinh theo tung con diem
            List<BookRecordLogChangeBO> listLogChange = new List<BookRecordLogChangeBO>();

            if (UtilsBusiness.IsMoveHistory(aca))
            {
                listLogChange = (from mr in JudgeRecordHistoryBusiness.All
                                 where mr.AcademicYearID == _globalInfo.AcademicYearID
                                 && mr.SchoolID == _globalInfo.SchoolID
                                 && mr.ClassID == classid
                                 && mr.SubjectID == subjectid
                                 && mr.Semester == semesterid
                                 && mr.Last2digitNumberSchool == _globalInfo.SchoolID % 100
                                 && lstPupilID.Contains(mr.PupilID)
                                 && titleMarkPeriod.Contains(mr.Title)
                                 select new BookRecordLogChangeBO
                                 {
                                     PupilID = mr.PupilID,
                                     Title = mr.Title,
                                     LogChange = mr.LogChange,
                                     ModifiedDate = mr.ModifiedDate,
                                     CreateDate = mr.CreatedDate,
                                     UpdateDate = mr.ModifiedDate.HasValue ? mr.ModifiedDate : mr.CreatedDate
                                 }).Where(o => o.UpdateDate != null).ToList();
            }
            else
            {
                listLogChange = (from mr in JudgeRecordBusiness.All
                                 where mr.AcademicYearID == _globalInfo.AcademicYearID
                                 && mr.SchoolID == _globalInfo.SchoolID
                                 && mr.ClassID == classid
                                 && mr.SubjectID == subjectid
                                 && mr.Semester == semesterid
                                 && mr.Last2digitNumberSchool == _globalInfo.SchoolID % 100
                                 && lstPupilID.Contains(mr.PupilID)
                                 && titleMarkPeriod.Contains(mr.Title)
                                 select new BookRecordLogChangeBO
                                 {
                                     PupilID = mr.PupilID,
                                     Title = mr.Title,
                                     LogChange = mr.LogChange,
                                     ModifiedDate = mr.ModifiedDate,
                                     CreateDate = mr.CreatedDate,
                                     UpdateDate = mr.ModifiedDate.HasValue ? mr.ModifiedDate : mr.CreatedDate
                                 }).Where(o => o.UpdateDate != null).ToList();
            }
            List<int?> lstEmployeeID = listLogChange != null ? listLogChange.Select(p => p.LogChange).Distinct().ToList() : new List<int?>();

            //viethd4: lay ra cac cap nhat gan nhat
            var lastUpdate = listLogChange.OrderByDescending(o => o.UpdateDate).FirstOrDefault();
            List<MarkRecordBO> lstLastMarkRecordID = new List<MarkRecordBO>();

            if (lastUpdate != null)
            {
                string lastUpdateTime = lastUpdate.UpdateDate != null ? lastUpdate.UpdateDate.Value.ToString("MM/dd/yyyy hh:mm") : null;
                lstLastMarkRecordID = listLogChange.Where(o => lastUpdateTime != null && o.UpdateDate != null ? o.UpdateDate.Value.ToString("MM/dd/yyyy hh:mm") == lastUpdateTime : false).Select(o =>
                    new MarkRecordBO
                    {
                        PupilID = o.PupilID,
                        Title = o.Title
                    }).ToList();

            }
            ViewData[JudgeRecordConstants.LAST_UPDATE_MARK_RECORD] = lstLastMarkRecordID;

            List<Employee> lstEmployeeChange = new List<Employee>();
            if (lstEmployeeID.Count > 0)
            {
                lstEmployeeChange = (from e in EmployeeBusiness.All
                                     where e.SchoolID == _globalInfo.SchoolID
                                     && lstEmployeeID.Contains(e.EmployeeID)
                                     select e).ToList();
            }
            Employee objEmployee = null;

            //viethd4: lay user va time cap nhat sau cung

            string tmpStr = String.Empty;
            string strLastUpdate = String.Empty;
            if (lastUpdate != null)
            {
                string lastUserName = String.Empty;
                if (lastUpdate.LogChange == 0)
                {
                    lastUserName = "Quản trị trường";
                }
                else
                {
                    Employee lastUser = lstEmployeeChange.Where(o => o.EmployeeID == lastUpdate.LogChange).FirstOrDefault();
                    if (lastUser != null)
                    {
                        lastUserName = lastUser.FullName;
                    }
                }

                tmpStr = string.Format("<span style='color:#d56900 '>{0}h{1} ngày {2:dd/MM/yyyy}</span> {3} <span style='color:#d56900 '>{4}</span>", new object[]{
                        lastUpdate.UpdateDate.Value.Hour.ToString(),
                        lastUpdate.UpdateDate.Value.Minute.ToString("D2"),
                        lastUpdate.UpdateDate,
                        !String.IsNullOrEmpty(lastUserName)?"bởi":String.Empty,
                        lastUserName});

                strLastUpdate = string.Format("<span>Lần cập nhật gần nhất: {0}</span>", tmpStr);
            }

            ViewData[JudgeRecordConstants.LAST_UPDATE_STRING] = strLastUpdate;


            List<JudgeRecordNewBO> lstJudgeRecordOfClass = new List<JudgeRecordNewBO>();
            if (listAfterGet != null && listAfterGet.Count() > 0)
                lstJudgeRecordOfClass = listAfterGet.ToList();

            List<SummedUpRecordBO> lstSummedUpRecordOfClass = new List<SummedUpRecordBO>();

            if (semesterid == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                lstSummedUpRecordOfClass = lstJudgeRecordOfClass.Where(u => u.HK1 != null).Select(o => new SummedUpRecordBO()
                {
                    PupilID = o.PUPIL_ID,
                    ClassID = classid,
                    SubjectID = subjectid,
                    Semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST,
                    JudgementResult = o.HK1,
                }).ToList();
            }
            else
            {
                lstSummedUpRecordOfClass = lstJudgeRecordOfClass.Where(u => u.HK2 != null).Select(o => new SummedUpRecordBO()
                {
                    PupilID = o.PUPIL_ID,
                    ClassID = classid,
                    SubjectID = subjectid,
                    Semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND,
                    JudgementResult = o.HK2,
                }).ToList();
            }

            //Lấy danh sách học sinh được miễn giảm
            List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.GetListExemptedSubject(classid, semesterid).Where(u => u.SubjectID == subjectid).ToList();

            //Lấy danh sách khóa con điểm lstLockedMarkDetail
            string lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(global.SchoolID.Value, global.AcademicYearID.Value, classid, semesterid, subjectid, periodid);
            lstLockedMarkDetail = lstLockedMarkDetail.Replace(",", " ");
            lstLockedMarkDetail = lstLockedMarkDetail.TrimStart();
            lstLockedMarkDetail = lstLockedMarkDetail.Replace(" ", ", ");
            ViewData[JudgeRecordConstants.LIST_PERIODDECLARATION] = periodDeclaration;
            ViewData[JudgeRecordConstants.LIST_MARKTYPE] = lstMarkType;

            ViewData[JudgeRecordConstants.STR_INTERVIEW_MARK] = !string.IsNullOrEmpty(periodDeclaration.StrInterviewMark) ? periodDeclaration.StrInterviewMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : null;
            ViewData[JudgeRecordConstants.STR_WRITINGVIEW_MARK] = !string.IsNullOrEmpty(periodDeclaration.StrWritingMark) ? periodDeclaration.StrWritingMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : null;
            ViewData[JudgeRecordConstants.STR_TWICEVIEW_MARK] = !string.IsNullOrEmpty(periodDeclaration.StrTwiceCoeffiecientMark) ? periodDeclaration.StrTwiceCoeffiecientMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : null;

            List<JudgeRecordBO> List_JudgeRecordBO = new List<JudgeRecordBO>();
            string[] listMarkPeriod = lstMarkType.Split(',');
            foreach (var JudgeBO in lstJudgeRecordOfClass)
            {
                JudgeRecordBO JudgeRecordBO = new JudgeRecordBO();
                JudgeRecordBO.PupilID = JudgeBO.PUPIL_ID;
                JudgeRecordBO.FullName = JudgeBO.FULL_NAME;
                JudgeRecordBO.Name = JudgeBO.S_NAME;
                JudgeRecordBO.OrderInClass = JudgeBO.ORDER_IN_CLASS;
                JudgeRecordBO.EndDate = JudgeBO.END_DATE;
                JudgeRecordBO.Status = JudgeBO.STATUS;
                bool flag = true;
                bool displayMark = true;
                bool statusDisplay = false;
                //-	Với học sinh có trạng thái thuộc một trong các trạng thái chuyển trường (lstJudgeRecordOfClass[i].Status = PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL),
                //chuyển lớp (lstJudgeRecordOfClass[i].Status = PUPIL_STATUS_MOVED_TO_OTHER_CLASS),
                //thôi học (lstJudgeRecordOfClass[i].Status = PUPIL_STATUS_LEAVED_OFF) (lấy trạng thái học sinh trong bảng PupilOfClass)
                //thì không hiển thị điểm, chỉ hiển thị danh sách.
                if (JudgeBO.STATUS == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL ||
                    JudgeBO.STATUS == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS ||
                    JudgeBO.STATUS == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_LEAVED_OFF)
                {
                    //thời gian chuyển so với đợt hiện tại                    

                    if (JudgeBO.END_DATE.HasValue && JudgeBO.END_DATE.Value > EndDateSemester)
                    {
                        flag = true;
                        displayMark = true;
                        statusDisplay = true;
                    }
                    else
                    {
                        flag = false;
                        displayMark = false;
                    }
                }
                else if (JudgeBO.STATUS == SMAS.Web.Constants.GlobalConstants.PUPIL_STATUS_GRADUATED)
                {
                    statusDisplay = true;
                }

                flag = !lstExempted.Any(u => u.PupilID == JudgeBO.PUPIL_ID);

                JudgeRecordBO.EnableMark = flag || global.IsViewAll;
                JudgeRecordBO.DisplayMark = displayMark || global.IsViewAll;
                JudgeRecordBO.StatusDisplay = statusDisplay;
                JudgeRecordBO.IsExempted = flag;

                //List<JudgeRecordBO> listPupilJudgeRecord = lstJudgeRecordOfClass.Where(o => o.PupilID == JudgeRecordBO.PupilID).ToList();
                JudgeRecordBO.InterviewMark = new string[Convert.ToByte(periodDeclaration.InterviewMark)];
                JudgeRecordBO.WritingMark = new string[Convert.ToByte(periodDeclaration.WritingMark)];
                JudgeRecordBO.TwiceCoeffiecientMark = new string[Convert.ToByte(periodDeclaration.TwiceCoeffiecientMark)];
                JudgeRecordBO.LogChangeM = new string[periodDeclaration.InterviewMark.Value];
                JudgeRecordBO.LogChangeP = new string[periodDeclaration.WritingMark.Value];
                JudgeRecordBO.LogChangeV = new string[periodDeclaration.TwiceCoeffiecientMark.Value];

                int InterviewMark = periodDeclaration.InterviewMark.Value;
                //int StartIndexOfInterviewMark = periodDeclaration.StartIndexOfInterviewMark.Value;
                int WritingMark = periodDeclaration.WritingMark.Value;
                //int StartIndexOfWritingMark = periodDeclaration.StartIndexOfWritingMark.Value;
                int TwiceCoeffiecientMark = periodDeclaration.TwiceCoeffiecientMark.Value;
                //int StartIndexOfTwiceCoeffiecientMark = periodDeclaration.StartIndexOfTwiceCoeffiecientMark.Value;
                StringBuilder stringTitle = new StringBuilder();
                int indexInterviewMark = 0;
                int indexWritingMark = 0;
                int indexTwiceCoeffiecientMark = 0;
                foreach (string TitleMarkPeriod in listMarkPeriod)
                {
                    object Mark = Utils.Utils.GetValueByProperty(JudgeBO, TitleMarkPeriod);
                    if (TitleMarkPeriod.Length < 2 || TitleMarkPeriod.Equals("HK"))
                        continue;
                    var indexMark = int.Parse(TitleMarkPeriod[1].ToString());
                    if (Mark != null)
                    {
                        if (TitleMarkPeriod.Trim().Length > 0)
                        {
                            if (TitleMarkPeriod.Contains("M"))
                            {
                                objEmployee = null;
                                JudgeRecordBO.InterviewMark[indexInterviewMark] = Mark.ToString();
                                var objLogChange = listLogChange.Where(p => p.PupilID == JudgeBO.PUPIL_ID && p.Title.Equals("M" + indexMark)).FirstOrDefault();
                                if (objLogChange != null && objLogChange.LogChange > 0)
                                {
                                    objEmployee = lstEmployeeChange.Where(p => p.EmployeeID == objLogChange.LogChange).FirstOrDefault();
                                }
                                JudgeRecordBO.LogChangeM[indexInterviewMark] = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                                indexInterviewMark++;
                            }
                            else if (TitleMarkPeriod.Contains("P"))
                            {
                                objEmployee = null;
                                JudgeRecordBO.WritingMark[indexWritingMark] = Mark.ToString();
                                var objLogChange = listLogChange.Where(p => p.PupilID == JudgeBO.PUPIL_ID && p.Title.Equals("P" + indexMark)).FirstOrDefault();
                                if (objLogChange != null && objLogChange.LogChange > 0)
                                {
                                    objEmployee = lstEmployeeChange.Where(p => p.EmployeeID == objLogChange.LogChange).FirstOrDefault();
                                }
                                JudgeRecordBO.LogChangeP[indexWritingMark] = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                                indexWritingMark++;
                            }
                            else if (TitleMarkPeriod.Contains("V"))
                            {
                                objEmployee = null;
                                JudgeRecordBO.TwiceCoeffiecientMark[indexTwiceCoeffiecientMark] = Mark.ToString();
                                var objLogChange = listLogChange.Where(p => p.PupilID == JudgeBO.PUPIL_ID && p.Title.Equals("V" + indexMark)).FirstOrDefault();
                                if (objLogChange != null && objLogChange.LogChange > 0)
                                {
                                    objEmployee = lstEmployeeChange.Where(p => p.EmployeeID == objLogChange.LogChange).FirstOrDefault();
                                }
                                JudgeRecordBO.LogChangeV[indexTwiceCoeffiecientMark] = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                                indexTwiceCoeffiecientMark++;
                            }
                            stringTitle.Append(TitleMarkPeriod + ",");

                        }
                    }
                    else
                    {
                        if (TitleMarkPeriod.Contains("M"))
                        {
                            indexInterviewMark++;
                        }
                        else if (TitleMarkPeriod.Contains("P"))
                        {
                            indexWritingMark++;
                        }
                        else if (TitleMarkPeriod.Contains("V"))
                        {
                            indexTwiceCoeffiecientMark++;
                        }
                        continue;
                    }
                }
                objEmployee = null;
                JudgeRecordBO.MarkTitle = lstMarkType.ToString();
                object MarkSemester = Utils.Utils.GetValueByProperty(JudgeBO, "HK");
                if (MarkSemester != null)
                {
                    JudgeRecordBO.SemesterMark = MarkSemester.ToString();
                    var objLogChange = listLogChange.Where(p => p.PupilID == JudgeBO.PUPIL_ID && p.Title.Equals("HK")).FirstOrDefault();
                    if (objLogChange != null && objLogChange.LogChange > 0)
                    {
                        objEmployee = lstEmployeeChange.Where(p => p.EmployeeID == objLogChange.LogChange).FirstOrDefault();
                    }
                    JudgeRecordBO.LogChangeHK = objLogChange != null ? "Cập nhật " + (objLogChange.ModifiedDate.HasValue ? objLogChange.ModifiedDate.Value.ToString("HH:mm dd/MM/yyyy") : (objLogChange.CreateDate != null ? objLogChange.CreateDate.Value.ToString("HH:mm dd/MM/yyyy") : "")) + (objEmployee != null ? "<br/>bởi " + objEmployee.FullName : (objLogChange.LogChange == 0 ? "<br/>bởi Quản trị trường" : "")) : "";
                }

                List_JudgeRecordBO.Add(JudgeRecordBO);
            }

            ViewData[JudgeRecordConstants.LIST_JUDGERECORDOFCLASS] = List_JudgeRecordBO.OrderBy(i => i.OrderInClass).ThenBy(i => i.Name).ThenBy(i => i.FullName).ToList();
            ViewData[JudgeRecordConstants.LIST_SUMMEDUPRECORDOFCLASS] = lstSummedUpRecordOfClass.ToList();
            ViewData[JudgeRecordConstants.LIST_SUMMEDUPRECORDOFCLASSIMPORT] = new List<SummedUpRecordBO>();
            Session["List_JudgeRecordBO"] = List_JudgeRecordBO;
            ViewData[JudgeRecordConstants.LIST_LOCKEDMARKDETAIL] = lstLockedMarkDetail;
            //Được lưu dưới dạng: “Sổ điểm môn” + tên môn học chọn + tên đợt + “Lớp” + tên lớp
            var strTitle = Res.Get("JudgeRecord_Label_SearchTitle");
            ViewData[JudgeRecordConstants.SEARCHTITLE] = string.Format(strTitle, subjectCat.DisplayName, periodDeclaration.Resolution, classProfile.DisplayName);
            var lstJudgeRecordMark = CommonList.JudgeRecordMark(true);
            ViewData[JudgeRecordConstants.JUDGERECORDMARK] = new SelectList(lstJudgeRecordMark, "Key", "Value");
            var lstJudgeRecordMarkTBM = CommonList.JudgeRecordMark(true);
            ViewData[JudgeRecordConstants.JUDGERECORDMARKTBM] = new SelectList(lstJudgeRecordMarkTBM, "Key", "Value");
            bool m_type = lstMarkType.Contains("M");
            ViewData[JudgeRecordConstants.M_TYPE] = m_type ? "M" : string.Empty;
            bool p_type = lstMarkType.Contains("P");
            ViewData[JudgeRecordConstants.P_TYPE] = p_type ? "P" : string.Empty;
            bool v_type = lstMarkType.Contains("V");
            ViewData[JudgeRecordConstants.V_TYPE] = v_type ? "V" : string.Empty;
            bool isPermissionSupervising = UtilsBusiness.HasSubjectTeacherPermission(global.UserAccountID, classid, subjectid, semesterid);

            ViewData[JudgeRecordConstants.ISCLASSIFICATION] = academicYear.IsClassification;
            if (global.IsCurrentYear == false)
            {
                ViewData[JudgeRecordConstants.LOCKACTION] = false;
            }
            else
            {
                if (isPermissionSupervising)
                {
                    if (global.IsAdminSchoolRole)
                    {
                        ViewData[JudgeRecordConstants.LOCKACTION] = true;
                        ViewData[JudgeRecordConstants.ISPERMISSIONSUPERVISING] = true;
                    }
                    else if (!_globalInfo.isCurrentSemester(aca, semesterid) || periodDeclaration.IsLock == true)
                    {
                        //Neu hoc ky da ket thuc thi khong cho nhap diem
                        //Neu dot bi khoa thi khong cho nhap diem
                        ViewData[JudgeRecordConstants.LOCKACTION] = false;
                        ViewData[JudgeRecordConstants.ISPERMISSIONSUPERVISING] = false;
                    }

                    else if (_globalInfo.isCurrentSemester(aca, semesterid) && periodDeclaration.IsLock == false && periodDeclaration.EndDate < DateTime.Now && periodDeclaration.IsEditMark == true)
                    {
                        //Neu la hoc ky hien tai, dot da ket thuc , dot chua bi khoa va giao vien duoc phep nhap diem thì cho phep nhap diem
                        ViewData[JudgeRecordConstants.LOCKACTION] = true;
                        ViewData[JudgeRecordConstants.ISPERMISSIONSUPERVISING] = true;
                    }
                    else if (periodDeclaration.FromDate.Value.Date <= DateTime.Now.Date && DateTime.Now.Date <= periodDeclaration.EndDate.Value.Date && periodDeclaration.IsLock == false)
                    {
                        //Neu la la dot ien tai va chua bi khoa thi cho phep nhap diem
                        ViewData[JudgeRecordConstants.LOCKACTION] = true;
                        ViewData[JudgeRecordConstants.ISPERMISSIONSUPERVISING] = true;
                    }
                    else
                    {
                        ViewData[JudgeRecordConstants.LOCKACTION] = false;
                        ViewData[JudgeRecordConstants.ISPERMISSIONSUPERVISING] = false;
                    }
                }
                else
                {
                    ViewData[JudgeRecordConstants.LOCKACTION] = false;
                    ViewData[JudgeRecordConstants.ISPERMISSIONSUPERVISING] = false;
                }
            }



            ViewData[JudgeRecordConstants.HASPERMISION] = isPermissionSupervising;

            DateTime startDatePeriod;
            DateTime endDatePeriod;
            DateTime checkDatePeriod = DateTime.Now;

            startDatePeriod = periodDeclaration.FromDate.Value;
            endDatePeriod = periodDeclaration.EndDate.Value;
            // nếu là admin trường thì enable checkbox nhập điểm cho cả thời gian ngoài đợt
            if (global.IsAdminSchoolRole)
            {
                //ViewData[JudgeRecordConstants.CHECKTIMEOUTOFPERIOD] = true;
                ViewData[JudgeRecordConstants.HASPERMISION] = true;
            }
            // nếu là giáo viên bộ môn chỉ được enable checkbox nhập điểm trong đợt chưa kết thúc
            else
            {
                if (checkDatePeriod >= startDatePeriod && checkDatePeriod <= endDatePeriod && periodDeclaration.IsLock == false)
                {
                    //ViewData[JudgeRecordConstants.CHECKTIMEOUTOFPERIOD] = true;
                    ViewData[JudgeRecordConstants.HASPERMISION] = true;
                }
                else
                {
                    //ViewData[JudgeRecordConstants.CHECKTIMEOUTOFPERIOD] = false;
                    ViewData[JudgeRecordConstants.HASPERMISION] = false;
                }
            }
            // Kiem tra quyen gvbm de lock con diem bi khoa
            if (isPermissionSupervising)
            {
                if (global.IsAdminSchoolRole)
                {
                    ViewData[JudgeRecordConstants.CHECK_PEMISSION_GVBM] = false;
                }
                else
                {
                    ViewData[JudgeRecordConstants.CHECK_PEMISSION_GVBM] = true;
                }
            }
            else
            {
                ViewData[JudgeRecordConstants.CHECK_PEMISSION_GVBM] = true;
            }

            //Kiem tra khoa nhap lieu
            LockInputSupervisingDept objLockInput = this.GetLockTitleBySupervisingDept(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value);
            bool isLockInput = false;
            if (objLockInput != null && !string.IsNullOrEmpty(objLockInput.LockTitle))
            {
                isLockInput = objLockInput.LockTitle.Contains("HK" + semesterid);
            }
            ViewData[JudgeRecordConstants.IS_LOCK_INPUT] = isLockInput;

            // Tạo dữ liệu ghi log
            String subjectName = (subjectCat != null) ? subjectCat.SubjectName : String.Empty;
            String className = (classProfile != null) ? classProfile.DisplayName : String.Empty;
            SetViewDataActionAudit(String.Empty, String.Empty,
                classid.ToString(),
                "View Judge_record class_id: " + classid + ", subject_id: " + subjectid + ", semester: " + semesterid,
                "class_id: " + classid + ", subject_id: " + subjectid + ", semester: " + semesterid + ", periodDeclarationid: " + periodid,
                "Sổ điểm", SMAS.Business.Common.GlobalConstants.ACTION_VIEW,
                "Xem sổ điểm lớp " + className + " học kì " + semesterid + " đợt \"" + periodDeclaration.Resolution + "\" môn học " + subjectName);


            string tmpStrLockMark = MarkRecordBusiness.GetLockMarkTitle(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, classid, semesterid, subjectid);
            ViewData[JudgeRecordConstants.LIST_LOCK_TITLE] = tmpStrLockMark.Split(new Char[] { ',' }).ToList();

            ViewData[JudgeRecordConstants.TITLE_STRING] = String.Format("sổ điểm môn <span style='color:#d56900 '>{0}</span> học kỳ <span style='color:#d56900 '>{1}</span> đợt <span style='color:#d56900 '>{2}</span>, lớp <span style='color:#d56900 '>{3}</span>",
                subjectCat != null ? subjectCat.DisplayName : String.Empty, semesterid.ToString(), periodDeclaration.Resolution, classProfile.DisplayName);

            return PartialView("_List");
        }

        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJudgeRecord(FormCollection col)
        {
            string keyNames = "";
            try
            {

                bool isDBChange = false;
                var listKeySelect = col.AllKeys.Where(o => o.Contains("rptJudgeRecord_"));

                StringBuilder listjudge = new StringBuilder();
                if (listKeySelect != null && listKeySelect.Count() > 0)
                {
                    listKeySelect.ToList().ForEach(u => listjudge.Append("," + u.Replace("rptJudgeRecord_", "")));
                }

                var listjudgerecord = listjudge.ToString();
                if (string.IsNullOrWhiteSpace(listjudgerecord))
                {
                    return Json(new
                    {
                        Message = "Thầy/cô chưa chọn học sinh để lưu điểm",
                        Type = "success",
                        isDBChange = isDBChange
                    });
                }
                List<string> lstChangeMark = new List<string>();
                // Tạo data ghi log
                StringBuilder objectIDStr = new StringBuilder();
                StringBuilder descriptionStr = new StringBuilder();
                StringBuilder paramsStr = new StringBuilder();
                StringBuilder oldObjectStr = new StringBuilder();
                StringBuilder newObjectStr = new StringBuilder();
                StringBuilder userFuntionsStr = new StringBuilder();
                StringBuilder userActionsStr = new StringBuilder();
                StringBuilder userDescriptionsStr = new StringBuilder();
                StringBuilder isInsertLogstr = new StringBuilder();
                StringBuilder inforLog = null;
                StringBuilder oldobjtmp = null;
                bool isInsertLog = false;

                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                StringBuilder isInsertLogstrtmp = new StringBuilder();
                int iLog = 0;
                List<OldMark> lstOldMark = new List<OldMark>();

                var global = new GlobalInfo();
                var AcademicYearID = global.AcademicYearID;
                var SchoolID = global.SchoolID;
                int classid = string.IsNullOrWhiteSpace(Request["txtclassid"]) ? 0 : int.Parse(Request["txtclassid"]);
                int subjectid = string.IsNullOrWhiteSpace(Request["txtsubjectid"]) ? 0 : int.Parse(Request["txtsubjectid"]);
                int periodid = string.IsNullOrWhiteSpace(Request["txtperiodid"]) ? 0 : int.Parse(Request["txtperiodid"]);
                int semesterid = string.IsNullOrWhiteSpace(Request["txtsemesterid"]) ? (int)0 : int.Parse(Request["txtsemesterid"]);

                //Lock action de tranh giao dich dong thoi
                List<string> lstKeyName = new List<string>();
                lstKeyName.Add("MarkRecord");
                lstKeyName.Add(_globalInfo.UserAccountID.ToString());
                lstKeyName.Add(classid.ToString());//
                lstKeyName.Add(semesterid.ToString());
                lstKeyName.Add(subjectid.ToString());
                lstKeyName.Add(periodid.ToString());

                keyNames = VTUtils.LockAction.LockManager.CreatedKey(lstKeyName);
                if (!VTUtils.LockAction.LockManager.Lock(keyNames, Session.SessionID))
                {
                    return Json(new
                    {
                        Message = Res.Get("MarkRecord_Label_SavingMark"),
                        Type = JsonMessage.ERROR,
                        isDBChange = isDBChange
                    });
                }

                string lockTitle = Request["LockTitle"];
                // Kiem tra co mo khoa diem hay khong de khong bi xoa nham diem vua duoc mo khoa
                LockedMarkDetailBusiness.CheckCurrentLockMark(lockTitle, new Dictionary<string, object>()
                {
                    {"Semester", semesterid},
                    {"ClassID", classid},
                    {"SubjectID", subjectid},
                    {"SchoolID", SchoolID.Value},
                    {"AcademicYearID", AcademicYearID},
                });

                AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID.Value);
                bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);
                #region Load thông tin phục vụ việc tính toán con điểm
                //Load tiêu đề hiển thị con điểm: gọi hàm MarkTypeBusiness.Search() với điều kiện truyền vào AppliedLevel = UserInfo.AppliedLevel
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["AppliedLevel"] = global.AppliedLevel;
                var listMarkType = MarkTypeBusiness.Search(SearchInfo).ToList();
                MarkType markTypeM = listMarkType.Where(u => u.Title == "M").SingleOrDefault();
                MarkType markTypeP = listMarkType.Where(u => u.Title == "P").SingleOrDefault();
                MarkType markTypeV = listMarkType.Where(u => u.Title == "V").SingleOrDefault();
                MarkType markTypeHK = listMarkType.Where(u => u.Title == "HK").SingleOrDefault();

                //Load thông tin số các con điểm M, P, V
                var PeriodDeclaration = PeriodDeclarationBusiness.Find(periodid);
                ClassSubject classSubject = ClassSubjectBusiness.SearchByClass(classid, new Dictionary<string, object> { { "SubjectID", subjectid }, { "SchoolID", SchoolID } }).SingleOrDefault();
                #endregion

                string[] pupilids = listjudgerecord.Split(new Char[] { ',' });
                //-	Thao tác trên những học sinh được check chọn và những con điểm không bị null
                List<int> listpupilid = new List<int>();
                List<JudgeRecord> ListJudgeRecord = new List<JudgeRecord>();
                List<SummedUpRecord> ListSummedUpRecord = new List<SummedUpRecord>();
                //1 ví dụ định dạng tên mỗi combobox chứa điểm là  8_M2_M
                //trong đó: 8 là ID của Pupil, M2 là Title của con điểm, M là Type của con điểm (có thể lấy dc từ Title, tuy nhiên nó để dùng cho xử lý ngoài view)
                //Thứ tự con điểm lấy từ Title của nó.
                string mark_format = "{0}_{1}";
                //1 ví dụ định dạng điểm trung bình là 8TBM
                //trong đó 8 là ID của Pupil
                //TBM là mã để nhận biết là combobox tính điểm TBM
                string tbm_format = "{0}TBM";
                JudgeRecord JudgeRecord = new JudgeRecord();
                SummedUpRecord SummedUpRecord = new SummedUpRecord();
                int PupilID = 0;
                List<string> lststrMarkM = !string.IsNullOrEmpty(PeriodDeclaration.StrInterviewMark) ? PeriodDeclaration.StrInterviewMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<string>();
                List<string> lststrMarkP = !string.IsNullOrEmpty(PeriodDeclaration.StrWritingMark) ? PeriodDeclaration.StrWritingMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<string>();
                List<string> lststrMarkV = !string.IsNullOrEmpty(PeriodDeclaration.StrTwiceCoeffiecientMark) ? PeriodDeclaration.StrTwiceCoeffiecientMark.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<string>();
                // Thong tin mien giam cua lop trong ky
                List<ExemptedSubject> listExemptedSubject = ExemptedSubjectBusiness.GetListExemptedSubject(classid, semesterid)
                    .Where(o => o.SubjectID == subjectid)
                    .ToList();
                List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object> { { "AcademicYearID", global.AcademicYearID }, { "ClassID", classid }, { "Check", "Check" } }).ToList();
                List<JudgeRecordHistory> listTempHistory = null;
                List<JudgeRecord> listTemp = null;
                int modSchoolID = global.SchoolID.Value % 100;
                if (isMovedHistory)
                {
                    var queryHis = from m in JudgeRecordHistoryBusiness.All
                                   where m.Last2digitNumberSchool == modSchoolID
                                   && m.AcademicYearID == global.AcademicYearID
                                   && m.ClassID == classid
                                   && m.SubjectID == subjectid
                                   && m.Semester == semesterid
                                   select m;
                    listTempHistory = queryHis.ToList();
                }
                else
                {
                    var query = from m in JudgeRecordBusiness.All
                                where m.Last2digitNumberSchool == modSchoolID
                                && m.AcademicYearID == global.AcademicYearID
                                && m.ClassID == classid
                                && m.SubjectID == subjectid
                                && m.Semester == semesterid
                                select m;
                    listTemp = query.ToList();
                }
                foreach (var pupilid in pupilids)
                {
                    isInsertLog = false;
                    lstOldMark = new List<OldMark>();
                    if (pupilid.Trim() == "")
                        continue;
                    PupilID = string.IsNullOrWhiteSpace(pupilid) ? 0 : int.Parse(pupilid);
                    // Neu mien giam thi bo qua
                    if (listExemptedSubject.Any(u => u.PupilID == PupilID))
                        continue;

                    // Tạo dữ liệu ghi log
                    PupilOfClass pop = lstPOC.Where(p => p.PupilID == Convert.ToInt32(pupilid)).FirstOrDefault();
                    inforLog = new StringBuilder();
                    oldobjtmp = new StringBuilder();
                    inforLog.Append("Cập nhật điểm HS " + pop.PupilProfile.FullName);
                    inforLog.Append(", mã " + pop.PupilProfile.PupilCode);
                    inforLog.Append(", Lớp " + pop.ClassProfile.DisplayName);
                    inforLog.Append("/" + classSubject.SubjectCat.SubjectName);
                    inforLog.Append("/Học kỳ " + semesterid);
                    inforLog.Append("/Đợt " + PeriodDeclaration.Resolution);
                    inforLog.Append("/" + pop.Year.Value + "-" + (pop.Year.Value + 1));
                    newObjectStrtmp = new StringBuilder();
                    if (isMovedHistory)
                    {
                        if (listTempHistory != null)
                        {
                            List<JudgeRecordHistory> listOldRecord = listTempHistory.Where(p => p.PupilID == Convert.ToInt32(pupilid)).ToList();
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listOldRecord.Count; j++)
                            {
                                OldMark objOldMark = new OldMark();
                                JudgeRecordHistory record = listOldRecord[j];
                                if (col["isChangeJudgePeriod_" + PupilID + record.Title] != null && "1".Equals(col["isChangeJudgePeriod_" + PupilID + record.Title]))
                                {
                                    oldobjtmp.Append(record.Title + ":" + record.Judgement);
                                    isInsertLog = true;
                                    objOldMark.JudgeRecordID = record.JudgeRecordID;
                                    objOldMark.Title = record.Title;
                                    objOldMark.Judgement = record.Judgement;
                                    lstOldMark.Add(objOldMark);
                                    if (j < listOldRecord.Count - 1)
                                    {
                                        oldobjtmp.Append(", ");
                                    }
                                }
                            }
                            oldObjectStr.Append("}");
                            // end
                        }
                    }
                    else
                    {
                        if (listTemp != null)
                        {
                            List<JudgeRecord> listOldRecord = listTemp.Where(p => p.PupilID == Convert.ToInt32(pupilid)).ToList();
                            oldobjtmp.Append("(Giá trị trước khi sửa): {");
                            for (int j = 0; j < listOldRecord.Count; j++)
                            {
                                JudgeRecord record = listOldRecord[j];
                                OldMark objOldMark = new OldMark();
                                if (col["isChangeJudgePeriod_" + PupilID + record.Title] != null && "1".Equals(col["isChangeJudgePeriod_" + PupilID + record.Title]))
                                {
                                    oldobjtmp.Append(record.Title + ":" + record.Judgement);
                                    isInsertLog = true;
                                    objOldMark.JudgeRecordID = record.JudgeRecordID;
                                    objOldMark.Title = record.Title;
                                    objOldMark.Judgement = record.Judgement;
                                    lstOldMark.Add(objOldMark);
                                    if (j < listOldRecord.Count - 1)
                                    {
                                        oldobjtmp.Append(", ");
                                    }
                                }
                            }
                            oldobjtmp.Append("}");
                        }
                        //// end
                    }
                    descriptionStrtmp.Append("Update mark record pupil_id:" + PupilID);
                    paramsStrtmp.Append("pupil_id:" + PupilID);
                    userFuntionsStrtmp.Append("Sổ điểm");
                    userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                    userDescriptionsStrtmp.Append(inforLog.ToString()).Append(": ");
                    objectIDStrtmp.Append(PupilID);
                    oldObjectStrtmp.Append(oldobjtmp);
                    newObjectStrtmp.Append("(Giá trị sau khi sửa): {");
                    OldMark objOM = null;

                    for (int i = 0; i < lststrMarkM.Count; i++)
                    {
                        var strname = string.Format(mark_format, PupilID.ToString(), lststrMarkM[i]);

                        if (col[strname] != "" && col[strname] != null && col[strname].ToString().Contains("-1"))
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                        JudgeRecord = new JudgeRecord();
                        int orderNumber = 1;
                        if (!String.IsNullOrEmpty(lststrMarkM[i]) && lststrMarkM[i] != "HK")
                        {
                            int.TryParse(Regex.Match(lststrMarkM[i], @"\d+").Value, out orderNumber);
                        }

                        #region Assign Data for JudgeRecord item
                        JudgeRecord.AcademicYearID = AcademicYearID.Value;
                        JudgeRecord.PupilID = PupilID;
                        JudgeRecord.ClassID = classid;
                        JudgeRecord.SchoolID = SchoolID.Value;
                        JudgeRecord.SubjectID = subjectid;
                        JudgeRecord.MarkTypeID = markTypeM.MarkTypeID;
                        JudgeRecord.CreatedAcademicYear = objAcademicYear.Year;
                        JudgeRecord.Semester = semesterid;
                        JudgeRecord.Title = lststrMarkM[i];
                        if ("1".Equals(col["isChangeJudgePeriod_" + PupilID + JudgeRecord.Title]))
                        {
                            //Danh dau cac o da luu
                            lstChangeMark.Add(col["isChangeJudgePeriod_" + PupilID + JudgeRecord.Title]);
                            if (lstOldMark.Count > 0)
                            {
                                objOM = lstOldMark.Where(p => p.Title.Equals(JudgeRecord.Title)).FirstOrDefault();
                                if (objOM != null)
                                {
                                    userDescriptionsStrtmp.Append(JudgeRecord.Title + "(" + objOM.Judgement + ":");
                                    JudgeRecord.JudgeRecordID = objOM.JudgeRecordID;
                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                                }
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                            }
                            if (!string.IsNullOrWhiteSpace(col[strname]))
                            {
                                JudgeRecord.Judgement = col[strname].ToString();
                                userDescriptionsStrtmp.Append(JudgeRecord.Judgement + "); ");
                                newObjectStrtmp.Append(JudgeRecord.Title + ":" + JudgeRecord.Judgement);

                            }
                            else
                            {
                                userDescriptionsStrtmp.Append("); ");
                                newObjectStrtmp.Append(JudgeRecord.Title + ":");
                                JudgeRecord.Judgement = "-1";
                            }
                            newObjectStrtmp.Append(", ");
                            isInsertLog = true;
                        }
                        else
                        {
                            JudgeRecord.Judgement = "-1";
                        }
                        JudgeRecord.OrderNumber = orderNumber; //Convert.ToByte(i + 1);
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;
                        #endregion
                        ListJudgeRecord.Add(JudgeRecord);
                    }
                    for (var i = 0; i < lststrMarkP.Count; i++)
                    {
                        var strname = string.Format(mark_format, PupilID.ToString(), lststrMarkP[i]);

                        if (col[strname] != "" && col[strname] != null && col[strname].ToString().Contains("-1"))
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                        JudgeRecord = new JudgeRecord();
                        int orderNumber = 1;
                        if (!String.IsNullOrEmpty(lststrMarkP[i]) && lststrMarkP[i] != "HK")
                        {
                            int.TryParse(Regex.Match(lststrMarkP[i], @"\d+").Value, out orderNumber);
                        }
                        #region Assign Data for JudgeRecord item
                        JudgeRecord.AcademicYearID = AcademicYearID.Value;
                        JudgeRecord.PupilID = PupilID;
                        JudgeRecord.ClassID = classid;
                        JudgeRecord.SchoolID = SchoolID.Value;
                        JudgeRecord.SubjectID = subjectid;
                        JudgeRecord.MarkTypeID = markTypeP.MarkTypeID;
                        JudgeRecord.CreatedAcademicYear = objAcademicYear.Year;
                        JudgeRecord.Semester = semesterid;
                        JudgeRecord.Title = lststrMarkP[i];
                        if ("1".Equals(col["isChangeJudgePeriod_" + PupilID + JudgeRecord.Title]))
                        {
                            lstChangeMark.Add(col["isChangeJudgePeriod_" + PupilID + JudgeRecord.Title]);
                            if (lstOldMark.Count > 0)
                            {
                                objOM = lstOldMark.Where(p => p.Title.Equals(JudgeRecord.Title)).FirstOrDefault();
                                if (objOM != null)
                                {
                                    userDescriptionsStrtmp.Append(JudgeRecord.Title + "(" + objOM.Judgement + ":");
                                    JudgeRecord.JudgeRecordID = objOM.JudgeRecordID;
                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                                }
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                            }
                            if (!string.IsNullOrWhiteSpace(col[strname]))
                            {
                                JudgeRecord.Judgement = col[strname].ToString();
                                userDescriptionsStrtmp.Append(JudgeRecord.Judgement + "); ");
                                newObjectStrtmp.Append(JudgeRecord.Title + ":" + JudgeRecord.Judgement);

                            }
                            else
                            {
                                userDescriptionsStrtmp.Append("); ");
                                newObjectStrtmp.Append(JudgeRecord.Title + ":");
                                JudgeRecord.Judgement = "-1";
                            }
                            newObjectStrtmp.Append(", ");
                            isInsertLog = true;
                        }
                        else
                        {
                            JudgeRecord.Judgement = "-1";
                        }
                        JudgeRecord.OrderNumber = orderNumber; //Convert.ToByte(i + 1);
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;

                        #endregion
                        ListJudgeRecord.Add(JudgeRecord);
                    }
                    for (var i = 0; i < lststrMarkV.Count; i++)
                    {
                        var strname = string.Format(mark_format, PupilID.ToString(), lststrMarkV[i]);

                        if (col[strname] != "" && col[strname] != null && col[strname].ToString().Contains("-1"))
                        {
                            throw new BusinessException("Common_Validate_NotCompatible");
                        }
                        int orderNumber = 1;
                        if (!String.IsNullOrEmpty(lststrMarkV[i]) && lststrMarkV[i] != "HK")
                        {
                            int.TryParse(Regex.Match(lststrMarkV[i], @"\d+").Value, out orderNumber);
                        }

                        JudgeRecord = new JudgeRecord();
                        #region Assign Data for JudgeRecord item
                        JudgeRecord.AcademicYearID = AcademicYearID.Value;
                        JudgeRecord.PupilID = PupilID;
                        JudgeRecord.ClassID = classid;
                        JudgeRecord.SchoolID = SchoolID.Value;
                        JudgeRecord.SubjectID = subjectid;
                        JudgeRecord.MarkTypeID = markTypeV.MarkTypeID;
                        JudgeRecord.CreatedAcademicYear = objAcademicYear.Year;
                        JudgeRecord.Semester = semesterid;
                        JudgeRecord.Title = lststrMarkV[i];
                        if ("1".Equals(col["isChangeJudgePeriod_" + PupilID + JudgeRecord.Title]))
                        {
                            lstChangeMark.Add(col["isChangeJudgePeriod_" + PupilID + JudgeRecord.Title]);
                            if (lstOldMark.Count > 0)
                            {
                                objOM = lstOldMark.Where(p => p.Title.Equals(JudgeRecord.Title)).FirstOrDefault();
                                if (objOM != null)
                                {
                                    userDescriptionsStrtmp.Append(JudgeRecord.Title + "(" + objOM.Judgement + ":");
                                    JudgeRecord.JudgeRecordID = objOM.JudgeRecordID;
                                }
                                else
                                {
                                    userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                                }
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                            }
                            if (!string.IsNullOrWhiteSpace(col[strname]))
                            {
                                JudgeRecord.Judgement = col[strname].ToString();
                                userDescriptionsStrtmp.Append(JudgeRecord.Judgement + "); ");
                                newObjectStrtmp.Append(JudgeRecord.Title + ":" + JudgeRecord.Judgement);

                            }
                            else
                            {
                                userDescriptionsStrtmp.Append("); ");
                                newObjectStrtmp.Append(JudgeRecord.Title + ":");
                                JudgeRecord.Judgement = "-1";
                            }
                            newObjectStrtmp.Append(", ");
                            isInsertLog = true;
                        }
                        else
                        {
                            JudgeRecord.Judgement = "-1";
                        }
                        JudgeRecord.OrderNumber = orderNumber; //Convert.ToByte(i + 1);
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;

                        #endregion
                        ListJudgeRecord.Add(JudgeRecord);
                    }
                    var strhkname = string.Format(mark_format, PupilID.ToString(), "HK");

                    if (col[strhkname] != "" && col[strhkname] != null && col[strhkname].ToString().Contains("-1"))
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    JudgeRecord = new JudgeRecord();
                    #region Assign Data for JudgeRecord item
                    JudgeRecord.AcademicYearID = AcademicYearID.Value;
                    JudgeRecord.PupilID = PupilID;
                    JudgeRecord.ClassID = classid;
                    JudgeRecord.SchoolID = SchoolID.Value;
                    JudgeRecord.SubjectID = subjectid;
                    JudgeRecord.MarkTypeID = markTypeHK.MarkTypeID;
                    JudgeRecord.CreatedAcademicYear = objAcademicYear.Year;
                    JudgeRecord.Semester = semesterid;
                    JudgeRecord.Title = "HK";
                    if ("1".Equals(col["isChangeJudgePeriod_" + PupilID + JudgeRecord.Title]))
                    {
                        lstChangeMark.Add(col["isChangeJudgePeriod_" + PupilID + JudgeRecord.Title]);
                        if (lstOldMark.Count > 0)
                        {
                            objOM = lstOldMark.Where(p => p.Title.Equals(JudgeRecord.Title)).FirstOrDefault();
                            if (objOM != null)
                            {
                                userDescriptionsStrtmp.Append(JudgeRecord.Title + "(" + objOM.Judgement + ":");
                                JudgeRecord.JudgeRecordID = objOM.JudgeRecordID;
                            }
                            else
                            {
                                userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                            }
                        }
                        else
                        {
                            userDescriptionsStrtmp.Append(JudgeRecord.Title + "(");
                        }
                        if (!string.IsNullOrWhiteSpace(col[strhkname]))
                        {
                            JudgeRecord.Judgement = col[strhkname].ToString();
                            userDescriptionsStrtmp.Append(JudgeRecord.Judgement + "); ");
                            newObjectStrtmp.Append(JudgeRecord.Title + ":" + JudgeRecord.Judgement);

                        }
                        else
                        {
                            userDescriptionsStrtmp.Append("); ");
                            newObjectStrtmp.Append(JudgeRecord.Title + ":");
                            JudgeRecord.Judgement = "-1";
                        }
                        newObjectStrtmp.Append(", ");
                        isInsertLog = true;
                    }
                    else
                    {
                        JudgeRecord.Judgement = "-1";
                    }
                    JudgeRecord.MarkedDate = DateTime.Now;
                    JudgeRecord.CreatedDate = DateTime.Now;
                    JudgeRecord.ModifiedDate = DateTime.Now;
                    #endregion
                    ListJudgeRecord.Add(JudgeRecord);
                    var strname_tbm = string.Format(tbm_format, PupilID.ToString());
                    if (col[strname_tbm] != "" && col[strname_tbm] != null && col[strname_tbm].ToString().Contains("-1"))
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    SummedUpRecord = new SummedUpRecord();
                    SummedUpRecord.AcademicYearID = AcademicYearID.Value;
                    SummedUpRecord.ClassID = classid;
                    SummedUpRecord.PeriodID = periodid;
                    SummedUpRecord.PupilID = PupilID;
                    SummedUpRecord.SchoolID = SchoolID.Value;
                    SummedUpRecord.Semester = (int)semesterid;
                    SummedUpRecord.SubjectID = (int)subjectid;
                    SummedUpRecord.SummedUpDate = DateTime.Now;
                    if (!string.IsNullOrWhiteSpace(col[strname_tbm]))
                    {
                        SummedUpRecord.JudgementResult = col[strname_tbm].ToString();
                    }
                    else
                    {
                        SummedUpRecord.JudgementResult = "-1";
                    }
                    SummedUpRecord.IsCommenting = classSubject.IsCommenting.Value;
                    SummedUpRecord.CreatedAcademicYear = objAcademicYear.Year;
                    ListSummedUpRecord.Add(SummedUpRecord);
                    // Tạo dữ liệu ghi log
                    if (isInsertLog)
                    {
                        string newObj = string.Empty;
                        newObj = newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) + "}";
                        newObj += SMAS.Business.Common.GlobalConstants.WILD_LOG;
                        string tmp = string.Empty;
                        objectIDStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        oldObjectStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFuntionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        tmp = userDescriptionsStrtmp != null ? userDescriptionsStrtmp.ToString().Substring(0, userDescriptionsStrtmp.Length - 2) : "";
                        tmp += SMAS.Business.Common.GlobalConstants.WILD_LOG;
                        objectIDStr.Append(objectIDStrtmp);
                        descriptionStr.Append(descriptionStrtmp);
                        paramsStr.Append(paramsStrtmp);
                        oldObjectStr.Append(oldObjectStrtmp);
                        newObjectStr.Append(newObj);
                        userFuntionsStr.Append(userFuntionsStrtmp);
                        userActionsStr.Append(userActionsStrtmp);
                        userDescriptionsStr.Append(tmp);
                    }
                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                    isInsertLogstrtmp = new StringBuilder();
                    iLog++;
                }

                if (isMovedHistory)
                {
                    List<JudgeRecordHistory> lstJudgeRecordHistory = new List<JudgeRecordHistory>();
                    List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                    JudgeRecordHistory objJudgeRecordHistory = null;
                    JudgeRecord objJudgeRecord = null;
                    SummedUpRecord objSummedUpRecord = null;
                    SummedUpRecordHistory objSummedUpRecordHistory = null;
                    for (int i = 0; i < ListJudgeRecord.Count; i++)
                    {
                        objJudgeRecordHistory = new JudgeRecordHistory();
                        objJudgeRecord = ListJudgeRecord[i];
                        objJudgeRecordHistory.JudgeRecordID = objJudgeRecord.JudgeRecordID;
                        objJudgeRecordHistory.AcademicYearID = objJudgeRecord.AcademicYearID;
                        objJudgeRecordHistory.PupilID = objJudgeRecord.PupilID;
                        objJudgeRecordHistory.SchoolID = objJudgeRecord.SchoolID;
                        objJudgeRecordHistory.ClassID = objJudgeRecord.ClassID;
                        objJudgeRecordHistory.Judgement = objJudgeRecord.Judgement;
                        objJudgeRecordHistory.MarkTypeID = objJudgeRecord.MarkTypeID;
                        objJudgeRecordHistory.Semester = objJudgeRecord.Semester;
                        objJudgeRecordHistory.Title = objJudgeRecord.Title;
                        objJudgeRecordHistory.Last2digitNumberSchool = objJudgeRecord.Last2digitNumberSchool;
                        objJudgeRecordHistory.MarkedDate = objJudgeRecord.MarkedDate;
                        objJudgeRecordHistory.ModifiedDate = objJudgeRecord.ModifiedDate;
                        objJudgeRecordHistory.SubjectID = objJudgeRecord.SubjectID;
                        objJudgeRecordHistory.CreatedDate = objJudgeRecord.CreatedDate;
                        objJudgeRecordHistory.CreatedAcademicYear = objJudgeRecord.CreatedAcademicYear;
                        objJudgeRecordHistory.OrderNumber = objJudgeRecord.OrderNumber;
                        lstJudgeRecordHistory.Add(objJudgeRecordHistory);
                    }
                    for (int i = 0; i < ListSummedUpRecord.Count; i++)
                    {
                        objSummedUpRecordHistory = new SummedUpRecordHistory();
                        objSummedUpRecord = ListSummedUpRecord[i];
                        objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                        objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                        objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                        objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                        objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                        objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                        objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                        objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                        objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                        objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                        objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                        objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                        objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                        objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                        objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                        lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                    }
                    isDBChange = JudgeRecordHistoryBusiness.InsertJudgeRecordHistory(global.UserAccountID, lstJudgeRecordHistory, lstSummedUpRecordHistory, semesterid, periodid, null, global.SchoolID.Value, global.AcademicYearID.Value, classid, subjectid, _globalInfo.EmployeeID);
                    //JudgeRecordBusiness.Save();
                }
                else
                {
                    isDBChange = JudgeRecordBusiness.InsertJudgeRecord(global.UserAccountID, ListJudgeRecord, ListSummedUpRecord, semesterid, periodid, _globalInfo.EmployeeID, null, global.SchoolID.Value, global.AcademicYearID.Value, classid, subjectid);
                    //JudgeRecordBusiness.Save();
                }


                ///Luu danh sach cac o diem thay doi
                ViewData[JudgeRecordConstants.LIST_CHANGE_MARK] = lstChangeMark;
                // Tạo dữ liệu ghi log
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObjectStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,newObjectStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectIDStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActionsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFuntionsStr.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptionsStr.ToString()}
                };
                SetViewDataActionAudit(dicLog);
                //SetViewDataActionAudit(oldObjectStr.ToString(), newObjectStr.ToString(), objectIDStr.ToString(), descriptionStr.ToString(), paramsStr.ToString(), userFuntionsStr.ToString(), userActionsStr.ToString(), userDescriptionsStr.ToString());

                return Json(new
                {
                    Message = Res.Get("Common_Label_UpdateMarkSuccess"),
                    Type = "success",
                    isDBChange = isDBChange
                });
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "CreateJudgeRecord", "",ex);

                return Json(new
                {
                    Message = "Lỗi trong quá trình thực hiện lưu điểm",
                    Type = "success",
                    isDBChange = false
                });
            }
            finally
            {
                //Giai phong log action
                VTUtils.LockAction.LockManager.ReleaseLock(keyNames, Session.SessionID);
            }
        }

        [HttpPost]
        [ActionAudit]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(FormCollection col)
        {
            var listKeySelect = col.AllKeys.Where(o => o.Contains("rptJudgeRecord_"));

            StringBuilder listjudge = new StringBuilder();
            if (listKeySelect != null && listKeySelect.Count() > 0)
            {
                listKeySelect.ToList().ForEach(u => listjudge.Append("," + u.Replace("rptJudgeRecord_", "")));
            }

            var listjudgerecord = listjudge.ToString();
            string[] pupilids = listjudgerecord.Split(new Char[] { ',' }).Where(u => !string.IsNullOrEmpty(u)).ToArray();

            if (pupilids.Length <= 0)
                return Json(new JsonMessage("Common_Validate_NoPupilSelected", JsonMessage.ERROR));

            var global = new GlobalInfo();
            int classid = string.IsNullOrWhiteSpace(Request["txtclassid"]) ? 0 : int.Parse(Request["txtclassid"]);
            int subjectid = string.IsNullOrWhiteSpace(Request["txtsubjectid"]) ? 0 : int.Parse(Request["txtsubjectid"]);
            int periodid = string.IsNullOrWhiteSpace(Request["txtperiodid"]) ? 0 : int.Parse(Request["txtperiodid"]);
            int semesterid = string.IsNullOrWhiteSpace(Request["txtsemesterid"]) ? (int)0 : int.Parse(Request["txtsemesterid"]);
            AcademicYear acaYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);
            if (classid <= 0 || subjectid <= 0 || periodid <= 0 || semesterid <= 0)
                return Json(new JsonMessage("Common_Validate_DataInvalid", JsonMessage.ERROR));

            List<int> lstPupilIDs = pupilids.Select(u => int.Parse(u)).ToList();

            // Tạo dữ liệu ghi log
            List<PupilProfile> listPupilProfile = PupilProfileBusiness.All.Where(o => lstPupilIDs.Contains(o.PupilProfileID)).ToList();
            ClassProfile classProfile = ClassProfileBusiness.Find(classid);
            SubjectCat subject = SubjectCatBusiness.Find(subjectid);
            PeriodDeclaration period = PeriodDeclarationBusiness.Find(periodid);
            StringBuilder oldObject = new StringBuilder();
            StringBuilder objectID = new StringBuilder();
            StringBuilder descriptionObject = new StringBuilder();
            StringBuilder paramObject = new StringBuilder();
            StringBuilder userFunctions = new StringBuilder();
            StringBuilder userActions = new StringBuilder();
            StringBuilder userDescriptions = new StringBuilder();
            StringBuilder userDescriptionstmp = new StringBuilder();
            StringBuilder oldObjecttmp = new StringBuilder();
            StringBuilder markStr = null;
            int modSchoolID = global.SchoolID.Value % 100;

            List<string> markTitle = new List<string>();
            string strAllTitle = period.StrInterviewMark + period.StrWritingMark + period.StrTwiceCoeffiecientMark;

            if (period.ContaintSemesterMark.HasValue && period.ContaintSemesterMark.Value)
            {
                strAllTitle += "HK";
            }

            markTitle = !string.IsNullOrEmpty(strAllTitle) ? strAllTitle.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<string>();

            var query = from m in JudgeRecordBusiness.All
                        where lstPupilIDs.Contains(m.PupilID) && m.Last2digitNumberSchool == modSchoolID
                        && m.AcademicYearID == global.AcademicYearID && markTitle.Contains(m.Title)
                        && m.ClassID == classid
                        && m.SubjectID == subjectid
                        && m.Semester == semesterid
                        orderby m.Title, m.MarkTypeID
                        select m;
            var queryHis = from m in JudgeRecordHistoryBusiness.All
                           where lstPupilIDs.Contains(m.PupilID) && m.Last2digitNumberSchool == modSchoolID
                           && m.AcademicYearID == global.AcademicYearID && markTitle.Contains(m.Title)
                           && m.ClassID == classid
                           && m.SubjectID == subjectid
                           && m.Semester == semesterid
                           orderby m.Title, m.MarkTypeID
                           select m;
            List<JudgeRecordHistory> allMarksHis = null;
            List<JudgeRecord> allMarks = null;
            if (isMovedHistory)
            {
                allMarksHis = queryHis.ToList();
            }
            else
            {
                allMarks = query.ToList();
            }
            bool isCheckDelete = false;
            List<int> lstPupilDeleteID = new List<int>();

            ClassSubject classSubject = ClassSubjectBusiness.All.FirstOrDefault(s => s.ClassID == classid && s.SubjectID == subjectid && s.Last2digitNumberSchool == modSchoolID);

            UserInfoBO userInfo = GlobalInfo.getInstance().GetUserLogin(User.Identity.Name);
            RESTORE_DATA objRes = new RESTORE_DATA
            {
                ACADEMIC_YEAR_ID = acaYear.AcademicYearID,
                SCHOOL_ID = acaYear.SchoolID,
                DELETED_DATE = DateTime.Now,
                DELETED_FULLNAME = userInfo.FullName,
                DELETED_USER = userInfo.UserName,
                RESTORED_DATE = DateTime.MinValue,
                RESTORED_STATUS = 0,
                RESTORE_DATA_ID = Guid.NewGuid(),
                RESTORE_DATA_TYPE_ID = RestoreDataConstant.RESTORE_DATA_TYPE_MARK,
                SHORT_DESCRIPTION = string.Format("Điểm HS lớp: {0}, học kỳ:{1}, môn: {2}, ", classProfile.DisplayName, semesterid, subject.DisplayName),
            };

            if (period != null)
            {
                objRes.SHORT_DESCRIPTION += string.Format(" đợt:{0}, ", period.Resolution);
            }
            objRes.SHORT_DESCRIPTION += string.Format(" năm học: {0}, ", classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1));


            List<RESTORE_DATA_DETAIL> lstRestoreDetail = new List<RESTORE_DATA_DETAIL>();

            for (int i = 0, size = listPupilProfile.Count; i < size; i++)
            {
                isCheckDelete = false;
                oldObjecttmp = new StringBuilder();
                oldObjecttmp.Append("Giá trị trước khi xóa: ");
                string strHK = string.Empty;
                if (isMovedHistory)
                {
                    if (allMarksHis != null)
                    {
                        JudgeRecordHistory mark = null;
                        markStr = new StringBuilder();
                        List<JudgeRecordHistory> marks = allMarksHis.Where(a => a.PupilID == listPupilProfile[i].PupilProfileID).ToList();
                        if (marks.Count > 0)
                        {
                            for (int j = 0; j < marks.Count; j++)
                            {
                                mark = marks[j];
                                if ("HK".Equals(mark.Title))
                                {
                                    strHK = mark.Title + ":" + mark.Judgement + "; ";
                                }
                                else
                                {
                                    oldObjecttmp.Append(mark.Title + ":" + mark.Judgement);
                                    oldObjecttmp.Append("; ");
                                }
                            }
                            oldObjecttmp.Append(strHK);
                            isCheckDelete = true;
                            lstPupilDeleteID.Add(listPupilProfile[i].PupilProfileID);
                        }
                    }
                }
                else
                {
                    if (allMarks != null)
                    {
                        JudgeRecord mark = null;
                        markStr = new StringBuilder();
                        List<JudgeRecord> marks = allMarks.Where(a => a.PupilID == listPupilProfile[i].PupilProfileID).ToList();
                        if (marks.Count > 0)
                        {
                            RESTORE_DATA_DETAIL objRestoreDetail;
                            for (int j = 0; j < marks.Count; j++)
                            {
                                mark = marks[j];
                                if ("HK".Equals(mark.Title))
                                {
                                    strHK = mark.Title + ":" + mark.Judgement + "; ";
                                }
                                else
                                {
                                    oldObjecttmp.Append(mark.Title + ":" + mark.Judgement);
                                    oldObjecttmp.Append("; ");
                                }

                                #region Luu thong tin phuc vu phuc hoi du lieu

                                //Luu thong tin khoi phuc diem
                                KeyDelMarkBO objKeyMark = new KeyDelMarkBO
                                {
                                    AcademicYearID = mark.AcademicYearID,
                                    ClassID = mark.ClassID,
                                    MarkTypeID = mark.MarkTypeID,
                                    PeriodID = mark.PeriodID,
                                    PupilID = mark.PupilID,
                                    SchoolID = mark.SchoolID,
                                    Semester = mark.Semester,
                                    SubjectID = mark.SubjectID
                                };

                                BakJudgeRecordBO objDelMark = new BakJudgeRecordBO
                                {
                                    AcademicYearID = mark.AcademicYearID,
                                    ClassID = mark.ClassID,
                                    CreatedAcademicYear = mark.CreatedAcademicYear,
                                    CreatedDate = mark.CreatedDate,
                                    IsOldData = mark.IsOldData,
                                    Last2digitNumberSchool = mark.Last2digitNumberSchool,
                                    LogChange = mark.LogChange,
                                    MarkedDate = mark.MarkedDate,
                                    MarkTypeID = mark.MarkTypeID,
                                    ModifiedDate = mark.ModifiedDate,
                                    MSourcedb = mark.MSourcedb,
                                    M_OldID = mark.M_OldID,
                                    OrderNumber = mark.OrderNumber,
                                    PeriodID = mark.PeriodID,
                                    PupilID = mark.PupilID,
                                    SchoolID = mark.SchoolID,
                                    Semester = mark.Semester,
                                    SubjectID = mark.SubjectID,
                                    SynchronizeID = mark.SynchronizeID,
                                    Title = mark.Title,
                                    Year = mark.Year,
                                    IsSMS = false,
                                    Judgement = mark.Judgement,
                                    JudgeRecordID = mark.JudgeRecordID,
                                    MJudgement = mark.MJudgement,
                                    M_ProvinceID = mark.M_ProvinceID,
                                    OldJudgement = mark.OldJudgement,
                                    ReTestJudgement = mark.ReTestJudgement
                                };
                                objRestoreDetail = new RESTORE_DATA_DETAIL
                                {
                                    ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                                    CREATED_DATE = DateTime.Now,
                                    END_DATE = null,
                                    IS_VALIDATE = 1,
                                    LAST_2DIGIT_NUMBER_SCHOOL = mark.Last2digitNumberSchool,
                                    ORDER_ID = 1,
                                    RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                                    RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                                    SCHOOL_ID = objRes.SCHOOL_ID,
                                    SQL_DELETE = JsonConvert.SerializeObject(objKeyMark),
                                    SQL_UNDO = JsonConvert.SerializeObject(objDelMark),
                                    TABLE_NAME = RestoreDataConstant.TABLE_JUDGE_RECORD
                                };

                                lstRestoreDetail.Add(objRestoreDetail);
                                #endregion
                            }
                            oldObjecttmp.Append(strHK);
                            isCheckDelete = true;
                            lstPupilDeleteID.Add(listPupilProfile[i].PupilProfileID);
                        }
                    }
                }
                if (isCheckDelete)
                {
                    userDescriptionstmp = new StringBuilder();
                    string tmp = string.Empty;
                    tmp = oldObjecttmp != null ? oldObjecttmp.ToString().Substring(0, oldObjecttmp.Length - 2) : "";
                    objectID.Append(listPupilProfile[i].PupilProfileID.ToString());
                    descriptionObject.Append("Delete mark_record:" + listPupilProfile[i].PupilProfileID.ToString());
                    paramObject.Append(listPupilProfile[i].PupilProfileID.ToString());
                    userFunctions.Append("Sổ điểm");
                    userActions.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);

                    userDescriptionstmp.Append("Xóa điểm HS " + listPupilProfile[i].FullName
                        + ", mã " + listPupilProfile[i].PupilCode
                        + ", Lớp " + classProfile.DisplayName
                        + "/" + subject.SubjectName
                        + "/Học kỳ " + semesterid
                        + "/Đợt " + period.Resolution
                        + "/" + classProfile.AcademicYear.Year + "-" + (classProfile.AcademicYear.Year + 1)
                        + ". " + tmp);
                    if (i < size - 1)
                    {
                        userDescriptionstmp.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        oldObject.Append(tmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionObject.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramObject.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        objectID.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFunctions.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActions.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    }
                    userDescriptions.Append(userDescriptionstmp);
                    objRes.SHORT_DESCRIPTION += string.Format(" HS: {0}, mã: {1}", listPupilProfile[i].FullName, listPupilProfile[i].PupilCode);
                }
            }
            // end
            if (isMovedHistory)
            {
                JudgeRecordHistoryBusiness.DeleteJudgeRecordHistory(global.UserAccountID, global.SchoolID.Value, global.AcademicYearID.Value, classid, semesterid, periodid, subjectid, lstPupilDeleteID);
                SummedUpRecordHistoryBusiness.DeleteSummedUpRecordHistory(global.UserAccountID, global.SchoolID.Value, global.AcademicYearID.Value, classid, semesterid, periodid, subjectid, lstPupilDeleteID);
                //JudgeRecordHistoryBusiness.Save();


            }
            else
            {
                JudgeRecordBusiness.DeleteJudgeRecord(global.UserAccountID, global.SchoolID.Value, global.AcademicYearID.Value, classid, semesterid, periodid, subjectid, lstPupilDeleteID);
                List<SummedUpRecord> lstSummed = SummedUpRecordBusiness.DeleteSummedUpRecord(global.UserAccountID, global.SchoolID.Value, global.AcademicYearID.Value, classid, semesterid, periodid, subjectid, lstPupilDeleteID);
                //JudgeRecordBusiness.Save();

                if (lstSummed != null)
                {
                    List<RESTORE_DATA_DETAIL> lstResSummed = SummedUpRecordBusiness.BackUpSummedMark(lstSummed, objRes);
                    if (lstResSummed != null)
                    {
                        lstRestoreDetail.AddRange(lstResSummed);
                        if (lstRestoreDetail != null && lstRestoreDetail.Count > 0)
                        {
                            if (objRes.SHORT_DESCRIPTION.Length > 1000)
                            {
                                objRes.SHORT_DESCRIPTION = objRes.SHORT_DESCRIPTION.Substring(-0, 1000) + "...";
                            }
                            RestoreDataBusiness.Insert(objRes);
                            RestoreDataBusiness.Save();
                            RestoreDataDetailBusiness.BulkInsert(lstRestoreDetail, ColumnMapping.Instance.RestoreDataDetail(), "RESTORE_DATA_DETAIL_ID");
                        }
                    }
                }
            }
            if (lstPupilDeleteID.Count > 0)
            {
                // Tạo dữ liệu ghi log   
                IDictionary<string, object> dicLog = new Dictionary<string, object>()
                {
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OLD_JSON,oldObject.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_NEW_JSON,""},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_OBJECTID,objectID.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_DESCRIPTION,descriptionObject.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_PARAMETER,paramObject.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERACTION,userActions.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERFUNTION,userFunctions.ToString()},
                    {SMAS.Web.Constants.GlobalConstants.ACTION_AUDIT_USERDESCRIPTION,userDescriptions.ToString()}
                };
                SetViewDataActionAudit(dicLog);
                //Tinh lai TBM cho hoc ky hoac dot
                JudgeRecordBusiness.SummedJudgeMark(lstPupilDeleteID, acaYear, semesterid, (period == null) ? 0 : 1, classSubject);
            }

            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }



        #region ImportExcel

        [ValidateAntiForgeryToken]
        public JsonResult ImportExcel(IEnumerable<HttpPostedFileBase> attachments, int? semesterid, int? educationlevelid, int? periodid, int? classid, int? subjectid)
        {

            GlobalInfo global = new GlobalInfo();
            AcademicYear aca = AcademicYearBusiness.Find(global.AcademicYearID);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(aca);
            if (attachments == null || attachments.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
            // The Name of the Upload component is "attachments"
            var file = attachments.FirstOrDefault();
            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> excelExtension = new List<string>();
                excelExtension.Add(".XLS");
                excelExtension.Add(".XLSX");
                var extension = Path.GetExtension(file.FileName);

                if (!excelExtension.Contains(extension.ToUpper()))
                {
                    throw new BusinessException("Common_Label_ExcelExtensionError");
                }
                if (file.ContentLength / 1024 > 1024)
                {
                    throw new BusinessException("JudgeRecord_Validate_FileMaxSize");
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Excels"), fileName);
                file.SaveAs(physicalPath);
                var dic = OnImport(physicalPath, semesterid.Value, educationlevelid.Value, periodid.Value, classid.Value, subjectid.Value);
                if (dic.FirstOrDefault().Value == false)
                {
                    ViewData[JudgeRecordConstants.ERROR_BASIC_DATA] = true;
                    ViewData[JudgeRecordConstants.LIST_PERIODDECLARATION] = new PeriodDeclaration();
                    ViewData[JudgeRecordConstants.LIST_JUDGERECORDOFCLASS] = new List<JudgeRecordBO>();
                    ViewData[JudgeRecordConstants.ERROR_IMPORT_MESSAGE] = dic.FirstOrDefault().Key;
                    return Json(new JsonMessage(RenderPartialViewToString("_ChooseAction", null), "grid"));
                }
                else
                {
                    var model = getDataFromImportFile(physicalPath, semesterid.Value, educationlevelid.Value, periodid.Value, classid.Value, subjectid.Value);
                    List<JudgeRecordViewModel> lsTemp = model.ListJudgeRecordViewModel;
                    List<JudgeRecord> ListJudgeRecord = new List<JudgeRecord>();
                    Session["ImportData_ListJudgeRecord"] = lsTemp;
                    Session["ImportData_ListSummedUpRecord"] = model.ListSummedUpRecord;
                    Session["semesterid"] = semesterid;
                    Session["periodid"] = periodid;
                    Session["AcademicYearID"] = global.AcademicYearID;
                    Session["SchoolID"] = global.SchoolID;
                    Session["ClassID"] = classid.Value;
                    Session["SubjectID"] = subjectid.Value;

                    //ViewData[JudgeRecordConstants.LIST_IMPORTDATA] = ListJudgeRecord;
                    ViewData[JudgeRecordConstants.HAS_ERROR_DATA] = lsTemp.Where(o => o.ErrorDescription != null).Count() > 0;

                    if (lsTemp.Where(o => o.IsLegalBot.HasValue && o.IsLegalBot.Value == false).Count() > 0)
                    {
                        IDictionary<string, object> SearchInfoPeriod = new Dictionary<string, object>();
                        SearchInfoPeriod["AcademicYearID"] = global.AcademicYearID;
                        SearchInfoPeriod["PeriodID"] = periodid;
                        var lstPeriodDeclaration = PeriodDeclarationBusiness.Search(SearchInfoPeriod);
                        var periodDecalaration = lstPeriodDeclaration.FirstOrDefault();
                        ViewData[JudgeRecordConstants.ERROR_BASIC_DATA] = false;
                        ViewData[JudgeRecordConstants.ERROR_IMPORT_MESSAGE] = "Có lỗi trong file excel";
                        List<JudgeRecordBO> List_JudgeRecordBO = new List<JudgeRecordBO>();
                        // lay
                        var lstPupilModel = lsTemp.Select(o => new { o.PupilID, o.flagPupil }).Distinct().ToList();

                        foreach (var ObjPupil in lstPupilModel)
                        {
                            //List<JudgeRecord> listPupilJudgeRecord = ListJudgeRecord.Where(o => o.PupilID == pupilID.PupilID).OrderBy(o => o.OrderNumber).ToList();
                            List<JudgeRecordViewModel> lstjudgeRecord = lsTemp.Where(o => o.PupilID == ObjPupil.PupilID && o.flagPupil == ObjPupil.flagPupil).ToList();
                            JudgeRecordBO JudgeRecordBO = new Business.BusinessObject.JudgeRecordBO();
                            JudgeRecordBO.PupilID = lstjudgeRecord[0].PupilID;
                            JudgeRecordBO.PupilCode = lstjudgeRecord[0].PupilCode;
                            JudgeRecordBO.FullName = lstjudgeRecord[0].PupilName;
                            JudgeRecordBO.AcademicYearID = lstjudgeRecord[0].AcademicYearID;
                            JudgeRecordBO.ClassID = lstjudgeRecord[0].ClassID;
                            JudgeRecordBO.SchoolID = lstjudgeRecord[0].SchoolID;
                            JudgeRecordBO.SubjectID = lstjudgeRecord[0].SubjectID;
                            JudgeRecordBO.MarkTypeID = lstjudgeRecord[0].MarkTypeID;
                            JudgeRecordBO.Semester = lstjudgeRecord[0].Semester;
                            JudgeRecordBO.Judgement = lstjudgeRecord[0].Judgement;
                            JudgeRecordBO.OrderNumber = lstjudgeRecord[0].OrderNumber;
                            JudgeRecordBO.Title = lstjudgeRecord[0].Title;
                            JudgeRecordBO.IsLegalBot = lstjudgeRecord.Where(o => o.IsLegalBot == false).Count() > 0 ? false : true;

                            JudgeRecordBO.InterviewMark = new string[Convert.ToByte(periodDecalaration.InterviewMark)];
                            JudgeRecordBO.WritingMark = new string[Convert.ToByte(periodDecalaration.WritingMark)];
                            JudgeRecordBO.TwiceCoeffiecientMark = new string[Convert.ToByte(periodDecalaration.TwiceCoeffiecientMark)];

                            PeriodDeclaration Period = PeriodDeclarationBusiness.Find(periodid);
                            int InterviewMark = Period.InterviewMark.Value;
                            int StartIndexOfInterviewMark = Period.StartIndexOfInterviewMark.Value;
                            int WritingMark = Period.WritingMark.Value;
                            int StartIndexOfWritingMark = Period.StartIndexOfWritingMark.Value;
                            int TwiceCoeffiecientMark = Period.TwiceCoeffiecientMark.Value;
                            int StartIndexOfTwiceCoeffiecientMark = Period.StartIndexOfTwiceCoeffiecientMark.Value;

                            foreach (JudgeRecordViewModel mr in lstjudgeRecord)
                            {
                                if (mr.Title != null)
                                {
                                    string title = mr.Title[0].ToString();
                                    if (title.Equals("M") && (mr.OrderNumber - StartIndexOfInterviewMark) < InterviewMark)
                                    {
                                        JudgeRecordBO.InterviewMark[mr.OrderNumber - StartIndexOfInterviewMark] = mr.Judgement;
                                    }
                                    if (title.Equals("P") && (mr.OrderNumber - StartIndexOfWritingMark) < WritingMark)
                                    {
                                        JudgeRecordBO.WritingMark[mr.OrderNumber - StartIndexOfWritingMark] = mr.Judgement;
                                    }
                                    if (title.Equals("V") && (mr.OrderNumber - StartIndexOfTwiceCoeffiecientMark) < TwiceCoeffiecientMark)
                                    {
                                        JudgeRecordBO.TwiceCoeffiecientMark[mr.OrderNumber - StartIndexOfTwiceCoeffiecientMark] = mr.Judgement;
                                    }
                                    if (mr.Title.Equals("HK"))
                                    {
                                        JudgeRecordBO.SemesterMark = mr.Judgement;
                                    }
                                    if ((JudgeRecordBO.ErrorDescription == null) || (JudgeRecordBO.ErrorDescription != null && mr.ErrorDescription != null && !JudgeRecordBO.ErrorDescription.Contains(mr.ErrorDescription)))
                                    {
                                        JudgeRecordBO.ErrorDescription += mr.ErrorDescription;
                                    }
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            //if (!List_JudgeRecordBO.Any(o => o.PupilID == JudgeRecordBO.PupilID))
                            //{
                            //    List_JudgeRecordBO.Add(JudgeRecordBO);
                            //}
                            List_JudgeRecordBO.Add(JudgeRecordBO);
                        }

                        ViewData[JudgeRecordConstants.LIST_SUMMEDUPRECORDOFCLASSIMPORT] = model.ListSummedUpRecord;
                        ViewData[JudgeRecordConstants.LIST_PERIODDECLARATION] = periodDecalaration;
                        ViewData[JudgeRecordConstants.LIST_JUDGERECORDOFCLASS] = List_JudgeRecordBO;
                        return Json(new JsonMessage(RenderPartialViewToString("_ChooseAction", null), "grid"));
                    }
                    else
                    {
                        // Lay thong tin mien giam
                        List<ExemptedSubject> listExemptedSubject = ExemptedSubjectBusiness.GetListExemptedSubject(classid.Value, semesterid.Value)
                        .Where(o => o.SubjectID == subjectid)
                        .ToList();


                        // Tạo data ghi log
                        PeriodDeclaration PeriodDeclaration = PeriodDeclarationBusiness.Find(periodid);
                        SubjectCat subject = SubjectCatBusiness.Find(subjectid);
                        StringBuilder objectIDStr = new StringBuilder();
                        StringBuilder descriptionStr = new StringBuilder();
                        StringBuilder paramsStr = new StringBuilder();
                        StringBuilder oldObjectStr = new StringBuilder();
                        StringBuilder newObjectStr = new StringBuilder();
                        StringBuilder userFuntionsStr = new StringBuilder();
                        StringBuilder userActionsStr = new StringBuilder();
                        StringBuilder userDescriptionsStr = new StringBuilder();
                        StringBuilder inforLog = null;
                        int iLog = 0;
                        List<int> pupilIds = lsTemp.Select(s => s.PupilID).Distinct().ToList();
                        List<PupilOfClass> lstPOC = (from p in PupilOfClassBusiness.All
                                                     where pupilIds.Contains(p.PupilID)
                                                     //&& p.Last2digitNumberSchool == (global.SchoolID % 100)
                                                     && p.ClassID == classid
                                                     && p.AcademicYearID == aca.AcademicYearID
                                                     && p.Year == aca.Year
                                                     select p).ToList();
                        List<JudgeRecordHistory> listTempHistory = null;
                        List<JudgeRecord> listTemp = null;
                        if (isMovedHistory)
                        {
                            var queryHis = from m in JudgeRecordHistoryBusiness.All
                                           where m.Last2digitNumberSchool == (global.SchoolID % 100)
                                           && m.AcademicYearID == global.AcademicYearID
                                           && m.ClassID == classid
                                           && m.SubjectID == subjectid
                                           && m.Semester == semesterid
                                           select m;
                            listTempHistory = queryHis.ToList();
                        }
                        else
                        {
                            var query = from m in JudgeRecordBusiness.All
                                        where m.Last2digitNumberSchool == (global.SchoolID % 100)
                                        && m.AcademicYearID == global.AcademicYearID
                                        && m.ClassID == classid
                                        && m.SubjectID == subjectid
                                        && m.Semester == semesterid
                                        select m;
                            listTemp = query.ToList();
                        }
                        for (int i = 0, size1 = pupilIds.Count; i < size1; i++)
                        {
                            // Tạo dữ liệu ghi log
                            int id = pupilIds[i];
                            if (listExemptedSubject.Any(u => u.PupilID == id))
                                continue;
                            PupilOfClass pop = lstPOC.Where(p => p.PupilID == id).FirstOrDefault();
                            objectIDStr.Append(id);
                            descriptionStr.Append("Import mark record pupil_id:" + id);
                            paramsStr.Append("pupil_id:" + id);
                            userFuntionsStr.Append("Sổ điểm");
                            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_IMPORT);
                            inforLog = new StringBuilder();
                            inforLog.Append("Import điểm cho " + pop.PupilProfile.FullName);
                            inforLog.Append(" mã " + pop.PupilProfile.PupilCode);
                            inforLog.Append(" lớp " + pop.ClassProfile.DisplayName);
                            inforLog.Append(" môn " + subject.SubjectName);
                            inforLog.Append(" học kì " + semesterid);
                            inforLog.Append(" đợt " + PeriodDeclaration.Resolution);
                            inforLog.Append(" năm " + pop.Year.Value + "-" + (pop.Year.Value + 1) + " ");
                            userDescriptionsStr.Append(inforLog.ToString());
                            if (isMovedHistory)
                            {
                                if (listTempHistory != null)
                                {
                                    List<JudgeRecordHistory> listOldRecord = listTempHistory.Where(p => p.PupilID == id).ToList();
                                    oldObjectStr.Append("{");
                                    oldObjectStr.Append("pupil_profile_id:" + id + ",");
                                    for (int j = 0; j < listOldRecord.Count; j++)
                                    {
                                        JudgeRecordHistory record = listOldRecord[j];
                                        oldObjectStr.Append("{JudgeRecordID:" + record.JudgeRecordID + "," + record.Title + ":" + record.Judgement + "}");
                                        if (j < listOldRecord.Count - 1)
                                        {
                                            oldObjectStr.Append(",");
                                        }
                                    }
                                    oldObjectStr.Append("}");
                                    // end
                                }
                            }
                            else
                            {
                                if (listTemp != null)
                                {
                                    List<JudgeRecord> listOldRecord = listTemp.Where(p => p.PupilID == id).ToList();
                                    oldObjectStr.Append("{");
                                    oldObjectStr.Append("pupil_profile_id:" + id + ",");
                                    for (int j = 0; j < listOldRecord.Count; j++)
                                    {
                                        JudgeRecord record = listOldRecord[j];
                                        oldObjectStr.Append("{JudgeRecordID:" + record.JudgeRecordID + "," + record.Title + ":" + record.Judgement + "}");
                                        if (j < listOldRecord.Count - 1)
                                        {
                                            oldObjectStr.Append(",");
                                        }
                                    }
                                    oldObjectStr.Append("}");
                                }
                            }
                            // end

                            List<JudgeRecordViewModel> temp = lsTemp.Where(t => t.PupilID == id).ToList();
                            for (int j = 0, size2 = temp.Count; j < size2; j++)
                            {
                                if (!String.IsNullOrEmpty(temp[j].Judgement) && !"-1".Equals(temp[j].Judgement))
                                {
                                    userDescriptionsStr.Append(temp[j].Title + ":" + temp[j].Judgement + " ");
                                }
                            }

                            // Tạo dữ liệu ghi log
                            if (iLog < pupilIds.Count - 1)
                            {
                                objectIDStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                descriptionStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                paramsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                oldObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                newObjectStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                userFuntionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                userActionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                                userDescriptionsStr.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                            }
                            iLog++;
                        }
                        // end

                        foreach (JudgeRecordViewModel item in lsTemp)
                        {
                            if (listExemptedSubject.Any(u => u.PupilID == item.PupilID))
                                continue;
                            // Bo qua cac hoc sinh mien giam
                            if (!string.IsNullOrWhiteSpace(item.Judgement) && item.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            {
                                JudgeRecord judgeRecord = new SMAS.Models.Models.JudgeRecord();
                                Utils.Utils.BindTo(item, judgeRecord);
                                judgeRecord.CreatedAcademicYear = aca.Year;
                                ListJudgeRecord.Add(judgeRecord);
                            }

                        }
                        List<SummedUpRecord> lstSummedUp = new List<SummedUpRecord>();
                        model.ListSummedUpRecord.ForEach(o =>
                        {
                            if (o.Status.HasValue && o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                 && !listExemptedSubject.Any(u => u.PupilID == o.PupilID))
                            {
                                SummedUpRecord obj = new SummedUpRecord();
                                Utils.Utils.BindTo(o, obj);
                                obj.CreatedAcademicYear = o.Year.Value;
                                lstSummedUp.Add(obj);
                            }
                        });
                        if (isMovedHistory)
                        {
                            List<JudgeRecordHistory> lstJudgeRecordHistory = new List<JudgeRecordHistory>();
                            List<SummedUpRecordHistory> lstSummedUpRecordHistory = new List<SummedUpRecordHistory>();
                            JudgeRecordHistory objJudgeRecordHistory = null;
                            JudgeRecord objJudgeRecord = null;
                            SummedUpRecord objSummedUpRecord = null;
                            SummedUpRecordHistory objSummedUpRecordHistory = null;
                            for (int i = 0; i < ListJudgeRecord.Count; i++)
                            {
                                objJudgeRecordHistory = new JudgeRecordHistory();
                                objJudgeRecord = ListJudgeRecord[i];
                                objJudgeRecordHistory.AcademicYearID = objJudgeRecord.AcademicYearID;
                                objJudgeRecordHistory.PupilID = objJudgeRecord.PupilID;
                                objJudgeRecordHistory.SchoolID = objJudgeRecord.SchoolID;
                                objJudgeRecordHistory.ClassID = objJudgeRecord.ClassID;
                                objJudgeRecordHistory.Judgement = objJudgeRecord.Judgement;
                                objJudgeRecordHistory.MarkTypeID = objJudgeRecord.MarkTypeID;
                                objJudgeRecordHistory.Semester = objJudgeRecord.Semester;
                                objJudgeRecordHistory.Title = objJudgeRecord.Title;
                                objJudgeRecordHistory.Last2digitNumberSchool = objJudgeRecord.Last2digitNumberSchool;
                                objJudgeRecordHistory.MarkedDate = objJudgeRecord.MarkedDate;
                                objJudgeRecordHistory.ModifiedDate = objJudgeRecord.ModifiedDate;
                                objJudgeRecordHistory.SubjectID = objJudgeRecord.SubjectID;
                                objJudgeRecordHistory.CreatedDate = objJudgeRecord.CreatedDate;
                                objJudgeRecordHistory.CreatedAcademicYear = objJudgeRecord.CreatedAcademicYear;
                                objJudgeRecordHistory.OrderNumber = objJudgeRecord.OrderNumber;
                                lstJudgeRecordHistory.Add(objJudgeRecordHistory);
                            }
                            for (int i = 0; i < lstSummedUp.Count; i++)
                            {
                                objSummedUpRecordHistory = new SummedUpRecordHistory();
                                objSummedUpRecord = lstSummedUp[i];
                                objSummedUpRecordHistory.PupilID = objSummedUpRecord.PupilID;
                                objSummedUpRecordHistory.ClassID = objSummedUpRecord.ClassID;
                                objSummedUpRecordHistory.SchoolID = objSummedUpRecord.SchoolID;
                                objSummedUpRecordHistory.SubjectID = objSummedUpRecord.SubjectID;
                                objSummedUpRecordHistory.SummedUpDate = objSummedUpRecord.SummedUpDate;
                                objSummedUpRecordHistory.SummedUpMark = objSummedUpRecord.SummedUpMark;
                                objSummedUpRecordHistory.SummedUpRecordID = objSummedUpRecord.SummedUpRecordID;
                                objSummedUpRecordHistory.Semester = objSummedUpRecord.Semester;
                                objSummedUpRecordHistory.PeriodID = objSummedUpRecord.PeriodID;
                                objSummedUpRecordHistory.Last2digitNumberSchool = objSummedUpRecord.Last2digitNumberSchool;
                                objSummedUpRecordHistory.AcademicYearID = objSummedUpRecord.AcademicYearID;
                                objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                                objSummedUpRecordHistory.IsCommenting = objSummedUpRecord.IsCommenting;
                                objSummedUpRecordHistory.JudgementResult = objSummedUpRecord.JudgementResult;
                                objSummedUpRecordHistory.ReTestJudgement = objSummedUpRecord.ReTestJudgement;
                                objSummedUpRecordHistory.ReTestMark = objSummedUpRecord.ReTestMark;
                                objSummedUpRecordHistory.CreatedAcademicYear = objSummedUpRecord.CreatedAcademicYear;
                                lstSummedUpRecordHistory.Add(objSummedUpRecordHistory);
                            }
                            JudgeRecordHistoryBusiness.InsertJudgeRecordHistory(global.UserAccountID, lstJudgeRecordHistory.Where(o => string.IsNullOrWhiteSpace(o.Judgement) == false).ToList(), lstSummedUpRecordHistory, semesterid.Value, periodid, null, global.SchoolID.Value, global.AcademicYearID.Value, classid.Value, subjectid.Value, _globalInfo.EmployeeID);


                        }
                        else
                        {
                            JudgeRecordBusiness.InsertJudgeRecord(global.UserAccountID, ListJudgeRecord.Where(o => string.IsNullOrWhiteSpace(o.Judgement) == false).ToList(), lstSummedUp, semesterid.Value, periodid, _globalInfo.EmployeeID, null, global.SchoolID.Value, global.AcademicYearID.Value, classid.Value, subjectid.Value);

                        }

                        // Tạo dữ liệu ghi log
                        SetViewDataActionAudit(oldObjectStr.ToString(), newObjectStr.ToString(), objectIDStr.ToString(), descriptionStr.ToString(), paramsStr.ToString(), userFuntionsStr.ToString(), userActionsStr.ToString(), userDescriptionsStr.ToString());

                        return Json(new JsonMessage(Res.Get("Common_Label_ImportMarkSuccess")));
                    }
                }
            }
            // Return an empty string to signify success
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        private Dictionary<string, bool> OnImport(string filePath, int semesterid, int educationlevelid, int periodid, int classid, int subjectid)
        {
            IVTWorkbook oBook = VTExport.OpenWorkbook(filePath);
            GlobalInfo global = new GlobalInfo();
            string subject = SubjectCatBusiness.Find(subjectid).DisplayName;
            string period = PeriodDeclarationBusiness.Find(periodid).Resolution;
            string semester = ReportUtils.ConvertSemesterForReportName(semesterid);
            string className = ClassProfileBusiness.Find(classid).DisplayName;

            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            string School = (string)sheet.GetCellValue(2, VTVector.dic['A']);
            string subjectAndSemesterAndClass = (string)sheet.GetCellValue(4, VTVector.dic['A']);
            string YearTitle = (string)sheet.GetCellValue(5, VTVector.dic['A']);
            subjectAndSemesterAndClass = subjectAndSemesterAndClass.ToUpper();
            YearTitle = YearTitle.ToUpper();

            string faultDescription = "";
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(faultDescription);
            bool isLegalTop = true;
            if (!subjectAndSemesterAndClass.Contains(subject.ToUpper()))
            {
                isLegalTop = false;
                stringBuilder.Append("- File excel không phải là file điểm của môn " + subject + "</br>");
            }
            if (!subjectAndSemesterAndClass.Contains(semester.ToUpper() + " "))
            {
                isLegalTop = false;
                stringBuilder.Append("- File excel không phải là file điểm của kỳ " + semester + "</br>");
            }
            if (!subjectAndSemesterAndClass.Contains(period.ToUpper()))
            {
                isLegalTop = false;
                stringBuilder.Append("- File excel không phải là file điểm của đợt " + period + "</br>");
            }
            if (!subjectAndSemesterAndClass.Contains(className.ToUpper()))
            {
                isLegalTop = false;
                stringBuilder.Append("- File excel không phải là file điểm của lớp " + className + "</br>");
            }
            faultDescription = stringBuilder.ToString();
            var dic = new Dictionary<string, bool>();
            dic.Add(faultDescription, isLegalTop);
            return dic;
        }

        private JudgeRecodeImportViewModel getDataFromImportFile(string filePath, int semesterid, int educationlevelid, int periodid, int classid, int subjectid)
        {
            JudgeRecodeImportViewModel model = new JudgeRecodeImportViewModel();
            List<JudgeRecordViewModel> ListJudgeRecord = new List<JudgeRecordViewModel>();
            List<SummedUpRecordBO> ListSummedUpRecord = new List<SummedUpRecordBO>();
            IVTWorkbook oBook = VTExport.OpenWorkbook(filePath);
            GlobalInfo global = new GlobalInfo();

            //Load tiêu đề hiển thị con điểm: gọi hàm MarkTypeBusiness.Search() với điều kiện truyền vào AppliedLevel = UserInfo.AppliedLevel
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AppliedLevel"] = global.AppliedLevel;
            var lstMarkType = MarkTypeBusiness.Search(SearchInfo).ToList();

            var PeriodDeclaration = PeriodDeclarationBusiness.Find(periodid);

            var lstPOC = (from poc in PupilOfClassBusiness.SearchBySchool(global.SchoolID.Value, new Dictionary<string, object> {
                            { "AcademicYearID", global.AcademicYearID.Value }, { "ClassID", classid }, { "Check", "Check" }
                            })
                          join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                          select new { poc.PupilID, pp.FullName, pp.PupilCode, poc.Status }
                          )
                .ToList();
            ClassSubject classSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>() { { "ClassID", classid }, { "SubjectID", subjectid } }).FirstOrDefault();
            SubjectCat subjectCat = SubjectCatBusiness.Find(subjectid);

            string strMarkTypePeriod = MarkRecordBusiness.GetMarkTitle(periodid);
            string subject = subjectCat.DisplayName;
            string period = PeriodDeclaration.Resolution;
            string semester = ReportUtils.ConvertSemesterForReportName(semesterid);
            string className = ClassProfileBusiness.Find(classid).DisplayName;
            int year = 0;

            AcademicYear AcademicYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            year = AcademicYear.Year;
            //lấy các sheet ra:
            IVTWorksheet sheet = oBook.GetSheet(1);
            string School = (string)sheet.GetCellValue(2, VTVector.dic['A']);
            string subjectAndSemesterAndClass = (string)sheet.GetCellValue(4, VTVector.dic['A']);
            string YearTitle = (string)sheet.GetCellValue(5, VTVector.dic['A']);
            subjectAndSemesterAndClass = subjectAndSemesterAndClass.ToUpper();
            YearTitle = YearTitle.ToUpper();

            var lstLockedMarkDetail = MarkRecordBusiness.GetLockMarkTitle(global.SchoolID.Value, global.AcademicYearID.Value, classid, (int)semesterid, (int)subjectid);
            List<List<string>> lsTemp = new List<List<string>>();
            int i = 0;
            // Kiem tra trung du lieu hay khong?
            List<string> lstPupilCode = new List<string>();
            int flagPupil = 1;
            bool checkDuplicate = false;
            while (sheet.GetCellValue(i + 8, VTVector.dic['A']) != null || sheet.GetCellValue(i + 8, VTVector.dic['C']) != null || sheet.GetCellValue(i + 8, VTVector.dic['D']) != null)
            {
                bool isLegalBot = true;
                //PupilCode = sheet.GetCellValue(i + 8, VTVector.dic['C']).ToString().Trim();
                string PupilCode = (string)sheet.GetCellValue(i + 8, 3);
                string FullName = (string)sheet.GetCellValue(i + 8, 4);

                if (lstPupilCode.Contains(PupilCode))
                {
                    checkDuplicate = true;
                }
                else
                {
                    lstPupilCode.Add(PupilCode);
                    checkDuplicate = false;
                }
                string ErrorDescription = string.Empty;
                #region Kiem tra du lieu hoc sinh
                // Check trùng dữ liệu:
                if (checkDuplicate)
                {
                    isLegalBot = false;
                    ErrorDescription = "- Trùng mã học sinh.";
                }
                var poc = lstPOC.Where(u => u.PupilCode.Equals(PupilCode, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (poc == null)
                {
                    isLegalBot = false;
                    ErrorDescription = "- Mã học sinh không hợp lệ </br>";
                }
                else if (!poc.FullName.Equals(FullName, StringComparison.InvariantCultureIgnoreCase))
                {
                    isLegalBot = false;
                    ErrorDescription = "- Họ và tên học sinh không hợp lệ </br>";
                }
                #endregion

                int start_m = PeriodDeclaration.StartIndexOfInterviewMark.Value;
                int start_p = PeriodDeclaration.StartIndexOfWritingMark.Value;
                int start_v = PeriodDeclaration.StartIndexOfTwiceCoeffiecientMark.Value;
                int colum_start = VTVector.dic['E'];

                for (int j = 0; j < PeriodDeclaration.InterviewMark; j++)
                {
                    var celvalue = sheet.GetCellValue(i + 8, colum_start) != null ? sheet.GetCellValue(i + 8, colum_start).ToString() : "";
                    colum_start++;

                    JudgeRecordViewModel JudgeRecord = new JudgeRecordViewModel();

                    #region Assign Data for JudgeRecord item
                    if (j > 0)
                    {
                        ErrorDescription = string.Empty;
                    }
                    if (poc != null)
                    {
                        JudgeRecord.Status = poc.Status;
                        JudgeRecord.PupilID = poc.PupilID;
                    }
                    JudgeRecord.ErrorDescription = ErrorDescription;
                    JudgeRecord.AcademicYearID = AcademicYear.AcademicYearID;
                    JudgeRecord.PupilName = FullName;
                    JudgeRecord.PupilCode = PupilCode;
                    JudgeRecord.ClassID = classid;
                    JudgeRecord.SchoolID = AcademicYear.SchoolID;
                    JudgeRecord.SubjectID = subjectid;
                    JudgeRecord.Year = year;
                    JudgeRecord.Semester = semesterid;
                    JudgeRecord.PeriodID = periodid;
                    if (string.IsNullOrEmpty(celvalue) || celvalue == GlobalConstants.SHORT_NOT_OK || celvalue == GlobalConstants.SHORT_OK)
                    {
                        JudgeRecord.MarkTypeID = lstMarkType.Where(o => o.Title == "M").FirstOrDefault().MarkTypeID;
                        JudgeRecord.Judgement = string.IsNullOrEmpty(celvalue) ? "-1" : celvalue.Trim();
                        JudgeRecord.OrderNumber = Convert.ToByte(start_m + j);
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;
                        JudgeRecord.Title = string.Format("M{0}", start_m + j);
                        if (lstLockedMarkDetail.Contains(JudgeRecord.Title) && (celvalue == GlobalConstants.SHORT_OK || celvalue == GlobalConstants.SHORT_NOT_OK) && !global.IsAdminSchoolRole)
                        {
                            //Nếu con điểm bị khoá thì không lấy lên nữa
                            JudgeRecord.Judgement = "";
                            //isLegalBot = false;
                            //JudgeRecord.ErrorDescription += "- Con điểm " + JudgeRecord.Title + " bị khóa </br>";
                        }
                    }
                    else
                    {
                        JudgeRecord.Judgement = celvalue.Trim();
                        JudgeRecord.OrderNumber = Convert.ToByte(start_m + j);
                        JudgeRecord.Title = string.Format("M{0}", start_m + j);
                        isLegalBot = false;
                        JudgeRecord.ErrorDescription += "- Điểm " + JudgeRecord.Title + " không hợp lệ </br>";
                    }
                    JudgeRecord.IsLegalBot = isLegalBot;
                    JudgeRecord.flagPupil = flagPupil;
                    #endregion
                    //if (!string.IsNullOrWhiteSpace(JudgeRecord.Judgement))
                    ListJudgeRecord.Add(JudgeRecord);
                }

                for (var j = 0; j < PeriodDeclaration.WritingMark; j++)
                {
                    var celvalue = sheet.GetCellValue(i + 8, colum_start) != null ? sheet.GetCellValue(i + 8, colum_start).ToString() : "";
                    colum_start++;

                    JudgeRecordViewModel JudgeRecord = new JudgeRecordViewModel();

                    #region Assign Data for JudgeRecord item
                    JudgeRecord.AcademicYearID = global.AcademicYearID.Value;
                    if (j > 0)
                    {
                        ErrorDescription = string.Empty;
                    }
                    if (poc != null)
                    {
                        JudgeRecord.Status = poc.Status;
                        JudgeRecord.PupilID = poc.PupilID;
                    }
                    JudgeRecord.ErrorDescription = ErrorDescription;
                    JudgeRecord.AcademicYearID = global.AcademicYearID.Value;
                    JudgeRecord.PupilName = FullName;
                    JudgeRecord.PupilCode = PupilCode;
                    JudgeRecord.ClassID = classid;
                    JudgeRecord.SchoolID = global.SchoolID.Value;
                    JudgeRecord.SubjectID = (int)subjectid;
                    JudgeRecord.Year = year;
                    JudgeRecord.Semester = (int)semesterid;
                    JudgeRecord.PeriodID = periodid;
                    if (string.IsNullOrWhiteSpace(celvalue) || celvalue == "CĐ" || celvalue == "Đ")
                    {
                        JudgeRecord.MarkTypeID = lstMarkType.Where(o => o.Title == "P").FirstOrDefault().MarkTypeID;
                        if (string.IsNullOrWhiteSpace(celvalue))
                        {
                            JudgeRecord.Judgement = "-1";
                        }
                        else
                        {
                            JudgeRecord.Judgement = celvalue;
                        }
                        JudgeRecord.OrderNumber = Convert.ToByte(start_p + j);
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;
                        JudgeRecord.Title = string.Format("P{0}", start_p + j);
                        if (lstLockedMarkDetail.Contains(JudgeRecord.Title) && (celvalue == "Đ" || celvalue == "CĐ") && !global.IsAdminSchoolRole)
                        {
                            //Nếu con điểm bị khoá thì không lấy lên nữa
                            JudgeRecord.Judgement = "";
                            //isLegalBot = false;
                            //JudgeRecord.ErrorDescription += "- Con điểm " + JudgeRecord.Title + " bị khóa </br>";
                        }
                    }
                    else
                    {
                        JudgeRecord.Judgement = celvalue;
                        JudgeRecord.OrderNumber = Convert.ToByte(start_p + j);
                        JudgeRecord.Title = string.Format("P{0}", start_p + j);
                        isLegalBot = false;
                        JudgeRecord.ErrorDescription += "- Điểm " + JudgeRecord.Title + " không hợp lệ </br>";
                    }
                    JudgeRecord.IsLegalBot = isLegalBot;
                    JudgeRecord.flagPupil = flagPupil;
                    #endregion
                    //if (!string.IsNullOrWhiteSpace(JudgeRecord.Judgement))
                    ListJudgeRecord.Add(JudgeRecord);
                }

                for (var j = 0; j < PeriodDeclaration.TwiceCoeffiecientMark; j++)
                {
                    var celvalue = sheet.GetCellValue(i + 8, colum_start) != null ? sheet.GetCellValue(i + 8, colum_start).ToString() : "";
                    colum_start++;

                    JudgeRecordViewModel JudgeRecord = new JudgeRecordViewModel();
                    #region Assign Data for JudgeRecord item

                    JudgeRecord.AcademicYearID = global.AcademicYearID.Value;
                    if (j > 0)
                    {
                        ErrorDescription = string.Empty;
                    }
                    if (poc != null)
                    {
                        JudgeRecord.Status = poc.Status;
                        JudgeRecord.PupilID = poc.PupilID;
                    }
                    JudgeRecord.ErrorDescription = ErrorDescription;
                    JudgeRecord.PupilName = FullName;
                    JudgeRecord.PupilCode = PupilCode;
                    JudgeRecord.ClassID = classid;
                    JudgeRecord.SchoolID = global.SchoolID.Value;
                    JudgeRecord.SubjectID = (int)subjectid;
                    JudgeRecord.Year = year;
                    JudgeRecord.Semester = (int)semesterid;
                    JudgeRecord.PeriodID = periodid;
                    if (string.IsNullOrWhiteSpace(celvalue) || celvalue == "CĐ" || celvalue == "Đ")
                    {
                        if (string.IsNullOrWhiteSpace(celvalue))
                        {
                            JudgeRecord.Judgement = "-1";
                        }
                        else
                        {
                            JudgeRecord.Judgement = celvalue;
                        }
                        JudgeRecord.MarkTypeID = lstMarkType.Where(o => o.Title == "V").FirstOrDefault().MarkTypeID;
                        JudgeRecord.OrderNumber = Convert.ToByte(start_v + j);
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;
                        JudgeRecord.Title = string.Format("V{0}", start_v + j);
                        if (lstLockedMarkDetail.Contains(JudgeRecord.Title) && (celvalue == "Đ" || celvalue == "CĐ") && !global.IsAdminSchoolRole)
                        {
                            //Nếu con điểm bị khoá thì không lấy lên nữa
                            JudgeRecord.Judgement = "";
                            //isLegalBot = false;
                            //JudgeRecord.ErrorDescription += "- Con điểm " + JudgeRecord.Title + " bị khóa </br>";
                        }
                    }
                    else
                    {
                        JudgeRecord.Judgement = celvalue;
                        JudgeRecord.OrderNumber = Convert.ToByte(start_v + j);
                        JudgeRecord.Title = string.Format("V{0}", start_v + j);
                        isLegalBot = false;
                        JudgeRecord.ErrorDescription += "- Điểm " + JudgeRecord.Title + " không hợp lệ </br>";
                    }
                    JudgeRecord.IsLegalBot = isLegalBot;
                    JudgeRecord.flagPupil = flagPupil;
                    #endregion
                    //if (!string.IsNullOrWhiteSpace(JudgeRecord.Judgement))
                    ListJudgeRecord.Add(JudgeRecord);
                }
                //Todo check con diem hoc ki
                if (strMarkTypePeriod.Contains("HK"))
                {
                    var celvalue = sheet.GetCellValue(i + 8, colum_start) != null ? sheet.GetCellValue(i + 8, colum_start).ToString() : "";
                    colum_start++;
                    JudgeRecordViewModel JudgeRecord = new JudgeRecordViewModel();

                    JudgeRecord.AcademicYearID = global.AcademicYearID.Value;
                    if (poc != null)
                    {
                        JudgeRecord.Status = poc.Status;
                        JudgeRecord.PupilID = poc.PupilID;
                    }
                    JudgeRecord.ErrorDescription = ErrorDescription;
                    JudgeRecord.PupilName = FullName;
                    JudgeRecord.PupilCode = PupilCode;
                    JudgeRecord.ClassID = classid;
                    JudgeRecord.SchoolID = global.SchoolID.Value;
                    JudgeRecord.SubjectID = (int)subjectid;
                    JudgeRecord.Year = AcademicYear.Year;
                    JudgeRecord.Semester = (int)semesterid;
                    JudgeRecord.PeriodID = periodid;
                    if (string.IsNullOrWhiteSpace(celvalue) || celvalue == "CĐ" || celvalue == "Đ")
                    {
                        if (string.IsNullOrWhiteSpace(celvalue))
                        {
                            JudgeRecord.Judgement = "-1";
                        }
                        else
                        {
                            JudgeRecord.Judgement = celvalue;
                        }
                        JudgeRecord.MarkTypeID = lstMarkType.Where(o => o.Title == "HK").FirstOrDefault().MarkTypeID;
                        JudgeRecord.MarkedDate = DateTime.Now;
                        JudgeRecord.CreatedDate = DateTime.Now;
                        JudgeRecord.ModifiedDate = DateTime.Now;
                        JudgeRecord.Title = "HK";
                        if (lstLockedMarkDetail.Contains(JudgeRecord.Title) && (celvalue == "Đ" || celvalue == "CĐ") && !global.IsAdminSchoolRole)
                        {
                            //Nếu con điểm bị khoá thì không lấy lên nữa
                            JudgeRecord.Judgement = "";
                            //isLegalBot = false;
                            //JudgeRecord.ErrorDescription += "- Con điểm " + JudgeRecord.Title + " bị khóa </br>";
                        }
                    }
                    else
                    {
                        JudgeRecord.Judgement = celvalue;
                        JudgeRecord.Title = "HK";
                        isLegalBot = false;
                        JudgeRecord.ErrorDescription += "- Điểm " + JudgeRecord.Title + " không hợp lệ </br>";
                    }
                    JudgeRecord.IsLegalBot = isLegalBot;
                    JudgeRecord.flagPupil = flagPupil;
                    ListJudgeRecord.Add(JudgeRecord);
                }
                var tbm_celvalue = sheet.GetCellValue(i + 8, colum_start) != null ? sheet.GetCellValue(i + 8, colum_start).ToString() : "";
                SummedUpRecordBO SummedUpRecord = new SummedUpRecordBO();
                SummedUpRecord.AcademicYearID = global.AcademicYearID.Value;
                SummedUpRecord.ClassID = classid;
                SummedUpRecord.PeriodID = periodid;
                if (poc != null)
                {
                    SummedUpRecord.PupilID = poc.PupilID;
                    SummedUpRecord.Status = poc.Status;
                }
                SummedUpRecord.SchoolID = global.SchoolID.Value;
                SummedUpRecord.Semester = (int)semesterid;
                SummedUpRecord.SubjectID = (int)subjectid;
                if (string.IsNullOrWhiteSpace(tbm_celvalue))
                {
                    SummedUpRecord.JudgementResult = "-1";
                }
                else
                {
                    SummedUpRecord.JudgementResult = tbm_celvalue;
                }
                SummedUpRecord.SummedUpDate = DateTime.Now;
                SummedUpRecord.IsCommenting = classSubject.IsCommenting.Value;
                SummedUpRecord.Year = year;
                SummedUpRecord.IsLegalBot = isLegalBot;

                //lay nhung diem cua hoc sinh hop le thoi

                ListSummedUpRecord.Add(SummedUpRecord);
                model.ListJudgeRecordViewModel = ListJudgeRecord;
                model.ListSummedUpRecord = ListSummedUpRecord;
                i++;
                flagPupil++;
            }
            return model;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImportLegalData()
        {
            List<JudgeRecordViewModel> lsTemp = (List<JudgeRecordViewModel>)Session["ImportData_ListJudgeRecord"];
            List<int> listPupilNotImport = lsTemp.Where(u => u.IsLegalBot == false).Select(u => u.PupilID).Distinct().ToList();
            lsTemp = lsTemp.Where(u => !listPupilNotImport.Contains(u.PupilID)).ToList();
            List<SummedUpRecordBO> lstSummedUpRecord = (List<SummedUpRecordBO>)Session["ImportData_ListSummedUpRecord"];
            int semesterid = (int)Session["semesterid"];
            int? periodid = (int?)Session["periodid"];
            int schoolID = (int)Session["SchoolID"];
            int academicYearID = (int)Session["AcademicYearID"];
            int classID = (int)Session["ClassID"];
            int subjectID = (int)Session["SubjectID"];

            lsTemp = lsTemp.Where(o => o.IsLegalBot.HasValue && o.IsLegalBot.Value && o.Judgement != null && o.Judgement != "").ToList();
            List<JudgeRecord> ListJudgeRecord = new List<JudgeRecord>();
            foreach (var item in lsTemp)
            {
                JudgeRecord JudgeRecord = new JudgeRecord();
                #region Assign Data for JudgeRecord item
                JudgeRecord.AcademicYearID = new GlobalInfo().AcademicYearID.Value;
                JudgeRecord.PupilID = item.PupilID;
                JudgeRecord.ClassID = item.ClassID;
                JudgeRecord.SchoolID = item.SchoolID;
                JudgeRecord.SubjectID = item.SubjectID;
                JudgeRecord.MarkTypeID = item.MarkTypeID;
                JudgeRecord.CreatedAcademicYear = item.Year;
                JudgeRecord.Semester = item.Semester;
                JudgeRecord.Judgement = item.Judgement;
                JudgeRecord.OrderNumber = item.OrderNumber;
                JudgeRecord.MarkedDate = item.MarkedDate;
                JudgeRecord.CreatedDate = item.CreatedDate;
                JudgeRecord.ModifiedDate = item.ModifiedDate;
                JudgeRecord.Title = item.Title;
                #endregion
                ListJudgeRecord.Add(JudgeRecord);
            }
            // Lay danh sach hoc sinh mien giam:
            List<ExemptedSubject> listExempted = ExemptedSubjectBusiness.GetListExemptedSubject(classID, semesterid).Where(u => u.SubjectID == subjectID).ToList();
            List<int> lstPupilID = listExempted.Select(u => u.PupilID).Distinct().ToList();
            // Loai bo nhung hoc sinh mien giam:
            ListJudgeRecord = ListJudgeRecord.Where(u => !lstPupilID.Contains(u.PupilID)).ToList();
            // Lay danh sach TBM
            List<SummedUpRecord> lstSummedUp = new List<SummedUpRecord>();
            lstSummedUpRecord.ForEach(o =>
            {
                if (o.Status.HasValue && o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                {
                    SummedUpRecord obj = new SummedUpRecord();
                    Utils.Utils.BindTo(o, obj);
                    obj.CreatedAcademicYear = o.Year.Value;
                    lstSummedUp.Add(obj);
                }
            });
            JudgeRecordBusiness.InsertJudgeRecord(new GlobalInfo().UserAccountID, ListJudgeRecord, lstSummedUp, (int)semesterid, periodid, _globalInfo.EmployeeID, null, schoolID, academicYearID, classID, subjectID);
            return Json(new JsonMessage(Res.Get("Common_Label_ImportMarkSuccess")));
        }
        #endregion
        private class OldMark
        {
            public long JudgeRecordID { get; set; }
            public string Title { get; set; }
            public string Judgement { get; set; }
        }
    }
}
