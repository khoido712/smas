/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
namespace SMAS.Web.Areas.JudgeRecordArea.Models
{
    public class JudgeRecordViewModel
    {
        public System.Int32 JudgeRecordID { get; set; }
        public System.Int32 PupilID { get; set; }
        public string PupilCode { get; set; }
        public System.Int32 ClassID { get; set; }
        public System.Int32 SchoolID { get; set; }
        public System.Int32 AcademicYearID { get; set; }
        public System.Int32 SubjectID { get; set; }
        public System.Int32 MarkTypeID { get; set; }
        public System.Int32 Year { get; set; }
        public System.Nullable<System.Int32> Semester { get; set; }
        public System.Nullable<System.Int32> PeriodID { get; set; }
        public System.String Judgement { get; set; }
        public System.String ReTestJudgement { get; set; }
        public System.Int32 OrderNumber { get; set; }
        public System.DateTime MarkedDate { get; set; }
        public System.Nullable<System.DateTime> CreatedDate { get; set; }
        public System.Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Title { get; set; }
        public string ErrorDescription { get; set; }
        public bool? IsLegalBot { get; set; }
        public int Status { get; set; }
        public string PupilName { get; set; }
        public System.Int32 flagPupil { get; set; }
        
    }

    public class JudgeRecodeImportViewModel
    {
        public List<SummedUpRecordBO> ListSummedUpRecord { get; set; }
        public List<JudgeRecordViewModel> ListJudgeRecordViewModel { get; set; }
    }
}


