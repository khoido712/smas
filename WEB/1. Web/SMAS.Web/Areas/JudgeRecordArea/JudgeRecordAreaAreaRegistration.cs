﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.JudgeRecordArea
{
    public class JudgeRecordAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "JudgeRecordArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "JudgeRecordArea_default",
                "JudgeRecordArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
