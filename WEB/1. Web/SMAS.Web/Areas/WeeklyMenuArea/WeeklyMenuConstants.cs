/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.WeeklyMenuArea
{
    public class WeeklyMenuConstants
    {
        public const string LIST_DalyMenuDetail = "LIST_DalyMenuDetail";
        public const string LIST_DalyMenu = "LIST_DalyMenu";
        public const string LIST_WEEKLYMENU = "listWeeklyMenu";
        public const string LIST_WEEKLYMENUDETAIL = "listWeeklyMenuDetail";
        public const string List_EatingGroup = "EatingGroup";
        public const string List_MealCat = "MealCat";
        public const string DateFirstOfMonth = "01/01/2012";
        public const string DateFirstOfWeek = "DateFirstOfWeek";
        public const string List_DailyMenuDetailALL = "List_DailyMenuDetailALL";
        public const string DisplayCreateButton = "DisplayCreateButton";
    }
}