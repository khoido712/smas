﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.WeeklyMenuArea
{
    public class WeeklyMenuAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "WeeklyMenuArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "WeeklyMenuArea_default",
                "WeeklyMenuArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
