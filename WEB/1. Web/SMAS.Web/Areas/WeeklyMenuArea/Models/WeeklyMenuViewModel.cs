/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
namespace SMAS.Web.Areas.WeeklyMenuArea.Models
{
    public class WeeklyMenuViewModel
    {
		public System.Int32 WeeklyMenuID { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_WeeklyMenuCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]	
		public System.String WeeklyMenuCode { get; set; }								

		public System.Int32 MenuType { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_Note")]
        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(500, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]			
		public System.String Note { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_StartDate")]
        [DataConstraint(DataConstraintAttribute.GREATER_EQUALS, "DateTime.Now", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Validate_DataConstraint")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [UIHint("DateTimePicker")]	
		public System.DateTime StartDate { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_EatingGroup")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]					
		public System.Nullable<System.Int32> EatingGroupID { get; set; }								
		public System.Int32 SchoolID { get; set; }								
		public System.Int32 AcademicYearID { get; set; }								
		public System.DateTime CreatedDate { get; set; }								
		public System.Boolean IsActive { get; set; }								
		public System.Nullable<System.DateTime> ModifiedDate { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_EatingGroup")]
        public string EatingGroupName { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_DateApply")]
        public string DateApply { get; set; }
        public string StartDate_string { get; set; }
        public string EndDate { get; set; }

        public System.Int32 WeeklyMenuDetailID { get; set; }
        public System.Int32 DayOfWeek { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_DailyMenuCode")]
        public Nullable<System.Int32> DailyMenuID { get; set; }
        //public System.Int32 WeeklyMenuID { get; set; }

        [ResourceDisplayName("WeeklyMenu_Label_DayName")]
        public System.String DayName { get; set; }
    }
}


