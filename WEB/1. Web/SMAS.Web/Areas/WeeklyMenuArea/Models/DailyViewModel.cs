﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.WeeklyMenuArea.Models
{
    public class DailyViewModel 
    {
        public System.Int32 DailyMenuID { get; set; }
        public System.String DishName { get; set; }
        public System.Int32 MealID { get; set; }

    }
}
