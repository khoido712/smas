﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.ReportInputMonitoringSituationArea.Models;
using SMAS.VTUtils.HtmlHelpers;
using System.Text;
using System.Threading.Tasks;
using SMAS.Business.BusinessObject;
using Telerik.Web.Mvc;
using System.IO;
using System.Configuration;

namespace SMAS.Web.Areas.ReportInputMonitoringSituationArea.Controllers
{
    public class ReportInputMonitoringSituationController : ThreadController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly IMarkRecordBusiness MarkRecordBusiness;
        private readonly IPupilRankingBusiness PupilRankingBusiness;
        private readonly IInputStatisticsBusiness InputStatisticsBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        public ReportInputMonitoringSituationController(
            ISchoolProfileBusiness SchoolProfileBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IReportDefinitionBusiness ReportDefinitionBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness
            , IDistrictBusiness DistrictBusiness
            , IProvinceBusiness ProvinceBusiness
            , IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IEmployeeBusiness EmployeeBusiness
            , IClassSubjectBusiness ClassSubjectBusiness
            , IClassAssigmentBusiness ClassAssigmentBusiness
            , ITeachingAssignmentBusiness TeachingAssignmentBusiness
            , IMarkRecordBusiness MarkRecordBusiness
            , IPupilRankingBusiness PupilRankingBusiness
            , IInputStatisticsBusiness InputStatisticsBusiness
            , IProcessedReportBusiness ProcessedReportBusiness
            )
        {
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ReportMarkInputSituationBusiness = ReportMarkInputSituationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ClassAssigmentBusiness = ClassAssigmentBusiness;
            this.MarkRecordBusiness = MarkRecordBusiness;
            this.PupilRankingBusiness = PupilRankingBusiness;
            this.InputStatisticsBusiness = InputStatisticsBusiness;
            this.ProcessedReportBusiness = ProcessedReportBusiness;
        }

        #region index page
        /// <summary>
        /// index page
        /// </summary>
        /// <modifier date="2014/04/22">HaiVT</modifier>
        /// <returns></returns>
        public ActionResult Index()
        {
            SetViewData();
            return View();

        }
        #endregion

        #region set viewdata on indexPage
        /// <summary>
        /// set view data on indexPage
        /// </summary>
        /// <modifier date="2014/04/22" author="HaiVT">code lai theo dtyc</author>
        private void SetViewData()
        {

            GlobalInfo global = new GlobalInfo();
            List<AcademicYear> lstAcademicYear = new List<AcademicYear>();
            AcademicYear AcademicYear = null;
            int curSemester = 1;
            if (_globalInfo.IsSystemAdmin)
            {
                int year = DateTime.Now.Year;
                int firstYear = 2011;
                for (int i = year + 2; i >= firstYear; i--)
                {
                    AcademicYear = new AcademicYear
                    {
                        Year = i - 1,
                        DisplayTitle = String.Format("{0} -{1}", i - 1, i)
                    };
                    lstAcademicYear.Add(AcademicYear);
                }
                if (DateTime.Now.Month <= 8)
                {
                    year = year - 1;
                }
                curSemester = 1;
                ViewData[ReportInputMonitoringSituationConstants.LS_YEAR] = new SelectList(lstAcademicYear, "Year", "DisplayTitle", year);
            }
            else
            {
                // nam hoc cho phong/so
                List<int> lstYear = AcademicYearBusiness.GetListYearForSupervisingDept_Pro(_globalInfo.SupervisingDeptID.Value).ToList();
                for (int i = 0; i < lstYear.Count(); i++)
                {
                    AcademicYear = new AcademicYear();
                    AcademicYear.Year = lstYear[i];
                    AcademicYear.DisplayTitle = lstYear[i] + "-" + (lstYear[i] + 1);
                    lstAcademicYear.Add(AcademicYear);
                }
                // Lay nam hoc hien tai
                string stringFirstStartDate = ConfigurationManager.AppSettings["StartFirstAcademicYear"];
                int curYear = AcademicYearOfProvinceBusiness.GetCurrentYearAndSemester(_globalInfo.ProvinceID.GetValueOrDefault(), out curSemester, stringFirstStartDate);
                // Nam hoc
                ViewData[ReportInputMonitoringSituationConstants.LS_YEAR] = new SelectList(lstAcademicYear, "Year", "DisplayTitle", curYear);
            }

            ViewData[ReportInputMonitoringSituationConstants.LIST_SEMESTER] = new SelectList(CommonList.Semester(), "key", "value");

            // cap hoc
            var lstAppliedLevel = CommonList.AppliedLevelName();
            //Tinh thanh
            List<Province> lstProvince = new List<Province>();
            if (_globalInfo.IsSystemAdmin)
            {
                lstProvince = ProvinceBusiness.All.Where(p => p.IsActive == true).OrderBy(p=>p.ProvinceName).ToList();
            }
            else
            {
                var objProvince = ProvinceBusiness.All.Where(p => p.ProvinceID == _globalInfo.ProvinceID).FirstOrDefault();
                lstProvince.Add(objProvince);
            }
            ViewData[ReportInputMonitoringSituationConstants.LIST_PROVINCE] = new SelectList(lstProvince, "ProvinceID", "ProvinceName");

            if (_globalInfo.IsSystemAdmin)
            {
                ViewData[ReportInputMonitoringSituationConstants.DISABLE_PROVINCE] = false;
                ViewData[ReportInputMonitoringSituationConstants.DISABLE_DISTRICT] = false;
            }
            else
            {

                ViewData[ReportInputMonitoringSituationConstants.DISABLE_PROVINCE] = true;
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    ViewData[ReportInputMonitoringSituationConstants.DISABLE_DISTRICT] = false;
                }
                else
                {
                    ViewData[ReportInputMonitoringSituationConstants.DISABLE_DISTRICT] = true;
                }

            }
            // quyen huyen
            List<District> lstDictrict = new List<District>();
            if (!_globalInfo.IsSystemAdmin)
            {
                IQueryable<District> iqtDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == _globalInfo.ProvinceID && o.IsActive == true);
                if (_globalInfo.IsSubSuperVisingDeptRole)
                {
                    iqtDistrict = iqtDistrict.Where(o => o.DistrictID == _globalInfo.DistrictID);
                    lstAppliedLevel.RemoveAt(2);
                }
                lstDictrict = iqtDistrict.OrderBy(p => p.DistrictName).ToList();
            }
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                ViewData[ReportInputMonitoringSituationConstants.LS_DISTRICT] = new SelectList(lstDictrict, "DistrictID", "DistrictName",_globalInfo.DistrictID);
            }
            else
            {
                ViewData[ReportInputMonitoringSituationConstants.LS_DISTRICT] = new SelectList(lstDictrict, "DistrictID", "DistrictName");
            }
            ViewData[ReportInputMonitoringSituationConstants.LS_EDU] = new SelectList(lstAppliedLevel, "key", "value", _globalInfo.IsSubSuperVisingDeptRole ? 2 : 3);
        }
        #endregion

        public JsonResult AjaxLoadDistrict(int? provinceId)
        {

            IQueryable<District> lstDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == provinceId && o.IsActive == true);
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                lstDistrict = lstDistrict.Where(o => o.DistrictID == _globalInfo.DistrictID);                
            }
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                return Json(new SelectList(lstDistrict.OrderBy(p => p.DistrictName).ToList(), "DistrictID", "DistrictName", _globalInfo.DistrictID));
            }
            else
            {
                return Json(new SelectList(lstDistrict.OrderBy(p => p.DistrictName).ToList(), "DistrictID", "DistrictName"));
            }
        }
        public JsonResult AjaxLoadSemester(int AppliedLevelID)
        {
            int curSemester = 0;
            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                curSemester = _globalInfo.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? 1 : 3;
                return Json(new SelectList(CommonList.Semester_GeneralSummed(), "key", "value", curSemester));
            }
            else 
            {
                return Json(new SelectList(CommonList.Semester(), "key", "value", _globalInfo.Semester));
            }
        }
        [ValidateAntiForgeryToken]
        public JsonResult CreatedReport(SearchViewModel data)
        {
            GlobalInfo global = GlobalInfo.getInstance();
            int SemesterID = 0;
            if (data.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                if (data.Semester == 1 || data.Semester == 3)
                {
                    SemesterID = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                }
                else
                {
                    SemesterID = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                }
            }
            else
            {
                SemesterID = data.Semester;
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ProvinceID"] = data.ProvinceID;
            SearchInfo["DistrictID"] = data.DistrictID;
            SearchInfo["SuperVisingDeptID"] = GetRole();
            SearchInfo["Year"] = data.AcademicYearID;
            SearchInfo["ReportCode"] = GetReportCode(data.AppliedLevel);
            SearchInfo["MarkType"] = data.ReportType;
            SearchInfo["Semester"] = SemesterID;
            SearchInfo["AppliedLevel"] = data.AppliedLevel;
            if (_globalInfo.IsSystemAdmin)
            {
                SearchInfo["UnitId"] = 0;
            }
            else
            {
                SearchInfo["UnitId"] = global.SupervisingDeptID;
            }
            if (data.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                SearchInfo["ReportType"] = data.Semester;
            }
            InputStatisticsBusiness.CreateDataInputSituation(SearchInfo);
            return Json(new { Type = "success" });
        }

        public FileResult ExportReport(int reportType, int year, int semester, int appliedLevel, int provinceID, int districtID)
        {

            GlobalInfo global = GlobalInfo.getInstance();
            int SemesterID = 0;
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY) 
            {
                if (semester == 1 || semester == 3)
                {
                    SemesterID = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                }
                else
                {
                    SemesterID = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                }
            }
            else 
            {
                SemesterID = semester;
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ProvinceID"] = provinceID;
            SearchInfo["DistrictID"] = districtID;
            SearchInfo["SuperVisingDeptID"] = GetRole();
            string reportCode = GetReportCode(appliedLevel);
            SearchInfo["ReportCode"] = reportCode;
            SearchInfo["MarkType"] = reportType;
            SearchInfo["Semester"] = SemesterID;
            SearchInfo["AppliedLevel"] = appliedLevel;
            SearchInfo["ReportType"] = semester;
            if (_globalInfo.IsSystemAdmin)
            {
                SearchInfo["UnitId"] = 0;
            }
            else
            {
                SearchInfo["UnitId"] = global.SupervisingDeptID;
            }
            SearchInfo["Year"] = year;
            Stream excel = InputStatisticsBusiness.ExportInputMonitoringSituation(SearchInfo);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string fileName = string.Empty;
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                fileName = String.Format("{0}_{1}.xls", GetFileName(appliedLevel), semester == 1 ? "GiuaHKI" : semester == 2 ? "GiuaHKII" : semester == 3 ? "CuoiHKI" : "CuoiHKII" );
            }
            else 
            {
                fileName = String.Format("{0}_{1}.xls", GetFileName(appliedLevel),SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII");
            }
            result.FileDownloadName = fileName;
            return result;
        }

        private string GetReportCode(int appliedLevel)
        {
            if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                return SystemParamsInFile.AdditionReport.REPORT_DATA_INPUT_MONITOR_PRIMARY;
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || appliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                return SystemParamsInFile.AdditionReport.REPORT_DATA_INPUT_MONITOR_TERTIARY;
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN || appliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE)
            {
                return SystemParamsInFile.AdditionReport.REPORT_DATA_INPUT_MONITOR_KINDER_GARTEN;
            }
            return "";
        }
        private int GetRole()
        {
            if (_globalInfo.IsSystemAdmin)
            {
                return 2;
            }
            else if (_globalInfo.IsSuperVisingDeptRole)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        private string GetFileName(int appliedLevel)
        {
            string fileName = "";
            if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE)
            {
                //fileName = SystemParamsInFile.AdditionReport.REPORT_DATA_INPUT_MONITOR_KINDER_GARTEN;
                fileName = SystemParamsInFile.AdditionReport.REPORT_DATA_INPUT_MONITOR_KINDER_GARTEN.Replace("NT_", "MN_");
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN)
            {
                fileName = SystemParamsInFile.AdditionReport.REPORT_DATA_INPUT_MONITOR_KINDER_GARTEN.Replace("NT_", "MN_");
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                fileName = SystemParamsInFile.AdditionReport.REPORT_DATA_INPUT_MONITOR_PRIMARY;
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                fileName = SystemParamsInFile.AdditionReport.REPORT_DATA_INPUT_MONITOR_TERTIARY.Replace("THPT_", "THCS_");
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                fileName = SystemParamsInFile.AdditionReport.REPORT_DATA_INPUT_MONITOR_TERTIARY;
            }
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                fileName = fileName.Replace("SGD", "PGD");
            }
            return fileName;
        }

    }
}
