﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportInputMonitoringSituationArea
{
    public class ReportInputMonitoringSituationConstants
    {
        /// <summary>
        /// danh sach quan huyen
        /// </summary>
        public const string LS_DISTRICT = "LS_DISTRICT";
        
        /// <summary>
        /// danh sach nam hoc
        /// </summary>
        public const string LS_YEAR = "LS_YEAR";
        
        /// <summary>
        /// danh sach cap hoc
        /// </summary>
        public const string LS_EDU = "LS_EDU";
        
        /// <summary>
        /// danh sach loai report
        /// </summary>
        public const string LS_TYPE = "LS_TYPE";

        /// <summary>
        /// noi dung bao cao
        /// </summary>
        public const string LS_DATA = "LS_DATA";

        /// <summary>
        /// string " - "
        /// </summary>
        public const string DASH = " - ";

        /// <summary>
        /// loai bao cao
        /// </summary>
        public const string REPORT_TYPE = "REPORT_TYPE";


        public const string LIST_SEMESTER = "LIST_SEMESTER";

        public const string LIST_PROVINCE = "LIST_PROVINCE";

        public const string DISABLE_PROVINCE = "DISABLE_PROVINCE";
        public const string DISABLE_DISTRICT = "DISABLE_DISTRICT";
        /// <summary>
        /// thong ke chi tiet
        /// </summary>
        public const int REPORT_DETAIL = 1;

        /// <summary>
        /// thong ke quan/huyen
        /// </summary>
        public const int REPORT_DISTRICT = 2;

        /// <summary>
        /// thong ke truong
        /// </summary>
        public const int REPORT_SCHOOL = 3;

        /// <summary>
        /// page size
        /// </summary>
        public const int PAGE_SIZE = 15;

        /// <summary>
        /// tong so record
        /// </summary>
        public const string TOTAL_RECORD = "TOTAL_RECORD";
    }
}