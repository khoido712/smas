﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.ReportInputMonitoringSituationArea.Models
{
    public class SearchViewModel
    {
        /*/// <summary>
        /// quan/huyen
        /// </summary>
        [ResourceDisplayName("ReportInputMonitoringSituation_Label_District")]
        public Nullable<int> DistrictID { get; set; }

        /// <summary>
        /// nam hoc
        /// </summary>
        [ResourceDisplayName("ReportInputMonitoringSituation_Label_AcademicYear")]
        public string Year { get; set; }

        /// <summary>
        /// cap hoc
        /// </summary>
        [ResourceDisplayName("ReportInputMonitoringSituation_Label_EducationLevelID")]
        public Nullable<int> EducationGradeID { get; set; }

        /// <summary>
        /// loai bao cao
        /// </summary>
        [ResourceDisplayName("ReportInputMonitoringSituation_Label_ReportType")]
        public Nullable<int> ReportType { get; set; }

        /// <summary>
        /// ngay bao cao
        /// </summary>
        //[ResDisplayName("ReportInputMonitoringSituation_Label_ReportDate")]
        //public DateTime ReportDate { get; set; }*/

        public int ReportType { get; set; }
        public int ProvinceID { get; set; }
        public int DistrictID { get; set; }
        public int AcademicYearID { get; set; }
        public int AppliedLevel { get; set; }
        public int Semester { get; set; }
        
    }
}