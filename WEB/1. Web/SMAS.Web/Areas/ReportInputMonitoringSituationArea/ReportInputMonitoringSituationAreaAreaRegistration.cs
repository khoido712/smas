﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ReportInputMonitoringSituationArea
{
    public class ReportInputMonitoringSituationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ReportInputMonitoringSituationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ReportInputMonitoringSituationArea_default",
                "ReportInputMonitoringSituationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
