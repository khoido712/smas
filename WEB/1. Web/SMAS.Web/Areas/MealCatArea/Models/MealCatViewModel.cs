/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.MealCatArea.Models
{
    public class MealCatViewModel
    {
        [ScaffoldColumn(false)]
        public System.Int32 MealCatID { get; set; }

        [ResourceDisplayName("MealCat_Label_MealName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
		public System.String MealName { get; set; }

        [ResourceDisplayName("MealCat_Label_Note")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
		public System.String Note { get; set; }

        [ScaffoldColumn(false)]
        public System.Int32 MealID { get; set; }
        [ScaffoldColumn(false)]
        public DateTime CreateDate { get; set; }
    }
}


