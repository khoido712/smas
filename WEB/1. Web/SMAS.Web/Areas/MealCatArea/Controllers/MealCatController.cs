﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.MealCatArea.Models;

using SMAS.Models.Models;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.MealCatArea.Controllers
{
    public class MealCatController : BaseController
    {        
        private readonly IMealCatBusiness MealCatBusiness;
        private readonly IDailyMenuDetailBusiness DailyMenuDetailBusiness;

        public MealCatController(IMealCatBusiness mealcatBusiness, IDailyMenuDetailBusiness dailyMenuDetailBusiness)
		{
			this.MealCatBusiness = mealcatBusiness;
            this.DailyMenuDetailBusiness = dailyMenuDetailBusiness;
		}
		
		//
        // GET: /MealCat/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["IsActive"] = true;

            //Get view data here

            IEnumerable<MealCatViewModel> lst = this._Search(SearchInfo);
            IEnumerable<int> lstDailyMenuDetail = DailyMenuDetailBusiness.All.Select(p=>p.MealID).Distinct();
            var list = (from mc in lst
                        join dmd in lstDailyMenuDetail on mc.MealCatID equals dmd
                        into g1
                        from g2 in g1.DefaultIfEmpty()
                        select new MealCatViewModel
                        {
                            MealCatID = mc.MealCatID,
                            MealName = mc.MealName,
                            Note = mc.Note,
                            CreateDate = mc.CreateDate,
                            MealID = g2,
                        }).OrderBy(p => p.CreateDate).ToList();

            ViewData[MealCatConstants.LIST_MEALCAT] = list;
            return View();
        }

		//
        // GET: /MealCat/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
			//add search info
			//

            IEnumerable<MealCatViewModel> lst = this._Search(SearchInfo);
            IEnumerable<int> lstDailyMenuDetail = DailyMenuDetailBusiness.All.Select(p => p.MealID).Distinct();
            var list = (from mc in lst
                        join dmd in lstDailyMenuDetail on mc.MealCatID equals dmd
                        into g1
                        from g2 in g1.DefaultIfEmpty()
                        select new MealCatViewModel
                        {
                            MealCatID = mc.MealCatID,
                            MealName = mc.MealName,
                            Note = mc.Note,
                            MealID = g2
                        }); 
            ViewData[MealCatConstants.LIST_MEALCAT] = list;
            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            MealCat mealcat = new MealCat();
            TryUpdateModel(mealcat); 
            Utils.Utils.TrimObject(mealcat);

            mealcat.SchoolID = new GlobalInfo().SchoolID.Value;
            this.MealCatBusiness.Insert(mealcat);
            this.MealCatBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int MealCatID)
        {
            MealCat mealcat = this.MealCatBusiness.Find(MealCatID);
            TryUpdateModel(mealcat);
            Utils.Utils.TrimObject(mealcat);
            this.MealCatBusiness.Update(mealcat);
            this.MealCatBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.MealCatBusiness.Delete(id);
            this.MealCatBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<MealCatViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<MealCat> query = this.MealCatBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value,SearchInfo);
            List<MealCatViewModel> lst = query.Select(o => new MealCatViewModel { MealName = o.MealName,								
						Note = o.Note,								
						MealCatID = o.MealCatID,
						CreateDate = o.CreatedDate		
            }).ToList();
            for (int i = 0, size = lst.Count; i < size; i++)
            {
                lst[i].Note = Utils.Utils.ConvertStrDownLine(lst[i].Note);
            }
            return lst;
        }
        
    }
}





