﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationDevelopmentGroupArea
{
    public class EvaluationDevelopmentGroupConstants
    {
        public const string LISTPROVINCE = "LISTPROVINCE";//all
        public const string LIST_EDUCATIONLEVEL = "ListEducation";
        public const string LIST_STATUS = "ListStatus";
        public const string TOTAL = "total";
        public const string PAGE_NUMBER = "position of page";
        public const string LIST_RESULT = "listResult";
        public const string LIST_EDUCATION_CREATE = "ListEducationCreate";
        public const string LIST_EDUCATION_EDIT = "ListEducationEdit";
        public const string LIST_PROVINCE_CREATE = "ListProvinceCreate";
        public const string LIST_PROVINCE_EDIT = "ListProvinceEdit";
        public const string SHOW_ORDER = "ShowOrder";
    }
}