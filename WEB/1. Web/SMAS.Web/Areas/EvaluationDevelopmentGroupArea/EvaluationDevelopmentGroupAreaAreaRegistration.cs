﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.EvaluationDevelopmentGroupArea
{
    public class EvaluationDevelopmentGroupAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "EvaluationDevelopmentGroupArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "EvaluationDevelopmentGroupArea_default",
                "EvaluationDevelopmentGroupArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
