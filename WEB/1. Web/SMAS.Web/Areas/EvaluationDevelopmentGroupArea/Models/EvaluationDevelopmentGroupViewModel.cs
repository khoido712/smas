﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationDevelopmentGroupArea.Models
{
    public class EvaluationDevelopmentGroupViewModel
    {
        public int? ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public int EvaluationGroupID { get; set; }
        public string EvaluationGroupCode { get; set; }
        public string EvaluationGroupName { get; set; }
        public int EducationID { get; set; }
        public string EducationName { get; set; }
        public string Description { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public int? OrderBy { get; set; }

        public bool ShowOrderBy { get; set; }
    }
}