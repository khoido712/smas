﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationDevelopmentGroupArea.Models
{
    public class StatusViewModel
    {
        public int StatusID { get; set; }
        public string StatusName { get; set; }
    }
}