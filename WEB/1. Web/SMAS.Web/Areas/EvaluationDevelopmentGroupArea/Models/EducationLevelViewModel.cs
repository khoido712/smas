﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.EvaluationDevelopmentGroupArea.Models
{
    public class EducationLevelViewModel
    {
        public int EducationLevel { get; set; }
        public int AppliedLevel { get; set; }
    }
}