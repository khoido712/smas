﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.EvaluationDevelopmentGroupArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using Telerik.Web.Mvc;


namespace SMAS.Web.Areas.EvaluationDevelopmentGroupArea.Controllers
{
    public class EvaluationDevelopmentGroupController : BaseController
    {
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness;
        private readonly IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness;
        private readonly IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness;

        public EvaluationDevelopmentGroupController(
            IProvinceBusiness ProvinceBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IEducationLevelBusiness EducationLevelBusiness,
            IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness,
            IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness,
            IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness)
        {
            this.ProvinceBusiness = ProvinceBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.DeclareEvaluationGroupBusiness = DeclareEvaluationGroupBusiness;
            this.EvaluationDevelopmentGroupBusiness = EvaluationDevelopmentGroupBusiness;
            this.DeclareEvaluationIndexBusiness = DeclareEvaluationIndexBusiness;
        }

        //
        // GET: /EvaluationDevelopmentGroupArea/EvaluationDevelopmentGroup/

        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        private void SetViewData()
        {           
            this.GetProvince(EvaluationDevelopmentGroupConstants.LISTPROVINCE);
            ViewData[EvaluationDevelopmentGroupConstants.LIST_EDUCATIONLEVEL] = new SelectList(ListEducationLevel(), "EducationLevelID", "Resolution");

            List<StatusViewModel> lstStatus = new List<StatusViewModel>();
            lstStatus.Add(new StatusViewModel { StatusID = 1, StatusName = "Đang hoạt động" });
            lstStatus.Add(new StatusViewModel { StatusID = 2, StatusName = "Tạm ngưng" });
            ViewData[EvaluationDevelopmentGroupConstants.LIST_STATUS] = new SelectList(lstStatus, "StatusID", "StatusName");
        }

        private void GetProvince(string strConstant)
        {
            List<Province> lsProvince = ListProvince();
            if (_globalInfo.IsSuperVisingDeptRole)
            {
                int province = _globalInfo.ProvinceID.Value;
                lsProvince = lsProvince.Where(x => x.ProvinceID == province).ToList();

                ViewData[strConstant] = new SelectList(lsProvince, "ProvinceID", "ProvinceName", province);
            } // admin
            else
            {
                List<ProvinceViewModel> lstProvinceNew = new List<ProvinceViewModel>();
                lstProvinceNew.Add(new ProvinceViewModel { ProvinceID = SystemParamsInFile.COUNTRY, ProvinceName = SystemParamsInFile.COUNTRY_NAME, OrderBy = 1 });
                for (int i = 0; i < lsProvince.Count(); i++)
                {
                    lstProvinceNew.Add(new ProvinceViewModel { ProvinceID = lsProvince[i].ProvinceID, ProvinceName = lsProvince[i].ProvinceName, OrderBy = (i + 2) });
                }
                lstProvinceNew = lstProvinceNew.OrderBy(x => x.OrderBy).ToList();
                ViewData[strConstant] = new SelectList(lstProvinceNew, "ProvinceID", "ProvinceName");
            }
        }

        private List<Province> ListProvince()
        {
            return ProvinceBusiness.Search(new Dictionary<string, object>() { })
               .OrderBy(o => o.ProvinceName).ToList();
        }

        private List<EducationLevel> ListEducationLevel()
        {
            return EducationLevelBusiness.All.Where(x => (x.Grade == 4 || x.Grade == 5) && x.IsActive == true)
                                            .OrderBy(x => x.EducationLevelID).ToList();
        }

        public PartialViewResult GetInfoEducation()
        {
            List<EducationLevel> lstEdu = ListEducationLevel();
            ViewData[EvaluationDevelopmentGroupConstants.LIST_EDUCATION_CREATE] = lstEdu;

            this.GetProvince(EvaluationDevelopmentGroupConstants.LIST_PROVINCE_CREATE);
            return PartialView("_Create");
        }

        public PartialViewResult GetInfoEditEducation(int evaluationId)
        {
            List<EducationLevel> lstEdu = ListEducationLevel();
            ViewData[EvaluationDevelopmentGroupConstants.LIST_EDUCATION_EDIT] = lstEdu;

            this.GetProvince(EvaluationDevelopmentGroupConstants.LIST_PROVINCE_EDIT);

            /*IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            EvaluationDevelopmentGroupViewModel objVM = this._Search(SearchInfo).Where(x => x.EvaluationGroupID == evaluationId).FirstOrDefault();

            int provinceId = 0;
            string evalName = string.Empty;
            string evalCode = string.Empty;
            string description = string.Empty;
            int educationId = 0;

            if (objVM != null)
            {
                evalName = objVM.EvaluationGroupName;
                evalCode = objVM.EvaluationGroupCode;
                description = objVM.Description;
                educationId = objVM.EducationID;
                provinceId = objVM.ProvinceID.Value;

                ViewData["A1"] = evalName;
                ViewData["A2"] = evalCode;
                ViewData["A3"] = description;
                ViewData["A4"] = educationId;
                ViewData["A5"] = provinceId;
            }*/
            return PartialView("_Edit");
        }

        [ValidateAntiForgeryToken]
        public JsonResult GetInfoEdit(int evaluationId)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            EvaluationDevelopmentGroupViewModel objVM = this._Search(SearchInfo).Where(x => x.EvaluationGroupID == evaluationId).FirstOrDefault();

            int provinceId = 0;
            string evalName = string.Empty;
            string evalCode = string.Empty;
            string description = string.Empty;
            int educationId = 0;
            int status = 0;

            if (objVM != null)
            {
                evalName = objVM.EvaluationGroupName;
                evalCode = objVM.EvaluationGroupCode;
                description = objVM.Description;
                educationId = objVM.EducationID;
                provinceId = objVM.ProvinceID.Value;
                status = objVM.StatusID;
            }

            return Json(new
            {
                evalName = evalName,
                evalCode = evalCode,
                description = description,
                educationId = educationId,
                provinceId = provinceId,
                status = status
            });
        }


        public PartialViewResult Search(int? eduId,
            int? provinceId, int? statusId,
            string evaluaCode, string evaluaName, bool showOrder)
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //if (eduId > 0)
            SearchInfo["EducationLevelID"] = eduId.HasValue == true ? eduId.Value : 0;
            //if (provinceId > 0)

            if (_globalInfo.IsSuperVisingDeptRole)
            {
                SearchInfo["ProvinceID"] = _globalInfo.ProvinceID;
            }
            else
            {
                SearchInfo["ProvinceID"] = provinceId.HasValue == true ? provinceId.Value : 0;
            }

            // if (statusId > 0)
            SearchInfo["StatusID"] = statusId.HasValue == true ? statusId.Value : 0;
            // if(!string.IsNullOrEmpty(evaluaCode))
            SearchInfo["EvaluaCode"] = evaluaCode != string.Empty ? evaluaCode : string.Empty;
            // if (!string.IsNullOrEmpty(evaluaName))
            SearchInfo["EvaluaName"] = evaluaName != string.Empty ? evaluaName : string.Empty;

            //SearchInfo["showOrder"] = showOrder;
            // Page 1
            List<EvaluationDevelopmentGroupViewModel> lst = this._Search(SearchInfo).ToList();
            Session[EvaluationDevelopmentGroupConstants.LIST_RESULT] = lst;
            List<EvaluationDevelopmentGroupViewModel> lstPage1 = lst.Skip(0).Take(15).ToList();
            ViewData[EvaluationDevelopmentGroupConstants.TOTAL] = lst.Count;
            ViewData[EvaluationDevelopmentGroupConstants.PAGE_NUMBER] = 1;
            ViewData[EvaluationDevelopmentGroupConstants.LIST_RESULT] = lstPage1;
            ViewData[EvaluationDevelopmentGroupConstants.SHOW_ORDER] = showOrder;
            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<EvaluationDevelopmentGroupViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            //bool shwOrder = false;
            //shwOrder = (bool)SearchInfo["showOrder"];
            //this.SetViewDataPermission("EvaluationDevelopmentGroup", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID);
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            List<EvaluationDevelopmentGroup> lstDB = EvaluationDevelopmentGroupBusiness.SearchEval(SearchInfo).ToList();
            List<EducationLevel> lstEducation = ListEducationLevel();

            List<EvaluationDevelopmentGroupViewModel> lstResult = new List<EvaluationDevelopmentGroupViewModel>();
            List<Province> lstProvince = ListProvince();
            //Province obP = null;
            var lstSearch = (from lst in lstDB
                             join lstEdu in lstEducation
                             on lst.EducationLevelID equals lstEdu.EducationLevelID
                             select new EvaluationDevelopmentGroupViewModel()
                             {
                                 // ShowOrderBy = shwOrder,
                                 EducationID = lst.EducationLevelID,
                                 EducationName = lstEdu.Resolution,
                                 EvaluationGroupName = lst.EvaluationDevelopmentGroupName,
                                 EvaluationGroupCode = lst.EvaluationDGCode,
                                 StatusName = lst.IsActive == true ? "Đang hoạt động" : "Tạm ngưng",
                                 StatusID = lst.IsActive == true ? 1 : 0,
                                 Description = lst.Description,
                                 EvaluationGroupID = lst.EvaluationDevelopmentGroupID,
                                 OrderBy = lst.OrderID,
                                 ProvinceID = lst.ProvinceID != null && lst.ProvinceID != -1 ? lst.ProvinceID : SystemParamsInFile.COUNTRY,
                                 ProvinceName = lst.ProvinceID != null && lst.ProvinceID != -1 ? lstProvince.Where(x => x.ProvinceID == lst.ProvinceID).FirstOrDefault().ProvinceName : SystemParamsInFile.COUNTRY_NAME,
                             }).ToList();

            var lstNoCountry = lstSearch.Where(x => x.ProvinceID != SystemParamsInFile.COUNTRY)
                                        .OrderBy(x => x.ProvinceName).ThenBy(x => x.EducationID)
                                        .ThenBy(x => x.EvaluationGroupName).ToList();

            var lstIsCountry = lstSearch.Where(x => x.ProvinceID == SystemParamsInFile.COUNTRY).ToList();
            if (lstIsCountry.Count > 0)
            {
                lstIsCountry = lstIsCountry.OrderBy(x => x.EducationID).ThenBy(x => x.EvaluationGroupName).ToList();
                lstResult = lstIsCountry;
            }

            foreach (var item in lstNoCountry)
            {
                lstResult.Add(item);
            }

            return lstResult;
        }

        /// <summary>
        /// Tìm kiếm có phân trang
        /// </summary>
        /// 
        [GridAction(EnableCustomBinding = true)]
        [ValidateAntiForgeryToken]
        public ActionResult _GridBinding(GridCommand command)
        {
            int Page = 1;
            if (command != null)
                Page = command.Page;

            IEnumerable<EvaluationDevelopmentGroupViewModel> lst;
            if (Session[EvaluationDevelopmentGroupConstants.LIST_RESULT] != null)
            {
                lst = (IEnumerable<EvaluationDevelopmentGroupViewModel>)Session[EvaluationDevelopmentGroupConstants.LIST_RESULT];
            }
            else
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                //SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                lst = this._Search(SearchInfo);
            }

            List<EvaluationDevelopmentGroupViewModel> lstAtCurrentPage = lst.Skip((Page - 1) * 15).Take(15).ToList();
            ViewData[EvaluationDevelopmentGroupConstants.LIST_RESULT] = lstAtCurrentPage;
            GridModel<EvaluationDevelopmentGroupViewModel> gm = new GridModel<EvaluationDevelopmentGroupViewModel>(lstAtCurrentPage);
            gm.Total = lst.Count();
            return View(gm);
        }

        private bool SetIsNullOrEmpty(string evalName, string evalCode, string arrEdu)
        {
            if (!string.IsNullOrEmpty(evalName) && !string.IsNullOrEmpty(evalCode) && !string.IsNullOrEmpty(arrEdu))
                return true;
            return false;
        }

        private bool SetIsNullOrEmptyEdit(string evalName, string evalCode)
        {
            if (!string.IsNullOrEmpty(evalName) && !string.IsNullOrEmpty(evalCode))
                return true;
            return false;
        }

        #region SaveEvaluationDevGro
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SaveEvaluationDevGro(FormCollection frm)
        {
            int provinceId = !string.IsNullOrEmpty(frm["provinceId"]) ? int.Parse(frm["provinceId"]) : 0;
            string evaluationName = !string.IsNullOrEmpty(frm["evaluationName"]) ? frm["evaluationName"] : string.Empty;
            string evaluationCode = !string.IsNullOrEmpty(frm["evaluationCode"]) ? frm["evaluationCode"] : string.Empty;
            string description = !string.IsNullOrEmpty(frm["description"]) ? frm["description"] : string.Empty;
            string arrEducationID = !string.IsNullOrEmpty(frm["arrEducationIdSave"]) ? frm["arrEducationIdSave"] : string.Empty;

            if (provinceId != 0 && this.SetIsNullOrEmpty(evaluationName, evaluationCode, arrEducationID))
            {
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    if (_globalInfo.ProvinceID.Value != provinceId)
                    {
                        return Json(new JsonMessage(Res.Get("Thêm mới thất bại"), "error"));
                    }
                }

                string strPattern = @"[\s]+";
                Regex rgx = new Regex(strPattern);
                string evalName = rgx.Replace(evaluationName, " ");
                string evalCode = rgx.Replace(evaluationCode, " ");
                evalCode = evalCode.ToUpper();
                arrEducationID = arrEducationID.Replace(", ", ",");
                string arrEducationChoose = rgx.Replace(arrEducationID, "");

                List<int> arrEducation = new List<int>();
                arrEducation = arrEducationChoose.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();

                List<EducationLevel> lstEdu = ListEducationLevel();
                List<EducationLevelViewModel> lstEducationApplied = new List<EducationLevelViewModel>();

                foreach (var item in arrEducation)
                {
                    foreach (var item2 in lstEdu)
                    {
                        if (item == item2.EducationLevelID)
                        {
                            lstEducationApplied.Add(new EducationLevelViewModel { EducationLevel = item, AppliedLevel = item2.Grade });
                            break;
                        }
                    }
                }
                lstEducationApplied = lstEducationApplied.OrderBy(x => x.EducationLevel).ToList();

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                List<EvaluationDevelopmentGroup> lstDB = EvaluationDevelopmentGroupBusiness.SearchEval(SearchInfo).ToList();
                if (lstDB.Count > 0)
                {
                    var testCode = lstDB.Where(x => !string.IsNullOrEmpty(x.EvaluationDGCode) && x.EvaluationDGCode.ToUpper() == evalCode.ToUpper()
                        && (x.ProvinceID == provinceId)
                        && arrEducationChoose.Contains(x.EducationLevelID.ToString())).FirstOrDefault();
                    if (testCode != null)
                    {
                        string message = string.Format("Trong một Tỉnh/Thành các lĩnh vực cùng nhóm lớp thì ký hiệu không được trùng nhau.");
                        return Json(new JsonMessage(message, "error"));
                    }

                    var testName = lstDB.Where(x => x.EvaluationDevelopmentGroupName.ToUpper() == evalName.ToUpper()
                        && (x.ProvinceID == provinceId)
                        && arrEducationChoose.Contains(x.EducationLevelID.ToString())).FirstOrDefault();
                    if (testName != null)
                    {
                        string message = string.Format("Trong một Tỉnh/Thành các lĩnh vực cùng nhóm lớp thì tên lĩnh vực không được trùng nhau.");
                        return Json(new JsonMessage(message, "error"));
                    }
                }

                if (lstEducationApplied.Count > 0)
                {
                    List<EvaluationDevelopmentGroup> lstInput = new List<EvaluationDevelopmentGroup>();
                    EvaluationDevelopmentGroup objInput = null;

                    var orderDB = lstDB.Where(x => x.ProvinceID == provinceId).OrderByDescending(x=>x.OrderID).FirstOrDefault();

                    int orderBy = 1;
                    if (orderDB != null)
                        orderBy = orderDB.OrderID.Value;

                    int i = 1;                   
                    foreach (var item in lstEducationApplied)
                    {
                        objInput = new EvaluationDevelopmentGroup()
                        {
                            ProvinceID = provinceId,
                            EvaluationDGCode = evalCode,
                            EvaluationDevelopmentGroupName = evalName,
                            AppliedLevel = item.AppliedLevel,
                            EducationLevelID = item.EducationLevel,
                            IsActive = true,
                            Description = description,
                            CreatedDate = DateTime.Now,
                            OrderID = (orderBy + i),
                        };
                        lstInput.Add(objInput);
                        i++;
                    }
                    this.EvaluationDevelopmentGroupBusiness.InsertEval(lstInput);
                    return Json(new JsonMessage(Res.Get("Thêm mới thành công"), "success"));
                }
                return Json(new JsonMessage(Res.Get("Thêm mới thất bại"), "error"));
            }
            return Json(new JsonMessage(Res.Get("Thêm mới thất bại"), "error"));
        }
        #endregion

        #region EditEvaluationDevGro
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult EditEvaluationDevGro(FormCollection frm)
        {
            int evalId = !string.IsNullOrEmpty(frm["evalIdEdit"]) ? int.Parse(frm["evalIdEdit"]) : 0;
            int provinceId = !string.IsNullOrEmpty(frm["provinceIdEdit"]) ? int.Parse(frm["provinceIdEdit"]) : 0;
            int educationId = !string.IsNullOrEmpty(frm["educationIdEdit"]) ? int.Parse(frm["educationIdEdit"]) : 0;
            int provinceIdOld = !string.IsNullOrEmpty(frm["provinceIdOld"]) ? int.Parse(frm["provinceIdOld"]) : 0;
            string evaluationName = !string.IsNullOrEmpty(frm["evaluationNameEdit"]) ? frm["evaluationNameEdit"] : string.Empty;
            string evaluationCode = !string.IsNullOrEmpty(frm["evaluationCodeEdit"]) ? frm["evaluationCodeEdit"] : string.Empty;
            string description = !string.IsNullOrEmpty(frm["descriptionEdit"]) ? frm["descriptionEdit"] : string.Empty;

            if (provinceId != 0 && provinceIdOld != 0 && evalId != 0 && educationId != 0
                && this.SetIsNullOrEmptyEdit(evaluationName, evaluationCode))
            {
                if (_globalInfo.IsSuperVisingDeptRole)
                {
                    if (_globalInfo.ProvinceID.Value != provinceId && _globalInfo.ProvinceID.Value != provinceIdOld)
                    {
                        return Json(new JsonMessage(Res.Get("Cập nhật thất bại"), "error"));
                    }
                }

                string strPattern = @"[\s]+";
                Regex rgx = new Regex(strPattern);
                string evalName = rgx.Replace(evaluationName, " ");
                string evalCode = rgx.Replace(evaluationCode, " ");
                evalCode = evalCode.ToUpper();

                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                List<EvaluationDevelopmentGroup> lstDB = EvaluationDevelopmentGroupBusiness.SearchEval(SearchInfo).ToList();
                if (lstDB.Count > 0)
                {
                    var testCode = lstDB.Where(x => !string.IsNullOrEmpty(x.EvaluationDGCode) && x.EvaluationDGCode.ToUpper() == evalCode.ToUpper()
                        && (x.ProvinceID == provinceIdOld)
                        && x.EvaluationDevelopmentGroupID != evalId
                        && x.EducationLevelID == educationId).FirstOrDefault();
                    if (testCode != null)
                    {
                        string message = string.Format("Trong một Tỉnh/Thành các lĩnh vực cùng nhóm lớp thì ký hiệu không được trùng nhau.");
                        return Json(new JsonMessage(message, "error"));
                    }

                    var testName = lstDB.Where(x => x.EvaluationDevelopmentGroupName.ToUpper() == evalName.ToUpper()
                        && (x.ProvinceID == provinceIdOld)
                        && x.EvaluationDevelopmentGroupID != evalId
                        && x.EducationLevelID == educationId).FirstOrDefault();
                    if (testName != null)
                    {
                        string message = string.Format("Trong một Tỉnh/Thành các lĩnh vực cùng nhóm lớp thì tên lĩnh vực không được trùng nhau.");
                        return Json(new JsonMessage(message, "error"));
                    }
                }

                bool _isActive = true;
                if ("on".Equals(frm["statusEdit"]))
                {
                    _isActive = false;
                }

                EvaluationDevelopmentGroup objInput = null;
                objInput = new EvaluationDevelopmentGroup()
                {
                    EvaluationDevelopmentGroupID = evalId,
                    ProvinceID = provinceId,
                    EvaluationDGCode = evalCode,
                    EvaluationDevelopmentGroupName = evalName,
                    IsActive = _isActive,
                    Description = description,
                    ModifiedDate = DateTime.Now,
                };

                this.EvaluationDevelopmentGroupBusiness.UpdateVal(objInput);
                return Json(new JsonMessage(Res.Get("Cập nhật thành công"), "success"));
            }
            return Json(new JsonMessage(Res.Get("Cập nhật thất bại"), "error"));
        }
        #endregion

        #region EditOrderBy
        [HttpPost]
        [ActionAudit]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult EditOrderBy(FormCollection frm)
        {
            int provinceSTT = !string.IsNullOrEmpty(frm["provinceSTT"]) ? int.Parse(frm["provinceSTT"]) : 0;
            int educationSTT = !string.IsNullOrEmpty(frm["educationSTT"]) ? int.Parse(frm["educationSTT"]) : 0;
            int statusSTT = !string.IsNullOrEmpty(frm["statusSTT"]) ? int.Parse(frm["statusSTT"]) : 0;
            string codeSTT = !string.IsNullOrEmpty(frm["codeSTT"]) ? frm["codeSTT"] : string.Empty;
            string nameSTT = !string.IsNullOrEmpty(frm["nameSTT"]) ? frm["nameSTT"] : string.Empty;

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EducationLevelID"] = educationSTT;
            SearchInfo["ProvinceID"] = provinceSTT;
            SearchInfo["EvaluaName"] = nameSTT;
            SearchInfo["EvaluaCode"] = codeSTT;
            SearchInfo["StatusID"] = statusSTT;
            List<EvaluationDevelopmentGroup> lstDB = EvaluationDevelopmentGroupBusiness.SearchEval(SearchInfo).ToList();
            List<EvaluationDevelopmentGroup> lstUpdate = new List<EvaluationDevelopmentGroup>();
            EvaluationDevelopmentGroup objUpdate = null;
            int valOrder = 0;
            foreach (var item in lstDB)
            {
                valOrder = !string.IsNullOrEmpty(frm["orderBy_" + item.EvaluationDevelopmentGroupID]) ? int.Parse(frm["orderBy_" + item.EvaluationDevelopmentGroupID]) : 0;
                if (valOrder != 0)
                {
                    objUpdate = new EvaluationDevelopmentGroup()
                    {
                        EvaluationDevelopmentGroupID = item.EvaluationDevelopmentGroupID,
                        OrderID = valOrder,
                        ModifiedDate = DateTime.Now
                    };
                    lstUpdate.Add(objUpdate);
                }
            }

            if (lstUpdate.Count > 0)
            {
                this.EvaluationDevelopmentGroupBusiness.UpdateOrderID(lstUpdate, lstDB);
                return Json(new JsonMessage(Res.Get("Cập nhật thành công"), "success"));
            }
            return Json(new JsonMessage(Res.Get("Cập nhật thất bại"), "error"));
        }
        #endregion

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            if (id > 0)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                EvaluationDevelopmentGroup objBD = EvaluationDevelopmentGroupBusiness.SearchEval(SearchInfo)
                    .Where(x => x.EvaluationDevelopmentGroupID == id).FirstOrDefault();

                if (objBD != null)
                {
                    DeclareEvaluationIndex objTest = DeclareEvaluationIndexBusiness.All.Where(x => x.EvaluationDevelopmentGroID == objBD.EvaluationDevelopmentGroupID).FirstOrDefault();
                    if (objTest != null)
                    {
                        return Json(new JsonMessage(Res.Get("Xóa không thành công. Lĩnh vực đánh giá đã được khai báo"), "error"));
                    }

                    this.EvaluationDevelopmentGroupBusiness.Delete(id);
                    this.EvaluationDevelopmentGroupBusiness.Save();
                    return Json(new JsonMessage(Res.Get("Xóa thành công"), "success"));
                }
                return Json(new JsonMessage(Res.Get("Xóa không thành công"), "error"));
            }
            return Json(new JsonMessage(Res.Get("Xóa không thành công"), "error"));
        }

    }
}
