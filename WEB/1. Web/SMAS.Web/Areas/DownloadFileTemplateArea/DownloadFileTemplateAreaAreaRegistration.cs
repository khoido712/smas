﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DownloadFileTemplateArea
{
    public class DownloadFileTemplateAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DownloadFileTemplateArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DownloadFileTemplateArea_default",
                "DownloadFileTemplateArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
