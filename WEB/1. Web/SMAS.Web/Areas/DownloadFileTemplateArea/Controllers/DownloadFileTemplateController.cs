﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.VTUtils.Excel.Export;
using System.IO;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.ComponentModel;
using SMAS.Web.Models.Attributes;
namespace SMAS.Web.Areas.DownloadFileTemplateArea.Controllers
{
    public class DownloadFileTemplateController: BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
        public FileResult DownloadFileTemplate()
        {
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HS" + "/" + "HS_SoGoiTenVaGhiDiem.xls";
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");

            string ReportName = "HS_SoGoiTenVaGhiDiem.xls";
            result.FileDownloadName = ReportName;
            return result;
        }
    }
}