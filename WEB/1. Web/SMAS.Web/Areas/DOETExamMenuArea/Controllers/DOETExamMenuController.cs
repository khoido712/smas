﻿using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamMenuArea.Controllers
{
    public class DOETExamMenuController:BaseController
    {
        public ActionResult Index()
        {
            string DOETExamMenuKey = "KEY_MENU_DOET_EXAM";
            string tab = _globalInfo.TabIndexDOETExam;
            List<MenuExamination> lstSubMenu = GetSubMenuForSupervisingDept(DOETExamMenuKey, tab);
           
            return Redirect(lstSubMenu.First().LinkMenu);
           
        }
    }
}