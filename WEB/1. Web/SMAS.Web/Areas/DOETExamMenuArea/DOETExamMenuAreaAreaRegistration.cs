﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamMenuArea
{
    public class DOETExamMenuAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DOETExamMenuArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DOETExamMenuArea_default",
                "DOETExamMenuArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
