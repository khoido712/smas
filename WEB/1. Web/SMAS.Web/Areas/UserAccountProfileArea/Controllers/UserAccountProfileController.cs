﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.UserAccountProfileArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using SMAS.Web.Areas.UserAccountProfileArea;
using SMAS.VTUtils.Excel.Export;
using System.Web.Mvc;
using System.Web.Security;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Drawing.Imaging;


namespace SMAS.Web.Areas.UserAccountProfileArea.Controllers
{
    public class UserAccountProfileController : BaseController
    {

        #region variable
        private readonly ITrainingLevelBusiness TrainingLevelBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IWorkTypeBusiness WorkTypeBusiness;
        private readonly IGraduationLevelBusiness GraduationLevelBusiness;
        private readonly IContractTypeBusiness ContractTypeBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IGroupCatBusiness GroupCatBusiness;
        private readonly IEthnicBusiness EthnicBusiness;
        private readonly IReligionBusiness ReligionBusiness;
        private readonly IFamilyTypeBusiness FamilyTypeBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;

        private readonly IQualificationTypeBusiness QualificationTypeBusiness;
        private readonly ISpecialityCatBusiness SpecialityCatBusiness;
        private readonly IQualificationLevelBusiness QualificationLevelBusiness;

        private readonly IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness;
        private readonly IITQualificationLevelBusiness ITQualificationLevelBusiness;

        private readonly IStateManagementGradeBusiness StateManagementGradeBusiness;
        private readonly IPoliticalGradeBusiness PoliticalGradeBusiness;

        private readonly IEducationalManagementGradeBusiness EducationalManagementGradeBusiness;

        private readonly IStaffPositionBusiness StaffPositionBusiness;
        private readonly IContractBusiness ContractBusiness;
        private readonly IWorkGroupTypeBusiness WorkGroupTypeBusiness;
        private readonly IEmployeeWorkingHistoryBusiness EmployeeWorkingHistoryBusiness;
        private ISubjectCatBusiness SubjectCatBusiness;
        private IQualificationGradeBusiness QualificationGradeBusiness;
        private IEmployeeQualificationBusiness EmployeeQualificationBusiness;
        private IEmployeeSalaryBusiness EmployeeSalaryBusiness;
        private IEmployeeScaleBusiness EmployeeScaleBusiness;
        private string GRADE = Res.Get("Common_Label_Grade");
        private readonly ICodeConfigBusiness CodeConfigBusiness;
        private string TEMPLATE_EMPLOYEE = "GV_DanhSachCanBo.xls";
        private string TEMPLATE_EMPLOYEE_NAME = "GV_DanhSachCanBo_{0}.xls";
        private readonly IProvinceBusiness ProvinceBusiness;
        private IUserAccountBusiness UserAccountBusiness;
        #endregion variable

        #region contructor

        public UserAccountProfileController(IEmployeeBusiness employeeBusiness
            , ISchoolFacultyBusiness SchoolFacultyBusiness
            , IWorkTypeBusiness WorkTypeBusiness
            , IGraduationLevelBusiness GraduationLevelBusiness
            , IContractTypeBusiness ContractTypeBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness,
            IGroupCatBusiness GroupBusiness, IEthnicBusiness EthnicBusiness, IReligionBusiness ReligionBusiness
            , IFamilyTypeBusiness FamilyTypeBusiness, IQualificationTypeBusiness QualificationTypeBusiness,
            ISpecialityCatBusiness SpecialityCatBusiness, IQualificationLevelBusiness QualificationLevelBusiness,
            IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness, IITQualificationLevelBusiness ITQualificationLevelBusiness,
            IStateManagementGradeBusiness StateManagementGradeBusiness,
            IPoliticalGradeBusiness PoliticalGradeBusiness,
            IEducationalManagementGradeBusiness EducationalManagementGradeBusiness,
            IStaffPositionBusiness StaffPositionBusiness,
            IContractBusiness ContractBusiness
            , IWorkGroupTypeBusiness WorkGroupTypeBusiness
            , ISubjectCatBusiness SubjectCatBusiness
            , IEmployeeWorkingHistoryBusiness EmployeeWorkingHistoryBusiness
            , IQualificationGradeBusiness QualificationGradeBusiness
            , IEmployeeQualificationBusiness EmployeeQualificationBusiness
            , IEmployeeSalaryBusiness EmployeeSalaryBusiness
            , IEmployeeScaleBusiness EmployeeScaleBusiness
            , ISchoolProfileBusiness SchoolProfileBusiness
            , ICodeConfigBusiness CodeConfigBusiness
            , IProvinceBusiness ProvinceBusiness
            , ITrainingLevelBusiness TrainingLevelBusiness, IUserAccountBusiness UserAccountBusiness)
        {
            this.TrainingLevelBusiness = TrainingLevelBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.EmployeeBusiness = employeeBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.WorkTypeBusiness = WorkTypeBusiness;
            this.GraduationLevelBusiness = GraduationLevelBusiness;
            this.ContractTypeBusiness = ContractTypeBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.GroupCatBusiness = GroupBusiness;
            this.EthnicBusiness = EthnicBusiness;
            this.ReligionBusiness = ReligionBusiness;
            this.FamilyTypeBusiness = FamilyTypeBusiness;
            this.QualificationTypeBusiness = QualificationTypeBusiness;
            this.SpecialityCatBusiness = SpecialityCatBusiness;
            this.QualificationLevelBusiness = QualificationLevelBusiness;
            this.ForeignLanguageGradeBusiness = ForeignLanguageGradeBusiness;
            this.ITQualificationLevelBusiness = ITQualificationLevelBusiness;
            this.StateManagementGradeBusiness = StateManagementGradeBusiness;
            this.PoliticalGradeBusiness = PoliticalGradeBusiness;

            this.EducationalManagementGradeBusiness = EducationalManagementGradeBusiness;
            this.StaffPositionBusiness = StaffPositionBusiness;
            this.ContractBusiness = ContractBusiness;
            this.WorkGroupTypeBusiness = WorkGroupTypeBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.EmployeeWorkingHistoryBusiness = EmployeeWorkingHistoryBusiness;
            this.QualificationGradeBusiness = QualificationGradeBusiness;
            this.EmployeeQualificationBusiness = EmployeeQualificationBusiness;
            this.EmployeeSalaryBusiness = EmployeeSalaryBusiness;
            this.EmployeeScaleBusiness = EmployeeScaleBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.CodeConfigBusiness = CodeConfigBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
        }

        #endregion contructor

        //
        // GET: /UserAccountProfileArea/UserAccountProfile/

        GlobalInfo glo = new GlobalInfo();
        #region index
        public ActionResult Index(bool? isChangePw)
        {
            if (glo.EmployeeID != null)
            {
                ViewData[UserAccountProfileConstant.CHOSENEMPLOYEE] = GetDetailEmployee(glo.EmployeeID.Value);
            }
            //UserAccount

            UserAccountModel UserAccountObj = new UserAccountModel();
            UserAccountObj.UserAccountID = glo.UserAccountID;

            UserAccount User = UserAccountBusiness.Find(UserAccountObj.UserAccountID);

            if (User != null)
            {
                UserAccountObj.UserName = User.aspnet_Users.UserName;
            }

            ViewData[UserAccountProfileConstant.USERACCOUNT] = UserAccountObj;
            ViewData[UserAccountProfileConstant.IS_CHANGE_PASSWORD] = isChangePw == true ? true : false;

            return View();
        }
        #endregion  

        #region setViewData

        public void SetViewData()
        {
            GlobalInfo global = GlobalInfo.getInstance();
            if (global.SchoolID != null)
            {
                //Get view data here
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = global.SchoolID;
                IQueryable<SchoolFaculty> lsSchoolFaculty = SchoolFacultyBusiness.Search(SearchInfo).OrderBy(o => o.FacultyName);
                ViewData[UserAccountProfileConstant.LISTSCHOOLFACULTY] = new SelectList(lsSchoolFaculty, "SchoolFacultyID", "FacultyName");

                //cboWorkType: Lấy danh sách từ WorkTypeBusiness.Search()
                IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(new Dictionary<string, object>() { { "SchoolID", global.SchoolID.Value }, { "IsActive", true } }).OrderBy(o => o.Resolution);
                ViewData[UserAccountProfileConstant.LISTWORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution");

                //cboEmploymentStatus:
                //+ EMPLOYMENT_STATUS_WORKING : Đang làm việc
                //+ EMPLOYMENT_STATUS_WORK_CHANGED : Chuyển công tác
                //+ EMPLOYMENT_STATUS_RETIRED : Nghỉ hưu
                List<ComboObject> listEmploymentStatus = new List<ComboObject>();
                listEmploymentStatus.Add(new ComboObject(UserAccountProfileConstant.EMPLOYMENT_STATUS_WORKING.ToString(), Res.Get("Employee_Label_EmployeeStatusWorking")));
                listEmploymentStatus.Add(new ComboObject(UserAccountProfileConstant.EMPLOYMENT_STATUS_WORK_CHANGED.ToString(), Res.Get("Employee_Label_EmployeeStatusWorkingChange")));
                listEmploymentStatus.Add(new ComboObject(UserAccountProfileConstant.EMPLOYMENT_STATUS_RETIRED.ToString(), Res.Get("Employee_Label_EmployeeStatusRetired")));
                ViewData[UserAccountProfileConstant.LISTEMPLOYMENTSTATUS] = new SelectList(listEmploymentStatus, "key", "value");

                //   cboSex:
                //+ 1 : Nam
                //+ 2 : Nữ
                List<ComboObject> listSex = new List<ComboObject>();
                listSex.Add(new ComboObject("true", Res.Get("Common_Label_Male")));
                listSex.Add(new ComboObject("false", Res.Get("Common_Label_Female")));
                ViewData[UserAccountProfileConstant.LISTSEX] = new SelectList(listSex, "key", "value");

                //cboTrainingLevel: TrainingLevelBussiness.Search()    
                IQueryable<TrainingLevel> lsTrainingLevel = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } });
                ViewData[UserAccountProfileConstant.LISTGRADUATIONLEVEL] = new SelectList(lsTrainingLevel, "TrainingLevelID", "Resolution");

                //cboGraduationLevel: Lấy danh sách từ GraduationLevelBussiness.Search(Dictionary) với Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel ->cho form create,edit
                IQueryable<TrainingLevel> lsGraduationLevelUpdateOrEdit = TrainingLevelBusiness.Search(new Dictionary<string, object>() { { "IsActive", true } }).OrderBy(o => o.Resolution);
                ViewData[UserAccountProfileConstant.LS_GRADUATIONLEVEL] = new SelectList(lsGraduationLevelUpdateOrEdit, "TrainingLevelID", "Resolution");

                //cboContractType: Lấy danh sách từ ContractTypeBusiness.Search()
                IQueryable<ContractType> lsContractType = ContractTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution);
                ViewData[UserAccountProfileConstant.LISTCONTRACTTYPE] = new SelectList(lsContractType, "ContractTypeID", "Resolution");

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("IsActive", true);
                List<Ethnic> listEthnic = EthnicBusiness.Search(dic).OrderBy(o => o.EthnicName).ToList();
                //List<Ethnic> listEthnic= EthnicBusiness.All.Where(em => (em.IsActive == true)).ToList();
                ViewData[UserAccountProfileConstant.LS_ETHNIC] = new SelectList(listEthnic, "EthnicID", "EthnicName", SMAS.Business.Common.SystemParamsInFile.DEFAULT_ETHNIC);

                //List<Religion> listReligion = ReligionBusiness.All.Where(em => (em.IsActive == true)).ToList();
                List<Religion> listReligion = ReligionBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
                ViewData[UserAccountProfileConstant.LS_RELIGION] = new SelectList(listReligion, "ReligionID", "Resolution", SMAS.Business.Common.SystemParamsInFile.DEFAULT_RELIGION);

                // List<FamilyType> listFamilyType = FamilyTypeBusiness.All.Where(em => (em.IsActive == true)).ToList();
                List<FamilyType> listFamilyType = FamilyTypeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
                ViewData[UserAccountProfileConstant.LS_FAMILYTYPE] = new SelectList(listFamilyType, "FamilyTypeID", "Resolution");

                //List<QualificationType> listQualificationType = QualificationTypeBusiness.All.Where(em => (em.IsActive == true)).ToList();
                List<QualificationType> listQualificationType = QualificationTypeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
                ViewData[UserAccountProfileConstant.LS_QUALIFICATIONTYPE] = new SelectList(listQualificationType, "QualificationTypeID", "Resolution");

                //List<SpecialityCat> listSpecialityCat = SpecialityCatBusiness.All.Where(em => (em.IsActive == true)).ToList();
                List<SpecialityCat> listSpecialityCat = SpecialityCatBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
                ViewData[UserAccountProfileConstant.LS_SPECIALITYCAT] = new SelectList(listSpecialityCat, "SpecialityCatID", "Resolution");

                // List< QualificationLevel> listQualificationLevel =  QualificationLevelBusiness.All.Where(em => (em.IsActive == true)).ToList();
                List<QualificationLevel> listQualificationLevel = QualificationLevelBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
                ViewData[UserAccountProfileConstant.LS_QUALIFICATIONLEVEL] = new SelectList(listQualificationLevel, "QualificationLevelID", "Resolution");

                //List<ForeignLanguageGrade> listForeignLanguageGrade = ForeignLanguageGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
                List<ForeignLanguageGrade> listForeignLanguageGrade = ForeignLanguageGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
                ViewData[UserAccountProfileConstant.LS_FOREIGNLANGUAGEGRADE] = new SelectList(listForeignLanguageGrade, "ForeignLanguageGradeID", "Resolution");

                //List<ITQualificationLevel> listITQualificationLevel = ITQualificationLevelBusiness.All.Where(em => (em.IsActive == true)).ToList();
                List<ITQualificationLevel> listITQualificationLevel = ITQualificationLevelBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
                ViewData[UserAccountProfileConstant.LS_ITQUALIFICATIONLEVEL] = new SelectList(listITQualificationLevel, "ITQualificationLevelID", "Resolution");

                //List<StateManagementGrade> listStateManagementGrade = StateManagementGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
                List<StateManagementGrade> listStateManagementGrade = StateManagementGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
                ViewData[UserAccountProfileConstant.LS_STATEMANAGEMENTGRADE] = new SelectList(listStateManagementGrade, "StateManagementGradeID", "Resolution");

                //List<PoliticalGrade> listPoliticalGrade = PoliticalGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
                List<PoliticalGrade> listPoliticalGrade = PoliticalGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
                ViewData[UserAccountProfileConstant.LS_POLITICALGRADE] = new SelectList(listPoliticalGrade, "PoliticalGradeID", "Resolution");

                //List<EducationalManagementGrade> listEducationalManagementGrade =  EducationalManagementGradeBusiness.All.Where(em => (em.IsActive == true)).ToList();
                List<EducationalManagementGrade> listEducationalManagementGrade = EducationalManagementGradeBusiness.Search(dic).OrderBy(o => o.Resolution).ToList();
                ViewData[UserAccountProfileConstant.LS_EDUCATIONALMANAGEMENTGRADE] = new SelectList(listEducationalManagementGrade, "EducationalManagementGradeID", "Resolution");

                //List<ContractType> listContractType = ContractTypeBusiness.All.Where(em => (em.IsActive == true)).ToList();
                ////List<Contract> listContract = ContractBusiness.Search(dic).ToList();
                //ViewData[UserAccountProfileConstant.LS_CONTRACT] = new SelectList(listContractType, "ContractTypeID", "Resolution");
                List<Contract> listContract = ContractBusiness.All.Where(em => (em.IsActive == true)).ToList();
                //List<Contract> listContract = ContractBusiness.Search(dic).ToList();
                ViewData[UserAccountProfileConstant.LS_CONTRACT] = new SelectList(listContract, "ContractID", "ContractID");

                List<StaffPosition> listStaffPosition = StaffPositionBusiness.All.Where(em => (em.IsActive == true)).OrderBy(o => o.Resolution).ToList();

                // List<StaffPosition> listStaffPosition = StaffPositionBusiness.Search(dic).ToList();
                ViewData[UserAccountProfileConstant.LS_STAFFPOSITION] = new SelectList(listStaffPosition, "StaffPositionID", "Resolution");

                //List<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.GetDependantDept(UserInfo.SupervisingDeptID()).ToList();
                //List<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.Search(dic).OrderBy(o => o.SupervisingDeptName).ToList();
                //ViewData[UserAccountProfileConstant.LS_SUPERVISINGDEPT] = new SelectList(listSupervisingDept, "SupervisingDeptID", "SupervisingDeptName");

                //List<GroupCat> listGroupCat = GroupCatBusiness.All.Where(em => (em.IsActive == true)).OrderBy(o => o.GroupName).ToList();

                //IQueryable<SupervisingDept> lsSuperTemp = SupervisingDeptBusiness.All.Where(em=>(em.SupervisingDeptID==UserInfo.SupervisingDeptID()));
                //List<GroupCat> listGroupCat = GroupCatBusiness.GetGroupByRoleAndCreatedUser(UserInfo.RoleID(),lsSuperTemp.FirstOrDefault().AdminID.Value).ToList();
                //ViewData[UserAccountProfileConstant.LS_GROUPCAT] = new SelectList(listGroupCat, "GroupCatID", "GroupName");

                //cboWorkGroupType: Lấy danh sách từ WorkGroupTypeBussiness.Search()
                IQueryable<WorkGroupType> lsWorkGroupType = WorkGroupTypeBusiness.Search(new Dictionary<string, object>()).OrderBy(o => o.Resolution);
                ViewData[UserAccountProfileConstant.LS_WORKGROUPTYPE] = new SelectList(lsWorkGroupType, "WorkGroupTypeID", "Resolution");

                //cboWorkType: Không có dữ liệu
                ViewData[UserAccountProfileConstant.LS_WORKTYPE] = new SelectList(new string[] { });

                //cboPrimarilyAssignedSubject: Lấy danh sách từ SubjectCat.Search(Dictionary)
                //với Dictionary[“AppliedLevel”] = UserInfo.AppliedLevel
                IQueryable<SubjectCat> lsSubjectCat = SubjectCatBusiness.Search(new Dictionary<string, object>() { { "AppliedLevel", global.AppliedLevel.Value }, { "IsActive", true } }).OrderBy(o => o.DisplayName);
                ViewData[UserAccountProfileConstant.LS_PRIMARILYASSIGNEDSUBJECT] = new SelectList(new string[] { });

                SearchInfo["SchoolID"] = global.SchoolID;
                SearchInfo["AcademicYearID"] = global.AcademicYearID;

                //SchoolProfileBusiness.GetListAppliedLevel(UserInfo.SchoolID)

                List<int> lsAL = SchoolProfileBusiness.GetListAppliedLevel(global.SchoolID.Value);
                lsAL.Sort();
                List<ComboObject> listAppliedLevel = new List<ComboObject>();
                foreach (var item in lsAL)
                {
                    listAppliedLevel.Add(new ComboObject(item.ToString(), item.ToString()));
                }
                ViewData[UserAccountProfileConstant.LS_APPLIEDLEVEL] = new SelectList(listAppliedLevel, "key", "value");

                //Autogen code
                //Nếu  CodeConfigBusiness.IsAutoGenCode(UserInfo.SchoolID, CODE_CONFIG_TYPE_PUPIL) = TRUE thì chkAutoCode được chọn
                if (CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
                {
                    ViewData[UserAccountProfileConstant.AUTO_GEN_CODE_CHECK] = true;
                }
                else
                {
                    ViewData[UserAccountProfileConstant.AUTO_GEN_CODE_CHECK] = false;
                }

                //-	Nếu CodeConfigBusiness.IsSchoolModify(UserInfo.SchoolID, CODE_CONFIG_TYPE_PUPIL) = TRUE
                if (CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
                {
                    ViewData[UserAccountProfileConstant.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = true;
                    ViewData[UserAccountProfileConstant.AUTO_GEN_CODE_CHECK_AND_DISABLE] = false;
                    ViewData[UserAccountProfileConstant.IS_SCHOOL_MODIFIED] = true;
                }
                else
                {
                    ViewData[UserAccountProfileConstant.AUTO_GEN_CODE_CHECK_AND_NO_DISABLE] = false;
                    ViewData[UserAccountProfileConstant.AUTO_GEN_CODE_CHECK_AND_DISABLE] = true;
                    ViewData[UserAccountProfileConstant.IS_SCHOOL_MODIFIED] = false;
                }

                if (global.IsCurrentYear)
                {
                    ViewData[UserAccountProfileConstant.IS_CURRENT_YEAR] = true;
                }
                else
                {
                    ViewData[UserAccountProfileConstant.IS_CURRENT_YEAR] = false;
                }
            }
        }

        #endregion setViewData

        #region detail

        public EmployeeProfileManagementViewModel GetDetailEmployee(int EmployeeId)
        {
            #region load du lieu tab 1

            string male = Res.Get("Common_Label_Male");
            string female = Res.Get("Common_Label_Female");

            SetViewData();
            Employee employee = this.EmployeeBusiness.Find(EmployeeId);

            EmployeeProfileManagementViewModel EmployeeProfileManagementViewModel = new EmployeeProfileManagementViewModel();

            if (employee == null)
            {
                return EmployeeProfileManagementViewModel;
            }

            Utils.Utils.BindTo(employee, EmployeeProfileManagementViewModel);

            if (employee.FatherBirthDate.HasValue)
            {
                EmployeeProfileManagementViewModel.iFatherBirthDate = employee.FatherBirthDate.Value.Year;
            }
            if (employee.MotherBirthDate.HasValue)
            {
                EmployeeProfileManagementViewModel.iMotherBirthDate = employee.MotherBirthDate.Value.Year;
            }
            if (employee.SpouseBirthDate.HasValue)
            {
                EmployeeProfileManagementViewModel.iSpouseBirthDate = employee.SpouseBirthDate.Value.Year;
            }

            //vi employee chua co workgrouptypeid nen phai them vao dua vao worktypeid
            if (employee.WorkTypeID.HasValue)
            {
                //WorkType workType = WorkTypeBusiness.All.Where(o => (o.WorkTypeID == employee.WorkTypeID.Value)).FirstOrDefault();
                EmployeeProfileManagementViewModel.WorkGroupTypeID = employee.WorkGroupTypeID; // workType.WorkGroupTypeID;

                //them vao viewdata worktype
                //IDictionary<string, object> dic = new Dictionary<string, object>();
                //dic["WorkGroupTypeID"] = employee.WorkTypeID.Value;
                //IQueryable<WorkType> lsWorkType = WorkTypeBusiness.Search(dic);
                //ViewData[UserAccountProfileConstant.LS_WORKTYPE] = new SelectList(lsWorkType, "WorkTypeID", "Resolution");
            }
            if (EmployeeProfileManagementViewModel.Genre)
            {
                EmployeeProfileManagementViewModel.Sex = male;
            }
            else
            {
                EmployeeProfileManagementViewModel.Sex = female;
            }

            //dua vao cac id co duoc ma tim ten
            if (EmployeeProfileManagementViewModel.TrainingLevelID.HasValue)
            {
                //TrainingLevel temp = TrainingLevelBusiness.Find(EmployeeProfileManagementViewModel.TrainingLevelID);
                EmployeeProfileManagementViewModel.TrainingLevelName = employee.TrainingLevel == null ? "" : employee.TrainingLevel.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.SpecialityCatID.HasValue)
            {
                //SpecialityCat temp = SpecialityCatBusiness.All.Where(o => (o.SpecialityCatID == EmployeeProfileManagementViewModel.SpecialityCatID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.SpecialityCatName = employee.SpecialityCat == null ? "" : employee.SpecialityCat.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.QualificationLevelID.HasValue)
            {
                //QualificationLevel temp = QualificationLevelBusiness.All.Where(o => (o.QualificationLevelID == EmployeeProfileManagementViewModel.QualificationLevelID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.QualificationLevelName = employee.QualificationLevel == null ? "" : employee.QualificationLevel.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.ForeignLanguageGradeID.HasValue)
            {
                //ForeignLanguageGrade temp = ForeignLanguageGradeBusiness.All.Where(o => (o.ForeignLanguageGradeID == EmployeeProfileManagementViewModel.ForeignLanguageGradeID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.ForeignLanguageGradeName = employee.ForeignLanguageGrade == null ? "" : employee.ForeignLanguageGrade.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.ITQualificationLevelID.HasValue)
            {
                //ITQualificationLevel temp = ITQualificationLevelBusiness.All.Where(o => (o.ITQualificationLevelID == EmployeeProfileManagementViewModel.ITQualificationLevelID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.ITQualificationLevelName = employee.ITQualificationLevel == null ? "" : employee.ITQualificationLevel.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.PoliticalGradeID.HasValue)
            {
                //PoliticalGrade temp = PoliticalGradeBusiness.All.Where(o => (o.PoliticalGradeID == EmployeeProfileManagementViewModel.PoliticalGradeID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.PoliticalGradeName = employee.PoliticalGrade == null ? "" : employee.PoliticalGrade.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.StateManagementGradeID.HasValue)
            {
                //StateManagementGrade temp = StateManagementGradeBusiness.All.Where(o => (o.StateManagementGradeID == EmployeeProfileManagementViewModel.StateManagementGradeID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.StateManagementGradeName = employee.StateManagementGrade == null ? "" : employee.StateManagementGrade.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.EducationalManagementGradeID.HasValue)
            {
                //EducationalManagementGrade temp = EducationalManagementGradeBusiness.All.Where(o => (o.EducationalManagementGradeID == EmployeeProfileManagementViewModel.EducationalManagementGradeID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.EducationalManagementGradeName = employee.EducationalManagementGrade == null ? "" : employee.EducationalManagementGrade.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.ContractID.HasValue)
            {
                //Contract temp = ContractBusiness.All.Where(o => (o.ContractID == EmployeeProfileManagementViewModel.ContractID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.ContractName = employee.Contract == null ? "" : employee.Contract.ContractType == null ? "" : employee.Contract.ContractType.Resolution; // temp != null ? temp.ContractType.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.WorkGroupTypeID.HasValue)
            {
                //WorkGroupType temp = WorkGroupTypeBusiness.All.Where(o => (o.WorkGroupTypeID == EmployeeProfileManagementViewModel.WorkGroupTypeID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.WorkGroupTypeName = employee.WorkGroupType == null ? "" : employee.WorkGroupType.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.WorkTypeID.HasValue)
            {
                //WorkType temp = WorkTypeBusiness.All.Where(o => (o.WorkTypeID == EmployeeProfileManagementViewModel.WorkTypeID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.WorkTypeName = employee.WorkType == null ? "" : employee.WorkType.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.PrimarilyAssignedSubjectID.HasValue)
            {
                //SubjectCat temp = SubjectCatBusiness.All.Where(o => (o.SubjectCatID == EmployeeProfileManagementViewModel.PrimarilyAssignedSubjectID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.PrimarilyAssignedSubjectName = employee.SubjectCat == null ? "" : employee.SubjectCat.DisplayName; // temp != null ? temp.DisplayName : "";
            }

            if (EmployeeProfileManagementViewModel.ReligionID.HasValue)
            {
                //Religion temp = ReligionBusiness.All.Where(o => (o.ReligionID == EmployeeProfileManagementViewModel.ReligionID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.ReligionName = employee.Religion == null ? "" : employee.Religion.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.EthnicID.HasValue)
            {
                //Ethnic temp = EthnicBusiness.All.Where(o => (o.EthnicID == EmployeeProfileManagementViewModel.EthnicID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.EthnicName = employee.Ethnic == null ? "" : employee.Ethnic.EthnicName; // temp != null ? temp.EthnicName : "";
            }

            if (EmployeeProfileManagementViewModel.FamilyTypeID.HasValue)
            {
                //FamilyType temp = FamilyTypeBusiness.All.Where(o => (o.FamilyTypeID == EmployeeProfileManagementViewModel.FamilyTypeID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.FamilyTypeName = employee.FamilyType == null ? "" : employee.FamilyType.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.QualificationTypeID.HasValue)
            {
                //QualificationType temp = QualificationTypeBusiness.All.Where(o => (o.QualificationTypeID == EmployeeProfileManagementViewModel.QualificationTypeID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.QualificationTypeName = employee.QualificationType == null ? "" : employee.QualificationType.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.ContractTypeID.HasValue)
            {
                //ContractType temp = ContractTypeBusiness.Find(EmployeeProfileManagementViewModel.ContractTypeID);
                EmployeeProfileManagementViewModel.ContractTypeName = employee.ContractType == null ? "" : employee.ContractType.Resolution; // temp != null ? temp.Resolution : "";
            }

            if (EmployeeProfileManagementViewModel.SchoolFacultyID.HasValue)
            {
                //SchoolFaculty temp = SchoolFacultyBusiness.All.Where(o => (o.SchoolFacultyID == EmployeeProfileManagementViewModel.SchoolFacultyID)).FirstOrDefault();
                EmployeeProfileManagementViewModel.SchoolFacultyName = employee.SchoolFaculty == null ? "" : employee.SchoolFaculty.FacultyName; // temp != null ? temp.FacultyName : "";
            }

            #endregion load du lieu tab 1

            return EmployeeProfileManagementViewModel;
        }

        #endregion detail

        #region uploadImage

        [ValidateAntiForgeryToken]
        public JsonResult SaveImage(IEnumerable<HttpPostedFileBase> ImageUploader)
        {
            GlobalInfo global = new GlobalInfo();

            if (ImageUploader == null || ImageUploader.Count() <= 0) return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));

            // The Name of the Upload component is "attachments"
            var file = ImageUploader.FirstOrDefault();

            if (file != null)
            {
                //kiem tra file extension lan nua
                List<string> imageExtension = new List<string>();
                imageExtension.Add(".JPG");
                imageExtension.Add(".JPEG");
                imageExtension.Add(".PNG");
                imageExtension.Add(".BMP");
                imageExtension.Add(".ICO");
                imageExtension.Add(".GIF");
                var extension = Path.GetExtension(file.FileName);
                if (!imageExtension.Contains(extension.ToUpper()))
                {
                    return Json(new JsonMessage(Res.Get("Common_Label_ImageExtensionError"), "error"));
                }

                // luu ra dia
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                fileName = fileName + "-" + global.UserAccountID + "-" + Guid.NewGuid().ToString() + extension;
                var physicalPath = Path.Combine(Server.MapPath("~/Uploads/Photos"), fileName);
                file.SaveAs(physicalPath);
                Session["ImagePath"] = physicalPath;

                string relativePath = "/Uploads/Photos/" + fileName;
                JsonResult res = Json(new JsonMessage(fileName));
                res.ContentType = "text/plain";
                return res;
            }

            // Return an empty string to signify success
            return Json(new JsonMessage(Res.Get("Common_Label_ImageNullError"), "error"));
        }

        [ValidateAntiForgeryToken]
        public ActionResult RemoveImage(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"
            //foreach (var fullName in fileNames)
            //{
            //    var fileName = Path.GetFileName(fullName);
            //    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

            //    // TODO: Verify user permissions
            //    if (System.IO.File.Exists(physicalPath))
            //    {
            //        // The files are not actually removed in this demo
            //        System.IO.File.Delete(physicalPath);
            //    }
            //}

            // Return an empty string to signify success
            return Content("");
        }

        /// <summary>
        /// Function to get int array from a file
        /// </summary>
        /// <param name="_FileName">File name to get int array</param>
        /// <returns>Int32 Array</returns>
        public byte[] FileToByteArray(string _FileName)
        {
            return SMAS.Business.Common.UtilsBusiness.FileToByteArray(_FileName);
        }
        #endregion

        #region EditEmployee
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]

        public JsonResult Edit(EmployeeProfileManagementViewModel EmployeeProfileManagementViewModel)
        {
            Employee employee = EmployeeBusiness.Find(EmployeeProfileManagementViewModel.EmployeeID);

            #region "Fill lai thong tin chuyen mon hop dong truoc khi bind vao object. Fix loi 16179"
            EmployeeProfileManagementViewModel.TrainingLevelID = employee.TrainingLevelID; //trinhdo dao tao
            EmployeeProfileManagementViewModel.QualificationTypeID = employee.QualificationTypeID; // Hinh thuc dao tao
            EmployeeProfileManagementViewModel.SpecialityCatID = employee.SpecialityCatID;// chuyen nghanh dao tao
            EmployeeProfileManagementViewModel.ContractTypeID = employee.ContractTypeID; // Loai hop dong
            EmployeeProfileManagementViewModel.JoinedDate = employee.JoinedDate; // ngay bien che
            EmployeeProfileManagementViewModel.StartingDate = employee.StartingDate; //Ngay vao nghe
            EmployeeProfileManagementViewModel.QualificationLevelID = employee.QualificationLevelID; //trinh do van hoa
            EmployeeProfileManagementViewModel.ForeignLanguageGradeID = employee.ForeignLanguageGradeID; //trinh do ngoai ngu
            EmployeeProfileManagementViewModel.ITQualificationLevelID = employee.ITQualificationLevelID; //trinh do tin hoc
            EmployeeProfileManagementViewModel.PoliticalGradeID = employee.PoliticalGradeID; //trinh do ly luan chinh tri
            EmployeeProfileManagementViewModel.StateManagementGradeID = employee.StateManagementGradeID; //trinh do QL nha nuoc
            EmployeeProfileManagementViewModel.EducationalManagementGradeID = employee.EducationalManagementGradeID; //trinh do QL giao duc
            EmployeeProfileManagementViewModel.WorkGroupTypeID = employee.WorkGroupTypeID; //nhom cong viec
            EmployeeProfileManagementViewModel.WorkTypeID = employee.WorkTypeID; //loai cong viec
            EmployeeProfileManagementViewModel.AppliedLevel = (employee.AppliedLevel.HasValue) ? employee.AppliedLevel.Value : 0; //cap day chinh
            EmployeeProfileManagementViewModel.PrimarilyAssignedSubjectID = employee.PrimarilyAssignedSubjectID;//Phan cong giang day
            EmployeeProfileManagementViewModel.DedicatedForYoungLeague = (employee.DedicatedForYoungLeague.HasValue) ? employee.DedicatedForYoungLeague.HasValue : false;//Chuyen trach doan doi
            EmployeeProfileManagementViewModel.RegularRefresher = (employee.RegularRefresher.HasValue) ? employee.RegularRefresher.HasValue : false; // Buoi duong thuong xuyen
            //EmployeeProfileManagementViewModel.no = employee.Note;

            #endregion
            Utils.Utils.BindTo(EmployeeProfileManagementViewModel, employee, false);
            if (EmployeeProfileManagementViewModel.iFatherBirthDate.HasValue)
            {
                if (EmployeeProfileManagementViewModel.iFatherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "Employee_Label_FatherBirthDate" });
                }
                employee.FatherBirthDate = new DateTime(EmployeeProfileManagementViewModel.iFatherBirthDate.Value, 1, 1);
            }
            if (EmployeeProfileManagementViewModel.iMotherBirthDate.HasValue)
            {
                if (EmployeeProfileManagementViewModel.iMotherBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "Employee_Label_MotherBirthDate" });
                }
                employee.MotherBirthDate = new DateTime(EmployeeProfileManagementViewModel.iMotherBirthDate.Value, 1, 1);
            }
            if (EmployeeProfileManagementViewModel.iSpouseBirthDate.HasValue)
            {
                if (EmployeeProfileManagementViewModel.iSpouseBirthDate < 1900)
                {
                    throw new SMAS.Business.Common.BusinessException("Common_Min_BirthYearError", new List<object>() { "Employee_Label_SpouseBirthDate" });
                }
                employee.SpouseBirthDate = new DateTime(EmployeeProfileManagementViewModel.iSpouseBirthDate.Value, 1, 1);
            }

            //truong Name lay tu FullName
            int space = employee.FullName.LastIndexOf(" ");
            if (space == -1)
            {
                employee.Name = employee.FullName;
            }
            else
            {
                employee.Name = employee.FullName.Substring(space + 1);
            }

            //dua image vao employee
            object imagePath = Session["ImagePath"];
            if (imagePath != null)
            {
                byte[] imageBytes = FileToByteArray((string)imagePath);
                employee.Image = imageBytes;
            }
            GlobalInfo global = new GlobalInfo();
            employee.SchoolID = global.SchoolID;
            employee.IsActive = true;

            //Nếu ckbAutoGenCode được check thì EmployeeCode =  CodeConfigBusiness.CreateTeacherCode(UserInfo.SchoolID)
            if (EmployeeProfileManagementViewModel.AutoGenCode)
            {
                employee.EmployeeCode = CodeConfigBusiness.CreateTeacherCode(global.SchoolID.Value);
            }

            //truong hop autogencode check chon nhung bi disable thi form ko submit duoc,ta phai xet rieng
            if (CodeConfigBusiness.IsAutoGenCode(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
            {
                if (!CodeConfigBusiness.IsSchoolModify(global.SchoolID.Value, SMAS.Web.Constants.GlobalConstants.CODE_CONFIG_TYPE_TEACHER))
                {
                    //check chon va disable
                    employee.EmployeeCode = CodeConfigBusiness.CreateTeacherCode(global.SchoolID.Value);
                }
            }

            employee.EmploymentStatus = SMAS.Business.Common.GlobalConstants.EMPLOYMENT_STATUS_WORKING;
            employee.AppliedLevel = byte.Parse(global.AppliedLevel.Value.ToString());
            this.EmployeeBusiness.UpdateTeacher(employee);
            this.EmployeeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        #endregion

        public const int MinLengthPassword = 8;
        public const int MaxLengthPassword = 128;

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ChangePassword(UserAccountModel UserAccountModel)
        {
            //Kiem tra tai khoan co dung mat khau
            if (!Membership.ValidateUser(UserAccountModel.UserName, UserAccountModel.Password))
            {
                return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_PasswordFail"), "Error"));
            }
            else
            {
                if (!Session["CaptchaUser"].ToString().Equals(UserAccountModel.CaptchaChangPassUser))
                {
                    return Json(new { @Message = "Nhập mã xác thực không chính xác.", Type = "error" });
                }
                //Kiểm tra độ dài password
                else if (UserAccountModel.NewPassword.Length < 8)
                {
                    return Json(new { @Message = "Mật khẩu phải lớn hơn 8 ký tự.", Type = "error" });
                }
                //so sánh pass cũ và pass mới
                else if (UserAccountModel.NewPassword.Equals(UserAccountModel.Password))
                {
                    return Json(new { @Message = "Mật khẩu mới và mật khẩu cũ không được giống nhau.", Type = "error" });
                }
                //so sánh 2 pass mới
                else if (!UserAccountModel.NewPassword.Equals(UserAccountModel.RePassword))
                {
                    return Json(new { @Message = "Xác nhận mật khẩu mới không hợp lệ.", Type = "error" });
                }
                //Kiem tra pass mới có đúng định dạng chưa (kí tự khác blacklist)
                else if (!SMAS.Business.Common.Utils.CheckPasswordInValid(UserAccountModel.NewPassword))
                {
                    // return Res.Get("Mật khẩu mới phải có chữ in hoa, chữ thường và ký tự đặt biệt."); //kí tự đặt biệt không chưa dấu ' và / \
                    return Json(new { @Message = "Mật khẩu mới không được nhỏ hơn 8 ký tự, phải bao gồm số, chữ thường, chữ in hoa và ký tự đặc biệt.", Type = "error" });
                }
                else if (UserAccountModel.NewPassword != UserAccountModel.RePassword)
                {
                    return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_UserPasswordFail"), "Error"));
                }
                else if (!CheckPassRequire(UserAccountModel.NewPassword))
                {
                    return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_UserPasswordFormatFail"), "Error"));

                }
                else if (UserAccountModel.NewPassword.Length < MinLengthPassword || UserAccountModel.NewPassword.Length > MaxLengthPassword)
                {
                    return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_MaxlengthPassword"), "Error"));
                }
                else
                {
                    if (!UserAccountBusiness.ChangePassword(new GlobalInfo().UserAccountID, UserAccountModel.NewPassword))
                    {
                        return Json(new JsonMessage(Res.Get("UserAccountProfile_Validate_NotUpdate"), "Error"));
                    }
                }
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        public bool CheckPassRequire(string Password)
        {
            bool character = false;
            bool number = false;

            //kiem tra co kytu khong
            foreach (char ch in Password)
            {
                if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122))
                {
                    character = true;
                }
                if (ch >= 48 && ch <= 64)
                {
                    number = true;
                }
            }
            if (character && number)
                return true;
            return false;
        }

        [AllowAnonymous]
        [SkipCheckRole]
        public PartialViewResult LogonGetCapcha()
        {
            return PartialView("_Capcha");
        }
        [HttpGet]
        [AllowAnonymous]
        [SkipCheckRole]
        public ActionResult LogonCaptchaImage()
        {
            return LogonGenerateCaptcha();
        }

        [HttpGet]
        [AllowAnonymous]
        [SkipCheckRole]
        public FileContentResult LogonGenerateCaptcha()
        {
            FileContentResult captchaImg = null;
            string[] fonts = { "Time New Roman" };
            Random ran = new Random();
            int LENGTH = ran.Next(4, 6);
            int charWidth = 30;
            // chuá»—i Ä‘á»ƒ láº¥y cĂ¡c kĂ­ tá»± sáº½ sá»­ dá»¥ng cho captcha
            const string chars = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (Bitmap bmp = new Bitmap(150, 30))
                {
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        // Táº¡o ná»n cho áº£nh dáº¡ng sĂ³ng
                        HatchBrush brush = new HatchBrush(HatchStyle.LightDownwardDiagonal, Color.Wheat, Color.Silver);
                        g.FillRegion(brush, g.Clip);
                        // LÆ°u chuá»—i captcha trong quĂ¡ trĂ¬nh táº¡o
                        StringBuilder strCaptcha = new StringBuilder();
                        Random rand = new Random();
                        for (int i = 0; i < LENGTH; i++)
                        {
                            // Láº¥y kĂ­ tá»± ngáº«u nhiĂªn tá»« máº£ng chars
                            string str = chars[rand.Next(chars.Length)].ToString();
                            strCaptcha.Append(str);
                            // Táº¡o font vá»›i tĂªn font ngáº«u nhiĂªn chá»n tá»« máº£ng fonts
                            Font font = new Font(fonts[rand.Next(fonts.Length)], 14, FontStyle.Italic | FontStyle.Bold);
                            // Láº¥y kĂ­ch thÆ°á»›c cá»§a kĂ­ tá»±
                            SizeF size = g.MeasureString(str, font);
                            // Váº½ kĂ­ tá»± Ä‘Ă³ ra áº£nh táº¡i vá»‹ trĂ­ tÄƒng dáº§n theo i, vá»‹ trĂ­ top ngáº«u nhiĂªn
                            g.DrawString(str, font,
                            Brushes.Blue, i * charWidth, rand.Next(2, 7));
                            font.Dispose();
                        }
                        // LÆ°u captcha vĂ o session
                        Session["CaptchaUser"] = strCaptcha.ToString();
                        // Ghi áº£nh trá»±c tiáº¿p ra luá»“ng xuáº¥t theo Ä‘á»‹nh dáº¡ng gif

                        bmp.Save(memoryStream, ImageFormat.Gif);
                        captchaImg = this.File(memoryStream.GetBuffer(), "image/Jpeg");
                    }
                }
            }
            return captchaImg;
        }
    }
}
