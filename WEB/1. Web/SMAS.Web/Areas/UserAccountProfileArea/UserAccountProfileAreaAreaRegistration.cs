﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.UserAccountProfileArea
{
    public class UserAccountProfileAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "UserAccountProfileArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "UserAccountProfileArea_default",
                "UserAccountProfileArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
