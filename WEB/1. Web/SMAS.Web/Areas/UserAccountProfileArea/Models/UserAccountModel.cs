﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Web.Models.Attributes;
namespace SMAS.Web.Areas.UserAccountProfileArea.Models
{
    public class UserAccountModel
    {
        [ResourceDisplayName("UserAccountProfile_Label_UserAccountID")]
        public int UserAccountID { get; set; }

        [ResourceDisplayName("UserAccountProfile_Label_UserName")]        
        public string UserName { get; set; }

        [ResourceDisplayName("UserAccountProfile_Label_OldPassWord")]
        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(128, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "minLengthResourceKeyPass", MinimumLength = 8)]
        public string Password { get; set; }

        [ResourceDisplayName("UserAccountProfile_Label_NewPassWord")]
        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(128, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "minLengthResourceKeyPass", MinimumLength = 8)]
        [DisplayName("Mật khẩu mới")]
        public string NewPassword { get; set; }

        [ResourceDisplayName("UserAccountProfile_Label_RePassWord")]
        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(128, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "minLengthResourceKeyPass", MinimumLength = 8)]
        //[Compare("NewPassword", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "compareResourceKey")]
        [Compare("NewPassword", ErrorMessage = "Xác nhận mật khẩu mới không trùng với Mật khẩu mới")]
        public string RePassword { get; set; }

        [ResourceDisplayName("Lbl_Code_Authenticate_Captcha")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public string CaptchaChangPassUser { get; set; }

    }
}