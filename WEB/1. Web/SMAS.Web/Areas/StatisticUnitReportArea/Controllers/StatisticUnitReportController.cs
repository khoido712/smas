﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.StatisticUnitReportArea.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;

namespace SMAS.Web.Areas.StatisticUnitReportArea.Controllers
{
    public class StatisticUnitReportController : BaseController
    {
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IClassAssigmentBusiness ClassAssigmentBusiness;
        private readonly ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private readonly ISubCommitteeBusiness SubCommitteeBusiness;
        private readonly IContractTypeBusiness ContractTypeBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly ISchoolSubjectBusiness SchoolSubjectBusiness;
        private readonly IMarkStatisticBusiness MarkStatisticBusiness;
        private readonly ICapacityStatisticBusiness CapacityStatisticBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;
        private readonly IConductStatisticBusiness ConductStatisticBusiness;
        public StatisticUnitReportController(
            ISchoolProfileBusiness SchoolProfileBusiness
            , IAcademicYearBusiness AcademicYearBusiness
            , IReportDefinitionBusiness ReportDefinitionBusiness
            , IClassProfileBusiness ClassProfileBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , IReportMarkInputSituationBusiness ReportMarkInputSituationBusiness
            , IDistrictBusiness DistrictBusiness
            , IProvinceBusiness ProvinceBusiness
            , IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness
            , IPupilOfClassBusiness PupilOfClassBusiness
            , IEmployeeBusiness EmployeeBusiness
            , IClassSubjectBusiness ClassSubjectBusiness
            , IClassAssigmentBusiness ClassAssigmentBusiness
            , ITeachingAssignmentBusiness TeachingAssignmentBusiness
            , ISubCommitteeBusiness SubCommitteeBusiness
            , IContractTypeBusiness ContractTypeBusiness
            , ISubjectCatBusiness SubjectCatBusiness
            , ISchoolSubjectBusiness SchoolSubjectBusiness
            , IMarkStatisticBusiness MarkStatisticBusiness
            , ICapacityStatisticBusiness CapacityStatisticBusiness
            , ISupervisingDeptBusiness SupervisingDeptBusiness
            , IConductStatisticBusiness ConductStatisticBusiness 
            )
        {
            this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.AcademicYearOfProvinceBusiness = AcademicYearOfProvinceBusiness;
            this.ProvinceBusiness = ProvinceBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.ReportDefinitionBusiness = ReportDefinitionBusiness;
            this.ReportMarkInputSituationBusiness = ReportMarkInputSituationBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.DistrictBusiness = DistrictBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.ClassAssigmentBusiness = ClassAssigmentBusiness;
            this.SubCommitteeBusiness = SubCommitteeBusiness;
            this.ContractTypeBusiness = ContractTypeBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
            this.SchoolSubjectBusiness = SchoolSubjectBusiness;
            this.MarkStatisticBusiness = MarkStatisticBusiness;
            this.CapacityStatisticBusiness = CapacityStatisticBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.ConductStatisticBusiness = ConductStatisticBusiness;
        }


        public ActionResult Index()
        {
            SetViewData();
            return View();
        }
        public void SetViewData()
        {
            List<ViettelCheckboxList> lsradio = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio = new ViettelCheckboxList();
            //Thống kê điểm kiểm tra định kỳ, 
            //thống kê điểm kiểm tra học kỳ, 
            //thống kê điểm TBM, học lực, hạnh kiểm.
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("Label_Periodicmark_Primary");
            viettelradio.cchecked = true;
            viettelradio.disabled = false;
            viettelradio.Value = 1;
            lsradio.Add(viettelradio);
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("Label_SemesterMark_Primary");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 2;
            lsradio.Add(viettelradio);
            viettelradio = new ViettelCheckboxList();
            viettelradio.Label = Res.Get("StatisticUnitReport_Label_TBM");
            viettelradio.cchecked = false;
            viettelradio.disabled = false;
            viettelradio.Value = 3;
            lsradio.Add(viettelradio);

            ViewData[StatisticUnitReportConstants.LS_REPORTTYPE] = lsradio;
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SupervisingDeptID"] = Global.SupervisingDeptID;
            dic["ProvinceID"] = Global.ProvinceID;
            //Nam hoc
            //List<AcademicYearOfProvince> lstAcademicYear = AcademicYearOfProvinceBusiness.SearchByProvince(Global.ProvinceID.Value, dic).ToList();
            List<int> lstAcademicYear = AcademicYearBusiness.GetListYearForSupervisingDept_Pro(Global.SupervisingDeptID.Value).ToList();
            ViewData[StatisticUnitReportConstants.LS_YEAR] = lstAcademicYear.Select(u => new SelectListItem { Text = u + " - " + (u + 1), Value = u.ToString(), Selected = false }).ToList(); ;
            //Cap hoc
            List<ComboObject> lst = new List<ComboObject>();
            if (!Global.IsSystemAdmin)
            {
                if (Global.IsSuperVisingDeptRole)
                {
                    lst.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString(), Res.Get("SchoolProfile_Label_Primary")));
                    lst.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), Res.Get("SchoolProfile_Label_Secondary_Edit")));
                    lst.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_TERTIARY.ToString(), Res.Get("SchoolProfile_Label_Tertiary_Edit")));
                    ViewData[StatisticUnitReportConstants.LS_APPLIED] = lst;
                }
                if (Global.IsSubSuperVisingDeptRole)
                {
                    lst.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString(), Res.Get("SchoolProfile_Label_Primary")));
                    lst.Add(new ComboObject(SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString(), Res.Get("SchoolProfile_Label_Secondary_Edit")));
                    ViewData[StatisticUnitReportConstants.LS_APPLIED] = lst;
                }

            }
            //Khoi hoc
            List<EducationLevel> lstEdu = EducationLevelBusiness.All.Where(o => o.Grade == SystemParamsInFile.APPLIED_LEVEL_PRIMARY && o.IsActive == true).ToList();
            ViewData[StatisticUnitReportConstants.LS_EDUCATIONLEVEL] = lstEdu;
            //Hoc ki
            List<ComboObject> lstSemester = CommonList.Semester();
            ViewData[StatisticUnitReportConstants.LS_SEMESTER] = lstSemester;
            //Quan huyen
            ViewData[StatisticUnitReportConstants.DISTRICT_INT] = "0";
            if (Global.IsSubSuperVisingDeptRole)
            {
                dic["DistrictID"] = Global.DistrictID;
                ViewData[StatisticUnitReportConstants.DISTRICT_INT] = Global.DistrictID;
            }

            List<District> lstDistrict = DistrictBusiness.Search(dic).ToList();
            lstDistrict = lstDistrict.OrderBy(o => o.DistrictName).ToList();
            ViewData[StatisticUnitReportConstants.LS_DISTRICT] = lstDistrict;
        }

        public PartialViewResult Search(SearchViewModel frm)
        {
            if (frm.Year == null)
            {
                //Thầy cô chưa khai báo năm học hoặc nhà trường thuộc phòng sở chưa khai báo năm học
                throw new BusinessException("StatisticUnitReport_Validate_Year");
            }
            List<GridModel> lstGrid = new List<GridModel>();
            Utils.Utils.TrimObject(frm);
            GlobalInfo Global = new GlobalInfo();
            IDictionary<string, object> dicSearch = new Dictionary<string, object>();
            //dicSearch["SupervisingDeptID"] = Global.SupervisingDeptID;
            int? supervisingDeptID = Global.SupervisingDeptID;
            int? ProvinceID = Global.ProvinceID;
            dicSearch["Year"] = frm.Year;
            dicSearch["AppliedLevel"] = frm.AppliedLevel;
            dicSearch["Semester"] = frm.Semester;
            dicSearch["EducationLevel"] = frm.EducationLevel;
            SupervisingDept super = SupervisingDeptBusiness.Find(supervisingDeptID);
            if (!Global.IsSubSuperVisingDeptRole)
            {
                dicSearch["ProvinceID"] = Global.ProvinceID;
                dicSearch["DistrictID"] = frm.District;
                if (super.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
                {
                    supervisingDeptID = super.ParentID.Value;
                }
                dicSearch["SupervisingDeptID"] = supervisingDeptID;
            }
            else
            {
                if (super.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    supervisingDeptID = super.ParentID.Value;
                }
                dicSearch["SupervisingDeptID"] = supervisingDeptID;
            }
            dicSearch["IsActive"] = true;
            // Tim danh sach cac truong
            IQueryable<SchoolProfile> lstSchoolIQ = SchoolProfileBusiness.Search(dicSearch);
            List<SchoolProfile> lstSchool = lstSchoolIQ.ToList();
            lstSchool = lstSchool.OrderBy(o => o.SchoolName).ToList();

            List<int> lstSchoolID = lstSchool.Select(o => o.SchoolProfileID).ToList();
            //Khoi
            List<EducationLevel> lstEdu = EducationLevelBusiness.All.Where(o => o.Grade == frm.AppliedLevel && o.IsActive == true).ToList();
            if (frm.EducationLevel != null)
            {
                lstEdu = lstEdu.Where(o => o.EducationLevelID == frm.EducationLevel).ToList();
            }
            //Mon hoc
            IQueryable<SchoolSubject> lstSubjectObject = SchoolSubjectBusiness.All.Where(o => lstSchoolID.Contains(o.SchoolID) && o.SubjectCat.IsActive == true && o.SubjectCat.AppliedLevel == frm.AppliedLevel && o.AcademicYear.Year == frm.Year);
            List<SubjectObject> lstSchoolSubject = lstSubjectObject.Select(o => new SubjectObject { SubjectID = o.SubjectID, SubjectName = o.SubjectCat.SubjectName, OrderInSubject = o.SubjectCat.OrderInSubject }).Distinct().ToList();
            lstSchoolSubject = lstSchoolSubject.OrderBy(o => o.OrderInSubject).ToList();
            List<SchoolSubjectBO> lstSubjectSchool = lstSubjectObject.Select(o => new SchoolSubjectBO { SubjectID = o.SubjectID, SubjectName = o.SubjectCat.SubjectName, SchoolID = o.SchoolID, EducationLevelID = o.EducationLevelID }).ToList();
            //ViewData[StatisticUnitReportConstants.LS_SUBJECT] = lstSchoolSubject;
            //Lấy danh sách môn học
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["Year"] = frm.Year;
            dic["SupervisingDeptID"] = supervisingDeptID;
            dic["ProvinceID"] = Global.ProvinceID;
            dic["AppliedLevel"] = frm.AppliedLevel;
            dic["EducationLevelID"] = 0;
            List<SubjectCatBO> lstSubjectCatBO = SupervisingDeptBusiness.SearchSubject(dic);
            ViewData[StatisticUnitReportConstants.LS_SUBJECT] = lstSubjectCatBO;
            //Ngay gui bao cao dinh ki va hoc ki
            dicSearch["SentToSupervisor"] = true;
            //List<MarkStatistic> lstMark = MarkStatisticBusiness.Search(dicSearch).ToList();
            //List<MarkStatistic> listMark = new List<MarkStatistic>();
            //Ngay gui bao cao HLM
            //List<CapacityStatistic> lstCapa = CapacityStatisticBusiness.Search(dicSearch).ToList();
            //List<CapacityStatistic> listCapa = new List<CapacityStatistic>();
            #region thống kê điểm kiểm tra định kì
            if (frm.rptReportType == 1)
            {
                dicSearch["ReportCode"] = "DinhKy";
                List<MarkStatistic> listMark = (from p in MarkStatisticBusiness.Search(dicSearch)
                                                join q in lstSchoolIQ on p.SchoolID equals q.SchoolProfileID
                                                select p).ToList();

                //List<MarkStatistic>  listMark = lstMark.Where(o => o.ReportCode.Contains("DinhKy")).ToList();

                GridModel gm = new GridModel();
                foreach (SchoolProfile sp in lstSchool)
                {
                    List<SchoolSubjectBO> lstSubjectTemp = lstSubjectSchool.Where(o => o.SchoolID == sp.SchoolProfileID).ToList();
                    foreach (EducationLevel el in lstEdu)
                    {
                        List<int> lstSubject = lstSubjectTemp.Where(o => o.EducationLevelID == el.EducationLevelID).Select(o => o.SubjectID).ToList();
                        gm = new GridModel();
                        gm.SchoolID = sp.SchoolProfileID;
                        gm.EducationLevelID = el.EducationLevelID;
                        gm.SchoolName = sp.SchoolName;
                        gm.Resolution = el.Resolution;
                        gm.dicSubject = new Dictionary<string, object>();
                        foreach (SubjectCatBO Sub in lstSubjectCatBO)
                        {

                            if (!lstSubject.Any(item => item == Sub.SubjectCatID))
                            {
                                gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString()] = "x";
                            }
                            else
                            {
                                MarkStatistic markStatistic = listMark.Where(o => o.EducationLevelID == el.EducationLevelID
                               && o.SchoolID == sp.SchoolProfileID
                               && o.SubjectID == Sub.SubjectCatID).OrderByDescending(o => o.ProcessedDate).FirstOrDefault();
                                DateTime? ReportDate = null;
                                if (markStatistic != null)
                                {
                                    ReportDate = markStatistic.ProcessedDate;
                                }
                                if (ReportDate.HasValue)
                                {
                                    gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString()] = String.Format("{0:dd/MM/yyyy}", ReportDate).ToString();
                                }
                                else
                                {
                                    gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString()] = "";
                                }
                            }
                        }
                        lstGrid.Add(gm);
                    }
                }
                ViewData[StatisticUnitReportConstants.LS_GRID] = lstGrid;

                return PartialView("_ListReport");

            }
            #endregion
            #region thống kê điểm kiểm tra học kì
            if (frm.rptReportType == 2)
            {
                dicSearch["ReportCode"] = "HocKy";
                List<MarkStatistic> listMark = (from p in MarkStatisticBusiness.Search(dicSearch)
                                                join q in lstSchoolIQ on p.SchoolID equals q.SchoolProfileID
                                                select p).ToList();
                //IDictionary<string, object> dicCapa = new Dictionary<string, object>();
                //dicCapa["Year"] = frm.Year;
                //dicCapa["AppliedLevel"] = frm.AppliedLevel;
                //dicCapa["Semester"] = frm.Semester;
                //dicCapa["EducationLevel"] = frm.EducationLevel;
                //dicCapa["ReportCode"] = "HocKy";
                //dicCapa["SentToSupervisor"] = true;
                //List<CapacityStatistic> listCapa = (from p in CapacityStatisticBusiness.Search(dicSearch)
                //                                       join q in lstSchoolIQ on p.SchoolID equals q.SchoolProfileID
                //                                    select p).ToList();

                if (frm.AppliedLevel == 1)
                {
                    //listMark = lstMark.Where(o => o.ReportCode.Contains("HocKy")).ToList();
                    //Diem HLM
                    //listCapa = lstCapa.Where(o => o.ReportCode.Contains("HocLucMon")).ToList();
                    IDictionary<string, object> dicCapa = new Dictionary<string, object>();
                    dicCapa["Year"] = frm.Year;
                    dicCapa["AppliedLevel"] = frm.AppliedLevel;
                    dicCapa["Semester"] = frm.Semester;
                    dicCapa["EducationLevel"] = frm.EducationLevel;
                    dicCapa["ReportCode"] = "HocLucMon";
                    dicCapa["SentToSupervisor"] = true;
                    List<CapacityStatistic> listCapa = (from p in CapacityStatisticBusiness.Search(dicCapa)
                                                        join q in lstSchoolIQ on p.SchoolID equals q.SchoolProfileID
                                                        select p).ToList();
                    GridModel gm = new GridModel();
                    foreach (SchoolProfile sp in lstSchool)
                    {
                        List<SchoolSubjectBO> lstSubjectTemp = lstSubjectSchool.Where(o => o.SchoolID == sp.SchoolProfileID).ToList();
                        foreach (EducationLevel el in lstEdu)
                        {
                            List<int> lstSubject = lstSubjectTemp.Where(o => o.EducationLevelID == el.EducationLevelID).Select(o => o.SubjectID).ToList();
                            gm = new GridModel();
                            gm.SchoolID = sp.SchoolProfileID;
                            gm.EducationLevelID = el.EducationLevelID;
                            gm.SchoolName = sp.SchoolName;
                            gm.Resolution = el.Resolution;
                            gm.dicSubject = new Dictionary<string, object>();
                            foreach (SubjectCatBO Sub in lstSubjectCatBO)
                            {
                                if (lstSubject.Any(o => o == Sub.SubjectCatID))
                                {
                                    MarkStatistic markStatistic = listMark.Where(o => o.EducationLevelID == el.EducationLevelID
                                        && o.SchoolID == sp.SchoolProfileID
                                        && o.SubjectID == Sub.SubjectCatID).FirstOrDefault();
                                    DateTime? ReportDate = null;
                                    if (markStatistic != null)
                                    {
                                        ReportDate = markStatistic.ProcessedDate;
                                    }
                                    if (ReportDate.HasValue)
                                    {
                                        gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString()] = String.Format("{0:dd/MM/yyyy}", ReportDate).ToString();
                                    }
                                    else
                                    {
                                        gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString()] = "";
                                    }
                                    CapacityStatistic capacityStatistic = listCapa.Where(o => o.EducationLevelID == el.EducationLevelID
                                        && o.SchoolID == sp.SchoolProfileID
                                        && o.SubjectID == Sub.SubjectCatID).OrderByDescending(o => o.SentDate).FirstOrDefault();
                                    DateTime? ReportDateHLM = null;
                                    if (capacityStatistic != null)
                                    {
                                        ReportDateHLM = capacityStatistic.SentDate;
                                    }
                                    if (ReportDateHLM.HasValue)
                                    {
                                        gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString() + "_TBM"] = String.Format("{0:dd/MM/yyyy}", ReportDateHLM).ToString();
                                    }
                                    else
                                    {
                                        gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString() + "_TBM"] = "";
                                    }
                                }
                                else
                                {
                                    gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString()] = "x";
                                    gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString() + "_TBM"] = "x";
                                }
                            }
                            lstGrid.Add(gm);
                        }
                    }
                    ViewData[StatisticUnitReportConstants.LS_GRID] = lstGrid;

                    return PartialView("_ListReportSemesterSecond");
                }
                else
                {
                    //diem KTHK
                    //listMark = lstMark.Where(o => o.ReportCode.Contains("HocKy")).ToList();
                    GridModel gm = new GridModel();
                    foreach (SchoolProfile sp in lstSchool)
                    {
                        List<SchoolSubjectBO> lstSubjectTemp = lstSubjectSchool.Where(o => o.SchoolID == sp.SchoolProfileID).ToList();
                        foreach (EducationLevel el in lstEdu)
                        {
                            List<int> lstSubject = lstSubjectTemp.Where(o => o.EducationLevelID == el.EducationLevelID).Select(o => o.SubjectID).ToList();
                            gm = new GridModel();
                            gm.SchoolID = sp.SchoolProfileID;
                            gm.EducationLevelID = el.EducationLevelID;
                            gm.SchoolName = sp.SchoolName;
                            gm.Resolution = el.Resolution;
                            gm.dicSubject = new Dictionary<string, object>();
                            foreach (SubjectCatBO Sub in lstSubjectCatBO)
                            {

                                if (!lstSubject.Any(o => o == Sub.SubjectCatID))
                                {
                                    gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString()] = "x";
                                }
                                else
                                {
                                    MarkStatistic markStatistic = listMark.Where(o => o.EducationLevelID == el.EducationLevelID
                                    && o.SchoolID == sp.SchoolProfileID
                                    && o.SubjectID == Sub.SubjectCatID).OrderByDescending(o => o.ProcessedDate).FirstOrDefault();
                                    DateTime? ReportDate = null;
                                    if (markStatistic != null)
                                    {
                                        ReportDate = markStatistic.ProcessedDate;
                                    }
                                    if (ReportDate.HasValue)
                                    {
                                        gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString()] = String.Format("{0:dd/MM/yyyy}", ReportDate).ToString();
                                    }
                                    else
                                    {
                                        gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString()] = "";
                                    }
                                }

                            }
                            lstGrid.Add(gm);
                        }
                    }
                    ViewData[StatisticUnitReportConstants.LS_GRID] = lstGrid;

                    return PartialView("_ListReport");
                }

            }

            #endregion
            #region Thống kê điểm TBM,học lực hạnh kiểm
            if (frm.rptReportType == 3)
            {

                if (frm.AppliedLevel != 1)
                {
                    //Diem HLM
                    //listCapa = lstCapa.Where(o => o.ReportCode.Contains("HocLucMon")).ToList();
                    IDictionary<string, object> dicCapa = new Dictionary<string, object>();
                    dicCapa["Year"] = frm.Year;
                    dicCapa["AppliedLevel"] = frm.AppliedLevel;
                    dicCapa["Semester"] = frm.Semester;
                    dicCapa["EducationLevel"] = frm.EducationLevel;
                    //dicCapa["ReportCode"] = "HocLucMon";
                    dicCapa["SentToSupervisor"] = true;
                    if (frm.EducationLevel == null)
                    {
                        frm.EducationLevel = 0;
                    }
                    //List<CapacityStatistic> listCapa = CapacityStatisticBusiness.Search(dicSearch).ToList();
                    List<CapacityStatistic> listCapa = (from p in CapacityStatisticBusiness.All.Where(o => o.Year == frm.Year && o.AppliedLevel == frm.AppliedLevel && o.Semester == frm.Semester && (o.EducationLevelID == frm.EducationLevel || frm.EducationLevel == 0) && o.SentToSupervisor == true)
                                                        join q in lstSchoolIQ on p.SchoolID equals q.SchoolProfileID
                                                        select p).ToList();
                    List<ConductStatistic> listConduct = (from p in ConductStatisticBusiness.All.Where(o => o.Year == frm.Year && o.AppliedLevel == frm.AppliedLevel && o.Semester == frm.Semester && (o.EducationLevelID == frm.EducationLevel || frm.EducationLevel == 0) && o.SentToSupervisor == true)
                                                          join q in lstSchoolIQ on p.SchoolID equals q.SchoolProfileID
                                                          select p).ToList();
                    List<CapacityStatistic> listCapaHLM = listCapa.Where(o => o.ReportCode.Contains("HocLucMon")).ToList();
                    listCapaHLM = listCapaHLM.Where(o => o.SentDate == (from p in listCapaHLM where p.SchoolID == o.SchoolID && p.EducationLevelID == o.EducationLevelID && p.SubjectID == o.SubjectID select p.SentDate).Max()).ToList();
                    //Học lực theo khối
                    List<CapacityStatistic> listCapaHL = new List<CapacityStatistic>();
                    List<ConductStatistic> listCapaHK = new List<ConductStatistic>();
                    if (frm.AppliedLevel == 2)
                    {
                        listCapaHL = listCapa.Where(o => o.ReportCode.Contains("ThongKeHocLucCap")).ToList();
                        listCapaHL = listCapaHL.Where(o => o.SentDate == (from p in listCapaHL where p.SchoolID == o.SchoolID && p.EducationLevelID == o.EducationLevelID select p.SentDate).Max()).ToList();
                        listCapaHK = listConduct.Where(o => o.ReportCode.Contains("ThongKeHanhKiemCap")).ToList();
                        listCapaHK = listCapaHK.Where(o => o.SentDate == (from p in listCapaHK where p.SchoolID == o.SchoolID && p.EducationLevelID == o.EducationLevelID select p.SentDate).Max()).ToList();
                    }
                    else
                    {
                        listCapaHL = listCapa.Where(o => o.ReportCode.Contains("ThongKeHocLucTheoBanCap")).ToList();
                        listCapaHL = listCapaHL.Where(o => o.SentDate == (from p in listCapaHL where p.SchoolID == o.SchoolID && p.EducationLevelID == o.EducationLevelID select p.SentDate).Max()).ToList();
                        listCapaHK = listConduct.Where(o => o.ReportCode.Contains("ThongKeHanhKiemTheoBanCap")).ToList();
                        listCapaHK = listCapaHK.Where(o => o.SentDate == (from p in listCapaHK where p.SchoolID == o.SchoolID && p.EducationLevelID == o.EducationLevelID select p.SentDate).Max()).ToList();
                    }

                    GridModel gm = new GridModel();
                    foreach (SchoolProfile sp in lstSchool)
                    {
                        List<SchoolSubjectBO> lstSubjectTemp = lstSubjectSchool.Where(o => o.SchoolID == sp.SchoolProfileID).ToList();
                        foreach (EducationLevel el in lstEdu)
                        {
                            List<int> lstSubject = lstSubjectTemp.Where(o => o.EducationLevelID == el.EducationLevelID).Select(o => o.SubjectID).ToList();
                            gm = new GridModel();
                            gm.SchoolID = sp.SchoolProfileID;
                            gm.EducationLevelID = el.EducationLevelID;
                            gm.SchoolName = sp.SchoolName;
                            gm.Resolution = el.Resolution;
                            gm.dicSubject = new Dictionary<string, object>();
                            foreach (SubjectCatBO Sub in lstSubjectCatBO)
                            {
                                if (!lstSubject.Any(item => item == Sub.SubjectCatID))
                                {
                                    gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString()] = "x";
                                }
                                else
                                {
                                    CapacityStatistic cs = listCapaHLM.Where(o => o.EducationLevelID == el.EducationLevelID
                                    && o.SchoolID == sp.SchoolProfileID
                                    && o.SubjectID == Sub.SubjectCatID).FirstOrDefault();
                                    DateTime? ReportDate = null;
                                    if (cs != null)
                                    {
                                        ReportDate = cs.SentDate;
                                    }
                                    if (ReportDate.HasValue)
                                    {
                                        gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString()] = String.Format("{0:dd/MM/yyyy}", ReportDate).ToString();
                                    }
                                    else
                                    {
                                        gm.dicSubject[sp.SchoolProfileID.ToString() + "_" + el.EducationLevelID.ToString() + "_" + Sub.SubjectCatID.ToString()] = "";
                                    }
                                }

                            }
                            CapacityStatistic csHL = listCapaHL.Where(o => o.EducationLevelID == el.EducationLevelID && o.SchoolID == sp.SchoolProfileID).FirstOrDefault();
                            DateTime? ReportDateHL = null;
                            if (csHL != null)
                            {
                                ReportDateHL = csHL.SentDate;
                            }
                            if (ReportDateHL.HasValue)
                            {
                                gm.CapacityDate = String.Format("{0:dd/MM/yyyy}", ReportDateHL).ToString();
                            }
                            else
                            {
                                gm.CapacityDate = "";
                            }
                            ConductStatistic csHK = listCapaHK.Where(o => o.EducationLevelID == el.EducationLevelID && o.SchoolID == sp.SchoolProfileID).FirstOrDefault();
                            DateTime? ReportDateHK = null;
                            if (csHK != null)
                            {
                                ReportDateHK = csHK.SentDate;
                            }
                            if (ReportDateHK.HasValue)
                            {
                                gm.ConductDate = String.Format("{0:dd/MM/yyyy}", ReportDateHK).ToString();
                            }
                            else
                            {
                                gm.ConductDate = "";
                            }
                            lstGrid.Add(gm);
                        }

                    }

                    ViewData[StatisticUnitReportConstants.LS_GRID] = lstGrid;

                    return PartialView("_ListReportTBM");
                }

            }
            #endregion
            return PartialView("");
        }

        private IQueryable<SchoolProfile> searchSchoolProfiles(int? supervisingDeptID,int? ProvinceID, SearchViewModel searchViewModel)
        {
            IDictionary<string, object> dicSearch = new Dictionary<string, object>();
            dicSearch["Year"] = searchViewModel.Year;
            dicSearch["AppliedLevel"] = searchViewModel.AppliedLevel;
            dicSearch["DistrictID"] = searchViewModel.District;
            dicSearch["ProvinceID"] = ProvinceID;
            dicSearch["Semester"] = searchViewModel.Semester;
            dicSearch["EducationLevel"] = searchViewModel.EducationLevel;
            SupervisingDept super = SupervisingDeptBusiness.Find(supervisingDeptID);
            if (super.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || super.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
                supervisingDeptID = super.ParentID;
            }
            IQueryable<SchoolProfile> lstSchoolIQ = from p in SchoolProfileBusiness.Search(dicSearch)
                                                    join q in SupervisingDeptBusiness.All on p.SupervisingDeptID equals q.SupervisingDeptID
                                                    where (q.SupervisingDeptID == supervisingDeptID || q.ParentID == supervisingDeptID) && p.IsActive == true
                                                    orderby p.SchoolName
                                                    select p;
            return lstSchoolIQ;
        }

        public JsonResult AjaxLoadEducationLevel(int? AppliedLevel)
        {
            IEnumerable<EducationLevel> lst = new List<EducationLevel>();
            lst = EducationLevelBusiness.All.Where(o => o.Grade == AppliedLevel && o.IsActive == true).ToList();

            return Json(new SelectList(lst, "EducationLevelID", "Resolution"));
        }

        public JsonResult AjaxLoadSemester(int? AppliedLevel, int? ReportType)
        {
            List<ComboObject> lstSemester = new List<ComboObject>();
            //if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            //{
            //    lstSemester = CommonList.SemesterAndAll();
            //}
            //else
            //{
            //    lstSemester = CommonList.Semester();
            //}
            if ((AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY) && ReportType == 3)
            {
                lstSemester = CommonList.SemesterAndAll();
            }
            else
            {
                lstSemester = CommonList.Semester();
            }
            return Json(new SelectList(lstSemester, "key", "value"));
        }

        #region ExportExcel
        public FileResult ExportExcel(SearchViewModel searchViewModel
            , int rptReportType = 0, int Year = 0, int AppliedLevel = 0, int Semester = 0, int EducationLevel = 0, int District = 0)
        {
            GlobalInfo Global = GlobalInfo.getInstance();

            string strYear = Request["strYear"];
            string strAppliedLevel = Request["strAppliedLevel"];

            #region lay du lieu
            IDictionary<string, object> dicSearch = new Dictionary<string, object>();
            dicSearch["Year"] = Year;
            dicSearch["AppliedLevel"] = AppliedLevel;
            dicSearch["DistrictID"] = District;
            dicSearch["ProvinceID"] = Global.ProvinceID;
            dicSearch["Semester"] = Semester;
            int? supervisingDeptID = Global.SupervisingDeptID;
            string EducationLevelName = "";
            
            //truong
            IQueryable<SchoolProfile> lstSchoolIQ = this.searchSchoolProfiles(Global.SupervisingDeptID, Global.ProvinceID, searchViewModel);
            List<SchoolProfile> lstSchool = lstSchoolIQ.ToList();
            lstSchool = lstSchool.OrderBy(o => o.SchoolName).ToList();
            List<int> lstSchoolID = lstSchool.Select(o => o.SchoolProfileID).ToList();
            
            //Khoi
            List<EducationLevel> lstEdu = EducationLevelBusiness.All.Where(o => o.Grade == AppliedLevel && o.IsActive == true).ToList();
            if (EducationLevel != 0)
            {
                lstEdu = lstEdu.Where(o => o.EducationLevelID == EducationLevel).ToList();
                EducationLevelName = ReportUtils.StripVNSign(lstEdu.FirstOrDefault().Resolution);
            }
            //Mon hoc
            //List<SubjectObject> lstSchoolSubject = SchoolSubjectBusiness.All.Where(o => lstSchoolID.Contains(o.SchoolID) && o.SubjectCat.AppliedLevel == AppliedLevel).Select(o => new SubjectObject { SubjectID = o.SubjectID, SubjectName = o.SubjectCat.SubjectName }).Distinct().ToList();
            IQueryable<SchoolSubject> lstSubjectObject = SchoolSubjectBusiness.All.Where(o => lstSchoolID.Contains(o.SchoolID) && o.SubjectCat.IsActive == true && o.SubjectCat.AppliedLevel == AppliedLevel && o.AcademicYear.Year == Year);
            List<SubjectObject> lstSchoolSubject = lstSubjectObject.Select(o => new SubjectObject { SubjectID = o.SubjectID, SubjectName = o.SubjectCat.SubjectName, OrderInSubject = o.SubjectCat.OrderInSubject }).Distinct().ToList();
            lstSchoolSubject = lstSchoolSubject.OrderBy(o => o.OrderInSubject).ToList();
            List<SchoolSubjectBO> lstSubjectSchool = lstSubjectObject.Select(o => new SchoolSubjectBO { SubjectID = o.SubjectID, SubjectName = o.SubjectCat.SubjectName, SchoolID = o.SchoolID, EducationLevelID = o.EducationLevelID }).ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["Year"] = Year;
            dic["SupervisingDeptID"] = supervisingDeptID;
            dic["ProvinceID"] = Global.ProvinceID;
            dic["AppliedLevel"] = AppliedLevel;
            dic["EducationLevelID"] = 0;
            List<SubjectCatBO> lstSubjectCatBO = SupervisingDeptBusiness.SearchSubject(dic);
            int countSubject = lstSubjectCatBO.Count();

            //Ngay gui bao cao dinh ki va hoc ki
            //List<MarkStatistic> lstMark = MarkStatisticBusiness.Search(dicSearch).ToList();
            List<MarkStatistic> listMark = new List<MarkStatistic>();
            //Ngay gui bao cao HLM
            List<CapacityStatistic> listCapa = new List<CapacityStatistic>();
            #endregion
            #region Thống kê đơn vị báo cáo điểm trung bình môn
            if (rptReportType == 3)
            {
                if (AppliedLevel != 1)
                {
                    //Diem HLM
                    IDictionary<string, object> dicCapa = new Dictionary<string, object>();
                    dicCapa["Year"] = Year;
                    dicCapa["AppliedLevel"] = AppliedLevel;
                    dicCapa["Semester"] = Semester;
                    dicCapa["EducationLevel"] = EducationLevel;
                    //dicCapa["ReportCode"] = "HocLucMon";
                    dicCapa["SentToSupervisor"] = true;
                    //listCapa = CapacityStatisticBusiness.Search(dicCapa).ToList();
                    //List<CapacityStatistic> listCapa = CapacityStatisticBusiness.Search(dicSearch).ToList();
                    listCapa = (from p in CapacityStatisticBusiness.All.Where(o => o.Year == Year && o.AppliedLevel == AppliedLevel && o.Semester == Semester && (o.EducationLevelID == EducationLevel || EducationLevel == 0) && o.SentToSupervisor == true)
                                join q in lstSchoolIQ on p.SchoolID equals q.SchoolProfileID
                                select p).ToList();
                    List<ConductStatistic> listConduct = (from p in ConductStatisticBusiness.All.Where(o => o.Year == Year && o.AppliedLevel == AppliedLevel && o.Semester == Semester && (o.EducationLevelID == EducationLevel || EducationLevel == 0) && o.SentToSupervisor == true)
                                                          join q in lstSchoolIQ on p.SchoolID equals q.SchoolProfileID
                                                          select p).ToList();
                    List<CapacityStatistic> listCapaHLM = listCapa.Where(o => o.ReportCode.Contains("HocLucMon")).ToList();
                    listCapaHLM = listCapaHLM.Where(o => o.SentDate == (from p in listCapaHLM where p.SchoolID == o.SchoolID && p.EducationLevelID == o.EducationLevelID && p.SubjectID == o.SubjectID select p.SentDate).Max()).ToList();
                    //Học lực theo khối
                    List<CapacityStatistic> listCapaHL = new List<CapacityStatistic>();
                    List<ConductStatistic> listCapaHK = new List<ConductStatistic>();
                    if (AppliedLevel == 2)
                    {
                        listCapaHL = listCapa.Where(o => o.ReportCode.Contains("ThongKeHocLucCap")).ToList();
                        listCapaHL = listCapaHL.Where(o => o.SentDate == (from p in listCapaHL where p.SchoolID == o.SchoolID && p.EducationLevelID == o.EducationLevelID select p.SentDate).Max()).ToList();
                        listCapaHK = listConduct.Where(o => o.ReportCode.Contains("ThongKeHanhKiemCap")).ToList();
                        listCapaHK = listCapaHK.Where(o => o.SentDate == (from p in listCapaHK where p.SchoolID == o.SchoolID && p.EducationLevelID == o.EducationLevelID select p.SentDate).Max()).ToList();
                    }
                    else
                    {
                        listCapaHL = listCapa.Where(o => o.ReportCode.Contains("ThongKeHocLucTheoBanCap")).ToList();
                        listCapaHL = listCapaHL.Where(o => o.SentDate == (from p in listCapaHL where p.SchoolID == o.SchoolID && p.EducationLevelID == o.EducationLevelID select p.SentDate).Max()).ToList();
                        listCapaHK = listConduct.Where(o => o.ReportCode.Contains("ThongKeHanhKiemTheoBanCap")).ToList();
                        listCapaHK = listCapaHK.Where(o => o.SentDate == (from p in listCapaHK where p.SchoolID == o.SchoolID && p.EducationLevelID == o.EducationLevelID select p.SentDate).Max()).ToList();
                    }
                    string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "Phong_So" + "/" + "SoGD_THCS_TKDonViBCDiemTBM.xls";
                    IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                    IVTWorksheet Template = oBook.GetSheet(1);
                    IVTWorksheet sheet = oBook.GetSheet(2);
                    //fill du lieu 

                    sheet.CopyPasteSameSize(Template.GetRange("A1", "N6"), 1, 1);
                    SupervisingDept sd = SupervisingDeptBusiness.Find(Global.SupervisingDeptID);
                    Dictionary<string, object> dicVarable = new Dictionary<string, object>();
                    dicVarable["SupervisingDept"] = sd.SupervisingDeptName.ToUpper();
                    if (District != 0)
                    {
                        dicVarable["District"] = DistrictBusiness.Find(District).DistrictName;
                    }
                    else
                    {
                        dicVarable["District"] = "[Tất cả]";
                    }
                    dicVarable["AppliedLevel"] = strAppliedLevel;
                    dicVarable["Year"] = strYear;
                    dicVarable["Semester"] = CommonConvert.ConvertSemester(Semester).ToUpper();
                    if (EducationLevel == 0)
                    {
                        dicVarable["EducationLevel"] = "[Tất cả]";
                    }
                    else
                    {
                        dicVarable["EducationLevel"] = EducationLevelBusiness.Find(EducationLevel).Resolution;
                    }
                    sheet.FillVariableValue(dicVarable);

                    //Dong dau
                    IVTRange rangeFirst = Template.GetRange(7, 1, 7, 5 + countSubject);
                    //Dong giua
                    IVTRange rangeMidde = Template.GetRange(8, 1, 8, 5 + countSubject);
                    //Dong cuoi
                    IVTRange rangeLast = Template.GetRange(9, 1, 9, 5 + countSubject);
                    int StartRow = 7;

                    //Danh sach mon hoc
                    int startHeader = 4;
                    foreach (SubjectCatBO Sub in lstSubjectCatBO)
                    {

                        sheet.CopyPaste(Template.GetRange(6, 4, 6, 4), 6, startHeader, true);
                        sheet.SetCellValue(6, startHeader, Sub.SubjectName);
                        startHeader++;
                    }
                    //HỌc lực
                    sheet.CopyPaste(Template.GetRange(6, 4, 6, 4), 6, startHeader, true);
                    sheet.SetCellValue(6, startHeader, "Học Lực");
                    startHeader++;
                    //Hạnh Kiểm
                    sheet.CopyPaste(Template.GetRange(6, 4, 6, 4), 6, startHeader, true);
                    sheet.SetCellValue(6, startHeader, "Hạnh Kiểm");
                    startHeader++;
                    //Du lieu
                    int i = 1;
                    int StartEdu = 7;
                    foreach (SchoolProfile sp in lstSchool)
                    {
                        //Danh sach mon hoc tung truong
                        List<SchoolSubjectBO> ListSubject = lstSubjectSchool.Where(o => o.SchoolID == sp.SchoolProfileID).ToList();
                        //Duyet tung khoi
                        int FirstEdu = 0;
                        foreach (EducationLevel el in lstEdu)
                        {
                            if (FirstEdu == 0 && lstEdu.Count() > 1)
                            {
                                sheet.CopyPasteSameSize(rangeFirst, StartRow, 1);
                                sheet.SetCellValue(StartRow, 2, sp.SchoolName);
                            }
                            else if ((FirstEdu > 0 && FirstEdu == (lstEdu.Count() - 1)) || (lstEdu.Count() == 1))
                            {
                                sheet.CopyPasteSameSize(rangeLast, StartRow, 1);
                                if (lstEdu.Count() == 1)
                                {
                                    sheet.SetCellValue(StartRow, 2, sp.SchoolName);
                                }
                            }
                            else
                            {
                                sheet.CopyPasteSameSize(rangeMidde, StartRow, 1);
                            }
                            FirstEdu++;

                            //Danh sach id mon hoc tung khoi
                            List<int> lstID = ListSubject.Where(o => o.EducationLevelID == el.EducationLevelID).Select(o => o.SubjectID).ToList();
                            //STT
                            sheet.SetCellValue(StartRow, 1, i);
                            sheet.SetCellValue(StartRow, 3, el.Resolution);

                            int StartSubject = 4;
                            foreach (SubjectCatBO Sub in lstSubjectCatBO)
                            {
                                //Neu khoi ko hoc mon hoc nay thi se fill x neu co hoc thi fill ngay gui bao cao
                                if (!lstID.Any(item => item == Sub.SubjectCatID))
                                {
                                    sheet.SetCellValue(StartRow, StartSubject, "x");
                                    sheet.GetRange(StartRow, StartSubject, StartRow, StartSubject).FillColor(System.Drawing.Color.Yellow);
                                    StartSubject++;
                                }
                                else
                                {
                                    CapacityStatistic cs = listCapaHLM.Where(o => o.EducationLevelID == el.EducationLevelID
                                            && o.SchoolID == sp.SchoolProfileID
                                            && o.SubjectID == Sub.SubjectCatID).FirstOrDefault();
                                    DateTime? ReportDate = null;
                                    if (cs != null)
                                    {
                                        ReportDate = cs.SentDate;
                                    }
                                    if (ReportDate.HasValue)
                                    {
                                        sheet.SetCellValue(StartRow, StartSubject, String.Format("{0:dd/MM/yyyy}", ReportDate).ToString());
                                        StartSubject++;
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(StartRow, StartSubject, "");
                                        StartSubject++;
                                    }
                                }
                            }
                            //HỌc lỰc
                            CapacityStatistic csHL = listCapaHL.Where(o => o.SchoolID == sp.SchoolProfileID && o.EducationLevelID == el.EducationLevelID).FirstOrDefault();
                            DateTime? ReportDateHL = null;
                            if (csHL != null)
                            {
                                ReportDateHL = csHL.SentDate;
                            }
                            if (ReportDateHL.HasValue)
                            {
                                sheet.SetCellValue(StartRow, StartSubject, String.Format("{0:dd/MM/yyyy}", ReportDateHL).ToString());
                                StartSubject++;
                            }
                            else
                            {
                                sheet.SetCellValue(StartRow, StartSubject, "");
                                StartSubject++;
                            }
                            ConductStatistic csHK = listCapaHK.Where(o => o.SchoolID == sp.SchoolProfileID && o.EducationLevelID == el.EducationLevelID).FirstOrDefault();
                            DateTime? ReportDateHK = null;
                            if (csHK != null)
                            {
                                ReportDateHK = csHK.SentDate;
                            }
                            if (ReportDateHK.HasValue)
                            {
                                sheet.SetCellValue(StartRow, StartSubject, String.Format("{0:dd/MM/yyyy}", ReportDateHK).ToString());
                                StartSubject++;
                            }
                            else
                            {
                                sheet.SetCellValue(StartRow, StartSubject, String.Format("{0:dd/MM/yyyy}", ReportDateHK).ToString());
                                StartSubject++;
                            }
                            //Hạnh kiểm
                            StartRow++;
                            i++;

                        }
                        sheet.GetRange(StartEdu, 2, StartEdu + lstEdu.Count - 1, 2).MergeLeft();
                        StartEdu = StartEdu + lstEdu.Count;

                    }
                    sheet.CopyPasteSameSize(Template.GetRange("A12", "A12"), i + 10, 1);
                    sheet.FitAllColumnsOnOnePage = true;
                    oBook.GetSheet(1).Delete();
                    Stream excel = oBook.ToStream();//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
                    FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                    //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
                    string ReportName = "";
                    if (EducationLevel != 0)
                    {
                        ReportName = "SoGD_" + ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel) + "_TKDonViBCDiemTBM_" + EducationLevelName + "_" + ReportUtils.ConvertSemesterForReportName(Semester) + ".xls";
                        if (Global.IsSubSuperVisingDeptRole)
                        {
                            ReportName = "PGD_" + ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel) + "_TKDonViBCDiemTBM_" + EducationLevelName + "_" + ReportUtils.ConvertSemesterForReportName(Semester) + ".xls";
                        }
                    }
                    else
                    {
                        ReportName = "SoGD_" + ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel) + "_TKDonViBCDiemTBM_" + ReportUtils.ConvertSemesterForReportName(Semester) + ".xls";
                        if (Global.IsSubSuperVisingDeptRole)
                        {
                            ReportName = "PGD_" + ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel) + "_TKDonViBCDiemTBM_" + ReportUtils.ConvertSemesterForReportName(Semester) + ".xls";
                        }
                    }
                    result.FileDownloadName = ReportName;
                    return result;
                }
            }
            #endregion
            #region Thống kê điểm thi học kỳ
            else if (rptReportType == 2)
            {
                //Diem HLM
                if (AppliedLevel == 1)
                {
                    IDictionary<string, object> dicCapa = new Dictionary<string, object>();
                    dicCapa["Year"] = Year;
                    dicCapa["AppliedLevel"] = AppliedLevel;
                    dicCapa["Semester"] = Semester;
                    dicCapa["EducationLevel"] = EducationLevel;
                    dicCapa["ReportCode"] = "HocLucMon";
                    dicCapa["SentToSupervisor"] = true;
                    listCapa = (from p in CapacityStatisticBusiness.Search(dicCapa)
                                join q in lstSchoolIQ on p.SchoolID equals q.SchoolProfileID
                                select p).ToList();
                }

                IDictionary<string, object> dicMark = new Dictionary<string, object>();
                dicMark["Year"] = Year;
                dicMark["AppliedLevel"] = AppliedLevel;
                dicMark["Semester"] = Semester;
                dicMark["EducationLevel"] = EducationLevel;
                dicMark["ReportCode"] = "KiemTraHocKy";
                dicMark["SentToSupervisor"] = true;
                listMark = (from p in MarkStatisticBusiness.Search(dicMark)
                            join q in lstSchoolIQ on p.SchoolID equals q.SchoolProfileID
                            select p).ToList();

                string templatePath = "";
                if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                {
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "Phong_So" + "/" + "SoGD_TH_TKDonViBCDiemKTHocKy_HKII.xls";
                }
                else
                {
                    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "Phong_So" + "/" + "SoGD_THCS_TKDonViBCDiemKTHocKy.xls";
                }
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                IVTWorksheet Template = oBook.GetSheet(1);
                IVTWorksheet sheet = oBook.GetSheet(2);
                //fill du lieu 

                sheet.CopyPasteSameSize(Template.GetRange("A1", "N7"), 1, 1);
                SupervisingDept sd = SupervisingDeptBusiness.Find(Global.SupervisingDeptID);
                Dictionary<string, object> dicVarable = new Dictionary<string, object>();
                dicVarable["SupervisingDept"] = sd.SupervisingDeptName.ToUpper();
                if (District != 0)
                {
                    dicVarable["District"] = DistrictBusiness.Find(District).DistrictName;
                }
                else
                {
                    dicVarable["District"] = "[Tất cả]";
                }
                dicVarable["AppliedLevel"] = strAppliedLevel;
                dicVarable["StrYear"] = Year + " - " + (Year + 1);
                dicVarable["Semester"] = CommonConvert.ConvertSemester(Semester).ToUpper();
                if (EducationLevel == 0)
                {
                    dicVarable["EducationLevel"] = "[Tất cả]";
                }
                else
                {
                    dicVarable["EducationLevel"] = EducationLevelBusiness.Find(EducationLevel).Resolution;
                }
                sheet.FillVariableValue(dicVarable);
                IVTRange rangeFirst;
                IVTRange rangeMiddle;
                IVTRange rangeLast;
                if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                {
                    //Dong dau
                    rangeFirst = Template.GetRange(8, 1, 8, 3 + countSubject * 2);
                    //Dong giua
                    rangeMiddle = Template.GetRange(9, 1, 9, 3 + countSubject * 2);
                    //Dong cuoi
                    rangeLast = Template.GetRange(10, 1, 10, 3 + countSubject * 2);
                }
                else
                {
                    //Dong dau
                    rangeFirst = Template.GetRange(8, 1, 8, 3 + countSubject);
                    //Dong giua
                    rangeMiddle = Template.GetRange(9, 1, 9, 3 + countSubject);
                    //Dong cuoi
                    rangeLast = Template.GetRange(10, 1, 10, 3 + countSubject);
                }
                int StartRow = 8;

                //Danh sach mon hoc
                int startHeader = 4;
                foreach (SubjectCatBO Sub in lstSubjectCatBO)
                {
                    if (AppliedLevel != SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
                    {
                        sheet.CopyPaste(Template.GetRange(6, 4, 7, 4), 6, startHeader, true);
                        sheet.SetCellValue(6, startHeader, Sub.SubjectName);
                        startHeader++;
                    }
                    else
                    {
                        sheet.CopyPasteSameSize(Template.GetRange(6, 4, 7, 5), 6, startHeader);
                        sheet.SetCellValue(6, startHeader, Sub.SubjectName);
                        startHeader += 2;
                    }
                }
                //Du lieu
                int i = 1;
                int StartEdu = 8;
                foreach (SchoolProfile sp in lstSchool)
                {
                    //Danh sach mon hoc tung truong
                    List<SchoolSubjectBO> ListSubject = lstSubjectSchool.Where(o => o.SchoolID == sp.SchoolProfileID).ToList();
                    //Duyet tung khoi
                    int FirstEdu = 0;
                    foreach (EducationLevel el in lstEdu)
                    {
                        if (FirstEdu == 0 && lstEdu.Count() > 1)
                        {
                            sheet.CopyPasteSameSize(rangeFirst, StartRow, 1);
                            sheet.SetCellValue(StartRow, 2, sp.SchoolName);
                        }
                        else if ((FirstEdu == (lstEdu.Count() - 1) && FirstEdu > 0) || (lstEdu.Count() == 1))
                        {
                            sheet.CopyPasteSameSize(rangeLast, StartRow, 1);
                            if (lstEdu.Count() == 1)
                            {
                                sheet.SetCellValue(StartRow, 2, sp.SchoolName);
                            }
                        }
                        else
                        {
                            sheet.CopyPasteSameSize(rangeMiddle, StartRow, 1);
                        }
                        FirstEdu++;
                        //Danh sach id mon hoc tung khoi
                        List<int> lstID = ListSubject.Where(o => o.EducationLevelID == el.EducationLevelID).Select(o => o.SubjectID).ToList();
                        //STT
                        sheet.SetCellValue(StartRow, 1, i);
                        sheet.SetCellValue(StartRow, 3, el.Resolution);

                        int StartSubject = 4;
                        foreach (SubjectCatBO Sub in lstSubjectCatBO)
                        {
                            //Neu khoi ko hoc mon hoc nay thi se fill x neu co hoc thi fill ngay gui bao cao
                            if (AppliedLevel == 1)
                            {
                                if (!lstID.Any(item => item == Sub.SubjectCatID))
                                {
                                    sheet.SetCellValue(StartRow, StartSubject, "x");
                                    sheet.GetRange(StartRow, StartSubject, StartRow, StartSubject).FillColor(System.Drawing.Color.Yellow);
                                    StartSubject++;
                                    sheet.SetCellValue(StartRow, StartSubject, "x");
                                    sheet.GetRange(StartRow, StartSubject, StartRow, StartSubject).FillColor(System.Drawing.Color.Yellow);
                                    StartSubject++;
                                }
                                else
                                {
                                    CapacityStatistic cs = listCapa.Where(o => o.EducationLevelID == el.EducationLevelID
                                            && o.SchoolID == sp.SchoolProfileID
                                            && o.SubjectID == Sub.SubjectCatID).FirstOrDefault();
                                    MarkStatistic ms = listMark.Where(o => o.EducationLevelID == el.EducationLevelID &&
                                        o.SchoolID == sp.SchoolProfileID && o.SubjectID == Sub.SubjectCatID).FirstOrDefault();

                                    DateTime? ReportDate = null;
                                    DateTime? ReportDateMark = null;
                                    if (cs != null)
                                    {
                                        ReportDate = cs.SentDate;
                                    }
                                    if (ms != null)
                                    {
                                        ReportDateMark = ms.ProcessedDate;
                                    }
                                    if (ReportDateMark.HasValue)
                                    {
                                        sheet.SetCellValue(StartRow, StartSubject, String.Format("{0:dd/MM/yyyy}", ReportDateMark).ToString());
                                        StartSubject++;
                                        if (ReportDate.HasValue)
                                        {
                                            sheet.SetCellValue(StartRow, StartSubject, String.Format("{0:dd/MM/yyyy}", ReportDate).ToString());
                                            StartSubject++;
                                        }
                                        else
                                        {
                                            sheet.SetCellValue(StartRow, StartSubject, "");
                                            StartSubject++;
                                        }
                                    }
                                    else
                                    {

                                        sheet.SetCellValue(StartRow, StartSubject, "");
                                        StartSubject++;
                                        if (ReportDate.HasValue)
                                        {
                                            sheet.SetCellValue(StartRow, StartSubject, String.Format("{0:dd/MM/yyyy}", ReportDate).ToString());
                                            StartSubject++;
                                        }
                                        else
                                        {
                                            sheet.SetCellValue(StartRow, StartSubject, "");
                                            StartSubject++;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (!lstID.Any(item => item == Sub.SubjectCatID))
                                {
                                    sheet.SetCellValue(StartRow, StartSubject, "x");
                                    sheet.GetRange(StartRow, StartSubject, StartRow, StartSubject).FillColor(System.Drawing.Color.Yellow);
                                    StartSubject++;
                                }
                                else
                                {
                                    MarkStatistic ms = listMark.Where(o => o.EducationLevelID == el.EducationLevelID &&
                                        o.SchoolID == sp.SchoolProfileID && o.SubjectID == Sub.SubjectCatID).FirstOrDefault();

                                    DateTime? ReportDate = null;
                                    if (ms != null)
                                    {
                                        ReportDate = ms.ProcessedDate;
                                    }
                                    if (ReportDate.HasValue)
                                    {
                                        sheet.SetCellValue(StartRow, StartSubject, String.Format("{0:dd/MM/yyyy}", ReportDate).ToString());
                                        StartSubject++;
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(StartRow, StartSubject, "");
                                        StartSubject++;
                                    }
                                }
                            }
                        }

                        StartRow++;
                        i++;
                    }
                    sheet.GetRange(StartEdu, 2, StartEdu + lstEdu.Count - 1, 2).MergeLeft();
                    StartEdu = StartEdu + lstEdu.Count;

                }
                sheet.CopyPasteSameSize(Template.GetRange("A12", "A12"), i + 10, 1);
                sheet.FitAllColumnsOnOnePage = true;
                oBook.GetSheet(1).Delete();
                Stream excel = oBook.ToStream();//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
                string ReportName = "";
                if (EducationLevel != 0)
                {
                    ReportName = "SoGD_" + ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel) + "_TKDonViBCDiemKTHocKy_" + EducationLevelName + "_" + ReportUtils.ConvertSemesterForReportName(Semester) + ".xls";
                    if (Global.IsSubSuperVisingDeptRole)
                    {
                        ReportName = "PGD_" + ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel) + "_TKDonViBCDiemKTHocKy_" + EducationLevelName + "_" + ReportUtils.ConvertSemesterForReportName(Semester) + ".xls";
                    }
                }
                else
                {
                    ReportName = "SoGD_" + ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel) + "_TKDonViBCDiemKTHocKy_" + ReportUtils.ConvertSemesterForReportName(Semester) + ".xls";
                    if (Global.IsSubSuperVisingDeptRole)
                    {
                        ReportName = "PGD_" + ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel) + "_TKDonViBCDiemKTHocKy_" + ReportUtils.ConvertSemesterForReportName(Semester) + ".xls";
                    }
                }
                result.FileDownloadName = ReportName;
                return result;
            }
            #endregion
            #region Thống kê điểm kiểm tra định kỳ
            else
            {
                //Diem HLM
                IDictionary<string, object> dicMark = new Dictionary<string, object>();
                dicMark["Year"] = Year;
                dicMark["AppliedLevel"] = AppliedLevel;
                dicMark["Semester"] = Semester;
                dicMark["EducationLevel"] = EducationLevel;
                dicMark["ReportCode"] = "DinhKy";
                dicMark["SentToSupervisor"] = true;

                listMark = (from p in MarkStatisticBusiness.Search(dicMark)
                            join q in lstSchoolIQ on p.SchoolID equals q.SchoolProfileID
                            select p).ToList();

                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "Phong_So" + "/" + "SoGD_THCS_TKDonViBCDiemKTDinhKy.xls";
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                IVTWorksheet Template = oBook.GetSheet(1);
                IVTWorksheet sheet = oBook.GetSheet(2);
                //fill du lieu 

                sheet.CopyPasteSameSize(Template.GetRange("A1", "N6"), 1, 1);
                SupervisingDept sd = SupervisingDeptBusiness.Find(Global.SupervisingDeptID);
                Dictionary<string, object> dicVarable = new Dictionary<string, object>();
                dicVarable["SupervisingDept"] = sd.SupervisingDeptName.ToUpper();
                if (AppliedLevel == 1)
                {
                    dicVarable["AppliedLevelName"] = "Tiểu học";
                }
                else if (AppliedLevel == 2)
                {
                    dicVarable["AppliedLevelName"] = "THCS";
                }
                else
                {
                    dicVarable["AppliedLevelName"] = "THPT";
                }
                if (District != 0)
                {
                    dicVarable["District"] = DistrictBusiness.Find(District).DistrictName;
                }
                else
                {
                    dicVarable["District"] = "[Tất cả]";
                }
                dicVarable["StrYear"] = Year + " - " + (Year + 1);
                dicVarable["Semester"] = CommonConvert.ConvertSemester(Semester).ToUpper();
                if (EducationLevel == 0)
                {
                    dicVarable["EducationLevel"] = "[Tất cả]";
                }
                else
                {
                    dicVarable["EducationLevel"] = EducationLevelBusiness.Find(EducationLevel).Resolution;
                }
                sheet.FillVariableValue(dicVarable);

                //Dong dau

                IVTRange rangeFirst = Template.GetRange(7, 1, 7, 3 + countSubject);
                //Dong giua
                IVTRange rangeMiddle = Template.GetRange(8, 1, 8, 3 + countSubject);
                //Dong cuoi
                IVTRange rangeLast = Template.GetRange(11, 1, 11, 3 + countSubject);
                int StartRow = 7;

                //Danh sach mon hoc
                int startHeader = 4;
                foreach (SubjectCatBO Sub in lstSubjectCatBO)
                {

                    sheet.CopyPaste(Template.GetRange(6, 3, 6, 3), 6, startHeader, true);
                    sheet.SetCellValue(6, startHeader, Sub.SubjectName);
                    startHeader++;
                }
                //Du lieu
                int i = 1;
                int StartEdu = 7;
                foreach (SchoolProfile sp in lstSchool)
                {
                    //Danh sach mon hoc tung truong
                    List<SchoolSubjectBO> ListSubject = lstSubjectSchool.Where(o => o.SchoolID == sp.SchoolProfileID).ToList();
                    //Duyet tung khoi
                    int firstEdu = 0;
                    foreach (EducationLevel el in lstEdu)
                    {
                        //Danh sach id mon hoc tung khoi
                        if (firstEdu == 0 && lstEdu.Count() > 1)
                        {
                            sheet.CopyPasteSameSize(rangeFirst, StartRow, 1);
                            sheet.SetCellValue(StartRow, 2, sp.SchoolName);
                        }
                        else if ((firstEdu > 0 && (lstEdu.Count() - 1) == firstEdu) || lstEdu.Count() == 1)
                        {
                            sheet.CopyPasteSameSize(rangeLast, StartRow, 1);
                            if (lstEdu.Count() == 1)
                            {
                                sheet.SetCellValue(StartRow, 2, sp.SchoolName);
                            }
                        }
                        else
                        {
                            sheet.CopyPasteSameSize(rangeMiddle, StartRow, 1);
                        }
                        firstEdu++;
                        List<int> lstID = ListSubject.Where(o => o.EducationLevelID == el.EducationLevelID).Select(o => o.SubjectID).ToList();
                        //STT
                        sheet.SetCellValue(StartRow, 1, i);
                        sheet.SetCellValue(StartRow, 3, el.Resolution);
                        int StartSubject = 4;
                        foreach (SubjectCatBO Sub in lstSubjectCatBO)
                        {
                            //Neu khoi ko hoc mon hoc nay thi se fill x neu co hoc thi fill ngay gui bao cao
                            if (!lstID.Any(item => item == Sub.SubjectCatID))
                            {
                                sheet.SetCellValue(StartRow, StartSubject, "x");
                                sheet.GetRange(StartRow, StartSubject, StartRow, StartSubject).FillColor(System.Drawing.Color.Yellow);
                                StartSubject++;
                            }
                            else
                            {
                                MarkStatistic cs = listMark.Where(o => o.EducationLevelID == el.EducationLevelID
                                        && o.SchoolID == sp.SchoolProfileID
                                        && o.SubjectID == Sub.SubjectCatID).FirstOrDefault();
                                DateTime? ReportDate = null;
                                if (cs != null)
                                {
                                    ReportDate = cs.ProcessedDate;
                                }
                                if (ReportDate.HasValue)
                                {
                                    sheet.SetCellValue(StartRow, StartSubject, String.Format("{0:dd/MM/yyyy}", ReportDate).ToString());
                                    StartSubject++;
                                }
                                else
                                {
                                    sheet.SetCellValue(StartRow, StartSubject, "");
                                    StartSubject++;
                                }
                            }
                        }

                        StartRow++;
                        i++;
                    }
                    sheet.GetRange(StartEdu, 2, StartEdu + lstEdu.Count - 1, 2).MergeLeft();
                    StartEdu = StartEdu + lstEdu.Count;

                }
                sheet.CopyPasteSameSize(Template.GetRange("A13", "A13"), i + 7, 1);
                sheet.FitAllColumnsOnOnePage = true;
                oBook.GetSheet(1).Delete();
                Stream excel = oBook.ToStream();//ReportNumberOfPupilByTeacherBusiness.CreateReportNumberOfPupilByTeacher(global.SchoolID.Value, global.AcademicYearID.Value, Semester, FacultyID, lstTeacherID);
                FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
                //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
                string ReportName = "";
                if (EducationLevel != 0)
                {
                    ReportName = "SoGD_" + ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel) + "_TKDonViBCDiemKTDinhKy_" + EducationLevelName + "_" + ReportUtils.ConvertSemesterForReportName(Semester) + ".xls";
                    if (Global.IsSubSuperVisingDeptRole)
                    {
                        ReportName = "PGD_" + ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel) + "_TKDonViBCDiemKTDinhKy_" + EducationLevelName + "_" + ReportUtils.ConvertSemesterForReportName(Semester) + ".xls";
                    }
                }
                else
                {
                    ReportName = "SoGD_" + ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel) + "_TKDonViBCDiemKTDinhKy_" + ReportUtils.ConvertSemesterForReportName(Semester) + ".xls";
                    if (Global.IsSubSuperVisingDeptRole)
                    {
                        ReportName = "PGD_" + ReportUtils.ConvertAppliedLevelForReportName(AppliedLevel) + "_TKDonViBCDiemKTDinhKy_" + ReportUtils.ConvertSemesterForReportName(Semester) + ".xls";
                    }
                }
                result.FileDownloadName = ReportName;
                return result;
            }
            #endregion
            return null;
        }
        #endregion
    }
}
