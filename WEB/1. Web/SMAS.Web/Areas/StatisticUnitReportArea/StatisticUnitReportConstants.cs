﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticUnitReportArea
{
    public class StatisticUnitReportConstants
    {
        public const string LS_REPORTTYPE = "LS_REPORTTYPE";
        public const string LS_YEAR = "LS_YEAR";
        public const string LS_APPLIED = "LS_APPLIED";
        public const string LS_EDUCATIONLEVEL = "LS_EDUCATIONLEVEL";
        public const string LS_SEMESTER = "LS_SEMESTER";
        public const string LS_DISTRICT = "LS_DISTRICT";
        public const string LS_GRID = "LS_GRID";
        public const string LS_GRID_SEMESTER = "LS_GRID_SEMESTER";
        public const string LS_GRID_TBM = "LS_GRID_TBM";
        public const string LS_SUBJECT = "LS_SUBJECT";
        public const string DISTRICT_INT = "district_int";
    }
}