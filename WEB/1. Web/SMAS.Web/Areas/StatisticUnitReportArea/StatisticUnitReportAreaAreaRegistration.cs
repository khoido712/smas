﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticUnitReportArea
{
    public class StatisticUnitReportAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticUnitReportArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticUnitReportArea_default",
                "StatisticUnitReportArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
