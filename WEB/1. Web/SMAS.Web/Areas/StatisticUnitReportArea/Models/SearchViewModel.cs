﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticUnitReportArea.Models
{
    public class SearchViewModel
    {
        public int? Year { get; set; }
        public int AppliedLevel { get; set; }
        public int? EducationLevel { get; set; }
        public int? Semester { get; set; }
        public int? District { get; set; }
        public int rptReportType { get; set; }
    }
}