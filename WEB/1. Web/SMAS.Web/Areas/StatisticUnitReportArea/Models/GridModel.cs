﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticUnitReportArea.Models
{
    public class GridModel
    {
        public int SchoolID { get; set; }
        [ResourceDisplayName("GridMarkStatistics_Label_School")]
        public string SchoolName { get; set; }
        public int EducationLevelID { get; set; }
        [ResourceDisplayName("LockedMarkDetail_Label_EducationLevel")]
        public string Resolution { get; set; }
        public Dictionary<string, object> dicSubject { get; set; }
         [ResourceDisplayName("LockedMarkDetail_Label_CapacityDate")]
        public string CapacityDate { get; set; }
         [ResourceDisplayName("LockedMarkDetail_Label_ConductDate")]
        public string ConductDate { get; set; }
    }
}