/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.NutritionalNormArea.Models
{
    public class NutritionalNormViewModel
    {
        public System.Int32 NutritionalNormID { get; set; }
        public System.DateTime EffectDate { get; set; }
        public System.Int32 SchoolID { get; set; }
        public System.Int32 DailyDemandFrom { get; set; }
        public System.Int32 DailyDemandTo { get; set; }
        public System.Int32 RateFrom { get; set; }
        public System.Int32 RateTo { get; set; }
        public System.Int32 ProteinFrom { get; set; }
        public System.Int32 ProteinTo { get; set; }
        public System.Int32 AnimalProteinFrom { get; set; }
        public System.Int32 AnimalProteinTo { get; set; }
        public System.Int32 PlantProteinFrom { get; set; }
        public System.Int32 PlantProteinTo { get; set; }
        public System.Int32 FatFrom { get; set; }
        public System.Int32 FatTo { get; set; }
        public System.Int32 AnimalFatFrom { get; set; }
        public System.Int32 AnimalFatTo { get; set; }
        public System.Int32 PlantFatFrom { get; set; }
        public System.Int32 PlantFatTo { get; set; }
        public System.Int32 SugarFrom { get; set; }
        public System.Int32 SugarTo { get; set; }
        public System.Int32 EatingGroupID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.Boolean IsActive { get; set; }
        public System.Nullable<System.DateTime> ModifiedDate { get; set; }
        [ScaffoldColumn(false)]
        public bool? DeleteAble { get; set; }
    }

    public class ChildrenNutritionalNormViewModel
    {
        public int EatingGroupID { get; set; }
        public string EatingGroupName { get; set; }
        [ScaffoldColumn(false)]
        public bool? DeleteAble { get; set; }
    }

    public class TreeEatingGroupModel
    {
        public System.DateTime EffectDate { get; set; }
        public string TraversalPath { get; set; }
        public List<ChildrenNutritionalNormViewModel> ListChildren { get; set; }

        public TreeEatingGroupModel() { }

        public TreeEatingGroupModel(DateTime EffectDate, string TraversalPath, List<ChildrenNutritionalNormViewModel> ListChildren)
        {
            this.EffectDate = EffectDate;
            this.TraversalPath = TraversalPath;
            this.ListChildren = ListChildren;
        }
    }
}


