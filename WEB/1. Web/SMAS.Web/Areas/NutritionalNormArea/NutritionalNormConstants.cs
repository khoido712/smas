/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.NutritionalNormArea
{
    public class NutritionalNormConstants
    {
        public const string LIST_NUTRITIONALNORM = "listNutritionalNorm";
        public const string LIST_EATINGGROUP = "listEatinggroup";
        public const string NUTRITIONALNORM = "nutritionalnorm";
        public const string LIST_TREEEATINGGROUP = "listTreeEatinggroup";
        public const string LIST_MINENALCAT = "listMinenalCat";
        public const string DELETEABLE = "DELETEABLE";
    }
}