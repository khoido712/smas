﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.NutritionalNormArea
{
    public class NutritionalNormAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "NutritionalNormArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "NutritionalNormArea_default",
                "NutritionalNormArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
