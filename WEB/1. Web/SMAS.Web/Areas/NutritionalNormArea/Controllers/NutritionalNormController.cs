﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.NutritionalNormArea.Models;


namespace SMAS.Web.Areas.NutritionalNormArea.Controllers
{
    public class NutritionalNormController : BaseController
    {
        private readonly INutritionalNormBusiness NutritionalNormBusiness;
        private readonly IEatingGroupBusiness EatingGroupBusiness;
        private readonly IMinenalCatBusiness MinenalCatBusiness;
        private readonly IDailyMenuBusiness DailyMenuBusiness;
        private readonly INutritionalNormMineralBusiness NutritionalNormMineralBusiness;
        public NutritionalNormController(INutritionalNormBusiness nutritionalnormBusiness, IDailyMenuBusiness dailyMenuBusiness, IEatingGroupBusiness eatinggroupbusiness,
            IMinenalCatBusiness minenalcatbusiness, INutritionalNormMineralBusiness nutritionalnormmineralbusiness)
        {
            this.NutritionalNormBusiness = nutritionalnormBusiness;
            this.EatingGroupBusiness = eatinggroupbusiness;
            this.MinenalCatBusiness = minenalcatbusiness;
            this.NutritionalNormMineralBusiness = nutritionalnormmineralbusiness;
            this.DailyMenuBusiness = dailyMenuBusiness;
        }

        //
        // GET: /NutritionalNorm/

        public ActionResult Index()
        {
            var global = new GlobalInfo();
            var lsttree = CommonFunctions.TreeTreeEatingGroupModelByUser(NutritionalNormBusiness, EatingGroupBusiness);
            IDictionary<string, object> MinenalSearchInfo = new Dictionary<string, object>();
            var lstMineralCat = MinenalCatBusiness.Search(MinenalSearchInfo).Distinct().ToList();
            List<TreeEatingGroupModel> ListEatingGroupNode = new List<TreeEatingGroupModel>();
            if (lsttree != null)
            {
                ListEatingGroupNode = lsttree;
            }
            ViewData[NutritionalNormConstants.LIST_TREEEATINGGROUP] = ListEatingGroupNode;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            IEnumerable<NutritionalNormViewModel> lst = this._Search(SearchInfo);
            ViewData[NutritionalNormConstants.LIST_NUTRITIONALNORM] = lst;
            ViewData[NutritionalNormConstants.NUTRITIONALNORM] = null;


            IDictionary<string, object> EatGrpSearchInfo = new Dictionary<string, object>();
            EatGrpSearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
            var lstEatingGrp = EatingGroupBusiness.SearchBySchool(global.SchoolID.Value, EatGrpSearchInfo).Distinct().ToList();
            List<int?> listEg = new List<int?>();
            List<SelectListItem> SelectListItemEatingGrp = new List<SelectListItem>();
            if (lstEatingGrp != null && lstEatingGrp.Count > 0)
            {
                //sử dụng for() thay cho foreach() cải thiện tốc độ sử lý
                for (int i = 0,size = lstEatingGrp.Count; i < size; i++)
                {
                    var item = lstEatingGrp[i];
                    if (!listEg.Contains(item.EatingGroupID))
                    {
                        listEg.Add(item.EatingGroupID);
                        SelectListItemEatingGrp.Add(new SelectListItem { Text = item.EatingGroupName, Value = item.EatingGroupID.ToString(), Selected = false });
                    }
                }
                //foreach (var item in lstEatingGrp)
                //{
                //    if (!listEg.Contains(item.EatingGroupID))
                //    {
                //        listEg.Add(item.EatingGroupID);
                //        SelectListItemEatingGrp.Add(new SelectListItem { Text = item.EatingGroupName, Value = item.EatingGroupID.ToString(), Selected = false });
                //    }
                //}
            }
            ViewData[NutritionalNormConstants.LIST_EATINGGROUP] = SelectListItemEatingGrp;

            ViewData[NutritionalNormConstants.LIST_MINENALCAT] = lstMineralCat;
            return View();
        }

        //
        // GET: /NutritionalNorm/Search


        public PartialViewResult Search()
        {
            int? eatinggrpid = null;
            if (!string.IsNullOrWhiteSpace(Request["EatingGrpId"]))
                eatinggrpid = int.Parse(Request["EatingGrpId"]);
            DateTime? effectdate = null;
            if (!string.IsNullOrWhiteSpace(Request["EffectDate"]))
                effectdate = DateTime.Parse(Request["EffectDate"]);
            var global = new GlobalInfo();
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["EffectDate"] = effectdate;
            Dictionary["EatingGroupID"] = eatinggrpid;
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            IDictionary<string, object> MinenalSearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = global.AcademicYearID.Value;
            var lstNutri = NutritionalNormBusiness.SearchBySchool(global.SchoolID.Value, Dictionary).ToList();
            var lstEatingGrp = EatingGroupBusiness.SearchBySchool(global.SchoolID.Value, SearchInfo).Distinct().ToList();
            var lstMineralCat = MinenalCatBusiness.Search(MinenalSearchInfo).Distinct().ToList();
            List<int?> listEg = new List<int?>();
            List<SelectListItem> SelectListItemEatingGrp = new List<SelectListItem>();
            if (lstEatingGrp != null && lstEatingGrp.Count > 0)
            {
                for (int i = 0,size = lstEatingGrp.Count; i < size; i++)
                {
                    var item = lstEatingGrp[i];
                    if (!listEg.Contains(item.EatingGroupID))
                    {
                        listEg.Add(item.EatingGroupID);
                        if (effectdate.HasValue && eatinggrpid.HasValue)
                            SelectListItemEatingGrp.Add(new SelectListItem { Text = item.EatingGroupName, Value = item.EatingGroupID.ToString(), Selected = item.EatingGroupID == eatinggrpid.Value ? true : false });
                        else
                            SelectListItemEatingGrp.Add(new SelectListItem { Text = item.EatingGroupName, Value = item.EatingGroupID.ToString(), Selected = false });
                    }
                }
                //foreach (var item in lstEatingGrp)
                //{
                //    if (!listEg.Contains(item.EatingGroupID))
                //    {
                //        listEg.Add(item.EatingGroupID);
                //        if (effectdate.HasValue && eatinggrpid.HasValue)
                //            SelectListItemEatingGrp.Add(new SelectListItem { Text = item.EatingGroupName, Value = item.EatingGroupID.ToString(), Selected = item.EatingGroupID == eatinggrpid.Value ? true : false });
                //        else
                //            SelectListItemEatingGrp.Add(new SelectListItem { Text = item.EatingGroupName, Value = item.EatingGroupID.ToString(), Selected = false });
                //    }
                //}
            }
            NutritionalNorm NutritionalNorm = new NutritionalNorm();
            NutritionalNorm = (lstNutri != null && lstNutri.Count > 0) ? lstNutri.FirstOrDefault() : null;
            bool DeleteAble = true;
            if (NutritionalNorm != null)
            {
                IQueryable<DailyMenu> listDailyMenu = DailyMenuBusiness.SearchBySchool(Convert.ToInt32(_globalInfo.SchoolID), new Dictionary<string, object>() { { "IsActive", true } });
                var itemObjec = listDailyMenu.Where(a => a.EatingGroupID == NutritionalNorm.EatingGroupID).FirstOrDefault();
                if (itemObjec != null)
                {
                    DeleteAble = false;
                }
            }
            ViewData[NutritionalNormConstants.LIST_EATINGGROUP] = SelectListItemEatingGrp;
            ViewData[NutritionalNormConstants.LIST_MINENALCAT] = lstMineralCat;
            ViewData[NutritionalNormConstants.NUTRITIONALNORM] = NutritionalNorm;
            ViewData[NutritionalNormConstants.DELETEABLE] = DeleteAble;
            return PartialView("_Edit");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            NutritionalNorm nutritionalnorm = new NutritionalNorm();
            TryUpdateModel(nutritionalnorm);
            Utils.Utils.TrimObject(nutritionalnorm);

            this.NutritionalNormBusiness.Insert(nutritionalnorm);
            this.NutritionalNormBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Edit(FormCollection col)
        {
            var global = new GlobalInfo();
            IDictionary<string, object> MinenalSearchInfo = new Dictionary<string, object>();
            var lstMineralCat = MinenalCatBusiness.Search(MinenalSearchInfo).ToList();
            NutritionalNorm NutritionalNorm = new NutritionalNorm();
            var nutriID = int.Parse(col["OldNutritionalNormID"]);
            var isaddnew = col["IsAddNew"] == "0" ? false : true;
            if (nutriID > 0 && !isaddnew)
            {
                NutritionalNorm = NutritionalNormBusiness.All.Where(o => o.NutritionalNormID == nutriID).FirstOrDefault();
            }
            NutritionalNorm.EffectDate = DateTime.Parse(col["EffectDate"]);
            NutritionalNorm.EatingGroupID = int.Parse(col["EatingGroupID"]);
            NutritionalNorm.DailyDemandFrom = int.Parse(col["txtDailyDemandFrom"]);
            NutritionalNorm.DailyDemandTo = int.Parse(col["txtDailyDemandTo"]);
            NutritionalNorm.RateFrom = int.Parse(col["txtRateFrom"]);
            NutritionalNorm.RateTo = int.Parse(col["txtRateTo"]);
            NutritionalNorm.ProteinFrom = int.Parse(col["txtProteinFrom"]);
            NutritionalNorm.ProteinTo = int.Parse(col["txtProteinTo"]);
            NutritionalNorm.AnimalProteinFrom = int.Parse(col["txtAnimalProteinFrom"]);
            NutritionalNorm.AnimalProteinTo = int.Parse(col["txtAnimalProteinTo"]);
            NutritionalNorm.PlantProteinFrom = int.Parse(col["txtPlantProteinFrom"]);
            NutritionalNorm.PlantProteinTo = int.Parse(col["txtPlantProteinTo"]);
            NutritionalNorm.FatFrom = int.Parse(col["txtFatFrom"]);
            NutritionalNorm.FatTo = int.Parse(col["txtFatTo"]);
            NutritionalNorm.AnimalFatFrom = int.Parse(col["txtAnimalFatFrom"]);
            NutritionalNorm.AnimalFatTo = int.Parse(col["txtAnimalFatTo"]);
            NutritionalNorm.PlantFatFrom = int.Parse(col["txtPlantFatFrom"]);
            NutritionalNorm.PlantFatTo = int.Parse(col["txtPlantFatTo"]);
            NutritionalNorm.SugarFrom = int.Parse(col["txtSugarFrom"]);
            NutritionalNorm.SugarTo = int.Parse(col["txtSugarTo"]);
            NutritionalNorm.SchoolID = global.SchoolID.Value;
            List<NutritionalNormMineral> lstNutritionalNormMineral = new List<NutritionalNormMineral>();
            if (lstMineralCat != null && lstMineralCat.Count > 0)
            {
                var FromValueFromat = "MinenalCatFrom_{0}";
                var ToValueFromat = "MinenalCatTo_{0}";
                var ValueFrom = "";
                var ValueTo = "";
                for (int i = 0,size = lstMineralCat.Count; i < size; i++)
                {
                    var MineralCat = lstMineralCat[i];
                    ValueFrom = string.Format(FromValueFromat, MineralCat.MinenalCatID.ToString());
                    ValueTo = string.Format(ToValueFromat, MineralCat.MinenalCatID.ToString());
                    if (!string.IsNullOrWhiteSpace(col[ValueFrom]) && !string.IsNullOrWhiteSpace(col[ValueTo]))
                    {
                        NutritionalNormMineral NutritionalNormMineral = new NutritionalNormMineral();
                        NutritionalNormMineral.NutritionalNormID = 0;
                        NutritionalNormMineral.MinenalID = MineralCat.MinenalCatID;
                        NutritionalNormMineral.ValueFrom = decimal.Parse(col[ValueFrom]);
                        NutritionalNormMineral.ValueTo = decimal.Parse(col[ValueTo]);
                        lstNutritionalNormMineral.Add(NutritionalNormMineral);
                    }
                }
                //foreach (var MineralCat in lstMineralCat)
                //{
                //    ValueFrom = string.Format(FromValueFromat, MineralCat.MinenalCatID.ToString());
                //    ValueTo = string.Format(ToValueFromat, MineralCat.MinenalCatID.ToString());
                //    if (!string.IsNullOrWhiteSpace(col[ValueFrom]) && !string.IsNullOrWhiteSpace(col[ValueTo]))
                //    {
                //        NutritionalNormMineral NutritionalNormMineral = new NutritionalNormMineral();
                //        NutritionalNormMineral.NutritionalNormID = 0;
                //        NutritionalNormMineral.MinenalID = MineralCat.MinenalCatID;
                //        NutritionalNormMineral.ValueFrom = decimal.Parse(col[ValueFrom]);
                //        NutritionalNormMineral.ValueTo = decimal.Parse(col[ValueTo]);
                //        lstNutritionalNormMineral.Add(NutritionalNormMineral);
                //    }
                //}
            }
            if (nutriID > 0 && !isaddnew)
            {
                NutritionalNorm.NutritionalNormID = nutriID;
                NutritionalNormBusiness.Update(NutritionalNorm, lstNutritionalNormMineral);
                SetViewDataActionAudit("", "", "", "Edit");
                NutritionalNormBusiness.Save();
                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
            else if (isaddnew)
            {
                NutritionalNorm.IsActive = true;
                NutritionalNormBusiness.Insert(NutritionalNorm, lstNutritionalNormMineral);
                NutritionalNormBusiness.Save();
                SetViewDataActionAudit("", "", "", "Create");
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
            if (nutriID <= 0 && !isaddnew)
            {
                NutritionalNorm.IsActive = true;
                NutritionalNormBusiness.Insert(NutritionalNorm, lstNutritionalNormMineral);
                NutritionalNormBusiness.Save();
                SetViewDataActionAudit("", "", "", "Create");
                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete()
        {
            var id = int.Parse(Request["id"]);
            this.NutritionalNormBusiness.Delete(id, new GlobalInfo().SchoolID.Value);
            this.NutritionalNormBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<NutritionalNormViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<NutritionalNorm> query = this.NutritionalNormBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            if (query != null && query.Count() > 0)
            {
                IQueryable<NutritionalNormViewModel> lst = query.Select(o => new NutritionalNormViewModel
                {
                    NutritionalNormID = o.NutritionalNormID,
                    EffectDate = o.EffectDate,
                    SchoolID = o.SchoolID,
                    DailyDemandFrom = o.DailyDemandFrom,
                    DailyDemandTo = o.DailyDemandTo,
                    RateFrom = o.RateFrom,
                    RateTo = o.RateTo,
                    ProteinFrom = o.ProteinFrom,
                    ProteinTo = o.ProteinTo,
                    AnimalProteinFrom = o.AnimalProteinFrom,
                    AnimalProteinTo = o.AnimalProteinTo,
                    PlantProteinFrom = o.PlantProteinFrom,
                    PlantProteinTo = o.PlantProteinTo,
                    FatFrom = o.FatFrom,
                    FatTo = o.FatTo,
                    AnimalFatFrom = o.AnimalFatFrom,
                    AnimalFatTo = o.AnimalFatTo,
                    PlantFatFrom = o.PlantFatFrom,
                    PlantFatTo = o.PlantFatTo,
                    SugarFrom = o.SugarFrom,
                    SugarTo = o.SugarTo,
                    EatingGroupID = o.EatingGroupID,
                    CreatedDate = o.CreatedDate,
                    IsActive = o.IsActive,
                    ModifiedDate = o.ModifiedDate
                });
                

                IQueryable<DailyMenu> listDailyMenu = DailyMenuBusiness.SearchBySchool(Convert.ToInt32(_globalInfo.SchoolID), new Dictionary<string, object>() { { "IsActive", true } });
                List<NutritionalNormViewModel> list = new List<NutritionalNormViewModel>();
                foreach (NutritionalNormViewModel item in lst)
                {
                    var itemObjec = listDailyMenu.Where(a => a.EatingGroupID == item.EatingGroupID).FirstOrDefault();
                    if (itemObjec != null)
                    {
                        item.DeleteAble = false;
                    }
                    else
                    {
                        item.DeleteAble = true;
                    }
                    list.Add(item);
                }
                return list;
            }
            else return null;
        }

        public PartialViewResult ReLoadTreeView()
        {
            var global = new GlobalInfo();
            var lsttree = CommonFunctions.TreeTreeEatingGroupModelByUser(NutritionalNormBusiness, EatingGroupBusiness);
            List<TreeEatingGroupModel> ListEatingGroupNode = new List<TreeEatingGroupModel>();
            if (lsttree != null)
            {
                ListEatingGroupNode = lsttree;
            }
            ViewData[NutritionalNormConstants.LIST_TREEEATINGGROUP] = ListEatingGroupNode;
            return PartialView("_TreeEatingGroup");
        }
    }
}





