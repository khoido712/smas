﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Utils;
using SMAS.Business.IBusiness;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.MarkStatisticsByPeriodArea.Models;
using SMAS.Models.Models;
using SMAS.Web.Controllers;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Web.Areas.MarkStatisticsByPeriodArea.Controllers
{
    public class MarkStatisticsByPeriodController : BaseController
    {
        //
        // GET: /MarkStatisticsByPeriodArea/MarkStatisticsByPeriod/
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        public readonly ITrainingTypeBusiness TrainingTypeBusiness;
        public readonly ISubCommitteeBusiness SubCommitteeBusiness;
        public readonly IMarkStatisticBusiness MarkStatisticBusiness;
        public readonly IEducationLevelBusiness EducationLevelBusiness;
        public readonly IProcessedReportBusiness ProcessedReportBusiness;
        public MarkStatisticsByPeriodController(IProcessedReportBusiness ProcessedReportBusiness,IEducationLevelBusiness EducationLevelBusiness, IMarkStatisticBusiness MarkStatisticBusiness, ISubCommitteeBusiness SubCommitteeBusiness, IAcademicYearBusiness AcademicYearBusiness, IDistrictBusiness DistrictBusiness, ITrainingTypeBusiness TrainingTypeBusiness)
        { 
        this.DistrictBusiness = DistrictBusiness;
        this.AcademicYearBusiness = AcademicYearBusiness;
        this.SubCommitteeBusiness = SubCommitteeBusiness;
        this.MarkStatisticBusiness = MarkStatisticBusiness;
        this.TrainingTypeBusiness = TrainingTypeBusiness;
        this.EducationLevelBusiness = EducationLevelBusiness;
        this.ProcessedReportBusiness = ProcessedReportBusiness;
        }
        private GlobalInfo Global = new GlobalInfo();
        //private string ReportCode = SystemParamsInFile.REPORT_THONG_KE_DIEM_KIEM_TRA_DINH_KY_CAP_3;
        public ActionResult Index()
        {
            SetViewData();
            return View();
        }

        public void SetViewData()
        { 
            
            // Combobox Quan huyen
            ViewData[MarkStatisticsByPeriodConstants.CBO_DISTRICT] = new List<District>();
            int ProvinceID = Global.ProvinceID.Value;
            IQueryable<District> lsDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == ProvinceID);
            if (lsDistrict.Count() > 0)
            {
                ViewData[MarkStatisticsByPeriodConstants.CBO_DISTRICT] = lsDistrict.ToList();
            }

            //Combobox AcademicYear
            

            List<int> lsYear = AcademicYearBusiness.GetListYearForSupervisingDept(Global.SupervisingDeptID.Value);
            List<AcademicYearOB> lstAcademic = new List<AcademicYearOB>();
            foreach (int Year in lsYear)
            {
                AcademicYearOB AcademicYear = new AcademicYearOB();
                AcademicYear.AcademicYearID = Year;
                AcademicYear.Year = Year.ToString() + " - " + (Year + 1).ToString();
                lstAcademic.Add(AcademicYear);
            }

            ViewData[MarkStatisticsByPeriodConstants.CBO_ACADEMICYEAR] = lstAcademic;

            // Cbo Semester

            ViewData[MarkStatisticsByPeriodConstants.CBO_SEMESTER] = CommonList.SemesterAndAll();

            //Cbo EducationLevel

            ViewData[MarkStatisticsByPeriodConstants.CBO_EDUCATIONLEVEL] = EducationLevelBusiness.All.Where(o => o.Grade == 3); ;

            //-	cboTrainingType: Lấy danh sách từ hàm TrainingTypeBusiness.Search()
            //với các giá trị tìm kiếm theo mặc định. Giá trị mặc định là [Tất cả]
            ViewData[MarkStatisticsByPeriodConstants.CBO_TRAININGTYPE] = TrainingTypeBusiness.All.ToList();

            //-	cboSubCommittee: Lấy danh sách từ hàm SubCommitteeBusiness.Search()
            //với các giá trị tìm kiếm theo mặc định. Bổ sung thêm [Tất cả]

            ViewData[MarkStatisticsByPeriodConstants.CBO_SUBCOMMITTEE] = SubCommitteeBusiness.All.ToList();
            ViewData[MarkStatisticsByPeriodConstants.CBO_SUBJECT] = new List<SubjectCat>();
        }


        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int Year, int? EducationLevelID, int? SemesterID, int? TrainingTypeID, int? SubCommitteeID)
        {
            
            if (EducationLevelID == null)
            {
                EducationLevelID = 0;
            }
            if (SemesterID == null)
            {
                SemesterID = 0;
            }
            if (TrainingTypeID == null)
            {
                TrainingTypeID = 0;
            }
            
           
            if (SubCommitteeID==null)
            {
                return Json(new SelectList(new List<SubjectCat>(), "SubjectCatID", "SubjectName"), JsonRequestBehavior.AllowGet); 
            }

            
            GlobalInfo Global =new GlobalInfo();
            //-	cboSubject: Lấy danh sách từ hàm môn học đã gửi số liệu lên Phòng/Sở 
            //(từ bảng MarkStatistics) bằng các gọi hàm MarkStatisticsBusiness.SearchSubjectHasReport() với các tham số
            //o	Year = cboAcademicYear.Year, 
            //o	EducationLevelID = cboEducationLevel.Value, 
            //o	Semester = cboSemester.Value
            //o	TrainingTypeID = cboTrainingType.Value
            //o	ReportCode = “ThongKeDiemKiemTraDinhKyCap2” 
            //o	SentToSupervisor = 1
            //o	SubCommitteeID = cboSubcommittee.Value
            //o	ProvinceID = UserInfo.ProvinceID
            List<SubjectCat> lstSubjectCat = MarkStatisticBusiness.SearchSubjectHasReport("ThongKeDiemKiemTraDinhKyCap3",
                Year, SemesterID, true, EducationLevelID.Value, SubCommitteeID, TrainingTypeID.Value, Global.SupervisingDeptID, Global.ProvinceID);
            if (lstSubjectCat == null)
            {
                lstSubjectCat = new List<SubjectCat>();
            }

            ViewData[MarkStatisticsByPeriodConstants.CBO_SUBJECT] = lstSubjectCat;
            return Json(new SelectList(lstSubjectCat, "SubjectCatID", "SubjectName"), JsonRequestBehavior.AllowGet);
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult AjaxloadGrid(SearchViewModel frm)
        {           
            GlobalInfo global = new GlobalInfo();
            List<MarkStatisticsOfProvince23> lstMarkStatisticOfProvinceTertiary = new List<MarkStatisticsOfProvince23>();

            MarkReportBO MarkReport = new MarkReportBO();
            MarkReport.Year = frm.Year;
            MarkReport.EducationLevelID = (frm.EducationLevelID == null) ? 0 : frm.EducationLevelID;
            MarkReport.Semester = frm.SemesterID;
            MarkReport.TrainingTypeID = (frm.TrainingTypeID == null) ? 0 : frm.TrainingTypeID;
            MarkReport.SubcommitteeID = 0;
            MarkReport.SubjectID = frm.SubjectID;
            MarkReport.ProvinceID = global.ProvinceID;
            MarkReport.DistrictID = (frm.DistrictID == null) ? 0 : frm.DistrictID;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ReportCode"] = "ThongKeDiemKiemTraDinhKyCap3";
            dic["Year"] = MarkReport.Year;
            dic["EducationLevelID"] = MarkReport.EducationLevelID;
            dic["Semester"] = MarkReport.Semester;
            dic["TrainingTypeID"] = MarkReport.TrainingTypeID;
            dic["SentToSupervisor"] = true;
            dic["SubCommitteeID"] = 0;
            dic["ProvinceID"] = MarkReport.ProvinceID;
            dic["DistrictID"] = MarkReport.DistrictID;
            dic["SubjectID"] = MarkReport.SubjectID;
            dic["SupervisingDeptID"] = global.SupervisingDeptID;

            string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(MarkReport);
            int FileID = 0;
            lstMarkStatisticOfProvinceTertiary = this.MarkStatisticBusiness.CreateSGDPriodicMarkStatisticsTertiary(dic, InputParameterHashKey, out FileID);

            IEnumerable<MarkStatisticsByPeriodViewModel> lst = lstMarkStatisticOfProvinceTertiary.Select(o => new MarkStatisticsByPeriodViewModel
            {
                SchoolName = o.SchoolName,
                SchoolID = o.SchoolID,
                SentDate = o.SentDate,
                TotalSchool = o.TotalSchool,
                M00 = o.M00,
                M03 = o.M03,
                M05 = o.M05,
                M08 = o.M08,
                M10 = o.M10,
                M13 = o.M13,
                M15 = o.M15,
                M18 = o.M18,
                M20 = o.M20,
                M23 = o.M23,
                M25 = o.M25,
                M28 = o.M28,
                M30 = o.M30,
                M33 = o.M33,
                M35 = o.M35,
                M38 = o.M38,
                M40 = o.M40,
                M43 = o.M43,
                M45 = o.M45,
                M48 = o.M48,
                M50 = o.M50,
                M53 = o.M53,
                M55 = o.M55,
                M58 = o.M58,
                M60 = o.M60,
                M63 = o.M63,
                M65 = o.M65,
                M68 = o.M68,
                M70 = o.M70,
                M73 = o.M73,
                M75 = o.M75,
                M78 = o.M78,
                M80 = o.M80,
                M83 = o.M83,
                M85 = o.M85,
                M88 = o.M88,
                M90 = o.M90,
                M93 = o.M93,
                M95 = o.M95,
                M98 = o.M98,
                M100 = o.M100,
                TotalTest = o.TotalTest,
                TotalBelowAverage = o.TotalBelowAverage,
                PercentBelowAverage = o.PercentBelowAverage,
                TotalAboveAverage = o.TotalAboveAverage,
                PercentAboveAverage = o.PercentAboveAverage
            });
            ViewData[MarkStatisticsByPeriodConstants.LIST_MARKSTATISTIC] = lst.ToList();
            ViewData[MarkStatisticsByPeriodConstants.GRID_REPORT_ID] = FileID;  
            return PartialView("_List");
        }
        public ActionResult Search(SearchViewModel frm)
        {
            //string type = JsonReportMessage.NEW;
            //ProcessedReport processedReport = null;
            GlobalInfo global = new GlobalInfo();
            Utils.Utils.TrimObject(frm);
            //Nếu là account cấp Sở: hàm UserInfo.IsSupervisingDeptRole()trả về true            
            MarkReportBO MarkReport = new MarkReportBO();
            MarkReport.Year = frm.Year;
            MarkReport.EducationLevelID = (frm.EducationLevelID == null) ? 0 : frm.EducationLevelID;
            MarkReport.Semester = frm.SemesterID;
            MarkReport.TrainingTypeID = (frm.TrainingTypeID == null) ? 0 : frm.TrainingTypeID;
            MarkReport.SubcommitteeID = 0;
            MarkReport.SubjectID = frm.SubjectID;
            MarkReport.ProvinceID = global.ProvinceID;
            MarkReport.DistrictID = (frm.DistrictID == null) ? 0 : frm.DistrictID;


            string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(MarkReport);
            string ReportCode = "SGD_THPT_ThongKeDiemKiemTraDinhKy";
            ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
            if (entity == null)
            {
                return Json(new { ProcessedReportID = 0 });
            }
            else
            {
                int ProcessedReportID = entity.ProcessedReportID;
                return Json(new { ProcessedReportID = ProcessedReportID, ProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
            }
        }

        public FileResult DownloadReport(int ProcessedReportID)
        {

            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SupervisingDeptID", GlobalInfo.SupervisingDeptID}
            };
            List<string> listRC = new List<string> { "SGD_THPT_ThongKeDiemKiemTraDinhKy"};
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(ProcessedReportID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

    }
}
