﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MarkStatisticsByPeriodArea
{
    public class MarkStatisticsByPeriodAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MarkStatisticsByPeriodArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MarkStatisticsByPeriodArea_default",
                "MarkStatisticsByPeriodArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
