﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MarkStatisticsByPeriodArea
{
    public class MarkStatisticsByPeriodConstants
    {
        public const string CBO_DISTRICT = "CboDistrict";
        public const string CBO_ACADEMICYEAR = "CboAcademicYear";
        public const string CBO_EDUCATIONLEVEL = "CboEducationLevel";
        public const string CBO_TRAININGTYPE = "CboTrainingType";
        public const string CBO_SEMESTER = "CboSemester";
        public const string CBO_SUBJECT = "CboSubject";
        public const string CBO_SUBCOMMITTEE = "CboSubCommittee";
        public const string GRID_REPORT_ID = "GridReportID";
        public const string LIST_MARKSTATISTIC = "ListMarkStatistic";

    }
}