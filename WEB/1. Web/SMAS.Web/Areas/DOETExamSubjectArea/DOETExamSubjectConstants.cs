﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.DOETExamSubjectArea
{
    public class DOETExamSubjectConstants
    {
        public const string CBO_EXAM_YEAR = "CBO_EXAM_YEAR";
        public const string CBO_EXAMINATIONS = "CBO_EXAMINATIONS";
        public const string CBO_EXAM_GROUP = "CBO_EXAM_GROUP";
        public const string LIST_RESULT = "LIST_RESULT";
        public const string LIST_SUBJECT = "LIST_SUBJECT";
        public const string CREATE_SEARCH_MODEL = "CREATE_SEARCH_MODEL";
        public const string DIALOG_HIDDEN_EXAMINATIONS_ID = "DIALOG_HIDDEN_EXAMINATIONS_ID";
        public const string DIALOG_HIDDEN_EXAMGROUP_ID = "DIALOG_HIDDEN_EXAMGROUP_ID";
        public const string ENABLE_BUTTON_CREATE = "ENABLE_BUTTON_CREATE";
    }
}