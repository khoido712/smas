﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.DOETExamSubjectArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Filter;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamSubjectArea.Controllers
{
    public class DOETExamSubjectController:BaseController
    {
        #region Properties
        private readonly IDOETExaminationsBusiness DOETExaminationsBusiness;
        private readonly IDOETExamGroupBusiness DOETExamGroupBusiness;
        private readonly IDOETExamSubjectBusiness DOETExamSubjectBusiness;
        private readonly IDOETSubjectBusiness DOETSubjectBusiness;
        private readonly IDOETExamPupilBusiness DOETExamPupilBusiness;
        private readonly ISupervisingDeptBusiness SupervisingDeptBusiness;

        private int UnitID;

        private List<DOETExaminations> lstExaminations;
        private List<DOETExamGroup> lstExamGroup;
        private int? defaultExaminationsID;
        #endregion

        #region Constructor
        public DOETExamSubjectController(IDOETExaminationsBusiness DOETExaminationsBusiness, IDOETExamGroupBusiness DOETExamGroupBusiness,
            IDOETExamSubjectBusiness DOETExamSubjectBusiness, IDOETSubjectBusiness DOETSubjectBusiness, IDOETExamPupilBusiness DOETExamPupilBusiness,
            ISupervisingDeptBusiness SupervisingDeptBusiness)
        {
            this.DOETExaminationsBusiness = DOETExaminationsBusiness;
            this.DOETExamGroupBusiness = DOETExamGroupBusiness;
            this.DOETExamSubjectBusiness = DOETExamSubjectBusiness;
            this.DOETSubjectBusiness = DOETSubjectBusiness;
            this.DOETExamPupilBusiness = DOETExamPupilBusiness;
            this.SupervisingDeptBusiness = SupervisingDeptBusiness;
            this.UnitID = GetUnitID();
        }
        #endregion

        #region Actions
        //
        // GET: /ExamPupilArea/ExamPupil/

        public ActionResult Index()
        {
            
            SetViewData();

            //Search du lieu
            List<ListViewModel> lstModel = new List<ListViewModel>();
            if (defaultExaminationsID != null)
            {
                lstModel=_Search(defaultExaminationsID.Value,null);
            }
            ViewData[DOETExamSubjectConstants.LIST_RESULT] = lstModel;
            
            bool enable = false;
            DOETExaminations exam = DOETExaminationsBusiness.Find(defaultExaminationsID);
            if (exam != null && DateTime.Compare(exam.DeadLine, DateTime.Now.Date) >= 0)
            {
                enable = true;
            }
            ViewData[DOETExamSubjectConstants.ENABLE_BUTTON_CREATE] = enable;

            return View();
        }

        //
        // GET: /ExamGroup/Search
        
        public PartialViewResult Search(SearchViewModel model)
        {
            List<ListViewModel> lstModel = new List<ListViewModel>();
            if (model.ExaminationsID != null)
            {
                lstModel = _Search(model.ExaminationsID.Value, model.ExamGroupID).ToList();
            }

            bool enable = false;
            DOETExaminations exam = DOETExaminationsBusiness.Find(model.ExaminationsID);
            if (exam != null && DateTime.Compare(exam.DeadLine, DateTime.Now.Date) >= 0)
            {
                enable = true;
            }
            ViewData[DOETExamSubjectConstants.ENABLE_BUTTON_CREATE] = enable;

            return PartialView("_List", lstModel);
        }

        public PartialViewResult Create(int year, int? examinationsID, int? examGroupID)
        {
            SetViewDataForCreate(year,examinationsID);

            if (examGroupID == null && lstExamGroup.Count > 0)
            {
                examGroupID = lstExamGroup.First().ExamGroupID;
            }
            //Lay danh sach cac mon thi
            List<ListViewModel> lstModel = new List<ListViewModel>();

            if (examinationsID != null && examGroupID != null)
            {
                lstModel = _CreateSearch(examinationsID.Value, examGroupID.Value);
            }
            ViewData[DOETExamSubjectConstants.LIST_SUBJECT] = lstModel;

            CreateSearchViewModel model = new CreateSearchViewModel();
            model.CreateExaminationsID = examinationsID;
            model.CreateExamGroupID = examGroupID;
            ViewData[DOETExamSubjectConstants.CREATE_SEARCH_MODEL] = model;
            ViewData[DOETExamSubjectConstants.DIALOG_HIDDEN_EXAMINATIONS_ID] = examinationsID;
            ViewData[DOETExamSubjectConstants.DIALOG_HIDDEN_EXAMGROUP_ID] = examGroupID;

            return PartialView("_Create");
        }

        
        public PartialViewResult CreateSearch(CreateSearchViewModel model)
        {
            List<ListViewModel> lstModel = new List<ListViewModel>();
            if (model.CreateExaminationsID != null && model.CreateExamGroupID!=null)
            {
                lstModel = _CreateSearch(model.CreateExaminationsID.Value, model.CreateExamGroupID.Value).ToList();
            }
            ViewData[DOETExamSubjectConstants.DIALOG_HIDDEN_EXAMINATIONS_ID] = model.CreateExaminationsID;
            ViewData[DOETExamSubjectConstants.DIALOG_HIDDEN_EXAMGROUP_ID] = model.CreateExamGroupID;
            return PartialView("_CreateList", lstModel);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(List<ListViewModel> lstCreateViewModel, int? examinationsId, int? examGroupId)
        {
            if (examinationsId == null)
            {
                throw new BusinessException("DOETExamSubject_Validate_Examinations");
            }
            if (examGroupId == null)
            {
                throw new BusinessException("DOETExamSubject_Validate_ExamGroup");
            }

            if (lstCreateViewModel != null)
            {
                lstCreateViewModel = lstCreateViewModel.Where(o => o.isCheck == true).ToList();
            }
            else
            {
                lstCreateViewModel = new List<ListViewModel>();
            }
            if (lstCreateViewModel.Count == 0)
            {
                throw new BusinessException("DOETExamSubject_Validate_NoChoice");
            }

            for (int i = 0; i < lstCreateViewModel.Count; i++)
            {
                ListViewModel model=lstCreateViewModel[i];
                DOETExamSubject obj = new DOETExamSubject();
                obj.CreateTime = DateTime.Now;
                obj.DOETSubjectCode = model.SubjectCode;
                obj.DOETSubjectID = model.DOETSubjectID;
                obj.ExamGroupID = examGroupId.Value;
                obj.ExaminationsID = examinationsId.Value;
                obj.ExamSubjectID = DOETExamSubjectBusiness.GetNextSeq<int>("DOET_EXAM_SUBJECT_SEQ");
                obj.SchedulesExam = model.SchedulesExam;

                DOETExamSubjectBusiness.Insert(obj);
            }

            DOETExamSubjectBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));

        }

        public PartialViewResult Edit(int examSubjectID)
        {
            DOETExamSubject examSubject = DOETExamSubjectBusiness.Find(examSubjectID);
            DOETExaminations exam=DOETExaminationsBusiness.Find(examSubject.ExaminationsID);
            DOETExamGroup examGroup=DOETExamGroupBusiness.Find(examSubject.ExamGroupID);
            DOETSubject subject=DOETSubjectBusiness.Find(examSubject.DOETSubjectID);
            ListViewModel model = new ListViewModel();
            if (examSubject != null)
            {
                model.ExaminationsName = exam.ExaminationsName;
                model.ExamSubjectID = examSubject.ExamSubjectID;
                model.ExamGroupName = examGroup.ExamGroupName;
                model.SubjectName = subject.DOETSubjectName;
                model.SubjectCode = examSubject.DOETSubjectCode;
                model.SchedulesExam = examSubject.SchedulesExam;


            }

            return PartialView("_Edit", model);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(ListViewModel model)
        {
            if (ModelState.IsValid)
            {
                DOETExamSubject examSubject = DOETExamSubjectBusiness.Find(model.ExamSubjectID);
                examSubject.UpdateTime = DateTime.Now;
                examSubject.DOETSubjectCode = model.SubjectCode;
                examSubject.SchedulesExam = model.SchedulesExam;

                DOETExamSubjectBusiness.Update(examSubject);
                DOETExaminationsBusiness.Save();

                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
            string jsonErrList = Res.GetJsonErrorMessage(ModelState);
            return Json(new JsonMessage(jsonErrList, "error"));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(List<ListViewModel> lstDeleteViewModel)
        {
            List<DOETExamSubject> lstExamSubject = new List<DOETExamSubject>();
            List<int> lstExamSubjectID=new List<int>();

            if (lstDeleteViewModel.Count == 0)
            {
                throw new BusinessException("DOETExamSubject_Delete_NoChoice");
            }
            
            lstExamSubjectID = lstDeleteViewModel.Where(o => o.EnableToEditOrDelete == true && o.isCheck == true).Select(o => o.ExamSubjectID).ToList();
            
            lstExamSubject = DOETExamSubjectBusiness.All.Where(o => lstExamSubjectID.Contains(o.ExamSubjectID)).ToList();
            
            
            for (int i = 0; i < lstExamSubject.Count; i++)
            {
                DOETExamSubjectBusiness.Delete(lstExamSubject[i].ExamSubjectID);
            }

            DOETExamSubjectBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        public JsonResult AjaxLoadExaminations(int year)
        {
            List<DOETExaminations> lstExaminations = DOETExaminationsBusiness.All.Where(o => o.UnitID == UnitID && o.Year == year)
                .OrderByDescending(o => o.CreateTime).ToList();
           
            return Json(new SelectList(lstExaminations, "ExaminationsID", "ExaminationsName"));
        }

        public JsonResult AjaxLoadExamGroup(int? examinationsID)
        {
            List<DOETExamGroup> lstExamGroup = new List<DOETExamGroup>();
            if (examinationsID != null)
            {
                lstExamGroup = DOETExamGroupBusiness.All.Where(o => o.ExaminationsID == examinationsID).OrderBy(o => o.ExamGroupCode).ToList();
            }
            ViewData[DOETExamSubjectConstants.CBO_EXAM_GROUP] = new SelectList(lstExamGroup, "ExamGroupID", "ExamGroupName");

            return Json(new SelectList(lstExamGroup, "ExamGroupID", "ExamGroupName"));
        }
        #endregion

        

        #region Private methods
        
        /// <summary>
        /// Hàm khởi tạo dữ liệu cho vùng điều kiện search
        /// </summary>
        private void SetViewData()
        {
            var lstYear=new List<ComboObject>();
            //Lay nam hoc hien tai
            ComboObject curYear = GetCurYearObject();
            lstYear.Add(curYear);

            //Lay ra cac nam hoc co ky thi cua so
            List<int> lstExamYear = DOETExaminationsBusiness.GetExaminationsOfDept(UnitID).OrderByDescending(o => o.CreateTime).Select(o => o.Year).Distinct().ToList();
            for (int i = 0; i < lstExamYear.Count; i++)
            {
                int year = lstExamYear[i];
                if (year != Int32.Parse(curYear.key))
                {
                    lstYear.Add(GetYearObject(year));
                }
            }

            ViewData[DOETExamSubjectConstants.CBO_EXAM_YEAR] = new SelectList(lstYear, "key", "value");

            //Lay danh sach ky thi
            int tmpYear = Int32.Parse(curYear.key);
            lstExaminations = DOETExaminationsBusiness.All.Where(o => o.UnitID == UnitID && o.Year == tmpYear)
                                                        .OrderByDescending(o => o.CreateTime).ToList();
            ViewData[DOETExamSubjectConstants.CBO_EXAMINATIONS] = new SelectList(lstExaminations, "ExaminationsID", "ExaminationsName");
            
            //Lay danh sach nhom thi
            defaultExaminationsID = null;
            if (lstExaminations.Count > 0)
            {
                defaultExaminationsID = lstExaminations.First().ExaminationsID;
            }
            lstExamGroup = new List<DOETExamGroup>();
            if (defaultExaminationsID != null)
            {
                lstExamGroup = DOETExamGroupBusiness.All.Where(o => o.ExaminationsID == defaultExaminationsID).OrderBy(o=>o.ExamGroupCode).ToList();
            }
            ViewData[DOETExamSubjectConstants.CBO_EXAM_GROUP] = new SelectList(lstExamGroup, "ExamGroupID", "ExamGroupName");
        }

        private void SetViewDataForCreate(int year, int? examinationsID)
        {
            //Lay danh sach ky thi
            lstExaminations = DOETExaminationsBusiness.All.Where(o => o.UnitID == UnitID && o.Year == year)
                .OrderByDescending(o => o.CreateTime).ToList();
            ViewData[DOETExamSubjectConstants.CBO_EXAMINATIONS] = new SelectList(lstExaminations, "ExaminationsID", "ExaminationsName");

            //Lay danh sach nhom thi
            lstExamGroup = new List<DOETExamGroup>();
            if (examinationsID != null)
            {
                lstExamGroup = DOETExamGroupBusiness.All.Where(o => o.ExaminationsID == examinationsID).OrderBy(o => o.ExamGroupCode).ToList();
            }
            ViewData[DOETExamSubjectConstants.CBO_EXAM_GROUP] = new SelectList(lstExamGroup, "ExamGroupID", "ExamGroupName");
        }

        private List<ListViewModel> _Search(int ExaminationsID, int? ExamGroupID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ExaminationsID"] = ExaminationsID;
            dic["GroupID"] = ExamGroupID;
            DateTime nowDate=DateTime.Now.Date;
            List<ListViewModel> lstResult = (from des in DOETExamSubjectBusiness.Search(dic)
                                             join ds in DOETSubjectBusiness.All on des.DOETSubjectID equals ds.DOETSubjectID
                                             join de in DOETExaminationsBusiness.All on des.ExaminationsID equals de.ExaminationsID
                                             join deg in DOETExamGroupBusiness.All on des.ExamGroupID equals deg.ExamGroupID
                                             join dep in DOETExamPupilBusiness.All on new { deg.ExaminationsID, deg.ExamGroupID } equals new { dep.ExaminationsID, dep.ExamGroupID } into g
                                             from x in g.DefaultIfEmpty()
                                             select new ListViewModel
                                                 {
                                                     DOETSubjectID = des.DOETSubjectID,
                                                     ExamGroupName = deg.ExamGroupName,
                                                     ExamGroupCode=deg.ExamGroupCode,
                                                     ExamSubjectID = des.ExamSubjectID,
                                                     SchedulesExam = des.SchedulesExam,
                                                     SubjectCode = des.DOETSubjectCode,
                                                     SubjectName = ds.DOETSubjectName,
                                                     EnableToEditOrDelete = (x == null && DateTime.Compare(de.DeadLine, nowDate) >= 0) ? true : false
                                                 }).Distinct().ToList().OrderBy(o => o.ExamGroupCode).ThenBy(o => SMAS.Business.Common.Utils.SortABC(o.SubjectName)).ToList();


            return lstResult;
        }

        private List<ListViewModel> _CreateSearch(int examinationsID, int examGroupID)
        {
            List<ListViewModel> lstModel = new List<ListViewModel>();

            DOETExamGroup examGroup = DOETExamGroupBusiness.Find(examGroupID);
            string appliedLevel = examGroup.AppliedLevel.ToString();
            //Lay cac mon thi da co trong nhom
            List<int> lstRegistedSubjectID = _Search(examinationsID, examGroupID).Select(o => o.DOETSubjectID).ToList();

            IEnumerable<DOETSubject> lstSubject = DOETSubjectBusiness.All.Where(o => o.IsActive == true 
                && o.AppliedLevel.Contains(appliedLevel)
                && !lstRegistedSubjectID.Contains(o.DOETSubjectID)).ToList().OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.DOETSubjectName));

            lstModel = lstSubject.Select(o => new ListViewModel
            {
                DOETSubjectID = o.DOETSubjectID,
                SubjectCode = o.DOETSubjectCode,
                SubjectName = o.DOETSubjectName
            }).ToList();
            
            return lstModel;
        }
        private int GetCurYear()
        {
            int year;
            DateTime dateNow = DateTime.Now.Date;
            DateTime midDate = new DateTime(dateNow.Year, 7, 1);
            if (DateTime.Compare(dateNow, midDate) < 0)
            {
                year = dateNow.Year - 1;
            }
            else
            {
                year = dateNow.Year;
            }
            return year;
        }
        private ComboObject GetCurYearObject()
        {
            int year = GetCurYear();

            string strDisplay = year + " - " + (year + 1);
            return new ComboObject { key = year.ToString(), value = strDisplay };
        }

        private ComboObject GetYearObject(int year)
        {
            string strDisplay = year + " - " + (year + 1);
            return new ComboObject { key = year.ToString(), value = strDisplay };
        }

        //ham lay UnitID, neu la account thuoc phong thi lay ID phong, neu khong thi lay ID so
        private int GetUnitID()
        {
            int unitId = 0;
            if (_globalInfo.IsSubSuperVisingDeptRole)
            {
                int supervisingDeptID = _globalInfo.SupervisingDeptID.Value;
                SupervisingDept dept = SupervisingDeptBusiness.Find(supervisingDeptID);
                //Neu la accont phong ban thuoc phong
                if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    SupervisingDept subSupervisingDept = SupervisingDeptBusiness.Find(dept.ParentID);
                    unitId = subSupervisingDept.SupervisingDeptID;

                }
                //Neu la account phong
                else if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                {
                    unitId = supervisingDeptID;
                }
            }
            else if (_globalInfo.IsSuperVisingDeptRole)
            {
                int supervisingDeptID = _globalInfo.SupervisingDeptID.Value;
                SupervisingDept dept = SupervisingDeptBusiness.Find(supervisingDeptID);

                //Neu la phong ban thuoc so
                if (dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
                {
                    SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(dept.ParentID);
                    unitId = supervisingDept.SupervisingDeptID;
                }
                //Neu la account so
                else if (dept != null && dept.HierachyLevel == SMAS.Business.Common.SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
                {
                    unitId = supervisingDeptID;
                }
            }

            return unitId;
        }
        #endregion
    }
}