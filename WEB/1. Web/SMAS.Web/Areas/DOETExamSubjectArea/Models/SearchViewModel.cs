﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamSubjectArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("DOETExamSubject_Year")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamSubjectConstants.CBO_EXAM_YEAR)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onYearChange()")]
        public int Year { get; set; }

        [ResourceDisplayName("DOETExamSubject_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamSubjectConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onExaminationsChange()")]
        public int? ExaminationsID { get; set; }

        [ResourceDisplayName("DOETExamSubject_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamSubjectConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("OnChange", "onExamGroupChange()")]
        public int? ExamGroupID { get; set; }
    }
}