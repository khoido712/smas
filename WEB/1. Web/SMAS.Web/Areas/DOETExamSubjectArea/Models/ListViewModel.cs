﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;

namespace SMAS.Web.Areas.DOETExamSubjectArea.Models
{
    public class ListViewModel
    {

        public int ExamSubjectID { get; set; }
        [ResourceDisplayName("DOETExamSubject_ExamGroup")]
        public string ExamGroupName { get; set; }
        [ResourceDisplayName("DOETExamSubject_SubjectCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "DOETExamSubject_Required_SubjectCode")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SubjectCode { get; set; }
        [ResourceDisplayName("DOETExamSubject_Subject")]
        public string SubjectName { get; set; }
        [ResourceDisplayName("DOETExamSubject_Schedule")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public string SchedulesExam { get; set; }
        public bool EnableToEditOrDelete { get; set; }
        public int DOETSubjectID { get; set; }
        public bool isCheck { get; set; }
        [ResourceDisplayName("DOETExamSubject_Examinations")]
        public string ExaminationsName { get; set; }
        public string ExamGroupCode { get; set; }

    }
}