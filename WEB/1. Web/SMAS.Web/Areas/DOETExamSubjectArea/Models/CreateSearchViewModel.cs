﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamSubjectArea.Models
{
    public class CreateSearchViewModel
    {
        [ResourceDisplayName("DOETExamSubject_Examinations")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamSubjectConstants.CBO_EXAMINATIONS)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int? CreateExaminationsID { get; set; }

        [ResourceDisplayName("DOETExamSubject_ExamGroup")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", DOETExamSubjectConstants.CBO_EXAM_GROUP)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onCreateExamGroupChange()")]
        public int? CreateExamGroupID { get; set; }
    }
}