﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DOETExamSubjectArea
{
    public class DOETExamSubjectAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DOETExamSubjectArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DOETExamSubjectArea_default",
                "DOETExamSubjectArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
