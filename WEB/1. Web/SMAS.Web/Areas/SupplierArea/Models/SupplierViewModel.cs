﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.SupplierArea.Models
{
    public class SupplierViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 SupplierID { get; set; }
        
        [ResourceDisplayName("Supplier_Label_SupplierName")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
		public System.String SupplierName { get; set; }
        
        [ResourceDisplayName("Supplier_Label_Address")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]		
		public System.String Address { get; set; }
        
        [ResourceDisplayName("Supplier_Label_Phone")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        //[RegularExpression("^\\d{10,15}$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotPhoneNumber")]
       // [RegularExpression(@"^\+?([0-9]{8})?([0-9]{1})?([0-9]{1})?([0-9]{5})?$", ErrorMessage = "Trường {0} phải định dạng số")]
        //[RegularExpression("([0-9]+)", ErrorMessage = "Trường {0} phải định dạng số")]
		public System.String Phone { get; set; }
        
        [ResourceDisplayName("Supplier_Label_Shipper")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]		
		public System.String Shipper { get; set; }
        
        [ResourceDisplayName("Supplier_Label_ShipperPhone")]
        [StringLength(15, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        //[RegularExpression("^\\d{10,15}$", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_NotPhoneNumber")]
        //[RegularExpression(@"^\+?([0-9]{8})?([0-9]{1})?([0-9]{1})?([0-9]{5})?$", ErrorMessage = "Trường {0} phải định dạng số")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Trường {0} phải định dạng số")]
		public System.String ShipperPhone { get; set; }
        
        [ResourceDisplayName("Supplier_Label_SchoolID")]
        [ScaffoldColumn(false)]			
		public System.Int32 SchoolID { get; set; }

        [ResourceDisplayName("Supplier_Label_CreatedDate")]
        [ScaffoldColumn(false)]
		public System.DateTime CreatedDate { get; set; }
        
        [ResourceDisplayName("Supplier_Label_IsActive")]
        [ScaffoldColumn(false)]
		public System.Boolean IsActive { get; set; }

        [ResourceDisplayName("Supplier_Label_ModifiedDate")]
        [ScaffoldColumn(false)]
		public System.Nullable<System.DateTime> ModifiedDate { get; set; }								
	       
    }
}


