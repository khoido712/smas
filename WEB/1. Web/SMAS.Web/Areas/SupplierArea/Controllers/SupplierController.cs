﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.SupplierArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.SupplierArea.Controllers
{
    public class SupplierController : BaseController
    {        
        private readonly ISupplierBusiness SupplierBusiness;
		
		public SupplierController (ISupplierBusiness supplierBusiness)
		{
			this.SupplierBusiness = supplierBusiness;
		}
		
		//
        // GET: /Supplier/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here
            SearchInfo["IsActive"] = true;

            IEnumerable<SupplierViewModel> lst = this._Search(SearchInfo);
            ViewData[SupplierConstants.LIST_SUPPLIER] = lst;
            return View();
        }

		//
        // GET: /Supplier/Search

        [ValidateInput(false)]
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
            SearchInfo["IsActive"] = true;
            SearchInfo["SupplierName"] = frm.SupplierName;
			//

            IEnumerable<SupplierViewModel> lst = this._Search(SearchInfo);
            ViewData[SupplierConstants.LIST_SUPPLIER] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo global = new GlobalInfo();
            if (GetMenupermission("Supplier", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Supplier supplier = new Supplier();
            
            TryUpdateModel(supplier); 
            Utils.Utils.TrimObject(supplier);
            supplier.SchoolID = global.SchoolID.Value;
            this.SupplierBusiness.Insert(supplier);
            this.SupplierBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int SupplierID)
        {
            this.CheckPermissionForAction(SupplierID, "Supplier");
            GlobalInfo global = new GlobalInfo();
            if (GetMenupermission("Supplier", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Supplier supplier = this.SupplierBusiness.Find(SupplierID);
            supplier.ModifiedDate = DateTime.Now;
            TryUpdateModel(supplier);
            Utils.Utils.TrimObject(supplier);
            this.SupplierBusiness.Update(supplier);
            this.SupplierBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "Supplier");
            GlobalInfo global = new GlobalInfo();
            if (GetMenupermission("Supplier", global.UserAccountID, global.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            int? schoolID = global.SchoolID;
            this.SupplierBusiness.Delete(id,schoolID.Value);
            this.SupplierBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<SupplierViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            //GlobalInfo global = new GlobalInfo();
            //int? schoolID = global.SchoolID;
            SetViewDataPermission("Supplier", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            int? schoolID = _globalInfo.SchoolID;
            IQueryable<Supplier> query = this.SupplierBusiness.Search(SearchInfo).Where(a=>a.SchoolID==schoolID);
            //IQueryable<Supplier> query = this.SupplierBusiness.SearchBySchool(schoolID.Value, SearchInfo);
            IQueryable<SupplierViewModel> lst = query.Select(o => new SupplierViewModel {               
						SupplierID = o.SupplierID,								
						SupplierName = o.SupplierName,								
						Address = o.Address,								
						Phone = o.Phone,								
						Shipper = o.Shipper,								
						ShipperPhone = o.ShipperPhone,								
						SchoolID = o.SchoolID,								
						CreatedDate = o.CreatedDate,								
						IsActive = o.IsActive,								
						ModifiedDate = o.ModifiedDate								
					
				
            });
            List<SupplierViewModel> lstResult = lst.ToList();
            lstResult = lstResult.OrderBy(o => o.SupplierName).ToList();
            return lstResult;
        }        
    }
}





