﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ClassSupervisorAssignmentArea.Models;

using SMAS.Models.Models;
using System.Transactions;
using SMAS.Web.Filter;

namespace SMAS.Web.Areas.ClassSupervisorAssignmentArea.Controllers
{
    public class ClassSupervisorAssignmentController : BaseController
    {
        private readonly IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;

        public ClassSupervisorAssignmentController(IClassSupervisorAssignmentBusiness classsupervisorassignmentBusiness,
                                                    ISchoolFacultyBusiness SchoolFacultyBusiness,
                                                    IEmployeeBusiness EmployeeBusiness,
                                                    IClassProfileBusiness ClassProfileBusiness)
        {
            this.ClassSupervisorAssignmentBusiness = classsupervisorassignmentBusiness;
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
        }

        //
        // GET: /ClassSupervisorAssignment/

        private void SetViewData()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();

            // Danh sách tổ bộ môn
            List<SchoolFaculty> ListFaculty = new List<SchoolFaculty>();
            if (GlobalInfo.SchoolID != null)
            {
                ListFaculty = SchoolFacultyBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, new Dictionary<string, object>()).OrderBy(o => o.FacultyName).ToList();
            }
            ViewData[ClassSupervisorAssignmentConstants.LIST_FACULTY] = new SelectList(ListFaculty, "SchoolFacultyID", "FacultyName");

            // Danh sách giáo viên
            List<Employee> ListTeacher = new List<Employee>();
            ViewData[ClassSupervisorAssignmentConstants.LIST_TEACHER] = new SelectList(ListTeacher, "EmployeeID", "FullName");


            // Danh sách khối
            List<EducationLevel> ListEducationLevel = GlobalInfo.EducationLevels;
            if (ListEducationLevel == null)
            {
                ListEducationLevel = new List<EducationLevel>();
            }
            ViewData[ClassSupervisorAssignmentConstants.LIST_EDUCATIONLEVEL] = new SelectList(ListEducationLevel, "EducationLevelID", "Resolution");

            ViewData[ClassSupervisorAssignmentConstants.ENABLE_BUTTON_SAVE] = false;
        }

        public ActionResult Index()
        {
            SetViewData();

            return View();
        }

        //
        // GET: /ClassSupervisorAssignment/Search

        public PartialViewResult Search(SearchViewModel frm)
        {
            int HEAD_TEACHER = SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER;
            int SUBJECT_TEACHER = SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER;
            int OVERSEEING = SystemParamsInFile.SUPERVISING_PERMISSION_OVERSEEING;

            Utils.Utils.TrimObject(frm);

            GlobalInfo GlobalInfo = new GlobalInfo();

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            SearchInfo["TeacherID"] = frm.TeacherID;
            SearchInfo["EducationLevelID"] = frm.EducationLevelID;
            //

            List<ClassSupervisorAssignment> ListAssignment = this._Search(SearchInfo).ToList();
            Dictionary<string, object> DicAssigment = new Dictionary<string, object>();
            foreach (ClassSupervisorAssignment item in ListAssignment)
            {
                if (item.PermissionLevel == HEAD_TEACHER)
                {
                    DicAssigment[item.ClassID + "-" + HEAD_TEACHER] = true;
                }

                if (item.PermissionLevel == SUBJECT_TEACHER)
                {
                    DicAssigment[item.ClassID + "-" + SUBJECT_TEACHER] = true;
                }

                if (item.PermissionLevel == OVERSEEING)
                {
                    DicAssigment[item.ClassID + "-" + OVERSEEING] = true;
                }
            }


            SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = GlobalInfo.AcademicYearID;
            SearchInfo["EducationLevelID"] = frm.EducationLevelID;

            IEnumerable<ClassProfile> ListClass = this.ClassProfileBusiness.SearchBySchool(GlobalInfo.SchoolID.Value, SearchInfo).OrderBy(o => o.DisplayName).ToList();

            List<ClassSupervisorAssignmentViewModel> ListViewModel = new List<ClassSupervisorAssignmentViewModel>();
            foreach (ClassProfile Class in ListClass)
            {
                ClassSupervisorAssignmentViewModel item = new ClassSupervisorAssignmentViewModel();
                item.ClassID = Class.ClassProfileID;
                item.ClassName = Class.DisplayName;

                item.HeadTeacher = SMAS.Business.Common.Utils.GetBool(DicAssigment, item.ClassID + "-" + HEAD_TEACHER, false);

                item.SubjectTeacher = SMAS.Business.Common.Utils.GetBool(DicAssigment, item.ClassID + "-" + SUBJECT_TEACHER, false);

                item.Overseeing = SMAS.Business.Common.Utils.GetBool(DicAssigment, item.ClassID + "-" + OVERSEEING, false);

                ListViewModel.Add(item);
            }
           
            ViewData[ClassSupervisorAssignmentConstants.LIST_CLASSSUPERVISORASSIGNMENT] = ListViewModel;
            if (!frm.EducationLevelID.HasValue || !frm.TeacherID.HasValue || !frm.FacultyID.HasValue)
            {
                ViewData[ClassSupervisorAssignmentConstants.LIST_CLASSSUPERVISORASSIGNMENT] = null;
            }
            bool c = false;
            if (ListViewModel != null && ListViewModel.Count > 0)
            {
                c = true;
            }
            bool b = frm.EducationLevelID.HasValue && frm.FacultyID.HasValue && frm.TeacherID.HasValue;
			 bool d = GlobalInfo.IsAdminSchoolRole || GlobalInfo.IsEmployeeManager;
            ViewData[ClassSupervisorAssignmentConstants.ENABLE_BUTTON_SAVE] = GlobalInfo.IsCurrentYear && b && c && d;

            return PartialView("_List", frm);
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveAssignment(Dictionary<int, List<int>> ClassAndPermission, SearchViewModel frmSearch)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();

            this.ClassSupervisorAssignmentBusiness.UpdateForAllEducationLevel(
                GlobalInfo.SchoolID.Value,
                GlobalInfo.AppliedLevel.Value,
                GlobalInfo.AcademicYearID.Value,
                frmSearch.EducationLevelID.Value,
                frmSearch.TeacherID.Value,
                ClassAndPermission
            );
            this.ClassSupervisorAssignmentBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ClassSupervisorAssignment> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            IQueryable<ClassSupervisorAssignment> query = this.ClassSupervisorAssignmentBusiness
                                                              .SearchBySchool(GlobalInfo.SchoolID.Value, SearchInfo);

            return query.ToList();
        }

        /// <summary>
        /// Load dữ liệu cho commbobox Giáo viên khi chọn Tổ bộ môn
        /// </summary>
        /// <param name="FacultyID"></param>
        /// <returns></returns>
        [SkipCheckRole]

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadTeacher(int? FacultyID)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            IEnumerable<Employee> lst = new List<Employee>();
            if (FacultyID != null)
            {
                lst = this.EmployeeBusiness.SearchWorkingTeacherByFaculty(new GlobalInfo().SchoolID.Value, FacultyID.Value).OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            }
            if (lst == null)
                lst = new List<Employee>();

            //lst = lst.Where(o => o.AppliedLevel == globalInfo.AppliedLevel).ToList();
            return Json(new SelectList(lst, "EmployeeID", "FullName"));
        }
    }
}





