﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ClassSupervisorAssignmentArea
{
    public class ClassSupervisorAssignmentAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ClassSupervisorAssignmentArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ClassSupervisorAssignmentArea_default",
                "ClassSupervisorAssignmentArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
