/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.ClassSupervisorAssignmentArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("ClassSupervisorAssignment_Label_Faculty")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassSupervisorAssignmentConstants.LIST_FACULTY)]
        [AdditionalMetadata("OnChange", "AjaxLoadTeacher(this)")]
        public int? FacultyID { get; set; }

        [ResourceDisplayName("ClassSupervisorAssignment_Label_Teacher")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassSupervisorAssignmentConstants.LIST_TEACHER)]
        [AdditionalMetadata("OnChange", "search()")]
        public int? TeacherID { get; set; }

        [ResourceDisplayName("ClassSupervisorAssignment_Label_EducationLevel")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", ClassSupervisorAssignmentConstants.LIST_EDUCATIONLEVEL)]
        [AdditionalMetadata("OnChange", "search()")]
        public int? EducationLevelID { get; set; }
    }
}