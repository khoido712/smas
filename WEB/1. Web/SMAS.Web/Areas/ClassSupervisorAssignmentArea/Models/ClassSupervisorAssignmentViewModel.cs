/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.Models;
namespace SMAS.Web.Areas.ClassSupervisorAssignmentArea.Models
{
    public class ClassSupervisorAssignmentViewModel
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public bool HeadTeacher { get; set; }
        public bool SubjectTeacher { get; set; }
        public bool Overseeing { get; set; }
    }
}


