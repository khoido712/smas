/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.ClassSupervisorAssignmentArea
{
    public class ClassSupervisorAssignmentConstants
    {
        public const string LIST_CLASSSUPERVISORASSIGNMENT = "listClassSupervisorAssignment";
        public const string LIST_FACULTY = "ListFaculty";
        public const string LIST_TEACHER = "ListTeacher";
        public const string LIST_EDUCATIONLEVEL = "LISTEDUCATIONLEVEL";

        public const string ENABLE_BUTTON_SAVE = "ENABLE_BUTTON_SAVE";

    }
}