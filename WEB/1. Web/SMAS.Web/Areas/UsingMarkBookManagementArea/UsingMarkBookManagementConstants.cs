﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.UsingMarkBookManagementArea
{
    public class UsingMarkBookManagementConstants
    {
        public const string CBO_SEMESTER = "CBO_SEMESTER";
        public const string CBO_EDUCATION_LEVEL = "CBO_EDUCATION_LEVEL";
        public const string CBO_CLASS = "CBO_CLASS";
        public const string CBO_MONTH = "CBO_MONTH";
        public const string CBO_SUBJECT = "CBO_SUBJECT";
        public const string SELECTED_MONTH = "SELECTED_MONTH";
        public const string SELECTED_EDUCATION_LEVEL = "SELECTED_EDUCATION_LEVEL";
        public const string SELECTED_SEMESTER = "SELECTED_SEMESTER";
        public const string SELECTED_CLASS_ID = "SELECTED_CLASS_ID";
        public const string LIST_TEACHER = "LIST_TEACHER";
        public const string PREFIX_NAME_OF_SCHOOL = "GET_PREFIX_NAME_OF_SCHOOL";
        public const string IS_ALLOW_SEND_UNICODE_MSG = "allow send unicode message";
    }
}