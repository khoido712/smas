﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.UsingMarkBookManagementArea
{
    public class UsingMarkBookManagementAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "UsingMarkBookManagementArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "UsingMarkBookManagementArea_default",
                "UsingMarkBookManagementArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
