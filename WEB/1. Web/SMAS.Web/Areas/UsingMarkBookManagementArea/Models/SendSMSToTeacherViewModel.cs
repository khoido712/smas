﻿using SMAS.Business.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.UsingMarkBookManagementArea.Models
{
    public class SendSMSToTeacherViewModel
    {
        public int ContactGroupID { get; set; }

        public string GroupName { get; set; }

        public string GroupCode { get; set; }

        public bool isDefaultGroup { get; set; }

        public int FalcultyID { get; set; }

        public string FalcultyName { get; set; }

        public List<TeacherBO> TeacherBOList { get; set; }

        public int NumOfTeacher { get; set; }

        [AdditionalMetadata("OnChange", "CountSMS(this)")]
        public string SMSContent { get; set; }

        public bool AllowSignMessage { get; set; }

        public string SMSTemplate { get; set; }

        public int SMSCount { get; set; }

        public int RemainSMSCount { get; set; }

        public string BrandName { get; set; }

        /// <summary>
        /// so du
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// so tin noi mang khuyen mai
        /// </summary>
        public int NumOfPromotionInternalSMS { get; set; }

        /// <summary>
        /// Noi dung chuong trinh khuyen mai
        /// </summary>
        public string ContentPromotion { get; set; }

        public PromotionProgramBO Promotion { get; set; }
        public SendSMSToTeacherViewModel()
        {
            SMSTemplate = string.Empty;
            RemainSMSCount = 0;
            SMSCount = 0;
            ContactGroupID = 0;
            GroupName = string.Empty;
            GroupCode = FalcultyName = string.Empty;
            FalcultyID = 0;
            TeacherBOList = new List<TeacherBO>();
            SMSContent = string.Empty;
            Balance = 0;
            NumOfPromotionInternalSMS = 0;
            ContentPromotion = "";
        }
    }
}