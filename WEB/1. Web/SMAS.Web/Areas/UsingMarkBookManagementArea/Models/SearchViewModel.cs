﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.UsingMarkBookManagementArea;
namespace SMAS.Web.Areas.UsingMarkBookManagementArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Common_Label_Semester")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", UsingMarkBookManagementConstants.CBO_SEMESTER)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onSemesterChange()")]
        public int Semester { get; set; }

        [ResourceDisplayName("Common_Label_Education_Level")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", UsingMarkBookManagementConstants.CBO_EDUCATION_LEVEL)]
        [AdditionalMetadata("PlaceHolder", "All")]
        [AdditionalMetadata("OnChange", "onEducationLevelChange()")]
        public int? EducationLevel { get; set; }

        [ResourceDisplayName("Common_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", UsingMarkBookManagementConstants.CBO_CLASS)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public int? ClassID { get; set; }


        [ResourceDisplayName("Common_Label_Month")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", UsingMarkBookManagementConstants.CBO_MONTH)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public int Month { get; set; }
    }
}