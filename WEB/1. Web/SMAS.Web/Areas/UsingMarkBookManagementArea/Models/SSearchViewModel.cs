﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.UsingMarkBookManagementArea.Models
{
    public class SSearchViewModel
    {

        public int EducationLevel { get; set; }

        public int ClassID { get; set; }

        public int? SubjectID { get; set; }

        public int Semester { get; set; }
    }
}