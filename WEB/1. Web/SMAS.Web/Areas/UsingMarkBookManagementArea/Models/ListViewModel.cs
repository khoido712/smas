﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.UsingMarkBookManagementArea.Models
{
    public class ListViewModel
    {
        public int EducationLevelID { get; set; }

        public int ClassID { get; set; }

        [ResourceDisplayName("Common_Label_Class")]
        public string ClassName { get; set; }
        
        public decimal MarkNumPerMonth { get; set; }

        [ResourceDisplayName("UsingMarkBookManagement_MarkNumPerMonth")]
        public string StrMarkNumPerMonth {
            get
            {
                return MarkNumPerMonth.ToString("0.##").Replace(",", ".");
            }
        }

        public decimal MarkNumPerSemester { get; set; }

        [ResourceDisplayName("UsingMarkBookManagement_MarkNumPerSemester")]
        public string StrMarkNumPerSemester
        {
            get
            {
                return MarkNumPerSemester.ToString("0.##").Replace(",", ".");
            }
        }

        [ResourceDisplayName("UsingMarkBookManagement_ConductRankingRate")]
        public string ConductRankingRate { get; set; }

        [ResourceDisplayName("UsingMarkBookManagement_SummaryRate")]
        public string SummaryRate { get; set; }

        [ResourceDisplayName("UsingMarkBookManagement_PupilRankingRate")]
        public string PupilRankingRate { get; set; }

        public string Comment { get; set; }

        [ResourceDisplayName("UsingMarkBookManagement_UpdateHistory")]
        public string UpdateHistory { get; set; }

    }
}