﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.UsingMarkBookManagementArea.Models
{
    public class SListViewModel
    {
        public int EmployeeID { get; set; }

        public string EmployeeName { get; set; }

        public string FacultyName { get; set; }

        public string TeachingSubject { get; set; }

        public string Mobile { get; set; }

        public bool IsCheck { get; set; }
    }
}