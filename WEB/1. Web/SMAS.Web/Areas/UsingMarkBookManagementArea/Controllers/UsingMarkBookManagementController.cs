﻿using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Business.Common;
using SMAS.Web.Areas.UsingMarkBookManagementArea.Models;
using SMAS.Web.Utils;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common.Extension;

namespace SMAS.Web.Areas.UsingMarkBookManagementArea.Controllers
{
    [SkipCheckRole]
    public class UsingMarkBookManagementController:BaseController
    {
        #region Properties
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IVMarkRecordBusiness VMarkRecordBusiness;
        private readonly IVJudgeRecordBusiness VJudgeRecordBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IVPupilRankingBusiness VPupilRankingBusiness;
        private readonly IVSummedUpRecordBusiness VSummedUpRecordBusiness;
        private readonly IUsingMarkbookCommentBusiness UsingMarkbookCommentBusiness;
        private readonly IUserAccountBusiness UserAccountBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IPupilEmulationBusiness PupilEmulationBusiness;
        private readonly ISchoolConfigBusiness SchoolConfigBusiness;
        private readonly IEWalletBusiness EWalletBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IPromotionBusiness PromotionBusiness;
        private readonly IContactGroupBusiness ContactGroupBusiness;
        private readonly ISMSTypeBusiness SMSTypeBusiness;
        private readonly ISMSHistoryBusiness SMSHistoryBusiness;
        #endregion

        #region Contructor
        public UsingMarkBookManagementController(IClassProfileBusiness ClassProfileBusiness,
            IClassSubjectBusiness ClassSubjectBusiness,
            IVMarkRecordBusiness VMarkRecordBusiness,
            IVJudgeRecordBusiness VJudgeRecordBusiness,
            IAcademicYearBusiness AcademicYearBusiness,
            IPupilOfClassBusiness PupilOfClassBusiness,
            IVPupilRankingBusiness VPupilRankingBusiness,
            IVSummedUpRecordBusiness VSummedUpRecordBusiness,
            IUsingMarkbookCommentBusiness UsingMarkbookCommentBusiness,
            IUserAccountBusiness UserAccountBusiness,
            IEmployeeBusiness EmployeeBusiness,
            IPupilEmulationBusiness PupilEmulationBusiness,
            ISchoolConfigBusiness SchoolConfigBusiness,
            IEWalletBusiness EWalletBusiness,
            ISchoolProfileBusiness SchoolProfileBusiness,
            IPromotionBusiness PromotionBusiness,
            IContactGroupBusiness ContactGroupBusiness,
            ISMSTypeBusiness SMSTypeBusiness,
            ISMSHistoryBusiness SMSHistoryBusiness)
        {
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.VMarkRecordBusiness = VMarkRecordBusiness;
            this.VJudgeRecordBusiness = VJudgeRecordBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
            this.PupilOfClassBusiness = PupilOfClassBusiness;
            this.VPupilRankingBusiness = VPupilRankingBusiness;
            this.VSummedUpRecordBusiness = VSummedUpRecordBusiness;
            this.UsingMarkbookCommentBusiness = UsingMarkbookCommentBusiness;
            this.UserAccountBusiness = UserAccountBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.PupilEmulationBusiness = PupilEmulationBusiness;
            this.SchoolConfigBusiness = SchoolConfigBusiness;
            this.EWalletBusiness = EWalletBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.PromotionBusiness = PromotionBusiness;
            this.ContactGroupBusiness = ContactGroupBusiness;
            this.SMSTypeBusiness = SMSTypeBusiness;
            this.SMSHistoryBusiness = SMSHistoryBusiness;

        }
        #endregion

        #region Action methods
        public ActionResult Index()
        {
            SetViewData();

            return View("Index");
        }

        public PartialViewResult Search(SearchViewModel model)
        {
            List<ListViewModel> lstModel = new List<ListViewModel>();

            //Lay danh sach lop
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = model.EducationLevel;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["ClassProfileID"] = model.ClassID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsRolePrincipal)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }

            List<ClassProfile> listClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            
            //Lay danh sach mon hoc cua cac lop
            dic = new Dictionary<string, object>();
            dic["ClassID"] = model.ClassID;
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["EducationLevelID"] = model.EducationLevel;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = model.Semester;

            List<ClassSubject> lstCsAll = this.ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            

            
            //Lay danh sach con diem da nhap
            dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dic["Semester"] = model.Semester;
            dic["ClassID"] = model.ClassID;
            dic["EducationLevelID"] = model.EducationLevel;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;

            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            dic["Year"] = aca.Year;

            List<VMarkRecord> lstMarkRecord = this.VMarkRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            List<VJudgeRecord> lstJudgeRecord = this.VJudgeRecordBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            //Danh sach xep loai, hanh kiem
            dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ClassID"] = model.ClassID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["EducationLevelID"] = model.EducationLevel;
            dic["Semester"] = model.Semester;

            List<VPupilRanking> lstPupilRankingAll = this.VPupilRankingBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            //Danh sach xep lloai
            dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["ClassID"] = model.ClassID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["EducationLevelID"] = model.EducationLevel;
            dic["Semester"] = model.Semester;
            List<PupilEmulation> lstEmulationAll = this.PupilEmulationBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();

            //Danh sach hoc sinh
            IQueryable<PupilOfClass> listPupilAll = PupilOfClassBusiness.SearchBySchool(_globalInfo.SchoolID.Value, new Dictionary<string, object>
            {
                {"AcademicYearID", _globalInfo.AcademicYearID.Value},
                {"EducationLevelID", model.EducationLevel},
                {"ClassID", model.ClassID},
                {"CheckWithClass", "CheckWithClass"}
            }).AddCriteriaSemester(aca, model.Semester);

            //DAnh sach nhan xet
            var lstComment = (from c in UsingMarkbookCommentBusiness.All.Where(o => o.AcademicYearID == _globalInfo.AcademicYearID && o.Month == model.Month)
                              join u1 in UserAccountBusiness.All on c.CreateUserID equals u1.UserAccountID into g1
                              from x in g1.DefaultIfEmpty()
                              join u2 in UserAccountBusiness.All on c.UpdateUserID equals u2.UserAccountID into g2
                              from y in g2.DefaultIfEmpty()
                              select new
                              {
                                  ClassID = c.ClassID,
                                  ReviewComment = c.ReviewComment,
                                  CreateTime = c.CreateTime,
                                  CreateUserID = c.CreateUserID,
                                  CreateEmployeeID = x.EmployeeID,
                                  IsCreateUserAdmin = x.IsAdmin,
                                  UpdateTime = c.UpdateTime,
                                  UpdateUserID = c.UpdateUserID,
                                  IsUpdateUserAdmin = y != null ? y.IsAdmin : false,
                                  UpdateEmployeeID = y != null ? y.EmployeeID : null
                              });

            //Danh sach giao vien
            dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            List<Employee> lstEmployee = EmployeeBusiness.Search(dic).ToList();
            for (int i = 0; i < listClass.Count; i++)
            {
                ClassProfile cp = listClass[i];
                ListViewModel vm = new ListViewModel();

                //Danh sach mon hoc cua lop
                List<int> lstSubjectID = lstCsAll.Where(o => o.ClassID == cp.ClassProfileID).Select(o=>o.SubjectID).ToList();
                List<int> lstSubjectIDComment = lstCsAll.Where(o => o.ClassID == cp.ClassProfileID && o.IsCommenting.HasValue && o.IsCommenting.Value ==1).Select(o => o.SubjectID).ToList();
                List<int> lstSubjectIDNotComment = lstCsAll.Where(o => o.ClassID == cp.ClassProfileID && o.IsCommenting == null || o.IsCommenting == 0).Select(o => o.SubjectID).ToList();

                //danh sach hoc sinh cua lop
                List<PupilOfClass> listPupilClass = listPupilAll.Where(o=>o.ClassID == cp.ClassProfileID).ToList();
                List<int> lstPupilID = listPupilClass.Select(o=>o.PupilID).ToList();


                vm.ClassID = cp.ClassProfileID;
                vm.EducationLevelID = cp.EducationLevelID;
                vm.ClassName = cp.DisplayName;

                #region Lay so con diem TB thang
                int year = _globalInfo.Semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST ? aca.Year : aca.Year + 1;
                DateTime fromDate = new DateTime(year, model.Month, 1);
                DateTime toDate = new DateTime(year, model.Month, DateTime.DaysInMonth(year, model.Month));
                

                //Lay so con diem da nhap
                int markNotCommentCountMonth = lstMarkRecord.Where(o => o.CreatedDate.Value >= fromDate && o.CreatedDate.Value <= toDate
                    && o.ClassID == cp.ClassProfileID && lstPupilID.Contains(o.PupilID)
                    && lstSubjectIDNotComment.Contains(o.SubjectID)).Count();
                int markCommentCountMonth = lstJudgeRecord.Where(o => o.CreatedDate.Value >= fromDate && o.CreatedDate.Value <= toDate
                    && o.ClassID == cp.ClassProfileID && lstPupilID.Contains(o.PupilID)
                    && lstSubjectIDComment.Contains(o.SubjectID)).Count();

                int markNumMonth = markNotCommentCountMonth + markCommentCountMonth;

                vm.MarkNumPerMonth = lstSubjectID.Count > 0 && lstPupilID.Count > 0 ? (((decimal)markNumMonth / (decimal)lstSubjectID.Count) / (decimal)lstPupilID.Count) : 0;

               
                #endregion

                #region Lay so con diem TB hoc ky
                //Lay so con diem da nhap
                int markNotCommentCountSemester = lstMarkRecord.Where(o => o.ClassID == cp.ClassProfileID && lstPupilID.Contains(o.PupilID)
                    && lstSubjectIDNotComment.Contains(o.SubjectID)).Count();
                int markCommentCountSemester = lstJudgeRecord.Where(o => o.ClassID == cp.ClassProfileID && lstPupilID.Contains(o.PupilID)
                    && lstSubjectIDComment.Contains(o.SubjectID)).Count();

                int markNumSemester = markNotCommentCountSemester + markCommentCountSemester;

                vm.MarkNumPerSemester = lstSubjectID.Count > 0 && lstPupilID.Count > 0 ? ((decimal)markNumSemester / (decimal)lstSubjectID.Count) / (decimal)lstPupilID.Count : 0;
                #endregion

                #region Xep loai hanh kiem
                int conductNum = lstPupilRankingAll.Where(o => o.ClassID == cp.ClassProfileID
                    && lstPupilID.Contains(o.PupilID)
                    && o.ConductLevelID.HasValue).Count();
                vm.ConductRankingRate = string.Format("{0}/{1}", conductNum, lstPupilID.Count);
                #endregion

                #region Tong ket diem
                int summaryNum = lstPupilRankingAll.Where(o => o.ClassID == cp.ClassProfileID
                    && lstPupilID.Contains(o.PupilID)
                    && o.AverageMark.HasValue).Count();
                vm.SummaryRate = string.Format("{0}/{1}", summaryNum, lstPupilID.Count);
                #endregion

                #region Xep loai hoc sinh
                int rankingNum = lstPupilRankingAll.Where(o => o.ClassID == cp.ClassProfileID
                    && lstPupilID.Contains(o.PupilID)
                    && o.IsCategory.HasValue && o.IsCategory.Value).Count();
                vm.PupilRankingRate = string.Format("{0}/{1}", rankingNum, lstPupilID.Count);
                #endregion

                var comment = lstComment.FirstOrDefault(o => o.ClassID == cp.ClassProfileID);
                if (comment != null)
                {
                    vm.Comment = comment.ReviewComment;
                    string createUserName = string.Empty;
                    if (comment.IsCreateUserAdmin)
                    {
                        createUserName = "Quản trị trường";
                    }
                    else
                    {
                        Employee e = lstEmployee.FirstOrDefault(o => o.EmployeeID == comment.CreateEmployeeID);
                        if (e != null)
                        {
                            createUserName = e.FullName;
                        }
                    }
                    vm.UpdateHistory = string.Format("Nhận xét bởi {0} lúc {1}. ", createUserName, comment.CreateTime.ToString("HH'h'mm dd/MM/yyyy"));

                    if (comment.UpdateUserID.HasValue)
                    {
                        string updateUserName = string.Empty;
                        if (comment.IsUpdateUserAdmin)
                        {
                            updateUserName = "Quản trị trường";
                        }
                        else
                        {
                            Employee e = lstEmployee.FirstOrDefault(o => o.EmployeeID == comment.UpdateEmployeeID);
                            if (e != null)
                            {
                                updateUserName = e.FullName;
                            }
                        }

                        vm.UpdateHistory = vm.UpdateHistory + string.Format("Cập nhật bởi {0} lúc {1}", updateUserName, comment.UpdateTime.Value.ToString("HH'h'mm dd/MM/yyyy"));
                    }
                }

                lstModel.Add(vm);
            }

            ViewData[UsingMarkBookManagementConstants.SELECTED_MONTH] = model.Month;
            ViewData[UsingMarkBookManagementConstants.SELECTED_EDUCATION_LEVEL] = model.EducationLevel;
            ViewData[UsingMarkBookManagementConstants.SELECTED_SEMESTER] = model.Semester;

            return PartialView("_List", lstModel);
        }

        public PartialViewResult SSearch(SSearchViewModel model)
        {
            List<SListViewModel> lstModel = this.SearchTeacher(model.EducationLevel, model.ClassID, model.SubjectID, model.Semester);

            return PartialView("_SList", lstModel);
        }

        public JsonResult Save(List<ListViewModel> listModel, int month)
        {
            if (listModel != null)
            {
                List<UsingMarkbookComment> lstCommentFromDb = UsingMarkbookCommentBusiness.All.Where(o => o.AcademicYearID == _globalInfo.AcademicYearID).ToList();
                for (int i = 0; i < listModel.Count; i++)
                {
                    ListViewModel model = listModel[i];
                    if (!string.IsNullOrEmpty(model.Comment))
                    {
                        model.Comment = model.Comment.Trim();
                    }
                    UsingMarkbookComment obj = lstCommentFromDb.FirstOrDefault(o => o.ClassID == model.ClassID && o.Month == month);
                    if (obj != null)
                    {
                        if (!string.Equals(obj.ReviewComment, model.Comment))
                        {
                            obj.ReviewComment = model.Comment;
                            obj.UpdateTime = DateTime.Now;
                            obj.UpdateUserID = _globalInfo.UserAccountID;

                            UsingMarkbookCommentBusiness.Update(obj);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(model.Comment))
                        {
                            obj = new UsingMarkbookComment();
                            obj.AcademicYearID = _globalInfo.AcademicYearID.Value;
                            obj.ClassID = model.ClassID;
                            obj.CreateTime = DateTime.Now;
                            obj.CreateUserID = _globalInfo.UserAccountID;
                            obj.Month = month;
                            obj.ReviewComment = model.Comment;

                            UsingMarkbookCommentBusiness.Insert(obj);
                        }
                    }
                }

                UsingMarkbookCommentBusiness.Save();
            }

            return Json(new JsonMessage("Lưu nhận xét thành công"));
        }

        public JsonResult AjaxLoadClass(int? EducationLevelID)
        {
            List<ClassProfile> listClass = new List<ClassProfile>();

            if (EducationLevelID.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["EducationLevelID"] = EducationLevelID;
                dic["AcademicYearID"] = _globalInfo.AcademicYearID;
                if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsRolePrincipal)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }

                listClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            }

            return Json(new SelectList(listClass, "ClassProfileID", "DisplayName"));
        }

        public JsonResult AjaxLoadSubject(int EducationLevelID, int ClassID, int Semester)
        {
            IDictionary<string,object> dic = new Dictionary<string, object>();
            dic["ClassID"] = ClassID;
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["EducationLevelID"] = EducationLevelID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = Semester;

            List<SubjectCatBO> lstSubject = this.ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Select(o => new SubjectCatBO
            {
                SubjectCatID = o.SubjectID,
                SubjectName = o.SubjectCat.DisplayName,
                OrderInSubject = o.SubjectCat.OrderInSubject
            }).OrderBy(p=>p.OrderInSubject).ThenBy(p=>p.SubjectName).ToList();
            
            return Json(new SelectList(lstSubject, "SubjectCatID", "SubjectName"));
        }

        public JsonResult AjaxLoadMonth(int Semester)
        {
            List<KeyValuePair<int, string>> lstMonth = new List<KeyValuePair<int, string>>();
            if (Semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                lstMonth = new List<KeyValuePair<int, string>>
                {
                    new KeyValuePair<int, string>(8, "Tháng 8"),
                    new KeyValuePair<int, string>(9, "Tháng 9"),
                    new KeyValuePair<int, string>(10, "Tháng 10"),
                    new KeyValuePair<int, string>(11, "Tháng 11"),
                    new KeyValuePair<int, string>(12, "Tháng 12"),
                };
            }
            else if (Semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                lstMonth = new List<KeyValuePair<int, string>>
                {
                    new KeyValuePair<int, string>(1, "Tháng 1"),
                    new KeyValuePair<int, string>(2, "Tháng 2"),
                    new KeyValuePair<int, string>(3, "Tháng 3"),
                    new KeyValuePair<int, string>(4, "Tháng 4"),
                    new KeyValuePair<int, string>(5, "Tháng 5"),
                };
            }

            return Json(new SelectList(lstMonth, "key", "value"));
        }

        public PartialViewResult OpenSendMessageDialog(int educationLevel, int classId, int semester)
        {
            //Khoi
            ViewData[UsingMarkBookManagementConstants.CBO_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution", educationLevel);

            //Lop
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = educationLevel;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsViewAll && !_globalInfo.IsRolePrincipal)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }

            List<ClassProfile> listClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            ViewData[UsingMarkBookManagementConstants.CBO_CLASS] = new SelectList(listClass, "ClassProfileID", "DisplayName", classId);

            //Mon day
            dic = new Dictionary<string, object>();
            dic["ClassID"] = classId;
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["EducationLevelID"] = educationLevel;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = semester;

            List<SubjectCatBO> lstSubject = this.ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Select(o => new SubjectCatBO
            {
                SubjectCatID = o.SubjectID,
                SubjectName = o.SubjectCat.DisplayName,
                OrderInSubject = o.SubjectCat.OrderInSubject
            }).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();
            ViewData[UsingMarkBookManagementConstants.CBO_SUBJECT] = new SelectList(lstSubject, "SubjectCatID", "SubjectName", classId);
           
            //Danh sach tim kiem
            ViewData[UsingMarkBookManagementConstants.LIST_TEACHER] = this.SearchTeacher(educationLevel, classId, null, semester);
            ViewData[UsingMarkBookManagementConstants.SELECTED_SEMESTER] = semester;
            ViewData[UsingMarkBookManagementConstants.SELECTED_EDUCATION_LEVEL] = educationLevel;
            ViewData[UsingMarkBookManagementConstants.SELECTED_CLASS_ID] = classId;
            ViewData["SMS_SCHOOL_URL"] = System.Configuration.ConfigurationManager.AppSettings["smsEduUrl"];

            return PartialView("_SendMessage");
        }

        
        #endregion

        #region SendMessage
        public PartialViewResult LoadSendMessage(int educationLevel, int classId, int semester)
        {
            //Khoi
            ViewData[UsingMarkBookManagementConstants.CBO_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution", educationLevel);

            //Lop
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = educationLevel;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsViewAll && !_globalInfo.IsRolePrincipal)
            {
                dic["UserAccountID"] = _globalInfo.UserAccountID;
                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
            }

            List<ClassProfile> listClass = this.ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            ViewData[UsingMarkBookManagementConstants.CBO_CLASS] = new SelectList(listClass, "ClassProfileID", "DisplayName", classId);

            //Mon day
            dic = new Dictionary<string, object>();
            dic["ClassID"] = classId;
            dic["SchoolID"] = _globalInfo.SchoolID.Value;
            dic["EducationLevelID"] = educationLevel;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["Semester"] = semester;

            List<SubjectCatBO> lstSubject = this.ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).Select(o => new SubjectCatBO
            {
                SubjectCatID = o.SubjectID,
                SubjectName = o.SubjectCat.DisplayName,
                OrderInSubject = o.SubjectCat.OrderInSubject
            }).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();
            ViewData[UsingMarkBookManagementConstants.CBO_SUBJECT] = new SelectList(lstSubject, "SubjectCatID", "SubjectName");

            //Danh sach tim kiem
            ViewData[UsingMarkBookManagementConstants.LIST_TEACHER] = this.SearchTeacher(educationLevel, classId, null, semester);
            ViewData[UsingMarkBookManagementConstants.SELECTED_SEMESTER] = semester;

            SMS_SCHOOL_CONFIG cfg = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            if (cfg != null) ViewData[UsingMarkBookManagementConstants.IS_ALLOW_SEND_UNICODE_MSG] = cfg.IS_ALLOW_SEND_UNICODE;

            ViewData[UsingMarkBookManagementConstants.PREFIX_NAME_OF_SCHOOL] = CommonMethods.GetPrefix(cfg != null ? cfg.PRE_FIX : null);

            SendSMSToTeacherViewModel model = new SendSMSToTeacherViewModel();
            //Noi dung tin nhan
            model.SMSContent = CommonMethods.GetPrefix(cfg != null ? cfg.PRE_FIX : null);
            model.BrandName = CommonMethods.GetBrandName();

            // balance
            var ewallet = EWalletBusiness.GetEWalletIncludePromotion(_globalInfo.SchoolID.Value, true, false);
            if (ewallet != null)
            {
                model.Balance = ewallet.Balance;
                //model.NumOfPromotionInternalSMS = ewallet.InternalSMS;
            }

            //Lay chuong trinh khuyen mai
            int provinceId = _globalInfo.ProvinceID.Value;
            if (provinceId == 0)
            {
                SchoolProfile school = SchoolProfileBusiness.Find(_globalInfo.SchoolID);
                provinceId = (school != null) ? school.ProvinceID.Value : 0;
            }

            PromotionProgramBO objPromotion = PromotionBusiness.GetPromotionProgramByProvinceId(provinceId, _globalInfo.SchoolID.Value);
            if (objPromotion != null)
            {
                model.ContentPromotion = !String.IsNullOrEmpty(objPromotion.Note) ? objPromotion.Note : objPromotion.PromotionName;
                model.Promotion = objPromotion;
            }

            model.SMSTemplate = (string.Format("{0}:{1}", _globalInfo.SchoolName, string.Empty));
            return PartialView("_SendMessage",model);
        }
        [HttpPost]
        [ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public JsonResult SendSMS(SendSMSToTeacherViewModel model, bool RemoveSpecialChar = false, string LstTeacherReceiverID = "", bool CheckAllReceiver = false)
        {
            //Lay ra group toan truong
            SMS_TEACHER_CONTACT_GROUP dcg = ContactGroupBusiness.All.Where(o => o.SCHOOL_ID == _globalInfo.SchoolID
                && o.IS_LOCK == false
                && o.IS_DEFAULT == true).FirstOrDefault();

            model.ContactGroupID = dcg.CONTACT_GROUP_ID;

            var fullMdl = loadModel(model.ContactGroupID);

            if (RemoveSpecialChar)
            {
                model.SMSContent = model.SMSContent.RemoveUnprintableChar();
            }


            LstTeacherReceiverID = string.IsNullOrEmpty(LstTeacherReceiverID) ? string.Empty : LstTeacherReceiverID;

            if (string.IsNullOrEmpty(model.SMSContent))
            {
                logger.Info("Gửi tin nhắn không thành công cho giáo viên");
                return Json(new JsonMessage(Res.Get("Send_SMS_Group_Empty_SMS_Error"), JsonMessage.ERROR));
            }
            else model.SMSContent = model.SMSContent.Trim();

            if (fullMdl.NumOfTeacher == 0)
            {
                logger.Info("Gửi tin nhắn không thành công cho giáo viên");
                return Json(new JsonMessage(Res.Get("Send_SMS_Group_No_Teacher_In_Group"), JsonMessage.ERROR));
            }
            // Gan noi dung
            fullMdl.SMSContent = model.SMSContent;
            //kiem tra gui tin khi chua nhap tin nhan
            if (fullMdl.SMSContent.ToUpper().Equals(fullMdl.BrandName.ToUpper())
                || fullMdl.SMSContent.ToUpper().Equals(fullMdl.BrandName.ToUpper() + " " + fullMdl.SMSTemplate.ToUpper()))
            {
                logger.Info("Gửi tin nhắn không thành công cho giáo viên");
                return Json(new JsonMessage(Res.Get("Send_SMS_Group_Empty_SMS_Error"), JsonMessage.ERROR));
            }
            //Gửi tin nhan den giao vien
            var counter = 0;
            SMS_TYPE smsType = SMSTypeBusiness.All.FirstOrDefault(a => a.TYPE_CODE.StartsWith("TNGV"));
            if (smsType == null)
            {
                logger.Info("Gửi tin nhắn không thành công cho giáo viên");
                return Json(new JsonMessage(Res.Get("Send_SMS_Group_SMS_Not_Active"), JsonMessage.ERROR));
            }

            #region Short content
            string shortContent = fullMdl.SMSContent.Trim();

            if (!fullMdl.SMSTemplate.Trim().StartsWith(fullMdl.BrandName.Trim()))
            {
                fullMdl.SMSTemplate = fullMdl.BrandName.Trim() + " " + fullMdl.SMSTemplate.Trim();
            }
            if (shortContent.Trim().StartsWith(fullMdl.SMSTemplate.Trim()))
            {
                if (fullMdl.SMSTemplate.StartsWith(fullMdl.BrandName))
                {
                    shortContent = fullMdl.SMSContent.Trim().Remove(0, fullMdl.SMSTemplate.Trim().Length);
                }
                else
                {
                    shortContent = fullMdl.SMSContent.Trim().Remove(0, fullMdl.SMSTemplate.Trim().Length + fullMdl.BrandName.Trim().Length);
                }
            }
            else if (shortContent.Trim().StartsWith(fullMdl.BrandName.Trim()))
            {
                shortContent = fullMdl.SMSContent.Remove(0, fullMdl.BrandName.Length);
            }
            #endregion

            //Gui tin nhan den GV
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["contactGroupID"] = fullMdl.ContactGroupID;
            //Nếu là admin trường thì TeacherID = 0
            dic["senderID"] = _globalInfo.EmployeeID;

            bool isSendSignMessage = false;
            SMS_SCHOOL_CONFIG cfg = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            if (cfg != null) isSendSignMessage = cfg.IS_ALLOW_SEND_UNICODE && model.AllowSignMessage;
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary["schoolID"] = _globalInfo.SchoolID;
            dictionary["isLock"] = false;

            dictionary["isGetDefault"] = true;

            //if (fullMdl.isDefaultGroup)
            //{
            SMS_TEACHER_CONTACT_GROUP objContractGroup = null;
            if (ContactGroupBusiness.Search(dictionary) != null)
            {
                objContractGroup = ContactGroupBusiness.Search(dictionary).FirstOrDefault();
            }
            if (objContractGroup == null)
            {
                //Thong bao loi
                if (_globalInfo.IsRolePrincipal)
                {
                    logger.Info("Gửi tin nhắn không thành công cho giáo viên");
                    return Json(new JsonMessage(Res.Get("SendSMSToGroup_Label_LockContractGroup_Teacher"), SMAS.Web.Constants.GlobalConstantsEdu.ERROR));
                }
                else
                {
                    logger.Info("Gửi tin nhắn không thành công cho giáo viên");
                    return Json(new JsonMessage(Res.Get("SendSMSToGroup_Label_LockContractGroup_Admin"), SMAS.Web.Constants.GlobalConstantsEdu.ERROR));
                }
            }
            else
            {
                if (CheckAllReceiver)
                {
                    dic["lstReceiverID"] = new List<int>();
                }
                else
                {
                    List<int> lstTeacherReceiverIDToSend = LstTeacherReceiverID.Split(new char[] { SMAS.Web.Constants.GlobalConstantsEdu.charCOMMA }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                    if (lstTeacherReceiverIDToSend != null && lstTeacherReceiverIDToSend.Count > 0)
                    {
                        dic["lstReceiverID"] = lstTeacherReceiverIDToSend;
                    }
                    else
                    {
                        logger.Info("Gửi tin nhắn không thành công cho giáo viên");
                        return Json(new JsonMessage(Res.Get("Chưa chọn giáo viên để gửi tin"), JsonMessage.ERROR));
                    }
                }
            }

            //viethd31: Check ky tu dac biet
            bool isContentError = false;
            string dicContent = fullMdl.SMSContent.Trim();
            string unsignedContent = Utils.Utils.StripVNSign(dicContent);
            List<char> lstChar = new List<char>();
            string contentError = string.Empty;
            if (!this.IsPrintableString(unsignedContent, lstChar))
            {
                isContentError = true;
                contentError = string.Format("Nội dung tin nhắn có ký tự đặc biệt: {0}", string.Join(", ", lstChar));
            }

            if (!isContentError)
            {
                dic["schoolID"] = _globalInfo.SchoolID;
                dic["isSignMsg"] = isSendSignMessage;
                dic["content"] = fullMdl.SMSContent;
                dic["shortContent"] = shortContent;
                dic["academicYearID"] = _globalInfo.AcademicYearID;
                dic["type"] = SMAS.Web.Constants.GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_TEACHER;
                dic["typeHistory"] = SMAS.Web.Constants.GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TEACHER_ID;
                dic["isPrincipal"] = _globalInfo.IsRolePrincipal;
                dic["isAdmin"] = _globalInfo.IsAdminSchoolRole;
                dic["Promotion"] = fullMdl.Promotion;
                dic["UserName"] = SchoolProfileBusiness.GetUserNameBySchoolID(_globalInfo.SchoolID.Value);

                ResultBO result = SMSHistoryBusiness.SendSMS(dic);
                if (result.isError)
                {
                    logger.Info("Gửi tin nhắn không thành công cho giáo viên");
                    return Json(new { Message = Res.Get(result.ErrorMsg), Type = "error", OutOfBalance = result.OutOfBalance });
                }

                counter = result.NumSendSuccess;
                string desLog = string.Format("Gửi tin nhắn thành công cho {0} GV. ", counter);

                //Nếu giao dịch gửi tin gv có thay đổi tài khoản chính mới ghi log
                if (result.TransAmount > 0)
                {
                    desLog += ("Ngày giao dịch: " + DateTime.Now + ". Loại GD: Giao dịch thanh toán. Số lượng " + result.TotalSMSSentInMainBalance + " SMS. Số tiền: " + result.TransAmount + "VNĐ. Dịch vụ: Tin nhắn GV");
                }
                logger.Info(desLog);
                return Json(new { Message = string.Format(Res.Get("Send_SMS_Group_SMS_Success_Msg"), counter), Type = "success", Balance = result.Balance, NumOfPromotionInternalSMS = result.NumOfPromotionInternalSMS });
            }
            else
            {
                return Json(new JsonMessage(contentError, "ContentError"));
            }
        }
        public PartialViewResult ShowErrorConfirm(string contentError)
        {
            return PartialView("_ViewErrorConfirm", contentError);
        }
        private List<SListViewModel> SearchTeacher(int educationLevel, int classId, int? subjectId, int semester)
        {
            List<TeacherBO> lstTeacher = EmployeeBusiness.GetListTeacherWithTeachingSubject(_globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value, _globalInfo.AppliedLevel.Value, educationLevel, classId, subjectId.GetValueOrDefault(), semester,_globalInfo.IsRolePrincipal,_globalInfo.EmployeeID.HasValue ? _globalInfo.EmployeeID.Value : 0);

            return lstTeacher
                .Select(o => new SListViewModel
                {
                    EmployeeID = o.TeacherID,
                    EmployeeName = o.FullName,
                    FacultyName = o.SchoolFacultyName,
                    Mobile = o.Mobile,
                    TeachingSubject = o.TeacherSubject
                }).ToList();
        }
        private SendSMSToTeacherViewModel loadModel(int contactGroupID)
        {
            int academicYearID = _globalInfo.AcademicYearID.Value;
            int schoolID = _globalInfo.SchoolID.Value;
            int provinceId = _globalInfo.ProvinceID.Value;
            if (provinceId == 0)
            {
                SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
                provinceId = (school != null) ? school.ProvinceID.Value : 0;
            }
            SendSMSToTeacherViewModel model = new SendSMSToTeacherViewModel();
            //Noi dung tin nhan
            SMS_SCHOOL_CONFIG sc = SchoolConfigBusiness.GetSchoolConfigBySchoolID(_globalInfo.SchoolID);
            model.SMSContent = CommonMethods.GetPrefix(sc != null ? sc.PRE_FIX : null);
            model.BrandName = CommonMethods.GetBrandName();
            //Kiem tra, neu la group toan truong thi add tay giao vien vao
            SMS_TEACHER_CONTACT_GROUP contactGroupObj = null;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["contactGroupID"] = contactGroupID;
            if (ContactGroupBusiness.Search(dic) != null)
            {
                contactGroupObj = ContactGroupBusiness.Search(dic).FirstOrDefault();
            }
            if (contactGroupObj != null)
            {

                List<TeacherBO> teacherBOList = EmployeeBusiness.GetTeachersOfSchool(schoolID, academicYearID);
                if (teacherBOList != null && teacherBOList.Count > 0)
                {
                    teacherBOList = teacherBOList.Where(p => p.EmployeeStatus == SMAS.Web.Constants.GlobalConstants.EMPLOYEE_STATUS_WORKING && p.IsActive).ToList();
                }

                model.isDefaultGroup = true;
                model.ContactGroupID = contactGroupObj.CONTACT_GROUP_ID;
                model.GroupName = contactGroupObj.CONTACT_GROUP_NAME;
                if (teacherBOList != null)
                {
                    if (!_globalInfo.IsAdminSchoolRole)
                    {
                        model.NumOfTeacher = teacherBOList.Count() - 1;
                    }
                    else
                    {
                        model.NumOfTeacher = teacherBOList.Count();
                    }
                }
                else
                {
                    model.NumOfTeacher = 0;
                }


                // balance
                var ewallet = EWalletBusiness.GetEWalletIncludePromotion(schoolID, true, false);
                if (ewallet != null)
                {
                    model.Balance = ewallet.Balance;
                    //model.NumOfPromotionInternalSMS = ewallet.InternalSMS;
                }

                //Lay chuong trinh khuyen mai
                PromotionProgramBO objPromotion = PromotionBusiness.GetPromotionProgramByProvinceId(provinceId, schoolID);
                if (objPromotion != null)
                {
                    model.ContentPromotion = !String.IsNullOrEmpty(objPromotion.Note) ? objPromotion.Note : objPromotion.PromotionName;
                    model.Promotion = objPromotion;
                }

                //Config allow send unicode msg
                if (sc != null) ViewData[UsingMarkBookManagementConstants.IS_ALLOW_SEND_UNICODE_MSG] = sc.IS_ALLOW_SEND_UNICODE;
            }
            else throw new ArgumentNullException();

            model.SMSTemplate = (string.Format("{0}:{1}", _globalInfo.SchoolName, string.Empty));
            return model;
        }
        private bool IsPrintableString(string text, List<char> lstChar)
        {
            bool isPrintable = true;
            for (int i = 0; i < text.Length; i++)
            {
                string s = text[i].ToString().StripVNSign();
                if (!string.IsNullOrWhiteSpace(s) && (s[0] < 32 || s[0] > 126))
                {
                    isPrintable = false;
                    lstChar.Add(text[i]);
                }
            }

            return isPrintable;
        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            //Hoc ky
            List<KeyValuePair<int, string>> lstSemester = new List<KeyValuePair<int, string>>() { new KeyValuePair<int, string>(1, "Học kỳ I"), new KeyValuePair<int, string>(2, "Học kỳ II") };
            ViewData[UsingMarkBookManagementConstants.CBO_SEMESTER] = new SelectList(lstSemester, "key", "value", _globalInfo.Semester);

            //Khoi
            ViewData[UsingMarkBookManagementConstants.CBO_EDUCATION_LEVEL] = new SelectList(_globalInfo.EducationLevels, "EducationLevelID", "Resolution");

            //Lop
            ViewData[UsingMarkBookManagementConstants.CBO_CLASS] = new SelectList(new List<ClassProfile>(), "ClassProfileID", "DisplayName");

            //Thang
            int semester = _globalInfo.Semester.GetValueOrDefault();
            List<KeyValuePair<int, string>> lstMonth = new List<KeyValuePair<int, string>>();
            if (semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                lstMonth = new List<KeyValuePair<int, string>>
                {
                    new KeyValuePair<int, string>(8, "Tháng 8"),
                    new KeyValuePair<int, string>(9, "Tháng 9"),
                    new KeyValuePair<int, string>(10, "Tháng 10"),
                    new KeyValuePair<int, string>(11, "Tháng 11"),
                    new KeyValuePair<int, string>(12, "Tháng 12"),
                };
            }
            else if (semester == SMAS.Web.Constants.GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                lstMonth = new List<KeyValuePair<int, string>>
                {
                    new KeyValuePair<int, string>(1, "Tháng 1"),
                    new KeyValuePair<int, string>(2, "Tháng 2"),
                    new KeyValuePair<int, string>(3, "Tháng 3"),
                    new KeyValuePair<int, string>(4, "Tháng 4"),
                    new KeyValuePair<int, string>(5, "Tháng 5"),
                };
            }

            ViewData[UsingMarkBookManagementConstants.CBO_MONTH] = new SelectList(lstMonth, "key", "value");

        }

        #endregion
    }
}