﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.ExemptedObjectTypeArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.ExemptedObjectTypeArea.Controllers
{
    public class ExemptedObjectTypeController : BaseController
    {        
        private readonly IExemptedObjectTypeBusiness ExemptedObjectTypeBusiness;
		
		public ExemptedObjectTypeController (IExemptedObjectTypeBusiness exemptedobjecttypeBusiness)
		{
			this.ExemptedObjectTypeBusiness = exemptedobjecttypeBusiness;
		}
		
		//
        // GET: /ExemptedObjectType/

        public ActionResult Index()
        {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
            //Get view data here

            IEnumerable<ExemptedObjectTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[ExemptedObjectTypeConstants.LIST_EXEMPTEDOBJECTTYPE] = lst;
            return View();
        }

		//
        // GET: /ExemptedObjectType/Search

        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolution;
			//add search info
			//

            IEnumerable<ExemptedObjectTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[ExemptedObjectTypeConstants.LIST_EXEMPTEDOBJECTTYPE] = lst;

            //Get view data here

            return PartialView("_List");
        }
		
		/// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            ExemptedObjectType exemptedobjecttype = new ExemptedObjectType();
            TryUpdateModel(exemptedobjecttype); 
            Utils.Utils.TrimObject(exemptedobjecttype);
            exemptedobjecttype.IsActive = true;

            this.ExemptedObjectTypeBusiness.Insert(exemptedobjecttype);
            this.ExemptedObjectTypeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
		
		/// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ExemptedObjectTypeID)
        {
            ExemptedObjectType exemptedobjecttype = this.ExemptedObjectTypeBusiness.Find(ExemptedObjectTypeID);
            TryUpdateModel(exemptedobjecttype);
            Utils.Utils.TrimObject(exemptedobjecttype);
            this.ExemptedObjectTypeBusiness.Update(exemptedobjecttype);
            this.ExemptedObjectTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
		
		/// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.ExemptedObjectTypeBusiness.Delete(id);
            this.ExemptedObjectTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
		
		/// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<ExemptedObjectTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<ExemptedObjectType> query = this.ExemptedObjectTypeBusiness.Search(SearchInfo);
            IQueryable<ExemptedObjectTypeViewModel> lst = query.Select(o => new ExemptedObjectTypeViewModel {               
						ExemptedObjectTypeID = o.ExemptedObjectTypeID,								
						Resolution = o.Resolution				
            });

            return lst.ToList();
        }        
    }
}





