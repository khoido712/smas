﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExemptedObjectTypeArea
{
    public class ExemptedObjectTypeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExemptedObjectTypeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExemptedObjectTypeArea_default",
                "ExemptedObjectTypeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
