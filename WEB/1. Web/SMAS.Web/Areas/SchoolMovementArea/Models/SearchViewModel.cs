/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.SchoolMovementArea.Models
{
    public class SearchViewModel
    {
        public System.Nullable<System.Int32> Semester { get; set; }
        [ResourceDisplayName("SchoolMovement_Label_FullName")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string FullName { get; set; }
        [ResourceDisplayName("SchoolMovement_Label_PupilCode")]
        [StringLength(30, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "maxLengthResourceKey")]
        public string PupilCode { get; set; }

    }
}
