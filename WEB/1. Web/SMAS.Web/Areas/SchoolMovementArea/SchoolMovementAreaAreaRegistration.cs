﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.SchoolMovementArea
{
    public class SchoolMovementAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SchoolMovementArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SchoolMovementArea_default",
                "SchoolMovementArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
