﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.SchoolMovementArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.HtmlHelpers;

namespace SMAS.Web.Areas.SchoolMovementArea.Controllers
{
    public class SchoolMovementController : BaseController
    {
        private readonly ISchoolMovementBusiness SchoolMovementBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IPupilProfileBusiness PupilProfileBusiness;
        private readonly IProvinceBusiness ProvinceBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        public SchoolMovementController(ISchoolMovementBusiness schoolmovementBusiness, ISchoolProfileBusiness schoolProfileBusiness, IDistrictBusiness districtBusiness, IProvinceBusiness provinceBusiness, IPupilProfileBusiness pupilProfileBusiness, IClassProfileBusiness cp, IAcademicYearBusiness ab, IPupilOfClassBusiness pupilOfClassBusiness)
        {
            this.SchoolMovementBusiness = schoolmovementBusiness;
            this.AcademicYearBusiness = ab;
            this.ClassProfileBusiness = cp;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.PupilProfileBusiness = pupilProfileBusiness;
            this.ProvinceBusiness = provinceBusiness;
            this.DistrictBusiness = districtBusiness;
            this.SchoolProfileBusiness = schoolProfileBusiness;
        }

        public ActionResult Index()
        {
            GlobalInfo global = new GlobalInfo();

            ViewData[SchoolMovementConstants.PERMISSION] = true;
            if (!global.IsCurrentYear) ViewData[SchoolMovementConstants.PERMISSION] = false;
            if (!global.HasHeadTeacherPermission(0))
            {
                ViewData[SchoolMovementConstants.PERMISSION] = false;
                //throw new BusinessException("SchoolMovement_Validate_Permission");
            }
            SearchViewModel frm = new SearchViewModel();
            frm.Semester = (int)LoadSemester();

            LoadEducationLevel();
            ViewData[SchoolMovementConstants.LS_CBNEWPUPILOFCLASS] = new SelectList(new string[] { });
            ViewData[SchoolMovementConstants.LS_CBNEWCLASS] = new SelectList(new string[] { });
            ViewData[SchoolMovementConstants.LS_CBOLDCLASS] = new SelectList(new string[] { });
            ViewData[SchoolMovementConstants.LST_SEMESTER] = CommonFunctions.GetListSemester();
            ViewData[SchoolMovementConstants.LST_PROVINCE] = new SelectList(new string[] { });
            ViewData[SchoolMovementConstants.LST_DISTRICT] = new SelectList(new string[] { });
            ViewData[SchoolMovementConstants.LST_SCHOOL] = new SelectList(new string[] { });
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            List<SchoolMovementViewModel> lst = new List<SchoolMovementViewModel>();
            ViewData[SchoolMovementConstants.LIST_SCHOOLMOVEMENT] = lst;


            //frm.Semester = 1;
            Search(frm);
            return View();
        }

        #region Seach
        public ActionResult Search(SearchViewModel frm)
        {
            GlobalInfo global = new GlobalInfo();
            ViewData[SchoolMovementConstants.PERMISSION] = true;
            if (!global.IsCurrentYear) ViewData[SchoolMovementConstants.PERMISSION] = false;
            if (!global.HasHeadTeacherPermission(0))
            {
                ViewData[SchoolMovementConstants.PERMISSION] = false;
                //throw new BusinessException("SchoolMovement_Validate_Permission");
            }
            Utils.Utils.TrimObject(frm);
            if (frm.PupilCode != null)
            {
                if (frm.PupilCode.Length > 30)
                {
                    throw new BusinessException(Res.Get("SchoolMovement_Validate_PupilCode"));
                }
            }
            if (frm.FullName != null)
            {
                if (frm.FullName.Length > 100)
                {
                    throw new BusinessException(Res.Get("SchoolMovement_Validate_FullName"));
                }
            }
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["Semester"] = frm.Semester;
            SearchInfo["PupilCode"] = frm.PupilCode;
            SearchInfo["FullName"] = frm.FullName;
            SearchInfo["AcademicYearID"] = new GlobalInfo().AcademicYearID;
            SearchInfo["AppliedLevel"] = new GlobalInfo().AppliedLevel;
            IEnumerable<SchoolMovementViewModel> lst = this._Search(SearchInfo);
            ViewData[SchoolMovementConstants.LIST_SCHOOLMOVEMENT] = lst;

            //Get view data here

            return PartialView("_List");
        }
        private IEnumerable<SchoolMovementViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo global = new GlobalInfo();
            IQueryable<SchoolMovement> query = this.SchoolMovementBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo).OrderBy(o => o.PupilProfile.Name);
            // query = query.Where(o => o.SchoolMovementID == null);
            IQueryable<SchoolMovementViewModel> lst = query.Select(o => new SchoolMovementViewModel
            {
                SchoolMovementID = o.SchoolMovementID,
                PupilID = o.PupilID,
                ClassID = o.ClassID,
                EducationLevelID = o.EducationLevelID,
                SchoolID = o.SchoolID,
                AcademicYearID = o.AcademicYearID,
                Semester = o.Semester,
                MovedToProvinceID = o.MovedToProvinceID,
                MovedToDistrictID = o.MovedToDistrictID,
                MovedToClassID = o.MovedToClassID,
                MovedToSchoolID = o.MovedToSchoolID,
                MovedToClassName = o.MovedToClassName,
                MovedToSchoolName = o.MovedToSchoolName,
                MovedToSchoolAddress = o.MovedToSchoolAddress,
                MovedDate = o.MovedDate,
                Description = o.Description,
                FullName = o.PupilProfile.FullName,
                PupilCode = o.PupilProfile.PupilCode,
                MovedToProvinceName = o.Province.ProvinceName,
                ClassName = o.ClassProfile.DisplayName,
                canEdit = true

            });
            List<SchoolMovementViewModel> list = lst.ToList();
            foreach (SchoolMovementViewModel item in list)
            {
                if (item.MovedToSchoolID != null)
                {
                    item.MovedToSchoolNameGrid = SchoolProfileBusiness.Find(item.MovedToSchoolID).SchoolName;
                }
                else if (item.MovedToSchoolName != null)
                {
                    item.MovedToSchoolNameGrid = item.MovedToSchoolName;
                }
                int Year = (int)AcademicYearBusiness.Find(item.AcademicYearID).FirstSemesterStartDate.Value.Year;
                item.AcademicYearName = Year + "-" + (Year + 1);
            }
            if (!global.IsCurrentYear)
            {
                foreach (SchoolMovementViewModel item in list)
                {
                    item.canEdit = false;
                }
            }
            else
            {
                foreach (var item in list)
                {
                    if (!global.HasHeadTeacherPermission(item.ClassID))
                    {
                        item.canEdit = false;
                    }
                    else if (item.MovedToSchoolID != null && item.MovedToClassID != null)
                    {
                        item.canEdit = false;
                    }

                }
            }
            return list;
        }
        #endregion

        #region Create Controller
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create(SchoolMovementViewModel frm)
        {
            GlobalInfo global = new GlobalInfo();
            SchoolMovement schoolmovement = new SchoolMovement();
            TryUpdateModel(frm);
            Utils.Utils.TrimObject(frm);
            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            DateTime firstStartDate = academicYear.FirstSemesterStartDate.Value;
            DateTime firstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime secondStartDate = academicYear.SecondSemesterStartDate.Value;
            DateTime secondEndDate = academicYear.SecondSemesterEndDate.Value;
            if (frm.EducationLevelID == 0)
            {
                throw new BusinessException(Res.Get("SchoolMovement_Validate_EducationLevel"));
            }
            else if (frm.ClassID == 0)
            {
                throw new BusinessException(Res.Get("SchoolMovement_Validate_Class"));
            }
            else if (frm.PupilID == 0)
            {
                throw new BusinessException(Res.Get("SchoolMovement_Validate_Pupil"));
            }
            else if (frm.MovedDate == null)
            {
                throw new BusinessException(Res.Get("SchoolMovement_Validate_MovedDate"));
            }
            else if (frm.Semester == 0)
            {
                throw new BusinessException(Res.Get("SchoolMovement_Validate_Semester"));
            }
            else if (!frm.MovedToSchoolID.HasValue)
            {
                throw new BusinessException(Res.Get("SchoolMovement_Validate_School"));
            }
            else if (frm.Description != null && frm.Description.Length > 256)
            {
                throw new BusinessException(Res.Get("SchoolMovement_Validate_Description"));
            }
            else if (frm.Semester == 1 && (frm.MovedDate < firstStartDate || frm.MovedDate > firstEndDate))
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_FirstSemester"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_FirstSemester"));
            }
            else if (frm.Semester == 2 && (frm.MovedDate < secondStartDate || frm.MovedDate > secondEndDate))
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_SecondSemester"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_SecondSemester"));
            }
            else
            {
                schoolmovement.AcademicYearID = global.AcademicYearID.Value;
                schoolmovement.SchoolID = global.SchoolID.Value;
                schoolmovement.EducationLevelID = (int)frm.EducationLevelID;
                schoolmovement.ClassID = frm.ClassID;
                schoolmovement.PupilID = frm.PupilID;
                schoolmovement.Semester = frm.Semester;

                schoolmovement.MovedDate = frm.MovedDate.Value;
                schoolmovement.MovedToProvinceID = frm.MovedToProvinceID;
                schoolmovement.MovedToDistrictID = frm.MovedToDistrictID;
                schoolmovement.MovedToSchoolID = frm.MovedToSchoolID;
                schoolmovement.Description = frm.Description;
                this.SchoolMovementBusiness.InsertSchoolMovement(global.UserAccountID, schoolmovement);
                this.SchoolMovementBusiness.Save();

                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult CreateSchoolOut(SchoolMovementViewModel frm)
        {
            GlobalInfo global = new GlobalInfo();
            SchoolMovement schoolmovement = new SchoolMovement();
            TryUpdateModel(frm);
            Utils.Utils.TrimObject(frm);
            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            DateTime firstStartDate = academicYear.FirstSemesterStartDate.Value;
            DateTime firstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime secondStartDate = academicYear.SecondSemesterStartDate.Value;
            DateTime secondEndDate = academicYear.SecondSemesterEndDate.Value;
            if (frm.EducationLevelID == 0)
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_EducationLevel"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_EducationLevel"));
            }
            else if (frm.ClassID == 0)
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_Class"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_Class"));
            }
            else if (frm.PupilID == 0)
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_Pupil"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_Pupil"));
            }
            else if (!frm.MovedDateOut.HasValue)
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_MovedDate"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_MovedDate"));
            }
            else if (frm.Semester == 0)
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_Semester"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_Semester"));
            }
            else if (frm.MovedToSchoolName == null)
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_MovedToSchoolName"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_MovedToSchoolName"));
            }
            else if (frm.MovedToSchoolAddress == null)
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_MovedToSchoolAddress"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_MovedToSchoolAddress"));
            }
            else if (frm.MovedToClassName == null)
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_MovedToClassName"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_MovedToClassName"));
            }
            else if (frm.Description != null && frm.Description.Length > 256)
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_Description"), "error"));
                // throw new BusinessException(Res.Get("SchoolMovement_Validate_Description"));
            }
            else if (frm.Semester == 1 && (frm.MovedDateOut < firstStartDate || frm.MovedDate > firstEndDate))
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_FirstSemester"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_FirstSemester"));
            }
            else if (frm.Semester == 2 && (frm.MovedDateOut < secondStartDate || frm.MovedDate > secondEndDate))
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_SecondSemester"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_SecondSemester"));
            }
            else
            {
                schoolmovement.AcademicYearID = global.AcademicYearID.Value;
                schoolmovement.SchoolID = global.SchoolID.Value;
                schoolmovement.AcademicYearID = global.AcademicYearID.Value;
                schoolmovement.SchoolID = global.SchoolID.Value;
                schoolmovement.EducationLevelID = (int)frm.EducationLevelID;
                schoolmovement.ClassID = frm.ClassID;
                schoolmovement.PupilID = frm.PupilID;
                schoolmovement.Semester = frm.Semester;
                schoolmovement.MovedDate = frm.MovedDateOut.Value;
                schoolmovement.MovedToClassName = frm.MovedToClassName;
                schoolmovement.MovedToSchoolName = frm.MovedToSchoolName;
                schoolmovement.MovedToSchoolAddress = frm.MovedToSchoolAddress;
                schoolmovement.Description = frm.Description;

                this.SchoolMovementBusiness.InsertSchoolMovement(global.UserAccountID, schoolmovement);
                this.SchoolMovementBusiness.Save();

                return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
            }
        }
        #endregion

        #region Edit Controller
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int SchoolMovementID, FormCollection form)
        {
            String MovedDateEditIn = (String)form["MovedDateEditIn"];
            String Semester = (String)form["Semester"];
            GlobalInfo global = new GlobalInfo();
            SchoolMovement schoolmovement = this.SchoolMovementBusiness.Find(SchoolMovementID);
            TryUpdateModel(schoolmovement);
            Utils.Utils.TrimObject(schoolmovement);
            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            DateTime firstStartDate = academicYear.FirstSemesterStartDate.Value;
            DateTime firstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime secondStartDate = academicYear.SecondSemesterStartDate.Value;
            DateTime secondEndDate = academicYear.SecondSemesterEndDate.Value;
            DateTime MovedDate = Convert.ToDateTime(MovedDateEditIn);
            int SemesterEdit = Convert.ToInt32(Semester);
            if (!schoolmovement.MovedToProvinceID.HasValue)
            {
                throw new BusinessException("SchoolMovement_Validate_MovedToProvince");
            }
            else if (!schoolmovement.MovedToDistrictID.HasValue)
            {
                throw new BusinessException("SchoolMovement_Validate_MovedToDistrict");
            }
            else if (!schoolmovement.MovedToSchoolID.HasValue)
            {
                throw new BusinessException("SchoolMovement_Validate_MovedToSchool");
            }

            else if (SemesterEdit == 1 && (MovedDate < firstStartDate || MovedDate > firstEndDate))
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_FirstSemester"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_FirstSemester"));
            }
            else if (SemesterEdit == 2 && (MovedDate < secondStartDate || MovedDate > secondEndDate))
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_SecondSemester"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_SecondSemester"));
            }
            else
            {
                schoolmovement.AcademicYearID = global.AcademicYearID.Value;
                schoolmovement.SchoolID = global.SchoolID.Value;
                schoolmovement.MovedToSchoolAddress = "";
                schoolmovement.MovedToSchoolName = "";
                schoolmovement.MovedToClassName = null;
                schoolmovement.MovedDate = Convert.ToDateTime(MovedDateEditIn);
                this.SchoolMovementBusiness.UpdateSchoolMovement(global.UserAccountID, schoolmovement);
                this.SchoolMovementBusiness.Save();

                return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult EditSchoolOut(int SchoolMovementID, FormCollection form)
        {
            String MovedDateEditOut = (String)form["MovedDateEditOut"];
            String Semester = (String)form["Semester"];
            GlobalInfo global = new GlobalInfo();
            SchoolMovement schoolmovement = this.SchoolMovementBusiness.Find(SchoolMovementID);
            TryUpdateModel(schoolmovement);
            Utils.Utils.TrimObject(schoolmovement);
            DateTime MovedDate = Convert.ToDateTime(MovedDateEditOut);
            int SemesterEdit = Convert.ToInt32(Semester);
            AcademicYear academicYear = AcademicYearBusiness.Find(global.AcademicYearID.Value);
            DateTime firstStartDate = academicYear.FirstSemesterStartDate.Value;
            DateTime firstEndDate = academicYear.FirstSemesterEndDate.Value;
            DateTime secondStartDate = academicYear.SecondSemesterStartDate.Value;
            DateTime secondEndDate = academicYear.SecondSemesterEndDate.Value;
            if (schoolmovement.MovedToSchoolName == null)
            {
                throw new BusinessException("SchoolMovement_Validate_MovedToSchoolName");
            }
            else if (schoolmovement.MovedToSchoolAddress == null)
            {
                throw new BusinessException("SchoolMovement_Validate_MovedToSchoolAddress");
            }
            else if (schoolmovement.MovedToClassName == null)
            {
                throw new BusinessException("SchoolMovement_Validate_MovedToClassName");
            }
            else if (SemesterEdit == 1 && (MovedDate < firstStartDate || MovedDate > firstEndDate))
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_FirstSemester"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_FirstSemester"));
            }
            else if (SemesterEdit == 2 && (MovedDate < secondStartDate || MovedDate > secondEndDate))
            {
                return Json(new JsonMessage(Res.Get("SchoolMovement_Validate_SecondSemester"), "error"));
                //throw new BusinessException(Res.Get("SchoolMovement_Validate_SecondSemester"));
            }
            else
            {
                schoolmovement.AcademicYearID = global.AcademicYearID.Value;
                schoolmovement.SchoolID = global.SchoolID.Value;
                schoolmovement.MovedToDistrictID = null;
                schoolmovement.MovedToSchoolID = null;
                schoolmovement.MovedToProvinceID = null;
                schoolmovement.MovedDate = Convert.ToDateTime(MovedDateEditOut);

                this.SchoolMovementBusiness.UpdateSchoolMovement(global.UserAccountID, schoolmovement);
                this.SchoolMovementBusiness.Save();
            }
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        #endregion

        #region Delete
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo global = new GlobalInfo();
            this.SchoolMovementBusiness.DeleteSchoolMovement(global.UserAccountID, id, global.SchoolID.Value);
            this.SchoolMovementBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion

        #region Load School Type
        private void LoadSchoolType()
        {
            List<ViettelCheckboxList> lsradio = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio1 = new ViettelCheckboxList();
            ViettelCheckboxList viettelradio2 = new ViettelCheckboxList();
            viettelradio1.Label = Res.Get("SchoolMovement_Label_SchoolIn");
            viettelradio1.Value = 1;
            viettelradio1.cchecked = true;
            lsradio.Add(viettelradio1);

            viettelradio2 = new ViettelCheckboxList();
            viettelradio2.Label = Res.Get("SchoolMovement_Label_SchoolOut");
            viettelradio2.Value = 2;
            lsradio.Add(viettelradio2);

            ViewData[SchoolMovementConstants.LST_SCHOOLTYPE] = lsradio;

        }
        #endregion

        #region LoadSemester
        private int LoadSemester()
        {
            //Load RadioButton
            AcademicYear ay = AcademicYearBusiness.Find(new GlobalInfo().AcademicYearID.Value);
            bool rbchecked = false;
            List<int> lsSemester = new List<int> { 1, 2 };
            List<ViettelCheckboxList> lsradio = new List<ViettelCheckboxList>();
            ViettelCheckboxList viettelradio1 = new ViettelCheckboxList();
            ViettelCheckboxList viettelradio2 = new ViettelCheckboxList();
            ViewData[SchoolMovementConstants.DEFAULT_SEMESTER1] = "false";
            if (lsSemester != null)
            {
                for (int i = 0; i < lsSemester.Count; i++)
                {
                    if (lsSemester[i] == 1)
                    {
                        viettelradio1 = new ViettelCheckboxList();
                        viettelradio1.Label = Res.Get("Common_Label_FirstSemester");
                        viettelradio1.Value = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                        if ((ay.FirstSemesterStartDate < DateTime.Now) && (DateTime.Now < ay.FirstSemesterEndDate))
                        {
                            viettelradio1.cchecked = true;
                            rbchecked = true;
                            ViewData[SchoolMovementConstants.DEFAULT_SEMESTER1] = "true";
                        }

                        lsradio.Add(viettelradio1);
                    }
                    else if (lsSemester[i] == 2)
                    {
                        viettelradio2 = new ViettelCheckboxList();
                        viettelradio2.Label = Res.Get("Common_Label_SecondSemester");
                        viettelradio2.Value = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                        if ((ay.SecondSemesterStartDate < DateTime.Now) && (DateTime.Now < ay.SecondSemesterEndDate))
                        {
                            viettelradio2.cchecked = true;
                            rbchecked = true;
                            ViewData[SchoolMovementConstants.DEFAULT_SEMESTER1] = "false";
                        }
                        lsradio.Add(viettelradio2);
                    }
                    //if (!rbchecked)
                    //{
                    //    viettelradio1.cchecked = true;
                    //}
                }
                ViewData[SchoolMovementConstants.LST_SEMESTER] = lsradio;
            }
            int count = 0;
            foreach (var ls in lsradio)
            {
                count++;
                if (count == 1 && ls.cchecked == true)
                    return 1;
                else
                    return 2;
            }
            return 2;
        }
        #endregion

        #region Create School Movement View
        public PartialViewResult LoadCreateSchoolMovement()
        {
            GlobalInfo global = new GlobalInfo();
            //if (!global.HasHeadTeacherPermission(0))
            //{
            //    throw new BusinessException("SchoolMovement_Validate_Permission");
            //}
            LoadEducationLevel();
            LoadProvince();
            LoadSchoolType();
            ViewData[SchoolMovementConstants.LS_CBNEWPUPILOFCLASS] = new SelectList(new string[] { });
            ViewData[SchoolMovementConstants.LS_CBNEWCLASS] = new SelectList(new string[] { });
            ViewData[SchoolMovementConstants.LS_CBOLDCLASS] = new SelectList(new string[] { });
            ViewData[SchoolMovementConstants.LST_DISTRICT] = new SelectList(new string[] { });
            ViewData[SchoolMovementConstants.LST_SCHOOL] = new SelectList(new string[] { });
            ViewData[SchoolMovementConstants.LST_SEMESTER] = CommonFunctions.GetListSemester();
            return PartialView("SchoolType");
        }
        private void LoadProvince()
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["IsActive"] = true;
            List<Province> lst = ProvinceBusiness.Search(dic).ToList();
            ViewData[SchoolMovementConstants.LST_PROVINCE] = new SelectList(lst, "ProvinceID", "ProvinceName");
        }

        #region LoadEducationLevel
        public void LoadEducationLevel()
        {
            GlobalInfo global = new GlobalInfo();
            List<EducationLevel> lsEducationLevel = global.EducationLevels;
            if (lsEducationLevel != null)
            {
                ViewData[SchoolMovementConstants.LST_EDUCATIONLEVEL] = new SelectList(lsEducationLevel, "EducationLevelID", "Resolution");
            }
            else
            {
                ViewData[SchoolMovementConstants.LST_EDUCATIONLEVEL] = new SelectList(new string[] { });
            }

        }
        #endregion

        #region Load Class
        [HttpPost]
        //Load class when click education

        [ValidateAntiForgeryToken]
        public JsonResult _GetDropDownListClassForwardingOldClass(int? EducationLevelId)
        {

            return _GetClass(EducationLevelId);
        }

        private JsonResult _GetClass(int? ID)
        {
            if (ID.HasValue)
            {
                //•	AcademicYearID = UserInfo.AcademicYearID
                GlobalInfo globalInfo = new GlobalInfo();
                int? AcademicYearID = globalInfo.AcademicYearID;
                List<ClassProfile> lsClass = _GetClassProfile(AcademicYearID, ID);
                return Json(new SelectList(lsClass, "ClassProfileID", "DisplayName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);

            }
        }

        private List<ClassProfile> _GetClassProfile(int? AcademicYearID, int? EducationLevelID)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", AcademicYearID.Value);
            dic.Add("EducationLevelID", EducationLevelID);
            if (!globalInfo.IsAdminSchoolRole)
            {
                dic.Add("UserAccountID", globalInfo.UserAccountID);
                dic.Add("Type", SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER);
            }
            List<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).ToList();
            return lsClass;
        }


        #endregion

        #region Load Pupil
        [HttpPost]
        //Load pupil of class when click class

        [ValidateAntiForgeryToken]
        public JsonResult _GetDropDownListClassForwardingNewClass(int? EducationLevelId, int? OldClassID)
        {
            return _GetNewClass(EducationLevelId, OldClassID);
        }

        private JsonResult _GetNewClass(int? ID, int? OldClassID)
        {
            if (ID.HasValue)
            {
                int typeCreate = 1;
                //•	AcademicYearID = UserInfo.AcademicYearID
                List<PupilProfile> LstPupilProfile = new List<PupilProfile>();
                GlobalInfo globalInfo = new GlobalInfo();
                int? AcademicYearID = globalInfo.AcademicYearID;
                LstPupilProfile = _GetPupilProfile(AcademicYearID, OldClassID, typeCreate);
                return Json(new SelectList(LstPupilProfile.OrderBy(o => o.Name).ToList(), "PupilProfileID", "FullName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);

            }

        }

        private List<PupilProfile> _GetPupilProfile(int? AcademicYearID, int? ClassID, int type)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            IQueryable<PupilOfClass> lsClass = null;
            List<PupilProfile> LstPupilProfile = new List<PupilProfile>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            IDictionary<string, object> dicPupil = new Dictionary<string, object>();
            dic.Add("AcademicYearID", AcademicYearID.Value);
            dic.Add("ClassID", ClassID);
            //Neu type = 1 thi la load pupil khi create, chi load nhung hoc sinh dang hoc
            //Neu type = 2 thi la load moi pupil khi edit, de hien thi dc nhung hoc sinh da chuyen truong len combobox
            if (type == 1)
                lsClass = PupilOfClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName);
            else if (type == 2) lsClass = PupilOfClassBusiness.SearchBySchool(globalInfo.SchoolID.Value, dic).OrderBy(o => o.OrderInClass).ThenBy(o => o.PupilProfile.Name).ThenBy(o => o.PupilProfile.FullName);
            foreach (PupilOfClass item in lsClass)
            {
                PupilProfile PupilProfile = new PupilProfile();
                PupilProfile.PupilProfileID = item.PupilID;
                PupilProfile.FullName = item.PupilProfile.FullName;
                LstPupilProfile.Add(PupilProfile);
            }
            return LstPupilProfile;
        }


        #endregion

        #region Load District CBB
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetDropDownListClassForwardingDistrict(int? provinceId)
        {

            return _GetDistrict(provinceId);
        }

        private JsonResult _GetDistrict(int? ID)
        {
            if (ID.HasValue)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("ProvinceID", ID);

                List<District> ls = DistrictBusiness.Search(dic).ToList();
                return Json(new SelectList(ls, "DistrictID", "DistrictName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);

            }
        }
        #endregion

        #region Load School CBB
        [HttpPost]

        [ValidateAntiForgeryToken]
        public JsonResult _GetDropDownListClassForwardingSchool(int? districtId, int ProvinceID)
        {

            return _GetSchool(districtId, ProvinceID);
        }

        private JsonResult _GetSchool(int? DistrictID, int ProvinceID)
        {
            int AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            if (DistrictID.HasValue)
            {
                int SchoolID = 0;
                if (new GlobalInfo().SchoolID.HasValue)
                {
                    SchoolID = new GlobalInfo().SchoolID.Value;
                }
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("ProvinceID", ProvinceID);
                dic.Add("DistrictID", DistrictID);
                int grade = SMAS.Business.Common.Utils.GradeToBinary(AppliedLevel);
                List<SchoolProfile> ls = SchoolProfileBusiness.Search(dic).Where(o => o.SchoolProfileID != SchoolID && ((o.EducationGrade & grade) != 0)).ToList();
                return Json(new SelectList(ls, "SchoolProfileID", "SchoolName"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new SelectList(new SelectList(new string[] { })), JsonRequestBehavior.AllowGet);

            }
        }

        #endregion

        #endregion

        #region Edit SchoolMovement
        private List<District> _GetDistrictProfile(int? ProvinceID)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic.Add("ProvinceID", ProvinceID);
            List<District> ls = DistrictBusiness.Search(dic).ToList();
            return ls;
        }

        private List<SchoolProfile> _GetSchoolProfile(int? ProvinceID, int? DistrictID)
        {
            GlobalInfo globalInfo = new GlobalInfo();
            int AppliedLevel = new GlobalInfo().AppliedLevel.Value;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("DistrictID", DistrictID);
            dic.Add("ProvinceID", ProvinceID);
            int grade = SMAS.Business.Common.Utils.GradeToBinary(AppliedLevel);
            List<SchoolProfile> ls = SchoolProfileBusiness.Search(dic).Where(o => ((o.EducationGrade & grade) != 0)).ToList();
            return ls;
        }


        //[ValidateAntiForgeryToken]
        public PartialViewResult LoadEditSchoolMovement(int ID)
        {
            GlobalInfo global = new GlobalInfo();
            int typeEdit = 2;
            //if (!global.HasHeadTeacherPermission(0))
            //{
            //    throw new BusinessException("SchoolMovement_Validate_Permission");
            //}
            GlobalInfo globalInfo = new GlobalInfo();
            int? AcademicYearID = globalInfo.AcademicYearID;
            SchoolMovement sm = SchoolMovementBusiness.Find(ID);
            SchoolMovementViewModel frm = new SchoolMovementViewModel();
            List<ClassProfile> lstEdu = new List<SMAS.Models.Models.ClassProfile>();
            frm.SchoolMovementID = sm.SchoolMovementID;
            frm.EducationLevelID = sm.EducationLevelID;
            frm.ClassID = sm.ClassID;
            frm.PupilID = sm.PupilID;
            frm.MovedDate = sm.MovedDate;
            frm.MovedDateOut = sm.MovedDate;
            frm.MovedDateEditIn = sm.MovedDate;
            frm.MovedDateEditOut = sm.MovedDate;
            frm.Semester = sm.Semester;
            frm.Description = sm.Description;
            ViewData[SchoolMovementConstants.LS_CBNEWPUPILOFCLASS] = new SelectList(_GetPupilProfile(AcademicYearID, frm.ClassID, typeEdit), "PupilProfileID", "FullName");
            ViewData[SchoolMovementConstants.LS_CBOLDCLASS] = new SelectList(_GetClassProfile(AcademicYearID, frm.EducationLevelID), "ClassProfileID", "DisplayName");
            ViewData[SchoolMovementConstants.LST_DISTRICT] = new SelectList(_GetDistrictProfile(sm.MovedToProvinceID), "DistrictID", "DistrictName");
            ViewData[SchoolMovementConstants.LST_SCHOOL] = new SelectList(_GetSchoolProfile(sm.MovedToProvinceID, sm.MovedToDistrictID), "SchoolProfileID", "SchoolName");
            ViewData[SchoolMovementConstants.LST_SEMESTER] = CommonFunctions.GetListSemester();
            LoadEducationLevel();
            LoadProvince();
            LoadSchoolType();
            //Neu truong trong he thong thi co MovedToSchoolID
            if (sm.MovedToSchoolID != null)
            {
                frm.MovedToProvinceID = sm.MovedToProvinceID;
                frm.MovedToDistrictID = sm.MovedToDistrictID;
                frm.MovedToSchoolID = sm.MovedToSchoolID.Value;
                return PartialView("Edit", frm);
            }

            frm.MovedToSchoolName = sm.MovedToSchoolName;
            frm.MovedToSchoolAddress = sm.MovedToSchoolAddress;
            frm.MovedToClassName = sm.MovedToClassName;

            return PartialView("EditOut", frm);

        }
        #endregion
    }
}





