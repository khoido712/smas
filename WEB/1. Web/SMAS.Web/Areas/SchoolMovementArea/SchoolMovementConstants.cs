/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.SchoolMovementArea
{
    public class SchoolMovementConstants
    {
        public const string LIST_SCHOOLMOVEMENT = "listSchoolMovement";
        public const string LST_SEMESTER = "listSemester";

        public const string LST_SCHOOLTYPE = "listSchoolType";

        public const string LST_EDUCATIONLEVEL = "listEducationLevel";

        public const string LS_CBOLDCLASS = "listOldClass";

        public const string LS_CBNEWCLASS = "listNewClass";

        public const string LS_CBNEWPUPILOFCLASS = "listPupilOfClass";

        public const string LST_PROVINCE = "listProvince";

        public const string LST_DISTRICT = "listDistrict";

        public const string LST_SCHOOL = "listSchool";
        public const string PERMISSION = "permission";
        public const string DEFAULT_SEMESTER1 = "default_Semester1";
    }
}