﻿using SMAS.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Models.Models;
using SMAS.Business.IBusiness;
using SMAS.Business.Business;
using SMAS.Web.Areas.HeadTeacherAssignmentArea;
using SMAS.Web.Utils;
using SMAS.Web.Constants;
using SMAS.Business.BusinessObject;
using System.IO;

namespace SMAS.Web.Areas.HeadTeacherAssignmentArea.Controllers
{
    public class HeadTeacherAssignmentController : BaseController
    {
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;
        private readonly IEmployeeBusiness EmployeeBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IReportEmployeeProfileBusiness ReportEmployeeProfileBusiness;
        public HeadTeacherAssignmentController(ISchoolFacultyBusiness SchoolFacultyBusiness, IEmployeeBusiness EmployeeBusiness, IClassProfileBusiness ClassProfileBusiness,
            IReportEmployeeProfileBusiness reportEmployeeProfileBusiness)
        {
            this.SchoolFacultyBusiness = SchoolFacultyBusiness;
            this.EmployeeBusiness = EmployeeBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.ReportEmployeeProfileBusiness = reportEmployeeProfileBusiness;
        }
        public ActionResult Index()
        {
            IDictionary<string,object> SearchInfo = new Dictionary<string,object>();
            SearchInfo["SchoolID"] = _globalInfo.SchoolID;
            SearchInfo["AcademicYearID"] = _globalInfo.AcademicYearID;
            IQueryable<SchoolFaculty> lsSchoolFaculty = SchoolFacultyBusiness.Search(SearchInfo).OrderBy(o => o.FacultyName);
            ViewData[HeadTeacherAssignmentConstants.LIST_SCHOOL_FACULTY] = lsSchoolFaculty.ToList();
            //check quyen nguoi dung
            SetViewDataPermission("HeadTeacherAssignment", _globalInfo.UserAccountID, _globalInfo.IsAdmin);
            return View();
        }
        public PartialViewResult AjaxLoadGridHeadTeacher(int FacultyID,int EducationLevelID)
        {
            //lay danh sach giao vien theo to bo mon
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"FacultyID",FacultyID},
                {"EmploymentStatus",GlobalConstants.EMPLOYEE_STATUS_WORKING}
            };
            List<Employee> lstEmployeeByFaculty = EmployeeBusiness.SearchTeacher(_globalInfo.SchoolID.Value, dic).OrderBy(p=>p.FullName).ToList();
            ViewData[HeadTeacherAssignmentConstants.LIST_RESULT] = lstEmployeeByFaculty;
            //lay danh sach lop hoc theo khoi
            dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"EducationLevelID",EducationLevelID}
            };
            List<ClassProfileBO> lstClassProfileBO = ClassProfileBusiness.GetListClassByEducationLevel(dic);
            ViewData[HeadTeacherAssignmentConstants.LIST_CLASS_PROFILE] = lstClassProfileBO;
            return PartialView("ListHeadTeacherAssignment");
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult SaveAssignment(FormCollection frm)
        {
            int FacultyID = frm["hdfFacultyID"] != null ? int.Parse(frm["hdfFacultyID"]) : 0;
            int EducationLevelID = frm["hdfEducationLevelID"] != null ? int.Parse(frm["hdfEducationLevelID"]) : 0;
            //lay danh sach giao vien theo to bo mon
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"FacultyID",FacultyID}
            };
            List<Employee> lstEmployeeByFaculty = EmployeeBusiness.SearchTeacher(_globalInfo.SchoolID.Value, dic).ToList();
            Employee objE = null;
            //lay danh sach lop hoc theo khoi
            dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"EducationLevelID",EducationLevelID}
            };
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            ClassProfile objCP = null;

            
            for (int i = 0; i < lstClassProfile.Count; i++)
            {
                objCP = lstClassProfile[i];
                for (int j = 0; j < lstEmployeeByFaculty.Count; j++)
                {
                    objE = lstEmployeeByFaculty[j];
                    if ("on".Equals(frm["chk_" + objE.EmployeeID + "_" + objCP.ClassProfileID]))
                    {
                        objCP.HeadTeacherID = objE.EmployeeID;
                        break;
                    }
                    else
                    {
                        if (objCP.HeadTeacherID > 0 && objCP.HeadTeacherID == objE.EmployeeID)
                        {
                        objCP.HeadTeacherID = null;
                    }
                }
            }
            }

            ClassProfileBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        [HttpPost]
        public JsonResult DeleteHeadTeacherByEducationLevelID(int EducationLevelID)
        {
            //lay danh sach lop hoc theo khoi
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"EducationLevelID",EducationLevelID}
            };
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            ClassProfile objCP = null;
            for (int i = 0; i < lstClassProfile.Count; i++)
            {
                objCP = lstClassProfile[i];
                objCP.HeadTeacherID = null;
            }
            ClassProfileBusiness.Save();
            return Json(new JsonMessage(Res.Get("success_del")));
        }

        public FileResult ExportHeadTeacher()
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"SchoolID",_globalInfo.SchoolID},
                {"AppliedLevelID",_globalInfo.AppliedLevel}
            };
            Stream excel = ReportEmployeeProfileBusiness.CreateHeadTeacherReport(dic);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = "GV_" + "Cap" + _globalInfo.AppliedLevel + "_DanhSachCanBo.xls";
            return result;
        }
    }
}