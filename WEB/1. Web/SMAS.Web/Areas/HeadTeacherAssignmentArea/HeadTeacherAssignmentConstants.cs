﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.HeadTeacherAssignmentArea
{
    public class HeadTeacherAssignmentConstants
    {
        public const string LIST_SCHOOL_FACULTY = "LIST_SCHOOL_FACULTY";
        public const string LIST_RESULT = "LIST_RESULT";
        public const string LIST_CLASS_PROFILE = "LIST_CLASS_PROFILE";
    }
}