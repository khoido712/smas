﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.HeadTeacherAssignmentArea
{
    public class HeadTeacherAssignmentAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HeadTeacherAssignmentArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HeadTeacherAssignmentArea_default",
                "HeadTeacherAssignmentArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
