﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MarkStatisticsSecondaryHKArea
{
    public class MarkStatisticsSecondaryHKAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MarkStatisticsSecondaryHKArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MarkStatisticsSecondaryHKArea_default",
                "MarkStatisticsSecondaryHKArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
