﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.MarkStatisticsSecondaryHKArea.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.HtmlHelpers;
using System.IO;
namespace SMAS.Web.Areas.MarkStatisticsSecondaryHKArea.Controllers
{
    public class MarkStatisticsSecondaryHKController : BaseController
    {
        private readonly IMarkStatisticBusiness MarkStatisticBusiness;
        private readonly IDistrictBusiness DistrictBusiness;
        private readonly ITrainingTypeBusiness TrainingTypeBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IReportDefinitionBusiness ReportDefinitionBusiness;
        private readonly IProcessedReportBusiness ProcessedReportBusiness;

        //
        // GET: /MarkStatisticsSecondaryHKArea/MarkStatisticsSecondaryHK/
        public MarkStatisticsSecondaryHKController(IMarkStatisticBusiness markStatisticBusiness, IDistrictBusiness districtBusiness,
            ITrainingTypeBusiness trainingTypeBusiness, IEducationLevelBusiness educationLevelBusiness, ISubjectCatBusiness subjectCatBusiness,
            IAcademicYearBusiness academicYearBusiness, IReportDefinitionBusiness reportDefinitionBusiness, IProcessedReportBusiness processedReportBusiness)
        {
            this.MarkStatisticBusiness = markStatisticBusiness;
            this.DistrictBusiness = districtBusiness;
            this.TrainingTypeBusiness = trainingTypeBusiness;
            this.EducationLevelBusiness = educationLevelBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.ProcessedReportBusiness = processedReportBusiness;
        }
        public ActionResult Index()
        {
            IDictionary<string, object> DistrictsearchInfo = new Dictionary<string, object>();
            GlobalInfo global = new GlobalInfo();
            if (global.IsSuperVisingDeptRole == true)
            {
                DistrictsearchInfo["ProvinceID"] = global.ProvinceID;
                IQueryable<District> lstDistrict = this.DistrictBusiness.Search(DistrictsearchInfo);
                ViewData[MarkStatisticsSecondaryHKConstants.LIST_DISTRICT] = new SelectList(lstDistrict, "DistrictID", "DistrictName");
                ViewData[MarkStatisticsSecondaryHKConstants.DisableDistrict] = false;
            }
            else
            {
                DistrictsearchInfo["DistrictID"] = global.DistrictID;
                IQueryable<District> lstDistrict = this.DistrictBusiness.Search(DistrictsearchInfo);
                ViewData[MarkStatisticsSecondaryHKConstants.LIST_DISTRICT] = new SelectList(lstDistrict, "DistrictID", "DistrictName", global.DistrictID);
                ViewData[MarkStatisticsSecondaryHKConstants.DisableDistrict] = true;
            }

            List<int> lstAca = this.AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value).ToList();
            List<AcademicYear> lstAcademicYear = new List<AcademicYear>();
            for (int i = 0; i < lstAca.Count(); i++)
            {
                AcademicYear AcademicYear = new AcademicYear();
                AcademicYear.Year = (int)lstAca[i];
                AcademicYear.DisplayTitle = lstAca[i] + "-" + (lstAca[i] + 1);
                lstAcademicYear.Add(AcademicYear);
            }
            List<AcademicYear> ListAcademicYear = lstAcademicYear.OrderBy(o => o.Year).ToList();
            ViewData[MarkStatisticsSecondaryHKConstants.LIST_AcademicYear] = new SelectList(ListAcademicYear, "Year", "DisplayTitle");

            ViewData[MarkStatisticsSecondaryHKConstants.LIST_Semester] = new SelectList(CommonList.SemesterAndAll(), "key", "value");

            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.All.Where(o => o.Grade == SystemParamsInFile.EDUCATION_GRADE_SECONDARY).ToList();
            ViewData[MarkStatisticsSecondaryHKConstants.LIST_EducationLevel] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");

            IDictionary<string, object> TrainingTypesearchInfo = new Dictionary<string, object>();
            List<TrainingType> ListTrainingType = TrainingTypeBusiness.Search(TrainingTypesearchInfo).ToList();
            ViewData[MarkStatisticsSecondaryHKConstants.LIST_TrainingType] = new SelectList(ListTrainingType, "TrainingTypeID", "Resolution");

            IDictionary<string, object> MarkStatisticsearchInfo = new Dictionary<string, object>();
            MarkStatisticsearchInfo["Year"] = ListAcademicYear.FirstOrDefault().Year;
            MarkStatisticsearchInfo["EducationLevelID"] = 0;
            MarkStatisticsearchInfo["Semester"] = 1;
            MarkStatisticsearchInfo["TrainingTypeID"] = 0;
            MarkStatisticsearchInfo["ReportCode"] = "ThongKeDiemKiemTraHocKyCap2";
            MarkStatisticsearchInfo["SentToSupervisor"] = true;
            MarkStatisticsearchInfo["SubCommitteeID"] = 0;
            if (global.IsSuperVisingDeptRole == true)
            {
                MarkStatisticsearchInfo["ProvinceID"] = global.ProvinceID;
            }
            else
            {
                MarkStatisticsearchInfo["SupervisingDeptID "] = global.SupervisingDeptID;
            }
            List<SubjectCatBO> ListSubject = MarkStatisticBusiness.SearchSubjectHasReportTHCS(MarkStatisticsearchInfo).ToList();
            ViewData[MarkStatisticsSecondaryHKConstants.LIST_Subject] = new SelectList(ListSubject, "SubjectCatID", "DisplayName");
            ViewData[MarkStatisticsSecondaryHKConstants.LIST_MARKSTATISTICS] = null;
            return View();
        }
        //Tổng hợp
        public ActionResult Search(SearchViewModel frm)
        {
            //string type = JsonReportMessage.NEW;
            //ProcessedReport processedReport = null;
            GlobalInfo global = new GlobalInfo();
            Utils.Utils.TrimObject(frm);
            //Nếu là account cấp Sở: hàm UserInfo.IsSupervisingDeptRole()trả về true            
            if (global.IsSuperVisingDeptRole == true)
            {
                MarkReportBO MarkReport = new MarkReportBO();
                MarkReport.Year = frm.AcademicYearID.Value;
                MarkReport.EducationLevelID = (frm.EducationLevelID == null) ? 0 : frm.EducationLevelID;
                MarkReport.Semester = frm.Semester.Value;
                MarkReport.TrainingTypeID = (frm.TrainingTypeID == null) ? 0 : frm.TrainingTypeID;
                MarkReport.SubcommitteeID = 0;
                MarkReport.SubjectID = frm.SubjectID;
                MarkReport.ProvinceID = global.ProvinceID;
                MarkReport.DistrictID = (frm.DistrictID == null) ? 0 : frm.DistrictID;

                string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(MarkReport);
                string ReportCode = SystemParamsInFile.REPORT_SGD_ThongKeDiemKiemTraHocKy;
                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    return Json(new { ProcessedReportID = 0 });
                }
                else
                {
                    int ProcessedReportID = entity.ProcessedReportID;
                    return Json(new { ProcessedReportID = entity.ProcessedReportID, ReportProcessedDate = entity.ProcessedDate.ToString("dd/MM/yyyy") });
                }


                
            }
            else
            {
                MarkReportBO MarkReport = new MarkReportBO();
                MarkReport.Year = frm.AcademicYearID.Value;
                MarkReport.EducationLevelID = (frm.EducationLevelID == null) ? 0 : frm.EducationLevelID;
                MarkReport.Semester = frm.Semester.Value;
                MarkReport.TrainingTypeID = (frm.TrainingTypeID == null) ? 0 : frm.TrainingTypeID;
                MarkReport.SubcommitteeID = 0;
                MarkReport.SubjectID = frm.SubjectID;
                MarkReport.ProvinceID = global.ProvinceID;
                MarkReport.DistrictID = global.DistrictID;

                string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(MarkReport);
                string ReportCode = SystemParamsInFile.REPORT_PGD_ThongKeDiemKiemTraHocKy;
                ProcessedReport entity = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
                if (entity == null)
                {
                    return Json(new { ProcessedReportID = 0 });
                }
                else
                {
                    int ProcessedReportID = entity.ProcessedReportID;
                    return Json(new { ProcessedReportID = ProcessedReportID });
                }


                
            }
        }

        [ValidateAntiForgeryToken]
        public JsonResult AjaxLoadSubject(int? academicYearID, int? educationLevelID, int? trainingTypeID, int? semester)
        {
            GlobalInfo global = new GlobalInfo();
            IEnumerable<SubjectCatBO> lst = new List<SubjectCatBO>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["Year"] = academicYearID;
            dic["EducationLevelID"] = educationLevelID;
            dic["Semester"] = semester;
            dic["TrainingTypeID"] = trainingTypeID;
            dic["ReportCode"] = "ThongKeDiemKiemTraHocKyCap2";
            dic["SentToSupervisor"] = true;
            dic["SubCommitteeID"] = 0;
            if (global.IsSuperVisingDeptRole == true)
            {
                dic["ProvinceID"] = global.ProvinceID;
            }
            else
            {
                dic["SupervisingDeptID "] = global.SupervisingDeptID;
            }
            lst = MarkStatisticBusiness.SearchSubjectHasReportTHCS(dic).ToList();
            if (lst == null)
                lst = new List<SubjectCatBO>();
            return Json(new SelectList(lst, "SubjectCatID", "DisplayName"));
        }

        public FileResult DownloadReport(int ProcessedReportID)
        {

            GlobalInfo GlobalInfo = new GlobalInfo();
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SupervisingDeptID", GlobalInfo.SupervisingDeptID}
            };
            List<string> listRC = new List<string> { 
                SystemParamsInFile.REPORT_SGD_ThongKeDiemKiemTraHocKy, 
                SystemParamsInFile.REPORT_PGD_ThongKeDiemKiemTraHocKy
            };
            ProcessedReport processedReport = UtilsBusiness.ValidateProcessReport(ProcessedReportID, _globalInfo.SchoolID.Value, _globalInfo.AcademicYearID.Value);
            if (!listRC.Contains(processedReport.ReportCode))
            {
                throw new BusinessException("Common_Error_DataInputError");
            }
            Stream excel = ReportUtils.Decompress(processedReport.ReportData);

            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = processedReport.ReportName;

            return result;
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult LoadGrid(SearchViewModel frm)
        {
            GlobalInfo global = new GlobalInfo();
            Utils.Utils.TrimObject(frm);
            //Nếu là account cấp Sở: hàm UserInfo.IsSupervisingDeptRole()trả về true            
            if (global.IsSuperVisingDeptRole == true)
            {
                MarkReportBO MarkReport = new MarkReportBO();
                MarkReport.Year = frm.AcademicYearID.Value;
                MarkReport.EducationLevelID = (frm.EducationLevelID == null) ? 0 : frm.EducationLevelID;
                MarkReport.Semester = frm.Semester.Value;
                MarkReport.TrainingTypeID = (frm.TrainingTypeID == null) ? 0 : frm.TrainingTypeID;
                MarkReport.SubcommitteeID = 0;
                MarkReport.SubjectID = frm.SubjectID;
                MarkReport.ProvinceID = global.ProvinceID;
                MarkReport.DistrictID = (frm.DistrictID == null) ? 0 : frm.DistrictID;                
               
                List<MarkStatisticsOfProvince23> lstMarkStatisticOfProvinceSecondary = new List<MarkStatisticsOfProvince23>();

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ReportCode"] = "ThongKeDiemKiemTraHocKyCap2";
                dic["Year"] = MarkReport.Year;
                dic["EducationLevelID"] = MarkReport.EducationLevelID;
                dic["Semester"] = MarkReport.Semester;
                dic["TrainingTypeID"] = MarkReport.TrainingTypeID;
                dic["SentToSupervisor"] = true;
                dic["SubCommitteeID"] = 0;
                dic["ProvinceID"] = MarkReport.ProvinceID;
                dic["DistrictID"] = MarkReport.DistrictID;
                dic["SubjectID"] = MarkReport.SubjectID;
                dic["SupervisingDeptID"] = global.SupervisingDeptID;
                string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(MarkReport);

                int FileID;
                lstMarkStatisticOfProvinceSecondary = this.MarkStatisticBusiness.CreateSGDSemesterMarkStatisticsSecondary(dic, InputParameterHashKey, out FileID);

                IEnumerable<MarkStatisticsViewModel> lst = lstMarkStatisticOfProvinceSecondary.Select(o => new MarkStatisticsViewModel
                {
                    SchoolName = o.SchoolName,
                    SchoolID = o.SchoolID,
                    SentDate = o.SentDate,
                    TotalSchool = o.TotalSchool,
                    M00 = o.M00,
                    M03 = o.M03,
                    M05 = o.M05,
                    M08 = o.M08,
                    M10 = o.M10,
                    M13 = o.M13,
                    M15 = o.M15,
                    M18 = o.M18,
                    M20 = o.M20,
                    M23 = o.M23,
                    M25 = o.M25,
                    M28 = o.M28,
                    M30 = o.M30,
                    M33 = o.M33,
                    M35 = o.M35,
                    M38 = o.M38,
                    M40 = o.M40,
                    M43 = o.M43,
                    M45 = o.M45,
                    M48 = o.M48,
                    M50 = o.M50,
                    M53 = o.M53,
                    M55 = o.M55,
                    M58 = o.M58,
                    M60 = o.M60,
                    M63 = o.M63,
                    M65 = o.M65,
                    M68 = o.M68,
                    M70 = o.M70,
                    M73 = o.M73,
                    M75 = o.M75,
                    M78 = o.M78,
                    M80 = o.M80,
                    M83 = o.M83,
                    M85 = o.M85,
                    M88 = o.M88,
                    M90 = o.M90,
                    M93 = o.M93,
                    M95 = o.M95,
                    M98 = o.M98,
                    M100 = o.M100,
                    TotalTest = o.TotalTest,
                    TotalBelowAverage = o.TotalBelowAverage,
                    PercentBelowAverage = o.PercentBelowAverage,
                    TotalAboveAverage = o.TotalAboveAverage,
                    PercentAboveAverage = o.PercentAboveAverage
                });
                ViewData[MarkStatisticsSecondaryHKConstants.LIST_MARKSTATISTICS] = lst.ToList();
                ViewData[MarkStatisticsSecondaryHKConstants.GRID_REPORT_ID] = FileID;
            }
            else
            {
                MarkReportBO MarkReport = new MarkReportBO();
                MarkReport.Year = frm.AcademicYearID.Value;
                MarkReport.EducationLevelID = (frm.EducationLevelID == null) ? 0 : frm.EducationLevelID;
                MarkReport.Semester = frm.Semester.Value;
                MarkReport.TrainingTypeID = (frm.TrainingTypeID == null) ? 0 : frm.TrainingTypeID;
                MarkReport.SubcommitteeID = 0;
                MarkReport.SubjectID = frm.SubjectID;
                MarkReport.ProvinceID = global.ProvinceID;
                MarkReport.DistrictID = global.DistrictID;


                List<MarkStatisticsOfProvince23> lstMarkStatisticsOfSchool23 = new List<MarkStatisticsOfProvince23>();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ReportCode"] = "ThongKeDiemKiemTraHocKyCap2";
                dic["Year"] = MarkReport.Year;
                dic["EducationLevelID"] = MarkReport.EducationLevelID;
                dic["Semester"] = MarkReport.Semester;
                dic["TrainingTypeID"] = MarkReport.TrainingTypeID;
                dic["SentToSupervisor"] = true;
                dic["SubCommitteeID"] = 0;
                dic["ProvinceID"] = MarkReport.ProvinceID;
                dic["DistrictID"] = MarkReport.DistrictID;
                dic["SubjectID"] = MarkReport.SubjectID;
                dic["SupervisingDeptID"] = global.SupervisingDeptID;

                string InputParameterHashKey = MarkStatisticBusiness.GetHashKeyForMarkStatistics(MarkReport);
                int FileID;
                lstMarkStatisticsOfSchool23 = this.MarkStatisticBusiness.CreatePGDSemesterMarkStatisticsSecondary(dic, InputParameterHashKey, out FileID);
                IEnumerable<MarkStatisticsViewModel> lst = lstMarkStatisticsOfSchool23.Select(o => new MarkStatisticsViewModel
                {
                    SchoolName = o.SchoolName,
                    SchoolID = o.SchoolID,
                    SentDate = o.SentDate,
                    TotalSchool = o.TotalSchool,
                    M00 = o.M00,
                    M03 = o.M03,
                    M05 = o.M05,
                    M08 = o.M08,
                    M10 = o.M10,
                    M13 = o.M13,
                    M15 = o.M15,
                    M18 = o.M18,
                    M20 = o.M20,
                    M23 = o.M23,
                    M25 = o.M25,
                    M28 = o.M28,
                    M30 = o.M30,
                    M33 = o.M33,
                    M35 = o.M35,
                    M38 = o.M38,
                    M40 = o.M40,
                    M43 = o.M43,
                    M45 = o.M45,
                    M48 = o.M48,
                    M50 = o.M50,
                    M53 = o.M53,
                    M55 = o.M55,
                    M58 = o.M58,
                    M60 = o.M60,
                    M63 = o.M63,
                    M65 = o.M65,
                    M68 = o.M68,
                    M70 = o.M70,
                    M73 = o.M73,
                    M75 = o.M75,
                    M78 = o.M78,
                    M80 = o.M80,
                    M83 = o.M83,
                    M85 = o.M85,
                    M88 = o.M88,
                    M90 = o.M90,
                    M93 = o.M93,
                    M95 = o.M95,
                    M98 = o.M98,
                    M100 = o.M100,
                    TotalTest = o.TotalTest,
                    TotalBelowAverage = o.TotalBelowAverage,
                    PercentBelowAverage = o.PercentBelowAverage,
                    TotalAboveAverage = o.TotalAboveAverage,
                    PercentAboveAverage = o.PercentAboveAverage
                });
                ViewData[MarkStatisticsSecondaryHKConstants.LIST_MARKSTATISTICS] = lst.ToList();
                ViewData[MarkStatisticsSecondaryHKConstants.GRID_REPORT_ID] = FileID;     
            }
            return PartialView("List");
        }
    }
}
