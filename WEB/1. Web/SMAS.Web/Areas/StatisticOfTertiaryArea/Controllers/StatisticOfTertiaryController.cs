﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Areas.StatisticOfTertiaryArea.Models;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Utils;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Web.Areas.StatisticOfTertiaryArea.Controllers
{
    public class StatisticOfTertiaryController : Controller
    {
        IDistrictBusiness DistrictBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        IStatisticOfTertiaryBusiness StatisticOfTertiaryBusiness;
        IReportDefinitionBusiness ReportDefinitionBusiness;
        ISupervisingDeptBusiness SupervisingDeptBusiness;
        public StatisticOfTertiaryController(IDistrictBusiness districtBusiness, IAcademicYearBusiness academicYearBusiness,
            IStatisticOfTertiaryBusiness statisticOfTertiaryBusiness, IReportDefinitionBusiness reportDefinitionBusiness,
            ISupervisingDeptBusiness supervisingDeptBusiness)
        {
            this.DistrictBusiness = districtBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.StatisticOfTertiaryBusiness = statisticOfTertiaryBusiness;
            this.ReportDefinitionBusiness = reportDefinitionBusiness;
            this.SupervisingDeptBusiness = supervisingDeptBusiness;
        }

        //
        // GET: /StatisticOfTertiaryArea/StatisticOfTertiary/

        public ActionResult Index()
        {
            GlobalInfo global = new GlobalInfo();
            /*
                cboDistrict: DistrictBusiness.Search(dic) với
                dic[“ProvinceID”] = UserInfo.ProvinceID
             */
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ProvinceID"] = global.ProvinceID;
            List<District> listDistrict = DistrictBusiness.Search(dic).ToList();

            /*
             - cboAcademicYear: List<int> AcademicYearBusiness.GetListYearForSupervisingDept(sd.SupervisingDeptID),
             * với mỗi phần tử a trong List thì tương ứng 1 option a : a – a+1
             */
            List<int> listAcademicYear = AcademicYearBusiness.GetListYearForSupervisingDept(global.SupervisingDeptID.Value);
            Dictionary<int, string> listAca = new Dictionary<int, string>();
            if (listAcademicYear != null && listAcademicYear.Count > 0)
            {
                foreach (var aca in listAcademicYear)
                {
                    listAca.Add(aca, string.Format("{0} - {1}", aca.ToString(), (aca + 1).ToString()));
                }
            }
            /*
             - cboStatus:
                0: Tất cả. Mặc định
                1: Chưa báo cáo
                2: Đã báo cáo
             */
            Dictionary<int, string> listStatus = new Dictionary<int, string>();
            listStatus.Add(1, "Chưa báo cáo");
            listStatus.Add(2, "Đã báo cáo");
            ViewData[StatisticOfTertiaryConstants.LIST_DISTRICT] = new SelectList(listDistrict, "DistrictID", "DistrictName");
            ViewData[StatisticOfTertiaryConstants.LIST_ACADEMICYEAR] = new SelectList(listAca, "Key", "Value");
            ViewData[StatisticOfTertiaryConstants.LIST_STATUS] = new SelectList(listStatus, "Key", "Value");
            ViewData["_DistrictID"] = "";
            ViewData["_AcademicYearID"] = "";
            ViewData["_Status"] = "";
            ViewData["EnableBtnExportListSchool"] = false;
            ViewData["EnableBtnExportProvince"] = false;
            return View();
        }


        public PartialViewResult Search(SearchViewModel frm)
        {
            GlobalInfo global = new GlobalInfo();
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            /*
                Gọi hàm StatisticOfTertiaryBusiness.GetListSchoolReported(UserInfo.ProvinceID, cboAcademicYear.Value)
                Lấy kết quả thu được Load lên Grid
                Bổ sung trường ẩn AcademicYearID cho mỗi hàng
             */
            int Year = frm.AcademicYearID.HasValue ? frm.AcademicYearID.Value : 0;
            int districtID = frm.DistrictID.HasValue ? frm.DistrictID.Value : 0;
            List<StatisticOfTertiaryBO> list = StatisticOfTertiaryBusiness.GetListSchoolReported(global.ProvinceID.Value, Year, districtID, frm.Status).ToList();
            ViewData[StatisticOfTertiaryConstants.LIST_STATISTICOFTERTIARYBO] = list;
            ViewData["_DistrictID"] = districtID;
            ViewData["_AcademicYearID"] = Year;
            ViewData["_Status"] = frm.Status;
            ViewData["EnableBtnExportListSchool"] = list.Count > 0 ? true : false;
            ViewData["EnableBtnExportProvince"] = list.Count > 0 && list.Any(o => o.StatusResolution == "Đã báo cáo") ? true : false;
            return PartialView("_List");
        }

        [HttpGet]
        public FileResult Detail(int? schoolID, int? year)
        {
            GlobalInfo global = new GlobalInfo();
            int ProvinceID = global.ProvinceID.Value;
            int SuperVisingDeptID = global.SupervisingDeptID.Value;

            AcademicYear aca = AcademicYearBusiness.SearchBySchool(schoolID.Value, new Dictionary<string, object> { { "Year", year } }).FirstOrDefault();
            string filename = "";
            Stream excel = StatisticOfTertiaryBusiness.CreateSchoolReport(aca.AcademicYearID, out filename);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string ReportName = filename;
            result.FileDownloadName = ReportName;
            return result;
        }

        [HttpPost]
        public FileResult ExportFileProvinceReport(FormCollection form)
        {
            GlobalInfo global = new GlobalInfo();
            int ProvinceID = global.ProvinceID.Value;
            int SuperVisingDeptID = global.SupervisingDeptID.Value;
            int DistrictID = SMAS.Business.Common.Utils.GetInt(form["_DistrictID"]);
            int AcademicYearID = SMAS.Business.Common.Utils.GetInt(form["_AcademicYearID"]);

            Dictionary<string, object> dic = new Dictionary<string, object>();
            string filename = "";
            Stream excel = StatisticOfTertiaryBusiness.CreateProvinceReport(SuperVisingDeptID, ProvinceID, AcademicYearID, out filename);
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            string ReportName = filename;
            result.FileDownloadName = ReportName;
            return result;
        }

        [HttpPost]
        public FileResult ExportListSchool(FormCollection form)
        {
            string reportName = string.Empty;
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.SOGD_THPT_DS);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet dataSheet = oBook.GetSheet(1);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            /*
                Gọi hàm StatisticOfTertiaryBusiness.GetListSchoolReported(UserInfo.ProvinceID, cboAcademicYear.Value)
                Lấy kết quả thu được Load lên Grid
                Bổ sung trường ẩn AcademicYearID cho mỗi hàng
             */
            GlobalInfo global = new GlobalInfo();
            int DistrictID = SMAS.Business.Common.Utils.GetInt(form["districtID"]);
            int AcademicYearID = SMAS.Business.Common.Utils.GetInt(form["academicYearID"]);
            int Status = !string.IsNullOrWhiteSpace(form["status"]) ? SMAS.Business.Common.Utils.GetInt(form["status"]) : 0;
            List<StatisticOfTertiaryBO> listStatisticOfTertiary = StatisticOfTertiaryBusiness.GetListSchoolReported(global.ProvinceID.Value, AcademicYearID, DistrictID, Status > 0 ? (int?)Status : null).ToList();
            IVTRange Row_Rank = dataSheet.GetRange(8, 1, 8, 6);
            int row_index = 8;
            int stt = 1;
            if (listStatisticOfTertiary != null && listStatisticOfTertiary.Count > 0)
            {
                foreach (var sot in listStatisticOfTertiary)
                {
                    //coppy row
                    dataSheet.CopyPaste(Row_Rank, row_index, 1, true);
                    //fill data
                    dataSheet.SetCellValue(row_index, 1, stt++);
                    dataSheet.SetCellValue(row_index, 2, sot.SchoolName);
                    dataSheet.SetCellValue(row_index, 3, sot.TrainingTypeResolution);
                    dataSheet.SetCellValue(row_index, 4, sot.DistrictName);
                    dataSheet.SetCellValue(row_index, 5, sot.StatusResolution);
                    row_index++;
                }
            }
            IVTWorksheet tempSheet = oBook.GetSheet(2);
            IVTRange NguoiBaoCao_Range = tempSheet.GetRange("A4", "A4");
            dataSheet.CopyPaste(NguoiBaoCao_Range, row_index + 1, 5, false);
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(global.SupervisingDeptID);
            dicVarable["SupervisingDept"] = supervisingDept.SupervisingDeptName;
            dicVarable["AcademicYear"] = AcademicYearID + " - " + (AcademicYearID + 1).ToString();
            dataSheet.FillVariableValue(dicVarable);

            string ReportName = "SoGD_THPT_DS_Truong_Bao_Cao_So_Lieu.xls";
            oBook.GetSheet(2).Delete();
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = ReportName;
            return result;
        }
    }
}
