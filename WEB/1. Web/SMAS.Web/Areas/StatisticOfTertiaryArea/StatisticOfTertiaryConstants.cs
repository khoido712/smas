﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.StatisticOfTertiaryArea
{
    public class StatisticOfTertiaryConstants
    {
        public const string LIST_DISTRICT = "list_district";
        public const string LIST_ACADEMICYEAR = "list_AcademicYear";
        public const string LIST_STATUS = "list_Status";
        public const string LIST_STATISTICOFTERTIARYBO = "list_Statisticoftertiarybo";
    }
}