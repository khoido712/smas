﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticOfTertiaryArea
{
    public class StatisticOfTertiaryAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "StatisticOfTertiaryArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "StatisticOfTertiaryArea_default",
                "StatisticOfTertiaryArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
