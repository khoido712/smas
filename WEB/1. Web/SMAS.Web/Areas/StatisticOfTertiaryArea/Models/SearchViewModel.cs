﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SMAS.Web.Areas.StatisticOfTertiaryArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("StatisticOfTertiary_Label_SchoolName")]
        public string SchoolName { get; set; }

        [ResourceDisplayName("StatisticOfTertiary_Label_District")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticOfTertiaryConstants.LIST_DISTRICT)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public Nullable<int> DistrictID { get; set; }

        [ResourceDisplayName("StatisticOfTertiary_Label_AcademicYear")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticOfTertiaryConstants.LIST_ACADEMICYEAR)]
        [AdditionalMetadata("PlaceHolder", "null")]
        public Nullable<int> AcademicYearID { get; set; }

        [ResourceDisplayName("StatisticOfTertiary_Label_Status")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", StatisticOfTertiaryConstants.LIST_STATUS)]
        [AdditionalMetadata("PlaceHolder", "All")]
        public Nullable<int> Status { get; set; }
    }
}