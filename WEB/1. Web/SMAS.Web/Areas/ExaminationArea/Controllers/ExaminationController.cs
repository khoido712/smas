﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Areas.ExaminationArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using SMAS.Web.Filter;
using Telerik.Web.Mvc;

namespace SMAS.Web.Areas.ExaminationArea.Controllers
{
    public class ExaminationController : BaseController
    {
        private readonly IExaminationBusiness ExaminationBusiness;
        private readonly IExaminationSubjectBusiness ExaminationSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private static int total = 0;


        public ExaminationController(IExaminationBusiness examinationBusiness
            , IExaminationSubjectBusiness ExaminationSubjectBusiness
            , IEducationLevelBusiness EducationLevelBusiness
            , ISubjectCatBusiness SubjectCatBusiness)
        {
            this.ExaminationBusiness = examinationBusiness;
            this.ExaminationSubjectBusiness = ExaminationSubjectBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.SubjectCatBusiness = SubjectCatBusiness;
        }

        //
        // GET: /Examination/

        public ActionResult Index()
        {
            SetViewData();

            GlobalInfo gi = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = gi.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = gi.AppliedLevel.Value;

            //IEnumerable<ExaminationViewModel> lst = this._Search(SearchInfo);
            //ViewData[ExaminationConstants.LIST_EXAMINATION] = lst.Count() > 0 ? lst : new List<ExaminationViewModel>();

            //Nếu UserInfo.IsCurrentYear = FALSE: disable các nút thêm, sửa , xoá
            ViewData[ExaminationConstants.ENABLE_BUTTON_ADD_EDIT_DELETE] = gi.IsCurrentYear;
            return View();
        }

        //
        // GET: /Examination/Search

        [GridAction(EnableCustomBinding = true)]
        public ActionResult Search(SearchViewModel frm, GridCommand command, int currentPage = 1, bool IsReload = false,int RowInPage = 0)
        {
            Utils.Utils.TrimObject(frm);
            GlobalInfo gi = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            int Page = 1;
            //add search info
            SearchInfo["AcademicYearID"] = gi.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = gi.AppliedLevel.Value;

            string Member = command.SortDescriptors.Count > 0 ? command.SortDescriptors[0].Member : "";
            string SortDirection = command.SortDescriptors.Count > 0 ? command.SortDescriptors[0].SortDirection.ToString() : "Descending";

            IQueryable<ExaminationViewModel> lst = this._Search(SearchInfo).OrderByDescending(p => p.ToDate);
            lst = SortGrid(lst,true, Member, SortDirection);
            if (command.Page > 0)
            {
                Page = command.Page;
            }
            //dung khi load lai trang cho chuc nang xoa,sua
            if (IsReload)
            {
                if (currentPage > 1)
                {
                    if (RowInPage == 1)
                    {
                        Page = currentPage - 1;
                    }
                    else
                    {
                        Page = currentPage;
                    }
                }
                else
                {
                    Page = 1;
                }                
            }
            // neu page hien tai la page 1 va 1 record duoc xoa thi dat page = 1;
            if (Page == 0)
            {
                Page = 1;
            }
            List<ExaminationViewModel> result = lst.Skip((Page - 1) * ExaminationConstants.PAGE_SIZE).Take(ExaminationConstants.PAGE_SIZE).ToList();
            foreach (var item in result)
            {
                switch (item.CurrentStage)
                {
                    case SystemParamsInFile.EXAMINATION_STAGE_CREATED:
                        item.StatusName = Res.Get("Examination_Label_CurrentStage_Create");
                        break;

                    case SystemParamsInFile.EXAMINATION_STAGE_CANDIDATE_LISTED:
                        item.StatusName = Res.Get("Examination_Label_CurrentStage_Candidate_Listed");
                        break;

                    case SystemParamsInFile.EXAMINATION_STAGE_ROOM_ASSIGNED:
                        item.StatusName = Res.Get("Examination_Label_CurrentStage_Room_Assigned");
                        break;

                    case SystemParamsInFile.EXAMINATION_STAGE_INVILIGATOR_ASSIGNED:
                        item.StatusName = Res.Get("Examination_Label_CurrentStage_Inviligator_Assigned");
                        break;

                    case SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED:
                        item.StatusName = Res.Get("Examination_Label_CurrentStage_Head_Attached");
                        break;

                    case SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED:
                        item.StatusName = Res.Get("Examination_Label_CurrentStage_Mark_Complete");
                        break;

                    case SystemParamsInFile.EXAMINATION_STAGE_FINISHED:
                        item.StatusName = Res.Get("Examination_Label_CurrentStage_Finish");
                        break;

                    default:
                        break;
                }

                item.bUsingSeparateList = item.UsingSeparateList == 1 ? true : false;
                item.bCandidateFromMultipleLevel = item.CandidateFromMultipleLevel == 1 ? true : false;
            }

            ViewData[ExaminationConstants.LIST_EXAMINATION] = result;
            //Nếu UserInfo.IsCurrentYear = FALSE: disable các nút thêm, sửa , xoá
            ViewData[ExaminationConstants.ENABLE_BUTTON_ADD_EDIT_DELETE] = gi.IsCurrentYear;
            ViewData[ExaminationConstants.PAGE] = Page;
            //Get view data here
            if (command.Page == 0)
            {
                ViewData[ExaminationConstants.Total] = lst.Count();
                return PartialView("_List");
            }
            else
            {
            return View(new GridModel<ExaminationViewModel>()
            {
                Data = result,
                    Total = lst.Count(),
            });
        }
        }

        private IQueryable<ExaminationViewModel> SortGrid(IQueryable<ExaminationViewModel> lst,bool sort, string Member, string SortDirection)
        {

            if (string.IsNullOrEmpty(SortDirection))
            {
                if (sort)
                {
                    lst = lst.OrderByDescending(p => p.ToDate);
                }
            }
            else
            {
                if (Member.Equals("StatusName"))
                {
                    if (SortDirection.Equals("Descending"))
                    {
                        lst = lst.OrderByDescending(p => p.CurrentStage);
                    }
                    else
                    {
                        lst = lst.OrderBy(p => p.CurrentStage);
                    }
                }
                else if (Member.Equals("ToDate"))
                {
                    if (SortDirection.Equals("Descending"))
                    {
                        lst = lst.OrderByDescending(p => p.ToDate);
                    }
                    else
                    {
                        lst = lst.OrderBy(p => p.ToDate);
                    }
                }
                else if (Member.Equals("FromDate"))
                {
                    if (SortDirection.Equals("Descending"))
                    {
                        lst = lst.OrderByDescending(p => p.FromDate);
                    }
                    else
                    {
                        lst = lst.OrderBy(p => p.FromDate);
                    }
                }
            }
            return lst;
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            Examination examination = new Examination();
            ExaminationViewModel evm = new ExaminationViewModel();
            GlobalInfo gi = new GlobalInfo();

            TryUpdateModel(evm);
            Utils.Utils.BindTo(evm, examination);
            Utils.Utils.TrimObject(examination);

            //them
            examination.UsingSeparateList = (int)(evm.bUsingSeparateList ? 1 : 0);
            examination.CandidateFromMultipleLevel = (int)(evm.bCandidateFromMultipleLevel ? 1 : 0);

            examination.AcademicYearID = gi.AcademicYearID.Value;
            examination.SchoolID = gi.SchoolID.Value;
            examination.AppliedLevel = (int)gi.AppliedLevel.Value;
            examination.Description = examination.Description != null ? examination.Description.Trim() : "";
            this.ExaminationBusiness.Insert(examination);
            this.ExaminationBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int ExaminationID)
        {
            Examination examination = this.ExaminationBusiness.Find(ExaminationID);
            CheckPermissionForAction(ExaminationID, "Examination");
            ExaminationViewModel evm = new ExaminationViewModel();
            GlobalInfo gi = new GlobalInfo();

            TryUpdateModel(evm);


            //update nho le,dung binto la mat du lieu
            examination.Title = evm.Title;
            examination.FromDate = evm.FromDate;
            examination.ToDate = evm.ToDate;
            examination.Location = evm.Location;
            examination.Description = evm.Description;
            //Neu checkbox danh sach thi sinh cho tung mon khac nhau khong 
            if (!ExaminationBusiness.HasSeparatedCandidateInSubject(ExaminationID))
            {
                examination.UsingSeparateList = (int)(evm.bUsingSeparateList ? 1 : 0);
            }
            if (!ExaminationBusiness.HasCandidateFromMultipleLevel(ExaminationID))
            {
                examination.CandidateFromMultipleLevel = (int)(evm.bCandidateFromMultipleLevel ? 1 : 0);
            }
            examination.MarkImportType = evm.MarkImportType;
            examination.Description = examination.Description != null ? examination.Description.Trim() : "";

            this.ExaminationBusiness.Update(examination);
            this.ExaminationBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            //Examination examination = this.ExaminationBusiness.Find(ExaminationID);
            CheckPermissionForAction(id, "Examination");
            GlobalInfo gi = new GlobalInfo();
            this.ExaminationBusiness.Delete(gi.SchoolID.Value, (int)gi.AppliedLevel.Value, id);
            this.ExaminationBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IQueryable<ExaminationViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            GlobalInfo gi = new GlobalInfo();
            IQueryable<Examination> query = this.ExaminationBusiness.SearchBySchool(gi.SchoolID.Value, SearchInfo);
            IQueryable<ExaminationViewModel> lst = query.Select(o => new ExaminationViewModel
            {
                ExaminationID = o.ExaminationID,
                AcademicYearID = o.AcademicYearID,
                SchoolID = o.SchoolID,
                Year = o.Year,
                Title = o.Title,
                Location = o.Location,
                Description = o.Description,
                CurrentStage = o.CurrentStage,
                FromDate = o.FromDate,
                ToDate = o.ToDate,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate,
                UsingSeparateList = o.UsingSeparateList,
                CandidateFromMultipleLevel = o.CandidateFromMultipleLevel,
                MarkImportType = o.MarkImportType,
                AppliedLevel = o.AppliedLevel
            });
            // List<ExaminationViewModel> lsEVM = lst.ToList();

            // lsEVM = lsEVM.OrderByDescending(o => o.ToDate).ToList();
            return lst;
        }

        #region SetViewData

        private void SetViewData()
        {
            ViewData[ExaminationConstants.ENABLE_USINGSEPARATELIST] = true;
            ViewData[ExaminationConstants.ENABLE_CANDIDATEFROMMULTIPLELEVEL] = true;

            //ViewData[ExaminationConstants.LISTMARKIMPORTTYPE]
            List<ViettelCheckboxList> listMarkImportType = new List<ViettelCheckboxList>();
            listMarkImportType.Add(new ViettelCheckboxList(Res.Get("Examination_Label_mark_import_type_order_number"), SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER, true, false));
            listMarkImportType.Add(new ViettelCheckboxList(Res.Get("Examination_Label_mark_import_type_pupilcode"), SystemParamsInFile.MARK_IMPORT_TYPE_PUPILCODE, false, false));
            listMarkImportType.Add(new ViettelCheckboxList(Res.Get("Examination_Label_mark_import_type_detachable_head"), SystemParamsInFile.MARK_IMPORT_TYPE_DETACHABLE_HEAD, false, false));
            ViewData[ExaminationConstants.LISTMARKIMPORTTYPE] = listMarkImportType;
            GlobalInfo gi = new GlobalInfo();

            //Nếu UserInfo.IsCurrentYear = FALSE: disable các nút thêm, sửa , xoá
            ViewData[ExaminationConstants.ENABLE_BUTTON_ADD_EDIT_DELETE] = gi.IsCurrentYear;
        }

        #endregion SetViewData

        #region Get Edit

        [HttpGet]
        public ActionResult GetEdit(int ExaminationID)
        {
            Examination exam = ExaminationBusiness.Find(ExaminationID);
            ExaminationViewModel evm = new ExaminationViewModel();
            Utils.Utils.BindTo(exam, evm);
            evm.bCandidateFromMultipleLevel = (exam.CandidateFromMultipleLevel == 1) ? true : false;
            evm.bUsingSeparateList = (exam.UsingSeparateList == 1) ? true : false;

            bool order_number = false;
            bool pupil_code = false;
            bool detachable = false;
            switch (exam.MarkImportType)
            {
                case SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER:
                    order_number = true;
                    pupil_code = false;
                    detachable = false;
                    break;

                case SystemParamsInFile.MARK_IMPORT_TYPE_PUPILCODE:
                    order_number = false;
                    pupil_code = true;
                    detachable = false;
                    break;

                case SystemParamsInFile.MARK_IMPORT_TYPE_DETACHABLE_HEAD:
                    order_number = false;
                    pupil_code = false;
                    detachable = true;
                    break;

                default:
                    break;
            }

            // ViewData[ExaminationConstants.LISTMARKIMPORTTYPE]
            List<ViettelCheckboxList> listMarkImportType = new List<ViettelCheckboxList>();
            listMarkImportType.Add(new ViettelCheckboxList(Res.Get("Examination_Label_mark_import_type_order_number"), SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER, order_number, false));
            listMarkImportType.Add(new ViettelCheckboxList(Res.Get("Examination_Label_mark_import_type_pupilcode"), SystemParamsInFile.MARK_IMPORT_TYPE_PUPILCODE, pupil_code, false));
            listMarkImportType.Add(new ViettelCheckboxList(Res.Get("Examination_Label_mark_import_type_detachable_head"), SystemParamsInFile.MARK_IMPORT_TYPE_DETACHABLE_HEAD, detachable, false));
            ViewData[ExaminationConstants.LISTMARKIMPORTTYPE] = listMarkImportType;

            //ExaminationBusiness.HasSeparatedCandidateInSubject(ExaminationID) = TRUE thì disable chk
            if (ExaminationBusiness.HasSeparatedCandidateInSubject(ExaminationID))
            {
                ViewData[ExaminationConstants.ENABLE_USINGSEPARATELIST] = false;
            }
            else
            {
                ViewData[ExaminationConstants.ENABLE_USINGSEPARATELIST] = true;
            }

            //ExaminationBusiness.HasCandidateFromMultipleLevel(ExaminationID) = TRUE thì disable chkCandidateFromMultipleLevel
            if (ExaminationBusiness.HasCandidateFromMultipleLevel(ExaminationID))
            {
                ViewData[ExaminationConstants.ENABLE_CANDIDATEFROMMULTIPLELEVEL] = false;
            }
            else
            {
                ViewData[ExaminationConstants.ENABLE_CANDIDATEFROMMULTIPLELEVEL] = true;
            }

            return View("_Edit", evm);
        }

        #endregion Get Edit

        #region Detail


        public ActionResult GetDetail(int ExaminationID)
        {
            Examination exam = ExaminationBusiness.Find(ExaminationID);

            ExaminationViewModel evm = new ExaminationViewModel();
            Utils.Utils.BindTo(exam, evm);

            switch (exam.MarkImportType)
            {
                case SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER: evm.MarkImportTypeName = Res.Get("Examination_Label_mark_import_type_order_number"); break;
                case SystemParamsInFile.MARK_IMPORT_TYPE_PUPILCODE: evm.MarkImportTypeName = Res.Get("Examination_Label_mark_import_type_pupilcode"); break;
                case SystemParamsInFile.MARK_IMPORT_TYPE_DETACHABLE_HEAD: evm.MarkImportTypeName = Res.Get("Examination_Label_mark_import_type_detachable_head"); break;
            }

            switch (exam.CurrentStage)
            {
                case SystemParamsInFile.EXAMINATION_STAGE_CREATED:
                    evm.StatusName = Res.Get("Examination_Label_CurrentStage_Create");
                    break;

                case SystemParamsInFile.EXAMINATION_STAGE_CANDIDATE_LISTED:
                    evm.StatusName = Res.Get("Examination_Label_CurrentStage_Candidate_Listed");
                    break;

                case SystemParamsInFile.EXAMINATION_STAGE_ROOM_ASSIGNED:
                    evm.StatusName = Res.Get("Examination_Label_CurrentStage_Room_Assigned");
                    break;

                case SystemParamsInFile.EXAMINATION_STAGE_INVILIGATOR_ASSIGNED:
                    evm.StatusName = Res.Get("Examination_Label_CurrentStage_Inviligator_Assigned");
                    break;

                case SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED:
                    evm.StatusName = Res.Get("Examination_Label_CurrentStage_Head_Attached");
                    break;

                case SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED:
                    evm.StatusName = Res.Get("Examination_Label_CurrentStage_Mark_Complete");
                    break;

                case SystemParamsInFile.EXAMINATION_STAGE_FINISHED:
                    evm.StatusName = Res.Get("Examination_Label_CurrentStage_Finish");
                    break;

                default:
                    break;
            }
            evm.bCandidateFromMultipleLevel = (exam.CandidateFromMultipleLevel == 1) ? true : false;
            evm.bUsingSeparateList = (exam.UsingSeparateList == 1) ? true : false;
            string YearTitle = evm.Year.ToString() + " - " + (evm.Year + 1).ToString();
            evm.YearTitle = YearTitle;

            GlobalInfo gi = new GlobalInfo();
            IQueryable<ExaminationSubject> lsES = ExaminationSubjectBusiness.SearchBySchool(gi.SchoolID.Value, new Dictionary<string, object>()
            {
                {"ExaminationID",ExaminationID},
                {"AcademicYearID",gi.AcademicYearID.Value},
                {"Year",exam.Year}
            });
            List<ExaminationSubject> listES = lsES.ToList();
            List<ExaminationSubjectVM> lsESVM = new List<ExaminationSubjectVM>();
            foreach (var item in listES)
            {
                ExaminationSubjectVM esvm = new ExaminationSubjectVM();
                Utils.Utils.BindTo(item, esvm);

                EducationLevel el = EducationLevelBusiness.Find(item.EducationLevelID);
                esvm.EducationLevelName = el.Resolution;

                SubjectCat sc = SubjectCatBusiness.Find(item.SubjectID);
                esvm.SubjectName = sc.DisplayName;
                esvm.OrderInSubject = sc.OrderInSubject.HasValue ? sc.OrderInSubject.Value : 0;

                lsESVM.Add(esvm);
            }
            if (lsESVM != null && lsESVM.Count > 0)
            {
                ViewData[ExaminationConstants.LIST_EXAMINATIONSUBJECT] = lsESVM.OrderBy(p => p.EducationLevelID).ThenBy(p=>p.OrderInSubject).ThenBy(p=>p.SubjectName).ToList();
            }
            else
            {
            ViewData[ExaminationConstants.LIST_EXAMINATIONSUBJECT] = lsESVM;
            }
            ViewData[ExaminationConstants.ENABLE_BUTTON_ADD_EDIT_DELETE] = gi.IsCurrentYear && (evm.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_CREATED);
            return View("Detail", evm);
        }

        #endregion Detail

        #region ReloadGrid

        public ActionResult ReloadGrid()
        {
            GlobalInfo gi = new GlobalInfo();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //add search info
            SearchInfo["AcademicYearID"] = gi.AcademicYearID.Value;
            SearchInfo["AppliedLevel"] = gi.AppliedLevel.Value;

            //Nếu UserInfo.IsCurrentYear = FALSE: disable các nút thêm, sửa , xoá
            ViewData[ExaminationConstants.ENABLE_BUTTON_ADD_EDIT_DELETE] = gi.IsCurrentYear;
            IEnumerable<ExaminationViewModel> lst = this._Search(SearchInfo);
            ViewData[ExaminationConstants.LIST_EXAMINATION] = lst;
            return View("_List");
        }

        #endregion ReloadGrid

        #region SetExaminationStage

        public void SetExaminationStageViewData()
        {
            //viewdata
            GlobalInfo gi = new GlobalInfo();
            IQueryable<Examination> lsE = ExaminationBusiness.SearchBySchool(gi.SchoolID.Value, new Dictionary<string, object>()
            {
                {"AcademicYearID",gi.AcademicYearID.Value},
                {"AppliedLevel",gi.AppliedLevel.Value}
            }).OrderByDescending(o => o.ToDate);

            ViewData[ExaminationConstants.LIST_EXAMINATIONCHOICE] = new SelectList(lsE.ToList(), "ExaminationID", "Title");
            ViewData[ExaminationConstants.ENABLE_RADIOBUTTON] = false;
        }

        public ActionResult GetExaminationStage()
        {
            SetExaminationStageViewData();
            GlobalInfo global = new GlobalInfo();

            //LISTEXAMINATIONSTAGE
            List<ViettelCheckboxList> listExaminationStage = new List<ViettelCheckboxList>();
            listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_CREATED"), SystemParamsInFile.EXAMINATION_STAGE_CREATED, true, false));
            listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_CANDIDATE_LISTED"), SystemParamsInFile.EXAMINATION_STAGE_CANDIDATE_LISTED, false, false));
            listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_ROOM_ASSIGNED"), SystemParamsInFile.EXAMINATION_STAGE_ROOM_ASSIGNED, false, false));
            listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_INVILIGATOR_ASSIGNED"), SystemParamsInFile.EXAMINATION_STAGE_INVILIGATOR_ASSIGNED, false, false));
            listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_HEAD_ATTACHED"), SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED, false, false));
            listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_MARK_COMPLETED"), SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED, false, false));
            listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_FINISHED"), SystemParamsInFile.EXAMINATION_STAGE_FINISHED, false, false));
            ViewData[ExaminationConstants.LISTEXAMINATIONSTAGE] = listExaminationStage;
            ViewData[ExaminationConstants.ENABLE_BUTTON_ADD_EDIT_DELETE] = global.IsCurrentYear;

            //
            return View("SetExaminationStatus");
        }

        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult SetExaminationStage(int? ExaminationStage, int? ExaminationStageRadio)
        {
            //ExaminationBusiness.SetStage(UserInfo.SchoolID, UserInfo.AppliedLevel, cboExamination .Value, listRdCurrentStage.Value)
            if (ExaminationStage.HasValue && ExaminationStageRadio.HasValue)
            {
                GlobalInfo global = new GlobalInfo();
                ExaminationBusiness.SetStage(global.SchoolID.Value, (int)global.AppliedLevel.Value, ExaminationStage.Value, ExaminationStageRadio.Value);
                ExaminationBusiness.Save();
                return Json(new JsonMessage(Res.Get("Examination_Label_SetExaminationStageMessage")));
            }
            else
            {
                throw new BusinessException("Examination_Label_SetExaminationStageErrorMessage");
            }
        }


        [ValidateAntiForgeryToken]
        public PartialViewResult ChangeListRadioCheck(int? ExaminationID)
        {
            SetExaminationStageViewData();
            ViewData[ExaminationConstants.ENABLE_RADIOBUTTON] = ExaminationID.HasValue;
            ViewData[ExaminationConstants.ENABLE_BUTTON_ADD_EDIT_DELETE] = _globalInfo.IsCurrentYear;
            if (!ExaminationID.HasValue)
            {
                //default
                //LISTEXAMINATIONSTAGE
                List<ViettelCheckboxList> listExaminationStage = new List<ViettelCheckboxList>();
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_CREATED"), SystemParamsInFile.EXAMINATION_STAGE_CREATED, true, false));
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_CANDIDATE_LISTED"), SystemParamsInFile.EXAMINATION_STAGE_CANDIDATE_LISTED, false, false));
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_ROOM_ASSIGNED"), SystemParamsInFile.EXAMINATION_STAGE_ROOM_ASSIGNED, false, false));
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_INVILIGATOR_ASSIGNED"), SystemParamsInFile.EXAMINATION_STAGE_INVILIGATOR_ASSIGNED, false, false));
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_HEAD_ATTACHED"), SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED, false, false));
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_MARK_COMPLETED"), SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED, false, false));
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_FINISHED"), SystemParamsInFile.EXAMINATION_STAGE_FINISHED, false, false));
                ViewData[ExaminationConstants.LISTEXAMINATIONSTAGE] = listExaminationStage;

                //
            }
            else
            {
                //kiem tra gia tri stage cua examinationid truyen vao
                Examination exam = ExaminationBusiness.Find(ExaminationID.Value);
                int stage = exam.CurrentStage;
                bool stageCreate = false;
                bool stageCandidate = false;
                bool stageRoomAssigned = false;
                bool stageInviligator = false;
                bool stageHeadAttached = false;
                bool stageMarkComplete = false;
                bool stageFinished = false;
                switch (stage)
                {
                    case SystemParamsInFile.EXAMINATION_STAGE_CREATED:
                        stageCreate = true;
                        break;

                    case SystemParamsInFile.EXAMINATION_STAGE_CANDIDATE_LISTED:
                        stageCandidate = true;
                        break;

                    case SystemParamsInFile.EXAMINATION_STAGE_ROOM_ASSIGNED:
                        stageRoomAssigned = true;
                        break;

                    case SystemParamsInFile.EXAMINATION_STAGE_INVILIGATOR_ASSIGNED:
                        stageInviligator = true;
                        break;

                    case SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED:
                        stageHeadAttached = true;
                        break;

                    case SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED:
                        stageMarkComplete = true;
                        break;

                    case SystemParamsInFile.EXAMINATION_STAGE_FINISHED:
                        stageFinished = true;
                        break;

                    default:
                        stageCreate = true;
                        break;
                }

                //LISTEXAMINATIONSTAGE
                List<ViettelCheckboxList> listExaminationStage = new List<ViettelCheckboxList>();
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_CREATED"), SystemParamsInFile.EXAMINATION_STAGE_CREATED, stageCreate, false));
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_CANDIDATE_LISTED"), SystemParamsInFile.EXAMINATION_STAGE_CANDIDATE_LISTED, stageCandidate, false));
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_ROOM_ASSIGNED"), SystemParamsInFile.EXAMINATION_STAGE_ROOM_ASSIGNED, stageRoomAssigned, false));
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_INVILIGATOR_ASSIGNED"), SystemParamsInFile.EXAMINATION_STAGE_INVILIGATOR_ASSIGNED, stageInviligator, false));
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_HEAD_ATTACHED"), SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED, stageHeadAttached, false));
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_MARK_COMPLETED"), SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED, stageMarkComplete, false));
                listExaminationStage.Add(new ViettelCheckboxList(Res.Get("EXAMINATION_STAGE_FINISHED"), SystemParamsInFile.EXAMINATION_STAGE_FINISHED, stageFinished, false));
                ViewData[ExaminationConstants.LISTEXAMINATIONSTAGE] = listExaminationStage;

                //
            }
            return PartialView("_ListStage");
        }

        #endregion SetExaminationStage
    }
}
