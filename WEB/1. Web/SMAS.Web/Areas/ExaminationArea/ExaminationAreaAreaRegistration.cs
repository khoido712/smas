﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.ExaminationArea
{
    public class ExaminationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ExaminationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ExaminationArea_default",
                "ExaminationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}