/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.ComponentModel.DataAnnotations;
using Resources;
using SMAS.Models.CustomAttribute;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ExaminationArea.Models
{
    public class ExaminationViewModel
    {
        public System.Int32 ExaminationID { get; set; }

        public System.Int32 AcademicYearID { get; set; }

        public System.Nullable<System.Int32> SchoolID { get; set; }

        [ResourceDisplayName("Examination_Label_Year")]
        public System.Nullable<System.Int32> Year { get; set; }

        [ResourceDisplayName("Examination_Label_Year")]
        public string YearTitle { get; set; }

        [ResourceDisplayName("Examination_Label_Location")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Location { get; set; }

        [ResourceDisplayName("Examination_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
        public System.String Description { get; set; }

        public System.Int32 CurrentStage { get; set; }

        public System.Nullable<System.DateTime> CreatedDate { get; set; }

        public System.Boolean IsActive { get; set; }

        public System.Nullable<System.DateTime> ModifiedDate { get; set; }

        public System.Int32 UsingSeparateList { get; set; }

        public System.Int32 CandidateFromMultipleLevel { get; set; }

        public System.Int32 MarkImportType { get; set; }

        [ResourceDisplayName("Examination_Label_KindOfInput")]
        public System.String MarkImportTypeName { get; set; }

        public System.Int32 AppliedLevel { get; set; }

        #region grid

        [ResourceDisplayName("Examination_Label_Title")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(200, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Title { get; set; }

        [ResourceDisplayName("Examination_Label_FromDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.Nullable<System.DateTime> FromDate { get; set; }

        [ResourceDisplayName("Examination_Label_ToDate")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [UIHint("DateTimePicker")]
        [DateTime(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_DateTime")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.Nullable<System.DateTime> ToDate { get; set; }

        [ResourceDisplayName("Examination_Label_CurrentStage")]
        public string StatusName { get; set; }

        [ResourceDisplayName("Examination_Label_UsingSeparateList")]
        public bool bUsingSeparateList { get; set; }

        [ResourceDisplayName("Examination_Label_CandidateFromMultipleLevel")]
        public bool bCandidateFromMultipleLevel { get; set; }

        #endregion grid
    }
}
