using System;
using SMAS.Web.Models.Attributes;

namespace SMAS.Web.Areas.ExaminationArea.Models
{
    public class ExaminationSubjectVM
    {
        public int ExaminationSubjectID { get; set; }

        public int ExaminationID { get; set; }

        public int SubjectID { get; set; }

        public int EducationLevelID { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_StartTime")]
        public Nullable<System.DateTime> StartTime { get; set; }

        [ResourceDisplayName("Examination_Label_DurationInMinute")]
        public Nullable<int> DurationInMinute { get; set; }

        public string OrderNumberPrefix { get; set; }

        public bool IsLocked { get; set; }

        public bool HasDetachableHead { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_EducationLevel")]
        public string EducationLevelName { get; set; }

        [ResourceDisplayName("ExaminationSubject_Label_Subject")]
        public string SubjectName { get; set; }

        public int OrderInSubject { get; set; }
    }
}
