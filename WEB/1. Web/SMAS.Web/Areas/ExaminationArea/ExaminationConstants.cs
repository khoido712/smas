/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

namespace SMAS.Web.Areas.ExaminationArea
{
    public class ExaminationConstants
    {
        public const string LIST_EXAMINATION = "listExamination";
        public const string ENABLE_BUTTON_ADD_EDIT_DELETE = "ENABLE_BUTTON_ADD_EDIT_DELETE";
        public const string LISTMARKIMPORTTYPE = "LISTMARKIMPORTTYPE";
        public const string ENABLE_USINGSEPARATELIST = "ENABLE_USINGSEPARATELIST";
        public const string ENABLE_CANDIDATEFROMMULTIPLELEVEL = "ENABLE_CANDIDATEFROMMULTIPLELEVEL";
        public const string LIST_EXAMINATIONSUBJECT = "LIST_EXAMINATIONSUBJECT";
        public const string LISTEXAMINATIONSTAGE = "LISTEXAMINATIONSTAGE";
        public const string LIST_EXAMINATIONCHOICE = "LIST_EXAMINATIONCHOICE";

        public const string ENABLE_RADIOBUTTON = "ENABLE_RADIOBUTTON";
        public const int PAGE_SIZE = 10;
        public const string PAGE = "Page";
        public const string Total = "total";
    }
}