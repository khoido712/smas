﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.VTUtils.HtmlHelpers;
using SMAS.Web.Utils;
using SMAS.Web.Areas.GeneralSummedEvaluationArea;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Web.Areas.GeneralSummedEvaluationArea.Models;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using SMAS.Web.Filter;
using System.IO;
using System.Text;
using SMAS.Business.Business;
using SMAS.VTUtils.Log;

namespace SMAS.Web.Areas.GeneralSummedEvaluationArea.Controllers
{
    [ViewableBySupervisingDeptFilter]
    public class GeneralSummedEvaluationController : BaseController
    {
        //
        // GET: /SummedEvaluationArea/SummedEvaluation/

        private readonly ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness;
        private readonly ISummedEvaluationBusiness SummedEvaluationBusiness;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly IPupilOfClassBusiness PupilOfClassBusiness;
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        private readonly IBookmarkedFunctionBusiness BookmarkedFunctionBusiness;
        private readonly ISubjectCatBusiness SubjectCatBusiness;
        private readonly IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness;
        private readonly IRatedCommentPupilBusiness RatedCommentPupilBusiness;
        private readonly IEvaluationCriteriaBusiness EvaluationCriteriaBusiness;

        public GeneralSummedEvaluationController(IClassProfileBusiness classProfileBusiness, ISummedEndingEvaluationBusiness summedEndingEvaluationBusiness, ISummedEvaluationBusiness summedEvaluationBusiness,
                                           IPupilOfClassBusiness pupilOfClassBusiness, IClassSubjectBusiness classSubjectBusiness, IExemptedSubjectBusiness exemptedSubjectBusiness,
                                           IAcademicYearBusiness academicYearBusiness, IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness, IRatedCommentPupilBusiness RatedCommentPupilBusiness,
                                           IBookmarkedFunctionBusiness BookmarkedFunctionBusiness, ISubjectCatBusiness subjectCatBusiness, IEvaluationCriteriaBusiness evaluationCriteriaBusiness)
        {
            this.ClassProfileBusiness = classProfileBusiness;
            this.SummedEndingEvaluationBusiness = summedEndingEvaluationBusiness;
            this.SummedEvaluationBusiness = summedEvaluationBusiness;
            this.PupilOfClassBusiness = pupilOfClassBusiness;
            this.ClassSubjectBusiness = classSubjectBusiness;
            this.ExemptedSubjectBusiness = exemptedSubjectBusiness;
            this.AcademicYearBusiness = academicYearBusiness;
            this.RatedCommentPupilHistoryBusiness = RatedCommentPupilHistoryBusiness;
            this.RatedCommentPupilBusiness = RatedCommentPupilBusiness;
            this.BookmarkedFunctionBusiness = BookmarkedFunctionBusiness;
            this.SubjectCatBusiness = subjectCatBusiness;
            this.EvaluationCriteriaBusiness = evaluationCriteriaBusiness;
        }
        public ActionResult Index(int? ClassID)
        {
            return View(ClassID);
        }
        public ActionResult _index(int? ClassID)
        {
            SetViewData(ClassID);
            return PartialView();
        }
        private void SetViewData(int? ClassID)
        {
            bool semester1 = false;
            bool semester2 = false;
            if (!_globalInfo.Semester.HasValue)
            {
                semester1 = true;
            }
            else
            {
                if (_globalInfo.Semester.Value == 1)
                    semester1 = true;
                else semester2 = true;
            }
            List<ViettelCheckboxList> listSemester = new List<ViettelCheckboxList>();
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester1"), 1, semester1, false));
            listSemester.Add(new ViettelCheckboxList(Res.Get("MarkRecordPeriod_Label_Semester2"), 2, semester2, false));
            ViewData[GeneralSummedEvaluationConstant.LIST_SEMESTER] = listSemester;

            List<ViettelCheckboxList> listEducationLevel = new List<ViettelCheckboxList>();
            int i = 0;
            foreach (EducationLevel item in new GlobalInfo().EducationLevels)
            {
                i++;
                bool check = false;
                if (i == 1) check = true;
                listEducationLevel.Add(new ViettelCheckboxList(item.Resolution, item.EducationLevelID, check, false));
            }
            ViewData[GeneralSummedEvaluationConstant.LIST_EDUCATIONLEVEL] = listEducationLevel;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic = new Dictionary<string, object>();
            ClassProfile cp = null;
            if (listEducationLevel != null && listEducationLevel.Count > 0)
            {
                dic["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dic["AppliedLevel"] = _globalInfo.AppliedLevel.Value;
                if (_globalInfo.IsAdminSchoolRole == false && _globalInfo.IsViewAll == false && !_globalInfo.IsEmployeeManager)
                {
                    dic["UserAccountID"] = _globalInfo.UserAccountID;
                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER;
                }
                IEnumerable<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                ViewData[GeneralSummedEvaluationConstant.LIST_CLASS] = listClass;
                if (listClass.Count() == 0)
                {
                    ViewData[GeneralSummedEvaluationConstant.CHECK_CLASS_NULL] = true;
                }
                else
                {
                    var listCP = listClass.ToList();

                    if (listCP != null && listCP.Count() > 0)
                    {
                        //Tính ra lớp cần được chọn đầu tiên
                        cp = listCP.First();
                        if (listCP.Exists(x => x.ClassProfileID == ClassID.GetValueOrDefault()))
                        {
                            cp = listCP.Find(x => x.ClassProfileID == ClassID.GetValueOrDefault());
                        }
                        else
                        {
                            // Nếu không phải là QTHT, cán bộ quản lý thì xét theo quyền giáo viên để chọn lớp hiển thị đầu tiên
                            if (!_globalInfo.IsAdminSchoolRole && !_globalInfo.IsEmployeeManager)
                            {
                                // Ưu tiên trước đối với giáo viên bộ môn
                                dic["Type"] = SystemParamsInFile.TEACHER_ROLE_SUBJECTTEACHER;
                                List<ClassProfile> listSubjectTeacher = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                                .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                if (listSubjectTeacher != null && listSubjectTeacher.Count > 0)
                                {
                                    cp = listSubjectTeacher.First();
                                }
                                else
                                {
                                    dic["Type"] = SystemParamsInFile.TEACHER_ROLE_HEADTEACHER;
                                    List<ClassProfile> listHead = ClassProfileBusiness.SearchBySchool(_globalInfo.SchoolID.GetValueOrDefault(), dic)
                                                                            .OrderBy(x => x.EducationLevelID).ThenBy(x => x.DisplayName).ToList();
                                    if (listHead != null && listHead.Count > 0)
                                    {
                                        cp = listHead.First();
                                    }
                                }
                            }
                        }
                    }
                    ViewData[GeneralSummedEvaluationConstant.CLASS_PROFILE] = cp;
                }
            }
        }
        public ActionResult AjaxLoadGrid(int ClassID, int SemesterID)
        {
            if (SemesterID == 2)
            {
                SetVisbileButton(SemesterID, ClassID);
            }
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            bool isNotShowPupil = objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value;
            List<SummedEvaluationViewModel> lstSummedEvaluation = new List<SummedEvaluationViewModel>();
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName);
            if (isNotShowPupil)
            {
                iquery = iquery.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED);
            }
            lstPOC = iquery.ToList();
            //Load danh sach mon hoc
            IQueryable<ClassSubject> iqSubject;
            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = ClassID;
            dicSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSubject["IsApprenticeShipSubject"] = false;
            dicSubject["AppliedLevel"] = _globalInfo.AppliedLevel;
            dicSubject["Semester"] = SemesterID;
            List<ClassSubjectBO> lstSubject = new List<ClassSubjectBO>();
            if (ClassID > 0 && SemesterID > 0)
            {
                iqSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject);
                lstSubject = iqSubject.Select(o => new ClassSubjectBO()
                {
                    ClassID = o.ClassID,
                    SubjectID = o.SubjectID,
                    OrderInSubject = o.SubjectCat.OrderInSubject,
                    DisplayName = o.SubjectCat.DisplayName,
                    SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                    SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                    IsCommenting = o.IsCommenting,
                    Abbreviation = o.SubjectCat.Abbreviation
                }).ToList().OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ToList();
            }
            lstSummedEvaluation = this.SearchData(lstPOC, ClassID, SemesterID, lstSubject);
            ViewData[GeneralSummedEvaluationConstant.LIST_GENERAL_SUMMED_EVALUATION] = lstSummedEvaluation;
            ViewData[GeneralSummedEvaluationConstant.SEMESTER_ID] = SemesterID;
            ViewData[GeneralSummedEvaluationConstant.LIST_RATE_ADD] = CommonList.RateAdd();
            return PartialView("_GridViewGeneralSummedEvaluation");
        }
        public PartialViewResult AjaxLoadGirdRatedComment(int ClassID, int SemesterID, int PupilID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",ClassID},
                {"SemesterID",SemesterID},
                {"PupilID",PupilID}
            };
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            List<RatedCommentPupilBO> lstRatedCommentPupil = new List<RatedCommentPupilBO>();
            if (UtilsBusiness.IsMoveHistory(objAy))
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilHistoryBusiness.Search(dic)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                            Comment = rh.Comment,
                                            CreateTime = rh.CreateTime,
                                            UpdateTime = rh.UpdateTime,
                                            FullNameComment = rh.FullNameComment,
                                            LogChangeID = rh.LogChangeID,
                                        }).ToList();
            }
            else
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilBusiness.Search(dic)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            CapacityEndingEvaluationName = rh.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                                                        : rh.CapacityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : rh.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "",
                                            QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                            QualityEndingEvaluationName = rh.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                                                        : rh.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : rh.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "",
                                            Comment = rh.Comment,
                                            CreateTime = rh.CreateTime,
                                            UpdateTime = rh.UpdateTime,
                                            FullNameComment = rh.FullNameComment,
                                            LogChangeID = rh.LogChangeID
                                        }).ToList();
            }
            //lay danh sach mien giam
            List<int> lstExemSubject = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SemesterID).Where(p => p.PupilID == PupilID).Select(p => p.SubjectID).Distinct().ToList();
            List<RatedCommentPupilBO> lstSubjectRatedComment = lstRatedCommentPupil.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP).ToList();
            List<RatedCommentPupilBO> lstCapQuaRatedComment = lstRatedCommentPupil.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY).ToList();
            RatedCommentPupilBO objtmp = null;
            List<SubjectRatedCommentViewModel> lstSubjectViewModel = new List<SubjectRatedCommentViewModel>();
            SubjectRatedCommentViewModel objSubjectViewModel = new SubjectRatedCommentViewModel();
            List<CapacityQualityViewModel> lstCapQuaViewModel = new List<CapacityQualityViewModel>();
            CapacityQualityViewModel objCapQuaViewModel = null;

            //lay danh sach mon hoc cua lop
            bool ViewAll = false;

            if (_globalInfo.IsViewAll || _globalInfo.IsEmployeeManager)
            {
                ViewAll = true;
            }
            int schoolId = _globalInfo.SchoolID.Value;
            List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
            if (ClassID > 0 && SemesterID > 0)
            {
                lsClassSubject = (from cs in ClassSubjectBusiness.GetListSubjectBySubjectTeacher(_globalInfo.UserAccountID, _globalInfo.AcademicYearID.Value, _globalInfo.SchoolID.Value, SemesterID, ClassID, ViewAll).Where(u => SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester > 0 : u.SectionPerWeekSecondSemester > 0)
                                  join s in SubjectCatBusiness.All on cs.SubjectID equals s.SubjectCatID
                                  where s.IsActive == true
                                  && s.IsApprenticeshipSubject == false
                                  select new SubjectCatBO()
                                  {
                                      SubjectCatID = s.SubjectCatID,
                                      DisplayName = s.DisplayName,
                                      IsCommenting = cs.IsCommenting,
                                      OrderInSubject = s.OrderInSubject
                                  }
                                  ).OrderBy(p => p.IsCommenting).ThenBy(o => o.OrderInSubject).ToList();
            }
            int location = 0;
            int subjectID = 0;
            SubjectCatBO objSC = null;
            int lengthCount = lsClassSubject.Count / 2;
            int maxcount = lsClassSubject.Count % 2 == 0 ? lengthCount - 1 : lengthCount;
            for (int i = 0; i < lsClassSubject.Count; i++)
            {
                objSC = lsClassSubject[i];
                subjectID = objSC.SubjectCatID;
                objtmp = lstSubjectRatedComment.Where(p => p.SubjectID == subjectID).FirstOrDefault();
                if (i > maxcount)
                {
                    objSubjectViewModel = lstSubjectViewModel[location];
                    objSubjectViewModel.SubjectID2 = objSC.SubjectCatID;
                    objSubjectViewModel.SubjectName2 = objSC.DisplayName;
                    objSubjectViewModel.isCommenting2 = objSC.IsCommenting;
                    if (objtmp != null)
                    {
                        objSubjectViewModel.EndingSubject2 = objtmp.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                        : objtmp.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objtmp.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                        objSubjectViewModel.MarkSubject2 = objtmp.PeriodicEndingMark;
                    }
                    location++;
                }
                else
                {
                    objSubjectViewModel = new SubjectRatedCommentViewModel();
                    objSubjectViewModel.SubjectID1 = objSC.SubjectCatID;
                    objSubjectViewModel.SubjectName1 = objSC.DisplayName;
                    objSubjectViewModel.isCommenting1 = objSC.IsCommenting;
                    if (objtmp != null)
                    {
                        objSubjectViewModel.EndingSubject1 = objtmp.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                        : objtmp.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objtmp.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                        objSubjectViewModel.MarkSubject1 = objtmp.PeriodicEndingMark;

                    }
                    lstSubjectViewModel.Add(objSubjectViewModel);
                }
            }
            List<EvaluationCriteiaBO> lstEGBO = EvaluationCriteriaBusiness.All.Select(p => new EvaluationCriteiaBO
            {
                EvaluationCriteriaID = p.EvaluationCriteriaID,
                CriteriaName = p.CriteriaName,
                TypeID = p.TypeID
            }).OrderBy(p => p.EvaluationCriteriaID).ToList();
            EvaluationCriteiaBO objEGBO = null;
            for (int i = 0; i < lstEGBO.Count; i++)
            {
                objEGBO = lstEGBO[i];
                objCapQuaViewModel = new CapacityQualityViewModel();
                objCapQuaViewModel.CriteriaID = objEGBO.EvaluationCriteriaID;
                objCapQuaViewModel.CriteriaName = objEGBO.CriteriaName;
                objCapQuaViewModel.TypeID = objEGBO.TypeID;
                objtmp = lstRatedCommentPupil.Where(p => p.EvaluationID == objEGBO.TypeID && p.SubjectID == objEGBO.EvaluationCriteriaID).FirstOrDefault();
                if (objtmp != null)
                {
                    objCapQuaViewModel.RatedName = objEGBO.TypeID == GlobalConstants.EVALUATION_CAPACITY ? objtmp.CapacityEndingEvaluationName
                                                        : objtmp.QualityEndingEvaluationName;
                }
                lstCapQuaViewModel.Add(objCapQuaViewModel);
            }
            ViewData[GeneralSummedEvaluationConstant.LIST_SUBJECT_VIEW_MODEL] = lstSubjectViewModel;
            ViewData[GeneralSummedEvaluationConstant.LIST_CAPQUA_VIEW_MODEL] = lstCapQuaViewModel;
            ViewData[GeneralSummedEvaluationConstant.LIST_EXEMPTED_SUBJECT] = lstExemSubject;
            return PartialView("_ListRatedComment");
        }
        [ValidateAntiForgeryToken]
        public JsonResult EvaluationRateAddition(string arrayID, int ClassID, int SemesterID, string arrayAddID)
        {
            try
            {
                List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
                PupilOfClassBO objPOC = null;
                List<SummedEndingEvaluation> lstSummedEnding = new List<SummedEndingEvaluation>();
                SummedEndingEvaluation objSummedEvaluation = null; ;

                string[] ArrayID = arrayID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                List<string> IDStrList = ArrayID.ToList();
                List<int> idList = IDStrList.Select(p => int.Parse(p)).ToList();

                string[] RateID = arrayAddID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                List<string> RateStrList = RateID.ToList();
                IDictionary<int, int> dicArrAddID = new Dictionary<int, int>();
                string tmp = string.Empty;
                List<string> lstRate = null;
                int rateID = 0;
                for (int i = 0; i < RateStrList.Count; i++)
                {
                    tmp = RateStrList[i];
                    lstRate = new List<string>();
                    lstRate = tmp.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    dicArrAddID.Add(int.Parse(lstRate[0]), int.TryParse(lstRate[1], out rateID) ? rateID : 2);
                }
                List<SummedEvaluationViewModel> lstSummedEvaluation = new List<SummedEvaluationViewModel>();
                ClassSubjectBO objSubject = new ClassSubjectBO();
                int pupilID = 0;
                for (int i = 0; i < lstPOC.Count; i++)
                {
                    objSummedEvaluation = new SummedEndingEvaluation();
                    objSummedEvaluation.SummedEndingEvaluationID = SummedEndingEvaluationBusiness.GetNextSeq<int>();
                    objPOC = lstPOC[i];
                    pupilID = objPOC.PupilID;
                    if (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING || !idList.Contains(pupilID))
                    {
                        continue;
                    }
                    objSummedEvaluation.PupilID = int.Parse(pupilID.ToString());
                    objSummedEvaluation.SemesterID = GlobalConstants.SEMESTER_OF_EVALUATION_RESULT;
                    objSummedEvaluation.EvaluationID = GlobalConstants.EVALUATION_RESULT;
                    objSummedEvaluation.SchoolID = _globalInfo.SchoolID.Value;
                    objSummedEvaluation.LastDigitSchoolID = UtilsBusiness.GetPartionId(_globalInfo.SchoolID.Value);
                    objSummedEvaluation.ClassID = ClassID;
                    objSummedEvaluation.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objSummedEvaluation.CreateTime = DateTime.Now;
                    objSummedEvaluation.RateAdd = dicArrAddID[pupilID];
                    lstSummedEnding.Add(objSummedEvaluation);
                }
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"ClassID",ClassID},
                    {"IsRateAdd",true}
                };
                //gọi hàm insert or update summedEvaluationEnding
                SummedEndingEvaluationBusiness.InsertOrUpdate(dic, lstSummedEnding);
                return Json(new JsonMessage(Res.Get("Đánh giá thành công"), "success"));
            }
            catch (Exception ex)
            {
                string para = "ClassID=" + ClassID;
                para += "SemesterID=" + SemesterID;
                para += "arrayID=" + arrayID;
                para += "arrayAddID=" + arrayAddID;
                LogExtensions.ErrorExt(logger, DateTime.Now, "EvaluationRateAddition", para, ex);

                return Json(new JsonMessage(Res.Get("Có lỗi trong quá trình tổng hợp dữ liệu.Thầy/cô vui lòng thử lại."), "error"));
            }
        }
        [ValidateAntiForgeryToken]
        public JsonResult EvaluationRate(string arrayID, int ClassID, int SemesterID)
        {
            try
            {
                List<SummedEndingEvaluation> lstSummedEnding = new List<SummedEndingEvaluation>();
                SummedEndingEvaluation objSummedEvaluation = null;
                List<int> lstPupilID = arrayID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).Where(p => lstPupilID.Contains(p.PupilID))
                                                .OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
                //Load danh sach mon hoc
                IQueryable<ClassSubject> iqSubject;
                Dictionary<string, object> dicSubject = new Dictionary<string, object>();
                dicSubject["ClassID"] = ClassID;
                dicSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
                dicSubject["IsApprenticeShipSubject"] = false;
                dicSubject["AppliedLevel"] = _globalInfo.AppliedLevel;
                dicSubject["Semester"] = SemesterID;
                List<ClassSubjectBO> lstSubject = new List<ClassSubjectBO>();
                if (ClassID > 0 && SemesterID > 0)
                {
                    iqSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject);
                    lstSubject = iqSubject.Select(o => new ClassSubjectBO()
                    {
                        ClassID = o.ClassID,
                        SubjectID = o.SubjectID,
                        OrderInSubject = o.SubjectCat.OrderInSubject,
                        DisplayName = o.SubjectCat.DisplayName,
                        SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                        SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                        IsCommenting = o.IsCommenting,
                        Abbreviation = o.SubjectCat.Abbreviation
                    }).ToList().OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ToList();
                }
                List<SummedEvaluationViewModel> lstResult = this.SearchData(lstPOC, ClassID, SemesterID, lstSubject);
                SummedEvaluationViewModel objResult = null;
                for (int i = 0; i < lstResult.Count; i++)
                {
                    objResult = lstResult[i];
                    objSummedEvaluation = new SummedEndingEvaluation();
                    objSummedEvaluation.SchoolID = _globalInfo.SchoolID.Value;
                    objSummedEvaluation.AcademicYearID = _globalInfo.AcademicYearID.Value;
                    objSummedEvaluation.PupilID = objResult.PupilID;
                    objSummedEvaluation.ClassID = ClassID;
                    objSummedEvaluation.SemesterID = GlobalConstants.SEMESTER_OF_EVALUATION_RESULT;
                    objSummedEvaluation.EvaluationID = GlobalConstants.EVALUATION_RESULT;
                    if (objResult.isFullMark)
                    {
                        if (objResult.isHT)
                        {
                            objSummedEvaluation.EndingEvaluation = "HT";
                        }
                        else
                        {
                            objSummedEvaluation.EndingEvaluation = "CHT";
                        }

                        if (objResult.isHSXS)
                        {
                            objSummedEvaluation.Reward = 1;
                        }
                        else
                        {
                            objSummedEvaluation.Reward = null;
                        }
                    }
                    else
                    {
                        objSummedEvaluation.EndingEvaluation = string.Empty;
                    }
                    lstSummedEnding.Add(objSummedEvaluation);
                }

                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"ClassID",ClassID}
                };
                SummedEndingEvaluationBusiness.InsertOrUpdate(dic, lstSummedEnding);
                return Json(new JsonMessage(Res.Get("Common_Label_Summed_Ending_Evaluation"), "success"));
            }
            catch (Exception ex)
            {
                string para = "ClassID=" + ClassID;
                para += "SemesterID=" + SemesterID;
                para += "arrayID=" + arrayID;
                LogExtensions.ErrorExt(logger, DateTime.Now, "EvaluationRate",para, ex);

                return Json(new JsonMessage(Res.Get("General_Summed_Evaluation_Delete_Rate"), "error"));
            }
        }
        [ValidateAntiForgeryToken]
        public JsonResult DeleteEvaluationResult(FormCollection frm)
        {
            try
            {
                int classID = int.Parse(frm["HfClassID"]);
                List<long> lstpupilID = new List<long>();
                // Lay danh sach hoc sinh
                List<PupilOfClassBO> listPupil = PupilOfClassBusiness.GetPupilInClass(classID)
                                                                    .OrderBy(u => u.OrderInClass)
                                                                    .ThenBy(u => u.Name)
                                                                    .ThenBy(u => u.PupilFullName)
                                                                    .ToList();
                PupilOfClassBO objPupil = null;

                for (int i = 0; i < listPupil.Count; i++)
                {
                    objPupil = listPupil[i];
                    if ("on".Equals(frm["Chk_" + objPupil.PupilID]))
                    {
                        lstpupilID.Add(objPupil.PupilID);
                    }
                }
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",_globalInfo.SchoolID},
                    {"AcademicYearID",_globalInfo.AcademicYearID}
                };
                SummedEndingEvaluationBusiness.DeleteSummedEvaluation(dic, lstpupilID);
                return Json(new JsonMessage(Res.Get("success_del")));
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "DeleteEvaluationResult","", ex);
                return Json(new JsonMessage(Res.Get("General_Summed_Evaluation_Delete_Fail")));
            }
        }
        public FileResult ExportExcel(int classID, int semesterID)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(_globalInfo.AcademicYearID.Value);
            string templatePath = string.Empty;
            ClassProfile objCP = ClassProfileBusiness.Find(classID);
            //string OutPutName2 = string.Empty;
            templatePath = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", GeneralSummedEvaluationConstant.FILE_TEMPLATE);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            bool isNotShowPupil = academicYear.IsShowPupil.HasValue && academicYear.IsShowPupil.Value;
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            IQueryable<PupilOfClassBO> iquery = PupilOfClassBusiness.GetPupilInClass(classID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName);
            if (isNotShowPupil)
            {
                iquery = iquery.Where(p => p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED);
            }
            lstPOC = iquery.ToList();
            PupilOfClassBO objPOC = null;
            //Load danh sach mon hoc
            IQueryable<ClassSubject> iqSubject;
            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = classID;
            dicSubject["AcademicYearID"] = _globalInfo.AcademicYearID.Value;
            dicSubject["IsApprenticeShipSubject"] = false;
            dicSubject["AppliedLevel"] = _globalInfo.AppliedLevel;
            dicSubject["Semester"] = semesterID;
            List<ClassSubjectBO> lstSubject = new List<ClassSubjectBO>();
            ClassSubjectBO objCSBO = null;
            if (classID > 0 && semesterID > 0)
            {
                iqSubject = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dicSubject);
                lstSubject = iqSubject.Select(o => new ClassSubjectBO()
                {
                    ClassID = o.ClassID,
                    SubjectID = o.SubjectID,
                    OrderInSubject = o.SubjectCat.OrderInSubject,
                    DisplayName = o.SubjectCat.DisplayName,
                    SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                    SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                    IsCommenting = o.IsCommenting,
                    Abbreviation = o.SubjectCat.Abbreviation
                }).ToList().OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ToList();
            }

            List<SummedEvaluationViewModel> lstSummedEvaluation = this.SearchData(lstPOC, classID, semesterID, lstSubject);
            SummedEvaluationViewModel objSEVM = null;
            #region fill data to file
            //fill thong tin chung
            sheet.SetCellValue("A2", UtilsBusiness.GetSupervisingDeptName(_globalInfo.SchoolID.Value, _globalInfo.AppliedLevel.Value).ToUpper());
            sheet.SetCellValue("A3", _globalInfo.SchoolName.ToUpper());
            string Title = "BẢNG TỔNG HỢP ĐÁNH GIÁ - " + objCP.DisplayName;
            sheet.SetCellValue("A5", Title.ToUpper());
            Title = string.Empty;
            Title = (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "Học kỳ I , năm học " : "Học kỳ II , năm học ") + academicYear.DisplayTitle;
            sheet.SetCellValue("A6", Title);
            int startRow = 10;
            int startColSubject = 3;
            int startColCap = 0;
            int startcolQua = 0;
            #region fill ten lop
            SubjectEvaluationViewModel objSubjectViewModel = null;
            for (int i = 0; i < lstSubject.Count; i++)
            {
                objCSBO = lstSubject[i];
                sheet.GetRange(8, startColSubject, 8, (objCSBO.IsCommenting == 0 ? startColSubject + 1 : startColSubject)).Merge();
                sheet.SetCellValue(8, startColSubject, objCSBO.DisplayName);
                if (objCSBO.IsCommenting == 0)
                {
                    sheet.SetCellValue(9, startColSubject, "KTCK");
                    startColSubject += 2;
                }
                else
                {
                    startColSubject += 1;
                }

                sheet.SetCellValue(9, startColSubject - 1, "ĐGCK");
            }

            //nang luc pham chat
            startColCap = startColSubject;
            sheet.SetCellValue(8, startColCap, "Năng lực (*)");
            sheet.GetRange(8, startColCap, 8, startColCap + 2).Merge();
            sheet.SetCellValue(9, startColCap, "NL1");
            sheet.SetColumnWidth(startColCap, 4);
            sheet.SetCellValue(9, startColCap + 1, "NL2");
            sheet.SetColumnWidth(startColCap + 1, 4);
            sheet.SetCellValue(9, startColCap + 2, "NL3");
            sheet.SetColumnWidth(startColCap + 2, 4);

            startcolQua = startColCap + 3;
            sheet.SetCellValue(8, startcolQua, "Phẩm chất (*)");
            sheet.GetRange(8, startcolQua, 8, startcolQua + 3).Merge();
            sheet.SetCellValue(9, startcolQua, "PC1");
            sheet.SetColumnWidth(startcolQua, 4);
            sheet.SetCellValue(9, startcolQua + 1, "PC2");
            sheet.SetColumnWidth(startcolQua + 1, 4);
            sheet.SetCellValue(9, startcolQua + 2, "PC3");
            sheet.SetColumnWidth(startcolQua + 2, 4);
            sheet.SetCellValue(9, startcolQua + 3, "PC4");
            sheet.SetColumnWidth(startcolQua + 3, 4);

            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                //Ket qua nam hoc
                sheet.SetCellValue(8, startcolQua + 4, "Kết quả năm học");
                sheet.GetRange(8, startcolQua + 4, 9, startcolQua + 4).Merge();
                sheet.SetColumnWidth(startcolQua + 4, 7);
                //khen thuong
                sheet.SetCellValue(8, startcolQua + 5, "Khen thưởng");
                sheet.GetRange(8, startcolQua + 5, 9, startcolQua + 5).Merge();
                sheet.SetColumnWidth(startcolQua + 5, 16);
                //Danh gia bo sung
                sheet.SetCellValue(8, startcolQua + 6, "Đánh giá bổ sung");
                sheet.GetRange(8, startcolQua + 6, 9, startcolQua + 6).Merge();
                sheet.SetColumnWidth(startcolQua + 6, 7);
            }
            #endregion
            #region fill du lieu

            int PupilID = 0;
            int endColumn = 0;
            List<SubjectEvaluationViewModel> lstSubjectVM = new List<SubjectEvaluationViewModel>();
            for (int i = 0; i < lstPOC.Count; i++)
            {
                startColSubject = 3;
                startColCap = startcolQua = 0;
                objPOC = lstPOC[i];
                PupilID = objPOC.PupilID;
                //STT
                sheet.SetCellValue(startRow, 1, (i + 1));
                //Ho ten
                sheet.SetCellValue(startRow, 2, objPOC.PupilFullName);
                //fill diem
                objSEVM = lstSummedEvaluation.Where(p => p.PupilID == PupilID).FirstOrDefault();
                for (int j = 0; j < lstSubject.Count; j++)
                {
                    objCSBO = lstSubject[j];
                    if (objSEVM != null)
                    {
                        objSubjectViewModel = objSEVM.lstSubjectViewModel.Where(p => p.SubjectID == objCSBO.SubjectID).FirstOrDefault();
                        if (objCSBO.IsCommenting == 0)
                        {
                            if (objSubjectViewModel != null)
                            {
                                sheet.SetCellValue(startRow, startColSubject, objSubjectViewModel.KTDK);
                            }
                            startColSubject += 2;
                        }
                        else
                        {
                            startColSubject += 1;
                        }
                        sheet.SetCellValue(startRow, startColSubject - 1, objSubjectViewModel != null ? objSubjectViewModel.DG : "");
                    }
                }

                if (objSEVM != null)
                {
                    //Nang luc
                    startColCap = startColSubject;
                    sheet.SetCellValue(startRow, startColCap, objSEVM.NL1);
                    sheet.SetCellValue(startRow, startColCap + 1, objSEVM.NL2);
                    sheet.SetCellValue(startRow, startColCap + 2, objSEVM.NL3);
                    //Pham chat
                    startcolQua = startColCap + 3;
                    sheet.SetCellValue(startRow, startcolQua, objSEVM.PC1);
                    sheet.SetCellValue(startRow, startcolQua + 1, objSEVM.PC2);
                    sheet.SetCellValue(startRow, startcolQua + 2, objSEVM.PC3);
                    sheet.SetCellValue(startRow, startcolQua + 3, objSEVM.PC4);

                    if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                    {
                        //Ket qua nam hoc
                        sheet.SetCellValue(startRow, startcolQua + 4, objSEVM.EvaluationResult);
                        //khen thuong
                        sheet.SetCellValue(startRow, startcolQua + 5, objSEVM.RewardName);
                        //Danh gia bo sung
                        sheet.SetCellValue(startRow, startcolQua + 6, "CHT".Equals(objSEVM.EvaluationResult) && objSEVM.RateAdd.HasValue ? (objSEVM.RateAdd.Value == 2 ? "CĐG" : objSEVM.RateAdd.Value == 1 ? "HT" : "CHT") : "");
                    }
                }
                if (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.GetRange(startRow, 1, startRow, semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? startcolQua + 3 : startcolQua + 6).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
                startRow++;
            }

            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                endColumn = startcolQua + 3;
            }
            else
            {
                endColumn = startcolQua + 6;
            }
            sheet.GetRange(8, 1, lstPOC.Count + 9, endColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(5, 1, 5, endColumn).Merge();
            sheet.GetRange(5, 1, 5, endColumn).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(6, 1, 6, endColumn).Merge();
            sheet.GetRange(6, 1, 6, endColumn).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(7, 1, 7, endColumn).Merge();
            sheet.GetRange(7, 1, 7, endColumn).SetHAlign(VTHAlign.xlHAlignRight);

            sheet.SetCellValue(startRow + 1, 1, "(*)");
            sheet.SetCellValue(startRow + 1, 2, Res.Get("General_Summed_Evaluation_Capacity_Title"));
            sheet.SetCellValue(startRow + 2, 2, Res.Get("General_Summed_Evaluation_Quality_Title"));
            sheet.GetRange(5, 1, 5, endColumn).Merge();
            sheet.GetRange(6, 1, 6, endColumn).Merge();
            sheet.GetRange(7, 1, 7, endColumn).Merge();
            sheet.GetRange(7, 1, 7, endColumn).SetHAlign(VTHAlign.xlHAlignRight);
            #endregion
            #endregion
            if (semesterID == 1)
            {
                sheet.Name = "Tong hop - HKI";
            }
            else
            {
                sheet.Name = "Tong hop - HKII";
            }
            Stream excel = oBook.ToStream();
            FileStreamResult result = new FileStreamResult(excel, "application/octet-stream");
            result.FileDownloadName = string.Format("BangTongHopDanhGia_{0}.xls", Utils.Utils.StripVNSignAndSpace(objCP.DisplayName));
            return result;
        }
        private List<SummedEvaluationViewModel> SearchData(List<PupilOfClassBO> lstPOC, int classID, int semesterID, List<ClassSubjectBO> lstSubject)
        {
            List<int> lstPupilID = lstPOC.Select(p => p.PupilID).Distinct().ToList();
            //lay danh sach mien giam
            List<ExemptedSubject> lstExemSubject = ExemptedSubjectBusiness.GetListExemptedSubject(classID, GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                                                    .Where(p => lstPupilID.Contains(p.PupilID)).ToList();
            List<RatedCommentPupilBO> lstRatedCommentPupil = new List<RatedCommentPupilBO>();
            AcademicYear objAy = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            IDictionary<string, object> dic = new Dictionary<string, object>()
                            {
                                {"SchoolID",_globalInfo.SchoolID},
                                {"AcademicYearID",_globalInfo.AcademicYearID},
                                {"ClassID",classID},
                                {"SemesterID",semesterID},
                                {"lstPupilID",lstPupilID}
                            };
            if (UtilsBusiness.IsMoveHistory(objAy))
            {

                lstRatedCommentPupil = (from rh in RatedCommentPupilHistoryBusiness.Search(dic)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation
                                        }).ToList();
            }
            else
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilBusiness.Search(dic)
                                        select new RatedCommentPupilBO
                                        {
                                            SchoolID = rh.SchoolID,
                                            AcademicYearID = rh.AcademicYearID,
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                            QualityEndingEvaluation = rh.QualityEndingEvaluation
                                        }).ToList();
            }

            List<SummedEvaluationViewModel> lstResult = new List<SummedEvaluationViewModel>();
            SummedEvaluationViewModel objResult = null;
            RatedCommentPupilBO objRatedComment = null;
            List<RatedCommentPupilBO> lstRatedtmp = new List<RatedCommentPupilBO>();
            PupilOfClassBO objPOC = null;
            int PupilID = 0;
            List<SummedEndingEvaluation> lstSEE = new List<SummedEndingEvaluation>();
            dic = new Dictionary<string, object>()
            {
                {"SchoolID",_globalInfo.SchoolID},
                {"AcademicYearID",_globalInfo.AcademicYearID},
                {"ClassID",classID},
                {"lstPupilID",lstPupilID}
            };
            lstSEE = SummedEndingEvaluationBusiness.GetSummedEndingEvaluation(dic)
                        .Where(p => p.EvaluationID == GlobalConstants.EVALUATION_RESULT && p.SemesterID == GlobalConstants.SEMESTER_OF_EVALUATION_RESULT).ToList();
            SummedEndingEvaluation objSEE = null;
            int FullMark = 0;
            int CHT = 0;
            int NotHSXS = 0;
            ClassSubjectBO objCS = null;
            List<SubjectEvaluationViewModel> lstSubjectViewModel = new List<SubjectEvaluationViewModel>();
            SubjectEvaluationViewModel objSubjectViewModel = null;
            bool isExempted = false;
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objResult = new SummedEvaluationViewModel();
                lstSubjectViewModel = new List<SubjectEvaluationViewModel>();
                objPOC = lstPOC[i];
                PupilID = objPOC.PupilID;
                objResult.PupilID = PupilID;
                objResult.FullName = objPOC.PupilFullName;
                objResult.Status = objPOC.Status;
                FullMark = 0;
                CHT = 0;
                NotHSXS = 0;
                //tinh xem du diem hay chua
                for (int j = 0; j < lstSubject.Count; j++)
                {
                    objCS = lstSubject[j];
                    isExempted = lstExemSubject.Where(p => p.PupilID == PupilID && p.SubjectID == objCS.SubjectID).Count() > 0;
                    objSubjectViewModel = new SubjectEvaluationViewModel();
                    objSubjectViewModel.SubjectID = objCS.SubjectID;
                    objSubjectViewModel.isCommenting = objCS.IsCommenting;
                    objSubjectViewModel.SubjectName = objCS.DisplayName;
                    objSubjectViewModel.OrderInSubject = objCS.OrderInSubject;
                    objSubjectViewModel.Abbreviation = objCS.Abbreviation;
                    objRatedComment = lstRatedCommentPupil.Where(p => p.PupilID == PupilID && p.SubjectID == objCS.SubjectID && p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP).FirstOrDefault();
                    if (objRatedComment != null)
                    {
                        objSubjectViewModel.DG = objRatedComment.EndingEvaluation.HasValue ? objRatedComment.EndingEvaluation.Value == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.EndingEvaluation.Value == 2 ? GlobalConstants.EVALUATION_COMPLETE : GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                        objSubjectViewModel.KTDK = objCS.IsCommenting == 0 ? objRatedComment.PeriodicEndingMark : null;
                        if (!isExempted)//khong xet mon mien giam
                        {
                            if (!objRatedComment.EndingEvaluation.HasValue || (objCS.IsCommenting == 0 && !objRatedComment.PeriodicEndingMark.HasValue))
                            {
                                FullMark++;
                            }
                            else
                            {
                                if ((objRatedComment.EndingEvaluation == 3))
                                {
                                    CHT++;
                                    NotHSXS++;
                                }
                                else if (objRatedComment.EndingEvaluation == 2)
                                {
                                    NotHSXS++;
                                }

                                if (objCS.IsCommenting == 0)
                                {
                                    if (objRatedComment.PeriodicEndingMark.Value < 5)
                                    {
                                        CHT++;
                                        NotHSXS++;
                                    }
                                    else if (objRatedComment.PeriodicEndingMark.Value < 9)
                                    {
                                        NotHSXS++;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!isExempted)
                        {
                            FullMark++;
                        }
                    }
                    lstSubjectViewModel.Add(objSubjectViewModel);
                }
                lstRatedtmp = lstRatedCommentPupil.Where(p => p.PupilID == PupilID && (p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY)).ToList();
                if (lstRatedtmp.Count > 0)
                {
                    for (int j = 0; j < lstRatedtmp.Count; j++)
                    {
                        objRatedComment = lstRatedtmp[j];
                        if (objRatedComment.EvaluationID == GlobalConstants.EVALUATION_CAPACITY)
                        {
                            if (objRatedComment.SubjectID == 1)
                            {
                                objResult.NL1 = objRatedComment.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.CapacityEndingEvaluation == 2
                                ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            }
                            else if (objRatedComment.SubjectID == 2)
                            {
                                objResult.NL2 = objRatedComment.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.CapacityEndingEvaluation == 2
                                ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            }
                            else
                            {
                                objResult.NL3 = objRatedComment.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.CapacityEndingEvaluation == 2
                                ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            }

                            if (!objRatedComment.CapacityEndingEvaluation.HasValue)
                            {
                                FullMark++;
                            }
                            else if (objRatedComment.CapacityEndingEvaluation.Value == 3)
                            {
                                CHT++;
                                NotHSXS++;
                            }
                            else if (objRatedComment.CapacityEndingEvaluation.Value == 2)
                            {
                                NotHSXS++;
                            }
                        }
                        else
                        {
                            if (objRatedComment.SubjectID == 4)
                            {
                                objResult.PC1 = objRatedComment.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.QualityEndingEvaluation == 2
                                ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            }
                            else if (objRatedComment.SubjectID == 5)
                            {
                                objResult.PC2 = objRatedComment.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.QualityEndingEvaluation == 2
                                ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            }
                            else if (objRatedComment.SubjectID == 6)
                            {
                                objResult.PC3 = objRatedComment.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.QualityEndingEvaluation == 2
                                ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            }
                            else
                            {
                                objResult.PC4 = objRatedComment.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedComment.QualityEndingEvaluation == 2
                                ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedComment.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                            }

                            if (!objRatedComment.QualityEndingEvaluation.HasValue)
                            {
                                FullMark++;
                            }
                            else if (objRatedComment.QualityEndingEvaluation == 3)
                            {
                                CHT++;
                                NotHSXS++;
                            }
                            else if (objRatedComment.QualityEndingEvaluation == 2)
                            {
                                NotHSXS++;
                            }
                        }
                    }
                }
                else
                {
                    FullMark++;
                }
                objResult.isFullMark = FullMark > 0 ? false : true;
                objResult.isHT = CHT > 0 ? false : true;
                objResult.isHSXS = NotHSXS > 0 ? false : true;
                objResult.lstSubjectViewModel = lstSubjectViewModel;
                //lay ket qua cuoi nam
                objSEE = lstSEE.Where(p => p.PupilID == PupilID).OrderBy(x => x.CreateTime).ThenBy(y => y.UpdateTime).FirstOrDefault();
                if (objSEE != null)
                {
                    objResult.EvaluationResult = objSEE.EndingEvaluation;
                    objResult.RewardName = objSEE.Reward.HasValue && objSEE.Reward.Value > 0 ? "Học sinh xuất sắc" : "";
                }
                //lay danh gia bo sung
                objSEE = lstSEE.Where(p => p.PupilID == PupilID).OrderBy(x => x.CreateTime).ThenBy(y => y.UpdateTime).FirstOrDefault();
                if (objSEE != null)
                {
                    objResult.RateAdd = objSEE.RateAdd;
                }
                lstResult.Add(objResult);
            }
            return lstResult;
        }
        [ValidateAntiForgeryToken]
        public JsonResult AddMenuUsual(string url)
        {
            Menu objMenu = SMAS.Business.Common.StaticData.ListMenu.Where(c => c.URL.Contains(url)).FirstOrDefault();

            List<BookmarkedFunction> lstBm = BookmarkedFunctionBusiness.GetBookmarkedFunctionOfUser(_globalInfo.SchoolID.GetValueOrDefault(),
                _globalInfo.AcademicYearID.GetValueOrDefault(), _globalInfo.AppliedLevel.GetValueOrDefault(), _globalInfo.UserAccountID);

            BookmarkedFunction objDB = lstBm.Where(p => p.MenuID == objMenu.MenuID).FirstOrDefault();

            if (objDB == null)
            {
                BookmarkedFunction bf = new BookmarkedFunction();
                bf.SchoolID = _globalInfo.SchoolID.GetValueOrDefault();
                bf.AcademicYearID = _globalInfo.AcademicYearID;
                bf.LevelID = _globalInfo.AppliedLevel;
                bf.UserID = _globalInfo.UserAccountID;
                bf.MenuID = objMenu.MenuID;
                bf.Semester = _globalInfo.Semester;
                string name = (string)HttpContext.GetGlobalResourceObject("MenuResources", objMenu.MenuName);
                bf.FunctionName = string.IsNullOrEmpty(name) ? objMenu.MenuName : name;
                bf.Order = lstBm.Count + 1;
                bf.CreatedDate = DateTime.Now;

                BookmarkedFunctionBusiness.Insert(bf);
                BookmarkedFunctionBusiness.Save();
            }
            return Json(new JsonMessage("Đã thêm vào danh sách chức năng thường dùng.", "success"));
        }
        private void SetVisbileButton(int semester, int classID)
        {
            bool isAdminShcool = UtilsBusiness.HasHeadAdminSchoolPermission(_globalInfo.UserAccountID);
            bool isGVCN = UtilsBusiness.HasHeadTeacherPermission(_globalInfo.UserAccountID, classID);
            DateTime dateTimeNow = DateTime.Now.Date;
            AcademicYear objAca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            if (!(isAdminShcool || isGVCN))
            {
                ViewData[GeneralSummedEvaluationConstant.DISABLED_BUTTON] = false;
            }
            else if (isAdminShcool)
            {
                if (dateTimeNow < objAca.FirstSemesterStartDate.Value || (objAca.FirstSemesterEndDate < dateTimeNow && dateTimeNow < objAca.SecondSemesterStartDate)
                    || dateTimeNow > objAca.SecondSemesterEndDate)//ngoài thời gian năm học
                {
                    ViewData[GeneralSummedEvaluationConstant.DISABLED_BUTTON] = false;
                }
                else
                {
                    ViewData[GeneralSummedEvaluationConstant.DISABLED_BUTTON] = true;
                }
            }
            else if (isGVCN)//chỉ được thao tác trong học kỳ hiện tại
            {
                if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    if (dateTimeNow < objAca.FirstSemesterStartDate.Value || dateTimeNow > objAca.FirstSemesterEndDate)
                    {
                        ViewData[GeneralSummedEvaluationConstant.DISABLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[GeneralSummedEvaluationConstant.DISABLED_BUTTON] = true;
                    }
                }
                else if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    if (dateTimeNow < objAca.SecondSemesterStartDate.Value || dateTimeNow > objAca.SecondSemesterEndDate.Value)
                    {
                        ViewData[GeneralSummedEvaluationConstant.DISABLED_BUTTON] = false;
                    }
                    else
                    {
                        ViewData[GeneralSummedEvaluationConstant.DISABLED_BUTTON] = true;
                    }
                }
            }
        }

    }
}
