﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.GeneralSummedEvaluationArea
{
    public class GeneralSummedEvaluationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "GeneralSummedEvaluationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "GeneralSummedEvaluationArea_default",
                "GeneralSummedEvaluationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
