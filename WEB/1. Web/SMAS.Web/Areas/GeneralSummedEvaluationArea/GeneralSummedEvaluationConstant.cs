﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.GeneralSummedEvaluationArea
{
    public class GeneralSummedEvaluationConstant
    {
        public const string LIST_SEMESTER = "listSemester";
        public const string LIST_EDUCATIONLEVEL = "listEducationLevel";
        public const string LIST_CLASS = "listClass";
        public const string CLASS_PROFILE = "clasProfile";
        public const string LIST_SUBJECT = "listSubject";
        public const string LIST_GENERAL_SUMMED_EVALUATION = "listGeneralSummedEvaluation";
        public const string SEMESTER_ID = "SemesterID";
        public const string CHECK_CLASS_NULL = "CheckClassNull";
        public const string CHECK_SUBJECT_NULL = "CheckSubjectNull";
        public const string DISABLED_BUTTON = "DisabledButton";
        public const string FORMAT_DATETIME = "dd/MM/yyyy";
        public const string EVALUATION_RESULT_HT = "Hoàn thành";
        public const string EVALUATION_RESULT_CHT = "Chưa hoàn thành";
        public const string EVALUATION_RESULT_CDG = "Chưa đánh giá";
        public const string LIST_RATE_ADD = "ListRateAdd";
        public const string ENABLE_BTN_SAVE_RATEADD = "EnablebtnSaveRateAdd";
        public const string LIST_RATED_COMMENT_PUPIL = "lstRatedCommentPupil";
        public const string LIST_SUBJECT_VIEW_MODEL = "lstSubjectViewModel";
        public const string LIST_CAPQUA_VIEW_MODEL = "lstCapQuaViewModel";
        public const string LIST_EXEMPTED_SUBJECT = "lstExemptedSubject";
        public const string FILE_TEMPLATE = "BangTongHopDanhGia_Lop1A.xls";
        public const int CHECK_RATE_HT = 1;
        public const int CHECK_RATE_CHT = 2;
        public const int CHECK_RATE_CDG = 3;
    }
}