﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.GeneralSummedEvaluationArea.Models
{
    public class SubjectRatedCommentViewModel
    {
        public int PupilID { get; set; }
        public int SubjectID1 { get; set; }
        public string SubjectName1 { get; set; }
        public int? MarkSubject1 { get; set; }
        public int? isCommenting1 { get; set; }
        public string EndingSubject1 { get; set; }
        public int SubjectID2 { get; set; }
        public string SubjectName2 { get; set; }
        public int? MarkSubject2 { get; set; }
        public int? isCommenting2 { get; set; }
        public string EndingSubject2 { get; set; }
        public bool isExempted { get; set; }
    }
}