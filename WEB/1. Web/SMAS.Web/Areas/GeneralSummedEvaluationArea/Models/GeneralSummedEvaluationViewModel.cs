﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.GeneralSummedEvaluationArea.Models
{
    public class SummedEvaluationViewModel
    {
        public int PupilID { get; set; }
        public string PupilCode { get; set; }
        public string FullName { get; set; }
        public int Status { get; set; }
        public int? OrderInClass { get; set; }
        public string Name { get; set; }
        public string EvaluationResult { get; set; }//kết quả đánh giá
        public bool IsMove { get; set; }
        public int? RateAdd { get; set; }
        public int? RewardID { get; set; }
        public string RewardName { get; set; }
        public bool isHSXS { get; set; }
        public bool isHT { get; set; }
        public bool isFullMark { get; set; }
        public int ClassID { get; set; }
        public int AcademicYearID { get; set; }
        public int EducationLevelID { get; set; }
        public int EvaluationID { get; set; }
        public string NL1 { get; set; }
        public string NL2 { get; set; }
        public string NL3 { get; set; }
        public string PC1 { get; set; }
        public string PC2 { get; set; }
        public string PC3 { get; set; }
        public string PC4 { get; set; }
        public List<SubjectEvaluationViewModel> lstSubjectViewModel { get; set; }
    }
    public class SubjectEvaluationViewModel
    {
        public int SubjectID { get; set; }
        public int? isCommenting { get; set; }
        public string SubjectName { get; set; }
        public int? OrderInSubject { get; set; }
        public string Abbreviation { get; set; }
        public string DG { get; set; }
        public int? KTDK { get; set; }
    }
}