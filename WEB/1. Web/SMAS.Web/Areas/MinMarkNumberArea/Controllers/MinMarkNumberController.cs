﻿using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Web.Areas.MinMarkNumberArea.Models;
using SMAS.Web.Controllers;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.MinMarkNumberArea.Controllers
{
    public class MinMarkNumberController:BaseController
    {
        #region properties
        private readonly IClassSubjectBusiness ClassSubjectBusiness;
        private readonly IEducationLevelBusiness EducationLevelBusiness ;
        private readonly IClassProfileBusiness ClassProfileBusiness;
        private readonly ISchoolProfileBusiness SchoolProfileBusiness;
        private readonly IAcademicYearBusiness AcademicYearBusiness;
        
        private List<EducationLevel> lstEducationLevel;
        private List<ClassProfile> lstClass;
        #endregion

        #region Constructor
        public MinMarkNumberController(IClassSubjectBusiness ClassSubjectBusiness, IEducationLevelBusiness EducationLevelBusiness,
            IClassProfileBusiness ClassProfileBusiness, ISchoolProfileBusiness SchoolProfileBusiness, IAcademicYearBusiness AcademicYearBusiness)
        {
            this.ClassSubjectBusiness = ClassSubjectBusiness;
            this.EducationLevelBusiness = EducationLevelBusiness;
            this.ClassProfileBusiness = ClassProfileBusiness;
            this.SchoolProfileBusiness = SchoolProfileBusiness;
            this.AcademicYearBusiness = AcademicYearBusiness;
        }
        #endregion

        #region Actions
       
        public ActionResult Index()
        {
            SetViewData();

            //Lay danh sach mon hoc cua lop
            int? educationLevelID = null;
            if (lstEducationLevel.Count > 0)
            {
                educationLevelID = lstEducationLevel.First().EducationLevelID;
            }

            int? classID = null;
            if (lstClass.Count > 0)
            {
                classID = lstClass.First().ClassProfileID;
            }

            ViewData[MinMarkNumberConstants.LIST_RESULT] = new List<ListViewModel>();
            if (educationLevelID != null && classID != null)
            {
                ViewData[MinMarkNumberConstants.LIST_RESULT] = _Search(educationLevelID.Value, classID.Value);
            }

            ViewData[MinMarkNumberConstants.CLASS_ID] = classID;
            ViewData[MinMarkNumberConstants.EDUCATION_LEVEL] = educationLevelID;

            ViewData[MinMarkNumberConstants.PER_CHECK_BUTTON] = CheckButtonPermission();

            return View();
        }

        public PartialViewResult Search(SearchViewModel form)
        {
            

            Utils.Utils.TrimObject(form);

            List<ListViewModel> lstResult=new List<ListViewModel>();

            if (form.EducationLevelID != null && form.ClassID != null)
            {
                lstResult = _Search(form.EducationLevelID.Value, form.ClassID.Value);
            }

            ViewData[MinMarkNumberConstants.CLASS_ID] = form.ClassID;
            ViewData[MinMarkNumberConstants.EDUCATION_LEVEL] = form.EducationLevelID;
            ViewData[MinMarkNumberConstants.PER_CHECK_BUTTON] = CheckButtonPermission();
            return PartialView("_List", lstResult);
        }

        public JsonResult AjaxLoadClass(int educationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();

            dic.Add("EducationLevelID", educationLevelID);
            dic.Add("SchoolID", _globalInfo.SchoolID);
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel);
            lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic)
                .OrderBy(p => p.OrderNumber.HasValue ? p.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();

            return Json(new SelectList(lstClass, "ClassProfileID", "DisplayName"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Save(List<ListViewModel> lstModel, int classId, bool isConfirm)
        {
            if (!IsCurrentYear() || GetMenupermission("MinMarkNumber", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            if (lstModel == null)
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Save")));
            }

            bool isError = false;
            string strError = String.Empty;

            if (!isConfirm)
            {
                ValidateData(new List<int> { classId }, lstModel, ref isError, ref strError);
            }

            if (!isError)
            {
                SaveData(new List<int> { classId }, lstModel);
                return Json(new JsonMessage(Res.Get("Common_Label_Save")));
            }
            else
            {
                strError = "Môn " + strError + "có số con điểm KTtx tối thiểu nhỏ hơn quy định của Bộ Giáo dục & Đào tạo. Thầy cô có muốn thực hiện lưu không?";
                return Json(new JsonMessage(strError,"ERROR"));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveEdu(List<ListViewModel> lstModel, int? educationLevel, bool isConfirm)
        {
            if (!IsCurrentYear() || GetMenupermission("MinMarkNumber", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (lstModel == null)
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Save")));
            }

            //Lay ra cac lop cua khoi
            List<ClassProfile> lstClass = new List<ClassProfile>();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            if (educationLevel != null)
            {
                dic.Add("EducationLevelID", educationLevel);
                dic.Add("SchoolID", _globalInfo.SchoolID);
                dic.Add("AppliedLevel", _globalInfo.AppliedLevel);
                lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic).ToList();
            }

            bool isError = false;
            string strError = String.Empty;
            List<int> lstClassId = lstClass.Select(o => o.ClassProfileID).ToList();

            if (!isConfirm)
            {
                ValidateData(lstClassId, lstModel, ref isError, ref strError);
            }

            if (!isError)
            {
                SaveData(lstClassId, lstModel);
                return Json(new JsonMessage(Res.Get("Common_Label_Save")));
            }
            else
            {
                strError = "Môn " + strError + "có số con điểm KTtx tối thiểu nhỏ hơn quy định của Bộ Giáo dục & Đào tạo. Thầy cô có muốn thực hiện lưu không?";
                return Json(new JsonMessage(strError, "ERROR"));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveSchool(List<ListViewModel> lstModel, bool isConfirm)
        {
            if (!IsCurrentYear() || GetMenupermission("MinMarkNumber", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }

            if (lstModel == null)
            {
                return Json(new JsonMessage(Res.Get("Common_Label_Save")));
            }

            //Lay ra cac lop cua truong
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("SchoolID", _globalInfo.SchoolID);
            dic.Add("AppliedLevel", _globalInfo.AppliedLevel);
            List<ClassProfile> lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic).ToList();

            bool isError = false;
            string strError = String.Empty;
            List<int> lstClassId = lstClass.Select(o => o.ClassProfileID).ToList();

            if (!isConfirm)
            {
                ValidateData(lstClassId, lstModel, ref isError, ref strError);
            }

            if (!isError)
            {
                SaveData(lstClassId, lstModel);
                return Json(new JsonMessage(Res.Get("Common_Label_Save")));
            }
            else
            {
                strError = "Môn " + strError + "có số con điểm KTtx tối thiểu nhỏ hơn quy định của Bộ Giáo dục & Đào tạo. Thầy cô có muốn thực hiện lưu không?";
                return Json(new JsonMessage(strError, "ERROR"));
            }

        }
        #endregion

        #region Private methods
        private void SetViewData()
        {
            //Lay danh sach khoi
            lstEducationLevel = _globalInfo.EducationLevels;

            int? defaultEducationLevelID = null;
            if (lstEducationLevel.Count > 0)
            {
                defaultEducationLevelID = lstEducationLevel.First().EducationLevelID;
            }
            ViewData[MinMarkNumberConstants.CBO_EDUCATION_LEVEL] = new SelectList(lstEducationLevel, "EducationLevelID", "Resolution");

            //Lay danh sach lop
            if (defaultEducationLevelID != null)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();

                dic.Add("EducationLevelID", defaultEducationLevelID);
                dic.Add("SchoolID", _globalInfo.SchoolID);
                dic.Add("AppliedLevel", _globalInfo.AppliedLevel);
                lstClass = ClassProfileBusiness.SearchByAcademicYear(_globalInfo.AcademicYearID.Value, dic)
                    .OrderBy(p => p.OrderNumber.HasValue ? p.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
            }
            else
            {
                lstClass = new List<ClassProfile>();
            }
            ViewData[MinMarkNumberConstants.CBO_CLASS] = new SelectList(lstClass, "ClassProfileID", "DisplayName");
        }


        private List<ListViewModel> _Search(int educationLevelID, int classID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["EducationLevelID"] = educationLevelID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;

            List<ListViewModel> lstResult = ClassSubjectBusiness.SearchByClass(classID, dic)
                                                 .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName)
                                                 .Select(o => new ListViewModel
                                                 {
                                                     Abbreviation = o.SubjectCat.Abbreviation,
                                                     AppliedType = o.AppliedType,
                                                     ClassSubjectID = o.ClassSubjectID,
                                                     FirstSemesterSectionPerWeek = o.SectionPerWeekFirstSemester,
                                                     FirstSemesterMinM = o.MMinFirstSemester,
                                                     FirstSemesterMinP = o.PMinFirstSemester,
                                                     FirstSemesterMinV = o.VMinFirstSemester,
                                                     SecondSemesterSectionPerWeek = o.SectionPerWeekSecondSemester,
                                                     SecondSemesterMinM = o.MMinSecondSemester,
                                                     SecondSemesterMinP = o.PMinSecondSemester,
                                                     SecondSemesterMinV = o.VMinSecondSemester,
                                                     SubjectID = o.SubjectID,
                                                     SubjectName = o.SubjectCat.DisplayName
                                                 }).ToList();

            return lstResult;
        }

        /// <summary>
        /// Validate du lieu
        /// </summary>
        /// <param name="lstClassId"></param>
        /// <param name="lstModel"></param>
        /// <param name="isError"></param>
        /// <param name="strError"></param>
        private void ValidateData(List<int> lstClassId, List<ListViewModel> lstModel, ref bool isError, ref string strError)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = _globalInfo.SchoolID;
            dic["AcademicYearID"] = _globalInfo.AcademicYearID;
            dic["AppliedLevel"] = _globalInfo.AppliedLevel;
            dic["ListClassID"] = lstClassId;

            List<ClassSubject> lstClassSubjectAll = ClassSubjectBusiness.SearchBySchool(_globalInfo.SchoolID.Value, dic).ToList();
            List<int> lstErrorSubjectId = new List<int>();

            ListViewModel model;
            List<ClassSubject> lstClassSubject;
            ClassSubject cs;
            for (int i = 0; i < lstModel.Count; i++)
            {
                model = lstModel[i];
                lstClassSubject = lstClassSubjectAll.Where(o => o.SubjectID == model.SubjectID).ToList();

                for (int j = 0; j < lstClassSubject.Count; j++)
                {
                    cs = lstClassSubject[j];

                    //Kiem tra du lieu
                    //Lay so con diem theo thong tu 58 
                    int FirstSemesterKTTX = GetKTTXMarkNumber(cs.SectionPerWeekFirstSemester);
                    int SecondSemesterKTTX = GetKTTXMarkNumber(cs.SectionPerWeekSecondSemester);

                    if ((model.FirstSemesterMinM.GetValueOrDefault() + model.FirstSemesterMinP.GetValueOrDefault() > 0
                        && model.FirstSemesterMinM.GetValueOrDefault() + model.FirstSemesterMinP.GetValueOrDefault() < FirstSemesterKTTX)
                        || (model.SecondSemesterMinM.GetValueOrDefault() + model.SecondSemesterMinP.GetValueOrDefault() > 0
                        && model.SecondSemesterMinM.GetValueOrDefault() + model.SecondSemesterMinP.GetValueOrDefault() < SecondSemesterKTTX))
                    {
                        if (!lstErrorSubjectId.Contains(cs.SubjectID))
                        {
                            isError = true;
                            strError = strError + cs.SubjectCat.DisplayName + ", ";
                            lstErrorSubjectId.Add(cs.SubjectID);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Luu du lieu
        /// </summary>
        /// <param name="lstClassId"></param>
        /// <param name="lstModel"></param>
        private void SaveData(List<int> lstClassId, List<ListViewModel> lstModel)
        {
            List<ClassSubject> lstCs = lstModel.Select(o => new ClassSubject
                {
                    ClassSubjectID = o.ClassSubjectID,
                    SubjectID = o.SubjectID,
                    MMinFirstSemester = o.FirstSemesterMinM,
                    PMinFirstSemester = o.FirstSemesterMinP,
                    VMinFirstSemester = o.FirstSemesterMinV,
                    MMinSecondSemester = o.SecondSemesterMinM,
                    PMinSecondSemester = o.SecondSemesterMinP,
                    VMinSecondSemester = o.SecondSemesterMinV

                }).ToList();

            ClassSubjectBusiness.SaveMinMarkNumber(lstClassId, lstCs, _globalInfo.SchoolID, _globalInfo.AcademicYearID, _globalInfo.AppliedLevel);
        }

        private int GetKTTXMarkNumber(decimal? sectionPerWeek)
        {
            int num = 0;
            if (sectionPerWeek <= 1)
            {
                num = 2;
            }
            else if (sectionPerWeek > 1 && sectionPerWeek < 3)
            {
                num = 3;
            }
            else if (sectionPerWeek >= 3)
            {
                num = 4;
            }

            return num;
        }
        private bool CheckButtonPermission()
        {

            return IsCurrentYear() &&
                GetMenupermission("MinMarkNumber", _globalInfo.UserAccountID, _globalInfo.IsAdmin, _globalInfo.RoleID) >= SystemParamsInFile.PER_CREATE;
        }

        private bool IsCurrentYear()
        {
            bool isCurrentYear = false;
            AcademicYear aca = AcademicYearBusiness.Find(_globalInfo.AcademicYearID);
            DateTime nowDate = DateTime.Now.Date;
            if ((DateTime.Compare(nowDate, aca.FirstSemesterStartDate.Value) >= 0 && DateTime.Compare(nowDate, aca.FirstSemesterEndDate.Value) <= 0) ||
                (DateTime.Compare(nowDate, aca.SecondSemesterStartDate.Value) >= 0 && DateTime.Compare(nowDate, aca.SecondSemesterEndDate.Value) <= 0))
            {
                isCurrentYear = true;
            }
            return isCurrentYear;
        }
        #endregion
    }
}