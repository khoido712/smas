﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.MinMarkNumberArea
{
    public class MinMarkNumberAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "MinMarkNumberArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "MinMarkNumberArea_default",
                "MinMarkNumberArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
