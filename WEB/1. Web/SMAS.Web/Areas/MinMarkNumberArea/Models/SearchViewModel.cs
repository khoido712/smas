﻿using SMAS.Web.Models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.MinMarkNumberArea.Models
{
    public class SearchViewModel
    {
        [ResourceDisplayName("Common_Label_Education_Level")]
        [UIHint("Combobox")]
        [AdditionalMetadata("ViewDataKey", MinMarkNumberConstants.CBO_EDUCATION_LEVEL)]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("OnChange", "onEducationLevelChange()")]
        public int? EducationLevelID { get; set; }

        [ResourceDisplayName("Common_Label_Class")]
        [UIHint("Combobox")]
        [AdditionalMetadata("PlaceHolder", "null")]
        [AdditionalMetadata("ViewDataKey", MinMarkNumberConstants.CBO_CLASS)]
        [AdditionalMetadata("OnChange", "onClassChange()")]
        public int? ClassID { get; set; }
    }
}