﻿using SMAS.Web.Models.Attributes;
using SMAS.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MinMarkNumberArea.Models
{
    public class ListViewModel
    {
        public long ClassSubjectID { get; set; }

        public int SubjectID { get; set; }

        [ResourceDisplayName("MinMarkNumber_SubjectName")]
        public string SubjectName { get; set; }

        [ResourceDisplayName("MinMarkNumber_Abbreviation")]
        public string Abbreviation { get; set; }

        public int? AppliedType { get; set; }
        public decimal? FirstSemesterSectionPerWeek { get; set; }
        public decimal? SecondSemesterSectionPerWeek { get; set; }

        [ResourceDisplayName("MinMarkNumber_FirstSemesterSectionPerWeek")]
        public string StrFirstSemesterSectionPerWeek
        {
            get
            {
                if (FirstSemesterSectionPerWeek.HasValue)
                {
                    if (FirstSemesterSectionPerWeek.Value < 10 && FirstSemesterSectionPerWeek.Value % 1 != 0)
                    {
                        return FirstSemesterSectionPerWeek.Value.ToString("N1");
                    }
                    else
                    {
                        return FirstSemesterSectionPerWeek.Value.ToString();
                    }
                }
                else
                {
                    return "0";
                }
            }
        }

        [ResourceDisplayName("MinMarkNumber_SecondSemesterSectionPerWeek")]
        public string StrSecondSemesterSectionPerWeek
        {
            get
            {
                if (SecondSemesterSectionPerWeek.HasValue)
                {
                    if (SecondSemesterSectionPerWeek.Value < 10 && SecondSemesterSectionPerWeek.Value % 1 != 0)
                    {
                        return SecondSemesterSectionPerWeek.Value.ToString("N1");
                    }
                    else
                    {
                        return SecondSemesterSectionPerWeek.Value.ToString();
                    }
                }
                else
                {
                    return "0";
                }
            }
        }

        public int? FirstSemesterMinM { get; set; }
        public int? FirstSemesterMinP { get; set; }
        public int? FirstSemesterMinV { get; set; }
        public int? SecondSemesterMinM { get; set; }
        public int? SecondSemesterMinP { get; set; }
        public int? SecondSemesterMinV { get; set; }

        [ResourceDisplayName("MinMarkNumber_AppliedType")]
        public string StrAppliedType
        {
            get
            {
                switch (AppliedType)
                {
                    case 0:
                        return Res.Get("AppliedTypeSubject_Require");
                    case 1:
                        return Res.Get("AppliedTypeSubject_Choice");
                    case 2:
                        return Res.Get("AppliedTypeSubject_ChoiceAddPiority");
                    case 3:
                        return Res.Get("AppliedTypeSubject_ChoiceCalculatorMark");
                    case 4:
                        return Res.Get("AppliedTypeSubject_ApparentShip");
                    default:
                        return String.Empty;
                }

            }
        }
    }
}