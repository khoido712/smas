﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMAS.Web.Areas.MinMarkNumberArea
{
    public class MinMarkNumberConstants
    {
        public const string CBO_EDUCATION_LEVEL = "cbo_education_level";
        public const string CBO_CLASS = "cbo_class";
        public const string LIST_RESULT = "list_result";
        public const string CLASS_ID = "class_id";
        public const string EDUCATION_LEVEL = "education_level";
        public const string PER_CHECK_BUTTON = "per_check_button";
    }
}