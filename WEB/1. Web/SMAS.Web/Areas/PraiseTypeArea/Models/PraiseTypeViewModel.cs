/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.PraiseTypeArea.Models
{
    public class PraiseTypeViewModel
    {
         [ScaffoldColumn(false)]
		public System.Int32 PraiseTypeID { get; set; }

         [ScaffoldColumn(false)]
         public int? SchoolID { get; set; }


        [ResourceDisplayName("PraiseType_Column_Resolution")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.String Resolution { get; set; }


        [ResourceDisplayName("PraiseType_Column_ConductMark")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.Nullable<decimal> ConductMark { get; set; }

        [ResourceDisplayName("PraiseType_Column_GraduationMark")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.Nullable<decimal> GraduationMark { get; set; }       				
	       
    }
}


