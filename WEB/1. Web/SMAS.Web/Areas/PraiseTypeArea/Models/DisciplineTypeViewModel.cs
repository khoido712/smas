/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
using System.Web.Mvc;
namespace SMAS.Web.Areas.PraiseTypeArea.Models
{
    public class DisciplineTypeViewModel
    {

        [ScaffoldColumn(false)]
        public System.Int32 DisciplineTypeID { get; set; }

        [ScaffoldColumn(false)]
        public int? SchoolID { get; set; }


        [ResourceDisplayName("DisciplineType_Column_Resolution")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        public System.String Resolution { get; set; }


        [ResourceDisplayName("DisciplineType_Label_ConductMinusMark")]
        [UIHint("checkbox")]
        [AdditionalMetadata("OnChange", "chk_onchange(this)")]
        public bool chkConductMinusMark { get; set; }

        [ResourceDisplayName("DisciplineType_Column_ConductMinusMark")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "requireResourceKey")]
        public System.Nullable<System.Int32> ConductMinusMark { get; set; }


    }
}


