﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.PraiseTypeArea
{
    public class PraiseTypeAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PraiseTypeArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PraiseTypeArea_default",
                "PraiseTypeArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
