﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  tungnd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.PraiseTypeArea.Models;
using SMAS.Web.Areas.PraiseTypeArea;


namespace SMAS.Web.Areas.PraiseTypeArea.Controllers
{
    public class DisciplineTypeController : BaseController
    {
        private readonly IDisciplineTypeBusiness DisciplineTypeBusiness;

        public DisciplineTypeController(IDisciplineTypeBusiness disciplinetypeBusiness)
        {
            this.DisciplineTypeBusiness = disciplinetypeBusiness;
        }

        //
        // GET: /DisciplineType/

        #region SetViewData

        private void SetViewData()
        {

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            IEnumerable<DisciplineTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[PraiseTypeConstants.LIST_DISCIPLINETYPE] = lst;


        }

        #endregion

        #region Index

        public ActionResult Index()
        {
            SetViewDataPermission("PraiseType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            SetViewData();
            return View();
        }

        #endregion


        #region Search

        //
        // GET: /DisciplineType/Search

        
        public PartialViewResult Search(SearchDisciplineTypeViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            SearchInfo["Resolution"] = frm.Resolution;
            SearchInfo["IsActive"] = true;

            IEnumerable<DisciplineTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[PraiseTypeConstants.LIST_DISCIPLINETYPE] = lst;

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<DisciplineTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("PraiseType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<DisciplineType> query = this.DisciplineTypeBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            IQueryable<DisciplineTypeViewModel> lst = query.Select(o => new DisciplineTypeViewModel
            {
                DisciplineTypeID = o.DisciplineTypeID,
                Resolution = o.Resolution,
                ConductMinusMark = o.ConductMinusMark,
                SchoolID = o.SchoolID

            });
            return lst.ToList().OrderBy(p=>SMAS.Business.Common.Utils.SortABC(p.Resolution));
        }

        #endregion

        #region Create

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("PraiseType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            DisciplineType disciplinetype = new DisciplineType();
            TryUpdateModel(disciplinetype);
            disciplinetype.IsActive = true;
            GlobalInfo g = new GlobalInfo();
            disciplinetype.SchoolID = g.SchoolID.Value;
            Utils.Utils.TrimObject(disciplinetype);

            this.DisciplineTypeBusiness.Insert(disciplinetype);
            this.DisciplineTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        #endregion


        #region Edit

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int DisciplineTypeID)
        {
            this.CheckPermissionForAction(DisciplineTypeID, "DisciplineType");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("PraiseType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            DisciplineType disciplinetype = this.DisciplineTypeBusiness.Find(DisciplineTypeID);
            if (disciplinetype.SchoolID == null)
            {
                return Json(new JsonMessage(Res.Get("DisciplineType_No_Permission"), "error"));
            }
            TryUpdateModel(disciplinetype);
            disciplinetype.ConductMinusMark = Request["ConductMinusMark"] == null ? null : disciplinetype.ConductMinusMark;
            Utils.Utils.TrimObject(disciplinetype);
            this.DisciplineTypeBusiness.Update(disciplinetype);
            this.DisciplineTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        #endregion

        #region Delete

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "DisciplineType");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("PraiseType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            DisciplineType disciplinetype = DisciplineTypeBusiness.Find(id);
            if (disciplinetype.SchoolID == null)
            {
                return Json(new JsonMessage(Res.Get("DisciplineType_No_Permission"), "error"));
            }
            this.DisciplineTypeBusiness.Delete(id);
            this.DisciplineTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }


        #endregion

    }
}





