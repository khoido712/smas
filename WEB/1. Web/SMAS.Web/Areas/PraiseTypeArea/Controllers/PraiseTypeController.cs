﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  tungnd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Business.IBusiness;
using SMAS.Web.Controllers;
using SMAS.Web.Areas.PraiseTypeArea.Models;

using SMAS.Models.Models;

namespace SMAS.Web.Areas.PraiseTypeArea.Controllers
{
    public class PraiseTypeController : BaseController
    {        
        private readonly IPraiseTypeBusiness PraiseTypeBusiness;
        private readonly IDisciplineTypeBusiness DisciplineTypeBusiness;
		public PraiseTypeController (IPraiseTypeBusiness praisetypeBusiness
            , IDisciplineTypeBusiness DisciplineTypeBusiness)
		{
			this.PraiseTypeBusiness = praisetypeBusiness;
            this.DisciplineTypeBusiness = DisciplineTypeBusiness;
		}
		
		//
        // GET: /PraiseType/

        #region SetViewData

        private void SetViewData()
        {

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            IEnumerable<PraiseTypeViewModel> lst = this._Search(SearchInfo); // Lay danh sach cac danh muc khen thuong
            ViewData[PraiseTypeConstants.LIST_PRAISETYPE] = lst;

            IEnumerable<DisciplineTypeViewModel> lstDisciplineType = this._SearchDisciplineType(SearchInfo); // Danh sach cac danh muc ky luat
            ViewData[PraiseTypeConstants.LIST_DISCIPLINETYPE] = lstDisciplineType;

        }

        #endregion

        #region Index
        public ActionResult Index()
        {
            SetViewDataPermission("PraiseType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            SetViewData();
            return View();
        }

        #endregion


        #region Search

        //
        // GET: /PraiseType/Search

        
        public PartialViewResult Search(string Resolution)
        {
         

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            
			//add search info
			//
            SearchInfo["Resolution"] = Resolution;
            SearchInfo["IsActive"] = true;

            IEnumerable<PraiseTypeViewModel> lst = this._Search(SearchInfo);
            ViewData[PraiseTypeConstants.LIST_PRAISETYPE] = lst;

            //Get view data here

            return PartialView("_List");
        }

        private IEnumerable<DisciplineTypeViewModel> _SearchDisciplineType(IDictionary<string, object> SearchInfo)
        {
            IQueryable<DisciplineType> query = this.DisciplineTypeBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            IQueryable<DisciplineTypeViewModel> lst = query.Select(o => new DisciplineTypeViewModel
            {
                DisciplineTypeID = o.DisciplineTypeID,
                Resolution = o.Resolution,
                ConductMinusMark = o.ConductMinusMark,
                SchoolID = o.SchoolID
            });
            return lst.ToList().OrderBy(p=>SMAS.Business.Common.Utils.SortABC(p.Resolution));
        }


        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<PraiseTypeViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("PraiseType", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<PraiseType> query = this.PraiseTypeBusiness.SearchBySchool(new GlobalInfo().SchoolID.Value, SearchInfo);
            IQueryable<PraiseTypeViewModel> lst = query.Select(o => new PraiseTypeViewModel
            {
                PraiseTypeID = o.PraiseTypeID,
                Resolution = o.Resolution,
                ConductMark = o.ConductMark,
                GraduationMark = o.GraduationMark,
                SchoolID = o.SchoolID
            });

            return lst.ToList().OrderBy(p=>SMAS.Business.Common.Utils.SortABC(p.Resolution));
        }

        #endregion

        #region Create

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("PraiseType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            PraiseType praisetype = new PraiseType();
            TryUpdateModel(praisetype);
            praisetype.IsActive = true;
            GlobalInfo g = new GlobalInfo();
            praisetype.SchoolID = g.SchoolID.Value;
            Utils.Utils.TrimObject(praisetype);
            
           
            this.PraiseTypeBusiness.Insert(praisetype);
            this.PraiseTypeBusiness.Save();
            
            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        #endregion

        #region Edit

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int PraiseTypeID)
        {
            this.CheckPermissionForAction(PraiseTypeID, "PraiseType");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("PraiseType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            PraiseType praisetype = this.PraiseTypeBusiness.Find(PraiseTypeID);
            if (praisetype.SchoolID == null)
            {
                return Json(new JsonMessage(Res.Get("PraiseType_No_Permission"),"error"));
            }
            TryUpdateModel(praisetype);

            praisetype.ConductMark = Request["ConductMark"] == null ? null : praisetype.ConductMark;
            praisetype.GraduationMark = Request["GraduationMark"] == null ? null : praisetype.GraduationMark;
            Utils.Utils.TrimObject(praisetype);
            GlobalInfo g = new GlobalInfo();
            
            praisetype.SchoolID = g.SchoolID.Value;
            praisetype.IsActive = true;
            this.PraiseTypeBusiness.Update(praisetype);
            this.PraiseTypeBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        #endregion

        #region Delete

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "PraiseType");
            GlobalInfo GlobalInfo = new GlobalInfo();
            PraiseType praisetype = PraiseTypeBusiness.Find(id);
            if (praisetype.SchoolID == null)
            {
                return Json(new JsonMessage(Res.Get("PraiseType_No_Permission"), "error"));
            }
            if (GetMenupermission("PraiseType", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            
            this.PraiseTypeBusiness.Delete(id);
            this.PraiseTypeBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }


        #endregion





    }
}





