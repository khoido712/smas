/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SMAS.Web.Areas.PraiseTypeArea
{
    public class PraiseTypeConstants
    {
        public const string LIST_PRAISETYPE = "listPraiseType";
        public const string LIST_DISCIPLINETYPE = "listDisciplineType";
        public const string RESOLUTION = "Resolution";
    }
}