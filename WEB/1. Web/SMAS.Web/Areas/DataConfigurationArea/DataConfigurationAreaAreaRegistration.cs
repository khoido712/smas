﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.DataConfigurationArea
{
    public class DataConfigurationAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DataConfigurationArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DataConfigurationArea_default",
                "DataConfigurationArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
