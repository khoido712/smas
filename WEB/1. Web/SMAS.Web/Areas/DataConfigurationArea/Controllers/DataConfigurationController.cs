﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMAS.Web.Areas.DataConfigurationArea.Controllers
{
    public class DataConfigurationController : Controller
    {
        //
        // GET: /DataConfigurationArea/DataConfiguration/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /DataConfigurationArea/DataConfiguration/Details/5


        [ValidateAntiForgeryToken]
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /DataConfigurationArea/DataConfiguration/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /DataConfigurationArea/DataConfiguration/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /DataConfigurationArea/DataConfiguration/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /DataConfigurationArea/DataConfiguration/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /DataConfigurationArea/DataConfiguration/Delete/5
 

        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /DataConfigurationArea/DataConfiguration/Delete/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [ValidateAntiForgeryToken]
        public PartialViewResult getUrl(int? menuOrder) { 
            string url = "";
            if(menuOrder==1)
            {
                url = "/SemeterDeclarationArea/SemeterDeclaration/";
            }
            else if(menuOrder==2)
            {
                url = "/PeriodDeclarationArea/PeriodDeclaration/";
            }
            else if(menuOrder==3)
            {
                url = "/CodeConfigArea/CodeConfig/";
            }
            else if(menuOrder==4)
            {
                url = "/LockedMarkDetailArea/LockedMarkDetail/";
            }
            else if (menuOrder == 5)
            {
                url = "/ConductConfigArea/ConductConfig/";
            }

            ViewData["url"] = url;
            return PartialView("_List");
        }
    }
}
