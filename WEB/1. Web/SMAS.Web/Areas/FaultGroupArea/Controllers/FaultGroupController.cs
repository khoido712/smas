﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Web.Areas.FaultGroupArea.Models;

namespace SMAS.Web.Areas.FaultGroupArea.Controllers
{
    // TODO : HieuND, Chua xu li phan phan quyen, bo phan SkipCheckRole
    public class FaultGroupController : BaseController
    {
        private readonly IFaultGroupBusiness FaultGroupBusiness;
        private readonly ISchoolFacultyBusiness SchoolFacultyBusiness;

        public FaultGroupController(IFaultGroupBusiness faultgroupBusiness, ISchoolFacultyBusiness schoolFacultyBusiness)
        {
            this.FaultGroupBusiness = faultgroupBusiness;
            this.SchoolFacultyBusiness = schoolFacultyBusiness;
        }

        //
        // GET: /FaultGroup/

        public ActionResult Index()
        {
            SetViewDataPermission("FaultGroup", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["IsActive"] = true;
            IEnumerable<FaultGroupViewModel> lst = this._Search(SearchInfo);
            ViewData[FaultGroupConstants.LIST_FAULTGROUP] = lst;
            return View();
        }

        //
        // GET: /FaultGroup/Search
        
        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();

            //Add search info - Navigate to Search function in business
            SearchInfo["IsActive"] = true;
            SearchInfo["Resolution"] = frm.Resolusion;

            IEnumerable<FaultGroupViewModel> lst = this._Search(SearchInfo);
            ViewData[FaultGroupConstants.LIST_FAULTGROUP] = lst;

            return PartialView("_List");
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("FaultGroup", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            FaultGroup faultgroup = new FaultGroup();
            TryUpdateModel(faultgroup);
            Utils.Utils.TrimObject(faultgroup);
            faultgroup.SchoolID = new GlobalInfo().SchoolID.GetValueOrDefault();
            this.FaultGroupBusiness.Insert(faultgroup);
            this.FaultGroupBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Edit(int FaultGroupID)
        {
            this.CheckPermissionForAction(FaultGroupID, "FaultGroup");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("FaultGroup", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            FaultGroup faultgroup = this.FaultGroupBusiness.Find(FaultGroupID);
            TryUpdateModel(faultgroup);
            Utils.Utils.TrimObject(faultgroup);
            faultgroup.SchoolID = new GlobalInfo().SchoolID.GetValueOrDefault();
            this.FaultGroupBusiness.Update(faultgroup);
            this.FaultGroupBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public JsonResult Delete(int id)
        {
            this.CheckPermissionForAction(id, "FaultGroup");
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("FaultGroup", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.FaultGroupBusiness.Delete(new GlobalInfo().SchoolID.GetValueOrDefault(), id);
            this.FaultGroupBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IEnumerable<FaultGroupViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("FaultGroup", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<FaultGroup> query = this.FaultGroupBusiness.SearchBySchool(new GlobalInfo().SchoolID.GetValueOrDefault(), SearchInfo);
            IQueryable<FaultGroupViewModel> lst = query.Select(o => new FaultGroupViewModel
            {
                FaultGroupID = o.FaultGroupID,
                Resolution = o.Resolution,
                Description = o.Description
            });
            List<FaultGroupViewModel> listFaultGroup = lst.ToList();
            listFaultGroup = listFaultGroup.OrderBy(o => o.Resolution).ToList();
            return listFaultGroup;
        }
    }
}





