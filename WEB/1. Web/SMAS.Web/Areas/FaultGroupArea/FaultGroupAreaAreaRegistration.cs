﻿using System.Web.Mvc;

namespace SMAS.Web.Areas.FaultGroupArea
{
    public class FaultGroupAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "FaultGroupArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "FaultGroupArea_default",
                "FaultGroupArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
