﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMAS.Web.Controllers;
using SMAS.Business.IBusiness;
using System.Collections;
using SMAS.Models.Models;
using SMAS.Web.Constants;
using SMAS.Web.Utils;
using SMAS.Business.Common;
using SMAS.Web.Filter;
using SMAS.Business.Business;
using SMAS.Web.Areas.AreaArea.Models;


namespace SMAS.Web.Areas.AreaArea.Controllers
{
    public class AreaController : BaseController
    {
        private readonly IAreaBusiness AreaBusiness;

        public AreaController(IAreaBusiness areaBusiness)
        {
            this.AreaBusiness = areaBusiness;
        }


        #region Index
        // GET: /Area/  
        public ActionResult Index()
        {
            SetViewDataPermission("Area", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //SearchInfo["IsActive"] = true;            
            //Get view data here

            IEnumerable<AreaViewModel> lst = this._Search(SearchInfo);
            ViewData[AreaConstants.LIST_AREA] = lst.ToList().OrderBy(o=>o.AreaName).ToList();
            return View();
        }

        #endregion

        #region search
        // GET: /Area/Search

        public PartialViewResult Search(SearchViewModel frm)
        {
            Utils.Utils.TrimObject(frm);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AreaName"] = frm.AreaName;
            SearchInfo["AreaCode"] = frm.AreaCode;
            //add search info
            //

            IEnumerable<AreaViewModel> lst = this._Search(SearchInfo);
            ViewData[AreaConstants.LIST_AREA] = lst.ToList().OrderBy(o => o.AreaName).ToList();

            //Get view data here

            return PartialView("_List");
        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        /// 
        private IEnumerable<AreaViewModel> _Search(IDictionary<string, object> SearchInfo)
        {
            SetViewDataPermission("Area", new GlobalInfo().UserAccountID, new GlobalInfo().IsAdmin);
            IQueryable<Area> query = this.AreaBusiness.Search(SearchInfo);
            IQueryable<AreaViewModel> lst = query.Select(o => new AreaViewModel
            {
                AreaID = o.AreaID,
                AreaCode = o.AreaCode,
                AreaName = o.AreaName,
                Description = o.Description,
                CreatedDate = o.CreatedDate,
                IsActive = o.IsActive,
                ModifiedDate = o.ModifiedDate


            });

            return lst.OrderBy(o=>o.AreaName).ToList();
        }

        #endregion

        #region Create
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create()
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Area", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_CREATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Area area = new Area();
            TryUpdateModel(area);
            area.IsActive = true;
            Utils.Utils.TrimObject(area);

            this.AreaBusiness.Insert(area);
            this.AreaBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_AddNewMessage")));
        }
        #endregion

        #region Update
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(int AreaID)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Area", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_UPDATE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            Area area = this.AreaBusiness.Find(AreaID);
            TryUpdateModel(area);
            Utils.Utils.TrimObject(area);
            this.AreaBusiness.Update(area);
            this.AreaBusiness.Save();

            return Json(new JsonMessage(Res.Get("Common_Label_UpdateSuccessMessage")));
        }
        #endregion

        #region Delete
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            GlobalInfo GlobalInfo = new GlobalInfo();
            if (GetMenupermission("Area", GlobalInfo.UserAccountID, GlobalInfo.IsAdmin) < SystemParamsInFile.PER_DELETE)
            {
                throw new BusinessException("Validate_Permission_Teacher");
            }
            this.AreaBusiness.Delete(id);
            this.AreaBusiness.Save();
            return Json(new JsonMessage(Res.Get("Common_Label_DeleteSuccessMessage")));
        }
        #endregion

        #region Detail

        public PartialViewResult Detail(int id)
        {
            Area area = this.AreaBusiness.Find(id);
            AreaViewModel frm = new AreaViewModel();
            Utils.Utils.BindTo(area, frm);
            return PartialView("_Detail", frm);
        }
        #endregion
    }
}





