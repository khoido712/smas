/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Web.Models.Attributes;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace SMAS.Web.Areas.AreaArea.Models
{
    public class AreaViewModel
    {
        [ScaffoldColumn(false)]
		public System.Int32 AreaID { get; set; }

        [ResourceDisplayName("Area_Label_AreaCode")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(10, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		public System.String AreaCode { get; set; }

        [ResourceDisplayName("Area_Label_AreaName")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_Required")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
		public System.String AreaName { get; set; }

        [ResourceDisplayName("Area_Label_Description")]
        [StringLength(400, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "Common_Validate_MaxLength")]
        [DataType(DataType.MultilineText)]
		public System.String Description { get; set; }

        [ResourceDisplayName("Created_Date")]
        [ScaffoldColumn(false)]
		public System.Nullable<System.DateTime> CreatedDate { get; set; }

        [ResourceDisplayName("IsActive")]
        [ScaffoldColumn(false)]
		public System.Nullable<System.Boolean> IsActive { get; set; }

        [ResourceDisplayName("ModifiedDate")]
        [ScaffoldColumn(false)]
		public System.Nullable<System.DateTime> ModifiedDate { get; set; }								
	       
    }
}


