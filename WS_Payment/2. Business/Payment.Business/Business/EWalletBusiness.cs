﻿using Payment.Business.IBusiness;
using Payment.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Payment.ServiceLoader.ILoader;
using Payment.Utility.Common;
using Payment.Utility.Constants;
using Payment.Utility.Object.ResponseObject;
using Newtonsoft.Json;

namespace Payment.Business.Business
{
    public class EWalletBusiness : GenericBusiness<EWallet>, IEWalletBusiness
    {
        #region declare/contructor
        private readonly ILogBusiness LogBusiness;

        public EWalletBusiness(ILogBusiness LogBusiness)
        {
            this.LogBusiness = LogBusiness;
        }
        #endregion

        #region lay thong tin vi dien tu cua user
        /// <summary>
        /// lay thong tin vi dien tu cua user
        /// </summary>
        /// <author>HaiVT</author>
        /// <param name="dic"></param>
        /// <returns></returns>
        public EWalletResponse GetEWallet(Dictionary<string, object> dic)
        {
            int userID = dic.GetInt("UserID");
            bool schoolUser = dic.GetBool("SchoolUser", false);
            bool supervisingDeptUser = dic.GetBool("SupervisingDeptUser", false);

            EWalletResponse response = new EWalletResponse();
            if (userID > 0 && (schoolUser || supervisingDeptUser))
            {
                var account = (from a in Context.Accounts
                               where a.UserID == userID
                               && ((schoolUser && a.UserType == GlobalConstants.USER_TYPE_SCHOOL) || (supervisingDeptUser && a.UserType == GlobalConstants.USER_TYPE_SUPER_VISING_DEPT))
                               select a).FirstOrDefault();

                if (account == null)
                {
                    account = new Account();
                    account.UserID = userID;
                    account.UserName = string.Empty;
                    account.Name = string.Empty;
                    account.UserType = schoolUser ? GlobalConstants.USER_TYPE_SCHOOL : GlobalConstants.USER_TYPE_SUPER_VISING_DEPT;
                    account.CreatedTime = DateTime.Now;
                    Context.Accounts.Add(account);
                    Context.SaveChanges();
                }

                var ewallet = Context.EWallets.FirstOrDefault(e=>e.AccountID == account.AccountID);

                // neu chua ton tai vi thi tao moi vi
                if (ewallet == null)
                {
                    ewallet = new EWallet();
                    ewallet.AccountID = account.AccountID;
                    ewallet.Balance = 0;
                    ewallet.CreatedTime = DateTime.Now;
                    ewallet.TopupFailCount = 0;
                    ewallet.IsLockedOut = false;
                    Context.EWallets.Add(ewallet);
                    Context.SaveChanges();
                }

                response.EWalletID = ewallet.EWalletID;
                response.Balance = ewallet.Balance;

                // Kiem tra trang thai khoa cua vi dien tu
                if (!ewallet.IsLockedOut) // neu ko bi khoa thi tra lai ket qua la khong bi khoa
                {
                    response.IsLockedOut = false;
                    response.RemainOfTimeToUnlock = new TimeSpan(0);
                }
                else // Neu dang bi khoa thi kiem tra xem co con trong thoi gian bi khoa hay khong. Neu khong con trong thoi gian bi khoa thi mo khoa cho nguoi dung
                {
                    if (ewallet.LastTopupFailDate.HasValue)
                    {
                        // thoi gian khoa
                        int lockOutMinute = Config.GetInt("LockMin");
                        TimeSpan lockOutTimeSpan = new TimeSpan(0, lockOutMinute, 0);
                        TimeSpan lockedOutTimeSpan = DateTime.Now - ewallet.LastTopupFailDate.Value;
                        if (lockedOutTimeSpan >= lockOutTimeSpan)
                        {
                            // Mo kho nguoi dung
                            ewallet.IsLockedOut = false;
                            ewallet.TopupFailCount = 0;
                            Context.SaveChanges();
                            response.IsLockedOut = false;
                            response.RemainOfTimeToUnlock = new TimeSpan(0);
                        }
                        else
                        {
                            response.IsLockedOut = true;
                            response.RemainOfTimeToUnlock = lockOutTimeSpan -lockedOutTimeSpan;
                        }
                    }
                    else
                    {
                        ewallet.IsLockedOut = false;
                        ewallet.TopupFailCount = 0;
                        Context.SaveChanges();
                        response.IsLockedOut = false;
                        response.RemainOfTimeToUnlock = new TimeSpan(0);
                    }
                }
            }

            return response;
        }
        #endregion

        #region change amount sub
        /// <summary>
        /// change amount sub
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public EWalletResponse ChangesAmountSub(Dictionary<string, object> dic)
        {
            int ewalletID = dic.GetInt("EWalletID");
            decimal amount = dic.GetDecimal("Amount");

            EWalletResponse response = new EWalletResponse();
            // tao transaction
            var ewallet = this.Find(ewalletID);
            if (ewallet != null)
            {
                // tao transaction, mac dinh la that bai
                // sau khi thuc hien xong giao dich nap the thi update lai thong tin transaction
                EWalletTransaction etrans = new EWalletTransaction();
                etrans.EWalletID = ewallet.EWalletID;
                etrans.TransTypeID = GlobalConstants.TRANSACTION_TYPE_AMOUNT_SUB;
                etrans.BalanceBefore = ewallet.Balance;
                etrans.BalanceAfter = ewallet.Balance - amount;
                etrans.CreatedTime = DateTime.Now;
                etrans.TransAmount = amount;
                etrans.Status = 1;
                etrans.UpdatedTime = DateTime.Now;
                // cap nhat lai vi dien tu
                ewallet.Balance = ewallet.Balance - amount;
                ewallet.UpdatedTime = DateTime.Now;
                Context.EWalletTransactions.Add(etrans);
                Context.SaveChanges();

                response.Balance = ewallet.Balance;
                response.EWalletID = ewallet.EWalletID;                
            }
            
			LogBusiness.InsertLog(DateTime.Now, DateTime.Now, ewallet.AccountID.ToString(), "EWalletBusiness_ChangesAmountSub", GlobalConstants.COMMON_LOG_ACTION_ADD, JsonConvert.SerializeObject(dic, new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), JsonConvert.SerializeObject(response, new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), "Goi Ham Tru Tien Vi Dien Tu Khi Gui Tin Nhan");
            return response;
        }
        #endregion
    }
}
