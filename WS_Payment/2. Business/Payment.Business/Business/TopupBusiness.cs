﻿using Newtonsoft.Json;
using Payment.Business.IBusiness;
using Payment.Model.Model;
using Payment.ServiceLoader.ILoader;
using Payment.Utility.Common;
using Payment.Utility.Constants;
using Payment.Utility.Object.ResponseObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.Business
{
    public class TopupBusiness : GenericBusiness<Topup>, ITopupBusiness
    {
        #region contructor/declare
        private readonly IEWalletBusiness EWalletBusiness;
        private readonly IScratchCardServiceLoader ScratchCardServiceLoader;
        private readonly ILogBusiness LogBusiness;

        public TopupBusiness(IEWalletBusiness EWalletBusiness, IScratchCardServiceLoader ScratchCardServiceLoader, ILogBusiness LogBusiness)
        {
            this.EWalletBusiness = EWalletBusiness;
            this.ScratchCardServiceLoader = ScratchCardServiceLoader;
            this.LogBusiness = LogBusiness;
        }
        #endregion

        #region topup
        /// <summary>
        /// topup card
        /// </summary>
        /// <author date="140528">HaiVT</author>
        /// <param name="dic"></param>
        public TopupResponse TopUp(Dictionary<string, object> dic)
        {
            int ewalletID = dic.GetInt("EWalletID");
            string cardSerial = dic.GetString("CardSerial");
            string pinCard = dic.GetString("pinCard");

            TopupResponse response = new TopupResponse();
            // tao transaction
            var ewallet = this.Context.EWallets.Where(p => p.EWalletID == ewalletID).FirstOrDefault();
            if (ewallet != null)
            {
                // kiem tra trang thai vi dien tu
                if (ewallet.LastTopupFailDate.HasValue)
                {
                    DateTime dateTimeNow = DateTime.Now;
                    int lockMin = Config.GetInt("LockMin");
                    int min = (dateTimeNow - ewallet.LastTopupFailDate.Value).Minutes;
                    if (min >= lockMin)
                    {
                        ewallet.TopupFailCount = 0;
                        ewallet.IsLockedOut = false;
                    }
                    else
                    {
                        if (ewallet.TopupFailCount >= Config.GetInt("LockCount"))
                        {
                            ewallet.IsLockedOut = true;
                            response.Success = false;
                            response.ErrorCode = "200";
                            response.ErrorMessage = "Ví điện tử đã bi khóa do nạp thẻ sai";
                            response.RemainTimeToUnLock = lockMin - min;
                            Context.SaveChanges();
                        }
                        else
                        {
                            ewallet.IsLockedOut = false;
                        }
                    }
                }

                if (string.IsNullOrEmpty(response.ErrorCode))
                {
                    // tao transaction, mac dinh la that bai
                    // sau khi thuc hien xong giao dich nap the thi update lai thong tin transaction
                    EWalletTransaction etrans = new EWalletTransaction();
                    etrans.EWalletID = ewallet.EWalletID;
                    etrans.TransAmount = 0;
                    etrans.TransTypeID = GlobalConstants.TRANSACTION_TYPE_AMOUNT_PLUS;
                    etrans.BalanceBefore = ewallet.Balance;
                    etrans.BalanceAfter = ewallet.Balance;
                    etrans.Status = 0;
                    etrans.CreatedTime = DateTime.Now;
                    Context.EWalletTransactions.Add(etrans);
                    Context.SaveChanges();

                    // thuc hien nap card
                    string transID = CreateTransID();
                    var result = ScratchCardServiceLoader.Topup(Config.GetString("PartnerID"), Config.GetString("passPhare"), cardSerial, pinCard, transID);
                    LogBusiness.InsertLog(DateTime.Now, DateTime.Now, ewallet.AccountID.ToString(), "TopupBusiness_TopUp", GlobalConstants.COMMON_LOG_ACTION_ADD, JsonConvert.SerializeObject(dic, new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), JsonConvert.SerializeObject(result, new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), "Goi API Nap The");
                    if (result != null)
                    {
                        Topup topup = new Topup();
                        topup.EWalletTransactionID = etrans.EWalletTransactionID;
                        topup.CardSerial = cardSerial;
                        topup.PinCard = pinCard;
                        topup.ErrorCode = result.ErrorCode;
                        topup.ErrorMessage = result.ErrorMessage;
                        topup.CreatedTime = result.TopupTime;
                        topup.TransIDRequest = transID;
                        topup.TransIDResponse = result.TransID;
                        topup.OriginTransID = !string.IsNullOrEmpty(result.OriginTransID) ? result.OriginTransID : string.Empty;
                        topup.TopupTimeSpecified = result.TopupTimeSpecified;
                        Context.Topups.Add(topup);

                        if (topup.ErrorCode.Equals("00"))
                        {
                            // cap nhat lai transaction
                            etrans.TransAmount = result.Amount;
                            etrans.Status = 1;
                            etrans.UpdatedTime = DateTime.Now;
                            etrans.BalanceAfter = etrans.BalanceBefore + result.Amount;
                            // cap nhat lai vi dien tu
                            ewallet.Balance = ewallet.Balance + result.Amount;
                            ewallet.UpdatedTime = DateTime.Now;
                            // update lai trang thai vi dien tu khi co giao dich nap the thanh cong
                            ewallet.TopupFailCount = 0;
                            ewallet.IsLockedOut = false;
                            // update lai response tra ve
                            response.Success = true;
                            response.TransactionAmount = result.Amount;
                            response.TransactionTime = result.TopupTime;
                            response.BalanceAfterTransaction = ewallet.Balance;
                            response.ErrorCode = result.ErrorCode;
                            response.ErrorMessage = result.ErrorMessage;
                            response.TransactionID = etrans.EWalletTransactionID;
                        }
                        else
                        {
                            response.Success = false;
                            response.ErrorCode = result.ErrorCode;
                            response.ErrorMessage = result.ErrorMessage;
                            response.TransactionTime = result.TopupTime;
                            response.TransactionID = etrans.EWalletTransactionID;
                            // count thoi gian khoa tai khoan
                            ewallet.LastTopupFailDate = DateTime.Now;
                            ewallet.TopupFailCount += 1;
                        }

                        Context.SaveChanges();
                    }
                    else
                    {
                        response.Success = false;
                        response.ErrorCode = "202";
                        response.ErrorMessage = "Exception khi gọi API nạp thẻ tra về object null";
                    }
                }
            }
            else
            {
                response.Success = false;
                response.ErrorCode = "201";
                response.ErrorMessage = "Ví điện tử không tồn tại";
            }

            LogBusiness.InsertLog(DateTime.Now, DateTime.Now, ewallet.AccountID.ToString(), "TopupBusiness_TopUp", GlobalConstants.COMMON_LOG_ACTION_ADD, JsonConvert.SerializeObject(dic, new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), JsonConvert.SerializeObject(response, new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), "Ket qua tra ve sau khi goi ham Topup");
            return response;
        }
        #endregion
      
        #region private create TransID
        /// <summary>
        /// create transID yyMMddHHmmssPPPPPPPPPPXXX
        /// </summary>
        /// <returns></returns>
        private string CreateTransID()
        {
            string transId = string.Format("{0}{1}{2}", DateTime.Now.ToString("yyMMddHHmmss"), Config.GetString("PartnerID"), DateTime.Now.ToString("fff"));
            return transId;
        }
        #endregion
    }
}
