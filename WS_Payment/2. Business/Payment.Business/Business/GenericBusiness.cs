﻿using Payment.Business.IBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Payment.Model.Model;
using log4net;
using System.Data.Entity;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Data;

namespace Payment.Business.Business
{
    public class GenericBusiness<T> : IGenericBusiness<T> where T : class
    {
        #region declare/contructor/setter
        private PaymentEntities _context;

        public PaymentEntities Context
        {
            get
            {
                if (_context == null)
                {
                    _context = new PaymentEntities();
                }
                return _context;
            }
        }

        public static ILog logger;

        private const string STR_SYSTEM = "System";

        private const string STR_SYSTEM_COLLECTION = "System.Collections";

        internal DbSet<T> DbSet;

        public GenericBusiness()
        {
            logger = LogManager.GetLogger(this.GetType().UnderlyingSystemType.Name);
            DbSet = this.Context.Set<T>();
        }
        #endregion

        /// <summary>
        /// Get all object from database
        /// </summary>
        public IQueryable<T> All
        {
            get { return DbSet.AsQueryable(); }
        }

        /// <summary>
        /// Finds an entity with the given primary key values.  If an entity with the
        /// given primary key values exists in the context, then it is returned immediately
        /// without making a request to the store. Otherwise, a request is made to the
        /// store for an entity with the given primary key values and this entity, if
        /// found, is attached to the context and returned. If no entity is found in
        /// the context or the store, then null is returned.
        /// </summary>
        /// <param name="key">Specified key to find</param>
        public T Find(object key)
        {
            return DbSet.Find(key);
        }

        /// <summary>
        /// Asynchronously finds an entity with the given primary key values.  If an
        /// entity with the given primary key values exists in the context, then it is
        /// returned immediately without making a request to the store. Otherwise, a
        /// request is made to the store for an entity with the given primary key values
        /// and this entity, if found, is attached to the context and returned. If no
        /// entity is found in the context or the store, then null is returned.
        /// </summary>
        /// <param name="key">Specified key to find</param>
        public Task<T> FindAsync(object key)
        {
            return DbSet.FindAsync(key);
        }

        /// <summary>
        /// Add object to database
        /// </summary>
        /// <param name="entityToInsert">Specified object to insert</param>      
        public T Add(T entityToAdd)
        {
            return DbSet.Add(entityToAdd);
        }

        /// <summary>
        /// Add range object to database
        /// </summary>
        /// <param name="lstT">Specified range object to insert</param>    
        public IEnumerable<T> AddRange(IEnumerable<T> rangeEntitytoAdd)
        {
            return DbSet.AddRange(rangeEntitytoAdd);
        }

        /// <summary>
        /// update object change and save to database
        /// </summary>
        /// <param name="entityToUpdate"></param>
        public T Attach(T entityToAttach)
        {
            return DbSet.Attach(entityToAttach);
        }

        /// <summary>
        /// Remove object from database
        /// </summary>
        /// <param name="entityToRemove">Specified object to remove</param>
        public T Remove(T entityToRemove)
        {
            return DbSet.Remove(entityToRemove);
        }

        // <summary>
        /// Remove range object from database
        /// </summary>
        /// <param name="rangeEntityToRemove">Specified range object to remove</param>
        public IEnumerable<T> RemoveRange(IEnumerable<T> rangeEntityToRemove)
        {
            return DbSet.RemoveRange(rangeEntityToRemove);
        }

        /// <summary>
        /// Saves all changes made in this context to the underlying database.
        /// </summary>
        public int SaveChanges()
        {
            return this.Context.SaveChanges();
        }

        /// <summary>
        /// Asynchronously saves all changes made in this context to the underlying database.
        /// </summary>
        public Task<int> SaveChangesAsync()
        {
            return this.Context.SaveChangesAsync();
        }

        #region bulkcopy insert with list A
        /// <summary>
        /// bulk insert copy
        /// </summary>
        /// <author date="2013/11/15">HaiVT</author>
        public bool BulkCopyInsert<A>(List<A> lstA, string strDesTableName, string strExceptColumn = "")
        {
            bool bIsSuccess = false;
            if (lstA != null && lstA.Count > 0)
            {
                SqlTransaction transaction = null;
                try
                {
                    string strConnection = Context.Database.Connection.ConnectionString;
                    using (SqlConnection conn = new SqlConnection(strConnection))
                    {
                        PropertyDescriptorCollection proCollection = TypeDescriptor.GetProperties(typeof(A));
                        PropertyDescriptor objPropertyDescriptor = null;
                        DataTable dtInsert = new DataTable();

                        conn.Open();
                        using (transaction = conn.BeginTransaction())
                        {
                            using (SqlBulkCopy sqlBulkCopyInsert = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, transaction))
                            {
                                sqlBulkCopyInsert.DestinationTableName = strDesTableName;
                                for (int i = 0, size = proCollection.Count; i < size; i++)
                                {
                                    objPropertyDescriptor = proCollection[i];
                                    if (objPropertyDescriptor.PropertyType.Namespace.Contains(STR_SYSTEM) && !objPropertyDescriptor.PropertyType.Namespace.Contains(STR_SYSTEM_COLLECTION) && !strExceptColumn.Contains(objPropertyDescriptor.Name))
                                    {
                                        dtInsert.Columns.Add(objPropertyDescriptor.Name, Nullable.GetUnderlyingType(objPropertyDescriptor.PropertyType) ?? objPropertyDescriptor.PropertyType);
                                        sqlBulkCopyInsert.ColumnMappings.Add(objPropertyDescriptor.Name, objPropertyDescriptor.Name);
                                    }
                                }

                                A item;
                                DataRow row;
                                for (int i = 0, size = lstA.Count; i < size; i++)
                                {
                                    item = lstA[i];
                                    row = dtInsert.NewRow();
                                    for (int k = 0, kSize = proCollection.Count; k < kSize; k++)
                                    {
                                        objPropertyDescriptor = proCollection[k];
                                        if (objPropertyDescriptor.PropertyType.Namespace.Contains(STR_SYSTEM) && !objPropertyDescriptor.PropertyType.Namespace.Contains(STR_SYSTEM_COLLECTION) && !strExceptColumn.Contains(objPropertyDescriptor.Name))
                                        {
                                            row[objPropertyDescriptor.Name] = objPropertyDescriptor.GetValue(item) ?? DBNull.Value;
                                        }
                                    }
                                    dtInsert.Rows.Add(row);
                                }
                                sqlBulkCopyInsert.WriteToServer(dtInsert);
                            }
                            transaction.Commit();
                            bIsSuccess = true;
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                }
            }
            return bIsSuccess;
        }
        #endregion

        #region bulkcopy insert with dataTable
        /// <summary>
        /// bulk insert copy
        /// </summary>
        /// <author date="2013/11/15">HaiVT</author>
        public bool BulkCopyInsertDT(Dictionary<string, DataTable> dic)
        {
            bool bIsSuccess = false;
            if (dic != null && dic.Count > 0)
            {
                SqlTransaction transaction = null;
                try
                {
                    string strConnection = Context.Database.Connection.ConnectionString;
                    using (SqlConnection conn = new SqlConnection(strConnection))
                    {
                        List<KeyValuePair<string, DataTable>> lstDtInsert = dic.ToList();
                        KeyValuePair<string, DataTable> objKeyValuePair = new KeyValuePair<string, DataTable>();
                        DataTable dtInsert = new DataTable();
                        conn.Open();
                        using (transaction = conn.BeginTransaction())
                        {

                            for (int i = 0, size = lstDtInsert.Count; i < size; i++)
                            {
                                objKeyValuePair = lstDtInsert[i];
                                dtInsert = objKeyValuePair.Value;
                                using (SqlBulkCopy sqlBulkCopyInsert = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, transaction))
                                {
                                    sqlBulkCopyInsert.DestinationTableName = objKeyValuePair.Key;
                                    for (int k = 0, ksize = dtInsert.Columns.Count; k < ksize; k++)
                                    {
                                        sqlBulkCopyInsert.ColumnMappings.Add(dtInsert.Columns[k].ColumnName, dtInsert.Columns[k].ColumnName);
                                    }
                                    sqlBulkCopyInsert.WriteToServer(dtInsert);
                                }
                            }
                            transaction.Commit();
                            bIsSuccess = true;
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                }
            }
            return bIsSuccess;
        }
        #endregion
    }
}
