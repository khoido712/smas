﻿using Payment.Business.IBusiness;
using Payment.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.Business
{
    public class TransactionTypeBusiness : GenericBusiness<TransactionType>, ITransactionTypeBusiness
    {
    }
}
