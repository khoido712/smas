﻿using Payment.Business.IBusiness;
using Payment.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Payment.Business.Business
{
    public class LogBusiness : GenericBusiness<Log>, ILogBusiness
    {
        #region contructor/declare
        private const string IP_UN_KNOW = "UnKnow";
        #endregion

        #region ghi log vao db
        /// <summary>
        /// ghi log vao db
        /// </summary>
        /// <author date="2014/02/28">HaiVT</author>
        /// <param name="startTime">thoi gian bat dau</param>
        /// <param name="endTime">thoi gian ket thuc</param>
        /// <param name="userID">userid, doi voi sms_school la userName dang nhap; Service la user ws</param>
        /// <param name="module">ten module dang thao tac. MVC: AreaName.ControllerName.ActionName; Service la ten Service</param>
        /// <param name="action">Loai tac dong: Add, Update, Delete, View</param>
        /// <param name="input">du lieu dau vao</param>
        /// <param name="output">du lieu dau ra</param>
        /// <param name="description">mo ta</param>
        /// <param name="responseTime">responseTime</param>
        /// <returns></returns>
        public bool InsertLog(DateTime startTime, DateTime endTime, string userID, string module, string action, string input, string output, string description, string responseTime = "")
        {
            try
            {
                if (startTime == null || endTime == null || string.IsNullOrEmpty(userID) || string.IsNullOrEmpty(module) || string.IsNullOrEmpty(action) || string.IsNullOrEmpty(input) || string.IsNullOrEmpty(output) || string.IsNullOrEmpty(description))
                {
                    return false;
                }

                string ipAddress = IP_UN_KNOW;
                if (HttpContext.Current != null && HttpContext.Current.Request != null && !string.IsNullOrEmpty(HttpContext.Current.Request.UserHostAddress))
                {
                    ipAddress = HttpContext.Current.Request.UserHostAddress;
                }

                string ipLocal = IP_UN_KNOW;
                var internalIP = Dns.GetHostAddresses(Dns.GetHostName()).Where(address => address.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault();
                if (internalIP != null)
                {
                    ipLocal = internalIP.ToString();
                }

                Log log = new Log();
                log.AppCode = Assembly.GetExecutingAssembly().GetName().Name;
                log.ThreadID = Thread.CurrentThread.ManagedThreadId;
                log.StartTime = startTime;
                log.EndTime = endTime;
                log.UserID = userID;
                log.Module = module;
                log.Action = action;
                log.Input = input;
                log.Output = output;
                log.Desciption = description;
                log.IPAddress = ipAddress;
                log.IPLocal = ipLocal;
                this.Context.Logs.Add(log);
                this.Context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("Loi Khi Goi Ham Ghi Log" + ex.Message, ex);                
            }

            return false;
        }
        #endregion
    }
}
