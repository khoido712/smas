﻿using Payment.Business.IBusiness;
using Payment.Utility.Common;
using Payment.Model.Model;
using Payment.Utility.Object.ResponseObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using Payment.Utility.Constants;

namespace Payment.Business.Business
{
    public class EWalletTransactionBusiness : GenericBusiness<EWalletTransaction>, IEWalletTransactionBusiness
    {
        /// <summary>
        /// lay thong tin giao dich nap the
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public FindTransactionResponse FindTransaction(Dictionary<string, object> dic)
        {
            int ewalletID = dic.GetInt("EWalletID");
            DateTime? fromDate = dic.GetDateTime("FromDate");
            DateTime? toDate = dic.GetDateTime("ToDate");
            int index = dic.GetInt("beginIndex");
            int numberOfRetrievedRecord = dic.GetInt("numberOfRetrievedRecord");

            FindTransactionResponse response = new FindTransactionResponse();
            if(ewalletID > 0 && fromDate.HasValue && toDate.HasValue)
            {
                var result = from et in Context.EWalletTransactions
                             where et.EWalletID == ewalletID
                             && DbFunctions.TruncateTime(et.CreatedTime) >= DbFunctions.TruncateTime(fromDate.Value)
                             && DbFunctions.TruncateTime(et.CreatedTime) <= DbFunctions.TruncateTime(toDate.Value)
                             && et.Status == GlobalConstants.TRANSACTION_STATUS_SUCCESS
                             && et.TransTypeID == GlobalConstants.TRANSACTION_TYPE_AMOUNT_PLUS
                             select et;

                response.TotalRecord = result.Count();
                response.Transactions = (from p in result
                                         orderby p.CreatedTime descending
                                         select new Transaction
                                         {
                                             TransactionID = p.EWalletTransactionID,
                                             TransactionDateTime = p.CreatedTime,
                                             TransactionAmount = p.TransAmount,
                                             status = p.Status,
                                             BalanceAfterTransaction = p.BalanceAfter
                                         }).Skip(index).Take(numberOfRetrievedRecord).ToList();

            }

            return response;            
        }
    }
}
