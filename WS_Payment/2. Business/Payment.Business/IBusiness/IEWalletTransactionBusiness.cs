﻿using Payment.Model.Model;
using Payment.Utility.Object.ResponseObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.IBusiness
{
    public interface IEWalletTransactionBusiness : IGenericBusiness<EWalletTransaction>
    {
        #region lay danh sach giao dich
        /// <summary>
        /// lay danh sach giao dich trong dieu kien xac dinh
        /// </summary>
        /// <author date="140530">AnhVD</author>
        /// <param name="dic"></param>
        /// <returns></returns>
        FindTransactionResponse FindTransaction(Dictionary<string, object> dic);
        #endregion
        

    }
}
