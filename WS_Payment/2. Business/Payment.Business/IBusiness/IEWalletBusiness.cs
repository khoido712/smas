﻿using Payment.Model.Model;
using Payment.Utility.Object.ResponseObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.IBusiness
{
    public interface IEWalletBusiness : IGenericBusiness<EWallet>
    {
        #region get ewallet
        /// <summary>
        /// lay thong tin vi dien tu cua user
        /// </summary>
        /// <author>HaiVT</author>
        /// <param name="dic"></param>
        /// <returns></returns>
        EWalletResponse GetEWallet(Dictionary<string, object> dic);
        #endregion

        #region change amount sub
        /// <summary>
        /// change amount sub
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        EWalletResponse ChangesAmountSub(Dictionary<string, object> dic);
        #endregion
    }
}
