﻿using Payment.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.IBusiness
{
    public interface ILogBusiness : IGenericBusiness<Log>
    {
        #region ghi log vao db
        /// <summary>
        /// ghi log vao db
        /// </summary>
        /// <author date="2014/02/28">HaiVT</author>
        /// <param name="startTime">thoi gian bat dau</param>
        /// <param name="endTime">thoi gian ket thuc</param>
        /// <param name="userID">userid, doi voi sms_school la userName dang nhap; Service la user ws</param>
        /// <param name="module">ten module dang thao tac. MVC: AreaName.ControllerName.ActionName; Service la ten Service</param>
        /// <param name="action">Loai tac dong: Add, Update, Delete, View</param>
        /// <param name="input">du lieu dau vao</param>
        /// <param name="output">du lieu dau ra</param>
        /// <param name="description">mo ta</param>
        /// <param name="responseTime">responseTime</param>
        /// <returns></returns>
        bool InsertLog(DateTime startTime, DateTime endTime, string userID, string module, string action, string input, string output, string description, string responseTime = "");
        #endregion
    }
}
