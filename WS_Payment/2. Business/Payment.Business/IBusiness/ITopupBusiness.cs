﻿using Payment.Model.Model;
using Payment.Utility.Object.ResponseObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.IBusiness
{
    public interface ITopupBusiness : IGenericBusiness<Topup>
    {
        #region Nap tien
        /// <summary>
        /// Nap the
        /// </summary>
        /// <param name="dic"></param>
        ///  <author date="140530">AnhVD</author>
        /// <returns></returns>
        TopupResponse TopUp(Dictionary<string, object> dic);
        #endregion
    }
}
