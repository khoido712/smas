﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.IBusiness
{
    public interface IGenericBusiness<T> where T : class
    {
        /// <summary>
        /// Get all object from database
        /// </summary>
        IQueryable<T> All { get; }

        /// <summary>
        /// Finds an entity with the given primary key values.  If an entity with the
        /// given primary key values exists in the context, then it is returned immediately
        /// without making a request to the store. Otherwise, a request is made to the
        /// store for an entity with the given primary key values and this entity, if
        /// found, is attached to the context and returned. If no entity is found in
        /// the context or the store, then null is returned.
        /// </summary>
        /// <param name="key">Specified key to find</param>
        T Find(object key);

        /// <summary>
        /// Asynchronously finds an entity with the given primary key values.  If an
        /// entity with the given primary key values exists in the context, then it is
        /// returned immediately without making a request to the store. Otherwise, a
        /// request is made to the store for an entity with the given primary key values
        /// and this entity, if found, is attached to the context and returned. If no
        /// entity is found in the context or the store, then null is returned.
        /// </summary>
        /// <param name="key"></param>
        Task<T> FindAsync(object key);

        /// <summary>
        /// Add object to database
        /// </summary>
        /// <param name="entityToInsert">Specified object to insert</param>        
        T Add(T entityToAdd);

        /// <summary>
        /// Add range object to database
        /// </summary>
        /// <param name="lstT">Specified range object to insert</param>        
        IEnumerable<T> AddRange(IEnumerable<T> rangeEntitytoAdd);

        /// <summary>
        /// update object change and save to database
        /// </summary>
        /// <param name="entityToUpdate"></param>
        T Attach(T entityToAttach);

        /// <summary>
        /// Remove object from database
        /// </summary>
        /// <param name="entityToRemove">Specified object to remove</param>        
        T Remove(T entityToRemove);

        /// <summary>
        /// Remove range object from database
        /// </summary>
        /// <param name="rangeEntityToRemove">Specified range object to remove</param>        
        IEnumerable<T> RemoveRange(IEnumerable<T> rangeEntityToRemove);

        /// <summary>
        /// Saves all changes made in this context to the underlying database.
        /// </summary>
        int SaveChanges();

        /// <summary>
        /// Asynchronously saves all changes made in this context to the underlying database.
        /// </summary>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// bulk insert copy
        /// </summary>
        /// <author date="2013/11/15">HaiVT</author>
        bool BulkCopyInsert<A>(List<A> lstT, string strDesTableName, string strExceptColumn = "");

        /// <summary>
        /// bulk insert copy
        /// </summary>
        /// <author date="2013/11/15">HaiVT</author>
        bool BulkCopyInsertDT(Dictionary<string, DataTable> dic);
    }
}
