﻿using Payment.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Business.IBusiness
{
    public interface IAccountBusiness : IGenericBusiness<Account>
    {
    }
}
