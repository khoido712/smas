﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Utility.Object.ResponseObject
{
    [DataContract]
    public class TopupResponse
    {
        /// <summary>
        /// id EWalletTransaction
        /// </summary>
        [DataMember]
        public long TransactionID { get; set; }
        /// <summary>
        /// trang thai
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// thoi gian bi khoa con lai
        /// </summary>
        [DataMember]
        public DateTime LastLockedOutDate { get; set; }

        /// <summary>
        /// so luong
        /// </summary>
        [DataMember]
        public decimal TransactionAmount { get; set; }

        /// <summary>
        /// thoi gian giao dich
        /// </summary>
        [DataMember]
        public DateTime TransactionTime { get; set; }

        /// <summary>
        /// so du tai khoan
        /// </summary>
        [DataMember]
        public decimal BalanceAfterTransaction { get; set; }

        /// <summary>
        /// ma loi
        /// </summary>
        [DataMember]
        public string ErrorCode { get; set; }

        /// <summary>
        /// thong bao loi
        /// </summary>
        [DataMember]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// thoi gian con lai khi bi khoa
        /// </summary>
        [DataMember]
        public int RemainTimeToUnLock { get; set; }
    }
}
