﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Utility.Object.ResponseObject
{
    [DataContract]
    public class EWalletResponse
    {
        /// <summary>
        /// Số hiệu ví điện tử
        /// </summary>
        [DataMember]
        public int EWalletID { get; set; }
        
        /// <summary>
        /// Tên đăng nhập
        /// </summary>
        [DataMember]
        public string Username;
        
        /// <summary>
        /// Số dư tài khoản
        /// </summary>
        [DataMember]
        public decimal Balance { get; set; }

        /// <summary>
        /// // Số tin nhắn khuyến mãi/tháng. Hiện giờ chưa có thì cho nó bằng 0.
        /// </summary>
        [DataMember]
        public int NumberOfPromotiveSMS { get; set; } 

        /// <summary>
        /// trang thai khong hay khong xoa cua vi dien tu
        /// </summary>
        [DataMember]
        public bool IsLockedOut { get; set; }

        /// <summary>
        /// thoi gian bi khoa
        /// </summary>
        [DataMember]
        public TimeSpan RemainOfTimeToUnlock { get; set; }
    }

    [DataContract]
    public class FindTransactionResponse
    {
        [DataMember]
        public int TotalRecord { get; set; }
        
        [DataMember]
        public List<Transaction> Transactions { get; set; }
    }

    [DataContract]
    public class Transaction
    {
        [DataMember]
        public long TransactionID { get; set; }

        [DataMember]
        public DateTime TransactionDateTime { get; set; }

        [DataMember]
        public decimal TransactionAmount { get; set; }

        [DataMember]
        public decimal BalanceAfterTransaction { get; set; }

        [DataMember]
        public int status { get; set; }
    }
}
