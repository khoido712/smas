﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Utility.Object.RequestObject
{
    public class EWalletTransactionRequest
    {
        public int EWalletID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public int BeginIndex { get; set; }

        public int NumberOfRetrievedRecord { get; set; }

        public int TotalRecord { get; set; }

    }
}
