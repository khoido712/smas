﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Utility.Object.RequestObject
{
    [DataContract]
    public class EWalletRequest
    {
        /// <summary>
        /// SchoolID or SuperUserID
        /// </summary>
        [DataMember]
        public int UserID { get; set; }       

        /// <summary>
        /// user truong
        /// </summary>
        [DataMember]
        public bool SchoolUser { get; set; }

        /// <summary>
        /// user phong so
        /// </summary>
        [DataMember]
        public bool SupervisingDeptUser { get; set; }
    }

    [DataContract]
    public class EWalletTransAmountRequest
    {
        /// <summary>
        /// id vi dien tu
        /// </summary>
        [DataMember]
        public int EWalletID { get; set; }

        /// <summary>
        /// so tien thay doi
        /// </summary>
        [DataMember]
        public decimal Amount { get; set; }
    }

    [DataContract]
    public class FindTransactionRequest
    {
        [DataMember]
        public int eWalletID;

        [DataMember]
        public DateTime fromdate;
        
        [DataMember]
        public DateTime todate;

        [DataMember]
        public int beginIndex;

        [DataMember]
        public int numberOfRetrievedRecord;
    }

}
