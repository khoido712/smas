﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Utility.Object.RequestObject
{
    [DataContract]
    public class TopupRequest
    {
        /// <summary>
        /// id vi dien tu
        /// </summary>
        [DataMember]
        public int EWalletID { get; set; }

        /// <summary>
        /// card serial in tren the
        /// </summary>
        [DataMember]
        public string CardSerial { get; set; }

        /// <summary>
        /// ma so cao duoc tren the
        /// </summary>
        [DataMember]
        public string PinCard { get; set; }
    }
}
