﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Utility.Object.LoaderObject
{
    /// <summary>
    /// object return when call topupCard API
    /// </summary>
    public class TopupLoaderResult
    {
        /// <summary>
        /// ma loi
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// mo ta loi
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// dinh danh giao dich topup
        /// </summary>
        public string TransID { get; set; }

        /// <summary>
        /// ??? confirm lai cung cap api xem tham so nay y nghia la gi
        /// </summary>
        public string OriginTransID { get; set; }

        /// <summary>
        /// thoi gian thuc hien giao dich
        /// </summary>
        public DateTime TopupTime { get; set; }

        /// <summary>
        /// ??? confirm lai cungcap api xem tham so nay y nghia la gi
        /// </summary>
        public bool TopupTimeSpecified { get; set; }

        /// <summary>
        /// chuoi so in tren the cao
        /// </summary>
        public string CardSerial { get; set; }

        /// <summary>
        /// so tien topup
        /// </summary>
        public decimal Amount { get; set; }
    }
}
