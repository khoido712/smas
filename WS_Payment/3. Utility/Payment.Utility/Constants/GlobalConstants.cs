﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Payment.Utility.Constants
{
    public partial class GlobalConstants
    {
        /// <summary>
        /// giao dich cong tien
        /// </summary>
        public const byte TRANSACTION_TYPE_AMOUNT_PLUS = 1;

        /// <summary>
        /// giao dich tru tien
        /// </summary>
        public const byte TRANSACTION_TYPE_AMOUNT_SUB = 2;

        /// <summary>
        /// giao dich that bai 
        /// </summary>
        public const byte TRANSACTION_STATUS_ERROR = 0;

        /// <summary>
        /// giao dich thanh cong
        /// </summary>
        public const byte TRANSACTION_STATUS_SUCCESS = 1;

        /// <summary>
        /// user truong
        /// </summary>
        public const byte USER_TYPE_SCHOOL = 1;

        /// <summary>
        /// user phong so
        /// </summary>
        public const byte USER_TYPE_SUPER_VISING_DEPT = 2;

        /// <summary>
        /// LOG action Add
        /// </summary>
        public const string COMMON_LOG_ACTION_ADD = "Add";

        /// <summary>
        /// LOG action Update
        /// </summary>
        public const string COMMON_LOG_ACTION_UPDATE = "Update";

        /// <summary>
        /// LOG action Delete
        /// </summary>
        public const string COMMON_LOG_ACTION_DELETE = "Delete";

        /// <summary>
        /// LOG action View
        /// </summary>
        public const string COMMON_LOG_ACTION_VIEW = "View";
    }
}
