﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Configuration;
using System.Web.Configuration;
using Payment.Utility.Constants;
using System.Text;
using System.Globalization;

namespace Payment.Utility.Common
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Get Guild value
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>08/05/2013</date>
        /// <param name="val"></param>
        /// <returns></returns>
        public static long GetLong(object val, long def = 0)
        {
            if (val == null)
            {
                return def;
            }

            if (val is long)
            {
                return (long)val;
            }
            else
            {
                try
                {
                    return long.Parse(val.ToString());
                }
                catch
                {
                    throw new Exception(" GetInt error, object is not a Decimal value");
                }
            }
        }

        /// <summary>
        /// Get Guild value
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>08/05/2013</date>
        /// <param name="val"></param>
        /// <returns></returns>
        public static long GetLong(this Dictionary<string, object> dic, string key, long def = 0)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetLong(dic[key], def);
        }
        /// <summary>
        /// fast convert string to int
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>09/04/2013</date>
        public static int IntParseFast(this string value)
        {
            int result = 0;
            for (int i = 0; i < value.Length; i++)
            {
                result = 10 * result + (value[i] - 48);
            }
            return result;
        }

        /// <summary>
        /// bind value object to object with property mapping
        /// </summary>
        /// <author>HaiVT</author>
        /// <date>19/07/2013</date>               
        /// <returns></returns>
        public static T BindTo<T>(this object source) where T : new()
        {
            T objectDes = new T();
            Type typeSource = source.GetType();
            IList<PropertyInfo> sourcePropertyInfo = new List<PropertyInfo>(typeSource.GetProperties());

            typeSource = objectDes.GetType();
            IList<PropertyInfo> desProperties = new List<PropertyInfo>(typeSource.GetProperties());
            List<string> desName = new List<string>();

            for (int i = desProperties.Count - 1; i >= 0; i--)
            {
                desName.Add(desProperties[i].Name);
            }

            PropertyInfo objPropertyInFo = null;
            for (int i = sourcePropertyInfo.Count - 1; i >= 0; i--)
            {
                objPropertyInFo = sourcePropertyInfo[i];
                if (objPropertyInFo.GetValue(source, null) == null)
                {
                    continue;
                }

                if (desName.Contains(objPropertyInFo.Name))
                {
                    typeSource.GetProperty(objPropertyInFo.Name).SetValue(objectDes, objPropertyInFo.GetValue(source, null), null);
                }

            }
            return objectDes;
        }

        /// <summary>
        /// bind list object to object with property mapping
        /// </summary>
        /// <author>HaiVT</author>
        /// <date>2014/03/04</date>               
        /// <returns></returns>
        public static List<T> BindToList<T>(this IEnumerable<object> sources) where T : new()
        {
            List<T> results = new List<T>();
            T item = new T();
            foreach (var obj in sources)
            {
                item = obj.BindTo<T>();
                results.Add(item);
            }

            return results;           
        }

        /// <summary>
        /// get 8 character random
        /// </summary>
        /// <author>HaiVT 09/08/2013</author>
        /// <returns></returns>
        public static string GetRandomCharacter(this string value)
        {
            Random random = new Random();
            char[] arrChar = new char[8];
            for (int i = 0; i < arrChar.Length; i++)
            {
                arrChar[i] = value[random.Next(value.Length)];
            }
            return new string(arrChar);
        }

     
        public static T GetObject<T>(this Dictionary<string, object> dic, string key) where T : new()
        {
            if (!dic.ContainsKey(key))
            {
                return new T();
            }
            return (T)dic[key];
        }
        public static Guid? GetGuid(this Dictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return null;
            }
            return GetGuid(dic[key]);
        }
        public static Guid? GetGuid(object val)
        {
            if (val == null)
            {
                return null;
            }
            else if (val is Guid)
            {
                return (Guid)val;
            }
            else
            {
                try
                {
                    return Guid.Parse(val.ToString());
                }
                catch
                {
                    throw new Exception("GetGuid error, object is not a Guid value");
                }
            }
        }
        public static string GetString(this Dictionary<string, object> dic, string key, string def = "")
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetString(dic[key], def);
        }

        public static string GetString(object val, string def = "")
        {
            if (val == null)
            {
                return def;
            }

            if (val is string)
            {
                return (string)val;
            }
            else
            {
                throw new Exception("GetString error, object is not a string value");
            }
        }

        /// <summary>
        /// get string list 
        /// </summary>
        /// <author date="2013/11/29">HaiVT</author>
        public static List<string> GetStringList(this Dictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return GetStringList(null);
            }

            return GetStringList(dic[key]);
        }

        /// <summary>
        /// get string list
        /// </summary>
        /// <author date="2013/11/29">HaiVT</author>
        public static List<string> GetStringList(object val)
        {
            List<string> def = new List<string>();
            if (val == null)
            {
                return def;
            }

            if (val is List<string>)
            {
                return (List<string>)val;
            }
            else
            {
                throw new Exception("SMAS.Business.Common.Utils: GetStringList error, object is not a List<string> value");
            }
        }

        public static int GetInt(this Dictionary<string, object> dic, string key, int def = 0)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetInt(dic[key], def);
        }

        public static decimal GetDecimal(this Dictionary<string, object> dic, string key, decimal def = 0)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetDecimal(dic[key], def);
        }

        public static List<int> GetIntList(this Dictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return GetIntList(null);
            }

            return GetIntList(dic[key]);
        }

        public static List<int> GetIntList(object val)
        {
            List<int> def = new List<int>();
            if (val == null)
            {
                return def;
            }

            if (val is List<int>)
            {
                return (List<int>)val;
            }
            else
            {
                throw new Exception("SMAS.Business.Common.Utils: GetIntList error, object is not a List<Int> value");
            }
        }

        public static bool ? GetNullableBool(this Dictionary<string, object> dic, string key, bool ? def = false)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetNullableBool(dic[key], def);
        }

        public static bool? GetNullableBool(object val, bool? def = true)
        {
            if (val == null)
            {
                return def;
            }

            if (val is bool)
            {
                return (bool)val;
            }
            else
            {
                throw new Exception("GetBool error, object is not a bool value");
            }
        }

        public static bool GetBool(this Dictionary<string, object> dic, string key, bool def = false)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetBool(dic[key], def);
        }

        public static bool GetBool(object val, bool def = false)
        {
            if (val == null)
            {
                return def;
            }

            if (val is bool)
            {
                return (bool)val;
            }
            else
            {
                throw new Exception("GetBool error, object is not a bool value");
            }
        }

        public static int GetInt(object val, int def = 0)
        {
            if ((val == null) || String.IsNullOrEmpty(val.ToString()))
            {
                return def;
            }

            if (val is int)
            {
                return (int)val;
            }
            else
            {
                try
                {
                    return Int32.Parse(val.ToString());
                }
                catch
                {
                    throw new Exception("GetInt error, object is not a Int value");
                }
            }
        }

        public static decimal GetDecimal(object val, decimal def = 0)
        {
            if ((val == null) || String.IsNullOrEmpty(val.ToString()))
            {
                return def;
            }

            if (val is decimal)
            {
                return (decimal)val;
            }
            else
            {
                try
                {
                    return decimal.Parse(val.ToString());
                }
                catch
                {
                    throw new Exception("GetInt error, object is not a Int value");
                }
            }
        }

        public static byte GetByte(object val, byte def = 0)
        {
            if (val == null)
            {
                return def;
            }

            if (val is byte)
            {
                return (byte)val;
            }
            else
            {
                try
                {
                    return byte.Parse(val.ToString());
                }
                catch
                {
                    throw new Exception("Getbyte error, object is not a byte value");
                }
            }
        }

        public static byte GetByte(this Dictionary<string, object> dic, string key, byte def = 0)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetByte(dic[key], def);
        }

        public static DateTime? GetDateTime(this Dictionary<string, object> dic, string key, DateTime? def = null)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetDateTime(dic[key], def);
        }

        public static DateTime? GetDateTime(object val, DateTime? def = null)
        {
            if (val == null)
            {
                return null;
            }

            if (val is DateTime)
            {
                return (DateTime)val;
            }
            else
            {
                throw new Exception("GetDateTime error, object is not a DateTime value");
            }
        }


        public static short? GetNullableShort(this Dictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return null;
            }
            if (dic[key] == null)
            {
                return null;
            }

            return GetShort(dic[key]);
        }

        public static short GetShort(object val, short def = 0)
        {
            if (val == null)
            {
                return def;
            }

            if (val is short)
            {
                return (short)val;
            }
            else
            {
                try
                {
                    return short.Parse(val.ToString());
                }
                catch
                {
                    throw new Exception("GetSchort error, object is not a int value");
                }
            }
        }

        public static short GetShort(this Dictionary<string, object> dic, string key, short def = 0)
        {
            if (!dic.ContainsKey(key))
            {
                return def;
            }

            return GetShort(dic[key], def);
        }

        public static int? GetNullableInt(this Dictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return null;
            }
            if (dic[key] == null)
            {
                return null;
            }

            return GetInt(dic[key]);
        }

        /// <summary>
        /// chuyển thành tiếng việt không dấu
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string StripVNSign(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Empty;
            }

            //'Dấu Ngang
            text = text.Replace("A", "A")
            .Replace("a", "a")
            .Replace("Ă", "A")
            .Replace("ă", "a")
            .Replace("Â", "A")
            .Replace("â", "a")
            .Replace("E", "E")
            .Replace("e", "e")
            .Replace("Ê", "E")
            .Replace("ê", "e")
            .Replace("I", "I")
            .Replace("i", "i")
            .Replace("O", "O")
            .Replace("o", "o")
            .Replace("Ô", "O")
            .Replace("ô", "o")
            .Replace("Ơ", "O")
            .Replace("ơ", "o")
            .Replace("U", "U")
            .Replace("u", "u")
            .Replace("Ư", "U")
            .Replace("ư", "u")
            .Replace("Y", "Y")
            .Replace("y", "y")

            //    'Dấu Huyền
            .Replace("À", "A")
            .Replace("à", "a")
            .Replace("Ằ", "A")
            .Replace("ằ", "a")
            .Replace("Ầ", "A")
            .Replace("ầ", "a")
            .Replace("È", "E")
            .Replace("è", "e")
            .Replace("Ề", "E")
            .Replace("ề", "e")
            .Replace("Ì", "I")
            .Replace("ì", "i")
            .Replace("Ò", "O")
            .Replace("ò", "o")
            .Replace("Ồ", "O")
            .Replace("ồ", "o")
            .Replace("Ờ", "O")
            .Replace("ờ", "o")
            .Replace("Ù", "U")
            .Replace("ù", "u")
            .Replace("Ừ", "U")
            .Replace("ừ", "u")
            .Replace("Ỳ", "Y")
            .Replace("ỳ", "y")

            //'Dấu Sắc
            .Replace("Á", "A")
            .Replace("á", "a")
            .Replace("Ắ", "A")
            .Replace("ắ", "a")
            .Replace("Ấ", "A")
            .Replace("ấ", "a")
            .Replace("É", "E")
            .Replace("é", "e")
            .Replace("Ế", "E")
            .Replace("ế", "e")
            .Replace("Í", "I")
            .Replace("í", "i")
            .Replace("Ó", "O")
            .Replace("ó", "o")
            .Replace("Ố", "O")
            .Replace("ố", "o")
            .Replace("Ớ", "O")
            .Replace("ớ", "o")
            .Replace("Ú", "U")
            .Replace("ú", "u")
            .Replace("Ứ", "U")
            .Replace("ứ", "u")
            .Replace("Ý", "Y")
            .Replace("ý", "y")

            //'Dấu Hỏi
            .Replace("Ả", "A")
            .Replace("ả", "a")
            .Replace("Ẳ", "A")
            .Replace("ẳ", "a")
            .Replace("Ẩ", "A")
            .Replace("ẩ", "a")
            .Replace("Ẻ", "E")
            .Replace("ẻ", "e")
            .Replace("Ể", "E")
            .Replace("ể", "e")
            .Replace("Ỉ", "I")
            .Replace("ỉ", "i")
            .Replace("Ỏ", "O")
            .Replace("ỏ", "o")
            .Replace("Ổ", "O")
            .Replace("ổ", "o")
            .Replace("Ở", "O")
            .Replace("ở", "o")
            .Replace("Ủ", "U")
            .Replace("ủ", "u")
            .Replace("Ử", "U")
            .Replace("ử", "u")
            .Replace("Ỷ", "Y")
            .Replace("ỷ", "y")

            //'Dấu Ngã   
            .Replace("Ã", "A")
            .Replace("ã", "a")
            .Replace("Ẵ", "A")
            .Replace("ẵ", "a")
            .Replace("Ẫ", "A")
            .Replace("ẫ", "a")
            .Replace("Ẽ", "E")
            .Replace("ẽ", "e")
            .Replace("Ễ", "E")
            .Replace("ễ", "e")
            .Replace("Ĩ", "I")
            .Replace("ĩ", "i")
            .Replace("Õ", "O")
            .Replace("õ", "o")
            .Replace("Ỗ", "O")
            .Replace("ỗ", "o")
            .Replace("Ỡ", "O")
            .Replace("ỡ", "o")
            .Replace("Ũ", "U")
            .Replace("ũ", "u")
            .Replace("Ữ", "U")
            .Replace("ữ", "u")
            .Replace("Ỹ", "Y")
            .Replace("ỹ", "y")

            //'Dẫu Nặng
            .Replace("Ạ", "A")
            .Replace("ạ", "a")
            .Replace("Ặ", "A")
            .Replace("ặ", "a")
            .Replace("Ậ", "A")
            .Replace("ậ", "a")
            .Replace("Ẹ", "E")
            .Replace("ẹ", "e")
            .Replace("Ệ", "E")
            .Replace("ệ", "e")
            .Replace("Ị", "I")
            .Replace("ị", "i")
            .Replace("Ọ", "O")
            .Replace("ọ", "o")
            .Replace("Ộ", "O")
            .Replace("ộ", "o")
            .Replace("Ợ", "O")
            .Replace("ợ", "o")
            .Replace("Ụ", "U")
            .Replace("ụ", "u")
            .Replace("Ự", "U")
            .Replace("ự", "u")
            .Replace("Ỵ", "Y")
            .Replace("ỵ", "y")
            .Replace("Đ", "D")
            .Replace("Ð", "D")
            .Replace("đ", "d");
            return text;
        }

        private static List<string> SplitStringToList(string inputText, char cSlpit)
        {
            if (string.IsNullOrEmpty(inputText))
            {
                return null;
            }
            return new List<string>(inputText.Split(new char[] { cSlpit }));
        }
      
        public static bool? GetNullableBool(this Dictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return null;
            }
            if (dic[key] == null)
            {
                return null;
            }

            return GetBool(dic[key]);
        }

        /// <summary>
        /// dau ":"
        /// </summary>
        private const string _COLON = ":";

        /// <summary>
        /// dau " : "
        /// </summary>
        private const string _COLON_SPACE = " : ";


        /// <summary>
        /// return short sms content
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>31/03/2013</date>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string ToShortSMSContent(this string content, int numToShort)
        {
            if (string.IsNullOrEmpty(content))
            {
                return string.Empty;
            }

            if (content.Length <= numToShort)
            {
                return content;
            }

            string[] tmp = content.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string shortContent = string.Empty;

            if (tmp.Length == 1)
            {
                if (tmp[0].Length > numToShort)
                {
                    shortContent = tmp[0].Substring(0, numToShort) + "...";
                }
                else
                {
                    shortContent = tmp[0];
                }
            }
            else
            {
                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0, size = tmp.Length; i < size; i++)
                {
                    if (strBuilder.Length > numToShort)
                    {
                        shortContent = strBuilder.ToString().Substring(0, numToShort) + "...";
                        break;
                    }
                    if (i == size - 1)
                    {
                        shortContent = strBuilder.ToString();
                        break;
                    }

                    strBuilder.Append(tmp[i]).Append(" ");
                }
            }
            return shortContent;
        }


    }
}