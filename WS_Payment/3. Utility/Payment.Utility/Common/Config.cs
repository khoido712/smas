﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Payment.Utility.Constants;

namespace Payment.Utility.Common
{
    public class Config
    {

        /// <summary>
        /// Lay thong tin chuoi
        /// </summary>
        /// <author date="2014/01/16">HaiVT</author>
        public static string GetString(string key)
        {
            return WebConfigurationManager.AppSettings[key];
        }

        /// <summary>
        /// Lay thong tin chuoi
        /// </summary>
        /// <author date="2014/01/16">HaiVT</author>
        public static int GetInt(string key)
        {
            return Convert.ToInt32(WebConfigurationManager.AppSettings[key]);
        }

    }
}
