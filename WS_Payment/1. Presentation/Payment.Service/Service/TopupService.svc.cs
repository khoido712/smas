﻿using log4net;
using Payment.Service.DI;
using Payment.Service.IService;
using Payment.Utility.Object.RequestObject;
using Payment.Utility.Object.ResponseObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Payment.Service.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TopupService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TopupService.svc or TopupService.svc.cs at the Solution Explorer and start debugging.
    public class TopupService : ITopupService
    {
        #region declare
        private static ILog _logger = LogManager.GetLogger(typeof(TopupService).Name);
        #endregion 


        #region topupCard
        /// <summary>
        /// topup card
        /// </summary>
        /// <author date="140528">HaiVT</author>
        /// <param name="dic"></param>
        public TopupResponse TopUp(TopupRequest request)
        {
            TopupResponse response = new TopupResponse();
            try
            {
                SetterInjectionFactory setter = new SetterInjectionFactory();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["EWalletID"] = request.EWalletID;
                dic["CardSerial"] = request.CardSerial;
                dic["pinCard"] = request.PinCard;
                response = setter.TopupBusiness.TopUp(dic);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
                response.Success = false;
                response.ErrorCode = "199";
                response.ErrorMessage = "Lỗi exception Payment service";
            }

            return response;
        }
        #endregion

        #region Tim kiem lich su giao dich trong khoang thoi gian tu ngay bat dau den ngay ket thuc
        /// <summary>
        /// Tim kiem lich su giao dich trong khoang thoi gian tu ngay bat dau den ngay ket thuc
        /// </summary>
        /// <param name="fromdate">Ngay bat dau</param>
        /// <param name="todate">Ngay ket thuc</param>
        /// <param name="beginIndex">Chi so cua hang bat dau lay</param>
        /// <param name="numberOfRecord">So luong hang can lay</param>
        /// <param name="totalNumberOfRecord">Tong so luong giao dich trong khoang thoi gian can tim</param>
        /// <returns>Danh sach cac giao dich da thuc hien trong khoang thoi gian can tim</returns>
        public FindTransactionResponse FindTransaction(FindTransactionRequest request)
        {
            FindTransactionResponse response = new FindTransactionResponse();
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["EWalletID"] = request.eWalletID;
                dic["FromDate"] = request.fromdate;
                dic["ToDate"] = request.todate;
                dic["beginIndex"] = request.beginIndex;
                dic["numberOfRetrievedRecord"] = request.numberOfRetrievedRecord;
                SetterInjectionFactory setter = new SetterInjectionFactory();
                response = setter.EWalletTransactionBusiness.FindTransaction(dic);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }

            return response;
        }
        #endregion

    }
}
