﻿using Payment.Business.IBusiness;
using Payment.Service.DI;
using Payment.Service.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Payment.Utility.Object.ResponseObject;
using Payment.Utility.Object.RequestObject;
using log4net;

namespace Payment.Service.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EWalletService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EWalletService.svc or EWalletService.svc.cs at the Solution Explorer and start debugging.
    public class EWalletService : IEWalletService
    {
        #region declare
        private static ILog _logger = LogManager.GetLogger(typeof(EWalletService).Name);
        #endregion 

        #region get ewalllet
        /// <summary>
        /// get ewallet
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public EWalletResponse GetEWallet(EWalletRequest request)
        
        {
            EWalletResponse response = new EWalletResponse();
            try
            {
                SetterInjectionFactory setter = new SetterInjectionFactory();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["UserID"] = request.UserID;
                dic["SchoolUser"] = request.SchoolUser;
                dic["SupervisingDeptUser"] = request.SupervisingDeptUser;
                response = setter.EWalletBusiness.GetEWallet(dic);                
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }

            return response;
        }
        #endregion

        #region change amount sub
        /// <summary>
        /// change amount sub
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public EWalletResponse ChangesAmountSub(EWalletTransAmountRequest request)
        {
            EWalletResponse response = new EWalletResponse();
            try
            {
                SetterInjectionFactory setter = new SetterInjectionFactory();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["EWalletID"] = request.EWalletID;
                dic["Amount"] = request.Amount;
                response = setter.EWalletBusiness.ChangesAmountSub(dic);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);
            }

            return response;
        }
        #endregion

        
    }
}
