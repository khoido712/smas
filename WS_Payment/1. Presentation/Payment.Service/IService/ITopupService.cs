﻿using Payment.Utility.Object.RequestObject;
using Payment.Utility.Object.ResponseObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Payment.Service.IService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITopupService" in both code and config file together.
    [ServiceContract]
    public interface ITopupService
    {
        #region topup
        /// <summary>
        /// topup card
        /// </summary>
        /// <author date="140528">HaiVT</author>
        /// <param name="dic"></param>
        [OperationContract]
        TopupResponse TopUp(TopupRequest request);
        #endregion

        /// <summary>
        /// Tim kiem lich su giao dich trong khoang thoi gian tu ngay bat dau den ngay ket thuc
        /// </summary>
        /// <param name="fromdate">Ngay bat dau</param>
        /// <param name="todate">Ngay ket thuc</param>
        /// <param name="beginIndex">Chi so cua hang bat dau lay</param>
        /// <param name="numberOfRecord">So luong hang can lay</param>
        /// <param name="totalNumberOfRecord">Tong so luong giao dich trong khoang thoi gian can tim</param>
        /// <returns>Danh sach cac giao dich da thuc hien trong khoang thoi gian can tim</returns>
        [OperationContract]
        FindTransactionResponse FindTransaction(FindTransactionRequest request);
    }
}
