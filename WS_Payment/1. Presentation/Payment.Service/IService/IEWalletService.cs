﻿using Payment.Utility.Object.RequestObject;
using Payment.Utility.Object.ResponseObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Payment.Service.IService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEWalletService" in both code and config file together.
    [ServiceContract]
    public interface IEWalletService
    {
        #region get ewalllet
        /// <summary>
        /// get ewallet
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        EWalletResponse GetEWallet(EWalletRequest request);
        #endregion

        #region change amount sub
        /// <summary>
        /// change amount sub
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        [OperationContract]
        EWalletResponse ChangesAmountSub(EWalletTransAmountRequest request);
        #endregion

        
    }
}
