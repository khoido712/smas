﻿using Microsoft.Practices.Unity;
using Payment.Business.Business;
using Payment.Business.IBusiness;
using Payment.ServiceLoader.ILoader;
using Payment.ServiceLoader.Loader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Payment.Service.DI
{
    public class SetterInjectionDTO
    {
        private UnityContainer _unityContainer;

        /// <summary>
        /// register all your components with the container here
        /// it is NOT necessary to register your controllers
        /// </summary>
        /// <author date="2014/02/08">HaiVT</author>
        public UnityContainer UnityContainer
        {
            get
            {
                if (_unityContainer == null)
                {
                    _unityContainer = new UnityContainer();                    
                    _unityContainer.RegisterType<IAccountBusiness, AccountBusiness>()
                        .RegisterType<IEWalletBusiness, EWalletBusiness>()
                        .RegisterType<ILogBusiness, LogBusiness>()
                        .RegisterType<IEWalletTransactionBusiness, EWalletTransactionBusiness>()
                        .RegisterType<ITopupBusiness, TopupBusiness>()
                        .RegisterType<ITransactionTypeBusiness, TransactionTypeBusiness>()
                        .RegisterType<IScratchCardServiceLoader, ScratchCardServiceLoader>();

                }
                
                return _unityContainer;
            }
        }

        private IAccountBusiness _accountBusiness;

        /// <summary>
        /// AccountBusiness DTO
        /// </summary>
        [Dependency]
        public IAccountBusiness AccountBusiness
        {            
            get
            {
                return _accountBusiness;
            }
            set
            {
                _accountBusiness = value;
            }
        }

        private IEWalletBusiness _eWalletBusiness;

        /// <summary>
        /// EWalletBusiness DTO
        /// </summary>
        [Dependency]
        public IEWalletBusiness EWalletBusiness
        {
            get
            {
                return _eWalletBusiness;
            }
            set
            {
                _eWalletBusiness = value;
            }
        }

        private ILogBusiness _logBusiness;

        /// <summary>
        /// LogBusiness DTO
        /// </summary>
        [Dependency]
        public ILogBusiness LogBusiness
        {
            get
            {
                return _logBusiness;
            }
            set
            {
                _logBusiness = value;
            }
        }

        private IEWalletTransactionBusiness _eWalletTransactionBusiness;

        /// <summary>
        /// EWalletTransactionBusiness DTO
        /// </summary>
        [Dependency]
        public IEWalletTransactionBusiness EWalletTransactionBusiness
        {
            get
            {
                return _eWalletTransactionBusiness;
            }
            set
            {
                _eWalletTransactionBusiness = value;
            }
        }

        private ITopupBusiness _topupBusiness;

        /// <summary>
        /// TopupBusiness DTO
        /// </summary>
        [Dependency]
        public ITopupBusiness TopupBusiness
        {
            get
            {
                return _topupBusiness;
            }
            set
            {
                _topupBusiness = value;
            }
        }

        private ITransactionTypeBusiness _transactionTypeBusiness;

        /// <summary>
        /// TransactionTypeBusiness DTO
        /// </summary>
        [Dependency]
        public ITransactionTypeBusiness TransactionTypeBusiness
        {
            get
            {
                return _transactionTypeBusiness;
            }
            set
            {
                _transactionTypeBusiness = value;
            }
        }

        private IScratchCardServiceLoader _scratchCardServiceLoader;

        /// <summary>
        /// ScratchCardServiceLoader DTO
        /// </summary>
        [Dependency]
        public IScratchCardServiceLoader ScratchCardServiceLoader
        {
            get
            {
                return _scratchCardServiceLoader;
            }
            set
            {
                _scratchCardServiceLoader = value;
            }
        } 
    }
}