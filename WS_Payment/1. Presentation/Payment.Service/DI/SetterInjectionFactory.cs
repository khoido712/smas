﻿using Payment.Business.IBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using Payment.ServiceLoader.ILoader;

namespace Payment.Service.DI
{
    public class SetterInjectionFactory
    {
        private SetterInjectionDTO _setterInjection;

        /// <summary>
        /// setterInjection object
        /// </summary>
        private SetterInjectionDTO SetterInjection
        {
            get
            {
                if (_setterInjection == null)
                {
                    _setterInjection = new SetterInjectionDTO();
                    _setterInjection = _setterInjection.UnityContainer.Resolve<SetterInjectionDTO>();
                }

                return _setterInjection;
            }
        }

        /// <summary>
        /// InjectionProperty AccountBusiness
        /// </summary>
        /// <author date="2013/02/08">HaiVT</author>
        public IAccountBusiness AccountBusiness
        {
            get
            {
                return SetterInjection.AccountBusiness;
            }
        }

        /// <summary>
        /// InjectionProperty EWalletBusiness
        /// </summary>
        /// <author date="2013/02/08">HaiVT</author>
        public IEWalletBusiness EWalletBusiness
        {
            get
            {
                return SetterInjection.EWalletBusiness;
            }
        }

        /// <summary>
        /// InjectionProperty LogBusiness
        /// </summary>
        /// <author date="2013/02/08">HaiVT</author>
        public ILogBusiness LogBusiness
        {
            get
            {
                return SetterInjection.LogBusiness;
            }
        }

        /// <summary>
        /// InjectionProperty EWalletTransactionBusiness
        /// </summary>
        /// <author date="2013/02/08">HaiVT</author>
        public IEWalletTransactionBusiness EWalletTransactionBusiness
        {
            get
            {
                return SetterInjection.EWalletTransactionBusiness;
            }
        }

        /// <summary>
        /// InjectionProperty TopupBusiness
        /// </summary>
        /// <author date="2013/02/08">HaiVT</author>
        public ITopupBusiness TopupBusiness
        {
            get
            {
                return SetterInjection.TopupBusiness;
            }
        }

        /// <summary>
        /// InjectionProperty TransactionTypeBusiness
        /// </summary>
        /// <author date="2013/02/08">HaiVT</author>
        public ITransactionTypeBusiness TransactionTypeBusiness
        {
            get
            {
                return SetterInjection.TransactionTypeBusiness;
            }
        }

        /// <summary>
        /// InjectionProperty ScratchCardServiceLoader
        /// </summary>
        /// <author date="2013/02/08">HaiVT</author>
        public IScratchCardServiceLoader ScratchCardServiceLoader
        {
            get
            {
                return SetterInjection.ScratchCardServiceLoader;
            }
        }
    }
}