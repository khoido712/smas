﻿using Payment.Business.IBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using Payment.ServiceLoader.ILoader;

namespace Payment.Service.DI
{
    public static class UnityConfig
    {
        /// <summary>
        /// Resolver container
        /// </summary>
        /// <author date="2013/02/08">HaiVT</author>
        public static void RegisterComponents()
        {
            SetterInjectionDTO setterInjectionDTO = new SetterInjectionDTO();            
            setterInjectionDTO.UnityContainer.Resolve<IAccountBusiness>();
            setterInjectionDTO.UnityContainer.Resolve<IEWalletBusiness>();
            setterInjectionDTO.UnityContainer.Resolve<ILogBusiness>();
            setterInjectionDTO.UnityContainer.Resolve<IEWalletTransactionBusiness>();
            setterInjectionDTO.UnityContainer.Resolve<ITopupBusiness>();
            setterInjectionDTO.UnityContainer.Resolve<ITransactionTypeBusiness>();
            setterInjectionDTO.UnityContainer.Resolve<IScratchCardServiceLoader>();    
        }
    }
}