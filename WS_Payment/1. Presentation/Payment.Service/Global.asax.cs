﻿using Payment.Service.DI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Payment.Service
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            // DI
            UnityConfig.RegisterComponents();

            // log4net
            log4net.Config.XmlConfigurator.Configure();
        }      
    }
}